layout (location = 0) in vec3  a_position;

layout (location = 0) in vec3  a_position;
layout (location = 1) in vec3  a_normal;

layout (location = 0) in vec3  a_position;
layout (location = 1) in vec3  a_normal;
layout (location = 2) in uvec4 a_joints_0;
layout (location = 3) in vec4  a_weights_0;

// 这种模型比较多
layout (location = 0) in vec3  a_position;
layout (location = 1) in vec3  a_normal;
layout (location = 2) in vec2  a_texcoord_0;
// 带四权重骨骼动画
layout (location = 0) in vec3  a_position;
layout (location = 1) in vec3  a_normal;
layout (location = 2) in vec2  a_texcoord_0;
layout (location = 3) in uvec4 a_joints_0;
layout (location = 4) in vec4  a_weights_0;

mmVKTFAttributePOSITION  = 0,   总是有这个属性              1
mmVKTFAttributeNORMAL    = 1,   有或者没有                  2
mmVKTFAttributeTANGENT   = 2,   有或者没有                  2
mmVKTFAttributeTEXCOORD0 = 3,   有或者没有                  2
mmVKTFAttributeTEXCOORD1 = 4,   有或者没有                  2
mmVKTFAttributeCOLOR0    = 5,   有或者没有，分有vec3和vec4  4
mmVKTFAttributeJOINTS0   = 6,   有或者没有                  2
mmVKTFAttributeJOINTS1   = 7,   有或者没有                  2
mmVKTFAttributeWEIGHTS0  = 8,   伴随JOINTS0                 1
mmVKTFAttributeWEIGHTS1  = 9,   伴随JOINTS1                 1

种类 = 1 x 2 x 2 x 2 x 2 x 4 x 2 x 2 x 1 x 1 = 256
three.js 里面是有一些宏来辨别顶点数据，然后动态生成着色器代码
那么在vulkan里面，该怎么做合适呢
或者我们仅支持特定几个格式？



/////////////////////
// INPUT VARIABLES //
/////////////////////
layout(location = 3) in vec3 inputTangent;
layout(location = 4) in vec3 inputBiTangent;

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
layout(location = 5) out mat3 TBN;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	vec3 tangent = normalize(vec3(modelMatrix * vec4(inputTangent, 0.0f)));
    //vec3 bitangent = normalize(vec3(modelMatrix * vec4(inputBiTangent, 0.0f)));
    // re-orthogonalize T with respect to N
    tangent = normalize(tangent - dot(tangent, normal_world.xyz) * normal_world.xyz);
    // then retrieve perpendicular vector B with the cross product of T and N
    vec3 bitangent = cross(normal_world.xyz, tangent);

    TBN = mat3(tangent, bitangent, normal_world.xyz);
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
void main()
{		
    vec3 tangent_normal = texture(normalMap, uv).rgb;
    tangent_normal = normalize(tangent_normal * 2.0f - 1.0f);   
    vec3 N = normalize(TBN * tangent_normal); 
}

varying mat3 vTbnMatrix;
void main(void)
{
	vec3 n = normalMatrix * aNormal;
	vec3 t = normalMatrix * vec3(aTangent.xyz);
	vec3 b = cross(n, t) * aTangent.w;
	vTbnMatrix = mat3(t, b, n);
}

SelectShader
{
    //enum mmVKTFAttributeIndex
    //{
    //    mmVKTFAttributePOSITION  = 0,  VEC3 FLOAT
    //    mmVKTFAttributeNORMAL    = 1,  VEC3 FLOAT
    //    mmVKTFAttributeTANGENT   = 2,  VEC4 FLOAT
    //    mmVKTFAttributeTEXCOORD0 = 3,  VEC2 FLOAT (UNSIGNED_BYTE)normalized (UNSIGNED_SHORT)normalized       4 2
    //    mmVKTFAttributeTEXCOORD1 = 4,  VEC2 FLOAT (UNSIGNED_BYTE)normalized (UNSIGNED_SHORT)normalized       4 2
    //    mmVKTFAttributeCOLOR0    = 5,  VEC3 FLOAT VEC4 (UNSIGNED_BYTE)normalized (UNSIGNED_SHORT)normalized  4 2
    //    mmVKTFAttributeJOINTS0   = 6,  VEC4 0(UNSIGNED_BYTE) 1(UNSIGNED_SHORT)                               2 1
    //    mmVKTFAttributeJOINTS1   = 7,  VEC4 0(UNSIGNED_BYTE) 1(UNSIGNED_SHORT)                               2 1
    //    mmVKTFAttributeWEIGHTS0  = 8,  VEC4 FLOAT (UNSIGNED_BYTE)normalized (UNSIGNED_SHORT)normalized       4 2
    //    mmVKTFAttributeWEIGHTS1  = 9,  VEC4 FLOAT (UNSIGNED_BYTE)normalized (UNSIGNED_SHORT)normalized       4 2
    //
    //    mmVKTFAttributeMax,
    //};
	
    //    mmVKTFAttributePOSITION  = 0,  
    //    mmVKTFAttributeNORMAL    = 1,  
    //    mmVKTFAttributeTANGENT   = 2,  
    //    mmVKTFAttributeTEXCOORD0 = 3,  0x0003
    //    mmVKTFAttributeTEXCOORD1 = 4,  0x000C
    //    mmVKTFAttributeCOLOR0    = 5,  0x0030
    //    mmVKTFAttributeJOINTS0   = 6,  0x0040
    //    mmVKTFAttributeJOINTS1   = 7,  0x0080
    //    mmVKTFAttributeWEIGHTS0  = 8,  0x0300
    //    mmVKTFAttributeWEIGHTS1  = 9,  0x0C00
	
    //    mmVKTFAttributePOSITION  = 0,  
    //    mmVKTFAttributeNORMAL    = 1,         0x0000001
    //    mmVKTFAttributeTANGENT   = 2,         0x0000002
    //    mmVKTFAttributeTEXCOORD0 = 3,         0x00000C0
    //    mmVKTFAttributeTEXCOORD1 = 4,         0x0000300
    //    mmVKTFAttributeCOLOR0    = 5,         0x0000C00
    //    mmVKTFAttributeJOINTS0   = 6,         0x0003000
    //    mmVKTFAttributeJOINTS1   = 7,         0x000C000
    //    mmVKTFAttributeWEIGHTS0  = 8,         0x0030000
    //    mmVKTFAttributeWEIGHTS1  = 9,         0x00C0000
	
    //    mmVKTFTextureBaseColor         = 0,   0x0100000
    //    mmVKTFTextureMetallicRoughness = 1,   0x0200000
    //    mmVKTFTextureNormal            = 2,   0x0400000
    //    mmVKTFTextureOcclusion         = 3,   0x0800000
    //    mmVKTFTextureEmissive          = 4,   0x1000000

    //enum mmVKTFTextureIndex
    //{
    //    mmVKTFTextureBaseColor         = 0,
    //    mmVKTFTextureMetallicRoughness = 1,
    //    mmVKTFTextureNormal            = 2,
    //    mmVKTFTextureOcclusion         = 3,
    //    mmVKTFTextureEmissive          = 4,
    //
    //    mmVKTFTextureMax,
    //};

    //    mmVKTFAttributePOSITION  = 0,                   0x0001
    //    mmVKTFAttributeNORMAL    = 1,         0x0001    0x0002  
    //    mmVKTFAttributeTANGENT   = 2,         0x0002    0x0004
    //    mmVKTFAttributeTEXCOORD0 = 3,         0x0004    0x0008
    //    mmVKTFAttributeTEXCOORD1 = 4,         0x0008    0x0010
    //    mmVKTFAttributeCOLOR0    = 5,         0x0030    0x0060
    //    mmVKTFAttributeJOINTS0   = 6,         0x0040    0x0080
    //    mmVKTFAttributeJOINTS1   = 7,         0x0080    0x0100
    //
    //    mmVKTFTextureBaseColor         = 0,   0x0100
    //    mmVKTFTextureMetallicRoughness = 1,   0x0200
    //    mmVKTFTextureNormal            = 2,   0x0400
    //    mmVKTFTextureOcclusion         = 3,   0x0800
    //    mmVKTFTextureEmissive          = 4,   0x1000
	
	material
    //    mmVKTFTextureBaseColor         = 0,   0x01
    //    mmVKTFTextureMetallicRoughness = 1,   0x02
    //    mmVKTFTextureNormal            = 2,   0x04
    //    mmVKTFTextureOcclusion         = 3,   0x08
    //    mmVKTFTextureEmissive          = 4,   0x10
	
	贴图不会影响顶点着色器，只会影响片段着色器
	
	
	
	
	pvii    名字 = 0x1000000
	psmi    名字 = 0x1000
	material名字 = 0x10
	
	顶点着色器参数影响
    //    mmVKTFAttributePOSITION  = 0,                   0x0001 总是有
    //    mmVKTFAttributeNORMAL    = 1,         0x0001    0x0002 可选，影响 
    //    mmVKTFAttributeTANGENT   = 2,         0x0002    0x0004 可选，影响
    //    mmVKTFAttributeTEXCOORD0 = 3,         0x0004    0x0008 可选，影响
    //    mmVKTFAttributeTEXCOORD1 = 4,         0x0008    0x0010 可选，影响
    //    mmVKTFAttributeCOLOR0    = 5,         0x0030    0x0060 可选，影响
    //    mmVKTFAttributeJOINTS0   = 6,         0x0040    0x0080 可选，影响
    //    mmVKTFAttributeJOINTS1   = 7,         0x0080    0x0100 可选，影响
    //    mmVKTFAttributeWEIGHTS0  = 8,         
    //    mmVKTFAttributeWEIGHTS1  = 9,  
	
	片段着色器参数影响
    //    mmVKTFAttributePOSITION  = 0,                   0x0001 总是有
    //    mmVKTFAttributeNORMAL    = 1,         0x0001    0x0002 可选，影响 
    //    mmVKTFAttributeTANGENT   = 2,         0x0002    0x0004 可选，影响
    //    mmVKTFAttributeTEXCOORD0 = 3,         0x0004    0x0008 可选，影响
    //    mmVKTFAttributeTEXCOORD1 = 4,         0x0008    0x0010 可选，影响
    //    mmVKTFAttributeCOLOR0    = 5,         0x0030    0x0060 可选，影响
    //    mmVKTFAttributeJOINTS0   = 6,         0x0040    0x0080 可选，不影响
    //    mmVKTFAttributeJOINTS1   = 7,         0x0080    0x0100 可选，不影响
    //    mmVKTFAttributeWEIGHTS0  = 8,         
    //    mmVKTFAttributeWEIGHTS1  = 9,         
	
	v_position   计算时在世界坐标系，方便计算灯光
	v_normal     如无骨骼动画，可以使用传入的世界法线矩阵
	v_tbn        如传入切线空间，则计算TBN矩阵
	v_texcoord_x 原样传入片段着色器
	v_color_x    原样传入片段着色器
	a_joints_x   仅在骨骼动画用于索引骨骼矩阵
	a_weights_x  仅在骨骼动画用于骨骼权重
	
	有无骨骼                   
	有无切线，有切线必有法线   
	有无法线
	有无颜色 0                 
	有无纹理 0 1

	纹理坐标设计只有01两种
	
	顶点着色器 常用81种                         取名位
	无法线无切线 有法线无切线 有法线有切线      3  000F 0000 0001 0002
	无纹理       有纹理0      有纹理1           3  00F0 0000 0010 0020
	无颜色       有颜色0vec3  有颜色0vec4       3  0F00 0000 0100 0200
	无骨骼       有骨骼0      有骨骼1           3  F000 0000 1000 2000
	
	片段着色器 常用27种                         取名位
	无法线无切线 有法线无切线 有法线有切线      3  000F 0000 0001 0002
	无纹理       有纹理0      有纹理1           3  00F0 0000 0010 0020
	无颜色       有颜色0vec3  有颜色0vec4       3  0F00 0000 0100 0200
	
	layout (location = 0) in vec3  a_position;
	layout (location = 1) in vec3  a_normal;
	layout (location = 1) in vec4  a_tangent;
	layout (location = 2) in vec2  a_texcoord_0;
	layout (location = 2) in vec3  a_color_0;    layout (location = 2) in vec4  a_color_0;
	layout (location = 3) in uvec4 a_joints_0;
	layout (location = 4) in vec4  a_weights_0;

	NONE            0
	FLOAT           1
	UNSIGNED_BYTE   2
	UNSIGNED_SHORT  3
	
	// POSITION   "VEC3" 5126 (FLOAT)
	
	// NORMAL     "VEC3" 5126 (FLOAT)
	
	// TANGENT    "VEC4" 5126 (FLOAT)
	
	// TEXCOORD_0 "VEC2" 5126 (FLOAT)                     0
	//                   5121 (UNSIGNED_BYTE)normalized   1
	//                   5123 (UNSIGNED_SHORT)normalized  2
	
	// COLOR_0	  "VEC3" 5126 (FLOAT)                     0
	//            "VEC4" 5121 (UNSIGNED_BYTE)normalized   1
	//                   5123 (UNSIGNED_SHORT)normalized  2
	
	// JOINTS_0   "VEC4" 5121 (UNSIGNED_BYTE)             0
	//                   5123 (UNSIGNED_SHORT)            1

	// WEIGHTS_0  "VEC4" 5126 (FLOAT)                     0
	//                   5121 (UNSIGNED_BYTE)normalized   1
	//                   5123 (UNSIGNED_SHORT)normalized  2

	管线取名区分参数类型
	无法线无切线 有法线无切线 有法线有切线      3  0000000F     0  1  2
	无纹理       有纹理0      有纹理1           4  00000FF0     00 0x xx
	无颜色       有颜色0vec3  有颜色0vec4       7  000FF000     x  x  x
	无骨骼       有骨骼0      有骨骼1          13  0FF00000     xx xx xx

WaterBottle.glb
mmVKTFMaterial-11111
spiv/gltf/3d-0012.vert.spv
spiv/gltf/3d-012-11111.frag.spv
3d-0012-11111-0000003-40

LittlestTokyo.glb
spiv/gltf/3d-022-01001.frag.spv
spiv/gltf/3d-012-00001.frag.spv
spiv/gltf/3d-012-01000.frag.spv
spiv/gltf/3d-012-10001.frag.spv
spiv/gltf/3d-002-00000.frag.spv

KHR_materials_emissive_strength
EmissiveStrengthTest.glb
spiv/gltf/3d-001-00000.frag.spv
spiv/gltf/3d-011-00001.frag.spv
spiv/gltf/3d-001-00000.frag.spv
spiv/gltf/3d-001-00000.frag.spv
spiv/gltf/3d-001-00000.frag.spv
spiv/gltf/3d-001-00000.frag.spv
spiv/gltf/3d-011-00001.frag.spv

Sponza.gltf
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-011-00001.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-012-00111.frag.spv
spiv/gltf/3d-011-00001.frag.spv


android texture vkCreateGraphicsPipelines failure
OpenGL ES Shader Compiler Version: EV031.32.02.00

model/Flamingo.glb
model/Stork.glb
model/Flower.glb
model/RobotExpressive.glb

出错的
model/Cesium_Man.glb
Pipeline lName: mmVKTFMaterial-1
Pipeline vName: spiv/gltf/3d-1011.vert.spv
Pipeline fName: spiv/gltf/3d-011-00001.frag.spv
Pipeline xName: 3d-1011-00001-0302003-40

model/Flamingo.glb
Pipeline lName: mmVKTFMaterial-0
Pipeline vName: spiv/gltf/3d-0110.vert.spv
Pipeline fName: spiv/gltf/3d-110-00000.frag.spv
Pipeline xName: 3d-0110-00000-0000303-40

model/Stork.glb
Pipeline lName: mmVKTFMaterial-0
Pipeline vName: spiv/gltf/3d-0110.vert.spv
Pipeline fName: spiv/gltf/3d-110-00000.frag.spv
Pipeline xName: 3d-0110-00000-0000303-40

model/Flower.glb
Pipeline lName: mmVKTFMaterial-0
Pipeline vName: spiv/gltf/3d-0000.vert.spv
Pipeline fName: spiv/gltf/3d-000-00000.frag.spv
Pipeline xName: 3d-0000-00000-0000000-40

model/RobotExpressive.glb
Pipeline lName: mmVKTFMaterial-0
Pipeline vName: spiv/gltf/3d-1001.vert.spv
Pipeline fName: spiv/gltf/3d-001-00000.frag.spv
Pipeline xName: 3d-1001-00000-0302000-40

对的
spiv/gltf/3d-0011.vert.spv
spiv/gltf/3d-011-00001.frag.spv

samplescene.gltf
spiv/gltf/3d-0011.vert.spv
spiv/gltf/3d-011-00000.frag.spv
3d-0011-00000-0000003-31