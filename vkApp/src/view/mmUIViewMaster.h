/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewMaster_h__
#define __mmUIViewMaster_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

// Type base define.
mmUIViewTypeDefine(Master);

struct mmUIViewMaster
{
    struct mmUIView view;
    /* UIView weak reference. */
    struct mmUIView* MyImage;
    struct mmUIView* MyText;
    struct mmUIView* MyEditBox;
    struct mmUIView* MyButton;
};

void
mmUIViewMaster_Init(
    struct mmUIViewMaster*                         p);

void
mmUIViewMaster_Destroy(
    struct mmUIViewMaster*                         p);

void
mmUIViewMaster_OnPrepare(
    struct mmUIViewMaster*                         p);

void
mmUIViewMaster_OnDiscard(
    struct mmUIViewMaster*                         p);

/* special */

void
mmUIViewMaster_OnEventClicked(
    struct mmUIViewMaster*                         p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewMaster_h__
