/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewModel.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "core/mmKeyCode.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKSwapchain.h"
#include "vk/mmVKCommandBuffer.h"

#include "nwsi/mmEventSurface.h"

// Type base implement.
mmUIViewTypeImplement(Model);

void
mmUIViewModel_Init(
    struct mmUIViewModel*                          p)
{
    mmUIView_Init(&p->view);
    
    mmVKScene_Init(&p->vScene);
    mmVKTFObject_Init(&p->vAsset1);
    mmString_Init(&p->vModelPath);

    p->l = 1.0f;
    mmVec2Make(p->angle, 0.0f, 45.0f);
}

void
mmUIViewModel_Destroy(
    struct mmUIViewModel*                          p)
{
    mmVec2Make(p->angle, 0.0f, 45.0f);
    p->l = 1.0f;

    mmString_Destroy(&p->vModelPath);
    mmVKTFObject_Destroy(&p->vAsset1);
    mmVKScene_Destroy(&p->vScene);
    
    mmUIView_Destroy(&p->view);
}

void
mmUIViewModel_OnPrepare(
    struct mmUIViewModel*                          p)
{
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;
    struct mmVKSwapchain* pSwapchain = p->view.context->swapchain;
    
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewModel.view.json");
    
    //p->vScene.hCamera.n = 50;
    //p->vScene.hCamera.f = 2000;

    uint32_t* hWindowSize = pSwapchain->windowSize;
    float n = p->vScene.hCamera.n;
    float f = p->vScene.hCamera.f;
    float aspect = (float)hWindowSize[0] / (float)hWindowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    float eye[3] = { 1.8f, 2.0f, 5.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0f };
    
    float ProjectionView[4][4];
    float Projection[4][4];
    float View[4][4];
    //mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, 0.01f, 1000.0f);
    mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, n, f);
    mmMat4x4LookAtRH(View, eye, origin, up);
    
    mmMat4x4Mul(ProjectionView, Projection, View);
    
    const char* path = "model/Cesium_Man.glb";
    //const char* path = "F:/package/gltf/Vulkan/data/models/FlightHelmet/glTF/FlightHelmet.gltf";
    //const char* path = "model/shadowscene_fire.gltf";
    //const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/LittlestTokyo.glb";

    struct mmVKLightInfo hLightInfos[] =
    {
        {
            { -30.0f, -40.0f, 0.0f },   // float direction[3];
            { 0.0f, 0.0f, 0.0f },       // float position[3];
            { 1.0f, 1.0f, 1.0f },       // float color[3];
            0.0f,                       // float range;
            1.0f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypeDirectional, // int type;
            0,                          // int castShadow;
            4,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
    };
    
    struct mmVectorValue hInfos = mmVectorValue_Make(hLightInfos, struct mmVKLightInfo);
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    p->vScene.uboSceneFragment.lightCount = 1;
    mmVKScene_Prepare(&p->vScene, pAssets, &hInfos, pSwapchain->depthFormat);

    mmString_Assigns(&p->vModelPath, path);
    mmVKTFModelLoader_LoadFromPath(pModelLoader, path);

    mmVKTFObject_Prepare(&p->vAsset1, pAssets, path);
    
    mmVKTFObject_PlayAnimation(&p->vAsset1, "");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Take 001");
    
    mmUIViewModel_UpdateSceneUBO(p);

    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorMoved,
        &mmUIViewModel_OnEventCursorMoved,
        p);

    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorWheel,
        &mmUIViewModel_OnEventCursorWheel,
        p);

    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewModel_OnEventUpdate,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventSized,
        &mmUIViewModel_OnEventSized,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventRecordBasicsView,
        &mmUIViewModel_OnEventRecordBasicsView,
        p);
    
    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewModel_OnDiscard(
    struct mmUIViewModel*                          p)
{
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;

    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventRecordBasicsView,
        &mmUIViewModel_OnEventRecordBasicsView,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventSized,
        &mmUIViewModel_OnEventSized,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewModel_OnEventUpdate,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorWheel,
        &mmUIViewModel_OnEventCursorWheel,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorMoved,
        &mmUIViewModel_OnEventCursorMoved,
        p);

    mmVKTFObject_Discard(&p->vAsset1, pAssets);

    mmVKTFModelLoader_UnloadByPath(pModelLoader, mmString_CStr(&p->vModelPath));

    mmVKScene_Discard(&p->vScene, pAssets);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewModel_UpdateSceneUBO(
    struct mmUIViewModel*                          p)
{
    struct mmVKSwapchain* pSwapchain = p->view.context->swapchain;
    uint32_t* windowSize = pSwapchain->windowSize;

    float ProjectionView[4][4];
    float Projection[4][4];
    float View[4][4];

    float n = p->vScene.hCamera.n;
    float f = p->vScene.hCamera.f;
    float eye[3] = { 0.0f, 0.0f, 2.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0 };

    mmVec3MulK(eye, eye, p->l);

    float hAngleX = mmMathRadian(p->angle[0]);
    float hAngleY = mmMathRadian(p->angle[1]);
    float radius = mmVec3Length(eye);
    eye[0] = sinf(hAngleY) * cosf(hAngleX) * radius;
    eye[1] = cosf(hAngleY)                 * radius;
    eye[2] = sinf(hAngleY) * sinf(hAngleX) * radius;

    mmMat4x4LookAtRH(View, eye, origin, up);

    // z-fighting resolve need modify near to suitable.
    float aspect = (float)windowSize[0] / (float)windowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, n, f);

    mmMat4x4Mul(ProjectionView, Projection, View);

    mmUniformVec3Assign(p->vScene.uboSceneFragment.camera, eye);


    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKUploader* pUploader = pAssets->pUploader;

    mmUniformMat4Assign(p->vScene.uboSceneWholeVert.Projection, Projection);
    mmUniformMat4Assign(p->vScene.uboSceneWholeVert.View, View);
    mmMat4x4Mul(p->vScene.uboSceneWholeVert.ProjectionView, Projection, View);
    mmUniformVec3Assign(p->vScene.uboSceneFragment.camera, eye);

    mmVKScene_UpdateUBOBufferSceneWhole(&p->vScene, pUploader);
    mmVKScene_UpdateUBOBufferSceneFragment(&p->vScene, pUploader);
}

void
mmUIViewModel_OnEventCursorMoved(
    struct mmUIViewModel*                          p,
    struct mmEventCursorArgs*                      args)
{
    struct mmEventCursor* pCursor = args->content;

    int viewUpdated = 0;
    if ((1 << MM_MB_LBUTTON) & pCursor->button_mask)
    {
        p->angle[0] += (float)pCursor->rel_x;
        p->angle[1] -= (float)pCursor->rel_y;

        p->angle[0] = fmodf(p->angle[0], 360.0f);
        p->angle[1] = mmMathClamp(p->angle[1], 1e-5f, 180.0f - 1e-5f);

        viewUpdated = 1;
    }
    if ((1 << MM_MB_RBUTTON) & pCursor->button_mask)
    {
        viewUpdated = 1;
    }
    if ((1 << MM_MB_MBUTTON) & pCursor->button_mask)
    {
        viewUpdated = 1;
    }

    if (viewUpdated)
    {
        mmUIViewContext_DeviceWaitIdle(p->view.context);

        mmUIViewModel_UpdateSceneUBO(p);
    }
}

void
mmUIViewModel_OnEventCursorWheel(
    struct mmUIViewModel*                          p,
    struct mmEventCursorArgs*                      args)
{
    struct mmEventCursor* pCursor = args->content;

    p->l = p->l + p->l * (float)(pCursor->wheel_dy) * 0.1f;

    mmUIViewContext_DeviceWaitIdle(p->view.context);

    mmUIViewModel_UpdateSceneUBO(p);
}

void
mmUIViewModel_OnEventUpdate(
    struct mmUIViewModel*                          p,
    struct mmEventUpdateArgs*                      args)
{
    struct mmEventUpdate* pUpdate = args->content;
    
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKUploader* pUploader = pAssets->pUploader;
    
    float interval = (float)pUpdate->interval;
    mmVKTFObject_Update(&p->vAsset1, pUploader, interval);

    static float angle = 0.0f;
    float rfAngle = (float)(angle * MM_PI / 180.0);
    angle += 0.03f;
    mmQuatFromAngleAxis(p->vAsset1.node.quaternion, rfAngle, mmVec3PositiveUnitY);
    mmVKNode_MarkCacheInvalidWorldTransform(&p->vAsset1.node);

    struct mmVKUBOLight* lights;
    lights = (struct mmVKUBOLight*)p->vScene.uboSceneLights.arrays;
    static float time = 0.0f;
    time += interval * 0.1f;
    float hAngle = mmMathRadian(time * 360.0f);
    float radius = 5.0f;
    float lightPos[3];
    lightPos[0] = cosf(hAngle) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle) * radius;
    mmUniformVec3Assign(lights[1].position, lightPos);
    lightPos[0] = cosf(hAngle + (float)MM_PI_DIV_2) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle + (float)MM_PI_DIV_2) * radius;
    mmUniformVec3Assign(lights[2].position, lightPos);
    
    mmVKScene_UpdateUBODataLights(&p->vScene);
    mmVKScene_UpdateUBOBufferSceneLights(&p->vScene, pUploader);

    mmVKScene_UpdateTransforms(&p->vScene);
    mmVKScene_UpdateUBODataTransforms(&p->vScene);

    mmVKScene_UpdateUBOBufferShadow(&p->vScene, pUploader);
}

void
mmUIViewModel_OnEventSized(
    struct mmUIViewModel*                          p,
    struct mmEventUIViewArgs*                      args)
{
    //struct mmVKAssets* pAssets = p->view.context->assets;
    //struct mmVKUploader* pUploader = pAssets->pUploader;
    //struct mmVKSwapchain* pSwapchain = p->view.context->swapchain;
    //
    //uint32_t* hWindowSize = pSwapchain->windowSize;
    //
    //float aspect = (float)hWindowSize[0] / (float)hWindowSize[1];
    //float angle = (float)(45 * MM_PI / 180.0);
    //float eye[3] = { 1.8f, 2.0f, 5.0f };
    //float origin[3] = { 0, 0, 0 };
    //float up[3] = { 0.0f, 1.0f, 0.0f };
    //
    //mmMat4x4Ref ProjectionView = p->vScene.uboSceneWholeVert.ProjectionView;
    //mmMat4x4Ref Projection = p->vScene.uboSceneWholeVert.Projection;
    //mmMat4x4Ref View = p->vScene.uboSceneWholeVert.View;

    //mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, 0.01f, 1000.0f);
    //mmMat4x4LookAtRH(View, eye, origin, up);

    //mmMat4x4Mul(ProjectionView, Projection, View);
    //
    //mmVKScene_UpdateUBOBufferSceneWhole(&p->vScene, pUploader);

    mmUIViewContext_DeviceWaitIdle(p->view.context);

    mmUIViewModel_UpdateSceneUBO(p);
}

void
mmUIViewModel_OnEventRecordBasicsView(
    struct mmUIViewModel*                          p,
    struct mmEventUIViewCommandArgs*               args)
{
    VkCommandBuffer cmdBuffer = args->pCmd->cmdBuffer;

    mmVKScene_SetPipelineIndex(&p->vScene, mmVKPipelineWhole, 0, 0);
    mmVKTFObject_OnRecordCommand(cmdBuffer, &p->vScene, &p->vAsset1);
}
