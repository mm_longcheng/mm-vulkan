/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCDay_h__
#define __mmUIViewCDay_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmUIDataCalendar;
struct mmEventUIClickedArgs;

// Type base define.
mmUIViewTypeDefine(CDay);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCDay_EventSelected;

struct mmUIViewCDay
{
    struct mmUIView view;
    int vY, vM, vD;

    struct mmUIDataCalendar* pCalendar;
    /* UIView weak reference. */
    struct mmUIView* MyImage_0;

    struct mmUIView* MyImage_1;
    struct mmUIView* MyText_00;
    struct mmUIView* MyButton_00;
    struct mmUIView* MyText_01;

    struct mmUIView* MyText_0;
    struct mmUIView* MyText_1;
    struct mmUIView* MyText_2;
    struct mmUIView* MyText_3;
    struct mmUIView* MyText_4;
    struct mmUIView* MyText_5;
    struct mmUIView* MyText_6;
    struct mmUIView* MyText_7;
};

void
mmUIViewCDay_Init(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_Destroy(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_SetCalendar(
    struct mmUIViewCDay*                           p,
    struct mmUIDataCalendar*                       calendar);

void
mmUIViewCDay_OnPrepare(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_OnDiscard(
    struct mmUIViewCDay*                           p);

/* special */

void
mmUIViewCDay_ResetReference(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_UpdateContentTitle(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_UpdateContentDay(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_UpdateContentTime(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_UpdateContent(
    struct mmUIViewCDay*                           p);

void
mmUIViewCDay_UpdateTime(
    struct mmUIViewCDay*                           p,
    int                                            change);

void
mmUIViewCDay_OnEventTodayClicked(
    struct mmUIViewCDay*                           p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCDay_h__
