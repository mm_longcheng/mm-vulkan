/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewRoot_h__
#define __mmUIViewRoot_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"
#include "ui/mmUIRoot.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmEventUIViewContextArgs;

mmUIViewTypeDefine(Root);

struct mmUIViewRoot
{
    struct mmUIView view;
    struct mmUIRoot root;

    /* UIView weak reference. */
    struct mmUIView* Background;
    struct mmUIView* RootSafety;
    struct mmUIView* RootNotify;
};

void
mmUIViewRoot_Init(
    struct mmUIViewRoot*                           p);

void
mmUIViewRoot_Destroy(
    struct mmUIViewRoot*                           p);

void
mmUIViewRoot_OnPrepare(
    struct mmUIViewRoot*                           p);

void
mmUIViewRoot_OnDiscard(
    struct mmUIViewRoot*                           p);

/* special */

void
mmUIViewRoot_ResetReference(
    struct mmUIViewRoot*                           p);

void
mmUIViewRoot_OnEventSafeRectChanged(
    struct mmUIViewRoot*                           p,
    struct mmEventUIViewContextArgs*               args);

void
mmUIViewRoot_OnAspectSafeRect(
    struct mmUIViewRoot*                           p);

#include "core/mmSuffix.h"

#endif//__mmUIViewRoot_h__
