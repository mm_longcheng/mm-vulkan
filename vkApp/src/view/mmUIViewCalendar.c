/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCalendar.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"
#include "core/mmTimeUtils.h"

#include "nwsi/mmEventSurface.h"

#include "vk/mmVKAssets.h"

#include "view/mmUIViewCButton.h"
#include "view/mmUIViewCDay.h"
#include "view/mmUIViewCMonth.h"
#include "view/mmUIViewCYear.h"
#include "view/mmUIViewCEditBox.h"

// Type base implement.
mmUIViewTypeImplement(Calendar);

void
mmUIViewCalendar_Init(
    struct mmUIViewCalendar*                       p)
{
    mmUIView_Init(&p->view);
    mmUIDataCalendar_Init(&p->calendar);

    mmUIViewCalendar_ResetReference(p);

    mmMemset(p->Layer, 0, sizeof(p->Layer));
    mmMemset(p->Button, 0, sizeof(p->Button));

    p->hSelect = 1;
}

void
mmUIViewCalendar_Destroy(
    struct mmUIViewCalendar*                       p)
{
    p->hSelect = 1;

    mmMemset(p->Button, 0, sizeof(p->Button));
    mmMemset(p->Layer, 0, sizeof(p->Layer));

    mmUIViewCalendar_ResetReference(p);

    mmUIDataCalendar_Destroy(&p->calendar);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCalendar_OnPrepare(
    struct mmUIViewCalendar*                       p)
{
    struct mmUIViewContext* context;
    struct mmVKAssets* assets;
    struct mmFileContext* pFileContext;

    context = p->view.context;
    assets = context->assets;
    pFileContext = assets->pFileContext;

    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCalendar.view.json");

    p->MyDay = mmUIView_GetChildAssert(&p->view, "MyDay");
    p->MyMonth = mmUIView_GetChildAssert(&p->view, "MyMonth");
    p->MyYear = mmUIView_GetChildAssert(&p->view, "MyYear");


    p->MyImage_2 = mmUIView_GetChildAssert(&p->view, "MyImage_2");
    p->MyButton_20 = mmUIView_GetChildAssert(p->MyImage_2, "MyButton_20");
    p->MyButton_21 = mmUIView_GetChildAssert(p->MyImage_2, "MyButton_21");
    p->MyButton_22 = mmUIView_GetChildAssert(p->MyImage_2, "MyButton_22");

    p->MyView_3 = mmUIView_GetChildAssert(&p->view, "MyView_3");
    p->MyButton_01 = mmUIView_GetChildAssert(p->MyView_3, "MyButton_01");

    p->MyCEditBox_00 = mmUIView_GetChildAssert(&p->view, "MyCEditBox_00");

    p->Layer[0] = p->MyDay;
    p->Layer[1] = p->MyMonth;
    p->Layer[2] = p->MyYear;
    p->Button[0] = p->MyButton_20;
    p->Button[1] = p->MyButton_21;
    p->Button[2] = p->MyButton_22;

    mmUIDataCalendar_Prepare(&p->calendar, pFileContext);
    mmUIViewCEditBox_SetCalendar((struct mmUIViewCEditBox*)(p->MyCEditBox_00), &p->calendar);
    mmUIViewCDay_SetCalendar((struct mmUIViewCDay*)(p->MyDay), &p->calendar);
    mmUIViewCMonth_SetCalendar((struct mmUIViewCMonth*)(p->MyMonth), &p->calendar);
    mmUIViewCYear_SetCalendar((struct mmUIViewCYear*)(p->MyYear), &p->calendar);

    mmEventSet_SubscribeEvent(
        &p->MyButton_20->events,
        mmUIViewCButton_EventSelected,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_21->events,
        mmUIViewCButton_EventSelected,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_22->events,
        mmUIViewCButton_EventSelected,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCalendar_OnEventClicked01,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyCEditBox_00->events,
        mmUIViewCEditBox_EventDateAccepted,
        &mmUIViewCalendar_OnEventClicked000,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyCEditBox_00->events,
        mmUIViewCEditBox_EventClosed,
        &mmUIViewCalendar_OnEventClicked001,
        p);

    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewCalendar_OnEventUpdate,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyDay->events,
        mmUIViewCDay_EventSelected,
        &mmUIViewCalendar_OnEventSelected0,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyMonth->events,
        mmUIViewCMonth_EventSelected,
        &mmUIViewCalendar_OnEventSelected1,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyYear->events,
        mmUIViewCYear_EventSelected,
        &mmUIViewCalendar_OnEventSelected2,
        p);

    mmUIView_PrepareChildren(&p->view);

    mmUIViewCalendar_UpdateSelect(p);
}

void
mmUIViewCalendar_OnDiscard(
    struct mmUIViewCalendar*                       p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyYear->events,
        mmUIViewCYear_EventSelected,
        &mmUIViewCalendar_OnEventSelected2,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyMonth->events,
        mmUIViewCMonth_EventSelected,
        &mmUIViewCalendar_OnEventSelected1,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyDay->events,
        mmUIViewCDay_EventSelected,
        &mmUIViewCalendar_OnEventSelected0,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewCalendar_OnEventUpdate,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyCEditBox_00->events,
        mmUIViewCEditBox_EventClosed,
        &mmUIViewCalendar_OnEventClicked001,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyCEditBox_00->events,
        mmUIViewCEditBox_EventDateAccepted,
        &mmUIViewCalendar_OnEventClicked000,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCalendar_OnEventClicked01,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_22->events,
        mmUIView_EventClicked,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_21->events,
        mmUIView_EventClicked,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_20->events,
        mmUIView_EventClicked,
        &mmUIViewCalendar_OnEventClicked2x,
        p);

    mmUIDataCalendar_Discard(&p->calendar);

    mmMemset(p->Button, 0, sizeof(p->Button));
    mmMemset(p->Layer, 0, sizeof(p->Layer));

    mmUIViewCalendar_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCalendar_ResetReference(
    struct mmUIViewCalendar*                       p)
{
    p->MyDay = NULL;
    p->MyMonth = NULL;
    p->MyYear = NULL;

    p->MyImage_2 = NULL;
    p->MyButton_20 = NULL;
    p->MyButton_21 = NULL;
    p->MyButton_22 = NULL;

    p->MyView_3 = NULL;
    p->MyButton_01 = NULL;
    p->MyCEditBox_00 = NULL;
}

void
mmUIViewCalendar_UpdateSelect(
    struct mmUIViewCalendar*                       p)
{
    int i;
    struct mmUIViewCButton* u;
    for (i = 0; i < 3; i++)
    {
        u = (struct mmUIViewCButton*)p->Button[i];
        mmUIView_SetVisible(p->Layer[i], i == p->hSelect);
        mmUIViewCButton_UpdateSelect(u, i == p->hSelect);
    }
}

void
mmUIViewCalendar_UpdateTime(
    struct mmUIViewCalendar*                       p,
    struct mmEventUpdate*                          content)
{
    int change = mmUIDataCalendar_UpdateTime(&p->calendar, content->index);
    mmUIViewCDay_UpdateTime((struct mmUIViewCDay*)p->MyDay, change);
    mmUIViewCMonth_UpdateTime((struct mmUIViewCMonth*)p->MyMonth, change);
    mmUIViewCYear_UpdateTime((struct mmUIViewCYear*)p->MyYear, change);
}

void
mmUIViewCalendar_OnEventClicked2x(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    p->hSelect = args->pView->identity;
    mmUIViewCalendar_UpdateSelect(p);
}

void
mmUIViewCalendar_OnEventClicked000(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    struct mmUIViewCEditBox* u = (struct mmUIViewCEditBox*)(args->pView);

    mmUIView_SetVisible(p->MyCEditBox_00, 0);
    mmUIView_SetVisible(p->MyButton_01, 1);

    if (u->hValid)
    {
        struct mmUIDataCalendar* pCalendar;
        pCalendar = &p->calendar;

        mmTimeYMNormalize(u->hY, u->hM, &pCalendar->hY, &pCalendar->hM);
        pCalendar->sY = u->hY;
        pCalendar->sM = u->hM;
        pCalendar->sD = u->hD;
        mmUIDataCalendar_UpdateSQH(pCalendar);
        mmUIDataCalendar_UpdateLun(&p->calendar);
        mmUIDataCalendar_UpdateYearJQ(&p->calendar);

        mmUIViewCDay_UpdateContent((struct mmUIViewCDay*)p->MyDay);
        mmUIViewCMonth_UpdateContent((struct mmUIViewCMonth*)p->MyMonth);
        mmUIViewCYear_UpdateContent((struct mmUIViewCYear*)p->MyYear);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        // date is invalid.
        mmLogger_LogV(gLogger, "%s %d"
            " date is invalid.", __FUNCTION__, __LINE__);
    }
}

void
mmUIViewCalendar_OnEventClicked001(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIView_SetVisible(p->MyCEditBox_00, 0);
    mmUIView_SetVisible(p->MyButton_01, 1);
}

void
mmUIViewCalendar_OnEventClicked01(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIView_SetVisible(p->MyCEditBox_00, 1);
    mmUIView_SetVisible(p->MyButton_01, 0);
}

void
mmUIViewCalendar_OnEventUpdate(
    struct mmUIViewCalendar*                       p,
    struct mmEventUpdateArgs*                      args)
{
    mmUIViewCalendar_UpdateTime(p, args->content);
}

void
mmUIViewCalendar_OnEventSelected0(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIViewCMonth_UpdateContent((struct mmUIViewCMonth*)(p->MyMonth));
    mmUIViewCYear_UpdateContent((struct mmUIViewCYear*)(p->MyYear));
}

void
mmUIViewCalendar_OnEventSelected1(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIViewCDay_UpdateContent((struct mmUIViewCDay*)(p->MyDay));
    mmUIViewCYear_UpdateContent((struct mmUIViewCYear*)(p->MyYear));
}

void
mmUIViewCalendar_OnEventSelected2(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIViewCDay_UpdateContent((struct mmUIViewCDay*)(p->MyDay));
    mmUIViewCMonth_UpdateContent((struct mmUIViewCMonth*)(p->MyMonth));
}
