/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCEditBox_h__
#define __mmUIViewCEditBox_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmUIDataCalendar;

struct mmEventUIViewArgs;

// Type base define.
mmUIViewTypeDefine(CEditBox);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCEditBox_EventDateAccepted;
extern const char* mmUIViewCEditBox_EventClosed;

struct mmUIViewCEditBox
{
    struct mmUIView view;

    struct mmUIDataCalendar* pCalendar;
    /* UIView weak reference. */
    struct mmUIView* MyImage_0;
    struct mmUIView* MyImage_00;
    struct mmUIView* MyImage_01;
    struct mmUIView* MyImage_02;
    struct mmUIView* MyEditBox_000;
    struct mmUIView* MyEditBox_001;
    struct mmUIView* MyEditBox_002;
    struct mmUIView* MyText_000;
    struct mmUIView* MyText_001;
    struct mmUIView* MyText_002;
    struct mmUIView* MyButton_00;
    struct mmUIView* MyText_01;

    struct mmUIView* MyButton_01;

    int hY, hM, hD;
    int hValid;
};

void
mmUIViewCEditBox_Init(
    struct mmUIViewCEditBox*                       p);

void
mmUIViewCEditBox_Destroy(
    struct mmUIViewCEditBox*                       p);

void
mmUIViewCEditBox_SetCalendar(
    struct mmUIViewCEditBox*                       p,
    struct mmUIDataCalendar*                       calendar);

void
mmUIViewCEditBox_OnPrepare(
    struct mmUIViewCEditBox*                       p);

void
mmUIViewCEditBox_OnDiscard(
    struct mmUIViewCEditBox*                       p);

/* special */

void
mmUIViewCEditBox_ResetReference(
    struct mmUIViewCEditBox*                       p);

void
mmUIViewCEditBox_UpdateContent(
    struct mmUIViewCEditBox*                       p);

void
mmUIViewCEditBox_OnEventTextChanged(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCEditBox_OnEventClicked00(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCEditBox_OnEventClicked01(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCEditBox_h__
