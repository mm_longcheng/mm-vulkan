/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCButton_h__
#define __mmUIViewCButton_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmUICButton
{
    struct mmString Text;
    struct mmString FrameColor;
    struct mmString BackgroundColor;
};

void
mmUICButton_Init(
    struct mmUICButton*                            p);

void
mmUICButton_Destroy(
    struct mmUICButton*                            p);

mmUIViewTypeDefine(CButton);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCButton_EventSelected;

struct mmUIViewCButton
{
    struct mmUIView view;
    struct mmUICButton button;
    /* UIView weak reference. */
    struct mmUIView* MyButton_0;
    struct mmUIView* MyText_0;
    struct mmUIView* MyImage_0;
};

void
mmUIViewCButton_Init(
    struct mmUIViewCButton*                        p);

void
mmUIViewCButton_Destroy(
    struct mmUIViewCButton*                        p);

void
mmUIViewCButton_OnPrepare(
    struct mmUIViewCButton*                        p);

void
mmUIViewCButton_OnDiscard(
    struct mmUIViewCButton*                        p);

/* special */

void
mmUIViewCButton_ResetReference(
    struct mmUIViewCButton*                        p);

void
mmUIViewCButton_UpdateSelect(
    struct mmUIViewCButton*                        p,
    int                                            hValue);

void
mmUIViewCButton_OnEventClicked(
    struct mmUIViewCButton*                        p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCButton_h__
