/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCYear_h__
#define __mmUIViewCYear_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmUIDataCalendar;
struct mmEventUIClickedArgs;
struct mmEventUIViewArgs;

// Type base define.
mmUIViewTypeDefine(CYear);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCYear_EventSelected;

struct mmUIViewCYear
{
    struct mmUIView view;
    int vY, vM, vD;

    struct mmUIDataCalendar* pCalendar;

    /* UIView weak reference. */
    struct mmUIView* MyImage_0;

    struct mmUIView* MyImage_1;
    struct mmUIView* MyText_00;
    struct mmUIView* MyButton_00;
    struct mmUIView* MyText_01;

    struct mmUIView* MyImage_2;

    struct mmUIView* Term[12][2];

    struct mmUIView* pSelect;
};

void
mmUIViewCYear_Init(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_Destroy(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_SetCalendar(
    struct mmUIViewCYear*                          p,
    struct mmUIDataCalendar*                       calendar);

void
mmUIViewCYear_OnPrepare(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_OnDiscard(
    struct mmUIViewCYear*                          p);

/* special */

void
mmUIViewCYear_ResetReference(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_PrepareDays(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_DiscardDays(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_UpdateContentTitle(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_UpdateContentTerm(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_UpdateSelectTerm(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_UpdateContent(
    struct mmUIViewCYear*                          p);

void
mmUIViewCYear_UpdateTime(
    struct mmUIViewCYear*                          p,
    int                                            change);

void
mmUIViewCYear_OnEventSelected(
    struct mmUIViewCYear*                          p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCYear_OnEventTodayClicked(
    struct mmUIViewCYear*                          p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCYear_h__
