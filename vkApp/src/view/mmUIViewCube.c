/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCube.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "core/mmKeyCode.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKSwapchain.h"
#include "vk/mmVKCommandBuffer.h"

#include "nwsi/mmEventSurface.h"

// http://www.songho.ca/opengl/gl_sphere.html

// Type base implement.
mmUIViewTypeImplement(Cube);

void
mmUIViewCube_Init(
    struct mmUIViewCube*                           p)
{
    mmUIView_Init(&p->view);
    
    p->pImageInfo = NULL;
    
    mmNoise_Init(&p->vNoise);
    mmNoise3DImage_Init(&p->vNoise3DImage);
    
    mmVKTexel_Init(&p->vTexel);
    p->pSamplerInfo = NULL;
    
    mmNoiseIcoSphere_Init(&p->vNoiseIcoSphere);
    mmIcoSphere_Init(&p->vIcoSphere);
    mmUVSphere_Init(&p->vSphereEarth);
    mmUVSphere_Init(&p->vSphereCloud);
    
    mmMemset(&p->vUBOSceneVS, 0, sizeof(struct mmUICubeUBOVS));
    mmMemset(&p->vUBOSceneFS, 0, sizeof(struct mmUICubeUBOFS));
    mmVKBuffer_Init(&p->vBufferUBOVS);
    mmVKBuffer_Init(&p->vBufferUBOFS);
    
    mmMemset(&p->vConstant0, 0, sizeof(struct mmUICubePushConstant));
    mmMemset(&p->vConstant1, 0, sizeof(struct mmUICubePushConstant));
    
    mmVKDrawable_Init(&p->vDrawable0);
    mmVKDrawable_Init(&p->vDrawable1);
    
    p->l = 1.0f;
    mmVec2Make(p->angle, 0.0f, 45.0f);
    
    // default value.
    mmMat4x4MakeIdentity(p->vConstant0.Normal);
    mmMat4x4MakeIdentity(p->vConstant0.Model);
    
    mmMat4x4MakeIdentity(p->vConstant1.Normal);
    mmMat4x4MakeIdentity(p->vConstant1.Model);

    p->l = 2.56780481f;
}

void
mmUIViewCube_Destroy(
    struct mmUIViewCube*                           p)
{
    mmVec2Make(p->angle, 0.0f, 45.0f);
    p->l = 1.0f;
    
    mmVKDrawable_Destroy(&p->vDrawable1);
    mmVKDrawable_Destroy(&p->vDrawable0);
    
    mmMemset(&p->vConstant1, 0, sizeof(struct mmUICubePushConstant));
    mmMemset(&p->vConstant0, 0, sizeof(struct mmUICubePushConstant));
    
    mmVKBuffer_Destroy(&p->vBufferUBOFS);
    mmVKBuffer_Destroy(&p->vBufferUBOVS);
    mmMemset(&p->vUBOSceneFS, 0, sizeof(struct mmUICubeUBOFS));
    mmMemset(&p->vUBOSceneVS, 0, sizeof(struct mmUICubeUBOVS));
    
    mmUVSphere_Destroy(&p->vSphereCloud);
    mmUVSphere_Destroy(&p->vSphereEarth);
    mmIcoSphere_Destroy(&p->vIcoSphere);
    mmNoiseIcoSphere_Destroy(&p->vNoiseIcoSphere);
    
    p->pSamplerInfo = NULL;
    mmVKTexel_Destroy(&p->vTexel);
    
    mmNoise3DImage_Destroy(&p->vNoise3DImage);
    mmNoise_Destroy(&p->vNoise);
    
    p->pImageInfo = NULL;
    
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCube_UpdateSize(
    struct mmUIViewCube*                           p)
{
    mmUIViewContext_DeviceWaitIdle(p->view.context);

    mmUIViewCube_UpdateSceneUBO(p);
    mmUIViewCube_UpdateSceneVS(p);
}

void
mmUIViewCube_OnPrepare(
    struct mmUIViewCube*                           p)
{
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKUploader* pUploader = pAssets->pUploader;
    struct mmVKSamplerPool* pSamplerPool = &pAssets->vSamplerPool;
    struct mmVKImageLoader* pImageLoader = &pAssets->vImageLoader;
    
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCube.view.json");
    
    mmVKAssets_LoadPipelineFromName(pAssets, "mmUICube");
    mmVKAssets_LoadPipelineFromName(pAssets, "mmUISphere");

    // VK_FORMAT_R16G16_UNORM
    mmNoise3DImage_Prepare(&p->vNoise3DImage, &p->vNoise, 8, 32);
    
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
//        p->pImageInfo =
//        mmVKImageLoader_LoadFromFile(
//            pImageLoader,
//            "images/earth2048.jpg");
        
//        p->pImageInfo =
//        mmVKImageLoader_LoadFromFile(
//            pImageLoader,
//            "images/icosa_earth.jpg");
        
//        p->pImageInfo =
//        mmVKImageLoader_LoadFromFile(
//            pImageLoader,
//            "images/icosa_earth.bmp");
        
//        p->pImageInfo =
//        mmVKImageLoader_LoadFromFile(
//            pImageLoader,
//            "images/icosahedron_template2.bmp");
        
        p->pImageInfo =
        mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/earth.1024x512.jpg");
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);
        
    } while(0);
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmUIViewCube_UpdateSceneUBO(p);
        
        err = mmVKUploader_UploadCPU2GPUBuffer(
            pUploader,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            0,
            (uint8_t*)&p->vUBOSceneVS,
            (size_t)sizeof(struct mmUICubeUBOVS),
            &p->vBufferUBOVS);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKUploader_UploadCPU2GPUBuffer(
            pUploader,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            0,
            (uint8_t*)&p->vUBOSceneFS,
            (size_t)sizeof(struct mmUICubeUBOFS),
            &p->vBufferUBOFS);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while(0);
    
    p->vNoiseIcoSphere.pNoise3DImage = &p->vNoise3DImage;
    p->vNoiseIcoSphere.height = 0.045f;
    p->vNoiseIcoSphere.offset = 0.09f;
    p->vNoiseIcoSphere.detail = 1;
    p->vNoiseIcoSphere.subdivision = 3;
    p->vNoiseIcoSphere.radius = 1.0f;
    mmNoiseIcoSphere_BuildVerticesSmooth(&p->vNoiseIcoSphere);
    
    p->vIcoSphere.subdivision = 3;
    p->vIcoSphere.radius = 1.0f;
    mmIcoSphere_BuildVerticesSmooth(&p->vIcoSphere);
    
    // p->vSphere.sectorCount = 4;
    // p->vSphere.stackCount = 3;
    p->vSphereEarth.radius = 1.0f;
    mmUVSphere_BuildVerticesSmooth(&p->vSphereEarth);
    
    // p->vSphere.sectorCount = 4;
    // p->vSphere.stackCount = 3;
    p->vSphereCloud.radius = 1.01f;
    mmUVSphere_BuildVerticesSmooth(&p->vSphereCloud);
    
    struct mmVKDrawableCreateInfo hDrawableEarth;
    struct mmVKDrawableCreateInfo hDrawableCloud;
    
//    hDrawableEarth.hIdxType = VK_INDEX_TYPE_UINT32;
//    hDrawableEarth.hIdxCount = (uint32_t)mmNoiseIcoSphere_GetIdxCount(&p->vNoiseIcoSphere);
//    hDrawableEarth.hVtxCount = (uint32_t)mmNoiseIcoSphere_GetVtxCount(&p->vNoiseIcoSphere);
//    hDrawableEarth.hIdxBufferSize = mmNoiseIcoSphere_GetIdxBufferSize(&p->vNoiseIcoSphere);
//    hDrawableEarth.hVtxBufferSize = mmNoiseIcoSphere_GetVtxBufferSize(&p->vNoiseIcoSphere);
//    hDrawableEarth.hCstBufferSize = sizeof(struct mmUICubePushConstant);
//    hDrawableEarth.pIdxBuffer = mmNoiseIcoSphere_GetIdxBuffer(&p->vNoiseIcoSphere);
//    hDrawableEarth.pVtxBuffer = mmNoiseIcoSphere_GetVtxBuffer(&p->vNoiseIcoSphere);
//    hDrawableEarth.pCstBuffer = (const uint8_t*)&p->vConstant0;
//    hDrawableEarth.pPipelineName = "mmUISphere";
//    hDrawableEarth.pPoolName = "mmUISphere";
    
//    hDrawableEarth.hIdxType = VK_INDEX_TYPE_UINT32;
//    hDrawableEarth.hIdxCount = (uint32_t)mmIcoSphere_GetIdxCount(&p->vIcoSphere);
//    hDrawableEarth.hVtxCount = (uint32_t)mmIcoSphere_GetVtxCount(&p->vIcoSphere);
//    hDrawableEarth.hIdxBufferSize = mmIcoSphere_GetIdxBufferSize(&p->vIcoSphere);
//    hDrawableEarth.hVtxBufferSize = mmIcoSphere_GetVtxBufferSize(&p->vIcoSphere);
//    hDrawableEarth.hCstBufferSize = sizeof(struct mmUICubePushConstant);
//    hDrawableEarth.pIdxBuffer = mmIcoSphere_GetIdxBuffer(&p->vIcoSphere);
//    hDrawableEarth.pVtxBuffer = mmIcoSphere_GetVtxBuffer(&p->vIcoSphere);
//    hDrawableEarth.pCstBuffer = (const uint8_t*)&p->vConstant0;
//    hDrawableEarth.pPipelineName = "mmUISphere";
//    hDrawableEarth.pPoolName = "mmUISphere";
    
    hDrawableEarth.hIdxType = VK_INDEX_TYPE_UINT32;
    hDrawableEarth.hIdxCount = (uint32_t)mmUVSphere_GetIdxCount(&p->vSphereEarth);
    hDrawableEarth.hVtxCount = (uint32_t)mmUVSphere_GetVtxCount(&p->vSphereEarth);
    hDrawableEarth.hIdxBufferSize = mmUVSphere_GetIdxBufferSize(&p->vSphereEarth);
    hDrawableEarth.hVtxBufferSize = mmUVSphere_GetVtxBufferSize(&p->vSphereEarth);
    hDrawableEarth.hCstBufferSize = sizeof(struct mmUICubePushConstant);
    hDrawableEarth.pIdxBuffer = mmUVSphere_GetIdxBuffer(&p->vSphereEarth);
    hDrawableEarth.pVtxBuffer = mmUVSphere_GetVtxBuffer(&p->vSphereEarth);
    hDrawableEarth.pCstBuffer = (const uint8_t*)&p->vConstant0;
    hDrawableEarth.pPipelineName = "mmUISphere";
    hDrawableEarth.pPoolName = "mmUISphere";
    mmVKDrawable_Prepare(&p->vDrawable0, pAssets, &hDrawableEarth);
    
    hDrawableCloud.hIdxType = VK_INDEX_TYPE_UINT32;
    hDrawableCloud.hIdxCount = (uint32_t)mmUVSphere_GetIdxCount(&p->vSphereCloud);
    hDrawableCloud.hVtxCount = (uint32_t)mmUVSphere_GetVtxCount(&p->vSphereCloud);
    hDrawableCloud.hIdxBufferSize = mmUVSphere_GetIdxBufferSize(&p->vSphereCloud);
    hDrawableCloud.hVtxBufferSize = mmUVSphere_GetVtxBufferSize(&p->vSphereCloud);
    hDrawableCloud.hCstBufferSize = sizeof(struct mmUICubePushConstant);
    hDrawableCloud.pIdxBuffer = mmUVSphere_GetIdxBuffer(&p->vSphereCloud);
    hDrawableCloud.pVtxBuffer = mmUVSphere_GetVtxBuffer(&p->vSphereCloud);
    hDrawableCloud.pCstBuffer = (const uint8_t*)&p->vConstant1;
    hDrawableCloud.pPipelineName = "mmUICube";
    hDrawableCloud.pPoolName = "mmUICube";
    mmVKDrawable_Prepare(&p->vDrawable1, pAssets, &hDrawableCloud);
    
    struct mmVKTexelCreateInfo hTexelInfo;
    
    hTexelInfo.format = VK_FORMAT_R16G16_UNORM;
    hTexelInfo.imageType = VK_IMAGE_TYPE_3D;
    hTexelInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
    hTexelInfo.extent[0] = p->vNoise3DImage.width;
    hTexelInfo.extent[1] = p->vNoise3DImage.width;
    hTexelInfo.extent[2] = p->vNoise3DImage.width;
    hTexelInfo.mipLevels = 1;
    hTexelInfo.arrayLayers = 1;
    hTexelInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    hTexelInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    hTexelInfo.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    hTexelInfo.vmaFlags = 0;
    hTexelInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;
    mmVKTexel_Prepare(
        &p->vTexel,
        pUploader,
        (const uint8_t*)p->vNoise3DImage.buffer,
        (size_t)p->vNoise3DImage.length,
        &hTexelInfo);
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        p->pSamplerInfo = mmVKSamplerPool_Add(pSamplerPool, &mmVKSamplerLinearRepeat);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);
    } while(0);
    
    mmUIViewCube_DescriptorSetUpdate0(p);
    mmUIViewCube_DescriptorSetUpdate1(p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorMoved,
        &mmUIViewCube_OnEventCursorMoved,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorWheel,
        &mmUIViewCube_OnEventCursorWheel,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewCube_OnEventUpdate,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventSized,
        &mmUIViewCube_OnEventSized,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventRecordBasicsView,
        &mmUIViewCube_OnEventRecordBasicsView,
        p);
    
    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewCube_OnDiscard(
    struct mmUIViewCube*                           p)
{
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmVKUploader* pUploader = pAssets->pUploader;
    
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventRecordBasicsView,
        &mmUIViewCube_OnEventRecordBasicsView,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventSized,
        &mmUIViewCube_OnEventSized,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewCube_OnEventUpdate,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorWheel,
        &mmUIViewCube_OnEventCursorWheel,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventCursorMoved,
        &mmUIViewCube_OnEventCursorMoved,
        p);
    
    mmVKSamplerInfo_Decrease(p->pSamplerInfo);
    p->pSamplerInfo = NULL;
    
    mmVKTexel_Discard(&p->vTexel, pUploader);
    
    mmVKDrawable_Discard(&p->vDrawable1, pAssets);
    mmVKDrawable_Discard(&p->vDrawable0, pAssets);
    
    mmVKUploader_DeleteBuffer(pUploader, &p->vBufferUBOFS);
    mmVKUploader_DeleteBuffer(pUploader, &p->vBufferUBOVS);
    
    mmNoise3DImage_Discard(&p->vNoise3DImage);
    
    mmVKImageInfo_Decrease(p->pImageInfo);
    p->pImageInfo = NULL;
    
    mmVKAssets_UnloadPipelineByName(pAssets, "mmUISphere");
    mmVKAssets_UnloadPipelineByName(pAssets, "mmUICube");

    mmUIView_DiscardView(&p->view);
}

VkResult
mmUIViewCube_DescriptorSetUpdate0(
    struct mmUIViewCube*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDescriptorImageInfo hDescriptorImageInfo[1];
        VkWriteDescriptorSet writes[3];

        VkDescriptorBufferInfo hDescriptorBufferInfo[2];

        struct mmUIViewContext* pViewContext = p->view.context;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;

        VkDevice device = VK_NULL_HANDLE;
        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDrawable0.vDescriptorSet))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == p->vBufferUBOVS.buffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " ubo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        descriptorSet = p->vDrawable0.vDescriptorSet.descriptorSet;

        hDescriptorBufferInfo[0].buffer = p->vBufferUBOVS.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmUICubeUBOVS);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        hDescriptorImageInfo[0].sampler = p->pSamplerInfo->sampler;
        hDescriptorImageInfo[0].imageView = p->pImageInfo->imageView;
        hDescriptorImageInfo[0].imageLayout = p->pImageInfo->vImage.imageLayout;
        
        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 1;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[1].pImageInfo = &hDescriptorImageInfo[0];
        writes[1].pBufferInfo = NULL;
        writes[1].pTexelBufferView = NULL;
        
        hDescriptorBufferInfo[1].buffer = p->vBufferUBOFS.buffer;
        hDescriptorBufferInfo[1].offset = 0;
        hDescriptorBufferInfo[1].range = sizeof(struct mmUICubeUBOFS);
        
        writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[2].pNext = NULL;
        writes[2].dstSet = descriptorSet;
        writes[2].dstBinding = 2;
        writes[2].dstArrayElement = 0;
        writes[2].descriptorCount = 1;
        writes[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[2].pImageInfo = NULL;
        writes[2].pBufferInfo = &hDescriptorBufferInfo[1];
        writes[2].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 3, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmUIViewCube_DescriptorSetUpdate1(
    struct mmUIViewCube*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDescriptorImageInfo hDescriptorImageInfo[1];
        VkWriteDescriptorSet writes[3];

        VkDescriptorBufferInfo hDescriptorBufferInfo[2];

        struct mmUIViewContext* pViewContext = p->view.context;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;

        VkDevice device = VK_NULL_HANDLE;
        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDrawable1.vDescriptorSet))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == p->vBufferUBOVS.buffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " ubo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKTexel_Invalid(&p->vTexel))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vTexel is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        descriptorSet = p->vDrawable1.vDescriptorSet.descriptorSet;

        hDescriptorBufferInfo[0].buffer = p->vBufferUBOVS.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmUICubeUBOVS);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        hDescriptorImageInfo[0].sampler = p->pSamplerInfo->sampler;
        hDescriptorImageInfo[0].imageView = p->vTexel.imageView;
        hDescriptorImageInfo[0].imageLayout = p->vTexel.vImage.imageLayout;
        
        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 1;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[1].pImageInfo = &hDescriptorImageInfo[0];
        writes[1].pBufferInfo = NULL;
        writes[1].pTexelBufferView = NULL;
        
        hDescriptorBufferInfo[1].buffer = p->vBufferUBOFS.buffer;
        hDescriptorBufferInfo[1].offset = 0;
        hDescriptorBufferInfo[1].range = sizeof(struct mmUICubeUBOFS);
        
        writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[2].pNext = NULL;
        writes[2].dstSet = descriptorSet;
        writes[2].dstBinding = 2;
        writes[2].dstArrayElement = 0;
        writes[2].descriptorCount = 1;
        writes[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[2].pImageInfo = NULL;
        writes[2].pBufferInfo = &hDescriptorBufferInfo[1];
        writes[2].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 3, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmUIViewCube_UpdateSceneUBO(
    struct mmUIViewCube*                           p)
{
    struct mmVKSwapchain* pSwapchain = p->view.context->swapchain;
    uint32_t* windowSize = pSwapchain->windowSize;

    mmMat4x4Ref ProjectionView = p->vUBOSceneVS.ProjectionView;
    float Projection[4][4];
    float View[4][4];

    float n = 0.1f;
    float f = 20.0f;
    float eye[3] = { 0.0f, 0.0f, 2.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0 };

    mmVec3MulK(eye, eye, p->l);

    float hAngleX = mmMathRadian(p->angle[0]);
    float hAngleY = mmMathRadian(p->angle[1]);
    float radius = mmVec3Length(eye);
    eye[0] = sinf(hAngleY) * cosf(hAngleX) * radius;
    eye[1] = cosf(hAngleY)                 * radius;
    eye[2] = sinf(hAngleY) * sinf(hAngleX) * radius;

    mmMat4x4LookAtRH(View, eye, origin, up);

    // z-fighting resolve need modify near to suitable.
    float aspect = (float)windowSize[0] / (float)windowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, n, f);
    
    mmMat4x4Mul(ProjectionView, Projection, View);
    
    mmUniformVec3Assign(p->vUBOSceneFS.camera, eye);
}

void
mmUIViewCube_UpdateSceneVS(
    struct mmUIViewCube*                           p)
{    
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmVKAssets* pAssets = p->view.context->assets;
        struct mmVKUploader* pUploader = pAssets->pUploader;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (VK_NULL_HANDLE == p->vBufferUBOVS.buffer)
        {
            // buffer is not ready, not need update size.
            err = VK_SUCCESS;
            break;
        }
        
        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->vUBOSceneVS,
            (size_t)0,
            (size_t)sizeof(struct mmUICubeUBOVS),
            &p->vBufferUBOVS,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while(0);
}

void
mmUIViewCube_UpdateSceneFS(
    struct mmUIViewCube*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmVKAssets* pAssets = p->view.context->assets;
        struct mmVKUploader* pUploader = pAssets->pUploader;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (VK_NULL_HANDLE == p->vBufferUBOFS.buffer)
        {
            // buffer is not ready, not need update size.
            err = VK_SUCCESS;
            break;
        }
        
        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->vUBOSceneFS,
            (size_t)0,
            (size_t)sizeof(struct mmUICubeUBOFS),
            &p->vBufferUBOFS,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while(0);
}

void
mmUIViewCube_OnEventCursorMoved(
    struct mmUIViewCube*                           p,
    struct mmEventCursorArgs*                      args)
{
    struct mmEventCursor* pCursor = args->content;
    
    int viewUpdated = 0;
    if ((1 << MM_MB_LBUTTON) & pCursor->button_mask)
    {
        p->angle[0] += (float)pCursor->rel_x;
        p->angle[1] -= (float)pCursor->rel_y;

        p->angle[0] = fmodf(p->angle[0], 360.0f);
        p->angle[1] = mmMathClamp(p->angle[1], 1e-5f, 180.0f - 1e-5f);

        viewUpdated = 1;
    }
    if ((1 << MM_MB_RBUTTON) & pCursor->button_mask)
    {
        viewUpdated = 1;
    }
    if ((1 << MM_MB_MBUTTON) & pCursor->button_mask)
    {
        viewUpdated = 1;
    }

    if (viewUpdated)
    {
        mmUIViewContext_DeviceWaitIdle(p->view.context);

        mmUIViewCube_UpdateSceneUBO(p);

        mmUIViewCube_UpdateSceneVS(p);
        mmUIViewCube_UpdateSceneFS(p);
    }
}

void
mmUIViewCube_OnEventCursorWheel(
    struct mmUIViewCube*                           p,
    struct mmEventCursorArgs*                      args)
{
    struct mmEventCursor* pCursor = args->content;
    
    p->l = p->l + p->l * (float)(pCursor->wheel_dy) * 0.1f;

    mmUIViewContext_DeviceWaitIdle(p->view.context);

    mmUIViewCube_UpdateSceneUBO(p);

    mmUIViewCube_UpdateSceneVS(p);
    mmUIViewCube_UpdateSceneFS(p);
}

void
mmUIViewCube_OnEventUpdate(
    struct mmUIViewCube*                           p,
    struct mmEventUpdateArgs*                      args)
{
    struct mmEventUpdate* pUpdate = args->content;
    
    p->vUBOSceneFS.scale = 0.195f;
    p->vUBOSceneFS.time += (float)pUpdate->interval;
    mmUIViewCube_UpdateSceneFS(p);
}

void
mmUIViewCube_OnEventSized(
    struct mmUIViewCube*                           p,
    struct mmEventUIViewArgs*                      args)
{
    mmUIViewCube_UpdateSize(p);
}

void
mmUIViewCube_OnEventRecordBasicsView(
    struct mmUIViewCube*                           p,
    struct mmEventUIViewCommandArgs*               args)
{
    VkCommandBuffer cmdBuffer = args->pCmd->cmdBuffer;
    
    mmVKDrawable_OnRecord(&p->vDrawable0, cmdBuffer);
    mmVKDrawable_OnRecord(&p->vDrawable1, cmdBuffer);
}
