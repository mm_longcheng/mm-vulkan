/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCEditBox.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"
#include "ui/mmUIViewEditBox.h"

#include "data/mmUIDataCalendar.h"

#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"
#include "core/mmText.h"
#include "core/mmTimeUtils.h"

int
mmCStringIsNumber(
    const char* s,
    size_t l)
{
    size_t i;
    for (i = 0; i < l; ++i)
    {
        if (!mmTextIsNumber(s[i]))
        {
            return MM_FALSE;
        }
    }
    return MM_TRUE;
}

int
mmString_IsNumber(
    const struct mmString* p)
{
    size_t i;
    size_t l;
    const char* s;

    s = mmString_Data(p);
    l = mmString_Size(p);
    mmCStringRemoveBothSidesSpace(s, l, &i, &l);
    s += i;

    for (i = 0; i < l; ++i)
    {
        if (!mmTextIsNumber(s[i]))
        {
            return MM_FALSE;
        }
    }
    return MM_TRUE;
}

// Type base implement.
mmUIViewTypeImplement(CEditBox);

const char* mmUIViewCEditBox_EventDateAccepted = "EventDateAccepted";
const char* mmUIViewCEditBox_EventClosed = "EventClosed";

void
mmUIViewCEditBox_Init(
    struct mmUIViewCEditBox*                       p)
{
    mmUIView_Init(&p->view);

    mmUIViewCEditBox_ResetReference(p);

    p->hY = 0; p->hM = 0; p->hD = 0;
    p->hValid = 0;
}

void
mmUIViewCEditBox_Destroy(
    struct mmUIViewCEditBox*                       p)
{
    p->hValid = 0;
    p->hY = 0; p->hM = 0; p->hD = 0;

    mmUIViewCEditBox_ResetReference(p);

    mmUIView_Destroy(&p->view);
}

void
mmUIViewCEditBox_SetCalendar(
    struct mmUIViewCEditBox*                       p,
    struct mmUIDataCalendar*                       calendar)
{
    p->pCalendar = calendar;
}

void
mmUIViewCEditBox_OnPrepare(
    struct mmUIViewCEditBox*                       p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCEditBox.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");
    p->MyImage_00 = mmUIView_GetChildAssert(p->MyImage_0, "MyImage_00");
    p->MyImage_01 = mmUIView_GetChildAssert(p->MyImage_0, "MyImage_01");
    p->MyImage_02 = mmUIView_GetChildAssert(p->MyImage_0, "MyImage_02");
    p->MyEditBox_000 = mmUIView_GetChildAssert(p->MyImage_0, "MyEditBox_000");
    p->MyEditBox_001 = mmUIView_GetChildAssert(p->MyImage_0, "MyEditBox_001");
    p->MyEditBox_002 = mmUIView_GetChildAssert(p->MyImage_0, "MyEditBox_002");
    p->MyText_000 = mmUIView_GetChildAssert(p->MyImage_0, "MyText_000");
    p->MyText_001 = mmUIView_GetChildAssert(p->MyImage_0, "MyText_001");
    p->MyText_002 = mmUIView_GetChildAssert(p->MyImage_0, "MyText_002");
    p->MyButton_00 = mmUIView_GetChildAssert(p->MyImage_0, "MyButton_00");
    p->MyText_01 = mmUIView_GetChildAssert(p->MyImage_0, "MyText_01");

    p->MyButton_01 = mmUIView_GetChildAssert(&p->view, "MyButton_01");

    mmEventSet_SubscribeEvent(
        &p->MyEditBox_000->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyEditBox_001->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyEditBox_002->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCEditBox_OnEventClicked00,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCEditBox_OnEventClicked01,
        p);

    mmUIView_PrepareChildren(&p->view);

    mmUIViewCEditBox_UpdateContent(p);
}

void
mmUIViewCEditBox_OnDiscard(
    struct mmUIViewCEditBox*                       p)
{
    mmUIView_DiscardChildren(&p->view);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCEditBox_OnEventClicked01,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCEditBox_OnEventClicked00,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyEditBox_002->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyEditBox_001->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyEditBox_000->events,
        mmUIViewEditBox_EventTextChanged,
        &mmUIViewCEditBox_OnEventTextChanged,
        p);

    mmUIViewCEditBox_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCEditBox_ResetReference(
    struct mmUIViewCEditBox*                       p)
{
    p->MyImage_0 = NULL;
    p->MyImage_00 = NULL;
    p->MyImage_01 = NULL;
    p->MyImage_02 = NULL;
    p->MyEditBox_000 = NULL;
    p->MyEditBox_001 = NULL;
    p->MyEditBox_002 = NULL;
    p->MyText_000 = NULL;
    p->MyText_001 = NULL;
    p->MyText_002 = NULL;
    p->MyButton_00 = NULL;
    p->MyText_01 = NULL;

    p->MyButton_01 = NULL;
}

void
mmUIViewCEditBox_UpdateContent(
    struct mmUIViewCEditBox*                       p)
{
    char s[32];
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    sprintf(s, "%d", pCalendar->tY);
    mmUIView_SetValueCStr(p->MyEditBox_000, "Text.String", s);
    mmUIView_Changed(p->MyEditBox_000);

    sprintf(s, "%02d", pCalendar->tM);
    mmUIView_SetValueCStr(p->MyEditBox_001, "Text.String", s);
    mmUIView_Changed(p->MyEditBox_001);

    sprintf(s, "%02d", pCalendar->tD);
    mmUIView_SetValueCStr(p->MyEditBox_002, "Text.String", s);
    mmUIView_Changed(p->MyEditBox_002);
}

void
mmUIViewCEditBox_OnEventTextChanged(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args)
{
    static const char* cTextColor[] =
    {
        "Text-A",
        "Text-B",
        "Text-C",
    };

    int r0, r1, r2;
    const char* pColor;
    const char* str0;
    const char* str1;
    const char* str2;
    struct mmString s0;
    struct mmString s1;
    struct mmString s2;

    mmString_Init(&s0);
    mmString_Init(&s1);
    mmString_Init(&s2);

    mmUIView_GetValueString(p->MyEditBox_000, "Text.String", &s0);
    mmUIView_GetValueString(p->MyEditBox_001, "Text.String", &s1);
    mmUIView_GetValueString(p->MyEditBox_002, "Text.String", &s2);

    r0 = mmString_IsNumber(&s0);
    r1 = mmString_IsNumber(&s1);
    r2 = mmString_IsNumber(&s2);

    str0 = mmString_Data(&s0);
    str1 = mmString_Data(&s1);
    str2 = mmString_Data(&s2);

    mmValue_StringToSInt(str0, &p->hY);
    mmValue_StringToSInt(str1, &p->hM);
    mmValue_StringToSInt(str2, &p->hD);

    p->hValid = mmGregoryTimeIsValid(p->hY, p->hM, p->hD);
    if (p->hValid)
    {
        pColor = r0 ? cTextColor[0] : cTextColor[1];
        mmUIView_SetValueCStr(p->MyEditBox_000, "Text.ForegroundColor", pColor);

        pColor = r1 ? cTextColor[0] : cTextColor[1];
        mmUIView_SetValueCStr(p->MyEditBox_001, "Text.ForegroundColor", pColor);

        pColor = r2 ? cTextColor[0] : cTextColor[1];
        mmUIView_SetValueCStr(p->MyEditBox_002, "Text.ForegroundColor", pColor);
    }
    else
    {
        pColor = cTextColor[2];
        mmUIView_SetValueCStr(p->MyEditBox_000, "Text.ForegroundColor", pColor);
        mmUIView_SetValueCStr(p->MyEditBox_001, "Text.ForegroundColor", pColor);
        mmUIView_SetValueCStr(p->MyEditBox_002, "Text.ForegroundColor", pColor);
    }

    mmUIView_Changed(p->MyEditBox_000);
    mmUIView_Changed(p->MyEditBox_001);
    mmUIView_Changed(p->MyEditBox_002);

    mmString_Destroy(&s2);
    mmString_Destroy(&s1);
    mmString_Destroy(&s0);
}

void
mmUIViewCEditBox_OnEventClicked00(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args)
{
    struct mmEventUIViewArgs hArgs;

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCEditBox_EventDateAccepted,
        &hArgs);
}

void
mmUIViewCEditBox_OnEventClicked01(
    struct mmUIViewCEditBox*                       p,
    struct mmEventUIViewArgs*                      args)
{
    struct mmEventUIViewArgs hArgs;

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCEditBox_EventClosed,
        &hArgs);
}
