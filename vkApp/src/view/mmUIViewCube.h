/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCube_h__
#define __mmUIViewCube_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKBuffer.h"
#include "vk/mmVKImage.h"
#include "vk/mmVKDescriptorSet.h"
#include "vk/mmVKUniformType.h"
#include "vk/mmVKTexel.h"
#include "vk/mmVKDrawable.h"

#include "cube/mmNoise.h"
#include "cube/mmNoise3DImage.h"
#include "cube/mmUVSphere.h"
#include "cube/mmIcoSphere.h"
#include "cube/mmNoiseIcoSphere.h"

#include "core/mmPrefix.h"

// Type base define.
mmUIViewTypeDefine(Cube);

struct mmEventCursorArgs;
struct mmEventUpdateArgs;
struct mmEventUIViewArgs;
struct mmEventUIViewCommandArgs;

struct mmEventCursor;

struct mmVKSamplerInfo;
struct mmVKAssets;
struct mmVKUploader;
struct mmVKPipeline;
struct mmVKPipelineInfo;
struct mmVKPoolInfo;

struct mmUICubeUBOVS
{
    mmUniformMat4 ProjectionView;
};

struct mmUICubeUBOFS
{
    mmUniformMat4 inverseView;
    mmUniformVec3 camera;
    mmUniformVec3 ambient;
    mmUniformFloat exposure;
    mmUniformInt lightCount;
    mmUniformFloat time;
    mmUniformFloat scale;
};

struct mmUICubePushConstant
{
    mmUniformMat4 Normal;
    mmUniformMat4 Model;
};

struct mmUIViewCube
{
    struct mmUIView view;
    
    /* UIView weak reference. */
    struct mmVKImageInfo* pImageInfo;
    
    struct mmNoise vNoise;
    struct mmNoise3DImage vNoise3DImage;
    
    struct mmVKTexel vTexel;
    struct mmVKSamplerInfo* pSamplerInfo;
    
    struct mmNoiseIcoSphere vNoiseIcoSphere;
    struct mmIcoSphere vIcoSphere;
    struct mmUVSphere vSphereEarth;
    struct mmUVSphere vSphereCloud;
    
    struct mmUICubeUBOVS vUBOSceneVS;
    struct mmUICubeUBOFS vUBOSceneFS;
    struct mmVKBuffer vBufferUBOVS;
    struct mmVKBuffer vBufferUBOFS;
    
    struct mmUICubePushConstant vConstant0;
    struct mmUICubePushConstant vConstant1;
    
    struct mmVKDrawable vDrawable0;
    struct mmVKDrawable vDrawable1;
    
    float l;
    float angle[2];
};

void
mmUIViewCube_Init(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_Destroy(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_UpdateSize(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_OnPrepare(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_OnDiscard(
    struct mmUIViewCube*                           p);

VkResult
mmUIViewCube_DescriptorSetUpdate0(
    struct mmUIViewCube*                           p);

VkResult
mmUIViewCube_DescriptorSetUpdate1(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_UpdateSceneUBO(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_UpdateSceneVS(
    struct mmUIViewCube*                           p);

void
mmUIViewCube_UpdateSceneFS(
    struct mmUIViewCube*                           p);

/* special */

void
mmUIViewCube_OnEventCursorMoved(
    struct mmUIViewCube*                           p,
    struct mmEventCursorArgs*                      args);

void
mmUIViewCube_OnEventCursorWheel(
    struct mmUIViewCube*                           p,
    struct mmEventCursorArgs*                      args);

void
mmUIViewCube_OnEventUpdate(
    struct mmUIViewCube*                           p,
    struct mmEventUpdateArgs*                      args);

void
mmUIViewCube_OnEventSized(
    struct mmUIViewCube*                           p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCube_OnEventRecordBasicsView(
    struct mmUIViewCube*                           p,
    struct mmEventUIViewCommandArgs*               args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCube_h__
