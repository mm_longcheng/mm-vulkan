/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewRoot.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIViewDirector.h"
#include "ui/mmUIEventArgs.h"

#include "nwsi/mmEventSurface.h"
#include "nwsi/mmSurfaceMaster.h"

#include "math/mmVector2.h"

void
mmUIViewRoot_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(
        propertys,
        offset + mmOffsetof(struct mmUIViewRoot, view));

    mmUIRoot_DefinePropertys(
        propertys,
        offset + mmOffsetof(struct mmUIViewRoot, root));
}

void
mmUIViewRoot_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIRoot_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

mmUIViewTypeImplementAllocator(Root);

void
mmUIViewRoot_Init(
    struct mmUIViewRoot*                           p)
{
    mmUIView_Init(&p->view);
    mmUIRoot_Init(&p->root);

    mmUIViewRoot_ResetReference(p);
}

void
mmUIViewRoot_Destroy(
    struct mmUIViewRoot*                           p)
{
    mmUIViewRoot_ResetReference(p);

    mmUIRoot_Destroy(&p->root);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewRoot_OnPrepare(
    struct mmUIViewRoot*                           p)
{
    struct mmUIViewDirector* pViewDirector = p->view.context->director;

    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewRoot.view.json");

    p->Background = mmUIView_GetChildAssert(&p->view, "Background");
    p->RootSafety = mmUIView_GetChildAssert(&p->view, "RootSafety");
    p->RootNotify = mmUIView_GetChildAssert(&p->view, "RootNotify");

    mmUIViewDirector_SetRootSafety(pViewDirector, p->RootSafety);

    mmEventSet_SubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventSafeRectChanged,
        &mmUIViewRoot_OnEventSafeRectChanged,
        p);

    mmUIViewRoot_OnAspectSafeRect(p);

    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewRoot_OnDiscard(
    struct mmUIViewRoot*                           p)
{
    struct mmUIViewDirector* pViewDirector = p->view.context->director;

    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventSafeRectChanged,
        &mmUIViewRoot_OnEventSafeRectChanged,
        p);

    mmUIViewDirector_SetRootSafety(pViewDirector, NULL);
    
    mmUIViewRoot_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewRoot_ResetReference(
    struct mmUIViewRoot*                           p)
{
    p->Background = NULL;
    p->RootSafety = NULL;
    p->RootNotify = NULL;
}

void
mmUIViewRoot_OnEventSafeRectChanged(
    struct mmUIViewRoot*                           p,
    struct mmEventUIViewContextArgs*               args)
{
    mmUIViewRoot_OnAspectSafeRect(p);
}

void
mmUIViewRoot_OnAspectSafeRect(
    struct mmUIViewRoot*                           p)
{
    mmUIView_OnAspect(
        p->RootSafety,
        p->view.context->safe,
        p->root.hCanvasSize,
        p->root.hAspectMode,
        p->root.hAspectRatio);

    mmUIView_OnAspect(
        p->RootNotify,
        p->view.context->safe,
        p->root.hCanvasSize,
        p->root.hAspectMode,
        p->root.hAspectRatio);
}

