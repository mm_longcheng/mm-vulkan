/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCMonth_h__
#define __mmUIViewCMonth_h__

#include "core/mmCore.h"

#include "core/mmTimeCache.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmUIDataCalendar;

struct mmEventUpdateArgs;
struct mmEventUIViewArgs;

// Type base define.
mmUIViewTypeDefine(CMonth);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCMonth_EventSelected;

struct mmUIViewCMonth
{
    struct mmUIView view;
    int vY, vM, vD;

    struct mmUIDataCalendar* pCalendar;

    /* UIView weak reference. */
    struct mmUIView* MyImage_0;

    struct mmUIView* MyImage_1;
    struct mmUIView* MyText_00;
    struct mmUIView* MyButton_00;
    struct mmUIView* MyText_01;
    struct mmUIView* MyButton_01;
    struct mmUIView* MyText_02;
    struct mmUIView* MyButton_02;
    struct mmUIView* MyText_03;

    struct mmUIView* MyImage_2;
    struct mmUIView* MyImage_3;

    struct mmUIView* MyImage_4;
    struct mmUIView* MyText_20;
    struct mmUIView* MyText_21;
    struct mmUIView* MyText_22;
    struct mmUIView* MyText_23;
    struct mmUIView* MyText_24;
    struct mmUIView* MyText_25;
    struct mmUIView* MyText_26;

    struct mmUIView* Day[6][7];

    struct mmUIView* pSelect;
};

void
mmUIViewCMonth_Init(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_Destroy(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_SetCalendar(
    struct mmUIViewCMonth*                         p,
    struct mmUIDataCalendar*                       calendar);

void
mmUIViewCMonth_OnPrepare(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_OnDiscard(
    struct mmUIViewCMonth*                         p);

/* special */

void
mmUIViewCMonth_ResetReference(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContentTitle(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContentMonth(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContentTaday(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContentDay(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateSelectDayView(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateSelectDay(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContent(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateContentPreview(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_UpdateTime(
    struct mmUIViewCMonth*                         p,
    int                                            change);

void
mmUIViewCMonth_PrepareDays(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_DiscardDays(
    struct mmUIViewCMonth*                         p);

void
mmUIViewCMonth_OnEventTodayClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args);

void
mmUIViewCMonth_OnEventPrevMonthClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args);

void
mmUIViewCMonth_OnEventNextMonthClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args);

void
mmUIViewCMonth_OnEventSelected(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIViewArgs*                      args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCMonth_h__
