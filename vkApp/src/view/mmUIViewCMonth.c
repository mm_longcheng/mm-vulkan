/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCMonth.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIViewLoader.h"
#include "ui/mmUIEventArgs.h"
#include "ui/mmUIViewText.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"
#include "core/mmTimeUtils.h"
#include "core/mmUTFConvert.h"

#include "nwsi/mmEventSurface.h"
#include "nwsi/mmTextFormat.h"

#include "math/mmVector2.h"
#include "math/mmVector3.h"

#include "sxwnl/mmSxwnlConst.h"
#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"
#include "sxwnl/mmSxwnlJD.h"

#include "view/mmUIViewCMonthDay.h"

#include "data/mmUIDataCalendar.h"

// Type base implement.
mmUIViewTypeImplement(CMonth);

const char* mmUIViewCMonth_EventSelected = "EventSelected";

void
mmUIViewCMonth_Init(
    struct mmUIViewCMonth*                         p)
{
    mmUIView_Init(&p->view);
    p->vY = -1; p->vM = -1; p->vD = -1;

    p->pCalendar = NULL;

    mmUIViewCMonth_ResetReference(p);

    mmMemset(p->Day, 0, sizeof(p->Day));

    p->pSelect = NULL;
}

void
mmUIViewCMonth_Destroy(
    struct mmUIViewCMonth*                         p)
{
    p->pSelect = NULL;

    mmMemset(p->Day, 0, sizeof(p->Day));

    mmUIViewCMonth_ResetReference(p);

    p->pCalendar = NULL;

    p->vY = -1; p->vM = -1; p->vD = -1;
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCMonth_SetCalendar(
    struct mmUIViewCMonth*                         p,
    struct mmUIDataCalendar*                       calendar)
{
    p->pCalendar = calendar;
}

void
mmUIViewCMonth_OnPrepare(
    struct mmUIViewCMonth*                         p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCMonth.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");

    p->MyImage_1 = mmUIView_GetChildAssert(&p->view, "MyImage_1");
    p->MyText_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_00");
    p->MyButton_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyButton_00");
    p->MyText_01 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_01");
    p->MyButton_01 = mmUIView_GetChildAssert(p->MyImage_1, "MyButton_01");
    p->MyText_02 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_02");
    p->MyButton_02 = mmUIView_GetChildAssert(p->MyImage_1, "MyButton_02");
    p->MyText_03 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_03");

    p->MyImage_2 = mmUIView_GetChildAssert(&p->view, "MyImage_2");
    p->MyImage_3 = mmUIView_GetChildAssert(&p->view, "MyImage_3");

    p->MyImage_4 = mmUIView_GetChildAssert(&p->view, "MyImage_4");
    p->MyText_20 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_20");
    p->MyText_21 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_21");
    p->MyText_22 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_22");
    p->MyText_23 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_23");
    p->MyText_24 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_24");
    p->MyText_25 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_25");
    p->MyText_26 = mmUIView_GetChildAssert(p->MyImage_4, "MyText_26");

    mmEventSet_SubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventTodayClicked,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventPrevMonthClicked,
        p);

    mmEventSet_SubscribeEvent(
        &p->MyButton_02->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventNextMonthClicked,
        p);

    mmUIView_PrepareChildren(&p->view);

    mmUIViewCMonth_PrepareDays(p);

    mmUIViewCMonth_UpdateContent(p);
}

void
mmUIViewCMonth_OnDiscard(
    struct mmUIViewCMonth*                         p)
{
    mmUIViewCMonth_DiscardDays(p);

    mmUIView_DiscardChildren(&p->view);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_02->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventNextMonthClicked,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_01->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventPrevMonthClicked,
        p);

    mmEventSet_UnSubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCMonth_OnEventTodayClicked,
        p);
    
    mmUIViewCMonth_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCMonth_ResetReference(
    struct mmUIViewCMonth*                         p)
{
    p->MyImage_0 = NULL;

    p->MyImage_1 = NULL;
    p->MyText_00 = NULL;
    p->MyButton_00 = NULL;
    p->MyText_01 = NULL;
    p->MyButton_01 = NULL;
    p->MyText_02 = NULL;
    p->MyButton_02 = NULL;
    p->MyText_03 = NULL;

    p->MyImage_2 = NULL;
    p->MyImage_3 = NULL;

    p->MyImage_4 = NULL;
    p->MyText_20 = NULL;
    p->MyText_21 = NULL;
    p->MyText_22 = NULL;
    p->MyText_23 = NULL;
    p->MyText_24 = NULL;
    p->MyText_25 = NULL;
    p->MyText_26 = NULL;
}

void
mmUIViewCMonth_UpdateContentTitle(
    struct mmUIViewCMonth*                         p)
{
    char s[32];

    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;
    mmUIDataCalendarTimeCYM(pCalendar->hY, pCalendar->hM, s);
    mmUIView_SetValueCStr(p->MyText_00, "Text.String", s);
    mmUIView_Changed(p->MyText_00);
}

void
mmUIViewCMonth_UpdateContentMonth(
    struct mmUIViewCMonth*                         p)
{
    static const float opacity0[4] = { 1.0f, 1.0f, 1.0f, 0.8f };
    static const float opacity1[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
    static const float opacity2[4] = { 1.0f, 1.0f, 1.0f, 0.8f };
    static const float opacity3[4] = { 1.0f, 1.0f, 1.0f, 0.8f };

    int i, j;
    int dn0, dn1, dn2;

    struct mmUIView* w;
    struct mmUIViewCMonthDay* u;
    struct csLunarOBDay* pDay;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    dn0 = pCalendar->hLun[0].dn;
    dn1 = pCalendar->hLun[1].dn;
    dn2 = pCalendar->hLun[2].dn;
    if (0 < dn0 && 0 < dn1 && 0 < dn2)
    {
        // part 0
        for (i = 0; i < pCalendar->weekd[0]; i++)
        {
            pDay = &pCalendar->hLun[0].day[i - pCalendar->daydt[0]];
            w = p->Day[pCalendar->weeki[0]][pDay->week];
            u = (struct mmUIViewCMonthDay*)(w);
            mmUIView_SetOpacity(w, opacity0);
            mmUIViewCMonthDay_SetData(u, pDay);
            mmUIViewCMonthDay_UpdateContent(u);
            mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
            mmUIView_Invalidate(w);
        }

        // part 1
        for (i = 0; i < dn1; i++)
        {
            pDay = &pCalendar->hLun[1].day[i - pCalendar->daydt[1]];
            w = p->Day[pDay->weeki][pDay->week];
            u = (struct mmUIViewCMonthDay*)(w);
            mmUIView_SetOpacity(w, opacity1);
            mmUIViewCMonthDay_SetData(u, pDay);
            mmUIViewCMonthDay_UpdateContent(u);
            mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
            mmUIView_Invalidate(w);
        }

        // part 2
        for (i = pCalendar->weekd[1]; i < 7; i++)
        {
            pDay = &pCalendar->hLun[2].day[i - pCalendar->daydt[2]];
            w = p->Day[pCalendar->weeki[1]][pDay->week];
            u = (struct mmUIViewCMonthDay*)(w);
            mmUIView_SetOpacity(w, opacity2);
            mmUIViewCMonthDay_SetData(u, pDay);
            mmUIViewCMonthDay_UpdateContent(u);
            mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
            mmUIView_Invalidate(w);
        }
 
        // part 3
        for (i = pCalendar->weeki[1] + 1; i < 6; i++)
        {
            for (j = 0; j < 7; j++)
            {
                pDay = &pCalendar->hLun[2].day[i * 7 + j - pCalendar->daydt[3]];
                w = p->Day[i][j];
                u = (struct mmUIViewCMonthDay*)(w);
                mmUIView_SetOpacity(w, opacity3);
                mmUIViewCMonthDay_SetData(u, pDay);
                mmUIViewCMonthDay_UpdateContent(u);
                mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
                mmUIView_Invalidate(w);
            }
        }
    }
    else
    {
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 7; j++)
            {
                w = p->Day[i][j];
                u = (struct mmUIViewCMonthDay*)(w);
                mmUIView_SetOpacity(w, opacity3);
                mmUIViewCMonthDay_SetData(u, NULL);
                mmUIViewCMonthDay_UpdateContent(u);
                mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
                mmUIView_Invalidate(w);
            }
        }
    }
}

void
mmUIViewCMonth_UpdateContentTaday(
    struct mmUIViewCMonth*                         p)
{
    int i, j;

    struct mmUIView* w;
    struct mmUIViewCMonthDay* u;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            w = p->Day[i][j];
            u = (struct mmUIViewCMonthDay*)(w);
            mmUIViewCMonthDay_UpdateGeneral(u, pCalendar->hTodayJDi);
        }
    }
}

void
mmUIViewCMonth_UpdateContentDay(
    struct mmUIViewCMonth*                         p)
{
    struct mmUIDataCalendar* pCalendar;
    const struct csLunarOBDay* pDay;

    pCalendar = p->pCalendar;
    pDay = mmUIDataCalendar_GetSelectToday(pCalendar);
    if (NULL != pDay)
    {
        static const struct mmString cTextFTVColor[] =
        {
            mmString_Make("Text-7"),
            mmString_Make("Text-8"),
            mmString_Make("Text-9"),
        };

        static const mmUtf16_t cTextSign[] = { '\n', 0 };

        struct mmUIViewText* FTV;
        struct mmUtf16String* pDescribe;

        struct mmUtf16String ftv;
        struct mmRange r0, r1, r2;
        float hColor0[4], hColor1[4], hColor2[4];
        const mmUtf16_t* sign;

        size_t idx;
        char s[32];
        char ymd[32];
        char week[16];
        char xing[16];

        char tsgzy[16];
        char tsgzm[16];
        char tsgzd[16];

        mmUIDataCalendarTimeNYMD(pDay->Lyear4, pDay->Lmi, pDay->Ldi, ymd);
        sprintf(s, "%s %s", "农历", ymd);
        mmUIView_SetValueCStr(p->MyText_20, "Text.String", s);
        mmUIView_Changed(p->MyText_20);

        mmUIDataCalendarTimeCYMD(pDay->y, pDay->m, pDay->d, ymd);
        sprintf(s, "%s %s", "公历", ymd);
        mmUIView_SetValueCStr(p->MyText_21, "Text.String", s);
        mmUIView_Changed(p->MyText_21);

        mmUIDataCalendarTimeCYMD(pDay->Hyear, pDay->Hmonth, pDay->Hday, ymd);
        sprintf(s, "%s %s", "回历", ymd);
        mmUIView_SetValueCStr(p->MyText_22, "Text.String", s);
        mmUIView_Changed(p->MyText_22);

        mmUIDataCalendarTimeWeek(pDay->week, week);
        mmUIDataCalendarTimeXing(pDay->XiZid, xing);
        sprintf(s, "%s %s", week, xing);
        mmUIView_SetValueCStr(p->MyText_23, "Text.String", s);
        mmUIView_Changed(p->MyText_23);

        mmUIDataCalendarTimeGZ(pDay->Lgzjn, "年", tsgzy);
        mmUIDataCalendarTimeGZ(pDay->Lgzjy, "月", tsgzm);
        mmUIDataCalendarTimeGZ(pDay->Lgzjr, "日", tsgzd);
        sprintf(s, "%s %s %s", tsgzy, tsgzm, tsgzd);
        mmUIView_SetValueCStr(p->MyText_24, "Text.String", s);
        mmUIView_Changed(p->MyText_24);

        sprintf(s, "JD %d(%d)", pDay->d0 + cs_J2000, pDay->d0);
        mmUIView_SetValueCStr(p->MyText_25, "Text.String", s);
        mmUIView_Changed(p->MyText_25);

        idx = 0;
        FTV = (struct mmUIViewText*)p->MyText_26;
        pDescribe = mmUIViewText_GetTextUtf16String(FTV);

        mmUIView_SetValueCStr(p->MyText_26, "Text.String", "");
        mmUIView_SetValueCStr(p->MyText_26, "Text.ForegroundColor", "Text-4");

        mmUIView_GetSchemeColor(&p->view, &cTextFTVColor[0], hColor0);
        mmUIView_GetSchemeColor(&p->view, &cTextFTVColor[1], hColor1);
        mmUIView_GetSchemeColor(&p->view, &cTextFTVColor[2], hColor2);

        mmUtf16String_Reset(pDescribe);

        mmUtf16String_Init(&ftv);
        mmUTF_8to16(&pDay->A, &ftv);
        r0.o = idx;
        r0.l = mmUtf16String_Size(&ftv);
        mmUtf16String_Append(pDescribe, &ftv);
        idx += r0.l;

        sign = (0 == r0.l) ? mmUtf16StringEmpty : cTextSign;
        mmUtf16String_Appends(pDescribe, sign);
        idx += (0 == r0.l) ? 0 : 1;

        mmUTF_8to16(&pDay->B, &ftv);
        r1.o = idx;
        r1.l = mmUtf16String_Size(&ftv);
        mmUtf16String_Append(pDescribe, &ftv);
        idx += r1.l;

        sign = (0 == r1.l) ? mmUtf16StringEmpty : cTextSign;
        mmUtf16String_Appends(pDescribe, sign);
        idx += (0 == r1.l) ? 0 : 1;

        mmUTF_8to16(&pDay->C, &ftv);
        r2.o = idx;
        r2.l = mmUtf16String_Size(&ftv);
        mmUtf16String_Append(pDescribe, &ftv);
        idx += r2.l;

        mmUtf16String_Destroy(&ftv);

        mmUIViewText_SpannableReset(FTV);
        mmUIViewText_SetRangeProperty(FTV, &r0, mmTextPropertyForegroundColor, hColor0);
        mmUIViewText_SetRangeProperty(FTV, &r1, mmTextPropertyForegroundColor, hColor1);
        mmUIViewText_SetRangeProperty(FTV, &r2, mmTextPropertyForegroundColor, hColor2);
        mmUIView_Changed(p->MyText_26);
    }
}

void
mmUIViewCMonth_UpdateSelectDayView(
    struct mmUIViewCMonth*                         p)
{
    do
    {
        struct mmUIDataCalendar* pCalendar;
        const struct csLunarOBDay* pDay1;
        const struct csLunarOBDay* pDay0;
        int v, a, b;

        pCalendar = p->pCalendar;
        pDay1 = mmUIDataCalendar_GetSelectToday(pCalendar);

        if (NULL == pDay1 ||
            pDay1->y != pCalendar->sY ||
            pDay1->m != pCalendar->sM ||
            pDay1->d != pCalendar->sD)
        {
            p->pSelect = NULL;
            break;
        }
        
        pDay0 = mmUIDataCalendar_GetVisibleDay0(pCalendar);
        v = pDay1->d0 - pDay0->d0;
        a = v / 7;
        b = v - a * 7;
        if (a < 0 || 6 <= a || 
            b < 0 || 7 <= b)
        {
            p->pSelect = NULL;
            break;
        }

        p->pSelect = p->Day[a][b];
    } while(0);
}

void
mmUIViewCMonth_UpdateSelectDay(
    struct mmUIViewCMonth*                         p)
{
    struct mmUIView* w;
    struct mmUIViewCMonthDay* u;

    if (NULL != p->pSelect)
    {
        w = p->pSelect;
        u = (struct mmUIViewCMonthDay*)(w);
        mmUIViewCMonthDay_UpdateSelect(u, 0);
        p->pSelect = NULL;
    }

    mmUIViewCMonth_UpdateSelectDayView(p);

    if (NULL != p->pSelect)
    {
        w = p->pSelect;
        u = (struct mmUIViewCMonthDay*)(w);
        mmUIViewCMonthDay_UpdateSelect(u, 1);
    }
}

void
mmUIViewCMonth_UpdateContent(
    struct mmUIViewCMonth*                         p)
{
    int hDifferent;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    hDifferent = mmUIDataCalendarGetTwoDateDifferent(
        pCalendar->hY,
        pCalendar->hM,
        pCalendar->sD,
        p->vY,
        p->vM,
        p->vD);

    p->vY = pCalendar->hY;
    p->vM = pCalendar->hM;
    p->vD = pCalendar->sD;

    switch (hDifferent)
    {
    case 0: // 时间一样
        break;
    case 1: // 不同日
        mmUIViewCMonth_UpdateContentTitle(p);
        mmUIViewCMonth_UpdateContentTaday(p);
        mmUIViewCMonth_UpdateContentDay(p);
        mmUIViewCMonth_UpdateSelectDay(p);
        break;
    case 2: // 不同月
    case 3: // 不同年
        mmUIViewCMonth_UpdateContentTitle(p);
        mmUIViewCMonth_UpdateContentMonth(p);
        mmUIViewCMonth_UpdateContentDay(p);
        mmUIViewCMonth_UpdateSelectDay(p);
        break;
    default:
        break;
    }
}

void
mmUIViewCMonth_UpdateContentPreview(
    struct mmUIViewCMonth*                         p)
{
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    p->vY = pCalendar->hY;
    p->vM = pCalendar->hM;
    p->vD = pCalendar->sD;

    mmUIViewCMonth_UpdateContentTitle(p);
    mmUIViewCMonth_UpdateContentMonth(p);
    mmUIViewCMonth_UpdateSelectDay(p);
}

void
mmUIViewCMonth_UpdateTime(
    struct mmUIViewCMonth*                         p,
    int                                            change)
{
    if (2 == change)
    {
        mmUIViewCMonth_UpdateContent(p);
    }
}

void
mmUIViewCMonth_PrepareDays(
    struct mmUIViewCMonth*                         p)
{
    int i, j;
    float hDaySize[2];
    float hDayPosition[2];
    float LocalSize[3];
    struct mmUIDim uPosition[3];
    struct mmUIView* w;
    struct mmUIViewContext* pViewContext = p->view.context;
    struct mmUIViewLoader* pViewLoader = pViewContext->vloader;

    mmUIView_GetLocalSize(p->MyImage_3, LocalSize);
    hDaySize[0] = LocalSize[0] / 7.0f;
    hDaySize[1] = LocalSize[1] / 6.0f;
    mmVec2MulK(hDayPosition, hDaySize, 0.5f);

    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            w = mmUIViewLoader_ProduceViewType(
                pViewLoader,
                pViewContext,
                "mmUIViewCMonthDay",
                "");

            uPosition[0].o = hDayPosition[0] + hDaySize[0] * j;
            uPosition[0].s = 0.0f;
            uPosition[1].o = hDayPosition[1] + hDaySize[1] * i;
            uPosition[1].s = 0.0f;
            uPosition[2].o = 0.0f;
            uPosition[2].s = 0.0f;

            mmUIView_SetPosition(w, uPosition);
            mmUIView_SetContext(w, pViewContext);

            mmEventSet_SubscribeEvent(
                &w->events,
                mmUIViewCMonthDay_EventSelected,
                &mmUIViewCMonth_OnEventSelected,
                p);

            mmUIView_AddChild(p->MyImage_3, w);

            mmUIView_Prepare(w);
            p->Day[i][j] = w;
        }
    }
}

void
mmUIViewCMonth_DiscardDays(
    struct mmUIViewCMonth*                         p)
{
    int i, j;

    struct mmUIView* w;
    struct mmUIViewContext* pViewContext = p->view.context;
    struct mmUIViewLoader* pViewLoader = pViewContext->vloader;

    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            w = p->Day[i][j];
            p->Day[i][j] = NULL;
            mmUIView_Discard(w);

            mmUIView_RmvChild(p->MyImage_3, w);

            mmEventSet_UnSubscribeEvent(
                &w->events,
                mmUIViewCMonthDay_EventSelected,
                &mmUIViewCMonth_OnEventSelected,
                p);

            mmUIViewLoader_RecycleViewType(
                pViewLoader,
                w);
        }
    }
}

void
mmUIViewCMonth_OnEventTodayClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;
    
    mmUIDataCalendar_UpdateToday(p->pCalendar);
    mmUIDataCalendar_UpdateLun(p->pCalendar);
    mmUIDataCalendar_UpdateYearJQ(p->pCalendar);
    mmUIViewCMonth_UpdateContent(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCMonth_EventSelected,
        &hArgs);
}

void
mmUIViewCMonth_OnEventPrevMonthClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args)
{
    mmUIDataCalendar_PrevMonth(p->pCalendar);
    mmUIViewCMonth_UpdateContentPreview(p);
}

void
mmUIViewCMonth_OnEventNextMonthClicked(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIClickedArgs*                   args)
{
    mmUIDataCalendar_NextMonth(p->pCalendar);
    mmUIViewCMonth_UpdateContentPreview(p);
}

void
mmUIViewCMonth_OnEventSelected(
    struct mmUIViewCMonth*                         p,
    struct mmEventUIViewArgs*                      args)
{
    struct mmEventUIViewArgs hArgs;
    struct mmUIViewCMonthDay* u;
    struct csLunarOBDay* pDay;
    struct mmUIDataCalendar* pCalendar;
    u = (struct mmUIViewCMonthDay*)(args->pView);
    pDay = u->pOBDay;
    pCalendar = p->pCalendar;

    if (pCalendar->hM == pDay->m)
    {
        mmTimeYMNormalize(pDay->y, pDay->m, &pCalendar->hY, &pCalendar->hM);
        pCalendar->sY = pCalendar->hY;
        pCalendar->sM = pCalendar->hM;
        pCalendar->sD = pDay->d;
        mmUIDataCalendar_UpdateSQH(pCalendar);
        mmUIViewCMonth_UpdateContent(p);
    }
    else
    {
        mmTimeYMNormalize(pDay->y, pDay->m, &pCalendar->hY, &pCalendar->hM);
        mmUIDataCalendar_UpdateLun(pCalendar);
        mmUIDataCalendar_UpdateYearJQ(pCalendar);
        pCalendar->sY = pCalendar->hY;
        pCalendar->sM = pCalendar->hM;
        pCalendar->sD = pDay->d;
        mmUIDataCalendar_UpdateSQH(pCalendar);
        mmUIViewCMonth_UpdateContent(p);
    }

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCMonth_EventSelected,
        &hArgs);
}
