/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewWorld.h"

#include "core/mmLogger.h"

#include "nwsi/mmEventSurface.h"
#include "nwsi/mmSurfaceMaster.h"

#include "ui/mmUIViewContext.h"

#include "vk/mmVKAssets.h"

#include "al/mmALManager.h"
#include "al/mmALSource.h"

#include "emu/mmEmuPalette.h"

// Type base implement.
mmUIViewTypeImplement(World);

void mmALSourceViewOnLoaded(void* obj, struct mmALSource* s);
void mmALSourceViewOnDestroyed(void* obj, struct mmALSource* s);
void mmALSourceViewOnPlayed(void* obj, struct mmALSource* s);
void mmALSourceViewOnStopped(void* obj, struct mmALSource* s);
void mmALSourceViewOnFinished(void* obj, struct mmALSource* s);
void mmALSourceViewOnPaused(void* obj, struct mmALSource* s);
void mmALSourceViewOnLooping(void* obj, struct mmALSource* s);

void
mmUIViewWorld_Init(
    struct mmUIViewWorld*                          p)
{
    mmUIView_Init(&p->view);
    
    p->MyButton = NULL;
    p->MyEmulator = NULL;
    
    mmEmulatorMachine_Init(&p->hMachine);
    
    p->pALSource = NULL;
}

void
mmUIViewWorld_Destroy(
    struct mmUIViewWorld*                          p)
{
    p->pALSource = NULL;
    
    mmEmulatorMachine_Destroy(&p->hMachine);
    
    p->MyEmulator = NULL;
    p->MyButton = NULL;
    
    mmUIView_Destroy(&p->view);
}

void
mmUIViewWorld_OnPrepare(
    struct mmUIViewWorld*                          p)
{
    struct mmVKAssets* pAssets = p->view.context->assets;
    struct mmFileContext* pFileContext = pAssets->pFileContext;
    struct mmVKUploader* pUploader = pAssets->pUploader;
    struct mmPackageAssets* pPackageAssets = pAssets->pPackageAssets;

    struct mmALManager* pALManager;
    
    struct mmALSourceCallback hCallback =
    {
        &mmALSourceViewOnLoaded,
        &mmALSourceViewOnDestroyed,
        &mmALSourceViewOnPlayed,
        &mmALSourceViewOnStopped,
        &mmALSourceViewOnFinished,
        &mmALSourceViewOnPaused,
        &mmALSourceViewOnLooping,
        p,
    };
    
    pALManager = (struct mmALManager*)mmUIView_GetModuleAssert(&p->view, "mmALManager");

    mmVKAssets_LoadPipelineFromName(pAssets, "mmUIEmulator");
    
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewWorld.view.json");
    
    p->MyButton = mmUIView_GetChildAssert(&p->view, "MyButton");
    p->MyEmulator = mmUIView_GetChildAssert(&p->view, "MyEmulator");
    
    mmUIView_SetPointerDirect(p->MyEmulator, "Emulator.Machine", &p->hMachine);
        
    // struct mmALBufferInfo* pALBufferInfo0 =
    mmALManager_LoadBufferFromPath(
        pALManager,
        "audio/camera_shutter.ogg");
    
    // struct mmALBufferInfo* pALBufferInfo1 =
    mmALManager_LoadBufferFromPath(
        pALManager,
        "audio/click_magic.ogg");
    
    p->pALSource =
    mmALManager_ProduceSource(
        pALManager,
        "audio/Knife Club Overture.mp3",
        MM_TRUE);
    
    mmALSource_SetLoop(p->pALSource, MM_FALSE);
    mmALSource_SetCallback(p->pALSource, &hCallback);
    mmALSource_Play(p->pALSource);
    
    mmEmulatorMachine_SetUploader(&p->hMachine, pUploader);
    mmEmulatorMachine_SetPackageAssets(&p->hMachine, pPackageAssets);
    mmEmulatorMachine_SetFileContext(&p->hMachine, pFileContext);
    mmEmulatorMachine_SetName(&p->hMachine, "mm");
    mmEmulatorMachine_SetRomPath(&p->hMachine, "Kirby's Adventure (U) (PRG0) [b1].nes");
    mmEmulatorMachine_SetRomPath(&p->hMachine, "Zelda 2 - The Adventure of Link (U).nes");
    mmEmulatorMachine_SetRomPath(&p->hMachine, "Jackal (U) [o2].nes");
    mmEmulatorMachine_SetDataPath(&p->hMachine, "emulator");
    mmEmulatorMachine_SetWritablePath(&p->hMachine, "emulator");
    mmEmulatorMachine_SetTaskBackground(&p->hMachine, MM_FRAME_TIMER_BACKGROUND_PAUSED);
    mmEmulatorMachine_SetPaletteTableByte(&p->hMachine, MM_EMU_PALETTE_DEFAULT);
    mmEmulatorMachine_Prepare(&p->hMachine);

    mmEventSet_SubscribeEvent(
        &p->MyButton->events,
        mmUIView_EventClicked,
        &mmUIViewWorld_OnEventClicked,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewWorld_OnEventUpdate,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventKeypadPressed,
        &mmUIViewWorld_OnEventKeypadPressed,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.events,
        mmUIView_EventKeypadRelease,
        &mmUIViewWorld_OnEventKeypadRelease,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventEnterBackground,
        &mmUIViewWorld_OnEventEnterBackground,
        p);
    
    mmEventSet_SubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventEnterForeground,
        &mmUIViewWorld_OnEventEnterForeground,
        p);
    
    mmEmulatorMachine_Start(&p->hMachine);

    mmEmulatorMachine_LoadRom(&p->hMachine);
    mmEmulatorMachine_Play(&p->hMachine);
    
    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewWorld_OnDiscard(
    struct mmUIViewWorld*                          p)
{
    struct mmALManager* pALManager;
    struct mmVKAssets* pAssets = p->view.context->assets;
    pALManager = (struct mmALManager*)mmUIView_GetModuleAssert(&p->view, "mmALManager");

    mmUIView_DiscardChildren(&p->view);
    
    mmEmulatorMachine_Stop(&p->hMachine);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventEnterForeground,
        &mmUIViewWorld_OnEventEnterForeground,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.context->events,
        mmUIViewContext_EventEnterBackground,
        &mmUIViewWorld_OnEventEnterBackground,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventKeypadRelease,
        &mmUIViewWorld_OnEventKeypadRelease,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventKeypadPressed,
        &mmUIViewWorld_OnEventKeypadPressed,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->view.events,
        mmUIView_EventUpdate,
        &mmUIViewWorld_OnEventUpdate,
        p);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton->events,
        mmUIView_EventClicked,
        &mmUIViewWorld_OnEventClicked,
        p);
    
    mmEmulatorMachine_Discard(&p->hMachine);
    mmEmulatorMachine_Shutdown(&p->hMachine);
    mmEmulatorMachine_Join(&p->hMachine);

    mmALManager_StopEffectComplete(pALManager);

    mmALSource_Stop(p->pALSource);
    mmALManager_RecycleSource(
        pALManager,
        p->pALSource);
    p->pALSource = NULL;

    mmALManager_UnloadBufferByPath(
        pALManager,
        "audio/click_magic.ogg");

    mmALManager_UnloadBufferByPath(
        pALManager,
        "audio/camera_shutter.ogg");
    
    p->MyEmulator = NULL;
    p->MyButton = NULL;
    
    mmUIView_DiscardView(&p->view);
    
    mmVKAssets_UnloadPipelineByName(pAssets, "mmUIEmulator");
}

void
mmUIViewWorld_OnEventClicked(
    struct mmUIViewWorld*                          p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmALManager* pALManager;
    pALManager = (struct mmALManager*)mmUIView_GetModuleAssert(&p->view, "mmALManager");
    //if (NULL != p->pALSource)
    //{
    //    mmALSource_Rewind(p->pALSource);
    //    mmALSource_Play(p->pALSource);
    //}
    
    static const char* gEffets[] =
    {
        "audio/camera_shutter.ogg",
        "audio/click_magic.ogg",
    };

    static const int N = 100;
    int i;
    for (i = 0; i < N; i++)
    {
        int n = rand() % 2;
        mmALManager_PlayEffectDetail(
            pALManager,
            gEffets[n],
            rand() % 100 / 200.0,
            1.0f,
            1.0f);
    }
}

void
mmUIViewWorld_OnEventUpdate(
    struct mmUIViewWorld*                         p,
    struct mmEventUpdateArgs*                     args)
{
    mmEmulatorMachine_UpdateRenderer(&p->hMachine);
}

void
mmUIViewWorld_OnEventKeypadPressed(
    struct mmUIViewWorld*                         p,
    struct mmEventKeypadArgs*                     args)
{
    struct mmEventKeypad* content = args->content;
    mmEmulatorMachine_KeyboardPressed(&p->hMachine, content->key);
}

void
mmUIViewWorld_OnEventKeypadRelease(
    struct mmUIViewWorld*                         p,
    struct mmEventKeypadArgs*                     args)
{
    struct mmEventKeypad* content = args->content;
    mmEmulatorMachine_KeyboardRelease(&p->hMachine, content->key);
}

void
mmUIViewWorld_OnEventEnterBackground(
    struct mmUIViewWorld*                         p,
    struct mmEventArgs*                           args)
{
    mmEmulatorMachine_EnterBackground(&p->hMachine);
}

void
mmUIViewWorld_OnEventEnterForeground(
    struct mmUIViewWorld*                         p,
    struct mmEventArgs*                           args)
{
    mmEmulatorMachine_EnterForeground(&p->hMachine);
}

void mmALSourceViewOnLoaded(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnDestroyed(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnPlayed(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnStopped(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnFinished(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnPaused(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
void mmALSourceViewOnLooping(void* obj, struct mmALSource* s)
{
    printf("%s %d\n", __FUNCTION__, __LINE__);
}
