/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCYearTerm_h__
#define __mmUIViewCYearTerm_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "sxwnl/mmSxwnlJD.h"

#include "core/mmPrefix.h"

struct csLunarOBDay;

// Type base define.
mmUIViewTypeDefine(CYearTerm);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCYearTerm_EventSelected;

struct mmUIViewCYearTerm
{
    struct mmUIView view;
    struct csJD hJD;
    int hJQ;

    /* UIView weak reference. */
    struct mmUIView* MyImage_0;
    struct mmUIView* MyImage_1;
    struct mmUIView* MyImage_2;
    struct mmUIView* MyImage_3;
    struct mmUIView* MyButton_0;
    struct mmUIView* MyText_0;
};

void
mmUIViewCYearTerm_Init(
    struct mmUIViewCYearTerm*                      p);

void
mmUIViewCYearTerm_Destroy(
    struct mmUIViewCYearTerm*                      p);

void
mmUIViewCYearTerm_SetData(
    struct mmUIViewCYearTerm*                      p,
    struct csJD*                                   pJD,
    int                                            hJQ);

void
mmUIViewCYearTerm_OnPrepare(
    struct mmUIViewCYearTerm*                      p);

void
mmUIViewCYearTerm_OnDiscard(
    struct mmUIViewCYearTerm*                      p);

/* special */

void
mmUIViewCYearTerm_ResetReference(
    struct mmUIViewCYearTerm*                      p);

void
mmUIViewCYearTerm_UpdateGeneral(
    struct mmUIViewCYearTerm*                      p,
    int                                            tJQ,
    int                                            sJQ);

void
mmUIViewCYearTerm_UpdateToday(
    struct mmUIViewCYearTerm*                      p,
    int                                            hValue);

void
mmUIViewCYearTerm_UpdateSelect(
    struct mmUIViewCYearTerm*                      p,
    int                                            hValue);

void
mmUIViewCYearTerm_UpdateContent(
    struct mmUIViewCYearTerm*                      p);

void
mmUIViewCYearTerm_OnEventClicked(
    struct mmUIViewCYearTerm*                      p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCYearTerm_h__
