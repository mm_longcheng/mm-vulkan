/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewTypes.h"

#include "ui/mmUIViewFactory.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewLayer.h"
#include "ui/mmUIViewImage.h"
#include "ui/mmUIViewText.h"
#include "ui/mmUIViewEditBox.h"
#include "ui/mmUIViewButton.h"

#include "view/mmUIViewRoot.h"
#include "view/mmUIViewEmulator.h"
#include "view/mmUIViewMaster.h"
#include "view/mmUIViewWorld.h"
#include "view/mmUIViewCube.h"
#include "view/mmUIViewModel.h"

#include "view/mmUIViewCalendar.h"
#include "view/mmUIViewCButton.h"
#include "view/mmUIViewCDay.h"
#include "view/mmUIViewCMonth.h"
#include "view/mmUIViewCMonthDay.h"
#include "view/mmUIViewCYear.h"
#include "view/mmUIViewCYearTerm.h"
#include "view/mmUIViewCEditBox.h"

void mmUIViewFactoryAddTypes(struct mmUIViewFactory* pFactory)
{
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataView);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewLayer);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewImage);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewText);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewEditBox);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewButton);

    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewRoot);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewEmulator);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewMaster);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewWorld);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCube);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewModel);

    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCalendar);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCButton);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCDay);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCMonth);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCMonthDay);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCYear);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCYearTerm);
    mmUIViewFactory_AddType(pFactory, &mmUIMetadataViewCEditBox);
}

void mmUIViewFactoryRmvTypes(struct mmUIViewFactory* pFactory)
{
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCEditBox);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCYearTerm);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCYear);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCMonthDay);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCMonth);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCDay);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCButton);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCalendar);

    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewModel);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewCube);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewWorld);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewMaster);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewEmulator);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewRoot);

    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewButton);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewEditBox);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewText);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewImage);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataViewLayer);
    mmUIViewFactory_RmvType(pFactory, &mmUIMetadataView);
}

