/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCButton.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmLogger.h"
#include "core/mmValueTransform.h"
#include "core/mmPropertyHelper.h"

#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"

void
mmUICButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUICButton";

    mmPropertyDefineMutable(propertys,
        "CButton.String", offset, struct mmUICButton,
        Text, String, "",
        NULL,
        NULL,
        cOrigin,
        "CButton Text string default \"\".");

    mmPropertyDefineMutable(propertys,
        "CButton.FrameColor", offset, struct mmUICButton,
        FrameColor, String, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "CButton FrameColor string default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "CButton.BackgroundColor", offset, struct mmUICButton,
        BackgroundColor, String, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "CButton BackgroundColor string default (1, 1, 1, 1).");
}

void
mmUICButton_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmPropertyRemove(propertys, "CButton.String");
    mmPropertyRemove(propertys, "CButton.FrameColor");
    mmPropertyRemove(propertys, "CButton.BackgroundColor");
}

void
mmUICButton_Init(
    struct mmUICButton*                            p)
{
    mmString_Init(&p->Text);
    mmString_Init(&p->FrameColor);
    mmString_Init(&p->BackgroundColor);

    mmString_Assigns(&p->FrameColor, "(1, 1, 1, 1)");
    mmString_Assigns(&p->BackgroundColor, "(1, 1, 1, 1)");
}

void
mmUICButton_Destroy(
    struct mmUICButton*                            p)
{
    mmString_Destroy(&p->BackgroundColor);
    mmString_Destroy(&p->FrameColor);
    mmString_Destroy(&p->Text);
}

void
mmUIViewCButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(
        propertys,
        offset + mmOffsetof(struct mmUIViewCButton, view));

    mmUICButton_DefinePropertys(
        propertys,
        offset + mmOffsetof(struct mmUIViewCButton, button));
}

void
mmUIViewCButton_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUICButton_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

mmUIViewTypeImplementAllocator(CButton);

const char* mmUIViewCButton_EventSelected = "EventSelected";

void
mmUIViewCButton_Init(
    struct mmUIViewCButton*                        p)
{
    mmUIView_Init(&p->view);
    mmUICButton_Init(&p->button);

    mmUIViewCButton_ResetReference(p);
}

void
mmUIViewCButton_Destroy(
    struct mmUIViewCButton*                        p)
{
    mmUIViewCButton_ResetReference(p);

    mmUICButton_Destroy(&p->button);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCButton_OnPrepare(
    struct mmUIViewCButton*                        p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCButton.view.json");

    p->MyButton_0 = mmUIView_GetChildAssert(&p->view, "MyButton_0");
    p->MyText_0 = mmUIView_GetChildAssert(&p->view, "MyText_0");
    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");

    mmUIView_SetValueCStr(p->MyText_0, "Text.String", mmString_CStr(&p->button.Text));
    //mmUIView_SetValueCStr(p->MyButton_0, "Drawable.Color", mmString_CStr(&p->button.BackgroundColor));
    //mmUIView_SetValueCStr(p->MyImage_0, "Drawable.Color", mmString_CStr(&p->button.FrameColor));

    mmEventSet_SubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCButton_OnEventClicked,
        p);

    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewCButton_OnDiscard(
    struct mmUIViewCButton*                        p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCButton_OnEventClicked,
        p);
    
    mmUIViewCButton_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCButton_ResetReference(
    struct mmUIViewCButton*                        p)
{
    p->MyImage_0 = NULL;
}

void
mmUIViewCButton_UpdateSelect(
    struct mmUIViewCButton*                        p,
    int                                            hValue)
{
    mmUIView_SetVisible(p->MyImage_0, hValue);
}

void
mmUIViewCButton_OnEventClicked(
    struct mmUIViewCButton*                        p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCButton_EventSelected,
        &hArgs);
}
