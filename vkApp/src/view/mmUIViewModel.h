/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewModel_h__
#define __mmUIViewModel_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKBuffer.h"
#include "vk/mmVKImage.h"
#include "vk/mmVKDescriptorSet.h"
#include "vk/mmVKUniformType.h"

#include "vk/mmVKScene.h"
#include "vk/mmVKTFObject.h"

#include "core/mmPrefix.h"

// Type base define.
 mmUIViewTypeDefine(Model);

struct mmEventUpdateArgs;
struct mmEventUIViewArgs;
struct mmEventUIViewCommandArgs;

struct mmUIViewModel
{
    struct mmUIView view;
    
    /* UIView weak reference. */
    struct mmVKScene vScene;
    struct mmVKTFObject vAsset1;
    struct mmString vModelPath;

    float l;
    float angle[2];
};

void
mmUIViewModel_Init(
    struct mmUIViewModel*                          p);

void
mmUIViewModel_Destroy(
    struct mmUIViewModel*                          p);

void
mmUIViewModel_OnPrepare(
    struct mmUIViewModel*                          p);

void
mmUIViewModel_OnDiscard(
    struct mmUIViewModel*                          p);

/* special */

void
mmUIViewModel_UpdateSceneUBO(
    struct mmUIViewModel*                          p);

void
mmUIViewModel_OnEventCursorMoved(
    struct mmUIViewModel*                          p,
    struct mmEventCursorArgs*                      args);

void
mmUIViewModel_OnEventCursorWheel(
    struct mmUIViewModel*                          p,
    struct mmEventCursorArgs*                      args);

void
mmUIViewModel_OnEventUpdate(
    struct mmUIViewModel*                          p,
    struct mmEventUpdateArgs*                      args);

void
mmUIViewModel_OnEventSized(
    struct mmUIViewModel*                          p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewModel_OnEventRecordBasicsView(
    struct mmUIViewModel*                          p,
    struct mmEventUIViewCommandArgs*               args);

#include "core/mmSuffix.h"

#endif//__mmUIViewModel_h__
