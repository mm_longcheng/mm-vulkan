/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewEmulator_h__
#define __mmUIViewEmulator_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "vk/mmVKDescriptorLayout.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"

#include "core/mmPrefix.h"

struct mmVKCommandBuffer;

struct mmEmulatorMachine;

void
mmUIEmulator_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIEmulator_RemovePropertys(
    struct mmPropertySet*                          propertys);

struct mmUIEmulator
{
    struct mmEmulatorMachine* pMachine;
};

void
mmUIEmulator_Init(
    struct mmUIEmulator*                           p);

void
mmUIEmulator_Destroy(
    struct mmUIEmulator*                           p);

VkResult
mmUIEmulator_OnPrepare(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

void
mmUIEmulator_OnDiscard(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIEmulator_DescriptorSetUpdate(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable,
    struct mmEmulatorMachine*                      pMachine);

extern const struct mmVectorValue mmUIEmulator_DSLB0;
extern const struct mmVectorValue mmUIEmulator_DSLP;

extern const struct mmVKPipelineVertexInputInfo mmUIEmulator_PVII;

void
mmUIViewEmulator_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIViewEmulator_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorViewEmulator;
extern const struct mmUIMetadata mmUIMetadataViewEmulator;

struct mmUIViewEmulator
{
    struct mmUIView view;
    struct mmUIDrawable drawable;
    struct mmUIEmulator emulator;
};

void
mmUIViewEmulator_Init(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_Destroy(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_Produce(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_Recycle(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnPrepare(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnDiscard(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnUpdate(
    struct mmUIViewEmulator*                       p,
    struct mmEventUpdate*                          content);

void
mmUIViewEmulator_OnInvalidate(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnSized(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnChanged(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnDescriptorSetProduce(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnDescriptorSetRecycle(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnDescriptorSetUpdate(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnUpdatePushConstants(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnUpdatePipeline(
    struct mmUIViewEmulator*                       p);

void
mmUIViewEmulator_OnRecord(
    struct mmUIViewEmulator*                       p,
    struct mmVKCommandBuffer*                      cmd);

#include "core/mmSuffix.h"

#endif//__mmUIViewEmulator_h__
