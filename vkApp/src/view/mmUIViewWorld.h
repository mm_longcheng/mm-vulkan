/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewWorld_h__
#define __mmUIViewWorld_h__

#include "core/mmCore.h"

#include "container/mmRbtsetVpt.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "emulatorX/mmEmulatorMachine.h"

#include "core/mmPrefix.h"

struct mmALSource;

// Type base define.
mmUIViewTypeDefine(World);

struct mmUIViewWorld
{
    struct mmUIView view;
    
    /* UIView weak reference. */
    struct mmUIView* MyButton;
    struct mmUIView* MyEmulator;
    
    struct mmEmulatorMachine hMachine;
    
    struct mmALSource* pALSource;
};

void
mmUIViewWorld_Init(
    struct mmUIViewWorld*                          p);

void
mmUIViewWorld_Destroy(
    struct mmUIViewWorld*                          p);

void
mmUIViewWorld_OnPrepare(
    struct mmUIViewWorld*                          p);

void
mmUIViewWorld_OnDiscard(
    struct mmUIViewWorld*                          p);

/* special */

void
mmUIViewWorld_OnEventClicked(
    struct mmUIViewWorld*                          p,
    struct mmEventUIClickedArgs*                   args);

void
mmUIViewWorld_OnEventUpdate(
    struct mmUIViewWorld*                          p,
    struct mmEventUpdateArgs*                      args);

void
mmUIViewWorld_OnEventKeypadPressed(
    struct mmUIViewWorld*                         p,
    struct mmEventKeypadArgs*                     args);

void
mmUIViewWorld_OnEventKeypadRelease(
    struct mmUIViewWorld*                         p,
    struct mmEventKeypadArgs*                     args);

void
mmUIViewWorld_OnEventEnterBackground(
    struct mmUIViewWorld*                         p,
    struct mmEventArgs*                           args);

void
mmUIViewWorld_OnEventEnterForeground(
    struct mmUIViewWorld*                         p,
    struct mmEventArgs*                           args);

#include "core/mmSuffix.h"

#endif//__mmUIViewWorld_h__
