/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCYear.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIViewLoader.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

#include "math/mmVector2.h"

#include "sxwnl/mmSxwnlLunar.h"

#include "view/mmUIViewCYearTerm.h"

#include "data/mmUIDataCalendar.h"

// Type base implement.
mmUIViewTypeImplement(CYear);

const char* mmUIViewCYear_EventSelected = "EventSelected";

void
mmUIViewCYear_Init(
    struct mmUIViewCYear*                          p)
{
    mmUIView_Init(&p->view);
    p->vY = -1; p->vM = -1; p->vD = -1;

    p->pCalendar = NULL;

    mmUIViewCYear_ResetReference(p);

    mmMemset(p->Term, 0, sizeof(p->Term));

    p->pSelect = NULL;
}

void
mmUIViewCYear_Destroy(
    struct mmUIViewCYear*                          p)
{
    p->pSelect = NULL;

    mmMemset(p->Term, 0, sizeof(p->Term));

    mmUIViewCYear_ResetReference(p);

    p->pCalendar = NULL;

    p->vY = -1; p->vM = -1; p->vD = -1;
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCYear_SetCalendar(
    struct mmUIViewCYear*                          p,
    struct mmUIDataCalendar*                       calendar)
{
    p->pCalendar = calendar;
}

void
mmUIViewCYear_OnPrepare(
    struct mmUIViewCYear*                          p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCYear.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");

    p->MyImage_1 = mmUIView_GetChildAssert(&p->view, "MyImage_1");
    p->MyText_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_00");
    p->MyButton_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyButton_00");
    p->MyText_01 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_01");

    p->MyImage_2 = mmUIView_GetChildAssert(&p->view, "MyImage_2");

    mmEventSet_SubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCYear_OnEventTodayClicked,
        p);

    mmUIView_PrepareChildren(&p->view);

    mmUIViewCYear_PrepareDays(p);

    mmUIViewCYear_UpdateContent(p);
}

void
mmUIViewCYear_OnDiscard(
    struct mmUIViewCYear*                          p)
{
    mmUIViewCYear_DiscardDays(p);

    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCYear_OnEventTodayClicked,
        p);
    
    mmUIViewCYear_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCYear_ResetReference(
    struct mmUIViewCYear*                          p)
{
    p->MyImage_0 = NULL;

    p->MyImage_1 = NULL;
    p->MyText_00 = NULL;
    p->MyButton_00 = NULL;
    p->MyText_01 = NULL;

    p->MyImage_2 = NULL;
}

void
mmUIViewCYear_PrepareDays(
    struct mmUIViewCYear*                          p)
{
    int i, j;
    float hTermSize[2];
    float hTermPosition[2];
    float LocalSize[3];
    struct mmUIDim uPosition[3];
    struct mmUIView* w;
    struct mmUIViewContext* pViewContext = p->view.context;
    struct mmUIViewLoader* pViewLoader = pViewContext->vloader;

    mmUIView_GetLocalSize(p->MyImage_2, LocalSize);
    hTermSize[0] = LocalSize[0] / 2.0f;
    hTermSize[1] = LocalSize[1] / 12.0f;
    mmVec2MulK(hTermPosition, hTermSize, 0.5f);

    for (i = 0; i < 12; i++)
    {
        for (j = 0; j < 2; j++)
        {
            w = mmUIViewLoader_ProduceViewType(
                pViewLoader,
                pViewContext,
                "mmUIViewCYearTerm",
                "");

            uPosition[0].o = hTermPosition[0] + hTermSize[0] * j;
            uPosition[0].s = 0.0f;
            uPosition[1].o = hTermPosition[1] + hTermSize[1] * i;
            uPosition[1].s = 0.0f;
            uPosition[2].o = 0.0f;
            uPosition[2].s = 0.0f;

            mmUIView_SetPosition(w, uPosition);
            mmUIView_SetContext(w, pViewContext);

            mmEventSet_SubscribeEvent(
                &w->events,
                mmUIViewCYearTerm_EventSelected,
                &mmUIViewCYear_OnEventSelected,
                p);

            mmUIView_AddChild(p->MyImage_2, w);

            mmUIView_Prepare(w);
            p->Term[i][j] = w;
        }
    }
}

void
mmUIViewCYear_DiscardDays(
    struct mmUIViewCYear*                          p)
{
    int i, j;

    struct mmUIView* w;
    struct mmUIViewContext* pViewContext = p->view.context;
    struct mmUIViewLoader* pViewLoader = pViewContext->vloader;

    for (i = 0; i < 12; i++)
    {
        for (j = 0; j < 2; j++)
        {
            w = p->Term[i][j];
            p->Term[i][j] = NULL;
            mmUIView_Discard(w);

            mmUIView_RmvChild(p->MyImage_2, w);

            mmEventSet_UnSubscribeEvent(
                &w->events,
                mmUIViewCYearTerm_EventSelected,
                &mmUIViewCYear_OnEventSelected,
                p);

            mmUIViewLoader_RecycleViewType(
                pViewLoader,
                w);
        }
    }
}

void
mmUIViewCYear_UpdateContentTitle(
    struct mmUIViewCYear*                          p)
{
    char s[32];

    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    mmUIDataCalendarTimeCYMD(pCalendar->sY, pCalendar->sM, pCalendar->sD, s);
    mmUIView_SetValueCStr(p->MyText_00, "Text.String", s);
    mmUIView_Changed(p->MyText_00);
}

void
mmUIViewCYear_UpdateContentTerm(
    struct mmUIViewCYear*                          p)
{
    int i, j;
    int idx;

    struct mmUIView* w;
    struct mmUIViewCYearTerm* u;
    struct mmUIDataCalendar* pCalendar;
    struct csJD* hJDs;
    int* hJQs;

    pCalendar = p->pCalendar;
    hJDs = pCalendar->hJDs;
    hJQs = pCalendar->hJQs;

    for (i = 0; i < 12; i++)
    {
        for (j = 0; j < 2; j++)
        {
            w = p->Term[i][j];
            u = (struct mmUIViewCYearTerm*)(w);
            idx = i * 2 + j;
            mmUIViewCYearTerm_SetData(u, &hJDs[idx], hJQs[idx]);
            mmUIViewCYearTerm_UpdateContent(u);
            mmUIViewCYearTerm_UpdateGeneral(u, pCalendar->tJQ, pCalendar->sJQ);
        }
    }
}

void
mmUIViewCYear_UpdateSelectTerm(
    struct mmUIViewCYear*                          p)
{
    int i, j;
    int sJQ;
    int idx;
    struct mmUIView* w;
    struct mmUIViewCYearTerm* u;

    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;
    sJQ = pCalendar->sJQ;

    if (NULL != p->pSelect)
    {
        w = p->pSelect;
        u = (struct mmUIViewCYearTerm*)(w);
        mmUIViewCYearTerm_UpdateSelect(u, 0);
        p->pSelect = NULL;
    }

    idx = mmUIDataCalendar_GetJQIndex(pCalendar, sJQ);

    if (24 == idx)
    {
        p->pSelect = NULL;
    }
    else
    {
        i = idx / 2;
        j = idx - i * 2;
        p->pSelect = p->Term[i][j];
    }

    if (NULL != p->pSelect)
    {
        w = p->pSelect;
        u = (struct mmUIViewCYearTerm*)(w);
        mmUIViewCYearTerm_UpdateSelect(u, 1);
    }
}

void
mmUIViewCYear_UpdateContent(
    struct mmUIViewCYear*                          p)
{
    int hDifferent;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    hDifferent = mmUIDataCalendarGetTwoDateDifferent(
        pCalendar->sY,
        pCalendar->sM,
        pCalendar->sD,
        p->vY,
        p->vM,
        p->vD);

    p->vY = pCalendar->sY;
    p->vM = pCalendar->sM;
    p->vD = pCalendar->sD;

    switch (hDifferent)
    {
    case 0: // 时间一样
        break;
    case 1: // 不同日
    case 2: // 不同月
        mmUIViewCYear_UpdateContentTitle(p);
        mmUIViewCYear_UpdateSelectTerm(p);
        break;
    case 3: // 不同年
        mmUIViewCYear_UpdateContentTitle(p);
        mmUIViewCYear_UpdateContentTerm(p);
        mmUIViewCYear_UpdateSelectTerm(p);
        break;
    default:
        break;
    }
}

void
mmUIViewCYear_UpdateTime(
    struct mmUIViewCYear*                          p,
    int                                            change)
{
    if (2 == change)
    {
        mmUIViewCYear_UpdateContent(p);
    }
}

void
mmUIViewCYear_OnEventSelected(
    struct mmUIViewCYear*                          p,
    struct mmEventUIViewArgs*                      args)
{
    struct mmEventUIViewArgs hArgs;
    struct mmUIViewCYearTerm* u;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    u = (struct mmUIViewCYearTerm*)(args->pView);

    pCalendar->hY = u->hJD.Y;
    pCalendar->hM = u->hJD.M;

    pCalendar->sY = u->hJD.Y;
    pCalendar->sM = u->hJD.M;
    pCalendar->sD = u->hJD.D;

    mmUIDataCalendar_UpdateLun(pCalendar);
    mmUIDataCalendar_UpdateYearJQ(pCalendar);
    mmUIDataCalendar_UpdateSQH(pCalendar);

    mmUIViewCYear_UpdateContent(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCYear_EventSelected,
        &hArgs);
}

void
mmUIViewCYear_OnEventTodayClicked(
    struct mmUIViewCYear*                          p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;

    mmUIDataCalendar_UpdateToday(p->pCalendar);
    mmUIDataCalendar_UpdateLun(p->pCalendar);
    mmUIDataCalendar_UpdateYearJQ(p->pCalendar);
    mmUIViewCYear_UpdateContent(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCYear_EventSelected,
        &hArgs);
}