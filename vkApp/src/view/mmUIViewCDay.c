/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCDay.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmLogger.h"
#include "core/mmValueTransform.h"
#include "core/mmTime.h"

#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"
#include "sxwnl/mmSxwnlEph0.h"
#include "sxwnl/mmSxwnlConst.h"
#include "sxwnl/mmSxwnlJD.h"
#include "sxwnl/mmSxwnlSheet.h"

#include "data/mmUIDataCalendar.h"

#include "vk/mmVKAssets.h"

// Type base implement.
mmUIViewTypeImplement(CDay);

const char* mmUIViewCDay_EventSelected = "EventSelected";

void
mmUIViewCDay_Init(
    struct mmUIViewCDay*                           p)
{
    mmUIView_Init(&p->view);
    p->vY = -1; p->vM = -1; p->vD = -1;

    p->pCalendar = NULL;

    mmUIViewCDay_ResetReference(p);
}

void
mmUIViewCDay_Destroy(
    struct mmUIViewCDay*                           p)
{
    mmUIViewCDay_ResetReference(p);

    p->pCalendar = NULL;

    p->vY = -1; p->vM = -1; p->vD = -1;
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCDay_SetCalendar(
    struct mmUIViewCDay*                           p,
    struct mmUIDataCalendar*                       calendar)
{
    p->pCalendar = calendar;
}

void
mmUIViewCDay_OnPrepare(
    struct mmUIViewCDay*                           p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCDay.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");

    p->MyImage_1 = mmUIView_GetChildAssert(&p->view, "MyImage_1");
    p->MyText_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_00");
    p->MyButton_00 = mmUIView_GetChildAssert(p->MyImage_1, "MyButton_00");
    p->MyText_01 = mmUIView_GetChildAssert(p->MyImage_1, "MyText_01");

    p->MyText_0 = mmUIView_GetChildAssert(&p->view, "MyText_0");
    p->MyText_1 = mmUIView_GetChildAssert(&p->view, "MyText_1");
    p->MyText_2 = mmUIView_GetChildAssert(&p->view, "MyText_2");
    p->MyText_3 = mmUIView_GetChildAssert(&p->view, "MyText_3");
    p->MyText_4 = mmUIView_GetChildAssert(&p->view, "MyText_4");
    p->MyText_5 = mmUIView_GetChildAssert(&p->view, "MyText_5");
    p->MyText_6 = mmUIView_GetChildAssert(&p->view, "MyText_6");
    p->MyText_7 = mmUIView_GetChildAssert(&p->view, "MyText_7");

    mmEventSet_SubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCDay_OnEventTodayClicked,
        p);

    mmUIView_PrepareChildren(&p->view);

    mmUIViewCDay_UpdateContent(p);
}

void
mmUIViewCDay_OnDiscard(
    struct mmUIViewCDay*                           p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_00->events,
        mmUIView_EventClicked,
        &mmUIViewCDay_OnEventTodayClicked,
        p);
    
    mmUIViewCDay_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCDay_ResetReference(
    struct mmUIViewCDay*                           p)
{
    p->MyImage_0 = NULL;

    p->MyImage_1 = NULL;
    p->MyText_00 = NULL;
    p->MyButton_00 = NULL;
    p->MyText_01 = NULL;

    p->MyText_0 = NULL;
    p->MyText_1 = NULL;
    p->MyText_2 = NULL;
    p->MyText_3 = NULL;
    p->MyText_4 = NULL;
    p->MyText_5 = NULL;
    p->MyText_6 = NULL;
    p->MyText_7 = NULL;
}

void
mmUIViewCDay_UpdateContentTitle(
    struct mmUIViewCDay*                           p)
{
    struct mmUIDataCalendar* pCalendar;
    const struct csLunarOBDay* pDay;

    pCalendar = p->pCalendar;
    pDay = mmUIDataCalendar_GetSelectToday(pCalendar);
    if (NULL != pDay)
    {
        char s[32];

        mmUIDataCalendarTimeNY(pDay->Lyear4, s);
        mmUIView_SetValueCStr(p->MyText_00, "Text.String", s);
        mmUIView_Changed(p->MyText_00);
    }
}

void
mmUIViewCDay_UpdateContentDay(
    struct mmUIViewCDay*                           p)
{
    struct mmUIDataCalendar* pCalendar;
    const struct csLunarOBDay* pDay;

    pCalendar = p->pCalendar;
    pDay = mmUIDataCalendar_GetSelectToday(pCalendar);
    if (NULL != pDay)
    {
        static const char* cTextColor[] =
        {
            "Text-D",
            "Text-2",
        };

        char s[32];
        char ymd[32];
        char week[16];
        char poetry[256];

        const char** rows;
        const char* pColor;

        mmUIDataCalendarTimeCYMD(pDay->y, pDay->m, pDay->d, ymd);
        mmUIView_SetValueCStr(p->MyText_0, "Text.String", ymd);
        mmUIView_Changed(p->MyText_0);

        mmUIDataCalendarTimeWeek(pDay->week, week);
        mmUIView_SetValueCStr(p->MyText_1, "Text.String", week);
        mmUIView_Changed(p->MyText_1);

        mmUIDataCalendarTimeNMD(pDay->Lmi, pDay->Ldi, ymd);
        mmUIView_SetValueCStr(p->MyText_2, "Text.String", ymd);
        mmUIView_Changed(p->MyText_2);

        pColor = (-1 == pDay->Ljqid) ? cTextColor[1] : cTextColor[0];
        sprintf(s, "%s%s%s", csLunarOBB_jqmc[pCalendar->sJQ], "丨", csLunarOBB_qhmc[pCalendar->sHI]);
        mmUIView_SetValueCStr(p->MyText_3, "Text.String", s);
        mmUIView_SetValueCStr(p->MyText_3, "Text.ForegroundColor", pColor);
        mmUIView_Changed(p->MyText_3);

        sprintf(s, "%d", pCalendar->sJQ);
        rows = mmParseTSV_GetRowByPrimaryKey(&pCalendar->tsv, s);
        mmUIView_SetValueCStr(p->MyText_4, "Text.String", rows[2 + pCalendar->sHI]);
        mmUIView_Changed(p->MyText_4);

        mmUIDataCalendarGetPoetry(poetry, rows);
        mmUIView_SetValueCStr(p->MyText_7, "Text.String", poetry);
        mmUIView_Changed(p->MyText_7);
    }
}

void
mmUIViewCDay_UpdateContentTime(
    struct mmUIViewCDay*                           p)
{
    char s[32];
    char mlbz[64];

    int hIsToday;
    struct tm tm;

    hIsToday = mmUIDataCalendar_GetSelectIsToday(p->pCalendar, &tm);

    mmUIDataCalendarTimeMLBZ(&tm, mlbz);
    mmUIView_SetValueCStr(p->MyText_5, "Text.String", mlbz);
    mmUIView_SetVisible(p->MyText_5, hIsToday);
    mmUIView_Changed(p->MyText_5);

    mmUIDataCalendarTimeClock(&tm, s);
    mmUIView_SetValueCStr(p->MyText_6, "Text.String", s);
    mmUIView_SetVisible(p->MyText_6, hIsToday);
    mmUIView_Changed(p->MyText_6);
}

void
mmUIViewCDay_UpdateContent(
    struct mmUIViewCDay*                           p)
{
    int hDifferent;
    struct mmUIDataCalendar* pCalendar;

    pCalendar = p->pCalendar;

    hDifferent = mmUIDataCalendarGetTwoDateDifferent(
        pCalendar->sY,
        pCalendar->sM,
        pCalendar->sD,
        p->vY,
        p->vM,
        p->vD);

    p->vY = pCalendar->sY;
    p->vM = pCalendar->sM;
    p->vD = pCalendar->sD;

    switch (hDifferent)
    {
    case 0: // 时间一样
        break;
    case 1: // 不同日
    case 2: // 不同月
    case 3: // 不同年
        mmUIViewCDay_UpdateContentTitle(p);
        mmUIViewCDay_UpdateContentDay(p);
        mmUIViewCDay_UpdateContentTime(p);
        break;
    default:
        break;
    }
}

void
mmUIViewCDay_UpdateTime(
    struct mmUIViewCDay*                           p,
    int                                            change)
{
    switch (change)
    {
    case 0:
        break;
    case 1:
        mmUIViewCDay_UpdateContentTime(p);
        break;
    case 2:
        mmUIViewCDay_UpdateContent(p);
        break;
    default:
        break;
    }
}

void
mmUIViewCDay_OnEventTodayClicked(
    struct mmUIViewCDay*                           p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;

    mmUIDataCalendar_UpdateToday(p->pCalendar);
    mmUIDataCalendar_UpdateLun(p->pCalendar);
    mmUIDataCalendar_UpdateYearJQ(p->pCalendar);
    mmUIViewCDay_UpdateContent(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events,
        mmUIViewCDay_EventSelected,
        &hArgs);
}
