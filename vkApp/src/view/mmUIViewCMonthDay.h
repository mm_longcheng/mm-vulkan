/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCMonthDay_h__
#define __mmUIViewCMonthDay_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct csLunarOBDay;

// Type base define.
mmUIViewTypeDefine(CMonthDay);

/* struct mmEventUIViewArgs */
extern const char* mmUIViewCMonthDay_EventSelected;

struct mmUIViewCMonthDay
{
    struct mmUIView view;
    /* UIView weak reference. */
    struct mmUIView* MyImage_0;
    struct mmUIView* MyImage_1;
    struct mmUIView* MyText_0;
    struct mmUIView* MyText_1;
    struct mmUIView* MyButton_0;
    struct mmUIView* MyImage_2;
    struct mmUIView* MyImage_3;

    struct csLunarOBDay* pOBDay;
};

void
mmUIViewCMonthDay_Init(
    struct mmUIViewCMonthDay*                      p);

void
mmUIViewCMonthDay_Destroy(
    struct mmUIViewCMonthDay*                      p);

void
mmUIViewCMonthDay_SetData(
    struct mmUIViewCMonthDay*                      p,
    struct csLunarOBDay*                           pOBDay);

void
mmUIViewCMonthDay_OnPrepare(
    struct mmUIViewCMonthDay*                      p);

void
mmUIViewCMonthDay_OnDiscard(
    struct mmUIViewCMonthDay*                      p);

/* special */

void
mmUIViewCMonthDay_ResetReference(
    struct mmUIViewCMonthDay*                      p);

void
mmUIViewCMonthDay_UpdateGeneral(
    struct mmUIViewCMonthDay*                      p,
    int                                            hTodayJDi);

void
mmUIViewCMonthDay_UpdateToday(
    struct mmUIViewCMonthDay*                      p,
    int                                            hValue);

void
mmUIViewCMonthDay_UpdateSelect(
    struct mmUIViewCMonthDay*                      p,
    int                                            hValue);

void
mmUIViewCMonthDay_UpdateContent(
    struct mmUIViewCMonthDay*                      p);

void
mmUIViewCMonthDay_OnEventClicked(
    struct mmUIViewCMonthDay*                      p,
    struct mmEventUIClickedArgs*                   args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCMonthDay_h__
