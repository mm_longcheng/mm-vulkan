/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCYearTerm.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"

// Type base implement.
mmUIViewTypeImplement(CYearTerm);

const char* mmUIViewCYearTerm_EventSelected = "EventSelected";

void
mmUIViewCYearTerm_Init(
    struct mmUIViewCYearTerm*                      p)
{
    mmUIView_Init(&p->view);
    mmMemset(&p->hJD, 0, sizeof(struct csJD));
    p->hJQ = -1;

    mmUIViewCYearTerm_ResetReference(p);
}

void
mmUIViewCYearTerm_Destroy(
    struct mmUIViewCYearTerm*                      p)
{
    mmUIViewCYearTerm_ResetReference(p);

    p->hJQ = -1;
    mmMemset(&p->hJD, 0, sizeof(struct csJD));
    mmUIView_Destroy(&p->view);
}

void
mmUIViewCYearTerm_SetData(
    struct mmUIViewCYearTerm*                      p,
    struct csJD*                                   pJD,
    int                                            hJQ)
{
    p->hJD = (*pJD);
    p->hJQ = hJQ;
}

void
mmUIViewCYearTerm_OnPrepare(
    struct mmUIViewCYearTerm*                      p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCYearTerm.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");
    p->MyImage_1 = mmUIView_GetChildAssert(&p->view, "MyImage_1");
    p->MyImage_2 = mmUIView_GetChildAssert(&p->view, "MyImage_2");
    p->MyImage_3 = mmUIView_GetChildAssert(&p->view, "MyImage_3");
    p->MyButton_0 = mmUIView_GetChildAssert(&p->view, "MyButton_0");
    p->MyText_0 = mmUIView_GetChildAssert(&p->view, "MyText_0");

    mmEventSet_SubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCYearTerm_OnEventClicked,
        p);

    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewCYearTerm_OnDiscard(
    struct mmUIViewCYearTerm*                      p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCYearTerm_OnEventClicked,
        p);
    
    mmUIViewCYearTerm_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCYearTerm_ResetReference(
    struct mmUIViewCYearTerm*                      p)
{
    p->MyImage_0 = NULL;
    p->MyImage_1 = NULL;
    p->MyImage_2 = NULL;
    p->MyImage_3 = NULL;
    p->MyButton_0 = NULL;
    p->MyText_0 = NULL;
}

void
mmUIViewCYearTerm_UpdateGeneral(
    struct mmUIViewCYearTerm*                      p,
    int                                            tJQ,
    int                                            sJQ)
{
    mmUIView_SetVisible(p->MyImage_2, tJQ == p->hJQ);
    mmUIView_SetVisible(p->MyImage_3, sJQ == p->hJQ);
}

void
mmUIViewCYearTerm_UpdateToday(
    struct mmUIViewCYearTerm*                      p,
    int                                            hValue)
{
    mmUIView_SetVisible(p->MyImage_2, hValue);
}

void
mmUIViewCYearTerm_UpdateSelect(
    struct mmUIViewCYearTerm*                      p,
    int                                            hValue)
{
    mmUIView_SetVisible(p->MyImage_3, hValue);
}

void
mmUIViewCYearTerm_UpdateContent(
    struct mmUIViewCYearTerm*                      p)
{
    if (-1 != p->hJQ)
    {
        char s[32];
        char str[32];
        csJD_DD2str(&p->hJD, str);
        sprintf(s, "%s %s", str, csLunarOBB_jqmc[p->hJQ]);
        mmUIView_SetValueCStr(p->MyText_0, "Text.String", s);
        mmUIView_Changed(p->MyText_0);
    }
    else
    {
        mmUIView_SetValueCStr(p->MyText_0, "Text.String", "");
        mmUIView_Changed(p->MyText_0);

        mmUIView_SetVisible(p->MyImage_2, 0);
        mmUIView_SetVisible(p->MyImage_3, 0);
    }
}

void
mmUIViewCYearTerm_OnEventClicked(
    struct mmUIViewCYearTerm*                      p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events, 
        mmUIViewCYearTerm_EventSelected, 
        &hArgs);
}
