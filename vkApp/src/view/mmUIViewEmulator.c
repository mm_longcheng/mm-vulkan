/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewEmulator.h"

#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKCommandBuffer.h"

#include "ui/mmUIViewContext.h"

#include "emu/mmEmuScreen.h"
#include "emu/mmEmuPalette.h"

#include "emulatorX/mmEmulatorMachine.h"

static const VkDescriptorSetLayoutBinding dslb_bindings0[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 2,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
};

static const VkPushConstantRange dsl_pushs[] =
{
    {
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(struct mmUIDrawablePushConstant),
    },
};

const struct mmVectorValue mmUIEmulator_DSLB0 = mmVectorValue_Make(dslb_bindings0, VkDescriptorSetLayoutBinding);
const struct mmVectorValue mmUIEmulator_DSLP = mmVectorValue_Make(dsl_pushs, VkPushConstantRange);

static const VkVertexInputBindingDescription vibd[] =
{
    {
        .binding = 0,
        .stride = sizeof(struct mmUIVertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    },
};
static const VkVertexInputAttributeDescription viad[] =
{
    {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, position),
    },
    {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, texcoord),
    },
    {
        .location = 2,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, color),
    },
};

const struct mmVKPipelineVertexInputInfo mmUIEmulator_PVII =
{
    mmVectorValue_Make(vibd, VkVertexInputBindingDescription),
    mmVectorValue_Make(viad, VkVertexInputAttributeDescription),
};

void
mmUIEmulator_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIEmulator";

    mmPropertyDefineMutable(propertys,
        "Emulator.Machine", offset, struct mmUIEmulator,
        pMachine, Pointer, "1",
        NULL,
        NULL,
        cOrigin,
        "Emulator Machine void* default NULL.");
}

void
mmUIEmulator_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmPropertyRemove(propertys, "Emulator.Machine");
}

void
mmUIEmulator_Init(
    struct mmUIEmulator*                           p)
{
    p->pMachine = NULL;
}

void
mmUIEmulator_Destroy(
    struct mmUIEmulator*                           p)
{
    p->pMachine = NULL;
}

VkResult
mmUIEmulator_OnPrepare(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        size_t imageSize[2];
        int frame[4] = { 0, 0, MM_EMU_SCREEN_RENDER_W, MM_EMU_SCREEN_RENDER_H };
        int rotated = 0;

        struct mmUIViewContext* pViewContext = NULL;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pViewContext = pView->context;

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        imageSize[0] = MM_EMU_SCREEN_RENDER_W;
        imageSize[1] = MM_EMU_SCREEN_RENDER_H;
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, rotated);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePipeline(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePool(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmUIEmulator_OnDiscard(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmUIViewContext* pViewContext = NULL;
        struct mmVKAssets* pAssets = NULL;

        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pViewContext = pView->context;

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAssets = pViewContext->assets;

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmUIDrawable_DiscardPool(pDrawable);
        mmUIDrawable_DiscardPipeline(pDrawable);
        mmUIDrawable_DiscardBuffer(pDrawable, pUploader);
    } while (0);
}

VkResult
mmUIEmulator_DescriptorSetUpdate(
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable,
    struct mmEmulatorMachine*                      pMachine)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        struct mmVKBuffer* ubo = NULL;

        VkDescriptorImageInfo hDescriptorImageInfo[2];
        VkWriteDescriptorSet writes[3];

        VkDescriptorBufferInfo hDescriptorBufferInfo[1];

        struct mmUIViewContext* pViewContext = NULL;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;
        
        struct mmEmulatorFrame* pEmulatorFrame = NULL;

        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        pViewContext = pView->context;

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        if (NULL == pMachine)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pMachine is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pEmulatorFrame = &pMachine->hFrame;
        if (NULL == pEmulatorFrame)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pEmulatorFrame is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        ubo = &pViewContext->ubo;

        descriptorSet = pDrawable->vDescriptorSet.descriptorSet;

        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmUIDrawableUBOVS);
        hDescriptorBufferInfo[0].buffer = ubo->buffer;

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        hDescriptorImageInfo[0].sampler = pEmulatorFrame->sampler;
        hDescriptorImageInfo[0].imageView = pEmulatorFrame->vTexel[0].imageView;
        hDescriptorImageInfo[0].imageLayout = pEmulatorFrame->vTexel[0].vImage.imageLayout;
        
        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 1;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[1].pImageInfo = &hDescriptorImageInfo[0];
        writes[1].pBufferInfo = NULL;
        writes[1].pTexelBufferView = NULL;

        hDescriptorImageInfo[1].sampler = pEmulatorFrame->sampler;
        hDescriptorImageInfo[1].imageView = pEmulatorFrame->vTexel[1].imageView;
        hDescriptorImageInfo[1].imageLayout = pEmulatorFrame->vTexel[1].vImage.imageLayout;
        
        writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[2].pNext = NULL;
        writes[2].dstSet = descriptorSet;
        writes[2].dstBinding = 2;
        writes[2].dstArrayElement = 0;
        writes[2].descriptorCount = 1;
        writes[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[2].pImageInfo = &hDescriptorImageInfo[1];
        writes[2].pBufferInfo = NULL;
        writes[2].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 3, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmUIViewEmulator_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEmulator, view));
    mmUIDrawable_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEmulator, drawable));
    mmUIEmulator_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEmulator, emulator));
}

void
mmUIViewEmulator_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIEmulator_RemovePropertys(propertys);
    mmUIDrawable_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewEmulator =
{
    &mmUIViewEmulator_OnPrepare,
    &mmUIViewEmulator_OnDiscard,
};

const struct mmUIWidgetVariable mmUIWidgetVariableViewEmulator =
{
    &mmUIViewEmulator_OnUpdate,
    &mmUIViewEmulator_OnInvalidate,
    &mmUIViewEmulator_OnSized,
    &mmUIViewEmulator_OnChanged,
};

const struct mmUIWidgetDrawable mmUIWidgetDrawableViewEmulator =
{
    &mmUIViewEmulator_OnDescriptorSetProduce,
    &mmUIViewEmulator_OnDescriptorSetRecycle,
    &mmUIViewEmulator_OnDescriptorSetUpdate,
    &mmUIViewEmulator_OnUpdatePushConstants,
    &mmUIViewEmulator_OnUpdatePipeline,
    &mmUIViewEmulator_OnRecord,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewEmulator =
{
    "mmUIViewEmulator",
    sizeof(struct mmUIViewEmulator),
    &mmUIViewEmulator_Produce,
    &mmUIViewEmulator_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewEmulator =
{
    &mmUIMetaAllocatorViewEmulator,
    /*- Widget -*/
    &mmUIWidgetOperableViewEmulator,
    &mmUIWidgetVariableViewEmulator,
    &mmUIWidgetDrawableViewEmulator,
    /*- Responder -*/
    NULL,
    NULL,
    NULL,
    NULL,
};

void
mmUIViewEmulator_Init(
    struct mmUIViewEmulator*                       p)
{
    mmUIView_Init(&p->view);
    mmUIDrawable_Init(&p->drawable);
    mmUIEmulator_Init(&p->emulator);
}

void
mmUIViewEmulator_Destroy(
    struct mmUIViewEmulator*                       p)
{
    mmUIEmulator_Destroy(&p->emulator);
    mmUIDrawable_Destroy(&p->drawable);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewEmulator_Produce(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_Init(p);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewEmulator;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);

    mmUIViewEmulator_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewEmulator_Recycle(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_RemovePropertys(&p->view.propertys);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewEmulator;
    
    mmUIViewEmulator_Destroy(p);
}

void
mmUIViewEmulator_OnPrepare(
    struct mmUIViewEmulator*                       p)
{
    mmUIEmulator_OnPrepare(&p->view, &p->drawable);

    mmUIViewEmulator_OnDescriptorSetProduce(p);
    mmUIViewEmulator_OnDescriptorSetUpdate(p);
    mmUIViewEmulator_OnUpdatePushConstants(p);
}

void
mmUIViewEmulator_OnDiscard(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_OnDescriptorSetRecycle(p);

    mmUIEmulator_OnDiscard(&p->view, &p->drawable);
}

void
mmUIViewEmulator_OnUpdate(
    struct mmUIViewEmulator*                       p,
    struct mmEventUpdate*                          content)
{
    // do nothing here, interface api.
}

void
mmUIViewEmulator_OnInvalidate(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_OnUpdatePushConstants(p);
}

void
mmUIViewEmulator_OnSized(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_OnUpdatePushConstants(p);
}

void
mmUIViewEmulator_OnChanged(
    struct mmUIViewEmulator*                       p)
{
    mmUIViewEmulator_OnUpdatePushConstants(p);
}

void
mmUIViewEmulator_OnDescriptorSetProduce(
    struct mmUIViewEmulator*                       p)
{
    mmUIDrawable_DescriptorSetProduce(&p->drawable);
}

void
mmUIViewEmulator_OnDescriptorSetRecycle(
    struct mmUIViewEmulator*                       p)
{
    mmUIDrawable_DescriptorSetRecycle(&p->drawable);
}

void
mmUIViewEmulator_OnDescriptorSetUpdate(
    struct mmUIViewEmulator*                       p)
{
    mmUIEmulator_DescriptorSetUpdate(
        &p->view,
        &p->drawable,
        p->emulator.pMachine);
}

void
mmUIViewEmulator_OnUpdatePushConstants(
    struct mmUIViewEmulator*                       p)
{
    mmUIDrawable_UpdatePushConstants(&p->drawable, &p->view);
}

void
mmUIViewEmulator_OnUpdatePipeline(
    struct mmUIViewEmulator*                       p)
{
    mmUIDrawable_UpdatePool(&p->drawable, &p->view);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void
mmUIViewEmulator_OnRecord(
    struct mmUIViewEmulator*                       p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIDrawable_OnRecord(&p->drawable, cmd->cmdBuffer);
    mmUIView_OnRecordChildren(&p->view, cmd);
}
