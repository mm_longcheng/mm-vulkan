/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewCMonthDay.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"

// Type base implement.
mmUIViewTypeImplement(CMonthDay);

const char* mmUIViewCMonthDay_EventSelected = "EventSelected";

void
mmUIViewCMonthDay_Init(
    struct mmUIViewCMonthDay*                      p)
{
    mmUIView_Init(&p->view);

    mmUIViewCMonthDay_ResetReference(p);

    p->pOBDay = NULL;
}

void
mmUIViewCMonthDay_Destroy(
    struct mmUIViewCMonthDay*                      p)
{
    p->pOBDay = NULL;

    mmUIViewCMonthDay_ResetReference(p);

    mmUIView_Destroy(&p->view);
}

void
mmUIViewCMonthDay_SetData(
    struct mmUIViewCMonthDay*                      p,
    struct csLunarOBDay*                           pOBDay)
{
    p->pOBDay = pOBDay;
}

void
mmUIViewCMonthDay_OnPrepare(
    struct mmUIViewCMonthDay*                      p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewCMonthDay.view.json");

    p->MyImage_0 = mmUIView_GetChildAssert(&p->view, "MyImage_0");
    p->MyImage_1 = mmUIView_GetChildAssert(&p->view, "MyImage_1");
    p->MyText_0 = mmUIView_GetChildAssert(&p->view, "MyText_0");
    p->MyText_1 = mmUIView_GetChildAssert(&p->view, "MyText_1");
    p->MyButton_0 = mmUIView_GetChildAssert(&p->view, "MyButton_0");
    p->MyImage_2 = mmUIView_GetChildAssert(&p->view, "MyImage_2");
    p->MyImage_3 = mmUIView_GetChildAssert(&p->view, "MyImage_3");

    mmEventSet_SubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCMonthDay_OnEventClicked,
        p);

    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewCMonthDay_OnDiscard(
    struct mmUIViewCMonthDay*                      p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton_0->events,
        mmUIView_EventClicked,
        &mmUIViewCMonthDay_OnEventClicked,
        p);
    
    mmUIViewCMonthDay_ResetReference(p);

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewCMonthDay_ResetReference(
    struct mmUIViewCMonthDay*                      p)
{
    p->MyImage_0 = NULL;
    p->MyImage_1 = NULL;
    p->MyText_0 = NULL;
    p->MyText_1 = NULL;
    p->MyButton_0 = NULL;
    p->MyImage_2 = NULL;
    p->MyImage_3 = NULL;
}

void
mmUIViewCMonthDay_UpdateGeneral(
    struct mmUIViewCMonthDay*                      p,
    int                                            hTodayJDi)
{
    mmUIView_SetVisible(p->MyImage_2, hTodayJDi == p->pOBDay->d0);
    mmUIView_SetVisible(p->MyImage_3, 0);
}

void
mmUIViewCMonthDay_UpdateToday(
    struct mmUIViewCMonthDay*                      p,
    int                                            hValue)
{
    mmUIView_SetVisible(p->MyImage_2, hValue);
}

void
mmUIViewCMonthDay_UpdateSelect(
    struct mmUIViewCMonthDay*                      p,
    int                                            hValue)
{
    mmUIView_SetVisible(p->MyImage_3, hValue);
}

void
mmUIViewCMonthDay_UpdateContent(
    struct mmUIViewCMonthDay*                      p)
{
    if (NULL != p->pOBDay)
    {
        static const char* cBackgroundColor[] =
        {
            "Background-0",
            "Background-E",
        };
        static const char* cText0Color[] =
        {
            "Text-D",
            "Text-0",
        };
        static const char* cText1Color[] =
        {
            "Text-E",
            "Text-1",
        };

        struct csLunarOBDay* pDay;

        int hIsWeekend;
        int hNotFVT;
        char hDay0[4];
        const char* hDay1;
        const char* pColor;

        pDay = p->pOBDay;

        hIsWeekend = (0 == pDay->week || 6 == pDay->week);
        pColor = hIsWeekend ? cBackgroundColor[1] : cBackgroundColor[0];
        mmUIView_SetValueCStr(p->MyImage_0, "Drawable.Color", pColor);
        mmUIView_Changed(p->MyImage_0);
        
        pColor = (-1 == pDay->Ljqid) ? cText0Color[1] : cText0Color[0];
        mmValue_SInt32ToA(&pDay->d, hDay0);
        mmUIView_SetValueCStr(p->MyText_0, "Text.String", hDay0);
        mmUIView_SetValueCStr(p->MyText_0, "Text.ForegroundColor", pColor);
        mmUIView_Changed(p->MyText_0);

        hNotFVT = mmString_Empty(&pDay->A) && mmString_Empty(&pDay->B) && mmString_Empty(&pDay->C);
        pColor = (hNotFVT) ? cText1Color[1] : cText1Color[0];
        hDay1 = csLunarOBB_rmc[p->pOBDay->Ldi];
        mmUIView_SetValueCStr(p->MyText_1, "Text.String", hDay1);
        mmUIView_SetValueCStr(p->MyText_1, "Text.ForegroundColor", pColor);
        mmUIView_Changed(p->MyText_1);
    }
    else
    {
        mmUIView_SetValueCStr(p->MyText_0, "Text.String", "");
        mmUIView_Changed(p->MyText_0);

        mmUIView_SetValueCStr(p->MyText_1, "Text.String", "");
        mmUIView_Changed(p->MyText_1);
    }
}

void
mmUIViewCMonthDay_OnEventClicked(
    struct mmUIViewCMonthDay*                      p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmEventUIViewArgs hArgs;
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(
        &p->view.events, 
        mmUIViewCMonthDay_EventSelected, 
        &hArgs);
}
