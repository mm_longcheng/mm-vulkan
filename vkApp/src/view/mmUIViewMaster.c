/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewMaster.h"

#include "core/mmLogger.h"

// Type base implement.
mmUIViewTypeImplement(Master);

void
mmUIViewMaster_Init(
    struct mmUIViewMaster*                         p)
{
    mmUIView_Init(&p->view);

    p->MyImage = NULL;
    p->MyText = NULL;
    p->MyEditBox = NULL;
    p->MyButton = NULL;
}

void
mmUIViewMaster_Destroy(
    struct mmUIViewMaster*                         p)
{
    p->MyButton = NULL;
    p->MyEditBox = NULL;
    p->MyText = NULL;
    p->MyImage = NULL;

    mmUIView_Destroy(&p->view);
}

void
mmUIViewMaster_OnPrepare(
    struct mmUIViewMaster*                         p)
{
    mmUIView_PrepareViewFromFile(&p->view, "view/mmUIViewMaster.view.json");

    p->MyImage = mmUIView_GetChildAssert(&p->view, "MyImage");
    p->MyText = mmUIView_GetChildAssert(&p->view, "MyText");
    p->MyEditBox = mmUIView_GetChildAssert(&p->view, "MyEditBox");
    p->MyButton = mmUIView_GetChildAssert(&p->view, "MyButton");

    mmEventSet_SubscribeEvent(
        &p->MyButton->events,
        mmUIView_EventClicked,
        &mmUIViewMaster_OnEventClicked,
        p);
    
    mmUIView_PrepareChildren(&p->view);
}

void
mmUIViewMaster_OnDiscard(
    struct mmUIViewMaster*                         p)
{
    mmUIView_DiscardChildren(&p->view);
    
    mmEventSet_UnSubscribeEvent(
        &p->MyButton->events,
        mmUIView_EventClicked,
        &mmUIViewMaster_OnEventClicked,
        p);
    
    p->MyButton = NULL;
    p->MyEditBox = NULL;
    p->MyText = NULL;
    p->MyImage = NULL;

    mmUIView_DiscardView(&p->view);
}

void
mmUIViewMaster_OnEventClicked(
    struct mmUIViewMaster*                         p,
    struct mmEventUIClickedArgs*                   args)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmLogger_LogI(gLogger, "%s %d", __FUNCTION__, __LINE__);
}
