/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewCalendar_h__
#define __mmUIViewCalendar_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIViewMacro.h"

#include "vk/mmVKPlatform.h"

#include "data/mmUIDataCalendar.h"

#include "core/mmPrefix.h"

struct mmEventUIClickedArgs;
struct mmEventUIViewArgs;
struct mmEventUpdateArgs;

// Type base define.
mmUIViewTypeDefine(Calendar);

struct mmUIViewCalendar
{
    struct mmUIView view;

    struct mmUIDataCalendar calendar;

    /* UIView weak reference. */
    struct mmUIView* MyDay;
    struct mmUIView* MyMonth;
    struct mmUIView* MyYear;

    struct mmUIView* MyImage_2;
    struct mmUIView* MyButton_20;
    struct mmUIView* MyButton_21;
    struct mmUIView* MyButton_22;

    struct mmUIView* MyView_3;
    struct mmUIView* MyButton_01;
    struct mmUIView* MyCEditBox_00;

    struct mmUIView* Layer[3];
    struct mmUIView* Button[3];

    int hSelect;
};

void
mmUIViewCalendar_Init(
    struct mmUIViewCalendar*                       p);

void
mmUIViewCalendar_Destroy(
    struct mmUIViewCalendar*                       p);

void
mmUIViewCalendar_OnPrepare(
    struct mmUIViewCalendar*                       p);

void
mmUIViewCalendar_OnDiscard(
    struct mmUIViewCalendar*                       p);

/* special */

void
mmUIViewCalendar_ResetReference(
    struct mmUIViewCalendar*                       p);

void
mmUIViewCalendar_UpdateSelect(
    struct mmUIViewCalendar*                       p);

void
mmUIViewCalendar_UpdateTime(
    struct mmUIViewCalendar*                       p,
    struct mmEventUpdate*                          content);

void
mmUIViewCalendar_OnEventClicked2x(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventClicked000(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventClicked001(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventClicked01(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventUpdate(
    struct mmUIViewCalendar*                       p,
    struct mmEventUpdateArgs*                      args);

void
mmUIViewCalendar_OnEventSelected0(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventSelected1(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

void
mmUIViewCalendar_OnEventSelected2(
    struct mmUIViewCalendar*                       p,
    struct mmEventUIViewArgs*                      args);

#include "core/mmSuffix.h"

#endif//__mmUIViewCalendar_h__
