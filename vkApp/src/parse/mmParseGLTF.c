/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmParseGLTF.h"
#include "mmParseJSON.h"
#include "mmParseJSONHelper.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"
#include "core/mmLimit.h"
#include "core/mmByteOrder.h"
#include "core/mmLogger.h"
#include "core/mmStringUtils.h"
#include "core/mmFilePath.h"

#include "algorithm/mmBase64.h"
#include "algorithm/mmMurmurHash.h"

#include "math/mmMathConst.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix3x3.h"
#include "math/mmMatrix4x4.h"

// 0x33C146DA KHR_draco_mesh_compression                  o
// 0x6881A6AB KHR_lights_punctual                         o
// 0xEBF30246 KHR_materials_clearcoat                     o
// 0x46B2C06A KHR_materials_emissive_strength             o
// 0x22931160 KHR_materials_ior                           o
// 0xE43EEDE9 KHR_materials_iridescence                   o
// 0x82920765 KHR_materials_pbrSpecularGlossiness         o
// 0xB4389BE3 KHR_materials_sheen                         o
// 0x50F5D15D KHR_materials_specular                      o
// 0x72F0A2D9 KHR_materials_transmission                  o
// 0xA04EA645 KHR_materials_unlit                         o
// 0x01820F80 KHR_materials_variants                      o
// 0x2ED19E38 KHR_materials_volume                        o
// 0x67BAC19B KHR_mesh_quantization
// 0xEBD8AD8E KHR_techniques_webgl
// 0xFA0B3420 KHR_texture_basisu                          o
// 0x2FDC584B KHR_texture_transform                       o
// 0xB0CF044F KHR_xmp
// 0x57A44AAE KHR_xmp_json_ld
// 0xFD933596 EXT_meshopt_compression                     o

static const float mmParseJSONFactorOne = 1.0f;

int mmParseJSON_DecodeGLTFBufferViewType(struct mmParseJSON* p, int i,
    enum mmGLTFBufferView_t* v,
    enum mmGLTFBufferView_t d)
{
    return mmParseJSON_DecodeSInt(p, i, (mmSInt_t*)v, d);
}

void mmValue_StringToGLTFAttributeType(const char* _value, enum mmGLTFAttribute_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x303D8C9E:
        // 0x303D8C9E POSITION
        (*val) = mmGLTFAttributeTypePosition;
        break;
    case 0x06C4DD06:
        // 0x06C4DD06 NORMAL
        (*val) = mmGLTFAttributeTypeNormal;
        break;
    case 0x98BA9D64:
        // 0x98BA9D64 TANGENT
        (*val) = mmGLTFAttributeTypeTangent;
        break;
    case 0x3F31E47F:
        // 0x3F31E47F TEXCOORD
        (*val) = mmGLTFAttributeTypeTexcoord;
        break;
    case 0x5BBDA918:
        // 0x5BBDA918 COLOR
        (*val) = mmGLTFAttributeTypeColor;
        break;
    case 0xC29B45D8:
        // 0xC29B45D8 JOINTS
        (*val) = mmGLTFAttributeTypeJoints;
        break;
    case 0xC5721DE4:
        // 0xC5721DE4 WEIGHTS
        (*val) = mmGLTFAttributeTypeWeights;
        break;
    default:
        (*val) = mmGLTFAttributeTypeInvalid;
        break;
    }
}

int mmParseJSON_DecodeGLTFComponentType(struct mmParseJSON* p, int i,
    enum mmGLTFComponent_t* v,
    enum mmGLTFComponent_t d)
{
    return mmParseJSON_DecodeSInt(p, i, (mmSInt_t*)v, d);
}

int32_t mmGLTFGetComponentSizeInBytes(enum mmGLTFComponent_t componentType)
{
    switch (componentType)
    {
    case mmGLTFComponentTypeUnsignedByte:
        return 1;
    case mmGLTFComponentTypeByte:
        return 1;
    case mmGLTFComponentTypeUnsignedShort:
        return 2;
    case mmGLTFComponentTypeShort:
        return 2;
    case mmGLTFComponentTypeInt:
        return 4;
    case mmGLTFComponentTypeUnsignedInt:
        return 4;
    case mmGLTFComponentTypeFloat:
        return 4;
    case mmGLTFComponentTypeDouble:
        return 8;
    default:
        return 0;
    }
}

void mmValue_StringToGLTFAccessorType(const char* _value, enum mmGLTFAccessor_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x600728D9:
        // 0x600728D9 SCALAR
        (*val) = mmGLTFTypeScalar;
        break;
    case 0x92CF0A52:
        // 0x92CF0A52 VEC2
        (*val) = mmGLTFTypeVec2;
        break;
    case 0x216C9B8A:
        // 0x216C9B8A VEC3
        (*val) = mmGLTFTypeVec3;
        break;
    case 0x8F196D66:
        // 0x8F196D66 VEC4
        (*val) = mmGLTFTypeVec4;
        break;
    case 0x4CE567F4:
        // 0x4CE567F4 MAT2
        (*val) = mmGLTFTypeMat2;
        break;
    case 0x26817A03:
        // 0x26817A03 MAT3
        (*val) = mmGLTFTypeMat3;
        break;
    case 0xB3F90FAE:
        // 0xB3F90FAE MAT4
        (*val) = mmGLTFTypeMat4;
        break;
    default:
        (*val) = mmGLTFTypeInvalid;
        break;
    }
}
int mmParseJSON_DecodeGLTFAccessorType(struct mmParseJSON* p, int i,
    enum mmGLTFAccessor_t* v,
    enum mmGLTFAccessor_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFAccessor_t),
        &mmValue_StringToGLTFAccessorType);
}

int32_t mmGLTFGetNumComponentsInType(enum mmGLTFAccessor_t ty)
{
    switch (ty)
    {
    case mmGLTFTypeInvalid:
        return 1;
    case mmGLTFTypeScalar:
        return 1;
    case mmGLTFTypeVec2:
        return 2;
    case mmGLTFTypeVec3:
        return 3;
    case mmGLTFTypeVec4:
        return 4;
    case mmGLTFTypeMat2:
        return 4;
    case mmGLTFTypeMat3:
        return 9;
    case mmGLTFTypeMat4:
        return 16;
    default:
        return 1;
    }
}

int mmParseJSON_DecodeGLTFPrimitiveType(struct mmParseJSON* p, int i,
    enum mmGLTFPrimitive_t* v,
    enum mmGLTFPrimitive_t d)
{
    return mmParseJSON_DecodeSInt(p, i, (mmSInt_t*)v, d);
}

void mmValue_StringToGLTFAlphaModeType(const char* _value, enum mmGLTFAlphaMode_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0xCC01C2D8:
        // 0xCC01C2D8 OPAQUE
        (*val) = mmGLTFAlphaModeOpaque;
        break;
    case 0x779CE970:
        // 0x779CE970 MASK
        (*val) = mmGLTFAlphaModeMask;
        break;
    case 0x145CC1BE:
        // 0x145CC1BE BLEND
        (*val) = mmGLTFAlphaModeBlend;
        break;
    default:
        (*val) = mmGLTFAlphaModeOpaque;
        break;
    }
}
int mmParseJSON_DecodeGLTFAlphaModeType(struct mmParseJSON* p, int i,
    enum mmGLTFAlphaMode_t* v,
    enum mmGLTFAlphaMode_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFAlphaMode_t),
        &mmValue_StringToGLTFAlphaModeType);
}

void mmValue_StringToGLTFAnimationPathType(const char* _value, enum mmGLTFAnimationPath_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x25A069A3:
        // 0x25A069A3 translation
        (*val) = mmGLTFAnimationPathTypeTranslation;
        break;
    case 0xA3FE4DF9:
        // 0xA3FE4DF9 rotation
        (*val) = mmGLTFAnimationPathTypeRotation;
        break;
    case 0xEF366486:
        // 0xEF366486 scale
        (*val) = mmGLTFAnimationPathTypeScale;
        break;
    case 0xAE6E45DD:
        // 0xAE6E45DD weights
        (*val) = mmGLTFAnimationPathTypeWeights;
        break;
    default:
        (*val) = mmGLTFAnimationPathTypeInvalid;
        break;
    }
}
int mmParseJSON_DecodeGLTFAnimationPathType(struct mmParseJSON* p, int i,
    enum mmGLTFAnimationPath_t* v,
    enum mmGLTFAnimationPath_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFAnimationPath_t),
        &mmValue_StringToGLTFAnimationPathType);
}

void mmValue_StringToGLTFInterpolationType(const char* _value, enum mmGLTFInterpolation_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0xF15E95EC:
        // 0xF15E95EC LINEAR
        (*val) = mmGLTFInterpolationTypeLinear;
        break;
    case 0xE1D92438:
        // 0xE1D92438 STEP
        (*val) = mmGLTFInterpolationTypeStep;
        break;
    case 0xC0E67342:
        // 0xC0E67342 CUBICSPLINE
        (*val) = mmGLTFInterpolationTypeCubicSpline;
        break;
    default:
        (*val) = mmGLTFInterpolationTypeLinear;
        break;
    }
}
int mmParseJSON_DecodeGLTFInterpolationType(struct mmParseJSON* p, int i,
    enum mmGLTFInterpolation_t* v,
    enum mmGLTFInterpolation_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFInterpolation_t),
        &mmValue_StringToGLTFInterpolationType);
}

void mmValue_StringToGLTFCameraType(const char* _value, enum mmGLTFCamera_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x329EF6A4:
        // 0x329EF6A4 perspective
        (*val) = mmGLTFCameraTypePerspective;
        break;
    case 0x5393C9A6:
        // 0x5393C9A6 orthographic
        (*val) = mmGLTFCameraTypeOrthographic;
        break;
    default:
        (*val) = mmGLTFCameraTypeInvalid;
        break;
    }
}
int mmParseJSON_DecodeGLTFCameraType(struct mmParseJSON* p, int i,
    enum mmGLTFCamera_t* v,
    enum mmGLTFCamera_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFCamera_t),
        &mmValue_StringToGLTFCameraType);
}

int mmParseJSON_DecodeGLTFSamplerFilterModeType(struct mmParseJSON* p, int i,
    enum mmGLTFSamplerFilterMode_t* v,
    enum mmGLTFSamplerFilterMode_t d)
{
    return mmParseJSON_DecodeSInt(p, i, (mmSInt_t*)v, d);
}

int mmParseJSON_DecodeGLTFSamplerAddressModeType(struct mmParseJSON* p, int i,
    enum mmGLTFSamplerAddressMode_t* v,
    enum mmGLTFSamplerAddressMode_t d)
{
    return mmParseJSON_DecodeSInt(p, i, (mmSInt_t*)v, d);
}

void mmValue_StringToGLTFLightType(const char* _value, enum mmGLTFLight_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x33AD138C:
        // 0x33AD138C directional
        (*val) = mmGLTFLightTypeDirectional;
        break;
    case 0xA2C734F4:
        // 0xA2C734F4 point
        (*val) = mmGLTFLightTypePoint;
        break;
    case 0xF39A54E5:
        // 0xF39A54E5 spot
        (*val) = mmGLTFLightTypeSpot;
        break;
    default:
        (*val) = mmGLTFLightTypeInvalid;
        break;
    }
}
int mmParseJSON_DecodeGLTFLightType(struct mmParseJSON* p, int i,
    enum mmGLTFLight_t* v,
    enum mmGLTFLight_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(enum mmGLTFLight_t),
        &mmValue_StringToGLTFLightType);
}

void mmValue_StringToGLTFMeshoptCompressionModeType(const char* _value, enum mmGLTFMeshoptCompressionMode_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x1B70B8DF:
        // 0x1B70B8DF ATTRIBUTES
        (*val) = mmGLTFMeshoptCompressionModeAttributes;
        break;
    case 0x7BFD2CB2:
        // 0x7BFD2CB2 TRIANGLES
        (*val) = mmGLTFMeshoptCompressionModeTriangles;
        break;
    case 0x4807F0E3:
        // 0x4807F0E3 INDICES
        (*val) = mmGLTFMeshoptCompressionModeIndices;
        break;
    default:
        (*val) = mmGLTFMeshoptCompressionModeInvalid;
        break;
    }
}
int mmParseJSON_DecodeGLTFMeshoptCompressionModeType(struct mmParseJSON* p, int i, 
    enum mmGLTFMeshoptCompressionMode_t* v, 
    enum mmGLTFMeshoptCompressionMode_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d, 
        sizeof(enum mmGLTFMeshoptCompressionMode_t), 
        &mmValue_StringToGLTFMeshoptCompressionModeType);
}

void mmValue_StringToGLTFMeshoptCompressionFilterType(const char* _value, enum mmGLTFMeshoptCompressionFilter_t* val)
{
    size_t len = strlen(_value);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)_value, (int)len, 0);
    switch (h)
    {
    case 0x22EB22E8:
        // 0x22EB22E8 NONE
        (*val) = mmGLTFMeshoptCompressionFilterNone;
        break;
    case 0x9DAD1A8B:
        // 0x9DAD1A8B OCTAHEDRAL
        (*val) = mmGLTFMeshoptCompressionFilterOctahedral;
        break;
    case 0x7F83A128:
        // 0x7F83A128 QUATERNION
        (*val) = mmGLTFMeshoptCompressionFilterQuaternion;
        break;
    case 0x616B61BC:
        // 0x616B61BC EXPONENTIAL
        (*val) = mmGLTFMeshoptCompressionFilterExponential;
        break;
    default:
        (*val) = mmGLTFMeshoptCompressionFilterNone;
        break;
    }
}

int mmParseJSON_DecodeGLTFMeshoptCompressionFilterType(struct mmParseJSON* p, int i, 
    enum mmGLTFMeshoptCompressionFilter_t* v, 
    enum mmGLTFMeshoptCompressionFilter_t d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d, 
        sizeof(enum mmGLTFMeshoptCompressionFilter_t), 
        &mmValue_StringToGLTFMeshoptCompressionFilterType);
}

void mmGLTFRanges_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(struct mmRange));
}
void mmGLTFRanges_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFUriData_Init(struct mmGLTFUriData* p)
{
    mmByteBuffer_Init(&p->bytes);
    p->embedded = MM_FALSE;
}
void mmGLTFUriData_Destroy(struct mmGLTFUriData* p)
{
    p->embedded = MM_FALSE;
    mmByteBuffer_Destroy(&p->bytes);
}

void mmGLTFExtras_Init(struct mmGLTFExtras* p)
{
    mmRange_Init(&p->data);
    p->i = -1;
}
void mmGLTFExtras_Destroy(struct mmGLTFExtras* p)
{
    p->i = -1;
    mmRange_Destroy(&p->data);
}
void mmGLTFExtras_Reset(struct mmGLTFExtras* p)
{
    mmRange_Reset(&p->data);
    p->i = -1;
}
int mmParseJSON_DecodeGLTFExtras(struct mmParseJSON* p, int i, struct mmGLTFExtras* v)
{
    v->i = i;
    i = mmParseJSON_DecodeRange(p, i, &v->data, &mmRangeZero);
    return i;
}

void mmGLTFExtension_Init(struct mmGLTFExtension* p)
{
    mmRange_Init(&p->name);
    p->i = -1;
}
void mmGLTFExtension_Destroy(struct mmGLTFExtension* p)
{
    p->i = -1;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFExtension(struct mmParseJSON* p, int i, mmUInt32_t m, void* o)
{
    struct mmGLTFExtension* v = (struct mmGLTFExtension*)(o);

    i = mmParseJSON_DecodeRange(p, i, &v->name, &mmRangeZero);

    if (i < 0)
    {
        return i;
    }

    v->i = i;

    i = mmParseJSON_SkipToken(p, i);

    return i;
}

void mmGLTFExtensions_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFExtension_Init;
    hValueAllocator.Recycle = &mmGLTFExtension_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFExtension));
}
void mmGLTFExtensions_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFBuffer_Init(struct mmGLTFBuffer* p)
{
    mmRange_Init(&p->name);
    mmRange_Init(&p->uri);
    p->byteLength = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    mmGLTFUriData_Init(&p->data);
}
void mmGLTFBuffer_Destroy(struct mmGLTFBuffer* p)
{
    mmGLTFUriData_Destroy(&p->data);

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->byteLength = 0;
    mmRange_Destroy(&p->uri);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFBuffer(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFBuffer* v = (struct mmGLTFBuffer*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xB235BA87:
        // 0xB235BA87 uri
        i = mmParseJSON_DecodeRange(p, ++i, &v->uri, &mmRangeZero);
        break;
    case 0x2113E37F:
        // 0x2113E37F byteLength
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteLength, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMeshoptCompression_Init(struct mmGLTFMeshoptCompression* p)
{
    p->buffer = -1;
    p->byteOffset = 0;
    p->byteLength = 0;
    p->byteStride = 0;
    p->count = 0;
    p->mode = mmGLTFMeshoptCompressionModeInvalid;
    p->filter = mmGLTFMeshoptCompressionFilterNone;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMeshoptCompression_Destroy(struct mmGLTFMeshoptCompression* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->filter = mmGLTFMeshoptCompressionFilterNone;
    p->mode = mmGLTFMeshoptCompressionModeInvalid;
    p->count = 0;
    p->byteStride = 0;
    p->byteLength = 0;
    p->byteOffset = 0;
    p->buffer = -1;
}

int mmParseJSON_DecodeGLTFMeshoptCompression(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMeshoptCompression* v = (struct mmGLTFMeshoptCompression*)(o);
    switch (h)
    {
    case 0x3E85D019:
        // 0x3E85D019 buffer
        i = mmParseJSON_DecodeSInt(p, ++i, &v->buffer, -1);
        break;
    case 0x7F9CED14:
        // 0x7F9CED14 byteOffset
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteOffset, 0);
        break;
    case 0x2113E37F:
        // 0x2113E37F byteLength
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteLength, 0);
        break;
    case 0x69A78132:
        // 0x69A78132 byteStride
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteStride, 0);
        break;
    case 0x23AC2F5C:
        // 0x23AC2F5C count
        i = mmParseJSON_DecodeSize(p, ++i, &v->count, 0);
        break;
    case 0x6C4AC7F7:
        // 0x6C4AC7F7 mode
        i = mmParseJSON_DecodeGLTFMeshoptCompressionModeType(p, ++i, &v->mode, mmGLTFMeshoptCompressionModeInvalid);
        break;
    case 0xA859BB48:
        // 0xA859BB48 filter
        i = mmParseJSON_DecodeGLTFMeshoptCompressionFilterType(p, ++i, &v->filter, mmGLTFMeshoptCompressionFilterNone);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFBufferView_Init(struct mmGLTFBufferView* p)
{
    mmRange_Init(&p->name);
    p->buffer = -1;
    p->byteOffset = 0;
    p->byteLength = 0;
    p->byteStride = 0;
    p->target = mmGLTFBufferViewTypeInvalid;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
    
    p->has_meshoptCompression = MM_FALSE;
    mmGLTFMeshoptCompression_Init(&p->ext_meshoptCompression);
}
void mmGLTFBufferView_Destroy(struct mmGLTFBufferView* p)
{
    mmGLTFMeshoptCompression_Destroy(&p->ext_meshoptCompression);
    p->has_meshoptCompression = MM_FALSE;
    
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->target = mmGLTFBufferViewTypeInvalid;
    p->byteStride = 0;
    p->byteLength = 0;
    p->byteOffset = 0;
    p->buffer = -1;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFBufferViewExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFBufferView* v = (struct mmGLTFBufferView*)(o);
    switch (h)
    {
    case 0xFD933596:
        // 0xFD933596 EXT_meshopt_compression
        v->has_meshoptCompression = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_meshoptCompression, &mmParseJSON_DecodeGLTFMeshoptCompression);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFBufferView(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFBufferView* v = (struct mmGLTFBufferView*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x3E85D019:
        // 0x3E85D019 buffer
        i = mmParseJSON_DecodeSInt(p, ++i, &v->buffer, -1);
        break;
    case 0x7F9CED14:
        // 0x7F9CED14 byteOffset
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteOffset, 0);
        break;
    case 0x2113E37F:
        // 0x2113E37F byteLength
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteLength, 0);
        break;
    case 0x69A78132:
        // 0x69A78132 byteStride
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteStride, 0);
        break;
    case 0x6E17A098:
        // 0x6E17A098 target
        i = mmParseJSON_DecodeGLTFBufferViewType(p, ++i, &v->target, mmGLTFBufferViewTypeInvalid);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFBufferViewExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAccessorSparseIndices_Init(struct mmGLTFAccessorSparseIndices* p)
{
    p->bufferView = -1;
    p->byteOffset = 0;
    p->componentType = mmGLTFComponentTypeInvalid;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAccessorSparseIndices_Destroy(struct mmGLTFAccessorSparseIndices* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->componentType = mmGLTFComponentTypeInvalid;
    p->byteOffset = 0;
    p->bufferView = -1;
}
int mmParseJSON_DecodeGLTFAccessorSparseIndices(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAccessorSparseIndices* v = (struct mmGLTFAccessorSparseIndices*)(o);
    switch (h)
    {
    case 0x9D7A1248:
        // 0x9D7A1248 bufferView
        i = mmParseJSON_DecodeSInt(p, ++i, &v->bufferView, -1);
        break;
    case 0x7F9CED14:
        // 0x7F9CED14 byteOffset
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteOffset, 0);
        break;
    case 0x72CF7F1B:
        // 0x72CF7F1B componentType
        i = mmParseJSON_DecodeGLTFComponentType(p, ++i, &v->componentType, mmGLTFComponentTypeInvalid);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAccessorSparseValues_Init(struct mmGLTFAccessorSparseValues* p)
{
    p->bufferView = -1;
    p->byteOffset = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAccessorSparseValues_Destroy(struct mmGLTFAccessorSparseValues* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->byteOffset = 0;
    p->bufferView = -1;
}

int mmParseJSON_DecodeGLTFAccessorSparseValues(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAccessorSparseValues* v = (struct mmGLTFAccessorSparseValues*)(o);
    switch (h)
    {
    case 0x9D7A1248:
        // 0x9D7A1248 bufferView
        i = mmParseJSON_DecodeSInt(p, ++i, &v->bufferView, -1);
        break;
    case 0x7F9CED14:
        // 0x7F9CED14 byteOffset
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteOffset, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAccessorSparse_Init(struct mmGLTFAccessorSparse* p)
{
    p->count = 0;
    mmGLTFAccessorSparseIndices_Init(&p->indices);
    mmGLTFAccessorSparseValues_Init(&p->values);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAccessorSparse_Destroy(struct mmGLTFAccessorSparse* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFAccessorSparseValues_Destroy(&p->values);
    mmGLTFAccessorSparseIndices_Destroy(&p->indices);
    p->count = 0;
}

int mmParseJSON_DecodeGLTFAccessorSparse(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAccessorSparse* v = (struct mmGLTFAccessorSparse*)(o);
    switch (h)
    {
    case 0x23AC2F5C:
        // 0x23AC2F5C count
        i = mmParseJSON_DecodeSize(p, ++i, &v->count, 0);
        break;
    case 0xA3875FAE:
        // 0xA3875FAE indices
        i = mmParseJSON_DecodeObject(p, ++i, &v->indices, &mmParseJSON_DecodeGLTFAccessorSparseIndices);
        break;
    case 0x5DF5635D:
        // 0x5DF5635D values
        i = mmParseJSON_DecodeObject(p, ++i, &v->values, &mmParseJSON_DecodeGLTFAccessorSparseValues);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAccessor_Init(struct mmGLTFAccessor* p)
{
    mmRange_Init(&p->name);
    p->bufferView = -1;
    p->byteOffset = 0;
    p->componentType = mmGLTFComponentTypeInvalid;
    p->normalized = MM_FALSE;
    p->count = 0;
    p->type = mmGLTFTypeInvalid;
    mmMemset(p->min, 0, sizeof(float) * 16);
    mmMemset(p->max, 0, sizeof(float) * 16);
    mmGLTFAccessorSparse_Init(&p->sparse);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
    p->minSize = 0;
    p->maxSize = 0;
    p->isSparse = MM_FALSE;
}
void mmGLTFAccessor_Destroy(struct mmGLTFAccessor* p)
{
    p->isSparse = MM_FALSE;
    p->maxSize = 0;
    p->minSize = 0;
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFAccessorSparse_Destroy(&p->sparse);
    mmMemset(p->max, 0, sizeof(float) * 16);
    mmMemset(p->min, 0, sizeof(float) * 16);
    p->type = mmGLTFTypeInvalid;
    p->count = 0;
    p->normalized = MM_FALSE;
    p->componentType = mmGLTFComponentTypeInvalid;
    p->byteOffset = 0;
    p->bufferView = -1;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFAccessor(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAccessor* v = (struct mmGLTFAccessor*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x9D7A1248:
        // 0x9D7A1248 bufferView
        i = mmParseJSON_DecodeSInt(p, ++i, &v->bufferView, -1);
        break;
    case 0x7F9CED14:
        // 0x7F9CED14 byteOffset
        i = mmParseJSON_DecodeSize(p, ++i, &v->byteOffset, 0);
        break;
    case 0x72CF7F1B:
        // 0x72CF7F1B componentType
        i = mmParseJSON_DecodeGLTFComponentType(p, ++i, &v->componentType, mmGLTFComponentTypeInvalid);
        break;
    case 0x909DA14D:
        // 0x909DA14D normalized
        i = mmParseJSON_DecodeBool(p, ++i, &v->normalized, MM_FALSE);
        break;
    case 0x23AC2F5C:
        // 0x23AC2F5C count
        i = mmParseJSON_DecodeSize(p, ++i, &v->count, 0);
        break;
    case 0x6A235628:
        // 0x6A235628 type
        i = mmParseJSON_DecodeGLTFAccessorType(p, ++i, &v->type, mmGLTFTypeInvalid);
        break;
    case 0x65F9712A:
        // 0x65F9712A min
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->min, &MM_FLOAT_ZERO, 16, &v->minSize);
        break;
    case 0x8B011038:
        // 0x8B011038 max
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->max, &MM_FLOAT_ZERO, 16, &v->maxSize);
        break;
    case 0x085D9CD6:
        // 0x085D9CD6 sparse
        i = mmParseJSON_DecodeObject(p, ++i, &v->sparse, &mmParseJSON_DecodeGLTFAccessorSparse);
        v->isSparse = i > 0;
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmGLTFAccessor_ByteStride(const struct mmGLTFAccessor* p, struct mmGLTFBufferView* bufferView)
{
    if ((NULL == bufferView) ||
        (NULL != bufferView && bufferView->byteStride == 0))
    {
        // Assume data is tightly packed.
        int componentSizeInBytes = mmGLTFGetComponentSizeInBytes(p->componentType);
        if (componentSizeInBytes <= 0)
        {
            return -1;
        }

        int numComponents = mmGLTFGetNumComponentsInType(p->type);
        if (numComponents <= 0)
        {
            return -1;
        }

        return componentSizeInBytes * numComponents;
    }
    else
    {
        // Check if byteStride is a mulple of the size of the accessor's component
        // type.
        int componentSizeInBytes = mmGLTFGetComponentSizeInBytes(p->componentType);
        if (componentSizeInBytes <= 0)
        {
            return -1;
        }

        if ((bufferView->byteStride % (uint32_t)(componentSizeInBytes)) != 0)
        {
            return -1;
        }
        return (int)(bufferView->byteStride);
    }

    // unreachable return 0;
    return 0;
}

void mmGLTFAttribute_Init(struct mmGLTFAttribute* p)
{
    mmRange_Init(&p->name);
    p->type = mmGLTFAttributeTypeInvalid;
    p->index = 0;
    p->accessor = -1;
}
void mmGLTFAttribute_Destroy(struct mmGLTFAttribute* p)
{
    p->accessor = -1;
    p->index = 0;
    p->type = mmGLTFAttributeTypeInvalid;
    mmRange_Destroy(&p->name);
}

void mmParseJSON_DecodeGLTFAttributeType(struct mmParseJSON* p, struct mmGLTFAttribute* v)
{
    char type[32] = { 0 };
    char buffer[32] = { 0 };

    const char* us = "";
    size_t len = 0;

    mmParseJSON_GetRangeBuffer(p, &v->name, buffer, 32);
    us = strchr(buffer, '_');
    len = us ? (size_t)(us - buffer) : strlen(buffer);
    mmMemcpy(type, buffer, len);
    mmValue_StringToGLTFAttributeType(type, &v->type);

    if (us && mmGLTFAttributeTypeInvalid != v->type)
    {
        v->index = atoi(us + 1);
    }
    else
    {
        v->index = 0;
    }
}

int mmParseJSON_DecodeGLTFAttribute(struct mmParseJSON* p, int i, mmUInt32_t m, void* o)
{
    struct mmGLTFAttribute* v = (struct mmGLTFAttribute*)(o);

    i = mmParseJSON_DecodeRange(p, i, &v->name, &mmRangeZero);

    if (i < 0)
    {
        return i;
    }

    i = mmParseJSON_DecodeSInt(p, i, &v->accessor, -1);

    mmParseJSON_DecodeGLTFAttributeType(p, v);

    if (i < 0)
    {
        return i;
    }

    return i;
}

void mmGLTFAttributes_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAttribute_Init;
    hValueAllocator.Recycle = &mmGLTFAttribute_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFAttribute));
}
void mmGLTFAttributes_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

int mmGLTFAttributes_GetAccessor(struct mmParseJSON* p, struct mmVectorValue* v, const char* name)
{
    size_t i = 0;
    struct mmGLTFAttribute* attr = NULL;
    char buffer[32] = { 0 };
    for (i = 0; i < v->size; ++i)
    {
        attr = (struct mmGLTFAttribute*)mmVectorValue_At(v, i);
        mmMemset(buffer, 0, 32);
        mmParseJSON_GetRangeBuffer(p, &attr->name, buffer, 32);
        if (0 == strcmp(buffer, name))
        {
            return attr->accessor;
        }
    }
    return -1;
}

int mmGLTFAttributes_GetAccessorSister(struct mmVectorValue* l, struct mmVectorValue* r, size_t idx)
{
    size_t i = 0;
    struct mmGLTFAttribute* attr1 = NULL;
    struct mmGLTFAttribute* attr2 = NULL;
    
    attr1 = (struct mmGLTFAttribute*)mmVectorValue_At(l, idx);
    attr2 = (struct mmGLTFAttribute*)mmVectorValue_At(r, idx);
    if (attr1->type == attr2->type &&
        attr1->index == attr2->index)
    {
        return attr2->accessor;
    }
    
    for (i = 0; i < r->size; ++i)
    {
        attr2 = (struct mmGLTFAttribute*)mmVectorValue_At(r, idx);
        if (attr1->type == attr2->type &&
            attr1->index == attr2->index)
        {
            return attr2->accessor;
        }
    }
    
    return -1;
}

void mmGLTFImage_Init(struct mmGLTFImage* p)
{
    mmRange_Init(&p->name);
    mmRange_Init(&p->uri);
    mmRange_Init(&p->mimeType);
    p->bufferView = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    mmGLTFUriData_Init(&p->data);
}
void mmGLTFImage_Destroy(struct mmGLTFImage* p)
{
    mmGLTFUriData_Destroy(&p->data);

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->bufferView = -1;
    mmRange_Destroy(&p->mimeType);
    mmRange_Destroy(&p->uri);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFImage(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFImage* v = (struct mmGLTFImage*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xB235BA87:
        // 0xB235BA87 uri
        i = mmParseJSON_DecodeRange(p, ++i, &v->uri, &mmRangeZero);
        break;
    case 0x46861E31:
        // 0x46861E31 mimeType
        i = mmParseJSON_DecodeRange(p, ++i, &v->mimeType, &mmRangeZero);
        break;
    case 0x9D7A1248:
        // 0x9D7A1248 bufferView
        i = mmParseJSON_DecodeSInt(p, ++i, &v->bufferView, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFSampler_Init(struct mmGLTFSampler* p)
{
    mmRange_Init(&p->name);
    p->magFilter = -1;
    p->minFilter = -1;
    p->wrapS = mmSamplerAddressModeRepeat;
    p->wrapT = mmSamplerAddressModeRepeat;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFSampler_Destroy(struct mmGLTFSampler* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->wrapT = mmSamplerAddressModeRepeat;
    p->wrapS = mmSamplerAddressModeRepeat;
    p->minFilter = -1;
    p->magFilter = -1;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFSampler(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFSampler* v = (struct mmGLTFSampler*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x7AA85CA2:
        // 0x7AA85CA2 magFilter
        i = mmParseJSON_DecodeGLTFSamplerFilterModeType(p, ++i, &v->magFilter, -1);
        break;
    case 0xBC6A4C92:
        // 0xBC6A4C92 minFilter
        i = mmParseJSON_DecodeGLTFSamplerFilterModeType(p, ++i, &v->minFilter, -1);
        break;
    case 0x6B24E931:
        // 0x6B24E931 wrapS
        i = mmParseJSON_DecodeGLTFSamplerAddressModeType(p, ++i, &v->wrapS, mmSamplerAddressModeRepeat);
        break;
    case 0x511B4500:
        // 0x511B4500 wrapT
        i = mmParseJSON_DecodeGLTFSamplerAddressModeType(p, ++i, &v->wrapT, mmSamplerAddressModeRepeat);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFTextureBasisu_Init(struct mmGLTFTextureBasisu* p)
{
    p->source = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFTextureBasisu_Destroy(struct mmGLTFTextureBasisu* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->source = -1;
}

int mmParseJSON_DecodeGLTFTextureBasisu(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTextureBasisu* v = (struct mmGLTFTextureBasisu*)(o);
    switch (h)
    {
    case 0xBF027747:
        // 0xBF027747 source
        i = mmParseJSON_DecodeSInt(p, ++i, &v->source, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFTexture_Init(struct mmGLTFTexture* p)
{
    mmRange_Init(&p->name);
    p->sampler = -1;
    p->source = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_basisu = MM_FALSE;
    mmGLTFTextureBasisu_Init(&p->ext_basisu);
}
void mmGLTFTexture_Destroy(struct mmGLTFTexture* p)
{
    mmGLTFTextureBasisu_Destroy(&p->ext_basisu);
    p->has_basisu = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->source = -1;
    p->sampler = -1;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFTextureExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTexture* v = (struct mmGLTFTexture*)(o);
    switch (h)
    {
    case 0xFA0B3420:
        // 0xFA0B3420 KHR_texture_basisu
        v->has_basisu = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_basisu, &mmParseJSON_DecodeGLTFTextureBasisu);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFTexture(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTexture* v = (struct mmGLTFTexture*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x82B39DE8:
        // 0x82B39DE8 sampler
        i = mmParseJSON_DecodeSInt(p, ++i, &v->sampler, -1);
        break;
    case 0xBF027747:
        // 0xBF027747 source
        i = mmParseJSON_DecodeSInt(p, ++i, &v->source, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFTextureExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }

    return i;
}

int mmGLTFTexture_GetSource(struct mmGLTFTexture* p)
{
    return p->has_basisu ? p->ext_basisu.source : p->source;
}

void mmGLTFTextureTransform_Init(struct mmGLTFTextureTransform* p)
{
    mmVec2Make(p->offset, 0, 0);
    p->rotation = 0;
    mmVec2Make(p->scale, 1, 1);
    p->texCoord = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFTextureTransform_Destroy(struct mmGLTFTextureTransform* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->texCoord = -1;
    mmVec2Make(p->scale, 1, 1);
    p->rotation = 0;
    mmVec2Make(p->offset, 0, 0);
}

void mmGLTFTextureTransform_MakeMat3x3(const struct mmGLTFTextureTransform* p, float m[3][3])
{
    const float* o = p->offset;
    const float  r = p->rotation;
    const float* s = p->scale;

    const float S = sinf(r);
    const float C = cosf(r);

    float translation[3][3] =
    {
        {    1,    0,    0, },
        {    0,    1,    0, },
        { o[0], o[1],    1, },
    };

    float rotation[3][3] =
    {
        {    C,    S,    0, },
        {   -S,    C,    0, },
        {    0,    0,    1, },
    };

    float scale[3][3] =
    {
        { s[0],    0,    0, },
        {    0, s[1],    0, },
        {    0,    0,    1, },
    };

    mmMat3x3Mul(m, scale, rotation);
    mmMat3x3Mul(m, translation, m);
}

int mmParseJSON_DecodeGLTFTextureTransform(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTextureTransform* v = (struct mmGLTFTextureTransform*)(o);
    switch (h)
    {
    case 0x9DF2C725:
        // 0x9DF2C725 offset
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->offset, &MM_FLOAT_ZERO, 2, NULL);
        break;
    case 0xA3FE4DF9:
        // 0xA3FE4DF9 rotation
        i = mmParseJSON_DecodeFloat(p, ++i, &v->rotation, MM_FLOAT_ZERO);
        break;
    case 0xEF366486:
        // 0xEF366486 scale
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->offset, &mmParseJSONFactorOne, 2, NULL);
        break;
    case 0xEF155B24:
        // 0xEF155B24 texCoord
        i = mmParseJSON_DecodeSInt(p, ++i, &v->texCoord, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFTextureExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFTextureInfo_Init(struct mmGLTFTextureInfo* p)
{
    p->index = -1;
    p->texCoord = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_transform = MM_FALSE;
    mmGLTFTextureTransform_Init(&p->ext_transform);
}
void mmGLTFTextureInfo_Destroy(struct mmGLTFTextureInfo* p)
{
    mmGLTFTextureTransform_Destroy(&p->ext_transform);
    p->has_transform = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->texCoord = 0;
    p->index = -1;
}

int mmParseJSON_DecodeGLTFTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTextureInfo* v = (struct mmGLTFTextureInfo*)(o);
    switch (h)
    {
    case 0x2FDC584B:
        // 0x2FDC584B KHR_texture_transform
        v->has_transform = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_transform, &mmParseJSON_DecodeGLTFTextureTransform);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFTextureInfo* v = (struct mmGLTFTextureInfo*)(o);
    switch (h)
    {
    case 0x0A3E2B42:
        // 0x0A3E2B42 index
        i = mmParseJSON_DecodeSInt(p, ++i, &v->index, -1);
        break;
    case 0xEF155B24:
        // 0xEF155B24 texCoord
        i = mmParseJSON_DecodeSInt(p, ++i, &v->texCoord, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFTextureInfoExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFNormalTextureInfo_Init(struct mmGLTFNormalTextureInfo* p)
{
    p->index = -1;
    p->texCoord = 0;
    p->scale = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_transform = MM_FALSE;
    mmGLTFTextureTransform_Init(&p->ext_transform);
}
void mmGLTFNormalTextureInfo_Destroy(struct mmGLTFNormalTextureInfo* p)
{
    mmGLTFTextureTransform_Destroy(&p->ext_transform);
    p->has_transform = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->scale = 1;
    p->texCoord = 0;
    p->index = -1;
}

int mmParseJSON_DecodeGLTFNormalTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFNormalTextureInfo* v = (struct mmGLTFNormalTextureInfo*)(o);
    switch (h)
    {
    case 0x2FDC584B:
        // 0x2FDC584B KHR_texture_transform
        v->has_transform = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_transform, &mmParseJSON_DecodeGLTFTextureTransform);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFNormalTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFNormalTextureInfo* v = (struct mmGLTFNormalTextureInfo*)(o);
    switch (h)
    {
    case 0x0A3E2B42:
        // 0x0A3E2B42 index
        i = mmParseJSON_DecodeSInt(p, ++i, &v->index, -1);
        break;
    case 0xEF155B24:
        // 0xEF155B24 texCoord
        i = mmParseJSON_DecodeSInt(p, ++i, &v->texCoord, 0);
        break;
    case 0xEF366486:
        // 0xEF366486 scale
        i = mmParseJSON_DecodeFloat(p, ++i, &v->scale, 1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFNormalTextureInfoExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFOcclusionTextureInfo_Init(struct mmGLTFOcclusionTextureInfo* p)
{
    p->index = -1;
    p->texCoord = 0;
    p->strength = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_transform = MM_FALSE;
    mmGLTFTextureTransform_Init(&p->ext_transform);
}
void mmGLTFOcclusionTextureInfo_Destroy(struct mmGLTFOcclusionTextureInfo* p)
{
    mmGLTFTextureTransform_Destroy(&p->ext_transform);
    p->has_transform = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->strength = 1;
    p->texCoord = 0;
    p->index = -1;
}

int mmParseJSON_DecodeGLTFOcclusionTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFOcclusionTextureInfo* v = (struct mmGLTFOcclusionTextureInfo*)(o);
    switch (h)
    {
    case 0x2FDC584B:
        // 0x2FDC584B KHR_texture_transform
        v->has_transform = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_transform, &mmParseJSON_DecodeGLTFTextureTransform);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFOcclusionTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFOcclusionTextureInfo* v = (struct mmGLTFOcclusionTextureInfo*)(o);
    switch (h)
    {
    case 0x0A3E2B42:
        // 0x0A3E2B42 index
        i = mmParseJSON_DecodeSInt(p, ++i, &v->index, -1);
        break;
    case 0xEF155B24:
        // 0xEF155B24 texCoord
        i = mmParseJSON_DecodeSInt(p, ++i, &v->texCoord, 0);
        break;
    case 0xE72722EA:
        // 0xE72722EA strength
        i = mmParseJSON_DecodeFloat(p, ++i, &v->strength, 1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFOcclusionTextureInfoExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsPbrMetallicRoughness_Init(struct mmGLTFMaterialsPbrMetallicRoughness* p)
{
    mmGLTFTextureInfo_Init(&p->baseColorTexture);
    mmGLTFTextureInfo_Init(&p->metallicRoughnessTexture);
    mmVec4AssignValue(p->baseColorFactor, 1);
    p->metallicFactor = 1;
    p->roughnessFactor = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsPbrMetallicRoughness_Destroy(struct mmGLTFMaterialsPbrMetallicRoughness* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->roughnessFactor = 1;
    p->metallicFactor = 1;
    mmVec4AssignValue(p->baseColorFactor, 1);
    mmGLTFTextureInfo_Destroy(&p->metallicRoughnessTexture);
    mmGLTFTextureInfo_Destroy(&p->baseColorTexture);
}

int mmParseJSON_DecodeGLTFMaterialsPbrMetallicRoughness(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsPbrMetallicRoughness* v = (struct mmGLTFMaterialsPbrMetallicRoughness*)(o);
    switch (h)
    {
    case 0x653802E1:
        // 0x653802E1 baseColorTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->baseColorTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x5689F2BE:
        // 0x5689F2BE metallicRoughnessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->metallicRoughnessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0xA15CF437:
        // 0xA15CF437 baseColorFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->baseColorFactor, &mmParseJSONFactorOne, 4, NULL);
        break;
    case 0xFA3CB3A2:
        // 0xFA3CB3A2 metallicFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->metallicFactor, 1);
        break;
    case 0xEFBAB5E3:
        // 0xEFBAB5E3 roughnessFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->roughnessFactor, 1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsPbrSpecularGlossiness_Init(struct mmGLTFMaterialsPbrSpecularGlossiness* p)
{
    mmGLTFTextureInfo_Init(&p->diffuseTexture);
    mmGLTFTextureInfo_Init(&p->specularGlossinessTexture);
    mmVec4AssignValue(p->diffuseFactor, 1);
    mmVec3AssignValue(p->specularFactor, 1);
    p->glossinessFactor = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsPbrSpecularGlossiness_Destroy(struct mmGLTFMaterialsPbrSpecularGlossiness* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->glossinessFactor = 1;
    mmVec3AssignValue(p->specularFactor, 1);
    mmVec4AssignValue(p->diffuseFactor, 1);
    mmGLTFTextureInfo_Destroy(&p->specularGlossinessTexture);
    mmGLTFTextureInfo_Destroy(&p->diffuseTexture);
}

int mmParseJSON_DecodeGLTFMaterialsPbrSpecularGlossiness(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsPbrSpecularGlossiness* v = (struct mmGLTFMaterialsPbrSpecularGlossiness*)(o);
    switch (h)
    {
    case 0x746ED9B5:
        // 0x746ED9B5 diffuseTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->diffuseTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x560CFE60:
        // 0x560CFE60 specularGlossinessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->specularGlossinessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x4C7FB9AA:
        // 0x4C7FB9AA diffuseFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->diffuseFactor, &mmParseJSONFactorOne, 4, NULL);
        break;
    case 0x00ACCB9E:
        // 0x00ACCB9E specularFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->specularFactor, &mmParseJSONFactorOne, 3, NULL);
        break;
    case 0xF480A009:
        // 0xF480A009 glossinessFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->glossinessFactor, 1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsClearcoat_Init(struct mmGLTFMaterialsClearcoat* p)
{
    mmGLTFTextureInfo_Init(&p->clearcoatTexture);
    mmGLTFTextureInfo_Init(&p->clearcoatRoughnessTexture);
    mmGLTFNormalTextureInfo_Init(&p->clearcoatNormalTexture);
    p->clearcoatFactor = 0;
    p->clearcoatRoughnessFactor = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsClearcoat_Destroy(struct mmGLTFMaterialsClearcoat* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->clearcoatRoughnessFactor = 0;
    p->clearcoatFactor = 0;
    mmGLTFNormalTextureInfo_Destroy(&p->clearcoatNormalTexture);
    mmGLTFTextureInfo_Destroy(&p->clearcoatRoughnessTexture);
    mmGLTFTextureInfo_Destroy(&p->clearcoatTexture);
}

int mmParseJSON_DecodeGLTFMaterialsClearcoat(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsClearcoat* v = (struct mmGLTFMaterialsClearcoat*)(o);
    switch (h)
    {
    case 0xE875EE5F:
        // 0xE875EE5F clearcoatTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->clearcoatTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x8DD603E2:
        // 0x8DD603E2 clearcoatRoughnessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->clearcoatRoughnessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0xAD6E0C80:
        // 0xAD6E0C80 clearcoatNormalTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->clearcoatNormalTexture, &mmParseJSON_DecodeGLTFNormalTextureInfo);
        break;
    case 0x25B85F15:
        // 0x25B85F15 clearcoatFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->clearcoatFactor, 0);
        break;
    case 0x569AE025:
        // 0x569AE025 clearcoatRoughnessFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->clearcoatRoughnessFactor, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsTransmission_Init(struct mmGLTFMaterialsTransmission* p)
{
    mmGLTFTextureInfo_Init(&p->transmissionTexture);
    p->transmissionFactor = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsTransmission_Destroy(struct mmGLTFMaterialsTransmission* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->transmissionFactor = 0;
    mmGLTFTextureInfo_Destroy(&p->transmissionTexture);
}

int mmParseJSON_DecodeGLTFMaterialsTransmission(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsTransmission* v = (struct mmGLTFMaterialsTransmission*)(o);
    switch (h)
    {
    case 0x09F84986:
        // 0x09F84986 transmissionTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->transmissionTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x3A1A773B:
        // 0x3A1A773B transmissionFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->transmissionFactor, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsIor_Init(struct mmGLTFMaterialsIor* p)
{
    p->ior = 1.5f;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsIor_Destroy(struct mmGLTFMaterialsIor* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->ior = 1.5f;
}

int mmParseJSON_DecodeGLTFMaterialsIor(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsIor* v = (struct mmGLTFMaterialsIor*)(o);
    switch (h)
    {
    case 0xBF9AA027:
        // 0xBF9AA027 ior
        i = mmParseJSON_DecodeFloat(p, ++i, &v->ior, 1.5f);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsSpecular_Init(struct mmGLTFMaterialsSpecular* p)
{
    mmGLTFTextureInfo_Init(&p->specularTexture);
    mmGLTFTextureInfo_Init(&p->specularColorTexture);
    mmVec3AssignValue(p->specularColorFactor, 1);
    p->specularFactor = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsSpecular_Destroy(struct mmGLTFMaterialsSpecular* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->specularFactor = 1;
    mmVec3AssignValue(p->specularColorFactor, 1);
    mmGLTFTextureInfo_Destroy(&p->specularColorTexture);
    mmGLTFTextureInfo_Destroy(&p->specularTexture);
}

int mmParseJSON_DecodeGLTFMaterialsSpecular(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsSpecular* v = (struct mmGLTFMaterialsSpecular*)(o);
    switch (h)
    {
    case 0x689903B5:
        // 0x689903B5 specularTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->specularTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0xC3BEAD34:
        // 0xC3BEAD34 specularColorTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->specularColorTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x87972D1D:
        // 0x87972D1D specularColorFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->specularColorFactor, &mmParseJSONFactorOne, 3, NULL);
        break;
    case 0x00ACCB9E:
        // 0x00ACCB9E specularFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->specularFactor, 1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsVolume_Init(struct mmGLTFMaterialsVolume* p)
{
    mmGLTFTextureInfo_Init(&p->thicknessTexture);
    p->thicknessFactor = 1;
    mmVec3AssignValue(p->attenuationColor, 1);
    p->attenuationDistance = 1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsVolume_Destroy(struct mmGLTFMaterialsVolume* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->attenuationDistance = 1;
    mmVec3AssignValue(p->attenuationColor, 1);
    p->thicknessFactor = 1;
    mmGLTFTextureInfo_Destroy(&p->thicknessTexture);
}

int mmParseJSON_DecodeGLTFMaterialsVolume(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsVolume* v = (struct mmGLTFMaterialsVolume*)(o);
    switch (h)
    {
    case 0x4013C71E:
        // 0x4013C71E thicknessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->thicknessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x20214B5A:
        // 0x20214B5A thicknessFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->thicknessFactor, 0);
        break;
    case 0x871FD7C8:
        // 0x871FD7C8 attenuationColor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->attenuationColor, &mmParseJSONFactorOne, 3, NULL);
        break;
    case 0xFEFDCAF1:
        // 0xFEFDCAF1 attenuationDistance
        i = mmParseJSON_DecodeFloat(p, ++i, &v->attenuationDistance, +INFINITY);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsSheen_Init(struct mmGLTFMaterialsSheen* p)
{
    mmGLTFTextureInfo_Init(&p->sheenColorTexture);
    mmGLTFTextureInfo_Init(&p->sheenRoughnessTexture);
    mmVec3AssignValue(p->sheenColorFactor, 0);
    p->sheenRoughnessFactor = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsSheen_Destroy(struct mmGLTFMaterialsSheen* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->sheenRoughnessFactor = 0;
    mmVec3AssignValue(p->sheenColorFactor, 0);
    mmGLTFTextureInfo_Destroy(&p->sheenRoughnessTexture);
    mmGLTFTextureInfo_Destroy(&p->sheenColorTexture);
}

int mmParseJSON_DecodeGLTFMaterialsSheen(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsSheen* v = (struct mmGLTFMaterialsSheen*)(o);
    switch (h)
    {
    case 0xA908BA70:
        // 0xA908BA70 sheenColorTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->sheenColorTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x89F6522C:
        // 0x89F6522C sheenRoughnessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->sheenRoughnessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0xA998C5E6:
        // 0xA998C5E6 sheenColorFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->sheenColorFactor, &MM_FLOAT_ZERO, 3, NULL);
        break;
    case 0x8971A12D:
        // 0x8971A12D sheenRoughnessFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->sheenRoughnessFactor, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsVariantsVariant_Init(struct mmGLTFMaterialsVariantsVariant* p)
{
    mmRange_Init(&p->name);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsVariantsVariant_Destroy(struct mmGLTFMaterialsVariantsVariant* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmRange_Destroy(&p->name);
}
int mmParseJSON_DecodeGLTFMaterialsVariantsVariant(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsVariantsVariant* v = (struct mmGLTFMaterialsVariantsVariant*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsVariantsMappingVariants_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(int));
}
void mmGLTFMaterialsVariantsMappingVariants_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMaterialsVariantsMapping_Init(struct mmGLTFMaterialsVariantsMapping* p)
{
    p->material = -1;
    mmGLTFMaterialsVariantsMappingVariants_Init(&p->variants);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsVariantsMapping_Destroy(struct mmGLTFMaterialsVariantsMapping* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFMaterialsVariantsMappingVariants_Destroy(&p->variants);
    p->material = -1;
}

int mmParseJSON_DecodeGLTFMaterialsVariantsMapping(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsVariantsMapping* v = (struct mmGLTFMaterialsVariantsMapping*)(o);
    switch (h)
    {
    case 0x46807A92:
        // 0x46807A92 material
        i = mmParseJSON_DecodeSInt(p, ++i, &v->material, -1);
        break;
    case 0xF9E1ECE0:
        // 0xF9E1ECE0 variants
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->variants, &MM_INT_ZERO, &mmValue_AToSInt);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsVariantsVariants_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFMaterialsVariantsVariant_Init;
    hValueAllocator.Recycle = &mmGLTFMaterialsVariantsVariant_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFMaterialsVariantsVariant));
}
void mmGLTFMaterialsVariantsVariants_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMaterialsVariantsMappings_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFMaterialsVariantsMapping_Init;
    hValueAllocator.Recycle = &mmGLTFMaterialsVariantsMapping_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFMaterialsVariantsMapping));
}
void mmGLTFMaterialsVariantsMappings_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMaterialsUnlit_Init(struct mmGLTFMaterialsUnlit* p)
{
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsUnlit_Destroy(struct mmGLTFMaterialsUnlit* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
}

int mmParseJSON_DecodeGLTFMaterialsUnlit(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsUnlit* v = (struct mmGLTFMaterialsUnlit*)(o);
    switch (h)
    {
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsVariants_Init(struct mmGLTFMaterialsVariants* p)
{
    mmGLTFMaterialsVariantsVariants_Init(&p->variants);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsVariants_Destroy(struct mmGLTFMaterialsVariants* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFMaterialsVariantsVariants_Destroy(&p->variants);
}

int mmParseJSON_DecodeGLTFMaterialsVariants(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsVariants* v = (struct mmGLTFMaterialsVariants*)(o);
    switch (h)
    {
    case 0xF9E1ECE0:
        // 0xF9E1ECE0 variants
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->variants, &mmParseJSON_DecodeGLTFMaterialsVariantsVariant);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsEmissiveStrength_Init(struct mmGLTFMaterialsEmissiveStrength* p)
{
    p->emissiveStrength = 1.0f;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsEmissiveStrength_Destroy(struct mmGLTFMaterialsEmissiveStrength* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->emissiveStrength = 1.0f;
}

int mmParseJSON_DecodeGLTFMaterialsEmissiveStrength(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsEmissiveStrength* v = (struct mmGLTFMaterialsEmissiveStrength*)(o);
    switch (h)
    {
    case 0x8335090B:
        // 0x8335090B emissiveStrength
        i = mmParseJSON_DecodeFloat(p, ++i, &v->emissiveStrength, 1.0f);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterialsIridescence_Init(struct mmGLTFMaterialsIridescence* p)
{
    p->iridescenceFactor = 0.0f;
    mmGLTFTextureInfo_Init(&p->iridescenceTexture);
    p->iridescenceIor = 1.3f;
    p->iridescenceThicknessMinimum = 100.0f;
    p->iridescenceThicknessMaximum = 400.0f;
    mmGLTFTextureInfo_Init(&p->iridescenceThicknessTexture);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMaterialsIridescence_Destroy(struct mmGLTFMaterialsIridescence* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFTextureInfo_Destroy(&p->iridescenceThicknessTexture);
    p->iridescenceThicknessMaximum = 400.0f;
    p->iridescenceThicknessMinimum = 100.0f;
    p->iridescenceIor = 1.3f;
    mmGLTFTextureInfo_Destroy(&p->iridescenceTexture);
    p->iridescenceFactor = 0.0f;
}

int mmParseJSON_DecodeGLTFMaterialsIridescence(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterialsIridescence* v = (struct mmGLTFMaterialsIridescence*)(o);
    switch (h)
    {
    case 0x2F2A61BE:
        // 0x2F2A61BE iridescenceFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->iridescenceFactor, 0.0f);
        break;
    case 0xF25DCDF6:
        // 0xF25DCDF6 iridescenceTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->iridescenceTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x3ED209E2:
        // 0x3ED209E2 iridescenceIor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->iridescenceIor, 1.3f);
        break;
    case 0x3B2D0194:
        // 0x3B2D0194 iridescenceThicknessMinimum
        i = mmParseJSON_DecodeFloat(p, ++i, &v->iridescenceThicknessMinimum, 100.0f);
        break;
    case 0xB43E3D19:
        // 0xB43E3D19 iridescenceThicknessMaximum
        i = mmParseJSON_DecodeFloat(p, ++i, &v->iridescenceThicknessMaximum, 400.0f);
        break;
    case 0xB7112D8D:
        // 0xB7112D8D iridescenceThicknessTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->iridescenceThicknessTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFPrimitiveVariants_Init(struct mmGLTFPrimitiveVariants* p)
{
    mmGLTFMaterialsVariantsMappings_Init(&p->mappings);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFPrimitiveVariants_Destroy(struct mmGLTFPrimitiveVariants* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFMaterialsVariantsMappings_Destroy(&p->mappings);
}

int mmParseJSON_DecodeGLTFPrimitiveVariants(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFPrimitiveVariants* v = (struct mmGLTFPrimitiveVariants*)(o);
    switch (h)
    {
    case 0xADA6F21B:
        // 0xADA6F21B mappings
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->mappings, &mmParseJSON_DecodeGLTFMaterialsVariantsMapping);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMaterial_Init(struct mmGLTFMaterial* p)
{
    mmRange_Init(&p->name);
    mmGLTFMaterialsPbrMetallicRoughness_Init(&p->pbrMetallicRoughness);
    mmGLTFNormalTextureInfo_Init(&p->normalTexture);
    mmGLTFOcclusionTextureInfo_Init(&p->occlusionTexture);
    mmGLTFTextureInfo_Init(&p->emissiveTexture);
    mmVec3AssignValue(p->emissiveFactor, 0);
    p->alphaMode = mmGLTFAlphaModeOpaque;
    p->alphaCutoff = 0.5f;
    p->doubleSided = MM_FALSE;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_clearcoat = MM_FALSE;
    p->has_ior = MM_FALSE;
    p->has_pbrSpecularGlossiness = MM_FALSE;
    p->has_sheen = MM_FALSE;
    p->has_specular = MM_FALSE;
    p->has_transmission = MM_FALSE;
    p->has_unlit = MM_FALSE;
    p->has_volume = MM_FALSE;
    p->has_emissiveStrength = MM_FALSE;
    p->has_iridescence = MM_FALSE;
    mmGLTFMaterialsClearcoat_Init(&p->ext_clearcoat);
    mmGLTFMaterialsIor_Init(&p->ext_ior);
    mmGLTFMaterialsPbrSpecularGlossiness_Init(&p->ext_pbrSpecularGlossiness);
    mmGLTFMaterialsSheen_Init(&p->ext_sheen);
    mmGLTFMaterialsSpecular_Init(&p->ext_specular);
    mmGLTFMaterialsTransmission_Init(&p->ext_transmission);
    mmGLTFMaterialsUnlit_Init(&p->ext_unlit);
    mmGLTFMaterialsVolume_Init(&p->ext_volume);
    mmGLTFMaterialsEmissiveStrength_Init(&p->ext_emissiveStrength);
    mmGLTFMaterialsIridescence_Init(&p->ext_iridescence);
}
void mmGLTFMaterial_Destroy(struct mmGLTFMaterial* p)
{
    mmGLTFMaterialsIridescence_Destroy(&p->ext_iridescence);
    mmGLTFMaterialsEmissiveStrength_Destroy(&p->ext_emissiveStrength);
    mmGLTFMaterialsVolume_Destroy(&p->ext_volume);
    mmGLTFMaterialsUnlit_Destroy(&p->ext_unlit);
    mmGLTFMaterialsTransmission_Destroy(&p->ext_transmission);
    mmGLTFMaterialsSpecular_Destroy(&p->ext_specular);
    mmGLTFMaterialsSheen_Destroy(&p->ext_sheen);
    mmGLTFMaterialsPbrSpecularGlossiness_Destroy(&p->ext_pbrSpecularGlossiness);
    mmGLTFMaterialsIor_Destroy(&p->ext_ior);
    mmGLTFMaterialsClearcoat_Destroy(&p->ext_clearcoat);
    p->has_iridescence = MM_FALSE;
    p->has_emissiveStrength = MM_FALSE;
    p->has_volume = MM_FALSE;
    p->has_unlit = MM_FALSE;
    p->has_transmission = MM_FALSE;
    p->has_specular = MM_FALSE;
    p->has_sheen = MM_FALSE;
    p->has_pbrSpecularGlossiness = MM_FALSE;
    p->has_ior = MM_FALSE;
    p->has_clearcoat = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->doubleSided = MM_FALSE;
    p->alphaCutoff = 0.5f;
    p->alphaMode = mmGLTFAlphaModeOpaque;
    mmVec3AssignValue(p->emissiveFactor, 0);
    mmGLTFTextureInfo_Destroy(&p->emissiveTexture);
    mmGLTFOcclusionTextureInfo_Destroy(&p->occlusionTexture);
    mmGLTFNormalTextureInfo_Destroy(&p->normalTexture);
    mmGLTFMaterialsPbrMetallicRoughness_Destroy(&p->pbrMetallicRoughness);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFMaterialExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterial* v = (struct mmGLTFMaterial*)(o);
    switch (h)
    {
    case 0xEBF30246:
        // 0xEBF30246 KHR_materials_clearcoat
        v->has_clearcoat = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_clearcoat, &mmParseJSON_DecodeGLTFMaterialsClearcoat);
        break;
    case 0x22931160:
        // 0x22931160 KHR_materials_ior
        v->has_ior = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_ior, &mmParseJSON_DecodeGLTFMaterialsIor);
        break;
    case 0x82920765:
        // 0x82920765 KHR_materials_pbrSpecularGlossiness
        v->has_pbrSpecularGlossiness = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_pbrSpecularGlossiness, &mmParseJSON_DecodeGLTFMaterialsPbrSpecularGlossiness);
        break;
    case 0xB4389BE3:
        // 0xB4389BE3 KHR_materials_sheen
        v->has_sheen = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_sheen, &mmParseJSON_DecodeGLTFMaterialsSheen);
        break;
    case 0x50F5D15D:
        // 0x50F5D15D KHR_materials_specular
        v->has_specular = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_specular, &mmParseJSON_DecodeGLTFMaterialsSpecular);
        break;
    case 0x72F0A2D9:
        // 0x72F0A2D9 KHR_materials_transmission
        v->has_transmission = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_transmission, &mmParseJSON_DecodeGLTFMaterialsTransmission);
        break;
    case 0xA04EA645:
        // 0xA04EA645 KHR_materials_unlit
        v->has_unlit = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_unlit, &mmParseJSON_DecodeGLTFMaterialsUnlit);
        break;
    case 0x2ED19E38:
        // 0x2ED19E38 KHR_materials_volume
        v->has_volume = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_volume, &mmParseJSON_DecodeGLTFMaterialsVolume);
        break;
    case 0x46B2C06A:
        // 0x46B2C06A KHR_materials_emissive_strength
        v->has_emissiveStrength = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_emissiveStrength, &mmParseJSON_DecodeGLTFMaterialsEmissiveStrength);
        break;
    case 0xE43EEDE9:
        // 0xE43EEDE9 KHR_materials_iridescence
        v->has_emissiveStrength = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_iridescence, &mmParseJSON_DecodeGLTFMaterialsIridescence);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFMaterial(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMaterial* v = (struct mmGLTFMaterial*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xD84CBE99:
        // 0xD84CBE99 pbrMetallicRoughness
        i = mmParseJSON_DecodeObject(p, ++i, &v->pbrMetallicRoughness, &mmParseJSON_DecodeGLTFMaterialsPbrMetallicRoughness);
        break;
    case 0xDB674EC1:
        // 0xDB674EC1 normalTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->normalTexture, &mmParseJSON_DecodeGLTFNormalTextureInfo);
        break;
    case 0x1AB38AC4:
        // 0x1AB38AC4 occlusionTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->occlusionTexture, &mmParseJSON_DecodeGLTFOcclusionTextureInfo);
        break;
    case 0x37031F01:
        // 0x37031F01 emissiveTexture
        i = mmParseJSON_DecodeObject(p, ++i, &v->emissiveTexture, &mmParseJSON_DecodeGLTFTextureInfo);
        break;
    case 0x48CFB04F:
        // 0x48CFB04F emissiveFactor
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->emissiveFactor, &mmParseJSONFactorOne, 3, NULL);
        break;
    case 0x45AB2E31:
        // 0x45AB2E31 alphaMode
        i = mmParseJSON_DecodeGLTFAlphaModeType(p, ++i, &v->alphaMode, mmGLTFAlphaModeOpaque);
        break;
    case 0xCCFF0C5F:
        // 0xCCFF0C5F alphaCutoff
        i = mmParseJSON_DecodeFloat(p, ++i, &v->alphaCutoff, 0.5f);
        break;
    case 0x0F34FD00:
        // 0x0F34FD00 doubleSided
        i = mmParseJSON_DecodeBool(p, ++i, &v->doubleSided, MM_FALSE);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFMaterialExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFDracoMeshCompression_Init(struct mmGLTFDracoMeshCompression* p)
{
    p->bufferView = -1;
    mmGLTFAttributes_Init(&p->attributes);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFDracoMeshCompression_Destroy(struct mmGLTFDracoMeshCompression* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFAttributes_Destroy(&p->attributes);
    p->bufferView = -1;
}

int mmParseJSON_DecodeGLTFDracoMeshCompression(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFDracoMeshCompression* v = (struct mmGLTFDracoMeshCompression*)(o);
    switch (h)
    {
    case 0x9D7A1248:
        // 0x9D7A1248 bufferView
        i = mmParseJSON_DecodeSInt(p, ++i, &v->bufferView, -1);
        break;
    case 0x69E8A568:
        // 0x69E8A568 attributes
        i = mmParseJSON_DecodeMember(p, ++i, &v->attributes, &mmParseJSON_DecodeGLTFAttribute);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFPrimitiveTargets_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAttributes_Init;
    hValueAllocator.Recycle = &mmGLTFAttributes_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVectorValue));
}
void mmGLTFPrimitiveTargets_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFPrimitive_Init(struct mmGLTFPrimitive* p)
{
    mmGLTFAttributes_Init(&p->attributes);
    mmGLTFPrimitiveTargets_Init(&p->targets);
    p->indices = -1;
    p->material = -1;
    p->mode = mmGLTFPrimitiveTypeTriangles;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_draco = MM_FALSE;
    p->has_variants = MM_FALSE;
    mmGLTFDracoMeshCompression_Init(&p->ext_draco);
    mmGLTFPrimitiveVariants_Init(&p->ext_variants);
}
void mmGLTFPrimitive_Destroy(struct mmGLTFPrimitive* p)
{
    mmGLTFPrimitiveVariants_Destroy(&p->ext_variants);
    mmGLTFDracoMeshCompression_Destroy(&p->ext_draco);
    p->has_variants = MM_FALSE;
    p->has_draco = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->mode = mmGLTFPrimitiveTypeTriangles;
    p->material = -1;
    p->indices = -1;
    mmGLTFPrimitiveTargets_Destroy(&p->targets);
    mmGLTFAttributes_Destroy(&p->attributes);
}

int mmParseJSON_DecodeGLTFPrimitiveExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFPrimitive* v = (struct mmGLTFPrimitive*)(o);
    switch (h)
    {
    case 0x33C146DA:
        // 0x33C146DA KHR_draco_mesh_compression
        v->has_draco = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_draco, &mmParseJSON_DecodeGLTFDracoMeshCompression);
        break;
    case 0x01820F80:
        // 0x01820F80 KHR_materials_variants
        v->has_variants = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_variants, &mmParseJSON_DecodeGLTFPrimitiveVariants);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFPrimitive(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFPrimitive* v = (struct mmGLTFPrimitive*)(o);
    switch (h)
    {
    case 0x69E8A568:
        // 0x69E8A568 attributes
        i = mmParseJSON_DecodeMember(p, ++i, &v->attributes, &mmParseJSON_DecodeGLTFAttribute);
        break;
    case 0xE0A4A38F:
        // 0xE0A4A38F targets
        i = mmParseJSON_DecodeMemberArray(p, ++i, &v->targets, &mmParseJSON_DecodeGLTFAttribute);
        break;
    case 0xA3875FAE:
        // 0xA3875FAE indices
        i = mmParseJSON_DecodeSInt(p, ++i, &v->indices, -1);
        break;
    case 0x46807A92:
        // 0x46807A92 material
        i = mmParseJSON_DecodeSInt(p, ++i, &v->material, -1);
        break;
    case 0x6C4AC7F7:
        // 0x6C4AC7F7 mode
        i = mmParseJSON_DecodeSInt(p, ++i, &v->mode, mmGLTFPrimitiveTypeTriangles);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFPrimitiveExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMeshPrimitives_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFPrimitive_Init;
    hValueAllocator.Recycle = &mmGLTFPrimitive_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFPrimitive));
}
void mmGLTFMeshPrimitives_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMeshWeights_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(float));
}
void mmGLTFMeshWeights_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMeshExtrasTargetNames_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmRange_Init;
    hValueAllocator.Recycle = &mmRange_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmRange));
}
void mmGLTFMeshExtrasTargetNames_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFMeshExtras_Init(struct mmGLTFMeshExtras* p)
{
    mmGLTFMeshExtrasTargetNames_Init(&p->targetNames);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFMeshExtras_Destroy(struct mmGLTFMeshExtras* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFMeshExtrasTargetNames_Destroy(&p->targetNames);
}
int mmParseJSON_DecodeGLTFMeshExtras(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMeshExtras* v = (struct mmGLTFMeshExtras*)(o);
    switch (h)
    {
    case 0xA8E0631C:
        // 0xA8E0631C targetNames
        i = mmParseJSON_DecodeRangeVector(p, ++i, &v->targetNames, &mmRangeZero);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFMesh_Init(struct mmGLTFMesh* p)
{
    mmRange_Init(&p->name);
    mmGLTFMeshPrimitives_Init(&p->primitives);
    mmGLTFMeshWeights_Init(&p->weights);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_extras = 0;
    mmGLTFMeshExtras_Init(&p->ext_extras);
}
void mmGLTFMesh_Destroy(struct mmGLTFMesh* p)
{
    mmGLTFMeshExtras_Destroy(&p->ext_extras);
    p->has_extras = 0;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFMeshWeights_Destroy(&p->weights);
    mmGLTFMeshPrimitives_Destroy(&p->primitives);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFMesh(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFMesh* v = (struct mmGLTFMesh*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x05289165:
        // 0x05289165 primitives
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->primitives, mmParseJSON_DecodeGLTFPrimitive);
        break;
    case 0xAE6E45DD:
        // 0xAE6E45DD weights
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->weights, &MM_FLOAT_ZERO, &mmValue_AToFloat);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        mmParseJSON_DecodeObject(p, i + 1, &v->ext_extras, &mmParseJSON_DecodeGLTFMeshExtras);
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFSkinJoints_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(int));
}
void mmGLTFSkinJoints_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFSkin_Init(struct mmGLTFSkin* p)
{
    mmRange_Init(&p->name);
    mmGLTFSkinJoints_Init(&p->joints);
    p->skeleton = -1;
    p->inverseBindMatrices = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFSkin_Destroy(struct mmGLTFSkin* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->inverseBindMatrices = -1;
    p->skeleton = -1;
    mmGLTFSkinJoints_Destroy(&p->joints);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFSkin(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFSkin* v = (struct mmGLTFSkin*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xE2B8B212:
        // 0xE2B8B212 joints
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->joints, &MM_SINT_ZERO, &mmValue_AToSInt);
        break;
    case 0x98EF0809:
        // 0x98EF0809 skeleton
        i = mmParseJSON_DecodeSInt(p, ++i, &v->skeleton, -1);
        break;
    case 0x5F0179B3:
        // 0x5F0179B3 inverseBindMatrices
        i = mmParseJSON_DecodeSInt(p, ++i, &v->inverseBindMatrices, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFCameraPerspective_Init(struct mmGLTFCameraPerspective* p)
{
    p->aspectRatio = 0;
    p->yfov = 0;
    p->zfar = 0;
    p->znear = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFCameraPerspective_Destroy(struct mmGLTFCameraPerspective* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->znear = 0;
    p->zfar = 0;
    p->yfov = 0;
    p->aspectRatio = 0;
}

int mmParseJSON_DecodeGLTFCameraPerspective(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFCameraPerspective* v = (struct mmGLTFCameraPerspective*)(o);
    switch (h)
    {
    case 0xD890B7FA:
        // 0xD890B7FA aspectRatio
        i = mmParseJSON_DecodeFloat(p, ++i, &v->aspectRatio, 0);
        break;
    case 0x63AA9CEC:
        // 0x63AA9CEC yfov
        i = mmParseJSON_DecodeFloat(p, ++i, &v->yfov, 0);
        break;
    case 0xFBBE0CD0:
        // 0xFBBE0CD0 zfar
        i = mmParseJSON_DecodeFloat(p, ++i, &v->zfar, 0);
        break;
    case 0x6540247A:
        // 0x6540247A znear
        i = mmParseJSON_DecodeFloat(p, ++i, &v->znear, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFCameraOrthographic_Init(struct mmGLTFCameraOrthographic* p)
{
    p->xmag = 0;
    p->ymag = 0;
    p->zfar = 0;
    p->znear = 0;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFCameraOrthographic_Destroy(struct mmGLTFCameraOrthographic* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->znear = 0;
    p->zfar = 0;
    p->ymag = 0;
    p->xmag = 0;
}

int mmParseJSON_DecodeGLTFCameraOrthographic(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFCameraOrthographic* v = (struct mmGLTFCameraOrthographic*)(o);
    switch (h)
    {
    case 0xF9563726:
        // 0xF9563726 xmag
        i = mmParseJSON_DecodeFloat(p, ++i, &v->xmag, 0);
        break;
    case 0xDD27F2A2:
        // 0xDD27F2A2 ymag
        i = mmParseJSON_DecodeFloat(p, ++i, &v->ymag, 0);
        break;
    case 0xFBBE0CD0:
        // 0xFBBE0CD0 zfar
        i = mmParseJSON_DecodeFloat(p, ++i, &v->zfar, 0);
        break;
    case 0x6540247A:
        // 0x6540247A znear
        i = mmParseJSON_DecodeFloat(p, ++i, &v->znear, 0);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFCamera_Init(struct mmGLTFCamera* p)
{
    mmRange_Init(&p->name);
    p->type = mmGLTFCameraTypeInvalid;
    mmGLTFCameraPerspective_Init(&p->perspective);
    mmGLTFCameraOrthographic_Init(&p->orthographic);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFCamera_Destroy(struct mmGLTFCamera* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFCameraOrthographic_Destroy(&p->orthographic);
    mmGLTFCameraPerspective_Destroy(&p->perspective);
    p->type = mmGLTFCameraTypeInvalid;
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFCamera(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFCamera* v = (struct mmGLTFCamera*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x6A235628:
        // 0x6A235628 type
        i = mmParseJSON_DecodeGLTFCameraType(p, ++i, &v->type, mmGLTFCameraTypeInvalid);
        break;
    case 0x329EF6A4:
        // 0x329EF6A4 perspective
        i = mmParseJSON_DecodeObject(p, ++i, &v->perspective, &mmParseJSON_DecodeGLTFCameraPerspective);
        break;
    case 0x5393C9A6:
        // 0x5393C9A6 orthographic
        i = mmParseJSON_DecodeObject(p, ++i, &v->orthographic, &mmParseJSON_DecodeGLTFCameraOrthographic);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFLightSpot_Init(struct mmGLTFLightSpot* p)
{
    p->innerConeAngle = 0;
    p->innerConeAngle = (float)MM_MATH_PI_DIV_4;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFLightSpot_Destroy(struct mmGLTFLightSpot* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->innerConeAngle = (float)MM_MATH_PI_DIV_4;
    p->innerConeAngle = 0;
}

int mmParseJSON_DecodeGLTFLightSpot(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFLightSpot* v = (struct mmGLTFLightSpot*)(o);
    switch (h)
    {
    case 0x52EA8360:
        // 0x52EA8360 innerConeAngle
        i = mmParseJSON_DecodeFloat(p, ++i, &v->innerConeAngle, 0);
        break;
    case 0x4416C1EE:
        // 0x4416C1EE outerConeAngle
        i = mmParseJSON_DecodeFloat(p, ++i, &v->outerConeAngle, (float)MM_MATH_PI_DIV_4);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFLight_Init(struct mmGLTFLight* p)
{
    mmRange_Init(&p->name);
    mmVec3AssignValue(p->color, 1);
    p->intensity = 1;
    p->type = mmGLTFLightTypeInvalid;
    p->range = +INFINITY;
    mmGLTFLightSpot_Init(&p->spot);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFLight_Destroy(struct mmGLTFLight* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFLightSpot_Destroy(&p->spot);
    p->range = +INFINITY;
    p->type = mmGLTFLightTypeInvalid;
    p->intensity = 1;
    mmVec3AssignValue(p->color, 1);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFLight(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFLight* v = (struct mmGLTFLight*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xFE7B3339:
        // 0xFE7B3339 color
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->color, &mmParseJSONFactorOne, 3, NULL);
        break;
    case 0x3D836879:
        // 0x3D836879 intensity
        i = mmParseJSON_DecodeFloat(p, ++i, &v->intensity, 1);
        break;
    case 0x6A235628:
        // 0x6A235628 type
        i = mmParseJSON_DecodeGLTFLightType(p, ++i, &v->type, mmGLTFLightTypeInvalid);
        break;
    case 0x218428DC:
        // 0x218428DC range
        i = mmParseJSON_DecodeFloat(p, ++i, &v->range, +INFINITY);
        break;
    case 0xF39A54E5:
        // 0xF39A54E5 spot
        i = mmParseJSON_DecodeObject(p, ++i, &v->spot, &mmParseJSON_DecodeGLTFLightSpot);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFLights_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFLight_Init;
    hValueAllocator.Recycle = &mmGLTFLight_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFLight));
}
void mmGLTFLights_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFLightPunctual_Init(struct mmGLTFLightPunctual* p)
{
    mmGLTFLights_Init(&p->lights);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFLightPunctual_Destroy(struct mmGLTFLightPunctual* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFLights_Destroy(&p->lights);
}

int mmParseJSON_DecodeGLTFLightPunctual(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFLightPunctual* v = (struct mmGLTFLightPunctual*)(o);
    switch (h)
    {
    case 0x8C3D3145:
        // 0x8C3D3145 lights
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->lights, &mmParseJSON_DecodeGLTFLight);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFNodeLight_Init(struct mmGLTFNodeLight* p)
{
    p->light = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFNodeLight_Destroy(struct mmGLTFNodeLight* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->light = -1;
}

int mmParseJSON_DecodeGLTFNodeLight(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFNodeLight* v = (struct mmGLTFNodeLight*)(o);
    switch (h)
    {
    case 0x4CF76C7B:
        // 0x4CF76C7B light
        i = mmParseJSON_DecodeSInt(p, ++i, &v->light, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFNodeChildren_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(int));
}
void mmGLTFNodeChildren_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFNodeWeights_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(float));
}
void mmGLTFNodeWeights_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFNode_Init(struct mmGLTFNode* p)
{
    mmRange_Init(&p->name);
    mmGLTFNodeChildren_Init(&p->children);
    mmGLTFNodeWeights_Init(&p->weights);
    mmVec3Make(p->translation, 0, 0, 0);
    mmVec4Make(p->rotation, 0, 0, 0, 1);
    mmVec3Make(p->scale, 1, 1, 1);
    mmMat4x4MakeIdentity(p->matrix);
    p->camera = -1;
    p->skin = -1;
    p->mesh = -1;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
    
    p->parent = NULL;

    p->sizeTranslation = 0;
    p->sizeRotation = 0;
    p->sizeScale = 0;
    p->sizeMatrix = 0;

    p->has_light = MM_FALSE;
    mmGLTFNodeLight_Init(&p->ext_light);
}
void mmGLTFNode_Destroy(struct mmGLTFNode* p)
{
    mmGLTFNodeLight_Destroy(&p->ext_light);
    p->has_light = MM_FALSE;

    p->sizeMatrix = 0;
    p->sizeScale = 0;
    p->sizeRotation = 0;
    p->sizeTranslation = 0;

    p->parent = NULL;
    
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->mesh = -1;
    p->skin = -1;
    p->camera = -1;
    mmMat4x4MakeIdentity(p->matrix);
    mmVec3Make(p->scale, 1, 1, 1);
    mmVec4Make(p->rotation, 0, 0, 0, 1);
    mmVec3Make(p->translation, 0, 0, 0);
    mmGLTFNodeWeights_Destroy(&p->weights);
    mmGLTFNodeChildren_Destroy(&p->children);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFNodeExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFNode* v = (struct mmGLTFNode*)(o);
    switch (h)
    {
    case 0x6881A6AB:
        // 0x6881A6AB KHR_lights_punctual
        v->has_light = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_light, &mmParseJSON_DecodeGLTFNodeLight);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFNode(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFNode* v = (struct mmGLTFNode*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xE5949022:
        // 0xE5949022 children
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->children, &MM_INT_ZERO, &mmValue_AToSInt);
        break;
    case 0xAE6E45DD:
        // 0xAE6E45DD weights
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->weights, &MM_FLOAT_ZERO, &mmValue_AToFloat);
        break;
    case 0x25A069A3:
        // 0x25A069A3 translation
        mmVec3Make(v->translation, 0, 0, 0);
        i = mmParseJSON_DecodeFloatArray(p, ++i, &v->translation, &MM_FLOAT_ZERO, 3, &v->sizeTranslation);
        break;
    case 0xA3FE4DF9:
        // 0xA3FE4DF9 rotation
        mmVec4Make(v->rotation, 0, 0, 0, 1);
        i = mmParseJSON_DecodeFloatArray(p, ++i, &v->rotation, &MM_FLOAT_ZERO, 4, &v->sizeRotation);
        break;
    case 0xEF366486:
        // 0xEF366486 scale
        mmVec3Make(v->scale, 1, 1, 1);
        i = mmParseJSON_DecodeFloatArray(p, ++i, &v->scale, &MM_FLOAT_ZERO, 3, &v->sizeScale);
        break;
    case 0x9F470338:
        // 0x9F470338 matrix
        mmMat4x4MakeIdentity(v->matrix);
        i = mmParseJSON_DecodeFloatArray(p, ++i, &v->matrix, &MM_FLOAT_ZERO, 16, &v->sizeMatrix);
        break;
    case 0x5005AC7A:
        // 0x5005AC7A camera
        i = mmParseJSON_DecodeSInt(p, ++i, &v->camera, -1);
        break;
    case 0x7335AA59:
        // 0x7335AA59 skin
        i = mmParseJSON_DecodeSInt(p, ++i, &v->skin, -1);
        break;
    case 0x742FBC9A:
        // 0x742FBC9A mesh
        i = mmParseJSON_DecodeSInt(p, ++i, &v->mesh, -1);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFNodeExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmGLTFNode_GetLigit(const struct mmGLTFNode* p)
{
    return p->has_light ? p->ext_light.light : -1;
}

void mmGLTFSceneNodes_Init(struct mmVectorValue* p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(int));
}
void mmGLTFSceneNodes_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFScene_Init(struct mmGLTFScene* p)
{
    mmRange_Init(&p->name);
    mmGLTFSceneNodes_Init(&p->nodes);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFScene_Destroy(struct mmGLTFScene* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFSceneNodes_Destroy(&p->nodes);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFScene(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFScene* v = (struct mmGLTFScene*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0x8837BDFC:
        // 0x8837BDFC nodes
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->nodes, &MM_INT_ZERO, &mmValue_AToSInt);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAnimationSampler_Init(struct mmGLTFAnimationSampler* p)
{
    p->input = -1;
    p->output = -1;
    p->interpolation = mmGLTFInterpolationTypeLinear;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAnimationSampler_Destroy(struct mmGLTFAnimationSampler* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->interpolation = mmGLTFInterpolationTypeLinear;
    p->output = -1;
    p->input = -1;
}

int mmParseJSON_DecodeGLTFAnimationSampler(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAnimationSampler* v = (struct mmGLTFAnimationSampler*)(o);
    switch (h)
    {
    case 0xC1A28470:
        // 0xC1A28470 input
        i = mmParseJSON_DecodeSInt(p, ++i, &v->input, -1);
        break;
    case 0xCA2E69BD:
        // 0xCA2E69BD output
        i = mmParseJSON_DecodeSInt(p, ++i, &v->output, -1);
        break;
    case 0x774A6607:
        // 0x774A6607 interpolation
        i = mmParseJSON_DecodeGLTFInterpolationType(p, ++i, &v->interpolation, mmGLTFInterpolationTypeLinear);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAnimationChannelTarget_Init(struct mmGLTFAnimationChannelTarget* p)
{
    p->node = -1;
    p->path = mmGLTFAnimationPathTypeInvalid;
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAnimationChannelTarget_Destroy(struct mmGLTFAnimationChannelTarget* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    p->path = mmGLTFAnimationPathTypeInvalid;
    p->node = -1;
}

int mmParseJSON_DecodeGLTFAnimationChannelTarget(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAnimationChannelTarget* v = (struct mmGLTFAnimationChannelTarget*)(o);
    switch (h)
    {
    case 0xA6D07F2C:
        // 0xA6D07F2C node
        i = mmParseJSON_DecodeSInt(p, ++i, &v->node, -1);
        break;
    case 0x27A2D463:
        // 0x27A2D463 path
        i = mmParseJSON_DecodeGLTFAnimationPathType(p, ++i, &v->path, mmGLTFAnimationPathTypeInvalid);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAnimationChannel_Init(struct mmGLTFAnimationChannel* p)
{
    p->sampler = -1;
    mmGLTFAnimationChannelTarget_Init(&p->target);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAnimationChannel_Destroy(struct mmGLTFAnimationChannel* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFAnimationChannelTarget_Destroy(&p->target);
    p->sampler = -1;
}

int mmParseJSON_DecodeGLTFAnimationChannel(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAnimationChannel* v = (struct mmGLTFAnimationChannel*)(o);
    switch (h)
    {
    case 0x82B39DE8:
        // 0x82B39DE8 sampler
        i = mmParseJSON_DecodeSInt(p, ++i, &v->sampler, -1);
        break;
    case 0x6E17A098:
        // 0x6E17A098 target
        i = mmParseJSON_DecodeObject(p, ++i, &v->target, &mmParseJSON_DecodeGLTFAnimationChannelTarget);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAnimationSamplers_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAnimationSampler_Init;
    hValueAllocator.Recycle = &mmGLTFAnimationSampler_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFAnimationSampler));
}
void mmGLTFAnimationSamplers_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFAnimationChannels_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAnimationChannel_Init;
    hValueAllocator.Recycle = &mmGLTFAnimationChannel_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFAnimationChannel));
}
void mmGLTFAnimationChannels_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFAnimation_Init(struct mmGLTFAnimation* p)
{
    mmRange_Init(&p->name);
    mmGLTFAnimationSamplers_Init(&p->samplers);
    mmGLTFAnimationChannels_Init(&p->channels);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAnimation_Destroy(struct mmGLTFAnimation* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFAnimationChannels_Destroy(&p->channels);
    mmGLTFAnimationSamplers_Destroy(&p->samplers);
    mmRange_Destroy(&p->name);
}

int mmParseJSON_DecodeGLTFAnimation(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAnimation* v = (struct mmGLTFAnimation*)(o);
    switch (h)
    {
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeRange(p, ++i, &v->name, &mmRangeZero);
        break;
    case 0xA4C91136:
        // 0xA4C91136 samplers
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->samplers, &mmParseJSON_DecodeGLTFAnimationSampler);
        break;
    case 0xE2F6F45A:
        // 0xE2F6F45A channels
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->channels, &mmParseJSON_DecodeGLTFAnimationChannel);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFAsset_Init(struct mmGLTFAsset* p)
{
    mmRange_Init(&p->copyright);
    mmRange_Init(&p->generator);
    mmRange_Init(&p->version);
    mmRange_Init(&p->minVersion);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);
}
void mmGLTFAsset_Destroy(struct mmGLTFAsset* p)
{
    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmRange_Destroy(&p->minVersion);
    mmRange_Destroy(&p->version);
    mmRange_Destroy(&p->generator);
    mmRange_Destroy(&p->copyright);
}
void mmGLTFAsset_Reset(struct mmGLTFAsset* p)
{
    mmRange_Reset(&p->copyright);
    mmRange_Reset(&p->generator);
    mmRange_Reset(&p->version);
    mmRange_Reset(&p->minVersion);
    mmGLTFExtras_Reset(&p->extras);
    mmVectorValue_Reset(&p->extensions);
}

int mmParseJSON_DecodeGLTFAsset(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFAsset* v = (struct mmGLTFAsset*)(o);
    switch (h)
    {
    case 0x095B5179:
        // 0x095B5179 copyright
        i = mmParseJSON_DecodeRange(p, ++i, &v->copyright, &mmRangeZero);
        break;
    case 0xEAA63610:
        // 0xEAA63610 generator
        i = mmParseJSON_DecodeRange(p, ++i, &v->generator, &mmRangeZero);
        break;
    case 0xEE2827F5:
        // 0xEE2827F5 version
        i = mmParseJSON_DecodeRange(p, ++i, &v->version, &mmRangeZero);
        break;
    case 0x8A73BBC6:
        // 0x8A73BBC6 minVersion
        i = mmParseJSON_DecodeRange(p, ++i, &v->minVersion, &mmRangeZero);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

void mmGLTFRootAccessors_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAccessor_Init;
    hValueAllocator.Recycle = &mmGLTFAccessor_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFAccessor));
}
void mmGLTFRootAccessors_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootBuffers_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFBuffer_Init;
    hValueAllocator.Recycle = &mmGLTFBuffer_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFBuffer));
}
void mmGLTFRootBuffers_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootBufferViews_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFBufferView_Init;
    hValueAllocator.Recycle = &mmGLTFBufferView_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFBufferView));
}
void mmGLTFRootBufferViews_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootImages_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFImage_Init;
    hValueAllocator.Recycle = &mmGLTFImage_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFImage));
}
void mmGLTFRootImages_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootSamplers_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFSampler_Init;
    hValueAllocator.Recycle = &mmGLTFSampler_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFSampler));
}
void mmGLTFRootSamplers_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootTextures_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFTexture_Init;
    hValueAllocator.Recycle = &mmGLTFTexture_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFTexture));
}
void mmGLTFRootTextures_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootMaterials_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFMaterial_Init;
    hValueAllocator.Recycle = &mmGLTFMaterial_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFMaterial));
}
void mmGLTFRootMaterials_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootSkins_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFSkin_Init;
    hValueAllocator.Recycle = &mmGLTFSkin_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFSkin));
}
void mmGLTFRootSkins_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootAnimations_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFAnimation_Init;
    hValueAllocator.Recycle = &mmGLTFAnimation_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFAnimation));
}
void mmGLTFRootAnimations_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootMeshes_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFMesh_Init;
    hValueAllocator.Recycle = &mmGLTFMesh_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFMesh));
}
void mmGLTFRootMeshes_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootCameras_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFCamera_Init;
    hValueAllocator.Recycle = &mmGLTFCamera_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFCamera));
}
void mmGLTFRootCameras_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootNodes_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFNode_Init;
    hValueAllocator.Recycle = &mmGLTFNode_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFNode));
}
void mmGLTFRootNodes_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRootScenes_Init(struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmGLTFScene_Init;
    hValueAllocator.Recycle = &mmGLTFScene_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmGLTFScene));
}
void mmGLTFRootScenes_Destroy(struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void mmGLTFRoot_Init(struct mmGLTFRoot* p)
{
    p->scene = -1;
    mmGLTFAsset_Init(&p->asset);

    mmGLTFRootAccessors_Init(&p->accessors);
    mmGLTFRootBuffers_Init(&p->buffers);
    mmGLTFRootBufferViews_Init(&p->bufferViews);
    mmGLTFRootImages_Init(&p->images);
    mmGLTFRootSamplers_Init(&p->samplers);
    mmGLTFRootTextures_Init(&p->textures);
    mmGLTFRootMaterials_Init(&p->materials);
    mmGLTFRootSkins_Init(&p->skins);
    mmGLTFRootAnimations_Init(&p->animations);
    mmGLTFRootMeshes_Init(&p->meshes);
    mmGLTFRootCameras_Init(&p->cameras);
    mmGLTFRootNodes_Init(&p->nodes);
    mmGLTFRootScenes_Init(&p->scenes);

    mmGLTFRanges_Init(&p->extensionsUsed);
    mmGLTFRanges_Init(&p->extensionsRequired);
    mmGLTFExtras_Init(&p->extras);
    mmGLTFExtensions_Init(&p->extensions);

    p->has_lights = MM_FALSE;
    p->has_variants = MM_FALSE;
    mmGLTFLightPunctual_Init(&p->ext_lights);
    mmGLTFMaterialsVariants_Init(&p->ext_variants);
}
void mmGLTFRoot_Destroy(struct mmGLTFRoot* p)
{
    mmGLTFMaterialsVariants_Destroy(&p->ext_variants);
    mmGLTFLightPunctual_Destroy(&p->ext_lights);
    p->has_variants = MM_FALSE;
    p->has_lights = MM_FALSE;

    mmGLTFExtensions_Destroy(&p->extensions);
    mmGLTFExtras_Destroy(&p->extras);
    mmGLTFRanges_Destroy(&p->extensionsRequired);
    mmGLTFRanges_Destroy(&p->extensionsUsed);

    mmGLTFRootScenes_Destroy(&p->scenes);
    mmGLTFRootNodes_Destroy(&p->nodes);
    mmGLTFRootCameras_Destroy(&p->cameras);
    mmGLTFRootMeshes_Destroy(&p->meshes);
    mmGLTFRootAnimations_Destroy(&p->animations);
    mmGLTFRootSkins_Destroy(&p->skins);
    mmGLTFRootMaterials_Destroy(&p->materials);
    mmGLTFRootTextures_Destroy(&p->textures);
    mmGLTFRootSamplers_Destroy(&p->samplers);
    mmGLTFRootImages_Destroy(&p->images);
    mmGLTFRootBufferViews_Destroy(&p->bufferViews);
    mmGLTFRootBuffers_Destroy(&p->buffers);
    mmGLTFRootAccessors_Destroy(&p->accessors);

    mmGLTFAsset_Destroy(&p->asset);
    p->scene = -1;
}
void mmGLTFRoot_Reset(struct mmGLTFRoot* p)
{
    p->scene = -1;
    mmGLTFAsset_Reset(&p->asset);

    mmVectorValue_Reset(&p->accessors);
    mmVectorValue_Reset(&p->buffers);
    mmVectorValue_Reset(&p->bufferViews);
    mmVectorValue_Reset(&p->images);
    mmVectorValue_Reset(&p->samplers);
    mmVectorValue_Reset(&p->textures);
    mmVectorValue_Reset(&p->materials);
    mmVectorValue_Reset(&p->skins);
    mmVectorValue_Reset(&p->animations);
    mmVectorValue_Reset(&p->meshes);
    mmVectorValue_Reset(&p->cameras);
    mmVectorValue_Reset(&p->nodes);
    mmVectorValue_Reset(&p->scenes);

    mmVectorValue_Reset(&p->extensionsUsed);
    mmVectorValue_Reset(&p->extensionsRequired);
    mmGLTFExtras_Reset(&p->extras);
    mmVectorValue_Reset(&p->extensions);
}

static void mmGLTFNodeParentContact(struct mmVectorValue* nodes)
{
    size_t i = 0;
    size_t j = 0;
    int* id = 0;
    struct mmGLTFNode* node = NULL;
    struct mmGLTFNode* child = NULL;
    for (i = 0; i < nodes->size; ++i)
    {
        node = (struct mmGLTFNode*)mmVectorValue_At(nodes, i);
        for (j = 0; j < node->children.size; ++j)
        {
            id = (int*)mmVectorValue_At(&node->children, j);
            child = (struct mmGLTFNode*)mmVectorValue_At(nodes, (*id));
            child->parent = node;
        }
    }
}

int mmParseJSON_DecodeGLTFRootExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFRoot* v = (struct mmGLTFRoot*)(o);
    switch (h)
    {
    case 0x6881A6AB:
        // 0x6881A6AB KHR_lights_punctual
        v->has_lights = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_lights, &mmParseJSON_DecodeGLTFLightPunctual);
        break;
    case 0x01820F80:
        // 0x01820F80 KHR_materials_variants
        v->has_variants = MM_TRUE;
        i = mmParseJSON_DecodeObject(p, ++i, &v->ext_variants, &mmParseJSON_DecodeGLTFMaterialsVariants);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int mmParseJSON_DecodeGLTFRoot(struct mmParseJSON* p, int i, mmUInt32_t h, void* o)
{
    struct mmGLTFRoot* v = (struct mmGLTFRoot*)(o);
    switch (h)
    {
    case 0x72F7BC57:
        // 0x72F7BC57 scene
        i = mmParseJSON_DecodeSInt(p, ++i, &v->scene, -1);
        break;
    case 0x5CFF184F:
        // 0x5CFF184F asset
        i = mmParseJSON_DecodeObject(p, ++i, &v->asset, &mmParseJSON_DecodeGLTFAsset);
        break;
    case 0x82377777:
        // 0x82377777 accessors
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->accessors, &mmParseJSON_DecodeGLTFAccessor);
        break;
    case 0x800343D5:
        // 0x800343D5 buffers
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->buffers, &mmParseJSON_DecodeGLTFBuffer);
        break;
    case 0x3E157BF1:
        // 0x3E157BF1 bufferViews
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->bufferViews, &mmParseJSON_DecodeGLTFBufferView);
        break;
    case 0xB7817D2C:
        // 0xB7817D2C images
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->images, &mmParseJSON_DecodeGLTFImage);
        break;
    case 0xA4C91136:
        // 0xA4C91136 samplers
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->samplers, &mmParseJSON_DecodeGLTFSampler);
        break;
    case 0xCB41FA0F:
        // 0xCB41FA0F textures
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->textures, &mmParseJSON_DecodeGLTFTexture);
        break;
    case 0x64ACC8A6:
        // 0x64ACC8A6 materials
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->materials, &mmParseJSON_DecodeGLTFMaterial);
        break;
    case 0xE486975E:
        // 0xE486975E skins
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->skins, &mmParseJSON_DecodeGLTFSkin);
        break;
    case 0xF372351A:
        // 0xF372351A animations
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->animations, &mmParseJSON_DecodeGLTFAnimation);
        break;
    case 0xE567B165:
        // 0xE567B165 meshes
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->meshes, &mmParseJSON_DecodeGLTFMesh);
        break;
    case 0x24855D89:
        // 0x24855D89 cameras
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->cameras, &mmParseJSON_DecodeGLTFCamera);
        break;
    case 0x8837BDFC:
        // 0x8837BDFC nodes
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->nodes, &mmParseJSON_DecodeGLTFNode);
        mmGLTFNodeParentContact(&v->nodes);
        break;
    case 0x61E93677:
        // 0x61E93677 scenes
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->scenes, &mmParseJSON_DecodeGLTFScene);
        break;
    case 0x3697B843:
        // 0x3697B843 extensionsUsed
        i = mmParseJSON_DecodeRangeVector(p, ++i, &v->extensionsUsed, &mmRangeZero);
        break;
    case 0x5797D28F:
        // 0x5797D28F extensionsRequired
        i = mmParseJSON_DecodeRangeVector(p, ++i, &v->extensionsRequired, &mmRangeZero);
        break;
    case 0x034338A9:
        // 0x034338A9 extras
        i = mmParseJSON_DecodeGLTFExtras(p, ++i, &v->extras);
        break;
    case 0x29A01550:
        // 0x29A01550 extensions
        mmParseJSON_DecodeObject(p, i + 1, v, &mmParseJSON_DecodeGLTFRootExtension);
        i = mmParseJSON_DecodeMember(p, ++i, &v->extensions, &mmParseJSON_DecodeGLTFExtension);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

struct mmGLTFAccessor* mmGLTFRoot_GetAccessor(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFAccessor*)mmVectorValue_At(&p->accessors, idx);
}
struct mmGLTFBuffer* mmGLTFRoot_GetBuffer(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFBuffer*)mmVectorValue_At(&p->buffers, idx);
}
struct mmGLTFBufferView* mmGLTFRoot_GetBufferView(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFBufferView*)mmVectorValue_At(&p->bufferViews, idx);
}
struct mmGLTFImage* mmGLTFRoot_GetImage(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFImage*)mmVectorValue_At(&p->images, idx);
}
struct mmGLTFSampler* mmGLTFRoot_GetSampler(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFSampler*)mmVectorValue_At(&p->samplers, idx);
}
struct mmGLTFTexture* mmGLTFRoot_GetTexture(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFTexture*)mmVectorValue_At(&p->textures, idx);
}
struct mmGLTFMaterial* mmGLTFRoot_GetMaterial(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFMaterial*)mmVectorValue_At(&p->materials, idx);
}
struct mmGLTFSkin* mmGLTFRoot_GetSkin(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFSkin*)mmVectorValue_At(&p->skins, idx);
}
struct mmGLTFAnimation* mmGLTFRoot_GetAnimation(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFAnimation*)mmVectorValue_At(&p->animations, idx);
}
struct mmGLTFMesh* mmGLTFRoot_GetMesh(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFMesh*)mmVectorValue_At(&p->meshes, idx);
}
struct mmGLTFCamera* mmGLTFRoot_GetCamera(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFCamera*)mmVectorValue_At(&p->cameras, idx);
}
struct mmGLTFNode* mmGLTFRoot_GetNode(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFNode*)mmVectorValue_At(&p->nodes, idx);
}
struct mmGLTFScene* mmGLTFRoot_GetScene(struct mmGLTFRoot* p, size_t idx)
{
    return (struct mmGLTFScene*)mmVectorValue_At(&p->scenes, idx);
}
struct mmGLTFLight* mmGLTFRoot_GetLight(struct mmGLTFRoot* p, size_t idx)
{
    if (p->has_lights)
    {
        return (struct mmGLTFLight*)mmVectorValue_At(&p->ext_lights.lights, idx);
    }
    else
    {
        return NULL;
    }
}

void mmGLTFHeader_Init(struct mmGLTFHeader* p)
{
    p->magic = 0;
    p->version = 0;
    p->length = 0;
}
void mmGLTFHeader_Destroy(struct mmGLTFHeader* p)
{
    p->length = 0;
    p->version = 0;
    p->magic = 0;
}
void mmGLTFHeader_Reset(struct mmGLTFHeader* p)
{
    p->magic = 0;
    p->version = 0;
    p->length = 0;
}

void mmGLTFHeader_Encode(struct mmGLTFHeader* p, struct mmByteBuffer* pByteBuffer, size_t* offset)
{
    mmByteOrder_ByteLittleEncodeU32(pByteBuffer, offset, &p->magic);
    mmByteOrder_ByteLittleEncodeU32(pByteBuffer, offset, &p->version);
    mmByteOrder_ByteLittleEncodeU32(pByteBuffer, offset, &p->length);
}
void mmGLTFHeader_Decode(struct mmGLTFHeader* p, struct mmByteBuffer* pByteBuffer, size_t* offset)
{
    mmByteOrder_ByteLittleDecodeU32(pByteBuffer, offset, &p->magic);
    mmByteOrder_ByteLittleDecodeU32(pByteBuffer, offset, &p->version);
    mmByteOrder_ByteLittleDecodeU32(pByteBuffer, offset, &p->length);
}

void mmGLTFChunk_Init(struct mmGLTFChunk* p)
{
    p->chunkLength = 0;
    p->chunkType = 0;
    p->chunkData = NULL;
}
void mmGLTFChunk_Destroy(struct mmGLTFChunk* p)
{
    p->chunkData = NULL;
    p->chunkType = 0;
    p->chunkLength = 0;
}
void mmGLTFChunk_Reset(struct mmGLTFChunk* p)
{
    p->chunkLength = 0;
    p->chunkType = 0;
    p->chunkData = NULL;
}
void mmGLTFChunk_Encode(struct mmGLTFChunk* p, struct mmByteBuffer* pByteBuffer, size_t* offset)
{
    mmByteOrder_ByteLittleEncodeU32(pByteBuffer, offset, &p->chunkLength);
    mmByteOrder_ByteLittleEncodeU32(pByteBuffer, offset, &p->chunkType);
}
void mmGLTFChunk_Decode(struct mmGLTFChunk* p, struct mmByteBuffer* pByteBuffer, size_t* offset)
{
    mmByteOrder_ByteLittleDecodeU32(pByteBuffer, offset, &p->chunkLength);
    mmByteOrder_ByteLittleDecodeU32(pByteBuffer, offset, &p->chunkType);
}

void mmGLTFLayout_Init(struct mmGLTFLayout* p)
{
    mmGLTFHeader_Init(&p->header);
    mmGLTFChunk_Init(&p->metadata);
    mmGLTFChunk_Init(&p->binary);
}
void mmGLTFLayout_Destroy(struct mmGLTFLayout* p)
{
    mmGLTFChunk_Destroy(&p->binary);
    mmGLTFChunk_Destroy(&p->metadata);
    mmGLTFHeader_Destroy(&p->header);
}
void mmGLTFLayout_Reset(struct mmGLTFLayout* p)
{
    mmGLTFHeader_Reset(&p->header);
    mmGLTFChunk_Reset(&p->metadata);
    mmGLTFChunk_Reset(&p->binary);
}

static
int 
mmGLTFFileIO_IsFileExists(
    struct mmGLTFFileIO*                           pSuper,
    const char*                                    path_name)
{
    return MM_FALSE;
}

static
void
mmGLTFFileIO_AcquireFileByteBufferDefault(
    struct mmGLTFFileIO*                           pSuper,
    const char*                                    path_name,
    struct mmByteBuffer*                           byte_buffer)
{

}

static
void
mmGLTFFileIO_ReleaseFileByteBufferDefault(
    struct mmGLTFFileIO*                           pSuper,
    struct mmByteBuffer*                           byte_buffer)
{

}

void mmGLTFFileIO_Init(struct mmGLTFFileIO* p)
{
    p->IsFileExists = &mmGLTFFileIO_IsFileExists;
    p->AcquireFileByteBuffer = &mmGLTFFileIO_AcquireFileByteBufferDefault;
    p->ReleaseFileByteBuffer = &mmGLTFFileIO_ReleaseFileByteBufferDefault;
}
void mmGLTFFileIO_Destroy(struct mmGLTFFileIO* p)
{
    p->IsFileExists = &mmGLTFFileIO_IsFileExists;
    p->AcquireFileByteBuffer = &mmGLTFFileIO_AcquireFileByteBufferDefault;
    p->ReleaseFileByteBuffer = &mmGLTFFileIO_ReleaseFileByteBufferDefault;
}

static const size_t mmGlbHeaderSize = 12;
static const size_t mmGlbChunkHeaderSize = 8;
static const uint32_t mmGlbVersion = 2;
static const uint32_t mmGlbMagic = 0x46546C67;
static const uint32_t mmGlbMagicJsonChunk = 0x4E4F534A;
static const uint32_t mmGlbMagicBinChunk = 0x004E4942;

void mmGLTFModel_Init(struct mmGLTFModel* p)
{
    p->type = mmGLTFFileTypeInvalid;
    mmGLTFLayout_Init(&p->layout);
    mmGLTFRoot_Init(&p->root);
    mmParseJSON_Init(&p->parse);
    mmByteBuffer_Init(&p->bytes);
}
void mmGLTFModel_Destroy(struct mmGLTFModel* p)
{
    mmByteBuffer_Destroy(&p->bytes);
    mmParseJSON_Destroy(&p->parse);
    mmGLTFRoot_Destroy(&p->root);
    mmGLTFLayout_Destroy(&p->layout);
    p->type = mmGLTFFileTypeInvalid;
}
void mmGLTFModel_Reset(struct mmGLTFModel* p)
{
    p->type = mmGLTFFileTypeInvalid;
    mmGLTFLayout_Reset(&p->layout);
    mmGLTFRoot_Reset(&p->root);
    mmParseJSON_Reset(&p->parse);
    mmByteBuffer_Reset(&p->bytes);
}

static int mmGLTFModel_AnalysisMetadata(struct mmGLTFModel* p)
{
    int i = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmGLTFChunk* pMetadata = &p->layout.metadata;

    mmParseJSON_SetJsonBuffer(&p->parse, (const char*)pMetadata->chunkData, pMetadata->chunkLength);
    mmParseJSON_Analysis(&p->parse);
    if (p->parse.code > 0)
    {
        i = mmParseJSON_DecodeObject(&p->parse, 0, &p->root, &mmParseJSON_DecodeGLTFRoot);
        return (i > 0) ? mmGLTFResultSuccess : mmGLTFResultInvalidGltf;
    }
    else
    {
        mmLogger_LogI(gLogger, "AnalysisMetadata failure.");
        return mmGLTFResultInvalidJson;
    }
}
static int mmGLTFModel_AnalysisGLTF(struct mmGLTFModel* p)
{
    int code = mmGLTFResultUnknownFormat;
    code = mmGLTFModel_AnalysisMetadata(p);
    p->type = (mmGLTFResultSuccess == code) ? mmGLTFFileTypeGltf : mmGLTFFileTypeInvalid;
    return code;
}
static int mmGLTFModel_AnalysisGLB(struct mmGLTFModel* p)
{
    int code = mmGLTFResultUnknownFormat;

    do
    {
        size_t offset = 0;

        struct mmGLTFHeader* pHeader = &p->layout.header;
        struct mmGLTFChunk* pMetadata = &p->layout.metadata;
        struct mmGLTFChunk* pBinary = &p->layout.binary;

        struct mmByteBuffer* bytes = &p->bytes;

        mmUInt8_t* data = NULL;
        size_t size = 0;

        size_t hMetadataSize = 0;

        p->type = mmGLTFFileTypeInvalid;

        if (NULL == bytes)
        {
            code = mmGLTFResultUnknownFormat;
            break;
        }

        data = bytes->buffer + bytes->offset;
        size = bytes->length;

        // Version
        if (pHeader->version != mmGlbVersion)
        {
            code = (pHeader->version < mmGlbVersion) ? mmGLTFResultLegacyGltf : mmGLTFResultUnknownFormat;
            break;
        }

        // Total length
        if (pHeader->length > size)
        {
            code = mmGLTFResultDataTooShort;
            break;
        }

        offset = mmGlbHeaderSize;
        mmGLTFChunk_Decode(pMetadata, bytes, &offset);
        pMetadata->chunkData = (mmUInt8_t*)(data + offset);

        if (mmGlbHeaderSize + mmGlbChunkHeaderSize > size)
        {
            code = mmGLTFResultDataTooShort;
            break;
        }

        // JSON chunk: length
        if (mmGlbHeaderSize + mmGlbChunkHeaderSize + pMetadata->chunkLength > size)
        {
            code = mmGLTFResultDataTooShort;
            break;
        }

        // JSON chunk: magic
        if (pMetadata->chunkType != mmGlbMagicJsonChunk)
        {
            code = mmGLTFResultUnknownFormat;
            break;
        }

        hMetadataSize = mmGlbHeaderSize + mmGlbChunkHeaderSize + pMetadata->chunkLength + mmGlbChunkHeaderSize;
        if (hMetadataSize <= size)
        {
            // We can read another chunk
            //const uint8_t* bin_chunk = json_chunk + pMetadata->chunkLength;
            offset = mmGlbHeaderSize + mmGlbChunkHeaderSize + pMetadata->chunkLength;
            mmGLTFChunk_Decode(pBinary, bytes, &offset);
            pBinary->chunkData = (mmUInt8_t*)(data + offset);

            // Bin chunk: length
            if (hMetadataSize + pBinary->chunkLength > size)
            {
                code = mmGLTFResultDataTooShort;
                break;
            }

            // Bin chunk: magic
            if (pBinary->chunkType != mmGlbMagicBinChunk)
            {
                code = mmGLTFResultUnknownFormat;
                break;
            }
        }
        else
        {
            pBinary->chunkLength = 0;
            pBinary->chunkType = mmGlbMagicBinChunk;
            pBinary->chunkData = NULL;
        }

        code = mmGLTFModel_AnalysisMetadata(p);
        if (code != mmGLTFResultSuccess)
        {
            break;
        }

        p->type = mmGLTFFileTypeGlb;

        code = mmGLTFResultSuccess;
    } while (0);

    return code;
}

static
int
mmGLTFModel_PrepareUriData(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io,
    struct mmString*                               pPathName,
    struct mmRange*                                uri,
    struct mmGLTFUriData*                          pUriData)
{
    int err = mmGLTFResultUnknown;

    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmByteBuffer* bytes = &pUriData->bytes;
    struct mmGLTFLayout* layout = &p->layout;

    const char* jbuffer = p->parse.jbuffer;

    size_t hUriLength = uri->l;
    const char* pUriBuffer = jbuffer + uri->o;

    do
    {
        if (NULL == io)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " io is invalid.", __FUNCTION__, __LINE__);
            err = mmGLTFResultInvalidOptions;
            break;
        }

        if (NULL == jbuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " jbuffer is invalid.", __FUNCTION__, __LINE__);
            err = mmGLTFResultInvalidOptions;
            break;
        }

        if (0 == hUriLength)
        {
            // glb GLB-stored Buffer Layout.binary.
            struct mmGLTFChunk* binary = &layout->binary;
            bytes->buffer = (mmUInt8_t*)(binary->chunkData);
            bytes->offset = 0;
            bytes->length = (size_t)binary->chunkLength;
            pUriData->embedded = MM_FALSE;

            err = mmGLTFResultSuccess;
        }
        else
        {
            // external resource.
            //
            // url Embedded data.
            //     "data:application/octet-stream;base64,"
            //     "data:image/jpeg;base64,"
            //     "data:image/png;base64,"
            //
            // url Relative URI paths.
            //     data.bin
            //     image.png

            static const char* cDataSignature = "data:";
            const size_t cDataLenght = strlen(cDataSignature);

            pUriData->embedded = (0 == mmMemcmp(pUriBuffer, cDataSignature, cDataLenght));

            if (hUriLength >= cDataLenght && pUriData->embedded)
            {
                // url Embedded data.

                static const char* cBase64Signature = "base64";
                const size_t cBase64Lenght = strlen(cBase64Signature);

                static const size_t cSignatureLimit = 64;

                size_t max = 0;
                size_t n0, n1, n2;
                const char* pMineType = NULL;
                const char* pBase64Type = NULL;

                max = cSignatureLimit > hUriLength ? hUriLength : cSignatureLimit;
                n0 = mmCStringFindNextOf(pUriBuffer, ';', cDataLenght, max);
                n0 = (n0 == mmStringNpos || n0 == cSignatureLimit) ? 0 : n0;
                n1 = (n0 != 0 && n0 < (cSignatureLimit - 1)) ? (n0 + 1) : 0;

                pMineType = pUriBuffer + cDataLenght;

                n2 = mmCStringFindNextOf(pMineType, ',', n1, max);
                n2 = (n2 == mmStringNpos || n2 == cSignatureLimit) ? 0 : n2;

                pBase64Type = pMineType + n1;

                if (0 != mmMemcmp(pBase64Type, cBase64Signature, cBase64Lenght))
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " not support Embedded encodeing format.", __FUNCTION__, __LINE__);

                    err = mmGLTFResultNotSupportFormat;
                }
                else
                {
                    // base64 encodeing.
                    size_t hBase64Offset = cDataLenght + n1 + 1 + n2;
                    size_t hBase64Length = hUriLength - hBase64Offset;
                    const char* pBase64Buffer = (const char*)(pUriBuffer + hBase64Offset);

                    size_t hDecodeLength = mmBase64_DecodeLength(hBase64Length);
                    mmByteBuffer_Malloc(bytes, hDecodeLength);
                    bytes->length = (size_t)mmBase64_Decode((char*)bytes->buffer, pBase64Buffer, hBase64Length);

                    err = mmGLTFResultSuccess;
                }
            }
            else
            {
                // url Relative URI paths.

                size_t max = 0;
                size_t idx = 0;
                char* pUriDecode = (char*)mmMalloc(hUriLength + 1);
                mmMemcpy(pUriDecode, pUriBuffer, hUriLength);
                pUriDecode[hUriLength] = 0;
                mmGLTFDecodeUri(pUriDecode);

                // This schemes not support. For example:
                // http://image.png
                // file://image.png 

                max = 32 > hUriLength ? hUriLength : 32;
                idx = mmCStringFindNextStringOf(pUriDecode, "://", 0, max);
                if (mmStringNpos != idx)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " not support Relative URI paths.", __FUNCTION__, __LINE__);

                    err = mmGLTFResultNotSupportFeature;
                }
                else
                {
                    struct mmString hPathUrl;
                    const char* pPathUrlCStr = "";

                    mmString_Init(&hPathUrl);

                    mmString_Assign(&hPathUrl, pPathName);
                    mmDirectoryHaveSuffix(&hPathUrl, mmString_CStr(&hPathUrl));
                    mmString_Appends(&hPathUrl, pUriDecode);

                    pPathUrlCStr = mmString_CStr(&hPathUrl);

                    if (!(*(io->IsFileExists))(io, pPathUrlCStr))
                    {
                        mmLogger_LogI(gLogger, "%s %d"
                            " IsFileExists failure. path: %s", __FUNCTION__, __LINE__, pPathUrlCStr);
                        err = mmGLTFResultFileNotFound;
                    }
                    else
                    {
                        (*(io->AcquireFileByteBuffer))(io, pPathUrlCStr, bytes);
                        err = mmGLTFResultSuccess;
                    }

                    mmString_Destroy(&hPathUrl);

                }

                mmFree(pUriDecode);
            }
        }
    } while (0);

    return err;
}

static
void
mmGLTFModel_DiscardUriData(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io,
    struct mmRange*                                uri,
    struct mmGLTFUriData*                          pUriData)
{
    struct mmByteBuffer* bytes = &pUriData->bytes;
    size_t hUriLength = uri->l;

    if (NULL == io)
    {
        mmByteBuffer_Reset(bytes);
    }
    else
    {
        if (0 == hUriLength)
        {
            // glb GLB-stored Buffer Layout.binary.
            mmByteBuffer_Reset(bytes);
        }
        else
        {
            // external resource.
            if (pUriData->embedded)
            {
                // url Embedded data.
                mmByteBuffer_Free(bytes);
            }
            else
            {
                // url Relative URI paths.
                (*(io->ReleaseFileByteBuffer))(io, bytes);
            }
        }
    }
}

static
int
mmGLTFModel_PrepareBufferUriDatas(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io,
    struct mmString*                               pPathName)
{
    int err = mmGLTFResultUnknown;

    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmGLTFRoot* pGLTFRoot = &p->root;
    struct mmGLTFBuffer* pGLTFBuffer = NULL;
    struct mmVectorValue* buffers = &pGLTFRoot->buffers;
    size_t i = 0;

    if (0 < buffers->size)
    {
        for (i = 0; i < buffers->size; ++i)
        {
            pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, i);

            err = mmGLTFModel_PrepareUriData(p, io, pPathName, &pGLTFBuffer->uri, &pGLTFBuffer->data);
            if (mmGLTFResultSuccess != err)
            {
                mmLogger_LogE(gLogger, "%s %d mmGLTFModel_PrepareUriData failure.",
                    __FUNCTION__, __LINE__);
                break;
            }
        }
    }
    else
    {
        err = mmGLTFResultSuccess;
    }

    return err;
}

static
void
mmGLTFModel_DiscardBufferUriDatas(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io)
{
    struct mmGLTFRoot* pGLTFRoot = &p->root;
    struct mmGLTFBuffer* pGLTFBuffer = NULL;
    struct mmVectorValue* buffers = &pGLTFRoot->buffers;
    size_t i = 0;

    for (i = 0; i < buffers->size; ++i)
    {
        pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, i);

        mmGLTFModel_DiscardUriData(p, io, &pGLTFBuffer->uri, &pGLTFBuffer->data);
    }
}

static
int
mmGLTFModel_PrepareImageUriDatas(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io,
    struct mmString*                               pPathName)
{
    int err = mmGLTFResultUnknown;

    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmGLTFRoot* pGLTFRoot = &p->root;
    struct mmGLTFImage* pGLTFImage = NULL;
    struct mmVectorValue* images = &pGLTFRoot->images;
    size_t i = 0;

    if (0 < images->size)
    {
        for (i = 0; i < images->size; ++i)
        {
            pGLTFImage = (struct mmGLTFImage*)mmVectorValue_At(images, i);

            err = mmGLTFModel_PrepareUriData(p, io, pPathName, &pGLTFImage->uri, &pGLTFImage->data);
            if (mmGLTFResultSuccess != err)
            {
                mmLogger_LogE(gLogger, "%s %d mmVKModel_PrepareUriData failure.",
                    __FUNCTION__, __LINE__);
                break;
            }
        }
    }
    else
    {
        err = mmGLTFResultSuccess;
    }

    return err;
}

static
void
mmGLTFModel_DiscardImageUriDatas(
    struct mmGLTFModel*                            p,
    struct mmGLTFFileIO*                           io)
{
    struct mmGLTFRoot* pGLTFRoot = &p->root;
    struct mmGLTFImage* pGLTFImage = NULL;
    struct mmVectorValue* images = &pGLTFRoot->images;
    size_t i = 0;

    for (i = 0; i < images->size; ++i)
    {
        pGLTFImage = (struct mmGLTFImage*)mmVectorValue_At(images, i);

        mmGLTFModel_DiscardUriData(p, io, &pGLTFImage->uri, &pGLTFImage->data);
    }
}

static
int 
mmGLTFModel_Analysis(
    struct mmGLTFModel*                            p)
{
    int code = mmGLTFResultUnknown;

    do
    {
        enum mmGLTFFile_t type = mmGLTFFileTypeInvalid;

        size_t offset = 0;
        struct mmGLTFHeader* pHeader = &p->layout.header;
        struct mmGLTFChunk* pMetadata = &p->layout.metadata;

        struct mmByteBuffer* bytes = &p->bytes;

        mmUInt8_t* data = NULL;
        size_t size = 0;

        if (NULL == bytes)
        {
            code = mmGLTFResultUnknownFormat;
            break;
        }

        data = bytes->buffer + bytes->offset;
        size = bytes->length;

        if (size < mmGlbHeaderSize)
        {
            code = mmGLTFResultDataTooShort;
            break;
        }

        offset = 0;
        mmGLTFHeader_Decode(pHeader, bytes, &offset);
        if (pHeader->magic != mmGlbMagic)
        {
            type = mmGLTFFileTypeGltf;
        }
        else
        {
            type = mmGLTFFileTypeGlb;
        }

        if (type == mmGLTFFileTypeGltf)
        {
            pMetadata->chunkLength = (mmUInt32_t)size;
            pMetadata->chunkType = mmGlbMagicJsonChunk;
            pMetadata->chunkData = data;

            code = mmGLTFModel_AnalysisGLTF(p);
        }
        else
        {
            code = mmGLTFModel_AnalysisGLB(p);
        }
    } while (0);

    return code;
}

int mmGLTFModel_PrepareUriDatas(struct mmGLTFModel* p, struct mmGLTFFileIO* io, const char* path)
{
    int err = mmGLTFResultUnknown;

    struct mmString hQualifiedName;
    struct mmString hBaseName;
    struct mmString hPathName;

    mmString_Init(&hQualifiedName);
    mmString_Init(&hBaseName);
    mmString_Init(&hPathName);

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_Assigns(&hQualifiedName, path);
        mmPathSplitFileName(&hQualifiedName, &hBaseName, &hPathName);

        if (!(*(io->IsFileExists))(io, path))
        {
            mmLogger_LogI(gLogger, "%s %d"
                " IsFileExists failure. path: %s", __FUNCTION__, __LINE__, path);
            err = mmGLTFResultFileNotFound;
            break;
        }

        (*(io->AcquireFileByteBuffer))(io, path, &p->bytes);
        if (NULL == p->bytes.buffer || 0 == p->bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d bytes is invalid.",
                __FUNCTION__, __LINE__);
            err = mmGLTFResultIOError;
            break;
        }

        err = mmGLTFModel_Analysis(p);
        if (mmGLTFResultSuccess != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmGLTFModel_Analysis failure.",
                __FUNCTION__, __LINE__);
            break;
        }

#if defined(_DEBUG) && defined(MM_GLTF_VALIDATE)
        err = mmGLTFModel_Validate(p);
        if (mmGLTFResultSuccess != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmGLTFModel_Validate failure.",
                __FUNCTION__, __LINE__);
            break;
        }
#endif//_DEBUG

        err = mmGLTFModel_PrepareBufferUriDatas(p, io, &hPathName);
        if (mmGLTFResultSuccess != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmGLTFModel_PrepareBufferUriDatas failure.",
                __FUNCTION__, __LINE__);
            break;
        }
        err = mmGLTFModel_PrepareImageUriDatas(p, io, &hPathName);
        if (mmGLTFResultSuccess != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmGLTFModel_PrepareImageUriDatas failure.",
                __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    mmString_Destroy(&hPathName);
    mmString_Destroy(&hBaseName);
    mmString_Destroy(&hQualifiedName);

    return err;
}
void mmGLTFModel_DiscardUriDatas(struct mmGLTFModel* p, struct mmGLTFFileIO* io)
{
    mmGLTFModel_DiscardImageUriDatas(p, io);
    mmGLTFModel_DiscardBufferUriDatas(p, io);
    (*(io->ReleaseFileByteBuffer))(io, &p->bytes);
}

static int mmGLTFModel_ValidateBufferViews(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmGLTFBuffer* buffer = NULL;
    const struct mmGLTFBufferView* bufferView = NULL;

    const struct mmVectorValue* buffers = &root->buffers;
    const struct mmVectorValue* bufferViews = &root->bufferViews;

    for (i = 0; i < bufferViews->size; ++i)
    {
        size_t req_size;

        bufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, i);
        if (bufferView->buffer >= buffers->size)
        {
            return mmGLTFResultInvalidGltf;
        }

        buffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, bufferView->buffer);

        req_size = bufferView->byteOffset + bufferView->byteLength;
        if (NULL != buffer && buffer->byteLength < req_size)
        {
            return mmGLTFResultDataTooShort;
        }

        if (bufferView->has_meshoptCompression)
        {
            const struct mmGLTFMeshoptCompression* mc = &bufferView->ext_meshoptCompression;
            if (mc->buffer >= buffers->size)
            {
                return mmGLTFResultInvalidGltf;
            }
            buffer = (const struct mmGLTFBuffer*)mmVectorValue_At(buffers, mc->buffer);
            if (buffer == NULL || buffer->byteLength < mc->byteOffset + mc->byteLength)
            {
                return mmGLTFResultDataTooShort;
            }

            if (bufferView->byteStride && mc->byteStride != bufferView->byteStride)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (bufferView->byteLength != mc->byteStride * mc->count)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (mc->mode == mmGLTFMeshoptCompressionModeInvalid)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (mc->mode == mmGLTFMeshoptCompressionModeAttributes &&
                !(mc->byteStride % 4 == 0 && mc->byteStride <= 256))
            {
                return mmGLTFResultInvalidGltf;
            }

            if (mc->mode == mmGLTFMeshoptCompressionModeTriangles &&
                mc->count % 3 != 0)
            {
                return mmGLTFResultInvalidGltf;
            }

            if ((mc->mode == mmGLTFMeshoptCompressionModeTriangles ||
                mc->mode == mmGLTFMeshoptCompressionModeIndices) &&
                mc->byteStride != 2 && mc->byteStride != 4)
            {
                return mmGLTFResultInvalidGltf;
            }

            if ((mc->mode == mmGLTFMeshoptCompressionModeTriangles ||
                mc->mode == mmGLTFMeshoptCompressionModeIndices) &&
                mc->filter != mmGLTFMeshoptCompressionFilterNone)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (mc->filter == mmGLTFMeshoptCompressionFilterOctahedral &&
                mc->byteStride != 4 && mc->byteStride != 8)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (mc->filter == mmGLTFMeshoptCompressionFilterQuaternion &&
                mc->byteStride != 8)
            {
                return mmGLTFResultInvalidGltf;
            }
        }
    }

    return mmGLTFResultSuccess;
}

static int mmGLTFModel_ValidateAccessors(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmVectorValue* buffers = &root->buffers;
    const struct mmVectorValue* bufferViews = &root->bufferViews;
    const struct mmVectorValue* accessors = &root->accessors;

    const struct mmGLTFAccessor* accessor = NULL;

    for (i = 0; i < accessors->size; ++i)
    {
        size_t element_size;

        accessor = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, i);

        element_size = mmGLTFCalculationSize(accessor->type, accessor->componentType);

        if (-1 != accessor->bufferView)
        {
            const struct mmGLTFBufferView* bufferView = NULL;
            size_t req_size;

            if (accessor->bufferView >= bufferViews->size)
            {
                return mmGLTFResultInvalidGltf;
            }
            bufferView = (const struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);
            req_size = accessor->byteOffset + bufferView->byteStride * (accessor->count - 1) + element_size;

            if (bufferView->byteLength < req_size)
            {
                return mmGLTFResultDataTooShort;
            }
        }

        if (accessor->isSparse)
        {
            const struct mmByteBuffer* bytes = NULL;
            const struct mmGLTFBuffer* buffer = NULL;
            const struct mmGLTFBufferView* bufferViewI = NULL;
            const struct mmGLTFBufferView* bufferViewV = NULL;
            const struct mmGLTFAccessorSparse* sparse = &accessor->sparse;

            size_t indices_component_size = mmGLTFCalculationSize(mmGLTFTypeScalar, sparse->indices.componentType);
            size_t indices_req_size = sparse->indices.byteOffset + indices_component_size * sparse->count;
            size_t values_req_size = sparse->values.byteOffset + element_size * sparse->count;

            if (sparse->indices.bufferView >= bufferViews->size ||
                sparse->values.bufferView >= bufferViews->size)
            {
                return mmGLTFResultInvalidGltf;
            }

            bufferViewI = (const struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, sparse->indices.bufferView);
            bufferViewV = (const struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, sparse->values.bufferView);

            if (bufferViewI->byteLength < indices_req_size ||
                bufferViewV->byteLength < values_req_size)
            {
                return mmGLTFResultDataTooShort;
            }

            if (sparse->indices.componentType != mmGLTFComponentTypeUnsignedByte &&
                sparse->indices.componentType != mmGLTFComponentTypeUnsignedShort &&
                sparse->indices.componentType != mmGLTFComponentTypeUnsignedInt)
            {
                return mmGLTFResultInvalidGltf;
            }

            if (bufferViewI->buffer >= buffers->size)
            {
                return mmGLTFResultInvalidGltf;
            }

            buffer = (const struct mmGLTFBuffer*)mmVectorValue_At(buffers, bufferViewI->buffer);
            bytes = &buffer->data.bytes;
            if (NULL != bytes->buffer && 0 != bytes->length)
            {
                size_t index_bound = mmGLTFCalculationIndexBound(
                    bufferViewI,
                    buffer,
                    sparse->indices.byteOffset,
                    sparse->indices.componentType,
                    sparse->count);

                if (index_bound >= accessor->count)
                {
                    return mmGLTFResultDataTooShort;
                }
            }
        }
    }
    return mmGLTFResultSuccess;
}

static int mmGLTFModel_ValidateMeshes(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmVectorValue* buffers = &root->buffers;
    const struct mmVectorValue* bufferViews = &root->bufferViews;
    const struct mmVectorValue* accessors = &root->accessors;
    const struct mmVectorValue* meshes = &root->meshes;

    const struct mmGLTFMesh* mesh = NULL;

    for (i = 0; i < meshes->size; ++i)
    {
        const struct mmGLTFPrimitive* primitive = NULL;
        const struct mmVectorValue* weights = NULL;
        const struct mmVectorValue* primitives = NULL;

        mesh = (const struct mmGLTFMesh*)mmVectorValue_At(meshes, i);

        weights = &mesh->weights;
        primitives = &mesh->primitives;

        if (0 < weights->size)
        {
            if (0 < primitives->size)
            {
                const struct mmGLTFPrimitive* primitive0 = NULL;
                primitive0 = (const struct mmGLTFPrimitive*)mmVectorValue_At(primitives, 0);
                if (primitive0->targets.size != mesh->weights.size)
                {
                    return mmGLTFResultInvalidGltf;
                }
            }
            else
            {
                return mmGLTFResultInvalidGltf;
            }
        }

        if (mesh->has_extras)
        {
            const struct mmGLTFMeshExtras* ext_extras = &mesh->ext_extras;
            if (0 < primitives->size)
            {
                const struct mmGLTFPrimitive* primitive0 = NULL;
                primitive0 = (const struct mmGLTFPrimitive*)mmVectorValue_At(primitives, 0);

                if (primitive0->targets.size != ext_extras->targetNames.size)
                {
                    return mmGLTFResultInvalidGltf;
                }
            }
            else
            {
                return mmGLTFResultInvalidGltf;
            }
        }

        if (0 < primitives->size)
        {
            size_t j;

            const struct mmGLTFPrimitive* primitive0 = NULL;

            primitive0 = (struct mmGLTFPrimitive*)mmVectorValue_At(primitives, 0);

            for (j = 0; j < primitives->size; ++j)
            {
                size_t k;
                const struct mmGLTFAccessor* first = NULL;
                const struct mmGLTFAttribute* attribute0 = NULL;

                const struct mmGLTFBufferView* bufferView = NULL;
                const struct mmGLTFBuffer* buffer = NULL;
                const struct mmByteBuffer* bytes = NULL;

                const struct mmGLTFAccessor* indices = NULL;

                primitive = (const struct mmGLTFPrimitive*)mmVectorValue_At(primitives, j);
                if (primitive->targets.size != primitive0->targets.size)
                {
                    return mmGLTFResultInvalidGltf;
                }

                if (0 >= primitive->attributes.size)
                {
                    continue;
                }

                attribute0 = (const struct mmGLTFAttribute*)mmVectorValue_At(&primitive->attributes, 0);
                if (attribute0->accessor < 0)
                {
                    return mmGLTFResultInvalidGltf;
                }
                first = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, attribute0->accessor);

                for (k = 0; k < primitive->attributes.size; ++k)
                {
                    const struct mmGLTFAttribute* attributek = NULL;
                    const struct mmGLTFAccessor* accessork = NULL;

                    attributek = (const struct mmGLTFAttribute*)mmVectorValue_At(&primitive->attributes, k);
                    if (attributek->accessor < 0)
                    {
                        return mmGLTFResultInvalidGltf;
                    }
                    accessork = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, attributek->accessor);
                    if (accessork->count != first->count)
                    {
                        return mmGLTFResultInvalidGltf;
                    }
                }

                for (k = 0; k < primitive->targets.size; ++k)
                {
                    const struct mmVectorValue* attributes = NULL;
                    size_t m;
                    attributes = (const struct mmVectorValue*)mmVectorValue_At(&primitive->targets, k);
                    for (m = 0; m < attributes->size; ++m)
                    {
                        const struct mmGLTFAttribute* attribute = NULL;
                        const struct mmGLTFAccessor* accessor = NULL;
                        attribute = (const struct mmGLTFAttribute*)mmVectorValue_At(attributes, m);
                        if (attribute->accessor < 0)
                        {
                            return mmGLTFResultInvalidGltf;
                        }
                        accessor = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, attribute->accessor);
                        if (accessor->count != first->count)
                        {
                            return mmGLTFResultInvalidGltf;
                        }
                    }
                }

                if (0 > primitive->indices)
                {
                    continue;
                }

                indices = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, primitive->indices);

                if (indices->componentType != mmGLTFComponentTypeUnsignedByte &&
                    indices->componentType != mmGLTFComponentTypeUnsignedShort &&
                    indices->componentType != mmGLTFComponentTypeUnsignedInt)
                {
                    return mmGLTFResultInvalidGltf;
                }

                if (0 > indices->bufferView)
                {
                    continue;
                }

                bufferView = (const struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, indices->bufferView);
                if (0 > bufferView->buffer)
                {
                    continue;
                }

                buffer = (const struct mmGLTFBuffer*)mmVectorValue_At(buffers, bufferView->buffer);
                bytes = &buffer->data.bytes;

                if (NULL != bytes->buffer && 0 != bytes->length)
                {
                    size_t index_bound = mmGLTFCalculationIndexBound(
                        bufferView,
                        buffer,
                        indices->byteOffset,
                        indices->componentType,
                        indices->count);
                    if (index_bound >= first->count)
                    {
                        return mmGLTFResultInvalidGltf;
                    }
                }
            }
        }
    }

    return mmGLTFResultSuccess;
}

static int mmGLTFModel_ValidateNodes(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmVectorValue* nodes = &root->nodes;
    const struct mmVectorValue* meshes = &root->meshes;

    const struct mmGLTFNode* node = NULL;
    for (i = 0; i < nodes->size; ++i)
    {
        node = (const struct mmGLTFNode*)mmVectorValue_At(nodes, i);
        if (0 < node->weights.size && 0 < node->mesh)
        {
            const struct mmGLTFMesh* mesh = (const struct mmGLTFMesh*)mmVectorValue_At(meshes, node->mesh);
            if (0 < mesh->primitives.size)
            {
                const struct mmGLTFPrimitive* primitive0 = NULL;
                primitive0 = (const struct mmGLTFPrimitive*)mmVectorValue_At(&mesh->primitives, 0);
                if (primitive0->targets.size != node->weights.size)
                {
                    return mmGLTFResultInvalidGltf;
                }
            }
        }
    }

    for (i = 0; i < nodes->size; ++i)
    {
        const struct mmGLTFNode* p1;
        const struct mmGLTFNode* p2;

        node = (const struct mmGLTFNode*)mmVectorValue_At(nodes, i);

        p1 = node->parent;
        p2 = p1 ? p1->parent : NULL;

        while (p1 && p2)
        {
            if (p1 == p2)
            {
                return mmGLTFResultInvalidGltf;
            }

            p1 = p1->parent;
            p2 = p2->parent ? p2->parent->parent : NULL;
        }
    }

    return mmGLTFResultSuccess;
}

static int mmGLTFModel_ValidateScenes(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmVectorValue* nodes = &root->nodes;
    const struct mmVectorValue* scenes = &root->scenes;

    const struct mmGLTFScene* scene = NULL;

    for (i = 0; i < scenes->size; ++i)
    {
        size_t j;
        const int* v;
        int n;
        scene = (const struct mmGLTFScene*)mmVectorValue_At(scenes, i);
        for (j = 0; j < scene->nodes.size; ++j)
        {
            const struct mmGLTFNode* node = NULL;
            v = (const int*)mmVectorValue_At(&scene->nodes, j);
            n = (*v);
            node = (const struct mmGLTFNode*)mmVectorValue_At(nodes, n);
            if (NULL != node->parent)
            {
                return mmGLTFResultInvalidGltf;
            }
        }
    }

    return mmGLTFResultSuccess;
}

static int mmGLTFModel_ValidateAnimations(const struct mmGLTFModel* p)
{
    size_t i = 0;

    const struct mmGLTFRoot* root = &p->root;

    const struct mmVectorValue* accessors = &root->accessors;
    const struct mmVectorValue* meshes = &root->meshes;
    const struct mmVectorValue* nodes = &root->nodes;
    const struct mmVectorValue* animations = &root->animations;

    const struct mmGLTFAnimation* animation = NULL;

    for (i = 0; i < animations->size; ++i)
    {
        size_t j;
        animation = (const struct mmGLTFAnimation*)mmVectorValue_At(animations, i);
        for (j = 0; j < animation->channels.size; ++j)
        {
            size_t values = 0;
            size_t components = 1;
            const struct mmGLTFAnimationSampler* sampler = NULL;
            const struct mmGLTFAnimationChannel* channel = NULL;
            channel = (const struct mmGLTFAnimationChannel*)mmVectorValue_At(&animation->channels, j);
            if (channel->target.node < 0)
            {
                continue;
            }

            if (channel->target.path == mmGLTFAnimationPathTypeWeights)
            {
                const struct mmGLTFMesh* mesh = NULL;
                const struct mmGLTFPrimitive* primitive0 = NULL;
                const struct mmGLTFNode* node = (const struct mmGLTFNode*)mmVectorValue_At(nodes, channel->target.node);
                if (-1 == node->mesh)
                {
                    return mmGLTFResultInvalidGltf;
                }
                mesh = (const struct mmGLTFMesh*)mmVectorValue_At(meshes, node->mesh);
                if (0 == mesh->primitives.size)
                {
                    return mmGLTFResultInvalidGltf;
                }

                primitive0 = (const struct mmGLTFPrimitive*)mmVectorValue_At(&mesh->primitives, 0);
                components = primitive0->targets.size;
            }

            if (channel->sampler >= animation->samplers.size)
            {
                return mmGLTFResultInvalidGltf;
            }

            sampler = (const struct mmGLTFAnimationSampler*)mmVectorValue_At(&animation->samplers, channel->sampler);

            values = sampler->interpolation == mmGLTFInterpolationTypeCubicSpline ? 3 : 1;
            if ((sampler->input >= 0 && sampler->input < accessors->size) &&
                (sampler->output >= 0 && sampler->output < accessors->size))
            {
                const struct mmGLTFAccessor* accessorI = NULL;
                const struct mmGLTFAccessor* accessorO = NULL;
                accessorI = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, sampler->input);
                accessorO = (const struct mmGLTFAccessor*)mmVectorValue_At(accessors, sampler->output);
                if (accessorI->count * components * values != accessorO->count)
                {
                    return mmGLTFResultDataTooShort;
                }
            }
        }
    }

    return mmGLTFResultSuccess;
}

int mmGLTFModel_Validate(const struct mmGLTFModel* p)
{
    int result = mmGLTFResultUnknown;

    result = mmGLTFModel_ValidateBufferViews(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    result = mmGLTFModel_ValidateAccessors(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    result = mmGLTFModel_ValidateMeshes(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    result = mmGLTFModel_ValidateNodes(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    result = mmGLTFModel_ValidateScenes(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    result = mmGLTFModel_ValidateAnimations(p);
    if (mmGLTFResultSuccess != result)
    {
        return result;
    }

    return mmGLTFResultSuccess;
}

int mmGLTFUnHex(char ch)
{
    return
        (unsigned)(ch - '0') < 10 ? (ch - '0') :
        (unsigned)(ch - 'A') <  6 ? (ch - 'A') + 10 :
        (unsigned)(ch - 'a') <  6 ? (ch - 'a') + 10 :
        -1;
}

void mmGLTFDecodeUri(char* uri)
{
    char* write = uri;
    char* i = uri;
    int ch1 = 0;
    int ch2 = 0;

    while (*i)
    {
        if (*i == '%')
        {
            ch1 = mmGLTFUnHex(i[1]);
            if (ch1 >= 0)
            {
                ch2 = mmGLTFUnHex(i[2]);
                if (ch2 >= 0)
                {
                    *write++ = (char)(ch1 * 16 + ch2);
                    i += 3;
                    continue;
                }
            }
        }
        *write++ = *i++;
    }
    *write = 0;
}

void mmGLTFNodeTransformLocal(const struct mmGLTFNode* node, float matrix[4][4])
{
    if (16 == node->sizeMatrix)
    {
        mmMat4x4Assign(matrix, node->matrix);
    }
    else
    {
        mmMat4x4MakeTransform(matrix, node->translation, node->rotation, node->scale);
    }
}

void mmGLTFNodeTransformWorld(const struct mmGLTFNode* node, float matrix[4][4])
{
    const struct mmGLTFNode* parent = node->parent;

    mmGLTFNodeTransformLocal(node, matrix);

    while (parent)
    {
        float pm[4][4];
        mmGLTFNodeTransformLocal(parent, pm);
        mmMat4x4Mul(matrix, pm, matrix);

        parent = parent->parent;
    }
}

size_t
mmGLTFCalculationSize(
    enum mmGLTFAccessor_t                          type,
    enum mmGLTFComponent_t                         componentType)
{
    int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);
    if (type == mmGLTFTypeMat2 && componentSize == 1)
    {
        return 8 * componentSize;
    }
    else if (type == mmGLTFTypeMat3 && (componentSize == 1 || componentSize == 2))
    {
        return 12 * componentSize;
    }
    return componentSize * mmGLTFGetNumComponentsInType(type);
}

size_t
mmGLTFCalculationIndexBound(
    const struct mmGLTFBufferView*                 bufferView,
    const struct mmGLTFBuffer*                     buffer,
    size_t                                         offset,
    enum mmGLTFComponent_t                         componentType,
    size_t                                         count)
{
    size_t bound = 0;
    size_t i;
    size_t v;

    const struct mmByteBuffer* bytes = &buffer->data.bytes;

    const char* data = (const char*)bytes->buffer + offset + bufferView->byteOffset;

    switch (componentType)
    {
    case mmGLTFComponentTypeUnsignedByte:
        for (i = 0; i < count; ++i)
        {
            v = ((unsigned char*)data)[i];
            bound = bound > v ? bound : v;
        }
        break;

    case mmGLTFComponentTypeUnsignedShort:
        for (i = 0; i < count; ++i)
        {
            v = ((unsigned short*)data)[i];
            bound = bound > v ? bound : v;
        }
        break;

    case mmGLTFComponentTypeUnsignedInt:
        for (i = 0; i < count; ++i)
        {
            v = ((unsigned int*)data)[i];
            bound = bound > v ? bound : v;
        }
        break;

    default:
        break;
    }

    return bound;
}

size_t 
mmGLTFComponentReadIndex(
    const void*                                    in, 
    enum mmGLTFComponent_t                         componentType)
{
    switch (componentType)
    {
    case mmGLTFComponentTypeShort:
        return *((const int16_t*)in);
    case mmGLTFComponentTypeUnsignedShort:
        return *((const uint16_t*)in);
    case mmGLTFComponentTypeUnsignedInt:
        return *((const uint32_t*)in);
    case mmGLTFComponentTypeFloat:
        return (size_t)*((const float*)in);
    case mmGLTFComponentTypeByte:
        return *((const int8_t*)in);
    case mmGLTFComponentTypeUnsignedByte:
        return *((const uint8_t*)in);
    default:
        return 0;
    }
}

float 
mmGLTFComponentReadFloat(
    const void*                                    in, 
    enum mmGLTFComponent_t                         componentType, 
    mmBool_t                                       normalized)
{
    if (componentType == mmGLTFComponentTypeFloat)
    {
        return *((const float*)in);
    }

    if (normalized)
    {
        switch (componentType)
        {
            // note: glTF spec doesn't currently define normalized conversions for 32-bit integers
        case mmGLTFComponentTypeShort:
            return *((const int16_t*)in) / (float)32767;
        case mmGLTFComponentTypeUnsignedShort:
            return *((const uint16_t*)in) / (float)65535;
        case mmGLTFComponentTypeByte:
            return *((const int8_t*)in) / (float)127;
        case mmGLTFComponentTypeUnsignedByte:
            return *((const uint8_t*)in) / (float)255;
        default:
            return 0;
        }
    }

    return (float)mmGLTFComponentReadIndex(in, componentType);
}

int 
mmGLTFElementReadFloat(
    const uint8_t*                                 element,
    enum mmGLTFAccessor_t                          type, 
    enum mmGLTFComponent_t                         componentType, 
    mmBool_t                                       normalized, 
    float*                                         out, 
    size_t                                         element_size)
{
    size_t i = 0;

    size_t num_components = mmGLTFGetNumComponentsInType(type);
    size_t component_size = mmGLTFGetComponentSizeInBytes(componentType);

    if (element_size < num_components) 
    {
        return MM_FALSE;
    }

    // There are three special cases for component extraction, see #data-alignment in the 2.0 spec.
    if (type == mmGLTFTypeMat2 && component_size == 1)
    {
        out[0] = mmGLTFComponentReadFloat(element     , componentType, normalized);
        out[1] = mmGLTFComponentReadFloat(element +  1, componentType, normalized);
        out[2] = mmGLTFComponentReadFloat(element +  4, componentType, normalized);
        out[3] = mmGLTFComponentReadFloat(element +  5, componentType, normalized);
        return MM_TRUE;
    }

    if (type == mmGLTFTypeMat3 && component_size == 1)
    {
        out[0] = mmGLTFComponentReadFloat(element     , componentType, normalized);
        out[1] = mmGLTFComponentReadFloat(element +  1, componentType, normalized);
        out[2] = mmGLTFComponentReadFloat(element +  2, componentType, normalized);
        out[3] = mmGLTFComponentReadFloat(element +  4, componentType, normalized);
        out[4] = mmGLTFComponentReadFloat(element +  5, componentType, normalized);
        out[5] = mmGLTFComponentReadFloat(element +  6, componentType, normalized);
        out[6] = mmGLTFComponentReadFloat(element +  8, componentType, normalized);
        out[7] = mmGLTFComponentReadFloat(element +  9, componentType, normalized);
        out[8] = mmGLTFComponentReadFloat(element + 10, componentType, normalized);
        return MM_TRUE;
    }

    if (type == mmGLTFTypeMat3 && component_size == 2)
    {
        out[0] = mmGLTFComponentReadFloat(element     , componentType, normalized);
        out[1] = mmGLTFComponentReadFloat(element +  2, componentType, normalized);
        out[2] = mmGLTFComponentReadFloat(element +  4, componentType, normalized);
        out[3] = mmGLTFComponentReadFloat(element +  8, componentType, normalized);
        out[4] = mmGLTFComponentReadFloat(element + 10, componentType, normalized);
        out[5] = mmGLTFComponentReadFloat(element + 12, componentType, normalized);
        out[6] = mmGLTFComponentReadFloat(element + 16, componentType, normalized);
        out[7] = mmGLTFComponentReadFloat(element + 18, componentType, normalized);
        out[8] = mmGLTFComponentReadFloat(element + 20, componentType, normalized);
        return MM_TRUE;
    }

    for (i = 0; i < num_components; ++i)
    {
        out[i] = mmGLTFComponentReadFloat(element + component_size * i, componentType, normalized);
    }
    return MM_TRUE;
}

void
mmGLTFElementReadVectorFloat(
    const uint8_t*                                 element,
    int32_t                                        numComponents,
    int32_t                                        componentSize,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized,
    float*                                         out)
{
    int32_t i = 0;
    for (i = 0; i < numComponents; ++i)
    {
        out[i] = mmGLTFComponentReadFloat(element + componentSize * i, componentType, normalized);
    }
}

int
mmGLTFElementReadFloatByIndex(
    const uint8_t*                                 in,
    enum mmGLTFAccessor_t                          type,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized,
    size_t                                         index,
    float*                                         out)
{
    int32_t numComponents = mmGLTFGetNumComponentsInType(type);
    int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);
    int32_t elementSize = numComponents * componentSize;
    return mmGLTFElementReadFloat(
        in + elementSize * index,
        type,
        componentType,
        normalized,
        out, 
        numComponents);
}

void
mmGLTFIndex8ToIndex16(
    uint8_t*                                      i,
    uint16_t*                                     o,
    size_t                                        count)
{
    size_t x = 0;
    for (x = 0; x < count; x++)
    {
        o[x] = i[x];
    }
}

struct mmGLTFBuffer*
mmGLTFRoot_BufferViewBytes(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFBufferView*                 bufferView,
    struct mmByteBuffer*                           bytes)
{
    struct mmGLTFBuffer* pGLTFBuffer = NULL;

    do
    {
        const struct mmVectorValue* buffers = &root->buffers;

        struct mmGLTFUriData* pGLTFUriData = NULL;

        mmByteBuffer_Reset(bytes);

        if (bufferView->buffer < 0)
        {
            break;
        }

        pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, bufferView->buffer);

        if (NULL == pGLTFBuffer)
        {
            break;
        }

        pGLTFUriData = &pGLTFBuffer->data;

        bytes->buffer = pGLTFUriData->bytes.buffer;
        bytes->offset = bufferView->byteOffset;
        bytes->length = bufferView->byteLength;
    } while (0);

    return pGLTFBuffer;
}

struct mmGLTFBufferView*
mmGLTFRoot_AccessorBytes(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    struct mmByteBuffer*                           bytes)
{
    struct mmGLTFBufferView* pGLTFBufferView = NULL;

    do
    {
        const struct mmVectorValue* bufferViews = &root->bufferViews;

        mmByteBuffer_Reset(bytes);

        if (accessor->bufferView < 0)
        {
            break;
        }

        pGLTFBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);

        if (NULL == pGLTFBufferView)
        {
            break;
        }

        mmGLTFRoot_BufferViewBytes(root, pGLTFBufferView, bytes);
    } while (0);

    return pGLTFBufferView;
}

struct mmGLTFAccessor*
mmGLTFRoot_AccessorIndexBytes(
    const struct mmGLTFRoot*                       root,
    int                                            index,
    struct mmByteBuffer*                           bytes)
{
    struct mmGLTFAccessor* pGLTFAccessor = NULL;
    struct mmGLTFBufferView* pGLTFBufferView = NULL;


    do
    {
        const struct mmVectorValue* accessors = &root->accessors;

        mmByteBuffer_Reset(bytes);

        if (index < 0)
        {
            break;
        }

        pGLTFAccessor = (struct mmGLTFAccessor*)mmVectorValue_At(accessors, index);

        if (NULL == pGLTFAccessor)
        {
            break;
        }

        pGLTFBufferView = mmGLTFRoot_AccessorBytes(root, pGLTFAccessor, bytes);

        if (NULL == pGLTFBufferView)
        {
            break;
        }

        bytes->offset += pGLTFAccessor->byteOffset;
    } while (0);

    return pGLTFAccessor;
}

int
mmGLTFRoot_AccessorByteStride(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor)
{
    struct mmGLTFBufferView* bufferView = NULL;

    const struct mmVectorValue* bufferViews = &root->bufferViews;

    if (accessor->isSparse)
    {
        return mmGLTFAccessor_ByteStride(accessor, NULL);
    }

    if (accessor->bufferView == -1)
    {
        return mmGLTFAccessor_ByteStride(accessor, NULL);
    }

    bufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);

    if (NULL == bufferView)
    {
        return mmGLTFAccessor_ByteStride(accessor, NULL);
    }

    return mmGLTFAccessor_ByteStride(accessor, bufferView);
}

int
mmGLTFRoot_AccessorReadFloat(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    size_t                                         index,
    float*                                         out,
    size_t                                         element_size)
{
    struct mmByteBuffer bytes;
    struct mmGLTFBufferView* bufferView = NULL;

    const struct mmVectorValue* bufferViews = &root->bufferViews;

    int stride = 0;
    const uint8_t* element = NULL;

    if (accessor->isSparse)
    {
        return MM_FALSE;
    }
    if (accessor->bufferView == -1)
    {
        mmMemset(out, 0, element_size * sizeof(float));
        return MM_TRUE;
    }

    bufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);

    if (NULL == bufferView)
    {
        return MM_FALSE;
    }

    mmGLTFRoot_BufferViewBytes(root, bufferView, &bytes);

    if (NULL == bytes.buffer || 0 == bytes.length)
    {
        return MM_FALSE;
    }

    stride = mmGLTFAccessor_ByteStride(accessor, bufferView);

    element = bytes.buffer + bytes.offset;
    element += accessor->byteOffset + stride * index;
    return mmGLTFElementReadFloat(element, accessor->type, accessor->componentType, accessor->normalized, out, element_size);
}

size_t
mmGLTFRoot_AccessorUnpackBytesFloats(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    const struct mmByteBuffer*                     bytes,
    float*                                         out,
    size_t                                         float_count)
{
    size_t num = 0;

    do
    {
        struct mmByteBuffer hAccessorBytes;

        int stride = 0;
        const uint8_t* element = NULL;

        size_t floats_per_element = 0;
        size_t available_floats = 0;

        size_t element_count = 0;

        size_t index = 0;

        float* dest = out;
        int code = 0;

        struct mmGLTFBufferView* bufferView = NULL;

        const struct mmVectorValue* bufferViews = &root->bufferViews;

        floats_per_element = mmGLTFGetNumComponentsInType(accessor->type);
        available_floats = accessor->count * floats_per_element;
        if (out == NULL)
        {
            num = available_floats;
            break;
        }

        float_count = available_floats < float_count ? available_floats : float_count;
        element_count = float_count / floats_per_element;

        if (-1 == accessor->bufferView)
        {
            bufferView = NULL;
        }
        else
        {
            bufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);
        }

        stride = mmGLTFAccessor_ByteStride(accessor, bufferView);

        if (NULL == bytes)
        {
            mmGLTFRoot_BufferViewBytes(root, bufferView, &hAccessorBytes);
        }
        else
        {
            hAccessorBytes.buffer = bytes->buffer;
            hAccessorBytes.offset = bytes->offset;
            hAccessorBytes.length = bytes->length;
        }

        if (NULL == hAccessorBytes.buffer || 0 == hAccessorBytes.length)
        {
            num = 0;
            break;
        }

        element = hAccessorBytes.buffer + hAccessorBytes.offset;
        element += accessor->byteOffset;

        // First pass: convert each element in the base accessor.
        if ((mmGLTFComponentTypeFloat == accessor->componentType) && 
            (stride == floats_per_element * sizeof(float)))
        {
            // type is float. stride is whole element. just copy.
            mmMemcpy(dest, element, float_count * sizeof(float));
        }
        else
        {
            // upack to float.
            for (index = 0; index < element_count; index++, dest += floats_per_element)
            {
                code = mmGLTFElementReadFloat(
                    element + stride * index,
                    accessor->type,
                    accessor->componentType,
                    accessor->normalized,
                    dest,
                    floats_per_element);

                if (!code)
                {
                    break;
                }
            }
            if (!code)
            {
                num = 0;
                break;
            }
        }

        // Second pass: write out each element in the sparse accessor.
        if (accessor->isSparse)
        {
            const struct mmGLTFAccessorSparse* sparse = &accessor->sparse;

            struct mmByteBuffer iBytes;
            struct mmByteBuffer vBytes;

            const uint8_t* index_data = NULL;
            const uint8_t* reader_head = NULL;

            size_t index_stride = 0;
            size_t reader_index = 0;

            int iView = sparse->indices.bufferView;
            int vView = sparse->values.bufferView;

            struct mmGLTFBufferView* iBufferView = NULL;
            struct mmGLTFBufferView* vBufferView = NULL;

            iBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, iView);
            if (NULL == iBufferView)
            {
                num = 0;
                break;
            }

            vBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, vView);
            if (NULL == vBufferView)
            {
                num = 0;
                break;
            }

            mmGLTFRoot_BufferViewBytes(root, iBufferView, &iBytes);
            mmGLTFRoot_BufferViewBytes(root, vBufferView, &vBytes);

            if (NULL == iBytes.buffer || 0 == iBytes.length)
            {
                num = 0;
                break;
            }

            if (NULL == vBytes.buffer || 0 == vBytes.length)
            {
                num = 0;
                break;
            }

            index_data = iBytes.buffer + iBytes.offset;
            index_data += sparse->indices.byteOffset;

            reader_head = vBytes.buffer + vBytes.offset;
            reader_head += sparse->values.byteOffset;

            index_stride = mmGLTFGetComponentSizeInBytes(sparse->indices.componentType);
            for (reader_index = 0; reader_index < sparse->count; reader_index++, index_data += index_stride)
            {
                size_t writer_index = mmGLTFComponentReadIndex(index_data, sparse->indices.componentType);
                float* writer_head = out + writer_index * floats_per_element;

                code = mmGLTFElementReadFloat(
                    reader_head,
                    accessor->type,
                    accessor->componentType,
                    accessor->normalized,
                    writer_head,
                    floats_per_element);

                if (!code)
                {
                    break;
                }

                reader_head += stride;
            }
            if (!code)
            {
                num = 0;
                break;
            }
        }

        num = element_count * floats_per_element;
    } while (0);

    return num;
}

size_t
mmGLTFRoot_AccessorUnpackBytes(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    const struct mmByteBuffer*                     bytes,
    uint8_t*                                       out,
    size_t                                         count)
{
    size_t num = 0;

    do
    {
        struct mmByteBuffer hAccessorBytes;

        int stride = 0;
        const uint8_t* element = NULL;

        size_t numComponents = 0;
        int32_t componentSize = 0;
        size_t availableComponents = 0;

        size_t elementCount = 0;

        uint8_t* dest = out;

        struct mmGLTFBufferView* bufferView = NULL;

        const struct mmVectorValue* bufferViews = &root->bufferViews;

        numComponents = mmGLTFGetNumComponentsInType(accessor->type);
        availableComponents = accessor->count * numComponents;
        if (out == NULL)
        {
            num = availableComponents;
            break;
        }

        count = availableComponents < count ? availableComponents : count;
        elementCount = count / numComponents;

        if (-1 == accessor->bufferView)
        {
            bufferView = NULL;
        }
        else
        {
            bufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, accessor->bufferView);
        }

        stride = mmGLTFAccessor_ByteStride(accessor, bufferView);

        if (NULL == bytes)
        {
            mmGLTFRoot_BufferViewBytes(root, bufferView, &hAccessorBytes);
        }
        else
        {
            hAccessorBytes.buffer = bytes->buffer;
            hAccessorBytes.offset = bytes->offset;
            hAccessorBytes.length = bytes->length;
        }

        if (NULL == hAccessorBytes.buffer || 0 == hAccessorBytes.length)
        {
            num = 0;
            break;
        }

        element = hAccessorBytes.buffer + hAccessorBytes.offset;
        element += accessor->byteOffset;

        componentSize = mmGLTFGetComponentSizeInBytes(accessor->componentType);
        mmMemcpy(dest, element, count * componentSize);

        // Second pass: write out each element in the sparse accessor.
        if (accessor->isSparse)
        {
            const struct mmGLTFAccessorSparse* sparse = &accessor->sparse;

            struct mmByteBuffer iBytes;
            struct mmByteBuffer vBytes;

            const uint8_t* index_data = NULL;
            const uint8_t* reader_head = NULL;

            size_t index_stride = 0;
            size_t reader_index = 0;

            int iView = sparse->indices.bufferView;
            int vView = sparse->values.bufferView;

            struct mmGLTFBufferView* iBufferView = NULL;
            struct mmGLTFBufferView* vBufferView = NULL;

            if (iView < 0 || bufferViews->size <= iView)
            {
                num = 0;
                break;
            }

            if (vView < 0 || bufferViews->size <= vView)
            {
                num = 0;
                break;
            }

            iBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, iView);
            if (NULL == iBufferView)
            {
                num = 0;
                break;
            }

            vBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, vView);
            if (NULL == vBufferView)
            {
                num = 0;
                break;
            }

            mmGLTFRoot_BufferViewBytes(root, iBufferView, &iBytes);
            mmGLTFRoot_BufferViewBytes(root, vBufferView, &vBytes);

            if (NULL == iBytes.buffer || 0 == iBytes.length)
            {
                num = 0;
                break;
            }

            if (NULL == vBytes.buffer || 0 == vBytes.length)
            {
                num = 0;
                break;
            }

            index_data = iBytes.buffer + iBytes.offset;
            index_data += sparse->indices.byteOffset;

            reader_head = vBytes.buffer + vBytes.offset;
            reader_head += sparse->values.byteOffset;

            index_stride = mmGLTFGetComponentSizeInBytes(sparse->indices.componentType);
            for (reader_index = 0; reader_index < sparse->count; reader_index++, index_data += index_stride)
            {
                size_t writer_index = mmGLTFComponentReadIndex(index_data, sparse->indices.componentType);
                uint8_t* writer_head = out + writer_index * numComponents * componentSize;
                mmMemcpy(writer_head, reader_head, numComponents * componentSize);
                reader_head += stride;
            }
        }

        num = elementCount * numComponents;
    } while (0);

    return num;
}