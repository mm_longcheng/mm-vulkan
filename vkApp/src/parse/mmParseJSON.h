/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
* MIT License
*
* Copyright (c) 2010 Serge Zaitsev
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef __mmParseJSON_h__
#define __mmParseJSON_h__

#include "core/mmCore.h"

#include "container/mmVectorVpt.h"

#include "core/mmPrefix.h"

#define MM_EXPORT_DLL_

// reference
// https://github.com/zserge/jsmn
// jsmn is good, but we hard to know how many token we need, jsmn_parse(p, js, len, NULL, 0) is expensive.
// This means it needs to be parsed twice. Although it will be slightly faster the first time.
//
// We use TokenPagePool to improve. Although this will use dynamic memory allocation.
//
// Performance Testing:
// parse semanticscholar-corpus.json 100 times.
// dt 1: 3461539 us improve
// dt 2: 5873640 us jsmn

/* MM_JSON_PARENT_LINKS is necessary to make parsing large structures linear in input size */
#define MM_JSON_PARENT_LINKS
/* MM_JSON_STRICT is necessary to reject invalid JSON documents */
#define MM_JSON_STRICT

/**
 * JSON type identifier. Basic types are:
 *     o Object
 *     o Array
 *     o String
 *     o Other primitive: number, boolean (true/false) or null
*/
enum mmJsonType_t
{
    mmJsonTypeUndefined = 0,
    mmJsonTypeObject    = 1,
    mmJsonTypeArray     = 2,
    mmJsonTypeString    = 3,
    mmJsonTypePrimitive = 4,
};

enum mmJsonError_t
{
    /* Not enough tokens were provided */
    mmJsonErrorMemory = -1,
    /* Invalid character inside JSON string */
    mmJsonErrorSymbol = -2,
    /* The string is not a full JSON packet, more bytes expected */
    mmJsonErrorString = -3,
};

/**
 * JSON token description.
 *     o type        type (object, array, string etc.)
 *     o start        start position in JSON data string
 *     o end        end position in JSON data string
 *     o size        size for JSON data string
*/
struct mmJsonToken
{
    int type;
    int start;
    int end;
    int size;
#ifdef MM_JSON_PARENT_LINKS
    int parent;
#endif
};

enum
{
    mmJsonTokenPageNumber = (64),
    mmJsonTokenPageMemory = (mmJsonTokenPageNumber * sizeof(struct mmJsonToken)),
    mmJsonTokenPageRemainder = mmJsonTokenPageNumber - 1,
    mmJsonTokenPageDivision = (6),
};

struct mmJsonTokenPage
{
    struct mmJsonToken tokens[mmJsonTokenPageNumber];
};

/**
 * JSON parser. Contains an array of token blocks available. Also stores
 * the string being parsed now and current position in that string.
*/
struct mmParseJSON
{
    /* token page. */
    struct mmVectorVpt pages;

    /* json buffer. */
    const char* jbuffer;
    /* json length. */
    size_t jlength;

    /* offset in the JSON string */
    unsigned int pos;
    /* next token to allocate */
    unsigned int toknext;
    /* superior token node, e.g. parent object or array */
    int toksuper;
    
    /* Analysis code. */
    int code;
};
MM_EXPORT_DLL_ void mmParseJSON_Init(struct mmParseJSON* p);
MM_EXPORT_DLL_ void mmParseJSON_Destroy(struct mmParseJSON* p);
MM_EXPORT_DLL_ void mmParseJSON_Reset(struct mmParseJSON* p);
MM_EXPORT_DLL_ void mmParseJSON_SetJsonBuffer(struct mmParseJSON* p, const char* jbuffer, size_t jlength);
MM_EXPORT_DLL_ struct mmJsonToken* mmParseJSON_GetToken(struct mmParseJSON* p, int idx);
MM_EXPORT_DLL_ struct mmJsonToken* mmParseJSON_AddToken(struct mmParseJSON* p);
MM_EXPORT_DLL_ void mmParseJSON_DestroyToken(struct mmParseJSON* p);
MM_EXPORT_DLL_ void mmParseJSON_Analysis(struct mmParseJSON* p);

#include "core/mmSuffix.h"

#endif//__mmParseJSON_h__
