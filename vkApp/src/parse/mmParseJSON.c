/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
* MIT License
*
* Copyright (c) 2010 Serge Zaitsev
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "mmParseJSON.h"

#include "core/mmAlloc.h"

static void mmJsonToken_Fill(struct mmJsonToken* p, int type, int start, int end);
static void mmParseJSON_ParseString(struct mmParseJSON* p);
static void mmParseJSON_ParsePrimitive(struct mmParseJSON* p);

MM_EXPORT_DLL_ void mmParseJSON_Init(struct mmParseJSON* p)
{
    mmVectorVpt_Init(&p->pages);
    p->jbuffer = NULL;
    p->jlength = 0;
    p->pos = 0;
    p->toknext = 0;
    p->toksuper = 0;
}
MM_EXPORT_DLL_ void mmParseJSON_Destroy(struct mmParseJSON* p)
{
    mmParseJSON_DestroyToken(p);

    p->toksuper = 0;
    p->toknext = 0;
    p->pos = 0;
    p->jlength = 0;
    p->jbuffer = NULL;
    mmVectorVpt_Destroy(&p->pages);
}
MM_EXPORT_DLL_ void mmParseJSON_Reset(struct mmParseJSON* p)
{
    // Pages not need reset. Tokens not need destroy.
    p->jbuffer = NULL;
    p->jlength = 0;
    p->pos = 0;
    p->toknext = 0;
    p->toksuper = 0;
}
MM_EXPORT_DLL_ void mmParseJSON_SetJsonBuffer(struct mmParseJSON* p, const char* jbuffer, size_t jlength)
{
    p->jbuffer = jbuffer;
    p->jlength = jlength;
}
MM_EXPORT_DLL_ struct mmJsonToken* mmParseJSON_GetToken(struct mmParseJSON* p, int idx)
{
    struct mmJsonTokenPage* page = NULL;
    unsigned int pid = idx >> mmJsonTokenPageDivision;
    unsigned int tid = idx & mmJsonTokenPageRemainder;
    page = (struct mmJsonTokenPage*)p->pages.arrays[pid];
    return &page->tokens[tid];
}
MM_EXPORT_DLL_ struct mmJsonToken* mmParseJSON_AddToken(struct mmParseJSON* p)
{
    struct mmJsonTokenPage* page = NULL;
    struct mmJsonToken* token = NULL;
    int idx = p->toknext++;
    unsigned int pid = idx >> mmJsonTokenPageDivision;
    unsigned int tid = idx & mmJsonTokenPageRemainder;
    if (pid >= p->pages.size)
    {
        mmVectorVpt_AlignedMemory(&p->pages, pid + 1);
    }
    page = (struct mmJsonTokenPage*)p->pages.arrays[pid];
    if (NULL == page)
    {
        page = (struct mmJsonTokenPage*)mmMalloc(mmJsonTokenPageMemory);
        mmMemset(page, 0, mmJsonTokenPageMemory);
        mmVectorVpt_SetIndex(&p->pages, pid, page);
    }
    token = &page->tokens[tid];
    token->start = token->end = -1;
    token->size = 0;
#ifdef MM_JSON_PARENT_LINKS
    token->parent = -1;
#endif
    return token;
}
MM_EXPORT_DLL_ void mmParseJSON_DestroyToken(struct mmParseJSON* p)
{
    struct mmJsonTokenPage* page = NULL;
    size_t i = 0;
    size_t size = mmVectorVpt_Size(&p->pages);
    for (i = 0; i < size; ++i)
    {
        page = (struct mmJsonTokenPage*)p->pages.arrays[i];
        mmFree(page);
    }
    mmVectorVpt_Clear(&p->pages);
}

MM_EXPORT_DLL_ void mmParseJSON_Analysis(struct mmParseJSON* p)
{
    int i;
    char c;
    int type;

    struct mmJsonToken* token;
    struct mmJsonToken* t = NULL;
    int count = p->toknext;
    
    const char* jbuffer = p->jbuffer;
    size_t jlength = p->jlength;
    
    // reset context.
    p->pos = 0;
    p->toknext = 0;
    p->toksuper = -1;

    for (; p->pos < jlength && jbuffer[p->pos] != '\0'; p->pos++)
    {
        c = jbuffer[p->pos];
        switch (c) 
        {
        case '{':
        case '[':
            count++;
            token = mmParseJSON_AddToken(p);
            if (token == NULL)
            {
                p->code = mmJsonErrorMemory;
                return;
            }
            if (p->toksuper != -1)
            {
                t = mmParseJSON_GetToken(p, p->toksuper);
#ifdef MM_JSON_STRICT
                /* In strict mode an object or array can't become a key */
                if (mmJsonTypeObject == t->type)
                {
                    p->code = mmJsonErrorSymbol;
                    return;
                }
#endif
                t->size++;
#ifdef MM_JSON_PARENT_LINKS
                token->parent = p->toksuper;
#endif
            }
            token->type = (c == '{' ? mmJsonTypeObject : mmJsonTypeArray);
            token->start = p->pos;
            p->toksuper = p->toknext - 1;
            break;
        case '}':
        case ']':
            type = (c == '}' ? mmJsonTypeObject : mmJsonTypeArray);
#ifdef MM_JSON_PARENT_LINKS
            if (p->toknext < 1)
            {
                p->code = mmJsonErrorSymbol;
                return;
            }
            token = mmParseJSON_GetToken(p, p->toknext - 1);
            for (;;)
            {
                if (token->start != -1 && token->end == -1)
                {
                    if (token->type != type)
                    {
                        p->code = mmJsonErrorSymbol;
                        return;
                    }
                    token->end = p->pos + 1;
                    p->toksuper = token->parent;
                    break;
                }
                if (token->parent == -1)
                {
                    if (token->type != type || p->toksuper == -1)
                    {
                        p->code = mmJsonErrorSymbol;
                        return;
                    }
                    break;
                }
                token = mmParseJSON_GetToken(p, token->parent);
            }
#else
            for (i = p->toknext - 1; i >= 0; i--)
            {
                token = mmParseJSON_GetToken(p, i);
                if (token->start != -1 && token->end == -1)
                {
                    if (token->type != type)
                    {
                        p->error = mmJsonErrorSymbol;
                        return;
                    }
                    p->toksuper = -1;
                    token->end = p->pos + 1;
                    break;
                }
            }
            /* Error if unmatched closing bracket */
            if (i == -1)
            {
                p->error = mmJsonErrorSymbol;
                return;
            }
            for (; i >= 0; i--)
            {
                token = mmParseJSON_GetToken(p, i);
                if (token->start != -1 && token->end == -1)
                {
                    p->toksuper = i;
                    break;
                }
            }
#endif
            break;
        case '\"':
            mmParseJSON_ParseString(p);
            if (0 > p->code)
            {
                return;
            }
            count++;
            if (p->toksuper != -1)
            {
                t = mmParseJSON_GetToken(p, p->toksuper);
                t->size++;
            }
            break;
        case '\t':
        case '\r':
        case '\n':
        case ' ':
            break;
        case ':':
            p->toksuper = p->toknext - 1;
            break;
        case ',':
            t = mmParseJSON_GetToken(p, p->toksuper);
            if (NULL != t && p->toksuper != -1 &&
                t->type != mmJsonTypeArray &&
                t->type != mmJsonTypeObject)
            {
#ifdef MM_JSON_PARENT_LINKS
                p->toksuper = t->parent;
#else
                for (i = p->toknext - 1; i >= 0; i--)
                {
                    t = mmParseJSON_GetToken(p, i);
                    if (t->type == mmJsonTypeArray || t->type == mmJsonTypeObject)
                    {
                        if (t->start != -1 && t->end == -1)
                        {
                            p->toksuper = i;
                            break;
                        }
                    }
                }
#endif
            }
            break;
#ifdef MM_JSON_STRICT
            /* In strict mode primitives are: numbers and booleans */
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 't':
        case 'f':
        case 'n':
            /* And they must not be keys of the object */
            if (p->toksuper != -1)
            {
                t = mmParseJSON_GetToken(p, p->toksuper);
                if ((t->type == mmJsonTypeObject) ||
                    (t->type == mmJsonTypeString && t->size != 0))
                {
                    p->code = mmJsonErrorSymbol;
                    return;
                }
            }
#else
            /* In non-strict mode every unquoted value is a primitive */
        default:
#endif
            mmParseJSON_ParsePrimitive(p);
            if (0 > p->code)
            {
                return;
            }
            count++;
            if (p->toksuper != -1)
            {
                t = mmParseJSON_GetToken(p, p->toksuper);
                t->size++;
            }
            break;

#ifdef MM_JSON_STRICT
            /* Unexpected char in strict mode */
        default:
            p->code = mmJsonErrorSymbol;
            return;
#endif
        }
    }

    for (i = p->toknext - 1; i >= 0; i--)
    {
        /* Unmatched opened object or array */
        t = mmParseJSON_GetToken(p, i);
        if (t->start != -1 && t->end == -1)
        {
            p->code = mmJsonErrorString;
            return;
        }
    }

    p->code = count;
}

static void mmJsonToken_Fill(struct mmJsonToken* p, int type, int start, int end)
{
    p->type = type;
    p->start = start;
    p->end = end;
    p->size = 0;
}

/**
 * Fills next token with JSON string.
 */
static void mmParseJSON_ParseString(struct mmParseJSON* p)
{
    int i;
    char c;
    
    struct mmJsonToken* token;
    
    const char* jbuffer = p->jbuffer;
    size_t jlength = p->jlength;

    int start = p->pos;

    p->pos++;

    /* Skip starting quote */
    for (; p->pos < jlength && jbuffer[p->pos] != '\0'; p->pos++)
    {
        c = jbuffer[p->pos];

        /* Quote: end of string */
        if (c == '\"')
        {
            token = mmParseJSON_AddToken(p);
            if (NULL == token)
            {
                p->pos = start;
                p->code = mmJsonErrorMemory;
                return;
            }
            mmJsonToken_Fill(token, mmJsonTypeString, start + 1, p->pos);
#ifdef MM_JSON_PARENT_LINKS
            token->parent = p->toksuper;
#endif
            p->code = 0;
            return;
        }

        /* Backslash: Quoted symbol expected */
        if (c == '\\' && p->pos + 1 < jlength)
        {
            p->pos++;
            switch (jbuffer[p->pos])
            {
            /* Allowed escaped symbols */
            case '\"':
            case '/':
            case '\\':
            case 'b':
            case 'f':
            case 'r':
            case 'n':
            case 't':
                break;
            /* Allows escaped symbol \uXXXX */
            case 'u':
                p->pos++;
                for (i = 0; i < 4 && p->pos < jlength && jbuffer[p->pos] != '\0'; i++)
                {
                    /* If it isn't a hex character we have an error */
                    c = jbuffer[p->pos];
                    if (!(
                        (c >= 48 && c <=  57) ||  /* 0-9 */
                        (c >= 65 && c <=  70) ||  /* A-F */
                        (c >= 97 && c <= 102)))   /* a-f */
                    {
                        p->pos = start;

                        p->code = mmJsonErrorSymbol;
                        return;
                    }
                    p->pos++;
                }
                p->pos--;
                break;
            /* Unexpected symbol */
            default:
                p->pos = start;
                p->code = mmJsonErrorSymbol;
                return;
            }
        }
    }
    p->pos = start;
    p->code = mmJsonErrorString;
}

/**
 * Fills next available token with JSON primitive.
 */
static void mmParseJSON_ParsePrimitive(struct mmParseJSON* p)
{
    char c;
    int start;
    int find = 0;
    struct mmJsonToken* token;

    const char* jbuffer = p->jbuffer;
    size_t jlength = p->jlength;
    
    start = p->pos;

    for (; p->pos < jlength && jbuffer[p->pos] != '\0'; p->pos++)
    {
        switch (jbuffer[p->pos])
        {
#ifndef MM_JSON_STRICT
        /* In strict mode primitive must be followed by "," or "}" or "]" */
        case ':':
#endif
        case '\t':
        case '\r':
        case '\n':
        case ' ':
        case ',':
        case ']':
        case '}':
            find = 1;
            break;
        default:
            /* to quiet a warning from gcc*/
            break;
        }
        if (1 == find)
        {
            break;
        }
        c = jbuffer[p->pos];
        if (c < 32 || c >= 127)
        {
            p->pos = start;
            p->code = mmJsonErrorSymbol;
            return;
        }
    }
    
    if (0 == find)
    {
#ifdef MM_JSON_STRICT
        /* In strict mode primitive must be followed by a comma/object/array */
        p->pos = start;
        p->code = mmJsonErrorString;
#endif
    }
    else
    {
        token = mmParseJSON_AddToken(p);
        if (NULL == token)
        {
            p->pos = start;
            p->code = mmJsonErrorMemory;
            return;
        }
        mmJsonToken_Fill(token, mmJsonTypePrimitive, start, p->pos);
#ifdef MM_JSON_PARENT_LINKS
        token->parent = p->toksuper;
#endif
        p->pos--;
        p->code = 0;
    }
}
