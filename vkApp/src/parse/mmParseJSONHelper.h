/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmParseJSONHelper_h__
#define __mmParseJSONHelper_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "math/mmRange.h"

#include "container/mmVectorValue.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

typedef void (*mmParseJSONNumberDecodeFunc)(const char*, void*);
typedef int(*mmParseJSONObjectDecodeFunc)(struct mmParseJSON* p, int i, mmUInt32_t h, void* v);
typedef int(*mmParseJSONMemberDecodeFunc)(struct mmParseJSON* p, int i, mmUInt32_t m, void* v);

int mmParseJSON_SkipToken(struct mmParseJSON* p, int i);
void mmParseJSON_GetTokenBuffer(struct mmParseJSON* p, struct mmJsonToken* t, char* ss, size_t sz);
void mmParseJSON_GetRangeBuffer(struct mmParseJSON* p, struct mmRange* r, char* ss, size_t sz);
void mmParseJSON_GetRangeString(struct mmParseJSON* p, struct mmRange* r, struct mmString* s);
void mmParseJSON_GetHash(struct mmParseJSON* p, struct mmJsonToken* t, mmUInt32_t* v);
int mmParseJSON_DecodeNumber(struct mmParseJSON* p, int i, void* v, const void* d, int z, void* f);
int mmParseJSON_DecodeEnumerate(struct mmParseJSON* p, int i, void* v, const void* d, int z, void* f);

int mmParseJSON_DecodeSChar(struct mmParseJSON* p, int i, mmSChar_t* v, mmSChar_t d);
int mmParseJSON_DecodeUChar(struct mmParseJSON* p, int i, mmUChar_t* v, mmUChar_t d);
int mmParseJSON_DecodeSShort(struct mmParseJSON* p, int i, mmSShort_t* v, mmSShort_t d);
int mmParseJSON_DecodeUShort(struct mmParseJSON* p, int i, mmUShort_t* v, mmUShort_t d);
int mmParseJSON_DecodeSInt(struct mmParseJSON* p, int i, mmSInt_t* v, mmSInt_t d);
int mmParseJSON_DecodeUInt(struct mmParseJSON* p, int i, mmUInt_t* v, mmUInt_t d);
int mmParseJSON_DecodeSLong(struct mmParseJSON* p, int i, mmSLong_t* v, mmSLong_t d);
int mmParseJSON_DecodeULong(struct mmParseJSON* p, int i, mmULong_t* v, mmULong_t d);
int mmParseJSON_DecodeSLLong(struct mmParseJSON* p, int i, mmSLLong_t* v, mmSLLong_t d);
int mmParseJSON_DecodeULLong(struct mmParseJSON* p, int i, mmULLong_t* v, mmULLong_t d);
int mmParseJSON_DecodeSInt32(struct mmParseJSON* p, int i, mmSInt32_t* v, mmSInt32_t d);
int mmParseJSON_DecodeUInt32(struct mmParseJSON* p, int i, mmUInt32_t* v, mmUInt32_t d);
int mmParseJSON_DecodeSInt64(struct mmParseJSON* p, int i, mmSInt64_t* v, mmSInt64_t d);
int mmParseJSON_DecodeUInt64(struct mmParseJSON* p, int i, mmUInt64_t* v, mmUInt64_t d);
int mmParseJSON_DecodeSize(struct mmParseJSON* p, int i, mmSize_t* v, mmSize_t d);
int mmParseJSON_DecodeFloat(struct mmParseJSON* p, int i, float* v, float d);
int mmParseJSON_DecodeDouble(struct mmParseJSON* p, int i, double* v, double d);
int mmParseJSON_DecodeBool(struct mmParseJSON* p, int i, mmBool_t* v, mmBool_t d);
int mmParseJSON_DecodeString(struct mmParseJSON* p, int i, struct mmString* v, const char* d);
int mmParseJSON_DecodeRange(struct mmParseJSON* p, int i, struct mmRange* v, const struct mmRange* d);
int mmParseJSON_DecodeObject(struct mmParseJSON* p, int i, void* v, void* f);
int mmParseJSON_DecodeObjectArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f);
int mmParseJSON_DecodeMember(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f);
int mmParseJSON_DecodeMemberArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f);

int mmParseJSON_DecodeNumberArray(struct mmParseJSON* p, int i, void* v, const void* d, int z, int mz, int* fz, void* f);
int mmParseJSON_DecodeEnumerateArray(struct mmParseJSON* p, int i, void* v, const void* d, int z, int mz, int* fz, void* f);

int mmParseJSON_DecodeFloatArray(struct mmParseJSON* p, int i, void* v, const void* d, int mz, int* fz);
int mmParseJSON_DecodeDoubleArray(struct mmParseJSON* p, int i, void* v, const void* d, int mz, int* fz);

int mmParseJSON_DecodeNumberVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const void* d, void* f);
int mmParseJSON_DecodeRangeVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const struct mmRange* d);
int mmParseJSON_DecodeStringVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const char* d);

int mmParseJSON_DecodeVectorObjectArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f);

#include "core/mmSuffix.h"

#endif//__mmParseJSONHelper_h__
