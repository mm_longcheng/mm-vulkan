/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmParseJSONHelper.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"

#include "algorithm/mmMurmurHash.h"

int mmParseJSON_SkipToken(struct mmParseJSON* p, int i)
{
    struct mmJsonToken* t = NULL;
    int end = i + 1;

    while (i < end)
    {
        t = mmParseJSON_GetToken(p, i);
        switch (t->type)
        {
        case mmJsonTypeObject:
            end += t->size * 2;
            break;

        case mmJsonTypeArray:
            end += t->size;
            break;

        case mmJsonTypeString:
        case mmJsonTypePrimitive:
            break;

        default:
            return -1;
        }

        i++;
    }

    return i;
}

void mmParseJSON_GetTokenBuffer(struct mmParseJSON* p, struct mmJsonToken* t, char* ss, size_t sz)
{
    size_t tz = t->end - t->start;
    size_t rz = (tz <= sz) ? tz : (sz - 1);
    const char* bs = (const char*)(p->jbuffer + t->start);
    mmMemcpy(ss, bs, rz);
}

void mmParseJSON_GetRangeBuffer(struct mmParseJSON* p, struct mmRange* r, char* ss, size_t sz)
{
    size_t tz = r->l;
    size_t rz = (tz <= sz) ? tz : (sz - 1);
    const char* bs = (const char*)(p->jbuffer + r->o);
    mmMemcpy(ss, bs, rz);
}

void mmParseJSON_GetRangeString(struct mmParseJSON* p, struct mmRange* r, struct mmString* s)
{
    if (0 == r->l)
    {
        mmString_Clear(s);
    }
    else
    {
        mmString_Resize(s, r->l);
        mmParseJSON_GetRangeBuffer(p, r, (char*)mmString_Data(s), r->l);
    }
}

void mmParseJSON_GetHash(struct mmParseJSON* p, struct mmJsonToken* t, mmUInt32_t* v)
{
    size_t tz = t->end - t->start;
    const char* bs = (const char*)(p->jbuffer + t->start);
    (*v) = (mmUInt32_t)mmMurmurHash2((const void*)bs, (int)tz, 0);
}

int mmParseJSON_DecodeNumber(struct mmParseJSON* p, int i, void* v, const void* d, int z, void* f)
{
    struct mmJsonToken* token = NULL;
    mmParseJSONNumberDecodeFunc func = (mmParseJSONNumberDecodeFunc)(f);
    if (i >= p->code)
    {
        mmMemcpy(v, d, z);
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        mmMemcpy(v, d, z);
        return MM_ERROR;
    }
    if (mmJsonTypePrimitive != token->type)
    {
        mmMemcpy(v, d, z);
    }
    else
    {
        size_t tz = token->end - token->start;
        char* bs = (char*)(p->jbuffer + token->start);
        char x = bs[tz];
        bs[tz] = 0;
        (*func)(bs, v);
        bs[tz] = x;
    }
    return i + 1;
}
int mmParseJSON_DecodeEnumerate(struct mmParseJSON* p, int i, void* v, const void* d, int z, void* f)
{
    struct mmJsonToken* token = NULL;
    mmParseJSONNumberDecodeFunc func = (mmParseJSONNumberDecodeFunc)(f);
    if (i >= p->code)
    {
        mmMemcpy(v, d, z);
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        mmMemcpy(v, d, z);
        return MM_ERROR;
    }
    if (mmJsonTypeString != token->type)
    {
        mmMemcpy(v, d, z);
    }
    else
    {
        size_t tz = token->end - token->start;
        char* bs = (char*)(p->jbuffer + token->start);
        char x = bs[tz];
        bs[tz] = 0;
        (*func)(bs, v);
        bs[tz] = x;
    }
    return i + 1;
}

int mmParseJSON_DecodeSChar(struct mmParseJSON* p, int i, mmSChar_t* v, mmSChar_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSChar_t), &mmValue_AToSChar);
}
int mmParseJSON_DecodeUChar(struct mmParseJSON* p, int i, mmUChar_t* v, mmUChar_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmUChar_t), &mmValue_AToUChar);
}

int mmParseJSON_DecodeSShort(struct mmParseJSON* p, int i, mmSShort_t* v, mmSShort_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSShort_t), &mmValue_AToSShort);
}
int mmParseJSON_DecodeUShort(struct mmParseJSON* p, int i, mmUShort_t* v, mmUShort_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmUShort_t), &mmValue_AToUShort);
}

int mmParseJSON_DecodeSInt(struct mmParseJSON* p, int i, mmSInt_t* v, mmSInt_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSInt_t), &mmValue_AToSInt);
}
int mmParseJSON_DecodeUInt(struct mmParseJSON* p, int i, mmUInt_t* v, mmUInt_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmUInt_t), &mmValue_AToUInt);
}

int mmParseJSON_DecodeSLong(struct mmParseJSON* p, int i, mmSLong_t* v, mmSLong_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSLong_t), &mmValue_AToSLong);
}
int mmParseJSON_DecodeULong(struct mmParseJSON* p, int i, mmULong_t* v, mmULong_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmULong_t), &mmValue_AToULong);
}

int mmParseJSON_DecodeSLLong(struct mmParseJSON* p, int i, mmSLLong_t* v, mmSLLong_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSLLong_t), &mmValue_AToSLLong);
}
int mmParseJSON_DecodeULLong(struct mmParseJSON* p, int i, mmULLong_t* v, mmULLong_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmULLong_t), &mmValue_AToULLong);
}

int mmParseJSON_DecodeSInt32(struct mmParseJSON* p, int i, mmSInt32_t* v, mmSInt32_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSInt32_t), &mmValue_AToSInt32);
}
int mmParseJSON_DecodeUInt32(struct mmParseJSON* p, int i, mmUInt32_t* v, mmUInt32_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmUInt32_t), &mmValue_AToUInt32);
}

int mmParseJSON_DecodeSInt64(struct mmParseJSON* p, int i, mmSInt64_t* v, mmSInt64_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSInt64_t), &mmValue_AToSInt64);
}
int mmParseJSON_DecodeUInt64(struct mmParseJSON* p, int i, mmUInt64_t* v, mmUInt64_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmUInt64_t), &mmValue_AToUInt64);
}
int mmParseJSON_DecodeSize(struct mmParseJSON* p, int i, mmSize_t* v, mmSize_t d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(mmSize_t), &mmValue_AToSize);
}
int mmParseJSON_DecodeFloat(struct mmParseJSON* p, int i, float* v, float d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(float), &mmValue_AToFloat);
}
int mmParseJSON_DecodeDouble(struct mmParseJSON* p, int i, double* v, double d)
{
    return mmParseJSON_DecodeNumber(p, i, v, &d, sizeof(double), &mmValue_AToDouble);
}

int mmParseJSON_DecodeBool(struct mmParseJSON* p, int i, mmBool_t* v, mmBool_t d)
{
    struct mmJsonToken* token = NULL;
    if (i >= p->code)
    {
        (*v) = d;
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        (*v) = d;
        return MM_ERROR;
    }
    if (mmJsonTypePrimitive != token->type)
    {
        (*v) = d;
    }
    else
    {
        size_t tz = token->end - token->start;
        const char* bs = (const char*)(p->jbuffer + token->start);
        (*v) = (4 == tz) && (0 == mmMemcmp(bs, "true", 4));
    }
    return i + 1;
}

int mmParseJSON_DecodeString(struct mmParseJSON* p, int i, struct mmString* v, const char* d)
{
    struct mmJsonToken* token = NULL;
    if (i >= p->code)
    {
        mmString_Assigns(v, d);
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        mmString_Assigns(v, d);
        return MM_ERROR;
    }
    if (mmJsonTypeString != token->type)
    {
        mmString_Assigns(v, d);
    }
    else
    {
        size_t tz = token->end - token->start;
        const char* bs = (const char*)(p->jbuffer + token->start);
        mmString_Assignsn(v, bs, tz);
    }
    return i + 1;
}

int mmParseJSON_DecodeRange(struct mmParseJSON* p, int i, struct mmRange* v, const struct mmRange* d)
{
    struct mmJsonToken* token = NULL;
    if (i >= p->code)
    {
        mmRange_Assign(v, d);
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        mmRange_Assign(v, d);
        return MM_ERROR;
    }
    if (mmJsonTypeString != token->type)
    {
        mmRange_Assign(v, d);
        i = mmParseJSON_SkipToken(p, i);
        return i;
    }
    else
    {
        v->o = token->start;
        v->l = token->end - token->start;
        return i + 1;
    }
}

int mmParseJSON_DecodeObject(struct mmParseJSON* p, int i, void* v, void* f)
{
    int n = 0;
    mmUInt32_t h = 0;
    struct mmJsonToken* token = NULL;
    struct mmJsonToken* k = NULL;
    int size = 0;
    mmParseJSONObjectDecodeFunc func = (mmParseJSONObjectDecodeFunc)(f);

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeObject != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    for (n = 0; n < size; ++n)
    {
        if (i >= p->code)
        {
            return MM_ERROR;
        }
        k = mmParseJSON_GetToken(p, i);
        if (mmJsonTypeString != k->type || k->size == 0)
        {
            return MM_ERROR;
        }

        mmParseJSON_GetHash(p, k, &h);

        i = (*func)(p, i, h, v);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeObjectArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    void* u = NULL;
    int size = 0;

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = mmVectorValue_At(v, n);
        i = mmParseJSON_DecodeObject(p, i, u, f);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeMember(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f)
{
    int m = 0;
    struct mmJsonToken* token = NULL;
    void* u = NULL;
    int size = 0;
    mmParseJSONMemberDecodeFunc func = (mmParseJSONMemberDecodeFunc)(f);

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeObject != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (m = 0; m < size; ++m)
    {
        u = mmVectorValue_At(v, m);

        i = (*func)(p, i, (mmUInt32_t)m, u);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeMemberArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    void* u = NULL;
    int size = 0;

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = mmVectorValue_At(v, n);
        i = mmParseJSON_DecodeMember(p, i, u, f);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeNumberArray(struct mmParseJSON* p, int i, void* v, const void* d, int z, int mz, int* fz, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    int size = 0;
    int lsz = 0;
    int rsz = 0;
    char* vn = NULL;

    if (NULL != fz)
    {
        (*fz) = 0;
    }

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    lsz = mz < 0 ? size : mz;
    rsz = size > lsz ? lsz : size;

    for (n = 0; n < rsz; ++n)
    {
        vn = (char*)v + z * n;
        i = mmParseJSON_DecodeNumber(p, i, vn, d, z, f);

        if (i < 0)
        {
            return i;
        }
    }

    if (NULL != fz)
    {
        (*fz) = rsz;
    }

    return i;
}

int mmParseJSON_DecodeEnumerateArray(struct mmParseJSON* p, int i, void* v, const void* d, int z, int mz, int* fz, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    int size = 0;
    int lsz = 0;
    int rsz = 0;
    char* vn = NULL;

    if (NULL != fz)
    {
        (*fz) = 0;
    }

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    lsz = mz < 0 ? size : mz;
    rsz = size > lsz ? lsz : size;

    for (n = 0; n < rsz; ++n)
    {
        vn = (char*)v + z * n;
        i = mmParseJSON_DecodeEnumerate(p, i, vn, d, z, f);

        if (i < 0)
        {
            return i;
        }
    }

    if (NULL != fz)
    {
        (*fz) = rsz;
    }

    return i;
}

int mmParseJSON_DecodeFloatArray(struct mmParseJSON* p, int i, void* v, const void* d, int mz, int* fz)
{
    return mmParseJSON_DecodeNumberArray(p, i, v, d, (int)sizeof(float), mz, fz, &mmValue_AToFloat);
}
int mmParseJSON_DecodeDoubleArray(struct mmParseJSON* p, int i, void* v, const void* d, int mz, int* fz)
{
    return mmParseJSON_DecodeNumberArray(p, i, v, d, (int)sizeof(double), mz, fz, &mmValue_AToDouble);
}

int mmParseJSON_DecodeNumberVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const void* d, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    void* u = NULL;
    int size = 0;
    int esz = (int)mmVectorValue_GetElement(v);

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = mmVectorValue_At(v, n);

        i = mmParseJSON_DecodeNumber(p, i, u, d, esz, f);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeRangeVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const struct mmRange* d)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    struct mmRange* u = NULL;
    int size = 0;

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = (struct mmRange*)mmVectorValue_At(v, n);
        i = mmParseJSON_DecodeRange(p, i, u, d);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeStringVector(struct mmParseJSON* p, int i, struct mmVectorValue* v, const char* d)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    struct mmString* u = NULL;
    int size = 0;

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = (struct mmString*)mmVectorValue_At(v, n);
        i = mmParseJSON_DecodeString(p, i, u, d);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}

int mmParseJSON_DecodeVectorObjectArray(struct mmParseJSON* p, int i, struct mmVectorValue* v, void* f)
{
    int n = 0;
    struct mmJsonToken* token = NULL;
    void* u = NULL;
    int size = 0;

    if (i >= p->code)
    {
        return MM_ERROR;
    }
    token = mmParseJSON_GetToken(p, i);
    if (NULL == token)
    {
        return MM_ERROR;
    }
    if (mmJsonTypeArray != token->type)
    {
        return MM_ERROR;
    }

    size = token->size;
    ++i;

    mmVectorValue_AllocatorMemory(v, size);

    for (n = 0; n < size; ++n)
    {
        u = mmVectorValue_At(v, n);
        i = mmParseJSON_DecodeObjectArray(p, i, u, f);

        if (i < 0)
        {
            return i;
        }
    }

    return i;
}
