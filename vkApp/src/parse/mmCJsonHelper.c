/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCJsonHelper.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"

void mmCJson_EncodeMember(cJSON* j, size_t n, const void* v, const void* f)
{
    typedef void(*mmJSONEncodeFunc)(const void* p, cJSON* j);
    cJSON* item = cJSON_CreateObject();
    (*((mmJSONEncodeFunc)f))(v, item);
    if (!cJSON_AddItemToArray(j, item))
    {
        cJSON_Delete(item);
    }
}
void mmCJson_EncodeObject(cJSON* j, const char* n, const void* v, const void* f)
{
    typedef void(*mmJSONEncodeFunc)(const void* p, cJSON* j);
    cJSON* item = cJSON_CreateObject();
    (*((mmJSONEncodeFunc)f))(v, item);
    if (!cJSON_AddItemToObject(j, n, item))
    {
        cJSON_Delete(item);
    }
}
void mmCJson_EncodeSChar(cJSON* j, const char* n, const mmSChar_t* v, mmSChar_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUChar(cJSON* j, const char* n, const mmUChar_t* v, mmUChar_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSShort(cJSON* j, const char* n, const mmSShort_t* v, mmSShort_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUShort(cJSON* j, const char* n, const mmUShort_t* v, mmUShort_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSInt(cJSON* j, const char* n, const mmSInt_t* v, mmSInt_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUInt(cJSON* j, const char* n, const mmUInt_t* v, mmUInt_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSLong(cJSON* j, const char* n, const mmSLong_t* v, mmSLong_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeULong(cJSON* j, const char* n, const mmULong_t* v, mmULong_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSLLong(cJSON* j, const char* n, const mmSLLong_t* v, mmSLLong_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeULLong(cJSON* j, const char* n, const mmULLong_t* v, mmULLong_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSInt8(cJSON* j, const char* n, const mmSInt8_t* v, mmSInt8_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUInt8(cJSON* j, const char* n, const mmUInt8_t* v, mmUInt8_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSInt16(cJSON* j, const char* n, const mmSInt16_t* v, mmSInt16_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUInt16(cJSON* j, const char* n, const mmUInt16_t* v, mmUInt16_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSInt32(cJSON* j, const char* n, const mmSInt32_t* v, mmSInt32_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUInt32(cJSON* j, const char* n, const mmUInt32_t* v, mmUInt32_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSInt64(cJSON* j, const char* n, const mmSInt64_t* v, mmSInt64_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUInt64(cJSON* j, const char* n, const mmUInt64_t* v, mmUInt64_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSize(cJSON* j, const char* n, const mmSize_t* v, mmSize_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeSIntptr(cJSON* j, const char* n, const mmSIntptr_t* v, mmSIntptr_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeUIntptr(cJSON* j, const char* n, const mmUIntptr_t* v, mmUIntptr_t d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeFloat(cJSON* j, const char* n, const float* v, float d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeDouble(cJSON* j, const char* n, const double* v, double d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddNumberToObject(j, n, (const double)(*v));
    }
}
void mmCJson_EncodeBool(cJSON* j, const char* n, const int* v, int d, int r)
{
    if (r || (*v) != d)
    {
        cJSON_AddBoolToObject(j, n, (const cJSON_bool)(*v));
    }
}
void mmCJson_EncodeString(cJSON* j, const char* n, const struct mmString* v, const char* d, int r)
{
    if (r || 0 != mmString_CompareCStr(v, d))
    {
        cJSON_AddStringToObject(j, n, (const char*)mmString_CStr(v));
    }
}
void mmCJson_EncodeEnumerate(cJSON* j, const char* n, const void* v, int d, int r, const void* f)
{
    int* m = (int*)v;
    if (r || (*m) != d)
    {
        typedef const char*(*mmJSONEncodeEnumerateFunc)(int);
        const char* w = (*((mmJSONEncodeEnumerateFunc)f))((*m));
        cJSON_AddStringToObject(j, n, w);
    }
}
void mmCJson_EncodeArray(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_AddArrayToObject(j, n);
    if (NULL != item)
    {
        size_t i = 0;
        void* u = NULL;
        size_t size = v->size;
        for (i = 0; i < size; ++i)
        {
            u = mmVectorValue_At(v, i);
            mmCJson_EncodeMember(item, i, u, f);
        }
    }
}
void mmCJson_EncodeArrays(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_AddArrayToObject(j, n);
    if (NULL != item)
    {
        size_t i = 0;
        for (i = 0; i < v->size; ++i)
        {
            cJSON* arr = cJSON_CreateArray();
            if (!cJSON_AddItemToArray(item, arr))
            {
                cJSON_Delete(arr);
            }
            else
            {
                const struct mmVectorValue* x = NULL;
                size_t idx = 0;
                void* val = NULL;
                x = (const struct mmVectorValue*)mmVectorValue_At(v, i);
                for (idx = 0; idx < x->size; ++idx)
                {
                    val = mmVectorValue_At(x, idx);
                    mmCJson_EncodeMember(arr, idx, val, f);
                }
            }
        }
    }
}

void mmCJson_EncodeNumberVector(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_AddArrayToObject(j, n);
    if (NULL != item)
    {
        size_t i = 0;
        for (i = 0; i < v->size; ++i)
        {
            cJSON* u = cJSON_CreateNumber(0);
            if (!cJSON_AddItemToArray(item, u))
            {
                cJSON_Delete(u);
            }
            else
            {
                double hValue = 0;
                char hBuffer[64] = { 0 };
                void* val = (void*)mmVectorValue_At(v, i);
                (*((mmTypeToAFunc)f))(val, hBuffer);
                mmValue_AToDouble(hBuffer, &hValue);
                cJSON_SetNumberValue(u, hValue);
            }
        }
    }
}

void mmCJson_EncodeStringVector(cJSON* j, const char* n, const struct mmVectorValue* v)
{
    cJSON* item = cJSON_AddArrayToObject(j, n);
    if (NULL != item)
    {
        size_t i = 0;
        for (i = 0; i < v->size; ++i)
        {
            cJSON* u = cJSON_CreateString("");
            if (!cJSON_AddItemToArray(item, u))
            {
                cJSON_Delete(u);
            }
            else
            {
                struct mmString* val = (struct mmString*)mmVectorValue_At(v, i);
                cJSON_SetValuestring(u, mmString_CStr(val));
            }
        }
    }
}

void mmCJson_DecodeMember(const cJSON* j, size_t n, void* v, const void* f)
{
    typedef void(*mmJSONDecodeFunc)(void* p, const cJSON* j);
    cJSON* item = cJSON_GetArrayItem(j, (int)n);
    if (cJSON_IsObject(item))
    {
        (*((mmJSONDecodeFunc)f))(v, item);
    }
}
void mmCJson_DecodeObject(const cJSON* j, const char* n, void* v, const void* f)
{
    typedef void(*mmJSONDecodeFunc)(void* p, const cJSON* j);
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsObject(item))
    {
        (*((mmJSONDecodeFunc)f))(v, item);
    }
}
void mmCJson_DecodeSChar(const cJSON* j, const char* n, mmSChar_t* v, mmSChar_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSChar_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUChar(const cJSON* j, const char* n, mmUChar_t* v, mmUChar_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUChar_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSShort(const cJSON* j, const char* n, mmSShort_t* v, mmSShort_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSShort_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUShort(const cJSON* j, const char* n, mmUShort_t* v, mmUShort_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUShort_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSInt(const cJSON* j, const char* n, mmSInt_t* v, mmSInt_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSInt_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUInt(const cJSON* j, const char* n, mmUInt_t* v, mmUInt_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUInt_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSLong(const cJSON* j, const char* n, mmSLong_t* v, mmSLong_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSLong_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeULong(const cJSON* j, const char* n, mmULong_t* v, mmULong_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmULong_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSLLong(const cJSON* j, const char* n, mmSLLong_t* v, mmSLLong_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSLLong_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeULLong(const cJSON* j, const char* n, mmULLong_t* v, mmULLong_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmULLong_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSInt8(const cJSON* j, const char* n, mmSInt8_t* v, mmSInt8_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSInt8_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUInt8(const cJSON* j, const char* n, mmUInt8_t* v, mmUInt8_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUInt8_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSInt16(const cJSON* j, const char* n, mmSInt16_t* v, mmSInt16_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSInt16_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUInt16(const cJSON* j, const char* n, mmUInt16_t* v, mmUInt16_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUInt16_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSInt32(const cJSON* j, const char* n, mmSInt32_t* v, mmSInt32_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSInt32_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUInt32(const cJSON* j, const char* n, mmUInt32_t* v, mmUInt32_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUInt32_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSInt64(const cJSON* j, const char* n, mmSInt64_t* v, mmSInt64_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSInt64_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUInt64(const cJSON* j, const char* n, mmUInt64_t* v, mmUInt64_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUInt64_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSize(const cJSON* j, const char* n, mmSize_t* v, mmSize_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSize_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeSIntptr(const cJSON* j, const char* n, mmSIntptr_t* v, mmSIntptr_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmSIntptr_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeUIntptr(const cJSON* j, const char* n, mmUIntptr_t* v, mmUIntptr_t d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (mmUIntptr_t)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeFloat(const cJSON* j, const char* n, float* v, float d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (float)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeDouble(const cJSON* j, const char* n, double* v, double d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsNumber(item))
    {
        (*v) = (double)cJSON_GetNumberValue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeBool(const cJSON* j, const char* n, int* v, int d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsBool(item))
    {
        (*v) = (int)cJSON_IsTrue(item);
    }
    else
    {
        (*v) = d;
    }
}
void mmCJson_DecodeString(const cJSON* j, const char* n, struct mmString* v, const char* d, int r)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsString(item))
    {
        mmString_Assigns(v, cJSON_GetStringValue(item));
    }
    else
    {
        mmString_Assigns(v, d);
    }
}
void mmCJson_DecodeEnumerate(const cJSON* j, const char* n, void* v, int d, int r, const void* f)
{
    int* m = (int*)v;
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsString(item))
    {
        typedef int(*mmJSONDecodeEnumerateFunc)(const char*);
        const char* w = cJSON_GetStringValue(item);
        (*m) = (*((mmJSONDecodeEnumerateFunc)f))(w);
    }
    else
    {
        (*m) = d;
    }
}
void mmCJson_DecodeArray(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsArray(item))
    {
        size_t i = 0;
        void* u = NULL;
        size_t size = cJSON_GetArraySize(item);
        mmVectorValue_AllocatorMemory(v, size);
        for (i = 0; i < size; ++i)
        {
            u = mmVectorValue_At(v, i);
            mmCJson_DecodeMember(item, i, u, f);
        }
    }
}
void mmCJson_DecodeArrays(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsArray(item))
    {
        size_t i = 0;
        size_t size = cJSON_GetArraySize(item);
        mmVectorValue_AllocatorMemory(v, size);
        for (i = 0; i < size; ++i)
        {
            cJSON* arr = cJSON_GetArrayItem(item, (int)i);
            if (NULL != arr)
            {
                struct mmVectorValue* x = NULL;
                size_t idx = 0;
                void* val = NULL;
                size_t z = cJSON_GetArraySize(arr);
                x = (struct mmVectorValue*)mmVectorValue_At(v, i);
                mmVectorValue_AllocatorMemory(x, z);
                for (idx = 0; idx < z; ++idx)
                {
                    val = mmVectorValue_At(x, idx);
                    mmCJson_DecodeMember(arr, idx, val, f);
                }
            }
        }
    }
}

void mmCJson_DecodeNumberVector(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsArray(item))
    {
        size_t i = 0;
        size_t size = cJSON_GetArraySize(item);
        size_t esz = mmVectorValue_GetElement(v);
        mmVectorValue_AllocatorMemory(v, size);
        for (i = 0; i < size; ++i)
        {
            void* val = (void*)mmVectorValue_At(v, i);
            cJSON* u = cJSON_GetArrayItem(item, (int)i);
            if (cJSON_IsNumber(u))
            {
                char hBuffer[64] = { 0 };
                double hValue = cJSON_GetNumberValue(u);
                mmValue_DoubleToA(&hValue, hBuffer);
                (*((mmAToTypeFunc)f))(hBuffer, val);
            }
            else
            {
                mmMemset(val, 0, esz);
            }
        }
    }
}

void mmCJson_DecodeStringVector(const cJSON* j, const char* n, struct mmVectorValue* v)
{
    cJSON* item = cJSON_GetObjectItem(j, n);
    if (cJSON_IsArray(item))
    {
        size_t i = 0;
        size_t size = cJSON_GetArraySize(item);
        mmVectorValue_AllocatorMemory(v, size);
        for (i = 0; i < size; ++i)
        {
            struct mmString* val = (struct mmString*)mmVectorValue_At(v, i);
            cJSON* u = cJSON_GetArrayItem(item, (int)i);
            if (cJSON_IsString(u))
            {
                mmString_Assigns(val, cJSON_GetStringValue(u));
            }
            else
            {
                mmString_Assigns(val, "");
            }
        }
    }
}