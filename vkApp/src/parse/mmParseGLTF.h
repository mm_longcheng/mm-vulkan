/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmParseGLTF_h__
#define __mmParseGLTF_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "math/mmRange.h"

#include "container/mmVectorValue.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

// reference
// https://github.com/jkuhlmann/cgltf
// https://github.com/KhronosGroup/glTF
// 
// cgltf is good, but 
// 1. We modify jsmn and we need parse metadata more standard.
// 2. We not need decode the buffer immediately.

enum mmGLTFResult_t
{
    mmGLTFResultSuccess           =  0,
    mmGLTFResultUnknown           = -1,
    mmGLTFResultUnknownFormat     = -2,
    mmGLTFResultDataTooShort      = -3,
    mmGLTFResultInvalidJson       = -4,
    mmGLTFResultInvalidGltf       = -5,
    mmGLTFResultInvalidOptions    = -6,
    mmGLTFResultFileNotFound      = -7,
    mmGLTFResultIOError           = -8,
    mmGLTFResultOutOfMemory       = -9,
    mmGLTFResultLegacyGltf        = -10,
    mmGLTFResultNotSupportFormat  = -11,
    mmGLTFResultNotSupportFeature = -12,
};

enum mmGLTFFile_t
{
    mmGLTFFileTypeInvalid , // Invalid
    mmGLTFFileTypeGltf    , // gltf
    mmGLTFFileTypeGlb     , // glb
};

enum mmGLTFBufferView_t
{
    mmGLTFBufferViewTypeInvalid             =     0, //       Invalid
    mmGLTFBufferViewTypeArrayBuffer         = 34962, // 34962 ARRAY_BUFFER         vertices
    mmGLTFBufferViewTypeElementArrayBuffer  = 34963, // 34963 ELEMENT_ARRAY_BUFFER indices
};
int mmParseJSON_DecodeGLTFBufferViewType(struct mmParseJSON* p, int i,
    enum mmGLTFBufferView_t* v,
    enum mmGLTFBufferView_t d);

enum mmGLTFAttribute_t
{
    mmGLTFAttributeTypeInvalid  , //  Invalid
    mmGLTFAttributeTypePosition , // "POSITION"
    mmGLTFAttributeTypeNormal   , // "NORMAL"
    mmGLTFAttributeTypeTangent  , // "TANGENT"
    mmGLTFAttributeTypeTexcoord , // "TEXCOORD"
    mmGLTFAttributeTypeColor    , // "COLOR"
    mmGLTFAttributeTypeJoints   , // "JOINTS"
    mmGLTFAttributeTypeWeights  , // "WEIGHTS"
};
void mmValue_StringToGLTFAttributeType(const char* _value, enum mmGLTFAttribute_t* val);

enum mmGLTFComponent_t
{
    mmGLTFComponentTypeInvalid        =    0,  //          Invalid
    mmGLTFComponentTypeByte           = 5120,  //  = 5120, BYTE
    mmGLTFComponentTypeUnsignedByte   = 5121,  //  = 5121, UNSIGNED_BYTE
    mmGLTFComponentTypeShort          = 5122,  //  = 5122, SHORT
    mmGLTFComponentTypeUnsignedShort  = 5123,  //  = 5123, UNSIGNED_SHORT
    mmGLTFComponentTypeInt            = 5124,  //  = 5124, INT
    mmGLTFComponentTypeUnsignedInt    = 5125,  //  = 5125, UNSIGNED_INT
    mmGLTFComponentTypeFloat          = 5126,  //  = 5126, FLOAT
    mmGLTFComponentTypeDouble         = 5130,  //  = 5130, DOUBLE
};
int mmParseJSON_DecodeGLTFComponentType(struct mmParseJSON* p, int i,
    enum mmGLTFComponent_t* v,
    enum mmGLTFComponent_t d);

int32_t mmGLTFGetComponentSizeInBytes(enum mmGLTFComponent_t componentType);

enum mmGLTFAccessor_t
{
    mmGLTFTypeInvalid , //  Invalid
    mmGLTFTypeScalar  , // "SCALAR"
    mmGLTFTypeVec2    , // "VEC2"
    mmGLTFTypeVec3    , // "VEC3"
    mmGLTFTypeVec4    , // "VEC4"
    mmGLTFTypeMat2    , // "MAT2"
    mmGLTFTypeMat3    , // "MAT3"
    mmGLTFTypeMat4    , // "MAT4"
};
void mmValue_StringToGLTFAccessorType(const char* _value, enum mmGLTFAccessor_t* val);
int mmParseJSON_DecodeGLTFAccessorType(struct mmParseJSON* p, int i,
    enum mmGLTFAccessor_t* v,
    enum mmGLTFAccessor_t d);

int32_t mmGLTFGetNumComponentsInType(enum mmGLTFAccessor_t ty);

enum mmGLTFPrimitive_t
{
    mmGLTFPrimitiveTypePoints         = 0, // 0 POINTS
    mmGLTFPrimitiveTypeLines          = 1, // 1 LINES
    mmGLTFPrimitiveTypeLineLoop       = 2, // 2 LINE_LOOP
    mmGLTFPrimitiveTypeLineStrip      = 3, // 3 LINE_STRIP
    mmGLTFPrimitiveTypeTriangles      = 4, // 4 TRIANGLES
    mmGLTFPrimitiveTypeTriangleStrip  = 5, // 5 TRIANGLE_STRIP
    mmGLTFPrimitiveTypeTriangleFan    = 6, // 6 TRIANGLE_FAN
};
int mmParseJSON_DecodeGLTFPrimitiveType(struct mmParseJSON* p, int i,
    enum mmGLTFPrimitive_t* v,
    enum mmGLTFPrimitive_t d);

enum mmGLTFAlphaMode_t
{
    mmGLTFAlphaModeOpaque  = 0, // "OPAQUE"
    mmGLTFAlphaModeMask    = 1, // "MASK"
    mmGLTFAlphaModeBlend   = 2, // "BLEND"
};
void mmValue_StringToGLTFAlphaModeType(const char* _value, enum mmGLTFAlphaMode_t* val);
int mmParseJSON_DecodeGLTFAlphaModeType(struct mmParseJSON* p, int i,
    enum mmGLTFAlphaMode_t* v,
    enum mmGLTFAlphaMode_t d);

enum mmGLTFAnimationPath_t
{
    mmGLTFAnimationPathTypeInvalid     , //  Invalid
    mmGLTFAnimationPathTypeTranslation , // "translation"
    mmGLTFAnimationPathTypeRotation    , // "rotation"
    mmGLTFAnimationPathTypeScale       , // "scale"
    mmGLTFAnimationPathTypeWeights     , // "weights"
};
void mmValue_StringToGLTFAnimationPathType(const char* _value, enum mmGLTFAnimationPath_t* val);
int mmParseJSON_DecodeGLTFAnimationPathType(struct mmParseJSON* p, int i,
    enum mmGLTFAnimationPath_t* v,
    enum mmGLTFAnimationPath_t d);

enum mmGLTFInterpolation_t
{
    mmGLTFInterpolationTypeLinear       , // "LINEAR"
    mmGLTFInterpolationTypeStep         , // "STEP"
    mmGLTFInterpolationTypeCubicSpline  , // "CUBICSPLINE"
};
void mmValue_StringToGLTFInterpolationType(const char* _value, enum mmGLTFInterpolation_t* val);
int mmParseJSON_DecodeGLTFInterpolationType(struct mmParseJSON* p, int i,
    enum mmGLTFInterpolation_t* v,
    enum mmGLTFInterpolation_t d);

enum mmGLTFCamera_t
{
    mmGLTFCameraTypeInvalid      , //  Invalid
    mmGLTFCameraTypePerspective  , // "perspective"
    mmGLTFCameraTypeOrthographic , // "orthographic"
};
void mmValue_StringToGLTFCameraType(const char* _value, enum mmGLTFCamera_t* val);
int mmParseJSON_DecodeGLTFCameraType(struct mmParseJSON* p, int i,
    enum mmGLTFCamera_t* v,
    enum mmGLTFCamera_t d);

enum mmGLTFSamplerFilterMode_t
{
    mmSamplerFilterModeNearest              = 9728,
    mmSamplerFilterModeLinear               = 9729,
    mmSamplerFilterModeNearestMipmapNearest = 9984,
    mmSamplerFilterModeLinearMipmapNearest  = 9985,
    mmSamplerFilterModeNearestMipmapLinear  = 9986,
    mmSamplerFilterModeLinearMipmapLinear   = 9987,
};
int mmParseJSON_DecodeGLTFSamplerFilterModeType(struct mmParseJSON* p, int i,
    enum mmGLTFSamplerFilterMode_t* v,
    enum mmGLTFSamplerFilterMode_t d);

enum mmGLTFSamplerAddressMode_t
{
    mmSamplerAddressModeRepeat              = 10497,
    mmSamplerAddressModeClampToEdge         = 33071,
    mmSamplerAddressModeMirroredRepeat      = 33648,
};
int mmParseJSON_DecodeGLTFSamplerAddressModeType(struct mmParseJSON* p, int i,
    enum mmGLTFSamplerAddressMode_t* v,
    enum mmGLTFSamplerAddressMode_t d);

enum mmGLTFLight_t
{
    mmGLTFLightTypeInvalid      = 0, //  Invalid
    mmGLTFLightTypeDirectional  = 1, // "directional"
    mmGLTFLightTypePoint        = 2, // "point"
    mmGLTFLightTypeSpot         = 3, // "spot"
};
void mmValue_StringToGLTFLightType(const char* _value, enum mmGLTFLight_t* val);
int mmParseJSON_DecodeGLTFLightType(struct mmParseJSON* p, int i,
    enum mmGLTFLight_t* v,
    enum mmGLTFLight_t d);

enum mmGLTFMeshoptCompressionMode_t
{
    mmGLTFMeshoptCompressionModeInvalid    , //  Invalid
    mmGLTFMeshoptCompressionModeAttributes , // "ATTRIBUTES"
    mmGLTFMeshoptCompressionModeTriangles  , // "TRIANGLES"
    mmGLTFMeshoptCompressionModeIndices    , // "INDICES"
};
void mmValue_StringToGLTFMeshoptCompressionModeType(const char* _value, enum mmGLTFMeshoptCompressionMode_t* val);
int mmParseJSON_DecodeGLTFMeshoptCompressionModeType(struct mmParseJSON* p, int i, 
    enum mmGLTFMeshoptCompressionMode_t* v, 
    enum mmGLTFMeshoptCompressionMode_t d);

enum mmGLTFMeshoptCompressionFilter_t
{
    mmGLTFMeshoptCompressionFilterNone        , // "NONE"
    mmGLTFMeshoptCompressionFilterOctahedral  , // "OCTAHEDRAL"
    mmGLTFMeshoptCompressionFilterQuaternion  , // "QUATERNION"
    mmGLTFMeshoptCompressionFilterExponential , // "EXPONENTIAL"
};
void mmValue_StringToGLTFMeshoptCompressionFilterType(const char* _value, enum mmGLTFMeshoptCompressionFilter_t* val);
int mmParseJSON_DecodeGLTFMeshoptCompressionFilterType(struct mmParseJSON* p, int i, 
    enum mmGLTFMeshoptCompressionFilter_t* v, 
    enum mmGLTFMeshoptCompressionFilter_t d);

void mmGLTFRanges_Init(struct mmVectorValue* p);
void mmGLTFRanges_Destroy(struct mmVectorValue* p);

struct mmGLTFUriData
{
    struct mmByteBuffer bytes;
    int embedded;
};

void mmGLTFUriData_Init(struct mmGLTFUriData* p);
void mmGLTFUriData_Destroy(struct mmGLTFUriData* p);

struct mmGLTFExtras
{
    struct mmRange data;
    int i;
};
void mmGLTFExtras_Init(struct mmGLTFExtras* p);
void mmGLTFExtras_Destroy(struct mmGLTFExtras* p);
void mmGLTFExtras_Reset(struct mmGLTFExtras* p);

int mmParseJSON_DecodeGLTFExtras(struct mmParseJSON* p, int i, struct mmGLTFExtras* v);

struct mmGLTFExtension
{
    struct mmRange name;
    int i;
};
void mmGLTFExtension_Init(struct mmGLTFExtension* p);
void mmGLTFExtension_Destroy(struct mmGLTFExtension* p);

int mmParseJSON_DecodeGLTFExtension(struct mmParseJSON* p, int i, mmUInt32_t m, void* o);

void mmGLTFExtensions_Init(struct mmVectorValue* p);
void mmGLTFExtensions_Destroy(struct mmVectorValue* p);

struct mmGLTFBuffer
{
    struct mmRange name;
    struct mmRange uri;
    size_t byteLength;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    struct mmGLTFUriData data;
};
void mmGLTFBuffer_Init(struct mmGLTFBuffer* p);
void mmGLTFBuffer_Destroy(struct mmGLTFBuffer* p);

int mmParseJSON_DecodeGLTFBuffer(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMeshoptCompression
{
    int buffer;
    size_t byteOffset;
    size_t byteLength;
    size_t byteStride;
    size_t count;
    enum mmGLTFMeshoptCompressionMode_t mode;
    enum mmGLTFMeshoptCompressionFilter_t filter;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMeshoptCompression_Init(struct mmGLTFMeshoptCompression* p);
void mmGLTFMeshoptCompression_Destroy(struct mmGLTFMeshoptCompression* p);

int mmParseJSON_DecodeGLTFMeshoptCompression(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFBufferView
{
    struct mmRange name;
    int buffer;
    size_t byteOffset;
    size_t byteLength;
    size_t byteStride;
    enum mmGLTFBufferView_t target;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
    
    int has_meshoptCompression;
    struct mmGLTFMeshoptCompression ext_meshoptCompression;
};
void mmGLTFBufferView_Init(struct mmGLTFBufferView* p);
void mmGLTFBufferView_Destroy(struct mmGLTFBufferView* p);

int mmParseJSON_DecodeGLTFBufferViewExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFBufferView(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAccessorSparseIndices
{
    int bufferView;
    size_t byteOffset;
    enum mmGLTFComponent_t componentType;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAccessorSparseIndices_Init(struct mmGLTFAccessorSparseIndices* p);
void mmGLTFAccessorSparseIndices_Destroy(struct mmGLTFAccessorSparseIndices* p);

int mmParseJSON_DecodeGLTFAccessorSparseIndices(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAccessorSparseValues
{
    int bufferView;
    size_t byteOffset;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAccessorSparseValues_Init(struct mmGLTFAccessorSparseValues* p);
void mmGLTFAccessorSparseValues_Destroy(struct mmGLTFAccessorSparseValues* p);

int mmParseJSON_DecodeGLTFAccessorSparseValues(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAccessorSparse
{
    size_t count;
    struct mmGLTFAccessorSparseIndices indices;
    struct mmGLTFAccessorSparseValues values;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAccessorSparse_Init(struct mmGLTFAccessorSparse* p);
void mmGLTFAccessorSparse_Destroy(struct mmGLTFAccessorSparse* p);

int mmParseJSON_DecodeGLTFAccessorSparse(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAccessor
{
    struct mmRange name;
    int bufferView;
    size_t byteOffset;
    enum mmGLTFComponent_t componentType;
    mmBool_t normalized;
    size_t count;
    enum mmGLTFAccessor_t type;
    float min[16];
    float max[16];
    struct mmGLTFAccessorSparse sparse;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int minSize;
    int maxSize;
    mmBool_t isSparse;
};
void mmGLTFAccessor_Init(struct mmGLTFAccessor* p);
void mmGLTFAccessor_Destroy(struct mmGLTFAccessor* p);

int mmParseJSON_DecodeGLTFAccessor(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

int mmGLTFAccessor_ByteStride(const struct mmGLTFAccessor* p, struct mmGLTFBufferView* bufferView);

struct mmGLTFAttribute
{
    struct mmRange name;
    enum mmGLTFAttribute_t type;
    int index;
    int accessor;
};
void mmGLTFAttribute_Init(struct mmGLTFAttribute* p);
void mmGLTFAttribute_Destroy(struct mmGLTFAttribute* p);

void mmParseJSON_DecodeGLTFAttributeType(struct mmParseJSON* p, struct mmGLTFAttribute* v);

int mmParseJSON_DecodeGLTFAttribute(struct mmParseJSON* p, int i, mmUInt32_t m, void* o);

// attributes is a array.
void mmGLTFAttributes_Init(struct mmVectorValue* p);
void mmGLTFAttributes_Destroy(struct mmVectorValue* p);

int mmGLTFAttributes_GetAccessor(struct mmParseJSON* p, struct mmVectorValue* v, const char* name);
int mmGLTFAttributes_GetAccessorSister(struct mmVectorValue* l, struct mmVectorValue* r, size_t idx);

struct mmGLTFImage
{
    struct mmRange name;
    struct mmRange uri;
    struct mmRange mimeType;
    int bufferView;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    struct mmGLTFUriData data;
};
void mmGLTFImage_Init(struct mmGLTFImage* p);
void mmGLTFImage_Destroy(struct mmGLTFImage* p);

int mmParseJSON_DecodeGLTFImage(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFSampler
{
    struct mmRange name;
    enum mmGLTFSamplerFilterMode_t magFilter;
    enum mmGLTFSamplerFilterMode_t minFilter;
    enum mmGLTFSamplerAddressMode_t wrapS;
    enum mmGLTFSamplerAddressMode_t wrapT;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFSampler_Init(struct mmGLTFSampler* p);
void mmGLTFSampler_Destroy(struct mmGLTFSampler* p);

int mmParseJSON_DecodeGLTFSampler(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFTextureBasisu
{
    int source;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFTextureBasisu_Init(struct mmGLTFTextureBasisu* p);
void mmGLTFTextureBasisu_Destroy(struct mmGLTFTextureBasisu* p);

int mmParseJSON_DecodeGLTFTextureBasisu(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFTexture
{
    struct mmRange name;
    int sampler;
    int source;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_basisu;
    struct mmGLTFTextureBasisu ext_basisu;
};
void mmGLTFTexture_Init(struct mmGLTFTexture* p);
void mmGLTFTexture_Destroy(struct mmGLTFTexture* p);

int mmParseJSON_DecodeGLTFTextureExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFTexture(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

int mmGLTFTexture_GetSource(struct mmGLTFTexture* p);

struct mmGLTFTextureTransform
{
    float offset[2];
    float rotation;
    float scale[2];
    int texCoord;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFTextureTransform_Init(struct mmGLTFTextureTransform* p);
void mmGLTFTextureTransform_Destroy(struct mmGLTFTextureTransform* p);

void mmGLTFTextureTransform_MakeMat3x3(const struct mmGLTFTextureTransform* p, float m[3][3]);

int mmParseJSON_DecodeGLTFTextureTransform(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFTextureInfo
{
    int index;
    int texCoord;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_transform;
    struct mmGLTFTextureTransform ext_transform;
};
void mmGLTFTextureInfo_Init(struct mmGLTFTextureInfo* p);
void mmGLTFTextureInfo_Destroy(struct mmGLTFTextureInfo* p);

int mmParseJSON_DecodeGLTFTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFNormalTextureInfo
{
    int index;
    int texCoord;
    float scale;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_transform;
    struct mmGLTFTextureTransform ext_transform;
};
void mmGLTFNormalTextureInfo_Init(struct mmGLTFNormalTextureInfo* p);
void mmGLTFNormalTextureInfo_Destroy(struct mmGLTFNormalTextureInfo* p);

int mmParseJSON_DecodeGLTFNormalTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFNormalTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFOcclusionTextureInfo
{
    int index;
    int texCoord;
    float strength;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_transform;
    struct mmGLTFTextureTransform ext_transform;
};
void mmGLTFOcclusionTextureInfo_Init(struct mmGLTFOcclusionTextureInfo* p);
void mmGLTFOcclusionTextureInfo_Destroy(struct mmGLTFOcclusionTextureInfo* p);

int mmParseJSON_DecodeGLTFOcclusionTextureInfoExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFOcclusionTextureInfo(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsPbrMetallicRoughness
{
    struct mmGLTFTextureInfo baseColorTexture;
    struct mmGLTFTextureInfo metallicRoughnessTexture;
    float baseColorFactor[4];
    float metallicFactor;
    float roughnessFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsPbrMetallicRoughness_Init(struct mmGLTFMaterialsPbrMetallicRoughness* p);
void mmGLTFMaterialsPbrMetallicRoughness_Destroy(struct mmGLTFMaterialsPbrMetallicRoughness* p);

int mmParseJSON_DecodeGLTFMaterialsPbrMetallicRoughness(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsPbrSpecularGlossiness
{
    struct mmGLTFTextureInfo diffuseTexture;
    struct mmGLTFTextureInfo specularGlossinessTexture;
    float diffuseFactor[4];
    float specularFactor[3];
    float glossinessFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsPbrSpecularGlossiness_Init(struct mmGLTFMaterialsPbrSpecularGlossiness* p);
void mmGLTFMaterialsPbrSpecularGlossiness_Destroy(struct mmGLTFMaterialsPbrSpecularGlossiness* p);

int mmParseJSON_DecodeGLTFMaterialsPbrSpecularGlossiness(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsClearcoat
{
    struct mmGLTFTextureInfo clearcoatTexture;
    struct mmGLTFTextureInfo clearcoatRoughnessTexture;
    struct mmGLTFNormalTextureInfo clearcoatNormalTexture;
    float clearcoatFactor;
    float clearcoatRoughnessFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsClearcoat_Init(struct mmGLTFMaterialsClearcoat* p);
void mmGLTFMaterialsClearcoat_Destroy(struct mmGLTFMaterialsClearcoat* p);

int mmParseJSON_DecodeGLTFMaterialsClearcoat(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsTransmission
{
    struct mmGLTFTextureInfo transmissionTexture;
    float transmissionFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsTransmission_Init(struct mmGLTFMaterialsTransmission* p);
void mmGLTFMaterialsTransmission_Destroy(struct mmGLTFMaterialsTransmission* p);

int mmParseJSON_DecodeGLTFMaterialsTransmission(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsIor
{
    float ior;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsIor_Init(struct mmGLTFMaterialsIor* p);
void mmGLTFMaterialsIor_Destroy(struct mmGLTFMaterialsIor* p);

int mmParseJSON_DecodeGLTFMaterialsIor(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsSpecular
{
    struct mmGLTFTextureInfo specularTexture;
    struct mmGLTFTextureInfo specularColorTexture;
    float specularColorFactor[3];
    float specularFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsSpecular_Init(struct mmGLTFMaterialsSpecular* p);
void mmGLTFMaterialsSpecular_Destroy(struct mmGLTFMaterialsSpecular* p);

int mmParseJSON_DecodeGLTFMaterialsSpecular(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsVolume
{
    struct mmGLTFTextureInfo thicknessTexture;
    float thicknessFactor;
    float attenuationColor[3];
    float attenuationDistance;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsVolume_Init(struct mmGLTFMaterialsVolume* p);
void mmGLTFMaterialsVolume_Destroy(struct mmGLTFMaterialsVolume* p);

int mmParseJSON_DecodeGLTFMaterialsVolume(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsSheen
{
    struct mmGLTFTextureInfo sheenColorTexture;
    struct mmGLTFTextureInfo sheenRoughnessTexture;
    float sheenColorFactor[3];
    float sheenRoughnessFactor;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsSheen_Init(struct mmGLTFMaterialsSheen* p);
void mmGLTFMaterialsSheen_Destroy(struct mmGLTFMaterialsSheen* p);

int mmParseJSON_DecodeGLTFMaterialsSheen(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsVariantsVariant
{
    struct mmRange name;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsVariantsVariant_Init(struct mmGLTFMaterialsVariantsVariant* p);
void mmGLTFMaterialsVariantsVariant_Destroy(struct mmGLTFMaterialsVariantsVariant* p);

int mmParseJSON_DecodeGLTFMaterialsVariantsVariant(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFMaterialsVariantsMappingVariants_Init(struct mmVectorValue* p);
void mmGLTFMaterialsVariantsMappingVariants_Destroy(struct mmVectorValue* p);

struct mmGLTFMaterialsVariantsMapping
{
    int material;
    struct mmVectorValue variants;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsVariantsMapping_Init(struct mmGLTFMaterialsVariantsMapping* p);
void mmGLTFMaterialsVariantsMapping_Destroy(struct mmGLTFMaterialsVariantsMapping* p);

int mmParseJSON_DecodeGLTFMaterialsVariantsMapping(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFMaterialsVariantsVariants_Init(struct mmVectorValue* p);
void mmGLTFMaterialsVariantsVariants_Destroy(struct mmVectorValue* p);

void mmGLTFMaterialsVariantsMappings_Init(struct mmVectorValue* p);
void mmGLTFMaterialsVariantsMappings_Destroy(struct mmVectorValue* p);

struct mmGLTFMaterialsUnlit
{
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsUnlit_Init(struct mmGLTFMaterialsUnlit* p);
void mmGLTFMaterialsUnlit_Destroy(struct mmGLTFMaterialsUnlit* p);

int mmParseJSON_DecodeGLTFMaterialsUnlit(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsVariants
{
    struct mmVectorValue variants;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsVariants_Init(struct mmGLTFMaterialsVariants* p);
void mmGLTFMaterialsVariants_Destroy(struct mmGLTFMaterialsVariants* p);

int mmParseJSON_DecodeGLTFMaterialsVariants(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsEmissiveStrength
{
    float emissiveStrength;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsEmissiveStrength_Init(struct mmGLTFMaterialsEmissiveStrength* p);
void mmGLTFMaterialsEmissiveStrength_Destroy(struct mmGLTFMaterialsEmissiveStrength* p);

int mmParseJSON_DecodeGLTFMaterialsEmissiveStrength(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterialsIridescence
{
    float iridescenceFactor;
    struct mmGLTFTextureInfo iridescenceTexture;
    float iridescenceIor;
    float iridescenceThicknessMinimum;
    float iridescenceThicknessMaximum;
    struct mmGLTFTextureInfo iridescenceThicknessTexture;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMaterialsIridescence_Init(struct mmGLTFMaterialsIridescence* p);
void mmGLTFMaterialsIridescence_Destroy(struct mmGLTFMaterialsIridescence* p);

int mmParseJSON_DecodeGLTFMaterialsIridescence(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFPrimitiveVariants
{
    struct mmVectorValue mappings;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFPrimitiveVariants_Init(struct mmGLTFPrimitiveVariants* p);
void mmGLTFPrimitiveVariants_Destroy(struct mmGLTFPrimitiveVariants* p);

int mmParseJSON_DecodeGLTFPrimitiveVariants(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMaterial
{
    struct mmRange name;
    struct mmGLTFMaterialsPbrMetallicRoughness pbrMetallicRoughness;
    struct mmGLTFNormalTextureInfo normalTexture;
    struct mmGLTFOcclusionTextureInfo occlusionTexture;
    struct mmGLTFTextureInfo emissiveTexture;
    float emissiveFactor[3];
    enum mmGLTFAlphaMode_t alphaMode;
    float alphaCutoff;
    mmBool_t doubleSided;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_clearcoat;
    int has_ior;
    int has_pbrSpecularGlossiness;
    int has_sheen;
    int has_specular;
    int has_transmission;
    int has_unlit;
    int has_volume;
    int has_emissiveStrength;
    int has_iridescence;
    struct mmGLTFMaterialsClearcoat ext_clearcoat;
    struct mmGLTFMaterialsIor ext_ior;
    struct mmGLTFMaterialsPbrSpecularGlossiness ext_pbrSpecularGlossiness;
    struct mmGLTFMaterialsSheen ext_sheen;
    struct mmGLTFMaterialsSpecular ext_specular;
    struct mmGLTFMaterialsTransmission ext_transmission;
    struct mmGLTFMaterialsUnlit ext_unlit;
    struct mmGLTFMaterialsVolume ext_volume;
    struct mmGLTFMaterialsEmissiveStrength ext_emissiveStrength;
    struct mmGLTFMaterialsIridescence ext_iridescence;
};

void mmGLTFMaterial_Init(struct mmGLTFMaterial* p);
void mmGLTFMaterial_Destroy(struct mmGLTFMaterial* p);

int mmParseJSON_DecodeGLTFMaterialExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFMaterial(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFDracoMeshCompression
{
    int bufferView;
    struct mmVectorValue attributes;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFDracoMeshCompression_Init(struct mmGLTFDracoMeshCompression* p);
void mmGLTFDracoMeshCompression_Destroy(struct mmGLTFDracoMeshCompression* p);

int mmParseJSON_DecodeGLTFDracoMeshCompression(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFPrimitiveTargets_Init(struct mmVectorValue* p);
void mmGLTFPrimitiveTargets_Destroy(struct mmVectorValue* p);

struct mmGLTFPrimitive
{
    struct mmVectorValue attributes;
    struct mmVectorValue targets;
    int indices;
    int material;
    int mode;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_draco;
    int has_variants;
    struct mmGLTFDracoMeshCompression ext_draco;
    struct mmGLTFPrimitiveVariants ext_variants;
};
void mmGLTFPrimitive_Init(struct mmGLTFPrimitive* p);
void mmGLTFPrimitive_Destroy(struct mmGLTFPrimitive* p);

int mmParseJSON_DecodeGLTFPrimitiveExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFPrimitive(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFMeshPrimitives_Init(struct mmVectorValue* p);
void mmGLTFMeshPrimitives_Destroy(struct mmVectorValue* p);

void mmGLTFMeshWeights_Init(struct mmVectorValue* p);
void mmGLTFMeshWeights_Destroy(struct mmVectorValue* p);

void mmGLTFMeshExtrasTargetNames_Init(struct mmVectorValue* p);
void mmGLTFMeshExtrasTargetNames_Destroy(struct mmVectorValue* p);

struct mmGLTFMeshExtras
{
    struct mmVectorValue targetNames;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFMeshExtras_Init(struct mmGLTFMeshExtras* p);
void mmGLTFMeshExtras_Destroy(struct mmGLTFMeshExtras* p);
int mmParseJSON_DecodeGLTFMeshExtras(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFMesh
{
    struct mmRange name;
    struct mmVectorValue primitives;
    struct mmVectorValue weights;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_extras;
    struct mmGLTFMeshExtras ext_extras;
};
void mmGLTFMesh_Init(struct mmGLTFMesh* p);
void mmGLTFMesh_Destroy(struct mmGLTFMesh* p);

int mmParseJSON_DecodeGLTFMesh(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFSkinJoints_Init(struct mmVectorValue* p);
void mmGLTFSkinJoints_Destroy(struct mmVectorValue* p);

struct mmGLTFSkin
{
    struct mmRange name;
    struct mmVectorValue joints;
    int skeleton;
    int inverseBindMatrices;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFSkin_Init(struct mmGLTFSkin* p);
void mmGLTFSkin_Destroy(struct mmGLTFSkin* p);

int mmParseJSON_DecodeGLTFSkin(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFCameraPerspective
{
    float aspectRatio;
    float yfov;
    float zfar;
    float znear;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFCameraPerspective_Init(struct mmGLTFCameraPerspective* p);
void mmGLTFCameraPerspective_Destroy(struct mmGLTFCameraPerspective* p);

int mmParseJSON_DecodeGLTFCameraPerspective(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFCameraOrthographic
{
    float xmag;
    float ymag;
    float zfar;
    float znear;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFCameraOrthographic_Init(struct mmGLTFCameraOrthographic* p);
void mmGLTFCameraOrthographic_Destroy(struct mmGLTFCameraOrthographic* p);

int mmParseJSON_DecodeGLTFCameraOrthographic(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFCamera
{
    struct mmRange name;
    enum mmGLTFCamera_t type;
    struct mmGLTFCameraPerspective perspective;
    struct mmGLTFCameraOrthographic orthographic;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFCamera_Init(struct mmGLTFCamera* p);
void mmGLTFCamera_Destroy(struct mmGLTFCamera* p);

int mmParseJSON_DecodeGLTFCamera(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFLightSpot
{
    float innerConeAngle;
    float outerConeAngle;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFLightSpot_Init(struct mmGLTFLightSpot* p);
void mmGLTFLightSpot_Destroy(struct mmGLTFLightSpot* p);

int mmParseJSON_DecodeGLTFLightSpot(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFLight
{
    struct mmRange name;
    float color[3];
    float intensity;
    enum mmGLTFLight_t type;
    float range;
    struct mmGLTFLightSpot spot;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFLight_Init(struct mmGLTFLight* p);
void mmGLTFLight_Destroy(struct mmGLTFLight* p);

int mmParseJSON_DecodeGLTFLight(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFLights_Init(struct mmVectorValue* p);
void mmGLTFLights_Destroy(struct mmVectorValue* p);

struct mmGLTFLightPunctual
{
    struct mmVectorValue lights;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};

void mmGLTFLightPunctual_Init(struct mmGLTFLightPunctual* p);
void mmGLTFLightPunctual_Destroy(struct mmGLTFLightPunctual* p);

int mmParseJSON_DecodeGLTFLightPunctual(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFNodeLight
{
    int light;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFNodeLight_Init(struct mmGLTFNodeLight* p);
void mmGLTFNodeLight_Destroy(struct mmGLTFNodeLight* p);

int mmParseJSON_DecodeGLTFNodeLight(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFNodeChildren_Init(struct mmVectorValue* p);
void mmGLTFNodeChildren_Destroy(struct mmVectorValue* p);

void mmGLTFNodeWeights_Init(struct mmVectorValue* p);
void mmGLTFNodeWeights_Destroy(struct mmVectorValue* p);

struct mmGLTFNode
{
    struct mmRange name;
    struct mmVectorValue children;
    struct mmVectorValue weights;
    float translation[3];
    float rotation[4];
    float scale[3];
    float matrix[4][4];
    int camera;
    int skin;
    int mesh;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
    
    struct mmGLTFNode* parent;

    int sizeTranslation;
    int sizeRotation;
    int sizeScale;
    int sizeMatrix;

    int has_light;
    struct mmGLTFNodeLight ext_light;
};
void mmGLTFNode_Init(struct mmGLTFNode* p);
void mmGLTFNode_Destroy(struct mmGLTFNode* p);

int mmParseJSON_DecodeGLTFNodeExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFNode(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

int mmGLTFNode_GetLigit(const struct mmGLTFNode* p);

void mmGLTFSceneNodes_Init(struct mmVectorValue* p);
void mmGLTFSceneNodes_Destroy(struct mmVectorValue* p);

struct mmGLTFScene
{
    struct mmRange name;
    struct mmVectorValue nodes;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};

void mmGLTFScene_Init(struct mmGLTFScene* p);
void mmGLTFScene_Destroy(struct mmGLTFScene* p);

int mmParseJSON_DecodeGLTFScene(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAnimationSampler
{
    int input;
    int output;
    enum mmGLTFInterpolation_t interpolation;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAnimationSampler_Init(struct mmGLTFAnimationSampler* p);
void mmGLTFAnimationSampler_Destroy(struct mmGLTFAnimationSampler* p);

int mmParseJSON_DecodeGLTFAnimationSampler(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAnimationChannelTarget
{
    int node;
    enum mmGLTFAnimationPath_t path;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAnimationChannelTarget_Init(struct mmGLTFAnimationChannelTarget* p);
void mmGLTFAnimationChannelTarget_Destroy(struct mmGLTFAnimationChannelTarget* p);

int mmParseJSON_DecodeGLTFAnimationChannelTarget(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAnimationChannel
{
    int sampler;
    struct mmGLTFAnimationChannelTarget target;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAnimationChannel_Init(struct mmGLTFAnimationChannel* p);
void mmGLTFAnimationChannel_Destroy(struct mmGLTFAnimationChannel* p);

int mmParseJSON_DecodeGLTFAnimationChannel(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFAnimationSamplers_Init(struct mmVectorValue* p);
void mmGLTFAnimationSamplers_Destroy(struct mmVectorValue* p);

void mmGLTFAnimationChannels_Init(struct mmVectorValue* p);
void mmGLTFAnimationChannels_Destroy(struct mmVectorValue* p);

struct mmGLTFAnimation
{
    struct mmRange name;
    struct mmVectorValue samplers;
    struct mmVectorValue channels;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAnimation_Init(struct mmGLTFAnimation* p);
void mmGLTFAnimation_Destroy(struct mmGLTFAnimation* p);

int mmParseJSON_DecodeGLTFAnimation(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAsset
{
    struct mmRange copyright;
    struct mmRange generator;
    struct mmRange version;
    struct mmRange minVersion;
    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;
};
void mmGLTFAsset_Init(struct mmGLTFAsset* p);
void mmGLTFAsset_Destroy(struct mmGLTFAsset* p);
void mmGLTFAsset_Reset(struct mmGLTFAsset* p);

int mmParseJSON_DecodeGLTFAsset(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

void mmGLTFRootAccessors_Init(struct mmVectorValue* p);
void mmGLTFRootAccessors_Destroy(struct mmVectorValue* p);

void mmGLTFRootBuffers_Init(struct mmVectorValue* p);
void mmGLTFRootBuffers_Destroy(struct mmVectorValue* p);

void mmGLTFRootBufferViews_Init(struct mmVectorValue* p);
void mmGLTFRootBufferViews_Destroy(struct mmVectorValue* p);

void mmGLTFRootImages_Init(struct mmVectorValue* p);
void mmGLTFRootImages_Destroy(struct mmVectorValue* p);

void mmGLTFRootSamplers_Init(struct mmVectorValue* p);
void mmGLTFRootSamplers_Destroy(struct mmVectorValue* p);

void mmGLTFRootTextures_Init(struct mmVectorValue* p);
void mmGLTFRootTextures_Destroy(struct mmVectorValue* p);

void mmGLTFRootMaterials_Init(struct mmVectorValue* p);
void mmGLTFRootMaterials_Destroy(struct mmVectorValue* p);

void mmGLTFRootSkins_Init(struct mmVectorValue* p);
void mmGLTFRootSkins_Destroy(struct mmVectorValue* p);

void mmGLTFRootAnimations_Init(struct mmVectorValue* p);
void mmGLTFRootAnimations_Destroy(struct mmVectorValue* p);

void mmGLTFRootMeshes_Init(struct mmVectorValue* p);
void mmGLTFRootMeshes_Destroy(struct mmVectorValue* p);

void mmGLTFRootCameras_Init(struct mmVectorValue* p);
void mmGLTFRootCameras_Destroy(struct mmVectorValue* p);

void mmGLTFRootNodes_Init(struct mmVectorValue* p);
void mmGLTFRootNodes_Destroy(struct mmVectorValue* p);

void mmGLTFRootScenes_Init(struct mmVectorValue* p);
void mmGLTFRootScenes_Destroy(struct mmVectorValue* p);

struct mmGLTFRoot
{
    int scene;
    struct mmGLTFAsset asset;

    struct mmVectorValue accessors;
    struct mmVectorValue buffers;
    struct mmVectorValue bufferViews;
    struct mmVectorValue images;
    struct mmVectorValue samplers;
    struct mmVectorValue textures;
    struct mmVectorValue materials;
    struct mmVectorValue skins;
    struct mmVectorValue animations;
    struct mmVectorValue meshes;
    struct mmVectorValue cameras;
    struct mmVectorValue nodes;
    struct mmVectorValue scenes;

    struct mmVectorValue extensionsUsed;
    struct mmVectorValue extensionsRequired;

    struct mmGLTFExtras extras;
    struct mmVectorValue extensions;

    int has_lights;
    int has_variants;
    struct mmGLTFLightPunctual ext_lights;
    struct mmGLTFMaterialsVariants ext_variants;
};
void mmGLTFRoot_Init(struct mmGLTFRoot* p);
void mmGLTFRoot_Destroy(struct mmGLTFRoot* p);
void mmGLTFRoot_Reset(struct mmGLTFRoot* p);

int mmParseJSON_DecodeGLTFRootExtension(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);
int mmParseJSON_DecodeGLTFRoot(struct mmParseJSON* p, int i, mmUInt32_t h, void* o);

struct mmGLTFAccessor* mmGLTFRoot_GetAccessor(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFBuffer* mmGLTFRoot_GetBuffer(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFBufferView* mmGLTFRoot_GetBufferView(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFImage* mmGLTFRoot_GetImage(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFSampler* mmGLTFRoot_GetSampler(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFTexture* mmGLTFRoot_GetTexture(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFMaterial* mmGLTFRoot_GetMaterial(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFSkin* mmGLTFRoot_GetSkin(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFAnimation* mmGLTFRoot_GetAnimation(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFMesh* mmGLTFRoot_GetMesh(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFCamera* mmGLTFRoot_GetCamera(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFNode* mmGLTFRoot_GetNode(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFScene* mmGLTFRoot_GetScene(struct mmGLTFRoot* p, size_t idx);
struct mmGLTFLight* mmGLTFRoot_GetLight(struct mmGLTFRoot* p, size_t idx);

struct mmGLTFHeader
{
    mmUInt32_t magic;
    mmUInt32_t version;
    mmUInt32_t length;
};
void mmGLTFHeader_Init(struct mmGLTFHeader* p);
void mmGLTFHeader_Destroy(struct mmGLTFHeader* p);
void mmGLTFHeader_Reset(struct mmGLTFHeader* p);

void mmGLTFHeader_Encode(struct mmGLTFHeader* p, struct mmByteBuffer* pByteBuffer, size_t* offset);
void mmGLTFHeader_Decode(struct mmGLTFHeader* p, struct mmByteBuffer* pByteBuffer, size_t* offset);

struct mmGLTFChunk
{
    mmUInt32_t chunkLength;
    mmUInt32_t chunkType;
    mmUInt8_t* chunkData;
};
void mmGLTFChunk_Init(struct mmGLTFChunk* p);
void mmGLTFChunk_Destroy(struct mmGLTFChunk* p);
void mmGLTFChunk_Reset(struct mmGLTFChunk* p);

void mmGLTFChunk_Encode(struct mmGLTFChunk* p, struct mmByteBuffer* pByteBuffer, size_t* offset);
void mmGLTFChunk_Decode(struct mmGLTFChunk* p, struct mmByteBuffer* pByteBuffer, size_t* offset);

//    Chunk Type ASCII   Description              Occurrences
// 1. 0x4E4F534A JSON    Structured JSON content  1
// 2. 0x004E4942 BIN     Binary buffer            0 or 1
struct mmGLTFLayout
{
    struct mmGLTFHeader header;
    struct mmGLTFChunk  metadata;
    struct mmGLTFChunk  binary;
};
void mmGLTFLayout_Init(struct mmGLTFLayout* p);
void mmGLTFLayout_Destroy(struct mmGLTFLayout* p);
void mmGLTFLayout_Reset(struct mmGLTFLayout* p);

struct mmGLTFFileIO
{
    int(*IsFileExists)(
        struct mmGLTFFileIO*                           pSuper,
        const char*                                    path_name);

    void(*AcquireFileByteBuffer)(
        struct mmGLTFFileIO*                           pSuper,
        const char*                                    path_name,
        struct mmByteBuffer*                           byte_buffer);

    void(*ReleaseFileByteBuffer)(
        struct mmGLTFFileIO*                           pSuper,
        struct mmByteBuffer*                           byte_buffer);
};
void mmGLTFFileIO_Init(struct mmGLTFFileIO* p);
void mmGLTFFileIO_Destroy(struct mmGLTFFileIO* p);

struct mmGLTFModel
{
    enum mmGLTFFile_t type;
    struct mmGLTFLayout layout;
    struct mmGLTFRoot root;
    struct mmParseJSON parse;
    struct mmByteBuffer bytes;
};
void mmGLTFModel_Init(struct mmGLTFModel* p);
void mmGLTFModel_Destroy(struct mmGLTFModel* p);
void mmGLTFModel_Reset(struct mmGLTFModel* p);

int mmGLTFModel_PrepareUriDatas(struct mmGLTFModel* p, struct mmGLTFFileIO* io, const char* path);
void mmGLTFModel_DiscardUriDatas(struct mmGLTFModel* p, struct mmGLTFFileIO* io);

int mmGLTFModel_Validate(const struct mmGLTFModel* p);

int mmGLTFUnHex(char ch);
void mmGLTFDecodeUri(char* uri);

void mmGLTFNodeTransformLocal(const struct mmGLTFNode* node, float matrix[4][4]);
void mmGLTFNodeTransformWorld(const struct mmGLTFNode* node, float matrix[4][4]);

size_t 
mmGLTFCalculationSize(
    enum mmGLTFAccessor_t                          type, 
    enum mmGLTFComponent_t                         componentType);

size_t 
mmGLTFCalculationIndexBound(
    const struct mmGLTFBufferView*                 bufferView,
    const struct mmGLTFBuffer*                     buffer,
    size_t                                         offset, 
    enum mmGLTFComponent_t                         componentType, 
    size_t                                         count);

size_t
mmGLTFComponentReadIndex(
    const void*                                    in,
    enum mmGLTFComponent_t                         componentType);

float
mmGLTFComponentReadFloat(
    const void*                                    in,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized);

int
mmGLTFElementReadFloat(
    const uint8_t*                                 element,
    enum mmGLTFAccessor_t                          type,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized,
    float*                                         out,
    size_t                                         element_size);

void
mmGLTFElementReadVectorFloat(
    const uint8_t*                                 element,
    int32_t                                        numComponents,
    int32_t                                        componentSize,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized,
    float*                                         out);

int
mmGLTFElementReadFloatByIndex(
    const uint8_t*                                 in,
    enum mmGLTFAccessor_t                          type,
    enum mmGLTFComponent_t                         componentType,
    mmBool_t                                       normalized,
    size_t                                         index,
    float*                                         out);

void
mmGLTFIndex8ToIndex16(
    uint8_t*                                      i,
    uint16_t*                                     o,
    size_t                                        count);

struct mmGLTFBuffer*
mmGLTFRoot_BufferViewBytes(
    const struct mmGLTFRoot*                       root, 
    const struct mmGLTFBufferView*                 bufferView, 
    struct mmByteBuffer*                           bytes);

struct mmGLTFBufferView*
mmGLTFRoot_AccessorBytes(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    struct mmByteBuffer*                           bytes);

struct mmGLTFAccessor*
mmGLTFRoot_AccessorIndexBytes(
    const struct mmGLTFRoot*                       root,
    int                                            index,
    struct mmByteBuffer*                           bytes);

int 
mmGLTFRoot_AccessorByteStride(
    const struct mmGLTFRoot*                       root, 
    const struct mmGLTFAccessor*                   accessor);

int 
mmGLTFRoot_AccessorReadFloat(
    const struct mmGLTFRoot*                       root, 
    const struct mmGLTFAccessor*                   accessor, 
    size_t                                         index, 
    float*                                         out, 
    size_t                                         element_size);

size_t 
mmGLTFRoot_AccessorUnpackBytesFloats(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    const struct mmByteBuffer*                     bytes,
    float*                                         out,
    size_t                                         float_count);

size_t
mmGLTFRoot_AccessorUnpackBytes(
    const struct mmGLTFRoot*                       root,
    const struct mmGLTFAccessor*                   accessor,
    const struct mmByteBuffer*                     bytes,
    uint8_t*                                       out,
    size_t                                         count);

#include "core/mmSuffix.h"

#endif//__mmParseGLTF_h__
