/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCJsonHelper_h__
#define __mmCJsonHelper_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "cJSON.h"

#include "core/mmPrefix.h"

void mmCJson_EncodeMember(cJSON* j, size_t n, const void* v, const void* f);
void mmCJson_EncodeObject(cJSON* j, const char* n, const void* v, const void* f);
void mmCJson_EncodeSChar(cJSON* j, const char* n, const mmSChar_t* v, mmSChar_t d, int r);
void mmCJson_EncodeUChar(cJSON* j, const char* n, const mmUChar_t* v, mmUChar_t d, int r);
void mmCJson_EncodeSShort(cJSON* j, const char* n, const mmSShort_t* v, mmSShort_t d, int r);
void mmCJson_EncodeUShort(cJSON* j, const char* n, const mmUShort_t* v, mmUShort_t d, int r);
void mmCJson_EncodeSInt(cJSON* j, const char* n, const mmSInt_t* v, mmSInt_t d, int r);
void mmCJson_EncodeUInt(cJSON* j, const char* n, const mmUInt_t* v, mmUInt_t d, int r);
void mmCJson_EncodeSLong(cJSON* j, const char* n, const mmSLong_t* v, mmSLong_t d, int r);
void mmCJson_EncodeULong(cJSON* j, const char* n, const mmULong_t* v, mmULong_t d, int r);
void mmCJson_EncodeSLLong(cJSON* j, const char* n, const mmSLLong_t* v, mmSLLong_t d, int r);
void mmCJson_EncodeULLong(cJSON* j, const char* n, const mmULLong_t* v, mmULLong_t d, int r);
void mmCJson_EncodeSInt8(cJSON* j, const char* n, const mmSInt8_t* v, mmSInt8_t d, int r);
void mmCJson_EncodeUInt8(cJSON* j, const char* n, const mmUInt8_t* v, mmUInt8_t d, int r);
void mmCJson_EncodeSInt16(cJSON* j, const char* n, const mmSInt16_t* v, mmSInt16_t d, int r);
void mmCJson_EncodeUInt16(cJSON* j, const char* n, const mmUInt16_t* v, mmUInt16_t d, int r);
void mmCJson_EncodeSInt32(cJSON* j, const char* n, const mmSInt32_t* v, mmSInt32_t d, int r);
void mmCJson_EncodeUInt32(cJSON* j, const char* n, const mmUInt32_t* v, mmUInt32_t d, int r);
void mmCJson_EncodeSInt64(cJSON* j, const char* n, const mmSInt64_t* v, mmSInt64_t d, int r);
void mmCJson_EncodeUInt64(cJSON* j, const char* n, const mmUInt64_t* v, mmUInt64_t d, int r);
void mmCJson_EncodeSize(cJSON* j, const char* n, const mmSize_t* v, mmSize_t d, int r);
void mmCJson_EncodeSIntptr(cJSON* j, const char* n, const mmSIntptr_t* v, mmSIntptr_t d, int r);
void mmCJson_EncodeUIntptr(cJSON* j, const char* n, const mmUIntptr_t* v, mmUIntptr_t d, int r);
void mmCJson_EncodeFloat(cJSON* j, const char* n, const float* v, float d, int r);
void mmCJson_EncodeDouble(cJSON* j, const char* n, const double* v, double d, int r);
void mmCJson_EncodeBool(cJSON* j, const char* n, const int* v, int d, int r);
void mmCJson_EncodeString(cJSON* j, const char* n, const struct mmString* v, const char* d, int r);
void mmCJson_EncodeEnumerate(cJSON* j, const char* n, const void* v, int d, int r, const void* f);
void mmCJson_EncodeArray(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f);
void mmCJson_EncodeArrays(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f);
void mmCJson_EncodeNumberVector(cJSON* j, const char* n, const struct mmVectorValue* v, const void* f);
void mmCJson_EncodeStringVector(cJSON* j, const char* n, const struct mmVectorValue* v);

void mmCJson_DecodeMember(const cJSON* j, size_t n, void* v, const void* f);
void mmCJson_DecodeObject(const cJSON* j, const char* n, void* v, const void* f);
void mmCJson_DecodeSChar(const cJSON* j, const char* n, mmSChar_t* v, mmSChar_t d, int r);
void mmCJson_DecodeUChar(const cJSON* j, const char* n, mmUChar_t* v, mmUChar_t d, int r);
void mmCJson_DecodeSShort(const cJSON* j, const char* n, mmSShort_t* v, mmSShort_t d, int r);
void mmCJson_DecodeUShort(const cJSON* j, const char* n, mmUShort_t* v, mmUShort_t d, int r);
void mmCJson_DecodeSInt(const cJSON* j, const char* n, mmSInt_t* v, mmSInt_t d, int r);
void mmCJson_DecodeUInt(const cJSON* j, const char* n, mmUInt_t* v, mmUInt_t d, int r);
void mmCJson_DecodeSLong(const cJSON* j, const char* n, mmSLong_t* v, mmSLong_t d, int r);
void mmCJson_DecodeULong(const cJSON* j, const char* n, mmULong_t* v, mmULong_t d, int r);
void mmCJson_DecodeSLLong(const cJSON* j, const char* n, mmSLLong_t* v, mmSLLong_t d, int r);
void mmCJson_DecodeULLong(const cJSON* j, const char* n, mmULLong_t* v, mmULLong_t d, int r);
void mmCJson_DecodeSInt8(const cJSON* j, const char* n, mmSInt8_t* v, mmSInt8_t d, int r);
void mmCJson_DecodeUInt8(const cJSON* j, const char* n, mmUInt8_t* v, mmUInt8_t d, int r);
void mmCJson_DecodeSInt16(const cJSON* j, const char* n, mmSInt16_t* v, mmSInt16_t d, int r);
void mmCJson_DecodeUInt16(const cJSON* j, const char* n, mmUInt16_t* v, mmUInt16_t d, int r);
void mmCJson_DecodeSInt32(const cJSON* j, const char* n, mmSInt32_t* v, mmSInt32_t d, int r);
void mmCJson_DecodeUInt32(const cJSON* j, const char* n, mmUInt32_t* v, mmUInt32_t d, int r);
void mmCJson_DecodeSInt64(const cJSON* j, const char* n, mmSInt64_t* v, mmSInt64_t d, int r);
void mmCJson_DecodeUInt64(const cJSON* j, const char* n, mmUInt64_t* v, mmUInt64_t d, int r);
void mmCJson_DecodeSize(const cJSON* j, const char* n, mmSize_t* v, mmSize_t d, int r);
void mmCJson_DecodeSIntptr(const cJSON* j, const char* n, mmSIntptr_t* v, mmSIntptr_t d, int r);
void mmCJson_DecodeUIntptr(const cJSON* j, const char* n, mmUIntptr_t* v, mmUIntptr_t d, int r);
void mmCJson_DecodeFloat(const cJSON* j, const char* n, float* v, float d, int r);
void mmCJson_DecodeDouble(const cJSON* j, const char* n, double* v, double d, int r);
void mmCJson_DecodeBool(const cJSON* j, const char* n, int* v, int d, int r);
void mmCJson_DecodeString(const cJSON* j, const char* n, struct mmString* v, const char* d, int r);
void mmCJson_DecodeEnumerate(const cJSON* j, const char* n, void* v, int d, int r, const void* f);
void mmCJson_DecodeArray(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f);
void mmCJson_DecodeArrays(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f);
void mmCJson_DecodeNumberVector(const cJSON* j, const char* n, struct mmVectorValue* v, const void* f);
void mmCJson_DecodeStringVector(const cJSON* j, const char* n, struct mmVectorValue* v);

#include "core/mmSuffix.h"

#endif//__mmCJsonHelper_h__
