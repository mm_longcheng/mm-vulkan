/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKSurface.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

#include "mmUIViewSurfaceMaster.h"

#import <Foundation/Foundation.h>
#import <QuartzCore/CAMetalLayer.h>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunguarded-availability-new"
#endif

void
mmVKSurface_Init(
    struct mmVKSurface*                            p)
{
    mmMemset(p, 0, sizeof(struct mmVKSurface));
}

void
mmVKSurface_Destroy(
    struct mmVKSurface*                            p)
{
    mmMemset(p, 0, sizeof(struct mmVKSurface));
}

void
mmVKSurface_Reset(
    struct mmVKSurface*                            p)
{
    mmMemset(p, 0, sizeof(struct mmVKSurface));
}

VkResult
mmVKSurface_Create(
    struct mmVKSurface*                            p,
    VkInstance                                     instance,
    void*                                          viewSurface,
    const VkAllocationCallbacks*                   pAllocator)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkMetalSurfaceCreateInfoEXT hSurfaceInfo;
        
        mmUIViewSurfaceMaster* pUIViewSurfaceMaster = NULL;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == instance)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " instance is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        if (NULL == viewSurface)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " viewSurface is null.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->viewSurface = viewSurface;

        pUIViewSurfaceMaster = (__bridge mmUIViewSurfaceMaster*)(viewSurface);
        p->window = [pUIViewSurfaceMaster GetSurfaceCALayer];
        
        if (NULL == p->window)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " window is null.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INVALID_EXTERNAL_HANDLE;
            break;
        }
        
        hSurfaceInfo.sType = VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT;
        hSurfaceInfo.pNext = NULL;
        hSurfaceInfo.flags = 0;
        hSurfaceInfo.pLayer = (__bridge const CAMetalLayer*)p->window;
        
        err = vkCreateMetalSurfaceEXT(instance, &hSurfaceInfo, pAllocator, &p->surface);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkCreateMetalSurfaceEXT failure.");
            break;
        }
        
        mmLogger_LogV(gLogger, "vkCreateMetalSurfaceEXT Success.");
        err = VK_SUCCESS;
        
    } while(0);
    
    return err;
}

void
mmVKSurface_Delete(
    struct mmVKSurface*                            p,
    VkInstance                                     instance,
    const VkAllocationCallbacks*                   pAllocator)
{
    if (NULL != instance && NULL != p->surface)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        vkDestroySurfaceKHR(instance, p->surface, pAllocator);
        p->surface = NULL;
        p->viewSurface = NULL;
        p->window = NULL;
        mmLogger_LogV(gLogger, "vkDestroySurfaceKHR Success.");
    }
}

#ifdef __clang__
#pragma clang diagnostic pop
#endif
