/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVK_h__
#define __mmVK_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKMacro.h"

#include "vkcube/linmath.h"

#include "core/mmPrefix.h"

// VulkanInstance
struct mmVK
{
    struct mmString hAppName;
    uint8_t hValidationLayer;
    VkInstance pInstance;
    
    mmVKDefineInstanceProcAddr(GetDeviceProcAddr);
    
    mmVKDefineInstanceProcAddr(GetPhysicalDeviceSurfaceSupportKHR);
    mmVKDefineInstanceProcAddr(GetPhysicalDeviceSurfaceCapabilitiesKHR);
    mmVKDefineInstanceProcAddr(GetPhysicalDeviceSurfaceFormatsKHR);
    mmVKDefineInstanceProcAddr(GetPhysicalDeviceSurfacePresentModesKHR);
    
    mmVKDefineInstanceProcAddr(CreateDebugUtilsMessengerEXT);
    mmVKDefineInstanceProcAddr(DestroyDebugUtilsMessengerEXT);
    mmVKDefineInstanceProcAddr(SubmitDebugUtilsMessageEXT);
    mmVKDefineInstanceProcAddr(CmdBeginDebugUtilsLabelEXT);
    mmVKDefineInstanceProcAddr(CmdEndDebugUtilsLabelEXT);
    mmVKDefineInstanceProcAddr(CmdInsertDebugUtilsLabelEXT);
    mmVKDefineInstanceProcAddr(SetDebugUtilsObjectNameEXT);
    
    VkDebugUtilsMessengerEXT hDebugUtilsMessenger;
};

void mmVK_Init(struct mmVK* p);
void mmVK_Destroy(struct mmVK* p);

void mmVK_OnFinishLaunching(struct mmVK* p);
void mmVK_OnBeforeTerminate(struct mmVK* p);

struct mmVKPhysicalDevice
{
    VkInstance pInstance;
    VkPhysicalDevice pPhysicalDevice;
    VkPhysicalDeviceProperties hPhysicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties hPhysicalDeviceMemoryProperties;
    VkPhysicalDeviceFeatures hPhysDevFeatures;
};

void mmVKPhysicalDevice_Init(struct mmVKPhysicalDevice* p);
void mmVKPhysicalDevice_Destroy(struct mmVKPhysicalDevice* p);

void mmVKPhysicalDevice_OnFinishLaunching(struct mmVKPhysicalDevice* p);
void mmVKPhysicalDevice_OnBeforeTerminate(struct mmVKPhysicalDevice* p);

#define FRAME_LAG 2
struct mmVKDevice
{
    VkDevice device;
    VkQueue graphics_queue;
    VkQueue present_queue;
    uint32_t graphics_queue_family_index;
    uint32_t present_queue_family_index;
    int separate_present_queue;
    VkFence fences[FRAME_LAG];
    VkSemaphore image_acquired_semaphores[FRAME_LAG];
    VkSemaphore draw_complete_semaphores[FRAME_LAG];
    VkSemaphore image_ownership_semaphores[FRAME_LAG];
    
    mmVKDefineDeviceProcAddr(CreateSwapchainKHR);
    mmVKDefineDeviceProcAddr(DestroySwapchainKHR);
    mmVKDefineDeviceProcAddr(GetSwapchainImagesKHR);
    mmVKDefineDeviceProcAddr(AcquireNextImageKHR);
    mmVKDefineDeviceProcAddr(QueuePresentKHR);
};

typedef struct
{
    VkImage image;
    VkCommandBuffer cmd;
    VkCommandBuffer graphics_to_present_cmd;
    VkImageView view;
    VkBuffer uniform_buffer;
    VkDeviceMemory uniform_memory;
    void *uniform_memory_ptr;
    VkFramebuffer framebuffer;
    VkDescriptorSet descriptor_set;
} mmVKSwapchainImageResources;

struct mmVKSurfaceDepth
{
    VkFormat format;

    VkImage image;
    VkMemoryAllocateInfo mem_alloc;
    VkDeviceMemory mem;
    VkImageView view;
};
struct mmVKSurface
{
    VkSurfaceKHR pSurface;
    VkSurfaceFormatKHR hSurfaceFormat;
    
    int width, height;
    int is_minimized;
    
    uint32_t swapchainImageCount;
    VkSwapchainKHR swapchain;
    mmVKSwapchainImageResources* swapchain_image_resources;
    VkPresentModeKHR presentMode;
    
    struct mmVKSurfaceDepth depth;
    
    void* caMetalLayer;
};

void mmVKPhysicalDevice_Queue(struct mmVK* p, VkPhysicalDevice pPhysicalDevice, VkSurfaceKHR pSurface, struct mmVKDevice* pVulkanDevice);
void mmVKCreateDevice(struct mmVKDevice* p, VkPhysicalDevice pPhysicalDevice, struct mmVK* pVulkan);
void mmVKSurfaceFormat(struct mmVK* pVulkan, VkPhysicalDevice pPhysicalDevice, struct mmVKSurface* pVulkanSurface);
void mmVKSemaphore(struct mmVKDevice* pVulkanDevice);
void mmVKSwapchain(struct mmVK* pVulkan, struct mmVKDevice* pVulkanDevice, struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKSurface* pVulkanSurface);
void mmVKPrepareDepth(struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKSurface* pVulkanSurface, struct mmVKDevice* pVulkanDevice);

struct mmVKTextureObject
{
    VkSampler sampler;

    VkImage image;
    VkBuffer buffer;
    VkImageLayout imageLayout;

    VkMemoryAllocateInfo mem_alloc;
    VkDeviceMemory mem;
    VkImageView view;
    int32_t tex_width, tex_height;
};

#define VTD_TEXTURE_COUNT 1
struct mmVKAppData
{
    int use_staging_buffer;
    
    struct mmVKTextureObject textures[VTD_TEXTURE_COUNT];
    struct mmVKTextureObject staging_texture;
    
    // Buffer for initialization commands
    VkCommandBuffer cmd;
    VkPipelineLayout pipeline_layout;
    VkDescriptorSetLayout desc_layout;
    VkPipelineCache pipelineCache;
    VkRenderPass render_pass;
    VkPipeline pipeline;
    
    VkShaderModule vert_shader_module;
    VkShaderModule frag_shader_module;
    
    VkDescriptorPool desc_pool;
    
    VkCommandPool cmd_pool;
    VkCommandPool present_cmd_pool;
    
    mat4x4 projection_matrix;
    mat4x4 view_matrix;
    mat4x4 model_matrix;
    
    uint32_t current_buffer;
    int prepared;
    int frame_index;
    
    float spin_angle;
};

void mmVKMakeInitCmd(struct mmVKAppData* pVulkanAppData, struct mmVKDevice* pVulkanDevice);
void mmVKPrepareTextures(struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareCubeDataBuffers(
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKAppData* pVulkanAppData,
    struct mmVKSurface* pVulkanSurface);

void mmVKPrepareDescriptorLayout(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareRenderPass(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData);
void mmVKPreparePipeline(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareSwapchainCmd(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareDescriptorPool(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareDescriptorSet(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData);
void mmVKPrepareFramebuffers(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData);
void mmVKSwapchainDrawBuildCmd(
    struct mmVK* pVulkan,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData);
void mmVKFlushInitCmdDestroyStagingTexture(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData);

void mmVKPrepare(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData);

void mmVKResize(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData);

void mmVKCleanup(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData);

void mmVKDraw(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData);

void mmVKPrepareMatrix(struct mmVKAppData* pVulkanAppData);

#include "core/mmSuffix.h"

#endif//__mmVK_h__
