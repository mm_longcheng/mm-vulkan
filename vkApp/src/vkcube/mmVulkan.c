/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVK.h"
#include "vk/mmVKExtension.h"
#include "vk/mmVKVersion.h"
#include "vk/mmVKPlatform.h"
#include "vk/mmVKDebugger.h"
#include "vk/mmVKSurface.h"

#include "core/mmLogger.h"

#include "vkcube/linmath.h"

#include "math/mmMath.h"

#include "mmVKUploader.h"

void mmVK_Init(struct mmVK* p)
{
    mmString_Init(&p->hAppName);
    p->hValidationLayer = 0;
    p->pInstance = NULL;
    
    mmVKInstanceProcAddrAssignNull(GetDeviceProcAddr);
    
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceSupportKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceCapabilitiesKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceFormatsKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfacePresentModesKHR);
    
    mmVKInstanceProcAddrAssignNull(CreateDebugUtilsMessengerEXT);
    mmVKInstanceProcAddrAssignNull(DestroyDebugUtilsMessengerEXT);
    mmVKInstanceProcAddrAssignNull(SubmitDebugUtilsMessageEXT);
    mmVKInstanceProcAddrAssignNull(CmdBeginDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(CmdEndDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(CmdInsertDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(SetDebugUtilsObjectNameEXT);
    
    memset(&p->hDebugUtilsMessenger, 0, sizeof(VkDebugUtilsMessengerEXT));
}
void mmVK_Destroy(struct mmVK* p)
{
    memset(&p->hDebugUtilsMessenger, 0, sizeof(VkDebugUtilsMessengerEXT));
    
    mmVKInstanceProcAddrAssignNull(CreateDebugUtilsMessengerEXT);
    mmVKInstanceProcAddrAssignNull(DestroyDebugUtilsMessengerEXT);
    mmVKInstanceProcAddrAssignNull(SubmitDebugUtilsMessageEXT);
    mmVKInstanceProcAddrAssignNull(CmdBeginDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(CmdEndDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(CmdInsertDebugUtilsLabelEXT);
    mmVKInstanceProcAddrAssignNull(SetDebugUtilsObjectNameEXT);
    
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceSupportKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceCapabilitiesKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfaceFormatsKHR);
    mmVKInstanceProcAddrAssignNull(GetPhysicalDeviceSurfacePresentModesKHR);
    
    mmVKInstanceProcAddrAssignNull(GetDeviceProcAddr);
    
    p->pInstance = NULL;
    p->hValidationLayer = 0;
    mmString_Destroy(&p->hAppName);
}

void mmVK_OnFinishLaunching(struct mmVK* p)
{
    VkResult err;

    struct mmVKExtension hInstanceLayerArray;
    struct mmVKExtension hInstanceExtensionArray;

    mmVKExtension_Init(&hInstanceLayerArray);
    mmVKExtension_Init(&hInstanceExtensionArray);

    mmVKExtension_AllocArraySize(&hInstanceLayerArray, 1);
    mmVKExtension_AllocArraySize(&hInstanceExtensionArray, 4);

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        const char* pApplicationName = mmString_CStr(&p->hAppName);

        VkApplicationInfo hApplicationInfo;
        VkInstanceCreateInfo hInstanceCreateInfo;
        VkDebugUtilsMessengerCreateInfoEXT hDebugUtilsMessengerCreateInfo;

        mmVKExtensionMakeInstanceLayer(&hInstanceLayerArray);
        mmVKExtensionMakeInstanceExtension(&hInstanceExtensionArray);
        mmVKExtensionMakeDebugger(&hInstanceLayerArray, &hInstanceExtensionArray, p->hValidationLayer);

        mmVKExtension_EnabledInstanceLayer(&hInstanceLayerArray);
        mmVKExtension_EnabledInstanceExtension(&hInstanceExtensionArray);

        if (!mmVKExtension_GetIsSuitable(&hInstanceLayerArray))
        {
            mmLogger_LogE(gLogger, "InstanceLayer extension is not complete suitable.");
            break;
        }
        if (!mmVKExtension_GetIsSuitable(&hInstanceExtensionArray))
        {
            mmLogger_LogE(gLogger, "InstanceExtension extension is not complete suitable.");
            break;
        }

        hApplicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        hApplicationInfo.pNext = NULL;
        hApplicationInfo.pApplicationName = pApplicationName;
        hApplicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        hApplicationInfo.pEngineName = mmVKEngineName;
        hApplicationInfo.engineVersion = VK_MAKE_VERSION(mmVKMajor, mmVKMinor, mmVKPatch);
        hApplicationInfo.apiVersion = VK_API_VERSION_1_0;

        hInstanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        hInstanceCreateInfo.pNext = NULL;
        hInstanceCreateInfo.flags = 0;
        hInstanceCreateInfo.pApplicationInfo = &hApplicationInfo;
        hInstanceCreateInfo.enabledLayerCount = hInstanceLayerArray.hEnabledCount;
        hInstanceCreateInfo.ppEnabledLayerNames = (const char* const*)hInstanceLayerArray.pEnabledArray;
        hInstanceCreateInfo.enabledExtensionCount = hInstanceExtensionArray.hEnabledCount;
        hInstanceCreateInfo.ppEnabledExtensionNames = (const char* const*)hInstanceExtensionArray.pEnabledArray;

        if (1 == p->hValidationLayer)
        {
            VkDebugUtilsMessageSeverityFlagsEXT hMessageSeverity = 0;
            VkDebugUtilsMessageTypeFlagsEXT hMessageType = 0;

            hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
            hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
            hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
            hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

            hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
            hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
            hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

            // VK_EXT_debug_utils style
            hDebugUtilsMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            hDebugUtilsMessengerCreateInfo.pNext = NULL;
            hDebugUtilsMessengerCreateInfo.flags = 0;
            hDebugUtilsMessengerCreateInfo.messageSeverity = hMessageSeverity;
            hDebugUtilsMessengerCreateInfo.messageType = hMessageType;
            hDebugUtilsMessengerCreateInfo.pfnUserCallback = mmVKDebuggerMessengerCallback;
            hDebugUtilsMessengerCreateInfo.pUserData = p;
            hInstanceCreateInfo.pNext = &hDebugUtilsMessengerCreateInfo;
        }
        else
        {
            hInstanceCreateInfo.pNext = NULL;
        }

        err = vkCreateInstance(&hInstanceCreateInfo, NULL, &p->pInstance);
        if (err == VK_ERROR_INCOMPATIBLE_DRIVER)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance Failure.");
            mmLogger_LogE(gLogger, "Cannot find a compatible Vulkan installable client driver (ICD).");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }
        if (err == VK_ERROR_EXTENSION_NOT_PRESENT)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance Failure.");
            mmLogger_LogE(gLogger, "Cannot find a specified extension library.");
            mmLogger_LogE(gLogger, "Make sure your layers path is set appropriately.");
            break;
        }
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance Failure.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }

        mmVKGetInstanceProcAddr(p, p->pInstance, GetDeviceProcAddr);
        // Setup PhysicalDeviceSurface function pointers
        mmVKGetInstanceProcAddr(p, p->pInstance, GetPhysicalDeviceSurfaceSupportKHR);
        mmVKGetInstanceProcAddr(p, p->pInstance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
        mmVKGetInstanceProcAddr(p, p->pInstance, GetPhysicalDeviceSurfaceFormatsKHR);
        mmVKGetInstanceProcAddr(p, p->pInstance, GetPhysicalDeviceSurfacePresentModesKHR);
        
        if (1 == p->hValidationLayer)
        {
            // Setup VK_EXT_debug_utils function pointers always (we use them for debug labels and names).
            mmVKGetInstanceProcAddr(p, p->pInstance, CreateDebugUtilsMessengerEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, DestroyDebugUtilsMessengerEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, SubmitDebugUtilsMessageEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, CmdBeginDebugUtilsLabelEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, CmdEndDebugUtilsLabelEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, CmdInsertDebugUtilsLabelEXT);
            mmVKGetInstanceProcAddr(p, p->pInstance, SetDebugUtilsObjectNameEXT);
             
            err = p->vkCreateDebugUtilsMessengerEXT(p->pInstance, &hDebugUtilsMessengerCreateInfo, NULL, &p->hDebugUtilsMessenger);
            if (err == VK_ERROR_OUT_OF_HOST_MEMORY)
            {
                mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT Failure.");
                mmLogger_LogE(gLogger, "CreateDebugUtilsMessengerEXT: out of host memory.");
                break;
            }
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT Failure.");
                mmLogger_LogE(gLogger, "CreateDebugUtilsMessengerEXT: unknown failure.");
                break;
            }
        }
    } while (0);

    mmVKExtension_DeallocArray(&hInstanceLayerArray);
    mmVKExtension_DeallocArray(&hInstanceExtensionArray);

    mmVKExtension_Destroy(&hInstanceLayerArray);
    mmVKExtension_Destroy(&hInstanceExtensionArray);
}
void mmVK_OnBeforeTerminate(struct mmVK* p)
{
    vkDestroyInstance(p->pInstance, NULL);
    p->pInstance = NULL;
}

void mmVKPhysicalDevice_Init(struct mmVKPhysicalDevice* p)
{
    memset(p, 0, sizeof(struct mmVKPhysicalDevice));
}
void mmVKPhysicalDevice_Destroy(struct mmVKPhysicalDevice* p)
{
    memset(p, 0, sizeof(struct mmVKPhysicalDevice));
}

void mmVKPhysicalDevice_OnFinishLaunching(struct mmVKPhysicalDevice* p)
{
    VkPhysicalDevice* pPhysicalDeviceArray = NULL;
    
    do
    {
        VkResult err;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        uint32_t hPhysicalDeviceCount = 0;
        err = vkEnumeratePhysicalDevices(p->pInstance, &hPhysicalDeviceCount, NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkEnumeratePhysicalDevices Failure.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }
        
        if (0 == hPhysicalDeviceCount)
        {
            mmLogger_LogE(gLogger, "vkEnumeratePhysicalDevices reported zero accessible devices.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }
        
        if (1 == hPhysicalDeviceCount)
        {
            // No choice.
            err = vkEnumeratePhysicalDevices(p->pInstance, &hPhysicalDeviceCount, &p->pPhysicalDevice);
            assert(!err);
        }
        else
        {
            uint32_t i = 0;
            
            pPhysicalDeviceArray = malloc(sizeof(VkPhysicalDevice) * hPhysicalDeviceCount);
            err = vkEnumeratePhysicalDevices(p->pInstance, &hPhysicalDeviceCount, pPhysicalDeviceArray);
            assert(!err);
            
            uint32_t pDeviceTypeCounter[VK_PHYSICAL_DEVICE_TYPE_CPU + 1] = { 0 };

            VkPhysicalDeviceProperties hPhysicalDeviceProperties;
            for (i = 0; i < hPhysicalDeviceCount; i++)
            {
                vkGetPhysicalDeviceProperties(pPhysicalDeviceArray[i], &hPhysicalDeviceProperties);
                assert(hPhysicalDeviceProperties.deviceType <= VK_PHYSICAL_DEVICE_TYPE_CPU);
                pDeviceTypeCounter[hPhysicalDeviceProperties.deviceType]++;
            }
            
            static const uint32_t gPhysicalDeviceTypepPriority[5] =
            {
                VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
                VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
                VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU,
                VK_PHYSICAL_DEVICE_TYPE_CPU,
                VK_PHYSICAL_DEVICE_TYPE_OTHER,
            };
            
            VkPhysicalDeviceType hSearchForDeviceType = VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
            for (i = 0; i < MM_ARRAY_SIZE(gPhysicalDeviceTypepPriority); i++)
            {
                uint32_t hPhysicalDeviceType = gPhysicalDeviceTypepPriority[i];
                if (0 < pDeviceTypeCounter[hPhysicalDeviceType])
                {
                    hSearchForDeviceType = hPhysicalDeviceType;
                    break;
                }
            }

            // Use graphics card storage to determine the priority.
            uint64_t hMaxMemoryAllocationCount = 0;
            uint32_t hPhysicalDeviceIndex = 0;
            for (i = 0; i < hPhysicalDeviceCount; i++)
            {
                vkGetPhysicalDeviceProperties(pPhysicalDeviceArray[i], &hPhysicalDeviceProperties);
                if (hPhysicalDeviceProperties.deviceType == hSearchForDeviceType)
                {
                    if (hMaxMemoryAllocationCount < hPhysicalDeviceProperties.limits.maxMemoryAllocationCount)
                    {
                        hMaxMemoryAllocationCount = hPhysicalDeviceProperties.limits.maxMemoryAllocationCount;
                        hPhysicalDeviceIndex = i;
                    }
                }
            }
            
            assert(hPhysicalDeviceIndex >= 0);
            
            p->pPhysicalDevice = pPhysicalDeviceArray[hPhysicalDeviceIndex];
        }
        
        // Query physical device properties.
        vkGetPhysicalDeviceProperties(p->pPhysicalDevice, &p->hPhysicalDeviceProperties);
        
        // Get Memory information and properties
        vkGetPhysicalDeviceMemoryProperties(p->pPhysicalDevice, &p->hPhysicalDeviceMemoryProperties);
        
        // Query fine-grained feature support for this device.
        //  If app has specific feature requirements it should check supported
        //  features based on this query
        vkGetPhysicalDeviceFeatures(p->pPhysicalDevice, &p->hPhysDevFeatures);
    } while(0);
    
    free(pPhysicalDeviceArray);
}
void mmVKPhysicalDevice_X1(struct mmVKPhysicalDevice* p)
{

}
void mmVKPhysicalDevice_OnBeforeTerminate(struct mmVKPhysicalDevice* p)
{
    
}

void mmVKPhysicalDevice_Queue(struct mmVK* p, VkPhysicalDevice pPhysicalDevice, VkSurfaceKHR pSurface, struct mmVKDevice* pVulkanDevice)
{
    VkResult err;
    VkQueueFamilyProperties* pQueueFamilyPropertyArray = NULL;
    VkBool32* pSupportPresentArray = NULL;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        uint32_t i = 0;
        /* Call with NULL data to get count */
        uint32_t hQueueFamilyPropertyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(pPhysicalDevice, &hQueueFamilyPropertyCount, NULL);
        if (0 == hQueueFamilyPropertyCount)
        {
            mmLogger_LogE(gLogger, "hQueueFamilyPropertyCount is 0.");
            break;
        }

        pQueueFamilyPropertyArray = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties) * hQueueFamilyPropertyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(pPhysicalDevice, &hQueueFamilyPropertyCount, pQueueFamilyPropertyArray);
        
        // Iterate over each queue to learn whether it supports presenting:
        pSupportPresentArray = (VkBool32*)malloc(sizeof(VkBool32) * hQueueFamilyPropertyCount);
        for (i = 0; i < hQueueFamilyPropertyCount; i++)
        {
            err = p->vkGetPhysicalDeviceSurfaceSupportKHR(pPhysicalDevice, i, pSurface, &pSupportPresentArray[i]);
            assert(!err);
        }
        
        // Search for a graphics and a present queue in the array of queue
        // families, try to find one that supports both
        uint32_t graphicsQueueFamilyIndex = UINT32_MAX;
        uint32_t presentQueueFamilyIndex = UINT32_MAX;
        for (i = 0; i < hQueueFamilyPropertyCount; i++)
        {
            if ((pQueueFamilyPropertyArray[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
            {
                if (graphicsQueueFamilyIndex == UINT32_MAX)
                {
                    graphicsQueueFamilyIndex = i;
                }

                if (pSupportPresentArray[i] == VK_TRUE)
                {
                    graphicsQueueFamilyIndex = i;
                    presentQueueFamilyIndex = i;
                    break;
                }
            }
        }

        if (presentQueueFamilyIndex == UINT32_MAX)
        {
            // If didn't find a queue that supports both graphics and present, then
            // find a separate present queue.
            for (i = 0; i < hQueueFamilyPropertyCount; ++i)
            {
                if (pSupportPresentArray[i] == VK_TRUE)
                {
                    presentQueueFamilyIndex = i;
                    break;
                }
            }
        }

        // Generate error if could not find both a graphics and a present queue
        if (graphicsQueueFamilyIndex == UINT32_MAX || presentQueueFamilyIndex == UINT32_MAX)
        {
            mmLogger_LogE(gLogger, "Swapchain Initialization Failure.");
            mmLogger_LogE(gLogger, "Could not find both graphics and present queues.");
            break;
        }

        pVulkanDevice->graphics_queue_family_index = graphicsQueueFamilyIndex;
        pVulkanDevice->present_queue_family_index = presentQueueFamilyIndex;
        pVulkanDevice->separate_present_queue = (pVulkanDevice->graphics_queue_family_index != pVulkanDevice->present_queue_family_index);
    } while(0);
    
    free(pSupportPresentArray);
    free(pQueueFamilyPropertyArray);
}

void mmVKCreateDevice(struct mmVKDevice* p, VkPhysicalDevice pPhysicalDevice, struct mmVK* pVulkan)
{
    struct mmVKExtension hDeviceExtensionArray;
    mmVKExtension_Init(&hDeviceExtensionArray);
    mmVKExtension_AllocArraySize(&hDeviceExtensionArray, 2);
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmVKExtension_SetRequireFeature(&hDeviceExtensionArray, 0, 1, VK_KHR_SWAPCHAIN_EXTENSION_NAME);
        mmVKExtension_SetRequireFeature(&hDeviceExtensionArray, 1, 0, "VK_KHR_portability_subset");
        mmVKExtension_EnabledDeviceExtension(&hDeviceExtensionArray, pPhysicalDevice);
        
        if (!mmVKExtension_GetIsSupport(&hDeviceExtensionArray, 0))
        {
            mmLogger_LogE(gLogger, "vkEnumerateDeviceExtensionProperties failed to find the %s extension.", VK_KHR_SWAPCHAIN_EXTENSION_NAME);
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }
        if (!mmVKExtension_GetIsSuitable(&hDeviceExtensionArray))
        {
            mmLogger_LogE(gLogger, "DeviceExtension extension is not complete suitable.");
            break;
        }
        
        VkResult err;
        float queue_priorities[1] = { 0.0f };
        VkDeviceQueueCreateInfo hDeviceQueueCreateInfo[2];
        hDeviceQueueCreateInfo[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        hDeviceQueueCreateInfo[0].pNext = NULL;
        hDeviceQueueCreateInfo[0].queueFamilyIndex = p->graphics_queue_family_index;
        hDeviceQueueCreateInfo[0].queueCount = 1;
        hDeviceQueueCreateInfo[0].pQueuePriorities = queue_priorities;
        hDeviceQueueCreateInfo[0].flags = 0;

        // If specific features are required, pass them in here
        VkDeviceCreateInfo hDeviceCreateInfo;
        hDeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        hDeviceCreateInfo.pNext = NULL;
        hDeviceCreateInfo.flags = 0;
        hDeviceCreateInfo.pQueueCreateInfos = hDeviceQueueCreateInfo;
        hDeviceCreateInfo.enabledLayerCount = 0;
        hDeviceCreateInfo.ppEnabledLayerNames = NULL;
        hDeviceCreateInfo.enabledExtensionCount = hDeviceExtensionArray.hEnabledCount;
        hDeviceCreateInfo.ppEnabledExtensionNames = (const char *const *)hDeviceExtensionArray.pEnabledArray;
        hDeviceCreateInfo.pEnabledFeatures = NULL;
        
        if (p->separate_present_queue)
        {
            hDeviceQueueCreateInfo[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            hDeviceQueueCreateInfo[1].pNext = NULL;
            hDeviceQueueCreateInfo[1].queueFamilyIndex = p->present_queue_family_index;
            hDeviceQueueCreateInfo[1].queueCount = 1;
            hDeviceQueueCreateInfo[1].pQueuePriorities = queue_priorities;
            hDeviceQueueCreateInfo[1].flags = 0;
            
            hDeviceCreateInfo.queueCreateInfoCount = 2;
        }
        else
        {
            hDeviceCreateInfo.queueCreateInfoCount = 1;
        }
        err = vkCreateDevice(pPhysicalDevice, &hDeviceCreateInfo, NULL, &p->device);
        assert(!err);
        
        mmVKGetDeviceProcAddr(pVulkan, p, p->device, CreateSwapchainKHR);
        mmVKGetDeviceProcAddr(pVulkan, p, p->device, DestroySwapchainKHR);
        mmVKGetDeviceProcAddr(pVulkan, p, p->device, GetSwapchainImagesKHR);
        mmVKGetDeviceProcAddr(pVulkan, p, p->device, AcquireNextImageKHR);
        mmVKGetDeviceProcAddr(pVulkan, p, p->device, QueuePresentKHR);
        
        vkGetDeviceQueue(p->device, p->graphics_queue_family_index, 0, &p->graphics_queue);
        // vkGetDeviceQueue(p->device, p->present_queue_family_index, 0, &p->present_queue);
        if (!p->separate_present_queue)
        {
            p->present_queue = p->graphics_queue;
        }
        else
        {
            vkGetDeviceQueue(p->device, p->present_queue_family_index, 0, &p->present_queue);
        }
    } while(0);
    
    mmVKExtension_DeallocArray(&hDeviceExtensionArray);
    mmVKExtension_Destroy(&hDeviceExtensionArray);
}

static VkSurfaceFormatKHR mmVKPickSurfaceFormat(const VkSurfaceFormatKHR *surfaceFormats, uint32_t count)
{
    uint32_t i = 0;
    // Prefer non-SRGB formats...
    for (i = 0; i < count; i++)
    {
        const VkFormat format = surfaceFormats[i].format;

        if (format == VK_FORMAT_R8G8B8A8_UNORM ||
            format == VK_FORMAT_B8G8R8A8_UNORM ||
            format == VK_FORMAT_A2B10G10R10_UNORM_PACK32 ||
            format == VK_FORMAT_A2R10G10B10_UNORM_PACK32 ||
            format == VK_FORMAT_R16G16B16A16_SFLOAT)
        {
            return surfaceFormats[i];
        }
    }

    printf("Can't find our preferred formats... Falling back to first exposed format. Rendering may be incorrect.\n");

    assert(count >= 1);
    return surfaceFormats[0];
}

void mmVKSurfaceFormat(struct mmVK* pVulkan, VkPhysicalDevice pPhysicalDevice, struct mmVKSurface* pVulkanSurface)
{
    VkResult err;
    // Get the list of VkFormat's that are supported:
    uint32_t formatCount;
    err = pVulkan->vkGetPhysicalDeviceSurfaceFormatsKHR(pPhysicalDevice, pVulkanSurface->pSurface, &formatCount, NULL);
    assert(!err);
    VkSurfaceFormatKHR *surfFormats = (VkSurfaceFormatKHR*)malloc(formatCount * sizeof(VkSurfaceFormatKHR));
    err = pVulkan->vkGetPhysicalDeviceSurfaceFormatsKHR(pPhysicalDevice, pVulkanSurface->pSurface, &formatCount, surfFormats);
    assert(!err);
    pVulkanSurface->hSurfaceFormat = mmVKPickSurfaceFormat(surfFormats, formatCount);
    free(surfFormats);
}
void mmVKSemaphore(struct mmVKDevice* pVulkanDevice)
{
    // Create semaphores to synchronize acquiring presentable buffers before
    // rendering and waiting for drawing to be complete before presenting
    VkSemaphoreCreateInfo semaphoreCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
    };

    // Create fences that we can use to throttle if we get too far
    // ahead of the image presents
    VkFenceCreateInfo fence_ci =
    {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT,
    };
    VkResult err;
    uint32_t i = 0;
    for (i = 0; i < FRAME_LAG; i++)
    {
        err = vkCreateFence(pVulkanDevice->device, &fence_ci, NULL, &pVulkanDevice->fences[i]);
        assert(!err);

        err = vkCreateSemaphore(pVulkanDevice->device, &semaphoreCreateInfo, NULL, &pVulkanDevice->image_acquired_semaphores[i]);
        assert(!err);

        err = vkCreateSemaphore(pVulkanDevice->device, &semaphoreCreateInfo, NULL, &pVulkanDevice->draw_complete_semaphores[i]);
        assert(!err);

        if (pVulkanDevice->separate_present_queue)
        {
            err = vkCreateSemaphore(pVulkanDevice->device, &semaphoreCreateInfo, NULL, &pVulkanDevice->image_ownership_semaphores[i]);
            assert(!err);
        }
    }
}

void mmVKSwapchain(struct mmVK* pVulkan, struct mmVKDevice* pVulkanDevice, struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKSurface* pVulkanSurface)
{
    VkResult err;
    VkSwapchainKHR oldSwapchain = pVulkanSurface->swapchain;

    // Check the surface capabilities and formats
    VkSurfaceCapabilitiesKHR surfCapabilities;
    err = pVulkan->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pVulkanPhysicalDevice->pPhysicalDevice, pVulkanSurface->pSurface, &surfCapabilities);
    assert(!err);

    uint32_t presentModeCount;
    err = pVulkan->vkGetPhysicalDeviceSurfacePresentModesKHR(pVulkanPhysicalDevice->pPhysicalDevice, pVulkanSurface->pSurface, &presentModeCount, NULL);
    assert(!err);
    VkPresentModeKHR *presentModes = (VkPresentModeKHR *)malloc(presentModeCount * sizeof(VkPresentModeKHR));
    assert(presentModes);
    err = pVulkan->vkGetPhysicalDeviceSurfacePresentModesKHR(pVulkanPhysicalDevice->pPhysicalDevice, pVulkanSurface->pSurface, &presentModeCount, presentModes);
    assert(!err);

    VkExtent2D swapchainExtent;
    // width and height are either both 0xFFFFFFFF, or both not 0xFFFFFFFF.
    if (surfCapabilities.currentExtent.width == 0xFFFFFFFF) {
        // If the surface size is undefined, the size is set to the size
        // of the images requested, which must fit within the minimum and
        // maximum values.
        swapchainExtent.width = pVulkanSurface->width;
        swapchainExtent.height = pVulkanSurface->height;

        if (swapchainExtent.width < surfCapabilities.minImageExtent.width) {
            swapchainExtent.width = surfCapabilities.minImageExtent.width;
        } else if (swapchainExtent.width > surfCapabilities.maxImageExtent.width) {
            swapchainExtent.width = surfCapabilities.maxImageExtent.width;
        }

        if (swapchainExtent.height < surfCapabilities.minImageExtent.height) {
            swapchainExtent.height = surfCapabilities.minImageExtent.height;
        } else if (swapchainExtent.height > surfCapabilities.maxImageExtent.height) {
            swapchainExtent.height = surfCapabilities.maxImageExtent.height;
        }
    } else {
        // If the surface size is defined, the swap chain size must match
        swapchainExtent = surfCapabilities.currentExtent;
        pVulkanSurface->width = surfCapabilities.currentExtent.width;
        pVulkanSurface->height = surfCapabilities.currentExtent.height;
    }

    if (pVulkanSurface->width == 0 || pVulkanSurface->height == 0)
    {
        pVulkanSurface->is_minimized = VK_TRUE;
        return;
    }
    else
    {
        pVulkanSurface->is_minimized = VK_FALSE;
    }

    // The FIFO present mode is guaranteed by the spec to be supported
    // and to have no tearing.  It's a great default present mode to use.
    VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

    //  There are times when you may wish to use another present mode.  The
    //  following code shows how to select them, and the comments provide some
    //  reasons you may wish to use them.
    //
    // It should be noted that Vulkan 1.0 doesn't provide a method for
    // synchronizing rendering with the presentation engine's display.  There
    // is a method provided for throttling rendering with the display, but
    // there are some presentation engines for which this method will not work.
    // If an application doesn't throttle its rendering, and if it renders much
    // faster than the refresh rate of the display, this can waste power on
    // mobile devices.  That is because power is being spent rendering images
    // that may never be seen.

    // VK_PRESENT_MODE_IMMEDIATE_KHR is for applications that don't care about
    // tearing, or have some way of synchronizing their rendering with the
    // display.
    // VK_PRESENT_MODE_MAILBOX_KHR may be useful for applications that
    // generally render a new presentable image every refresh cycle, but are
    // occasionally early.  In this case, the application wants the new image
    // to be displayed instead of the previously-queued-for-presentation image
    // that has not yet been displayed.
    // VK_PRESENT_MODE_FIFO_RELAXED_KHR is for applications that generally
    // render a new presentable image every refresh cycle, but are occasionally
    // late.  In this case (perhaps because of stuttering/latency concerns),
    // the application wants the late image to be immediately displayed, even
    // though that may mean some tearing.

    if (pVulkanSurface->presentMode != swapchainPresentMode)
    {
        for (size_t i = 0; i < presentModeCount; ++i)
        {
            if (presentModes[i] == pVulkanSurface->presentMode)
            {
                swapchainPresentMode = pVulkanSurface->presentMode;
                break;
            }
        }
    }
    if (swapchainPresentMode != pVulkanSurface->presentMode)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        // ERR_EXIT("Present mode specified is not supported\n", "Present mode unsupported");
        mmLogger_LogE(gLogger, "Present mode unsupported.");
        mmLogger_LogE(gLogger, "Present mode specified is not supportede.");
        return;
    }

    // Determine the number of VkImages to use in the swap chain.
    // Application desires to acquire 3 images at a time for triple
    // buffering
    uint32_t desiredNumOfSwapchainImages = 3;
    if (desiredNumOfSwapchainImages < surfCapabilities.minImageCount)
    {
        desiredNumOfSwapchainImages = surfCapabilities.minImageCount;
    }
    // If maxImageCount is 0, we can ask for as many images as we want;
    // otherwise we're limited to maxImageCount
    if ((surfCapabilities.maxImageCount > 0) && (desiredNumOfSwapchainImages > surfCapabilities.maxImageCount))
    {
        // Application must settle for fewer images than desired:
        desiredNumOfSwapchainImages = surfCapabilities.maxImageCount;
    }

    VkSurfaceTransformFlagsKHR preTransform;
    if (surfCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else
    {
        preTransform = surfCapabilities.currentTransform;
    }

    // Find a supported composite alpha mode - one of these is guaranteed to be set
    VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] =
    {
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
        VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
        VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
    };
    for (uint32_t i = 0; i < MM_ARRAY_SIZE(compositeAlphaFlags); i++)
    {
        if (surfCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i])
        {
            compositeAlpha = compositeAlphaFlags[i];
            break;
        }
    }

    VkSwapchainCreateInfoKHR swapchain_ci = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = NULL,
        .surface = pVulkanSurface->pSurface,
        .minImageCount = desiredNumOfSwapchainImages,
        .imageFormat = pVulkanSurface->hSurfaceFormat.format,
        .imageColorSpace = pVulkanSurface->hSurfaceFormat.colorSpace,
        .imageExtent =
            {
                .width = swapchainExtent.width,
                .height = swapchainExtent.height,
            },
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = preTransform,
        .compositeAlpha = compositeAlpha,
        .imageArrayLayers = 1,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL,
        .presentMode = swapchainPresentMode,
        .oldSwapchain = oldSwapchain,
        .clipped = VK_TRUE,
    };
    uint32_t i;
    err = pVulkanDevice->vkCreateSwapchainKHR(pVulkanDevice->device, &swapchain_ci, NULL, &pVulkanSurface->swapchain);
    assert(!err);

    // If we just re-created an existing swapchain, we should destroy the old
    // swapchain at this point.
    // Note: destroying the swapchain also cleans up all its associated
    // presentable images once the platform is done with them.
    if (oldSwapchain != VK_NULL_HANDLE)
    {
        pVulkanDevice->vkDestroySwapchainKHR(pVulkanDevice->device, oldSwapchain, NULL);
    }

    err = pVulkanDevice->vkGetSwapchainImagesKHR(pVulkanDevice->device, pVulkanSurface->swapchain, &pVulkanSurface->swapchainImageCount, NULL);
    assert(!err);

    VkImage *swapchainImages = (VkImage *)malloc(pVulkanSurface->swapchainImageCount * sizeof(VkImage));
    assert(swapchainImages);
    err = pVulkanDevice->vkGetSwapchainImagesKHR(pVulkanDevice->device, pVulkanSurface->swapchain, &pVulkanSurface->swapchainImageCount, swapchainImages);
    assert(!err);

    pVulkanSurface->swapchain_image_resources = (mmVKSwapchainImageResources*)malloc(sizeof(mmVKSwapchainImageResources) * pVulkanSurface->swapchainImageCount);
    assert(pVulkanSurface->swapchain_image_resources);

    for (i = 0; i < pVulkanSurface->swapchainImageCount; i++)
    {
        VkImageViewCreateInfo color_image_view =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = NULL,
            .format = pVulkanSurface->hSurfaceFormat.format,
            .components =
                {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY,
                },
            .subresourceRange =
                {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1,
                },
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .flags = 0,
        };

        pVulkanSurface->swapchain_image_resources[i].image = swapchainImages[i];

        color_image_view.image = pVulkanSurface->swapchain_image_resources[i].image;

        err = vkCreateImageView(pVulkanDevice->device, &color_image_view, NULL, &pVulkanSurface->swapchain_image_resources[i].view);
        assert(!err);
    }

    if (NULL != swapchainImages)
    {
        free(swapchainImages);
    }

    if (NULL != presentModes)
    {
        free(presentModes);
    }
}

void mmVKMakeInitCmd(struct mmVKAppData* pVulkanAppData, struct mmVKDevice* pVulkanDevice)
{
    VkResult err;
    if (pVulkanAppData->cmd_pool == VK_NULL_HANDLE)
    {
        const VkCommandPoolCreateInfo cmd_pool_info =
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .pNext = NULL,
            .queueFamilyIndex = pVulkanDevice->graphics_queue_family_index,
            .flags = 0,
        };
        err = vkCreateCommandPool(pVulkanDevice->device, &cmd_pool_info, NULL, &pVulkanAppData->cmd_pool);
        assert(!err);
    }

    const VkCommandBufferAllocateInfo cmd =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = pVulkanAppData->cmd_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1,
    };
    err = vkAllocateCommandBuffers(pVulkanDevice->device, &cmd, &pVulkanAppData->cmd);
    assert(!err);
    VkCommandBufferBeginInfo cmd_buf_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = 0,
        .pInheritanceInfo = NULL,
    };
    err = vkBeginCommandBuffer(pVulkanAppData->cmd, &cmd_buf_info);
    assert(!err);
}

static int mmVKMemoryTypeFromProperties(struct mmVKPhysicalDevice* pVulkanPhysicalDevice, uint32_t typeBits, VkFlags requirements_mask, uint32_t *typeIndex)
{
    uint32_t i = 0;
    // Search memtypes to find first index with those properties
    for (i = 0; i < VK_MAX_MEMORY_TYPES; i++)
    {
        if ((typeBits & 1) == 1)
        {
            // Type is available, does it match user properties?
            if ((pVulkanPhysicalDevice->hPhysicalDeviceMemoryProperties.memoryTypes[i].propertyFlags & requirements_mask) == requirements_mask)
            {
                *typeIndex = i;
                return VK_TRUE;
            }
        }
        typeBits >>= 1;
    }
    // No memory types matched, return failure
    return VK_FALSE;
}
void mmVKPrepareDepth(struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKSurface* pVulkanSurface, struct mmVKDevice* pVulkanDevice)
{
    const VkFormat depth_format = VK_FORMAT_D16_UNORM;
    const VkImageCreateInfo image =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = NULL,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = depth_format,
        .extent = {pVulkanSurface->width, pVulkanSurface->height, 1},
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .flags = 0,
    };

    VkImageViewCreateInfo view =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = NULL,
        .image = VK_NULL_HANDLE,
        .format = depth_format,
        .subresourceRange =
            {
                .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        .flags = 0,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
    };

    VkMemoryRequirements mem_reqs;
    VkResult err;
    int pass;

    pVulkanSurface->depth.format = depth_format;

    /* create image */
    err = vkCreateImage(pVulkanDevice->device, &image, NULL, &pVulkanSurface->depth.image);
    assert(!err);

    vkGetImageMemoryRequirements(pVulkanDevice->device, pVulkanSurface->depth.image, &mem_reqs);
    assert(!err);

    pVulkanSurface->depth.mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    pVulkanSurface->depth.mem_alloc.pNext = NULL;
    pVulkanSurface->depth.mem_alloc.allocationSize = mem_reqs.size;
    pVulkanSurface->depth.mem_alloc.memoryTypeIndex = 0;

    pass = mmVKMemoryTypeFromProperties(pVulkanPhysicalDevice, mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                       &pVulkanSurface->depth.mem_alloc.memoryTypeIndex);
    assert(pass);

    /* allocate memory */
    err = vkAllocateMemory(pVulkanDevice->device, &pVulkanSurface->depth.mem_alloc, NULL, &pVulkanSurface->depth.mem);
    assert(!err);

    /* bind memory */
    err = vkBindImageMemory(pVulkanDevice->device, pVulkanSurface->depth.image, pVulkanSurface->depth.mem, 0);
    assert(!err);

    /* create image view */
    view.image = pVulkanSurface->depth.image;
    err = vkCreateImageView(pVulkanDevice->device, &view, NULL, &pVulkanSurface->depth.view);
    assert(!err);
}

static char *tex_files[] = {"lunarg.ppm"};
/* Convert ppm image data from header file into RGBA texture image */
#include "vkcube/lunarg.ppm.h"
int mmVKLoadTexture(const char *filename, uint8_t *rgba_data, VkSubresourceLayout *layout, int32_t *width, int32_t *height)
{
    (void)filename;
    char *cPtr;
    cPtr = (char *)lunarg_ppm;
    if ((unsigned char *)cPtr >= (lunarg_ppm + lunarg_ppm_len) || strncmp(cPtr, "P6\n", 3))
    {
        return VK_FALSE;
    }
    while (strncmp(cPtr++, "\n", 1));
    sscanf(cPtr, "%u %u", width, height);
    if (rgba_data == NULL)
    {
        return VK_TRUE;
    }
    while (strncmp(cPtr++, "\n", 1));
    if ((unsigned char *)cPtr >= (lunarg_ppm + lunarg_ppm_len) || strncmp(cPtr, "255\n", 4))
    {
        return VK_FALSE;
    }
    while (strncmp(cPtr++, "\n", 1));
    for (int y = 0; y < *height; y++) {
        uint8_t *rowPtr = rgba_data;
        for (int x = 0; x < *width; x++) {
            memcpy(rowPtr, cPtr, 3);
            rowPtr[3] = 255; /* Alpha of 1 */
            rowPtr += 4;
            cPtr += 3;
        }
        rgba_data += layout->rowPitch;
    }
    return VK_TRUE;
}
void mmVKPrepareTextureImage(
    struct mmVKDevice* pVulkanDevice,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    const char *filename,
    struct mmVKTextureObject *tex_obj,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkFlags required_props)
{
    const VkFormat tex_format = VK_FORMAT_R8G8B8A8_UNORM;
    int32_t tex_width;
    int32_t tex_height;
    VkResult err;
    int pass;

    if (!mmVKLoadTexture(filename, NULL, NULL, &tex_width, &tex_height))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        // ERR_EXIT("Failed to load textures", "Load Texture Failure");
        mmLogger_LogE(gLogger, "Load Texture Failure.");
        mmLogger_LogE(gLogger, "Failed to load textures.");
        return;
    }

    tex_obj->tex_width = tex_width;
    tex_obj->tex_height = tex_height;

    const VkImageCreateInfo image_create_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = NULL,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = tex_format,
        .extent = {tex_width, tex_height, 1},
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = tiling,
        .usage = usage,
        .flags = 0,
        .initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED,
    };

    VkMemoryRequirements mem_reqs;

    err = vkCreateImage(pVulkanDevice->device, &image_create_info, NULL, &tex_obj->image);
    assert(!err);

    vkGetImageMemoryRequirements(pVulkanDevice->device, tex_obj->image, &mem_reqs);

    tex_obj->mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    tex_obj->mem_alloc.pNext = NULL;
    tex_obj->mem_alloc.allocationSize = mem_reqs.size;
    tex_obj->mem_alloc.memoryTypeIndex = 0;

    pass = mmVKMemoryTypeFromProperties(pVulkanPhysicalDevice, mem_reqs.memoryTypeBits, required_props, &tex_obj->mem_alloc.memoryTypeIndex);
    assert(pass);

    /* allocate memory */
    err = vkAllocateMemory(pVulkanDevice->device, &tex_obj->mem_alloc, NULL, &(tex_obj->mem));
    assert(!err);

    /* bind memory */
    err = vkBindImageMemory(pVulkanDevice->device, tex_obj->image, tex_obj->mem, 0);
    assert(!err);

    if (required_props & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
        const VkImageSubresource subres = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .arrayLayer = 0,
        };
        VkSubresourceLayout layout;
        void *data;

        vkGetImageSubresourceLayout(pVulkanDevice->device, tex_obj->image, &subres, &layout);

        err = vkMapMemory(pVulkanDevice->device, tex_obj->mem, 0, tex_obj->mem_alloc.allocationSize, 0, &data);
        assert(!err);

        if (!mmVKLoadTexture(filename, data, &layout, &tex_width, &tex_height))
        {
            fprintf(stderr, "Error loading texture: %s\n", filename);
        }

        vkUnmapMemory(pVulkanDevice->device, tex_obj->mem);
    }

    tex_obj->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
}
void mmVKSetImageLayoutApp(
    struct mmVKAppData* pVulkanAppData,
    VkImage image,
    VkImageAspectFlags aspectMask,
    VkImageLayout old_image_layout,
    VkImageLayout new_image_layout,
    VkAccessFlagBits srcAccessMask,
    VkPipelineStageFlags src_stages,
    VkPipelineStageFlags dest_stages)
{
    assert(pVulkanAppData->cmd);

    VkImageMemoryBarrier image_memory_barrier = {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                                                 .pNext = NULL,
                                                 .srcAccessMask = srcAccessMask,
                                                 .dstAccessMask = 0,
                                                 .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                                 .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                                 .oldLayout = old_image_layout,
                                                 .newLayout = new_image_layout,
                                                 .image = image,
                                                 .subresourceRange = {aspectMask, 0, 1, 0, 1}};

    switch (new_image_layout) {
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            /* Make sure anything that was copying from this image has completed */
            image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            image_memory_barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            image_memory_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
            image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
            break;

        default:
            image_memory_barrier.dstAccessMask = 0;
            break;
    }

    VkImageMemoryBarrier *pmemory_barrier = &image_memory_barrier;

    vkCmdPipelineBarrier(pVulkanAppData->cmd, src_stages, dest_stages, 0, 0, NULL, 0, NULL, 1, pmemory_barrier);
}
void mmVKPrepareTextureBuffer(
    struct mmVKDevice* pVulkanDevice,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    const char *filename,
    struct mmVKTextureObject *tex_obj)
{
    int32_t tex_width;
    int32_t tex_height;
    VkResult err;
    int pass;

    if (!mmVKLoadTexture(filename, NULL, NULL, &tex_width, &tex_height))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        // ERR_EXIT("Failed to load textures", "Load Texture Failure");
        mmLogger_LogE(gLogger, "Load Texture Failure.");
        mmLogger_LogE(gLogger, "Failed to load textures.");
        return;
    }

    tex_obj->tex_width = tex_width;
    tex_obj->tex_height = tex_height;

    const VkBufferCreateInfo buffer_create_info =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .size = tex_width * tex_height * 4,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL,
    };

    err = vkCreateBuffer(pVulkanDevice->device, &buffer_create_info, NULL, &tex_obj->buffer);
    assert(!err);

    VkMemoryRequirements mem_reqs;
    vkGetBufferMemoryRequirements(pVulkanDevice->device, tex_obj->buffer, &mem_reqs);

    tex_obj->mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    tex_obj->mem_alloc.pNext = NULL;
    tex_obj->mem_alloc.allocationSize = mem_reqs.size;
    tex_obj->mem_alloc.memoryTypeIndex = 0;

    VkFlags requirements = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    pass = mmVKMemoryTypeFromProperties(pVulkanPhysicalDevice, mem_reqs.memoryTypeBits, requirements, &tex_obj->mem_alloc.memoryTypeIndex);
    assert(pass);

    err = vkAllocateMemory(pVulkanDevice->device, &tex_obj->mem_alloc, NULL, &(tex_obj->mem));
    assert(!err);

    /* bind memory */
    err = vkBindBufferMemory(pVulkanDevice->device, tex_obj->buffer, tex_obj->mem, 0);
    assert(!err);

    VkSubresourceLayout layout;
    memset(&layout, 0, sizeof(layout));
    layout.rowPitch = tex_width * 4;

    void *data;
    err = vkMapMemory(pVulkanDevice->device, tex_obj->mem, 0, tex_obj->mem_alloc.allocationSize, 0, &data);
    assert(!err);

    if (!mmVKLoadTexture(filename, data, &layout, &tex_width, &tex_height))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        // ERR_EXIT("Failed to load textures", "Load Texture Failure");
        mmLogger_LogE(gLogger, "Error loading texture: %s", filename);
        return;
    }

    vkUnmapMemory(pVulkanDevice->device, tex_obj->mem);
}

//extern struct mmVKImage imageV;

void mmVKPrepareTextures(struct mmVKPhysicalDevice* pVulkanPhysicalDevice, struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    VkFormat tex_format = VK_FORMAT_R8G8B8A8_UNORM;
    VkImageViewType            viewType = VK_IMAGE_VIEW_TYPE_2D;
    VkFormatProperties props;
    uint32_t i;

    vkGetPhysicalDeviceFormatProperties(pVulkanPhysicalDevice->pPhysicalDevice, tex_format, &props);

    for (i = 0; i < VTD_TEXTURE_COUNT; i++)
    {
        VkResult err;
        
        //pVulkanAppData->textures[i].image = imageV.image;
        //pVulkanAppData->textures[i].imageLayout = imageV.imageLayout;
        //tex_format = imageV.imageFormat;
        //viewType = imageV.viewType;

        if ((props.linearTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT) && !pVulkanAppData->use_staging_buffer)
        {
            /* Device can texture using linear textures */
            mmVKPrepareTextureImage(pVulkanDevice,
                                        pVulkanPhysicalDevice,
                                        tex_files[i],
                                        &pVulkanAppData->textures[i],
                                        VK_IMAGE_TILING_LINEAR,
                                        VK_IMAGE_USAGE_SAMPLED_BIT,
                                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
            // Nothing in the pipeline needs to be complete to start, and don't allow fragment
            // shader to run until layout transition completes
            mmVKSetImageLayoutApp(
                pVulkanAppData,
                pVulkanAppData->textures[i].image,
                VK_IMAGE_ASPECT_COLOR_BIT,
                VK_IMAGE_LAYOUT_PREINITIALIZED,
                pVulkanAppData->textures[i].imageLayout,
                0,
                VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
            pVulkanAppData->staging_texture.image = 0;
        }
        else if (props.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT)
        {
            /* Must use staging buffer to copy linear texture to optimized */

            memset(&pVulkanAppData->staging_texture, 0, sizeof(pVulkanAppData->staging_texture));
            mmVKPrepareTextureBuffer(pVulkanDevice, pVulkanPhysicalDevice, tex_files[i], &pVulkanAppData->staging_texture);

            mmVKPrepareTextureImage(
                pVulkanDevice,
                pVulkanPhysicalDevice,
                tex_files[i],
                &pVulkanAppData->textures[i],
                VK_IMAGE_TILING_OPTIMAL,
                (VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

            mmVKSetImageLayoutApp(
                pVulkanAppData,
                pVulkanAppData->textures[i].image,
                VK_IMAGE_ASPECT_COLOR_BIT,
                VK_IMAGE_LAYOUT_PREINITIALIZED,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                0,
                VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT);

            VkBufferImageCopy copy_region =
            {
                .bufferOffset = 0,
                .bufferRowLength = pVulkanAppData->staging_texture.tex_width,
                .bufferImageHeight = pVulkanAppData->staging_texture.tex_height,
                .imageSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
                .imageOffset = {0, 0, 0},
                .imageExtent = {pVulkanAppData->staging_texture.tex_width, pVulkanAppData->staging_texture.tex_height, 1},
            };

            vkCmdCopyBufferToImage(
                pVulkanAppData->cmd,
                pVulkanAppData->staging_texture.buffer,
                pVulkanAppData->textures[i].image,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1,
                &copy_region);

            mmVKSetImageLayoutApp(
                pVulkanAppData,
                pVulkanAppData->textures[i].image,
                VK_IMAGE_ASPECT_COLOR_BIT,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                pVulkanAppData->textures[i].imageLayout,
                VK_ACCESS_TRANSFER_WRITE_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
        }
        else
        {
            /* Can't support VK_FORMAT_R8G8B8A8_UNORM !? */
            assert(!"No support for R8G8B8A8_UNORM as texture image format");
        }

        const VkSamplerCreateInfo sampler = {
            .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
            .pNext = NULL,
            .magFilter = VK_FILTER_NEAREST,
            .minFilter = VK_FILTER_NEAREST,
            .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
            .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
            .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
            .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
            .mipLodBias = 0.0f,
            .anisotropyEnable = VK_FALSE,
            .maxAnisotropy = 1,
            .compareOp = VK_COMPARE_OP_NEVER,
            .minLod = 0.0f,
            .maxLod = 0.0f,
            .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
            .unnormalizedCoordinates = VK_FALSE,
        };

        VkImageViewCreateInfo view = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = NULL,
            .image = VK_NULL_HANDLE,
            .viewType = viewType,
            .format = tex_format,
            .components =
                {
                    VK_COMPONENT_SWIZZLE_IDENTITY,
                    VK_COMPONENT_SWIZZLE_IDENTITY,
                    VK_COMPONENT_SWIZZLE_IDENTITY,
                    VK_COMPONENT_SWIZZLE_IDENTITY,
                },
            .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1},
            .flags = 0,
        };

        /* create sampler */
        err = vkCreateSampler(pVulkanDevice->device, &sampler, NULL, &pVulkanAppData->textures[i].sampler);
        assert(!err);

        /* create image view */
        view.image = pVulkanAppData->textures[i].image;
        err = vkCreateImageView(pVulkanDevice->device, &view, NULL, &pVulkanAppData->textures[i].view);
        assert(!err);
    }
}

struct mmVKTexCubeVsUniform
{
    // Must start with MVP
    float mvp[4][4];
    float position[12 * 3][4];
    float attr[12 * 3][4];
};

//--------------------------------------------------------------------------------------
// Mesh and VertexFormat Data
//--------------------------------------------------------------------------------------
// clang-format off
static const float g_vertex_buffer_data[] =
{
    -1.0f,-1.0f,-1.0f,  // -X side
    -1.0f,-1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,

    -1.0f,-1.0f,-1.0f,  // -Z side
     1.0f, 1.0f,-1.0f,
     1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,

    -1.0f,-1.0f,-1.0f,  // -Y side
     1.0f,-1.0f,-1.0f,
     1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
     1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,

    -1.0f, 1.0f,-1.0f,  // +Y side
    -1.0f, 1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
     1.0f, 1.0f, 1.0f,
     1.0f, 1.0f,-1.0f,

     1.0f, 1.0f,-1.0f,  // +X side
     1.0f, 1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f,-1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,

    -1.0f, 1.0f, 1.0f,  // +Z side
    -1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
};

static const float g_uv_buffer_data[] =
{
    0.0f, 1.0f,  // -X side
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,

    1.0f, 1.0f,  // -Z side
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,

    1.0f, 0.0f,  // -Y side
    1.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,

    1.0f, 0.0f,  // +Y side
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,

    1.0f, 0.0f,  // +X side
    0.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,

    0.0f, 0.0f,  // +Z side
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
};
// clang-format on


void mmVKPrepareCubeDataBuffers(
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKAppData* pVulkanAppData,
    struct mmVKSurface* pVulkanSurface)
{
    VkBufferCreateInfo buf_info;
    VkMemoryRequirements mem_reqs;
    VkMemoryAllocateInfo mem_alloc;
    mat4x4 MVP, VP;
    VkResult err;
    int pass;
    struct mmVKTexCubeVsUniform data;

    mat4x4_mul(VP, pVulkanAppData->projection_matrix, pVulkanAppData->view_matrix);
    mat4x4_mul(MVP, VP, pVulkanAppData->model_matrix);
    memcpy(data.mvp, MVP, sizeof(MVP));
    // dumpMatrix("MVP", MVP);

    for (unsigned int i = 0; i < 12 * 3; i++)
    {
        data.position[i][0] = g_vertex_buffer_data[i * 3];
        data.position[i][1] = g_vertex_buffer_data[i * 3 + 1];
        data.position[i][2] = g_vertex_buffer_data[i * 3 + 2];
        data.position[i][3] = 1.0f;
        data.attr[i][0] = g_uv_buffer_data[2 * i];
        data.attr[i][1] = g_uv_buffer_data[2 * i + 1];
        data.attr[i][2] = 0;
        data.attr[i][3] = 0;
    }

    memset(&buf_info, 0, sizeof(buf_info));
    buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buf_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    buf_info.size = sizeof(data);

    for (unsigned int i = 0; i < pVulkanSurface->swapchainImageCount; i++)
    {
        err = vkCreateBuffer(pVulkanDevice->device, &buf_info, NULL, &pVulkanSurface->swapchain_image_resources[i].uniform_buffer);
        assert(!err);

        vkGetBufferMemoryRequirements(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_buffer, &mem_reqs);

        mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        mem_alloc.pNext = NULL;
        mem_alloc.allocationSize = mem_reqs.size;
        mem_alloc.memoryTypeIndex = 0;

        pass = mmVKMemoryTypeFromProperties(
            pVulkanPhysicalDevice,
            mem_reqs.memoryTypeBits,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            &mem_alloc.memoryTypeIndex);
        assert(pass);

        err = vkAllocateMemory(pVulkanDevice->device, &mem_alloc, NULL, &pVulkanSurface->swapchain_image_resources[i].uniform_memory);
        assert(!err);

        err = vkMapMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_memory, 0, VK_WHOLE_SIZE, 0,
                          &pVulkanSurface->swapchain_image_resources[i].uniform_memory_ptr);
        assert(!err);

        memcpy(pVulkanSurface->swapchain_image_resources[i].uniform_memory_ptr, &data, sizeof data);

        err = vkBindBufferMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_buffer,
                                 pVulkanSurface->swapchain_image_resources[i].uniform_memory, 0);
        assert(!err);
    }
}

void mmVKPrepareDescriptorLayout(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    const VkDescriptorSetLayoutBinding layout_bindings[2] = {
        [0] =
            {
                .binding = 0,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
                .pImmutableSamplers = NULL,
            },
        [1] =
            {
                .binding = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = VTD_TEXTURE_COUNT,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = NULL,
            },
    };
    const VkDescriptorSetLayoutCreateInfo descriptor_layout = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .bindingCount = 2,
        .pBindings = layout_bindings,
    };
    VkResult err;

    err = vkCreateDescriptorSetLayout(pVulkanDevice->device, &descriptor_layout, NULL, &pVulkanAppData->desc_layout);
    assert(!err);

    const VkPipelineLayoutCreateInfo pPipelineLayoutCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .setLayoutCount = 1,
        .pSetLayouts = &pVulkanAppData->desc_layout,
    };

    err = vkCreatePipelineLayout(pVulkanDevice->device, &pPipelineLayoutCreateInfo, NULL, &pVulkanAppData->pipeline_layout);
    assert(!err);
}

void mmVKPrepareRenderPass(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    // The initial layout for the color and depth attachments will be LAYOUT_UNDEFINED
    // because at the start of the renderpass, we don't care about their contents.
    // At the start of the subpass, the color attachment's layout will be transitioned
    // to LAYOUT_COLOR_ATTACHMENT_OPTIMAL and the depth stencil attachment's layout
    // will be transitioned to LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL.  At the end of
    // the renderpass, the color attachment's layout will be transitioned to
    // LAYOUT_PRESENT_SRC_KHR to be ready to present.  This is all done as part of
    // the renderpass, no barriers are necessary.
    const VkAttachmentDescription attachments[2] = {
        [0] =
            {
                .format = pVulkanSurface->hSurfaceFormat.format,
                .flags = 0,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            },
        [1] =
            {
                .format = pVulkanSurface->depth.format,
                .flags = 0,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            },
    };
    const VkAttachmentReference color_reference = {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };
    const VkAttachmentReference depth_reference = {
        .attachment = 1,
        .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };
    const VkSubpassDescription subpass = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .flags = 0,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_reference,
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &depth_reference,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL,
    };

    VkSubpassDependency attachmentDependencies[2] = {
        [0] =
            {
                // Depth buffer is shared between swapchain images
                .srcSubpass = VK_SUBPASS_EXTERNAL,
                .dstSubpass = 0,
                .srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                .srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                .dependencyFlags = 0,
            },
        [1] =
            {
                // Image Layout Transition
                .srcSubpass = VK_SUBPASS_EXTERNAL,
                .dstSubpass = 0,
                .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .srcAccessMask = 0,
                .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
                .dependencyFlags = 0,
            },
    };

    const VkRenderPassCreateInfo rp_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 2,
        .pAttachments = attachments,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 2,
        .pDependencies = attachmentDependencies,
    };
    VkResult err;

    err = vkCreateRenderPass(pVulkanDevice->device, &rp_info, NULL, &pVulkanAppData->render_pass);
    assert(!err);
}

static VkShaderModule mmVKPrepareShaderModule(struct mmVKDevice* pVulkanDevice, const uint32_t *code, size_t size)
{
    VkShaderModule module;
    VkShaderModuleCreateInfo moduleCreateInfo;
    VkResult err;

    moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    moduleCreateInfo.pNext = NULL;
    moduleCreateInfo.flags = 0;
    moduleCreateInfo.codeSize = size;
    moduleCreateInfo.pCode = code;

    err = vkCreateShaderModule(pVulkanDevice->device, &moduleCreateInfo, NULL, &module);
    assert(!err);

    return module;
}

static void mmVKPrepareVS(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    const uint32_t vs_code[] =
    {
#include "vkcube/cube.vert.inc"
    };
    pVulkanAppData->vert_shader_module = mmVKPrepareShaderModule(pVulkanDevice, vs_code, sizeof(vs_code));
}

static void mmVKPrepareFS(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    const uint32_t fs_code[] =
    {
#include "vkcube/cube.frag.inc"
    };
    pVulkanAppData->frag_shader_module = mmVKPrepareShaderModule(pVulkanDevice, fs_code, sizeof(fs_code));
}

void mmVKPreparePipeline(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
#define NUM_DYNAMIC_STATES 2 /*Viewport + Scissor*/

    VkGraphicsPipelineCreateInfo pipeline;
    VkPipelineCacheCreateInfo pipelineCache;
    VkPipelineVertexInputStateCreateInfo vi;
    VkPipelineInputAssemblyStateCreateInfo ia;
    VkPipelineRasterizationStateCreateInfo rs;
    VkPipelineColorBlendStateCreateInfo cb;
    VkPipelineDepthStencilStateCreateInfo ds;
    VkPipelineViewportStateCreateInfo vp;
    VkPipelineMultisampleStateCreateInfo ms;
    VkDynamicState dynamicStateEnables[NUM_DYNAMIC_STATES];
    VkPipelineDynamicStateCreateInfo dynamicState;
    VkResult err;

    memset(dynamicStateEnables, 0, sizeof dynamicStateEnables);
    memset(&dynamicState, 0, sizeof dynamicState);
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.pDynamicStates = dynamicStateEnables;

    memset(&pipeline, 0, sizeof(pipeline));
    pipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline.layout = pVulkanAppData->pipeline_layout;

    memset(&vi, 0, sizeof(vi));
    vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    memset(&ia, 0, sizeof(ia));
    ia.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    ia.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    memset(&rs, 0, sizeof(rs));
    rs.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rs.polygonMode = VK_POLYGON_MODE_FILL;
    rs.cullMode = VK_CULL_MODE_BACK_BIT;
    rs.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rs.depthClampEnable = VK_FALSE;
    rs.rasterizerDiscardEnable = VK_FALSE;
    rs.depthBiasEnable = VK_FALSE;
    rs.lineWidth = 1.0f;

    memset(&cb, 0, sizeof(cb));
    cb.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    VkPipelineColorBlendAttachmentState att_state[1];
    memset(att_state, 0, sizeof(att_state));
    att_state[0].colorWriteMask = 0xf;
    att_state[0].blendEnable = VK_FALSE;
    cb.attachmentCount = 1;
    cb.pAttachments = att_state;

    memset(&vp, 0, sizeof(vp));
    vp.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    vp.viewportCount = 1;
    dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
    vp.scissorCount = 1;
    dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;

    memset(&ds, 0, sizeof(ds));
    ds.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ds.depthTestEnable = VK_TRUE;
    ds.depthWriteEnable = VK_TRUE;
    ds.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    ds.depthBoundsTestEnable = VK_FALSE;
    ds.back.failOp = VK_STENCIL_OP_KEEP;
    ds.back.passOp = VK_STENCIL_OP_KEEP;
    ds.back.compareOp = VK_COMPARE_OP_ALWAYS;
    ds.stencilTestEnable = VK_FALSE;
    ds.front = ds.back;

    memset(&ms, 0, sizeof(ms));
    ms.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    ms.pSampleMask = NULL;
    ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    mmVKPrepareVS(pVulkanDevice, pVulkanAppData);
    mmVKPrepareFS(pVulkanDevice, pVulkanAppData);

    // Two stages: vs and fs
    VkPipelineShaderStageCreateInfo shaderStages[2];
    memset(&shaderStages, 0, 2 * sizeof(VkPipelineShaderStageCreateInfo));

    shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    shaderStages[0].module = pVulkanAppData->vert_shader_module;
    shaderStages[0].pName = "main";

    shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shaderStages[1].module = pVulkanAppData->frag_shader_module;
    shaderStages[1].pName = "main";

    memset(&pipelineCache, 0, sizeof(pipelineCache));
    pipelineCache.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

    err = vkCreatePipelineCache(pVulkanDevice->device, &pipelineCache, NULL, &pVulkanAppData->pipelineCache);
    assert(!err);

    pipeline.pVertexInputState = &vi;
    pipeline.pInputAssemblyState = &ia;
    pipeline.pRasterizationState = &rs;
    pipeline.pColorBlendState = &cb;
    pipeline.pMultisampleState = &ms;
    pipeline.pViewportState = &vp;
    pipeline.pDepthStencilState = &ds;
    pipeline.stageCount = MM_ARRAY_SIZE(shaderStages);
    pipeline.pStages = shaderStages;
    pipeline.renderPass = pVulkanAppData->render_pass;
    pipeline.pDynamicState = &dynamicState;

    pipeline.renderPass = pVulkanAppData->render_pass;

    err = vkCreateGraphicsPipelines(pVulkanDevice->device, pVulkanAppData->pipelineCache, 1, &pipeline, NULL, &pVulkanAppData->pipeline);
    assert(!err);

    vkDestroyShaderModule(pVulkanDevice->device, pVulkanAppData->frag_shader_module, NULL);
    vkDestroyShaderModule(pVulkanDevice->device, pVulkanAppData->vert_shader_module, NULL);
}

void mmVKBuildImageOwnershipCmd(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, int i)
{
    VkResult err;

    const VkCommandBufferBeginInfo cmd_buf_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
        .pInheritanceInfo = NULL,
    };
    err = vkBeginCommandBuffer(pVulkanSurface->swapchain_image_resources[i].graphics_to_present_cmd, &cmd_buf_info);
    assert(!err);

    VkImageMemoryBarrier image_ownership_barrier = {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                                                    .pNext = NULL,
                                                    .srcAccessMask = 0,
                                                    .dstAccessMask = 0,
                                                    .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                                    .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                                    .srcQueueFamilyIndex = pVulkanDevice->graphics_queue_family_index,
                                                    .dstQueueFamilyIndex = pVulkanDevice->present_queue_family_index,
                                                    .image = pVulkanSurface->swapchain_image_resources[i].image,
                                                    .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};

    vkCmdPipelineBarrier(pVulkanSurface->swapchain_image_resources[i].graphics_to_present_cmd, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1, &image_ownership_barrier);
    err = vkEndCommandBuffer(pVulkanSurface->swapchain_image_resources[i].graphics_to_present_cmd);
    assert(!err);
}
void mmVKPrepareSwapchainCmd(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    VkResult err;
    
    const VkCommandBufferAllocateInfo cmd = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = pVulkanAppData->cmd_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1,
    };
    
    for (uint32_t i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
        err = vkAllocateCommandBuffers(pVulkanDevice->device, &cmd, &pVulkanSurface->swapchain_image_resources[i].cmd);
        assert(!err);
    }

    if (pVulkanDevice->separate_present_queue) {
        const VkCommandPoolCreateInfo present_cmd_pool_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .pNext = NULL,
            .queueFamilyIndex = pVulkanDevice->present_queue_family_index,
            .flags = 0,
        };
        err = vkCreateCommandPool(pVulkanDevice->device, &present_cmd_pool_info, NULL, &pVulkanAppData->present_cmd_pool);
        assert(!err);
        const VkCommandBufferAllocateInfo present_cmd_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .pNext = NULL,
            .commandPool = pVulkanAppData->present_cmd_pool,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = 1,
        };
        for (uint32_t i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
            err = vkAllocateCommandBuffers(pVulkanDevice->device, &present_cmd_info,
                                           &pVulkanSurface->swapchain_image_resources[i].graphics_to_present_cmd);
            assert(!err);
            mmVKBuildImageOwnershipCmd(pVulkanDevice, pVulkanSurface, i);
        }
    }
}
void mmVKPrepareDescriptorPool(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    const VkDescriptorPoolSize type_counts[2] = {
        [0] =
            {
                .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = pVulkanSurface->swapchainImageCount,
            },
        [1] =
            {
                .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = pVulkanSurface->swapchainImageCount * VTD_TEXTURE_COUNT,
            },
    };
    const VkDescriptorPoolCreateInfo descriptor_pool = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = NULL,
        .maxSets = pVulkanSurface->swapchainImageCount,
        .poolSizeCount = 2,
        .pPoolSizes = type_counts,
    };
    VkResult err;

    err = vkCreateDescriptorPool(pVulkanDevice->device, &descriptor_pool, NULL, &pVulkanAppData->desc_pool);
    assert(!err);
}

void mmVKPrepareDescriptorSet(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    VkDescriptorImageInfo tex_descs[VTD_TEXTURE_COUNT];
    VkWriteDescriptorSet writes[2];
    VkResult err;

    VkDescriptorSetAllocateInfo alloc_info = {.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                                              .pNext = NULL,
                                              .descriptorPool = pVulkanAppData->desc_pool,
                                              .descriptorSetCount = 1,
                                              .pSetLayouts = &pVulkanAppData->desc_layout};

    VkDescriptorBufferInfo buffer_info;
    buffer_info.offset = 0;
    buffer_info.range = sizeof(struct mmVKTexCubeVsUniform);

    memset(&tex_descs, 0, sizeof(tex_descs));
    for (unsigned int i = 0; i < VTD_TEXTURE_COUNT; i++) {
        tex_descs[i].sampler = pVulkanAppData->textures[i].sampler;
        tex_descs[i].imageView = pVulkanAppData->textures[i].view;
        tex_descs[i].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    memset(&writes, 0, sizeof(writes));

    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writes[0].pBufferInfo = &buffer_info;

    writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[1].dstBinding = 1;
    writes[1].descriptorCount = VTD_TEXTURE_COUNT;
    writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[1].pImageInfo = tex_descs;

    for (unsigned int i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
        err = vkAllocateDescriptorSets(pVulkanDevice->device, &alloc_info, &pVulkanSurface->swapchain_image_resources[i].descriptor_set);
        assert(!err);
        buffer_info.buffer = pVulkanSurface->swapchain_image_resources[i].uniform_buffer;
        writes[0].dstSet = pVulkanSurface->swapchain_image_resources[i].descriptor_set;
        writes[1].dstSet = pVulkanSurface->swapchain_image_resources[i].descriptor_set;
        vkUpdateDescriptorSets(pVulkanDevice->device, 2, writes, 0, NULL);
    }
}

void mmVKPrepareFramebuffers(struct mmVKDevice* pVulkanDevice, struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    VkImageView attachments[2];
    attachments[1] = pVulkanSurface->depth.view;

    const VkFramebufferCreateInfo fb_info = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .renderPass = pVulkanAppData->render_pass,
        .attachmentCount = 2,
        .pAttachments = attachments,
        .width = pVulkanSurface->width,
        .height = pVulkanSurface->height,
        .layers = 1,
    };
    VkResult err;
    uint32_t i;

    for (i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
        attachments[0] = pVulkanSurface->swapchain_image_resources[i].view;
        err = vkCreateFramebuffer(pVulkanDevice->device, &fb_info, NULL, &pVulkanSurface->swapchain_image_resources[i].framebuffer);
        assert(!err);
    }
}

static void mmVKDrawBuildCmd(
    struct mmVK* pVulkan,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData,
    VkCommandBuffer cmd_buf)
{
    VkDebugUtilsLabelEXT label;
    memset(&label, 0, sizeof(label));
    const VkCommandBufferBeginInfo cmd_buf_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
        .pInheritanceInfo = NULL,
    };
    const VkClearValue clear_values[2] = {
        [0] = {.color.float32 = {0.2f, 0.2f, 0.2f, 0.2f}},
        [1] = {.depthStencil = {1.0f, 0}},
    };
    const VkRenderPassBeginInfo rp_begin = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = NULL,
        .renderPass = pVulkanAppData->render_pass,
        .framebuffer = pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].framebuffer,
        .renderArea.offset.x = 0,
        .renderArea.offset.y = 0,
        .renderArea.extent.width = pVulkanSurface->width,
        .renderArea.extent.height = pVulkanSurface->height,
        .clearValueCount = 2,
        .pClearValues = clear_values,
    };
    VkResult err;

    err = vkBeginCommandBuffer(cmd_buf, &cmd_buf_info);

    if (pVulkan->hValidationLayer) {
        // Set a name for the command buffer
        VkDebugUtilsObjectNameInfoEXT cmd_buf_name = {
            .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
            .pNext = NULL,
            .objectType = VK_OBJECT_TYPE_COMMAND_BUFFER,
            .objectHandle = (uint64_t)cmd_buf,
            .pObjectName = "CubeDrawCommandBuf",
        };
        pVulkan->vkSetDebugUtilsObjectNameEXT(pVulkanDevice->device, &cmd_buf_name);

        label.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
        label.pNext = NULL;
        label.pLabelName = "DrawBegin";
        label.color[0] = 0.4f;
        label.color[1] = 0.3f;
        label.color[2] = 0.2f;
        label.color[3] = 0.1f;
        pVulkan->vkCmdBeginDebugUtilsLabelEXT(cmd_buf, &label);
    }

    assert(!err);
    vkCmdBeginRenderPass(cmd_buf, &rp_begin, VK_SUBPASS_CONTENTS_INLINE);

    if (pVulkan->hValidationLayer) {
        label.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
        label.pNext = NULL;
        label.pLabelName = "InsideRenderPass";
        label.color[0] = 8.4f;
        label.color[1] = 7.3f;
        label.color[2] = 6.2f;
        label.color[3] = 7.1f;
        pVulkan->vkCmdBeginDebugUtilsLabelEXT(cmd_buf, &label);
    }

    vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pVulkanAppData->pipeline);
    vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pVulkanAppData->pipeline_layout, 0, 1,
                            &pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].descriptor_set, 0, NULL);
    VkViewport viewport;
    memset(&viewport, 0, sizeof(viewport));
    float viewport_dimension;
    if (pVulkanSurface->width < pVulkanSurface->height) {
        viewport_dimension = (float)pVulkanSurface->width;
        viewport.y = (pVulkanSurface->height - pVulkanSurface->width) / 2.0f;
    } else {
        viewport_dimension = (float)pVulkanSurface->height;
        viewport.x = (pVulkanSurface->width - pVulkanSurface->height) / 2.0f;
    }
    viewport.height = viewport_dimension;
    viewport.width = viewport_dimension;
    viewport.minDepth = (float)0.0f;
    viewport.maxDepth = (float)1.0f;
    vkCmdSetViewport(cmd_buf, 0, 1, &viewport);

    VkRect2D scissor;
    memset(&scissor, 0, sizeof(scissor));
    scissor.extent.width = pVulkanSurface->width;
    scissor.extent.height = pVulkanSurface->height;
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    vkCmdSetScissor(cmd_buf, 0, 1, &scissor);

    if (pVulkan->hValidationLayer) {
        label.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
        label.pNext = NULL;
        label.pLabelName = "ActualDraw";
        label.color[0] = -0.4f;
        label.color[1] = -0.3f;
        label.color[2] = -0.2f;
        label.color[3] = -0.1f;
        pVulkan->vkCmdBeginDebugUtilsLabelEXT(cmd_buf, &label);
    }

    vkCmdDraw(cmd_buf, 12 * 3, 1, 0, 0);
    if (pVulkan->hValidationLayer) {
        pVulkan->vkCmdEndDebugUtilsLabelEXT(cmd_buf);
    }

    // Note that ending the renderpass changes the image's layout from
    // COLOR_ATTACHMENT_OPTIMAL to PRESENT_SRC_KHR
    vkCmdEndRenderPass(cmd_buf);
    if (pVulkan->hValidationLayer) {
        pVulkan->vkCmdEndDebugUtilsLabelEXT(cmd_buf);
    }

    if (pVulkanDevice->separate_present_queue) {
        // We have to transfer ownership from the graphics queue family to the
        // present queue family to be able to present.  Note that we don't have
        // to transfer from present queue family back to graphics queue family at
        // the start of the next frame because we don't care about the image's
        // contents at that point.
        VkImageMemoryBarrier image_ownership_barrier = {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                                                        .pNext = NULL,
                                                        .srcAccessMask = 0,
                                                        .dstAccessMask = 0,
                                                        .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                                        .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                                        .srcQueueFamilyIndex = pVulkanDevice->graphics_queue_family_index,
                                                        .dstQueueFamilyIndex = pVulkanDevice->present_queue_family_index,
                                                        .image = pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].image,
                                                        .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};

        vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0,
                             NULL, 1, &image_ownership_barrier);
    }
    if (pVulkan->hValidationLayer) {
        pVulkan->vkCmdEndDebugUtilsLabelEXT(cmd_buf);
    }
    err = vkEndCommandBuffer(cmd_buf);
    assert(!err);
}
void mmVKSwapchainDrawBuildCmd(
    struct mmVK* pVulkan,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData)
{
    for (uint32_t i = 0; i < pVulkanSurface->swapchainImageCount; i++)
    {
        pVulkanAppData->current_buffer = i;
        mmVKDrawBuildCmd(pVulkan, pVulkanDevice, pVulkanSurface, pVulkanAppData, pVulkanSurface->swapchain_image_resources[i].cmd);
    }
}

static void mmVKFlushInitCmd(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    VkResult err;

    // This function could get called twice if the texture uses a staging buffer
    // In that case the second call should be ignored
    if (pVulkanAppData->cmd == VK_NULL_HANDLE) return;

    err = vkEndCommandBuffer(pVulkanAppData->cmd);
    assert(!err);

    VkFence fence;
    VkFenceCreateInfo fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = NULL, .flags = 0};
    err = vkCreateFence(pVulkanDevice->device, &fence_ci, NULL, &fence);
    assert(!err);

    const VkCommandBuffer cmd_bufs[] = {pVulkanAppData->cmd};
    VkSubmitInfo submit_info = {.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                .pNext = NULL,
                                .waitSemaphoreCount = 0,
                                .pWaitSemaphores = NULL,
                                .pWaitDstStageMask = NULL,
                                .commandBufferCount = 1,
                                .pCommandBuffers = cmd_bufs,
                                .signalSemaphoreCount = 0,
                                .pSignalSemaphores = NULL};

    err = vkQueueSubmit(pVulkanDevice->graphics_queue, 1, &submit_info, fence);
    assert(!err);

    err = vkWaitForFences(pVulkanDevice->device, 1, &fence, VK_TRUE, UINT64_MAX);
    assert(!err);

    vkFreeCommandBuffers(pVulkanDevice->device, pVulkanAppData->cmd_pool, 1, cmd_bufs);
    vkDestroyFence(pVulkanDevice->device, fence, NULL);
    pVulkanAppData->cmd = VK_NULL_HANDLE;
}
static void mmVKDestroyTexture(struct mmVKDevice* pVulkanDevice, struct mmVKTextureObject *tex_objs)
{
    /* clean up staging resources */
    vkFreeMemory(pVulkanDevice->device, tex_objs->mem, NULL);
    if (tex_objs->image) vkDestroyImage(pVulkanDevice->device, tex_objs->image, NULL);
    if (tex_objs->buffer) vkDestroyBuffer(pVulkanDevice->device, tex_objs->buffer, NULL);
}
void mmVKFlushInitCmdDestroyStagingTexture(struct mmVKDevice* pVulkanDevice, struct mmVKAppData* pVulkanAppData)
{
    /*
     * Prepare functions above may generate pipeline commands
     * that need to be flushed before beginning the render loop.
     */
    mmVKFlushInitCmd(pVulkanDevice, pVulkanAppData);
    if (pVulkanAppData->staging_texture.buffer) {
        mmVKDestroyTexture(pVulkanDevice, &pVulkanAppData->staging_texture);
    }

    pVulkanAppData->current_buffer = 0;
    pVulkanAppData->prepared = VK_TRUE;
}

void mmVKPrepare(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData)
{
    mmVKMakeInitCmd(pVulkanAppData, pVulkanDevice);
    pVulkanSurface->presentMode = VK_PRESENT_MODE_FIFO_KHR;
    mmVKSwapchain(pVulkan, pVulkanDevice, pVulkanPhysicalDevice, pVulkanSurface);
    if (pVulkanSurface->is_minimized)
    {
        pVulkanAppData->prepared = VK_FALSE;
    }
    else
    {
        mmVKPrepareDepth(pVulkanPhysicalDevice, pVulkanSurface, pVulkanDevice);
        pVulkanAppData->use_staging_buffer = 1;
        mmVKPrepareTextures(pVulkanPhysicalDevice, pVulkanDevice, pVulkanAppData);
        mmVKPrepareCubeDataBuffers(pVulkanPhysicalDevice, pVulkanDevice, pVulkanAppData, pVulkanSurface);
        mmVKPrepareDescriptorLayout(pVulkanDevice, pVulkanAppData);
        mmVKPrepareRenderPass(pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKPreparePipeline(pVulkanDevice, pVulkanAppData);
        mmVKPrepareSwapchainCmd(pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKPrepareDescriptorPool(pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKPrepareDescriptorSet(pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKPrepareFramebuffers(pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKSwapchainDrawBuildCmd(pVulkan, pVulkanDevice, pVulkanSurface, pVulkanAppData);
        mmVKFlushInitCmdDestroyStagingTexture(pVulkanDevice, pVulkanAppData);
    }
}

void mmVKResize(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData)
{
    uint32_t i;

    // Don't react to resize until after first initialization.
    if (!pVulkanAppData->prepared)
    {
        if (pVulkanSurface->is_minimized)
        {
            mmVKPrepare(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
        }
        return;
    }
    else
    {
        // In order to properly resize the window, we must re-create the swapchain
        // AND redo the command buffers, etc.
        //
        // First, perform part of the demo_cleanup() function:
        pVulkanAppData->prepared = VK_FALSE;
        vkDeviceWaitIdle(pVulkanDevice->device);

        for (i = 0; i < pVulkanSurface->swapchainImageCount; i++)
        {
            vkDestroyFramebuffer(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].framebuffer, NULL);
        }
        vkDestroyDescriptorPool(pVulkanDevice->device, pVulkanAppData->desc_pool, NULL);

        vkDestroyPipeline(pVulkanDevice->device, pVulkanAppData->pipeline, NULL);
        vkDestroyPipelineCache(pVulkanDevice->device, pVulkanAppData->pipelineCache, NULL);
        vkDestroyRenderPass(pVulkanDevice->device, pVulkanAppData->render_pass, NULL);
        vkDestroyPipelineLayout(pVulkanDevice->device, pVulkanAppData->pipeline_layout, NULL);
        vkDestroyDescriptorSetLayout(pVulkanDevice->device, pVulkanAppData->desc_layout, NULL);

        for (i = 0; i < VTD_TEXTURE_COUNT; i++)
        {
            vkDestroyImageView(pVulkanDevice->device, pVulkanAppData->textures[i].view, NULL);
            vkDestroyImage(pVulkanDevice->device, pVulkanAppData->textures[i].image, NULL);
            vkFreeMemory(pVulkanDevice->device, pVulkanAppData->textures[i].mem, NULL);
            vkDestroySampler(pVulkanDevice->device, pVulkanAppData->textures[i].sampler, NULL);
        }

        vkDestroyImageView(pVulkanDevice->device, pVulkanSurface->depth.view, NULL);
        vkDestroyImage(pVulkanDevice->device, pVulkanSurface->depth.image, NULL);
        vkFreeMemory(pVulkanDevice->device, pVulkanSurface->depth.mem, NULL);

        for (i = 0; i < pVulkanSurface->swapchainImageCount; i++)
        {
            vkDestroyImageView(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].view, NULL);
            vkFreeCommandBuffers(pVulkanDevice->device, pVulkanAppData->cmd_pool, 1, &pVulkanSurface->swapchain_image_resources[i].cmd);
            vkDestroyBuffer(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_buffer, NULL);
            vkUnmapMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_memory);
            vkFreeMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_memory, NULL);
        }
        vkDestroyCommandPool(pVulkanDevice->device, pVulkanAppData->cmd_pool, NULL);
        pVulkanAppData->cmd_pool = VK_NULL_HANDLE;
        if (pVulkanDevice->separate_present_queue) {
            vkDestroyCommandPool(pVulkanDevice->device, pVulkanAppData->present_cmd_pool, NULL);
        }
        free(pVulkanSurface->swapchain_image_resources);

        // Second, re-perform the demo_prepare() function, which will re-create the
        // swapchain:
        mmVKPrepare(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
    }
}

void mmVKCleanup(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData)
{
    uint32_t i;

    pVulkanAppData->prepared = VK_FALSE;
    vkDeviceWaitIdle(pVulkanDevice->device);

    // Wait for fences from present operations
    for (i = 0; i < FRAME_LAG; i++) {
        vkWaitForFences(pVulkanDevice->device, 1, &pVulkanDevice->fences[i], VK_TRUE, UINT64_MAX);
        vkDestroyFence(pVulkanDevice->device, pVulkanDevice->fences[i], NULL);
        vkDestroySemaphore(pVulkanDevice->device, pVulkanDevice->image_acquired_semaphores[i], NULL);
        vkDestroySemaphore(pVulkanDevice->device, pVulkanDevice->draw_complete_semaphores[i], NULL);
        if (pVulkanDevice->separate_present_queue) {
            vkDestroySemaphore(pVulkanDevice->device, pVulkanDevice->image_ownership_semaphores[i], NULL);
        }
    }

    // If the window is currently minimized, demo_resize has already done some cleanup for us.
    if (!pVulkanSurface->is_minimized) {
        for (i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
            vkDestroyFramebuffer(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].framebuffer, NULL);
        }
        vkDestroyDescriptorPool(pVulkanDevice->device, pVulkanAppData->desc_pool, NULL);

        vkDestroyPipeline(pVulkanDevice->device, pVulkanAppData->pipeline, NULL);
        vkDestroyPipelineCache(pVulkanDevice->device, pVulkanAppData->pipelineCache, NULL);
        vkDestroyRenderPass(pVulkanDevice->device, pVulkanAppData->render_pass, NULL);
        vkDestroyPipelineLayout(pVulkanDevice->device, pVulkanAppData->pipeline_layout, NULL);
        vkDestroyDescriptorSetLayout(pVulkanDevice->device, pVulkanAppData->desc_layout, NULL);

        for (i = 0; i < VTD_TEXTURE_COUNT; i++) {
            vkDestroyImageView(pVulkanDevice->device, pVulkanAppData->textures[i].view, NULL);
            vkDestroyImage(pVulkanDevice->device, pVulkanAppData->textures[i].image, NULL);
            vkFreeMemory(pVulkanDevice->device, pVulkanAppData->textures[i].mem, NULL);
            vkDestroySampler(pVulkanDevice->device, pVulkanAppData->textures[i].sampler, NULL);
        }
        pVulkanDevice->vkDestroySwapchainKHR(pVulkanDevice->device, pVulkanSurface->swapchain, NULL);

        vkDestroyImageView(pVulkanDevice->device, pVulkanSurface->depth.view, NULL);
        vkDestroyImage(pVulkanDevice->device, pVulkanSurface->depth.image, NULL);
        vkFreeMemory(pVulkanDevice->device, pVulkanSurface->depth.mem, NULL);

        for (i = 0; i < pVulkanSurface->swapchainImageCount; i++) {
            vkDestroyImageView(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].view, NULL);
            vkFreeCommandBuffers(pVulkanDevice->device, pVulkanAppData->cmd_pool, 1, &pVulkanSurface->swapchain_image_resources[i].cmd);
            vkDestroyBuffer(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_buffer, NULL);
            vkUnmapMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_memory);
            vkFreeMemory(pVulkanDevice->device, pVulkanSurface->swapchain_image_resources[i].uniform_memory, NULL);
        }
        free(pVulkanSurface->swapchain_image_resources);
        // free(demo->queue_props);
        vkDestroyCommandPool(pVulkanDevice->device, pVulkanAppData->cmd_pool, NULL);

        if (pVulkanDevice->separate_present_queue) {
            vkDestroyCommandPool(pVulkanDevice->device, pVulkanAppData->present_cmd_pool, NULL);
        }
    }
    vkDeviceWaitIdle(pVulkanDevice->device);
    vkDestroyDevice(pVulkanDevice->device, NULL);
    if (pVulkan->hValidationLayer) {
        pVulkan->vkDestroyDebugUtilsMessengerEXT(pVulkan->pInstance, pVulkan->hDebugUtilsMessenger, NULL);
    }
    vkDestroySurfaceKHR(pVulkan->pInstance, pVulkanSurface->pSurface, NULL);

#if defined(VK_USE_PLATFORM_XLIB_KHR)
    XDestroyWindow(demo->display, demo->xlib_window);
    XCloseDisplay(demo->display);
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    xcb_destroy_window(demo->connection, demo->xcb_window);
    xcb_disconnect(demo->connection);
    free(demo->atom_wm_delete_window);
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
    wl_keyboard_destroy(demo->keyboard);
    wl_pointer_destroy(demo->pointer);
    wl_seat_destroy(demo->seat);
    wl_shell_surface_destroy(demo->shell_surface);
    wl_surface_destroy(demo->window);
    wl_shell_destroy(demo->shell);
    wl_compositor_destroy(demo->compositor);
    wl_registry_destroy(demo->registry);
    wl_display_disconnect(demo->display);
#elif defined(VK_USE_PLATFORM_DIRECTFB_EXT)
    demo->event_buffer->Release(demo->event_buffer);
    demo->window->Release(demo->window);
    demo->dfb->Release(demo->dfb);
#endif

    // vkDestroyInstance(pVulkan->pInstance, NULL);
    // pVulkan->pInstance = NULL;
}

void mmVKUpdateDataBuffer(struct mmVKSurface* pVulkanSurface, struct mmVKAppData* pVulkanAppData)
{
    mat4x4 MVP, Model, VP;
    int matrixSize = sizeof(MVP);

    mat4x4_mul(VP, pVulkanAppData->projection_matrix, pVulkanAppData->view_matrix);

    // Rotate around the Y axis
    mat4x4_dup(Model, pVulkanAppData->model_matrix);
    mat4x4_rotate(pVulkanAppData->model_matrix, Model, 0.0f, 1.0f, 0.0f, (float)degreesToRadians(pVulkanAppData->spin_angle));
    mat4x4_mul(MVP, VP, pVulkanAppData->model_matrix);

    memcpy(pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].uniform_memory_ptr, (const void *)&MVP[0][0], matrixSize);
}

void mmVKDraw(
    struct mmVK* pVulkan,
    struct mmVKPhysicalDevice* pVulkanPhysicalDevice,
    struct mmVKDevice* pVulkanDevice,
    struct mmVKSurface* pVulkanSurface,
    struct mmVKAppData* pVulkanAppData)
{
    VkResult err;

    // Ensure no more than FRAME_LAG renderings are outstanding
    vkWaitForFences(pVulkanDevice->device, 1, &pVulkanDevice->fences[pVulkanAppData->frame_index], VK_TRUE, UINT64_MAX);
    vkResetFences(pVulkanDevice->device, 1, &pVulkanDevice->fences[pVulkanAppData->frame_index]);

    do {
        // Get the index of the next available swapchain image:
        err =
            pVulkanDevice->vkAcquireNextImageKHR(pVulkanDevice->device, pVulkanSurface->swapchain, UINT64_MAX,
                                        pVulkanDevice->image_acquired_semaphores[pVulkanAppData->frame_index], VK_NULL_HANDLE, &pVulkanAppData->current_buffer);

        if (err == VK_ERROR_OUT_OF_DATE_KHR) {
            // demo->swapchain is out of date (e.g. the window was resized) and
            // must be recreated:
            mmVKResize(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
        } else if (err == VK_SUBOPTIMAL_KHR) {
            // demo->swapchain is not as optimal as it could be, but the platform's
            // presentation engine will still present the image correctly.
            break;
        } else if (err == VK_ERROR_SURFACE_LOST_KHR) {
            vkDestroySurfaceKHR(pVulkan->pInstance, pVulkanSurface->pSurface, NULL);
            mmVKSurfaceInitVk(pVulkanSurface, pVulkan->pInstance, pVulkanSurface->caMetalLayer);
            // demo_create_surface(demo);
            mmVKResize(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
        } else {
            assert(!err);
        }
    } while (err != VK_SUCCESS);

    mmVKUpdateDataBuffer(pVulkanSurface, pVulkanAppData);

    //    if (demo->VK_GOOGLE_display_timing_enabled) {
    //        // Look at what happened to previous presents, and make appropriate
    //        // adjustments in timing:
    //        DemoUpdateTargetIPD(demo);
    //
    //        // Note: a real application would position its geometry to that it's in
    //        // the correct locatoin for when the next image is presented.  It might
    //        // also wait, so that there's less latency between any input and when
    //        // the next image is rendered/presented.  This demo program is so
    //        // simple that it doesn't do either of those.
    //    }

    // Wait for the image acquired semaphore to be signaled to ensure
    // that the image won't be rendered to until the presentation
    // engine has fully released ownership to the application, and it is
    // okay to render to the image.
    VkPipelineStageFlags pipe_stage_flags;
    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = NULL;
    submit_info.pWaitDstStageMask = &pipe_stage_flags;
    pipe_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &pVulkanDevice->image_acquired_semaphores[pVulkanAppData->frame_index];
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].cmd;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &pVulkanDevice->draw_complete_semaphores[pVulkanAppData->frame_index];
    err = vkQueueSubmit(pVulkanDevice->graphics_queue, 1, &submit_info, pVulkanDevice->fences[pVulkanAppData->frame_index]);
    assert(!err);

    if (pVulkanDevice->separate_present_queue) {
        // If we are using separate queues, change image ownership to the
        // present queue before presenting, waiting for the draw complete
        // semaphore and signalling the ownership released semaphore when finished
        VkFence nullFence = VK_NULL_HANDLE;
        pipe_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        submit_info.waitSemaphoreCount = 1;
        submit_info.pWaitSemaphores = &pVulkanDevice->draw_complete_semaphores[pVulkanAppData->frame_index];
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &pVulkanSurface->swapchain_image_resources[pVulkanAppData->current_buffer].graphics_to_present_cmd;
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = &pVulkanDevice->image_ownership_semaphores[pVulkanAppData->frame_index];
        err = vkQueueSubmit(pVulkanDevice->present_queue, 1, &submit_info, nullFence);
        assert(!err);
    }

    // If we are using separate queues we have to wait for image ownership,
    // otherwise wait for draw complete
    VkPresentInfoKHR present = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = (pVulkanDevice->separate_present_queue) ? &pVulkanDevice->image_ownership_semaphores[pVulkanAppData->frame_index]
                                                          : &pVulkanDevice->draw_complete_semaphores[pVulkanAppData->frame_index],
        .swapchainCount = 1,
        .pSwapchains = &pVulkanSurface->swapchain,
        .pImageIndices = &pVulkanAppData->current_buffer,
    };

    //    VkRectLayerKHR rect;
    //    VkPresentRegionKHR region;
    //    VkPresentRegionsKHR regions;
    //    if (demo->VK_KHR_incremental_present_enabled) {
    //        // If using VK_KHR_incremental_present, we provide a hint of the region
    //        // that contains changed content relative to the previously-presented
    //        // image.  The implementation can use this hint in order to save
    //        // work/power (by only copying the region in the hint).  The
    //        // implementation is free to ignore the hint though, and so we must
    //        // ensure that the entire image has the correctly-drawn content.
    //        uint32_t eighthOfWidth = demo->width / 8;
    //        uint32_t eighthOfHeight = demo->height / 8;
    //
    //        rect.offset.x = eighthOfWidth;
    //        rect.offset.y = eighthOfHeight;
    //        rect.extent.width = eighthOfWidth * 6;
    //        rect.extent.height = eighthOfHeight * 6;
    //        rect.layer = 0;
    //
    //        region.rectangleCount = 1;
    //        region.pRectangles = &rect;
    //
    //        regions.sType = VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR;
    //        regions.pNext = present.pNext;
    //        regions.swapchainCount = present.swapchainCount;
    //        regions.pRegions = &region;
    //        present.pNext = &regions;
    //    }
    //
    //    if (demo->VK_GOOGLE_display_timing_enabled) {
    //        VkPresentTimeGOOGLE ptime;
    //        if (demo->prev_desired_present_time == 0) {
    //            // This must be the first present for this swapchain.
    //            //
    //            // We don't know where we are relative to the presentation engine's
    //            // display's refresh cycle.  We also don't know how long rendering
    //            // takes.  Let's make a grossly-simplified assumption that the
    //            // desiredPresentTime should be half way between now and
    //            // now+target_IPD.  We will adjust over time.
    //            uint64_t curtime = getTimeInNanoseconds();
    //            if (curtime == 0) {
    //                // Since we didn't find out the current time, don't give a
    //                // desiredPresentTime:
    //                ptime.desiredPresentTime = 0;
    //            } else {
    //                ptime.desiredPresentTime = curtime + (demo->target_IPD >> 1);
    //            }
    //        } else {
    //            ptime.desiredPresentTime = (demo->prev_desired_present_time + demo->target_IPD);
    //        }
    //        ptime.presentID = demo->next_present_id++;
    //        demo->prev_desired_present_time = ptime.desiredPresentTime;
    //
    //        VkPresentTimesInfoGOOGLE present_time = {
    //            .sType = VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE,
    //            .pNext = present.pNext,
    //            .swapchainCount = present.swapchainCount,
    //            .pTimes = &ptime,
    //        };
    //        if (demo->VK_GOOGLE_display_timing_enabled) {
    //            present.pNext = &present_time;
    //        }
    //    }

    err = pVulkanDevice->vkQueuePresentKHR(pVulkanDevice->present_queue, &present);
    pVulkanAppData->frame_index += 1;
    pVulkanAppData->frame_index %= FRAME_LAG;

    if (err == VK_ERROR_OUT_OF_DATE_KHR) {
        // demo->swapchain is out of date (e.g. the window was resized) and
        // must be recreated:
        mmVKResize(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
    } else if (err == VK_SUBOPTIMAL_KHR) {
        // demo->swapchain is not as optimal as it could be, but the platform's
        // presentation engine will still present the image correctly.
    } else if (err == VK_ERROR_SURFACE_LOST_KHR) {
        vkDestroySurfaceKHR(pVulkan->pInstance, pVulkanSurface->pSurface, NULL);
        mmVKSurfaceInitVk(pVulkanSurface, pVulkan->pInstance, pVulkanSurface->caMetalLayer);
        mmVKResize(pVulkan, pVulkanPhysicalDevice, pVulkanDevice, pVulkanSurface, pVulkanAppData);
    } else {
        assert(!err);
    }
}

void mmVKPrepareMatrix(struct mmVKAppData* pVulkanAppData)
{
    vec3 eye = {0.0f, 3.0f, 5.0f};
    vec3 origin = {0, 0, 0};
    vec3 up = {0.0f, 1.0f, 0.0};
    
    mat4x4_perspective(pVulkanAppData->projection_matrix, (float)degreesToRadians(45.0f), 1.0f, 0.1f, 100.0f);
    // mat4x4_ortho(demo->projection_matrix, -2.0, 2.0, -2.0, 2.0, -10, 10);
    mat4x4_look_at(pVulkanAppData->view_matrix, eye, origin, up);
    mat4x4_identity(pVulkanAppData->model_matrix);

    pVulkanAppData->projection_matrix[1][1] *= -1;  // Flip projection matrix from GL to Vulkan orientation.
}
