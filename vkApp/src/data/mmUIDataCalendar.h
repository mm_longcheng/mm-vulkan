/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIDataCalendar_h__
#define __mmUIDataCalendar_h__

#include "core/mmCore.h"

#include "parse/mmParseTSV.h"

#include "sxwnl/mmSxwnlJD.h"
#include "sxwnl/mmSxwnlLunar.h"

#include "core/mmPrefix.h"

struct csLunarSSQ;
struct csLunarLun;

struct mmFileContext;

// general type ym.
void
mmUIDataCalendarTimeCYM(
    int y,
    int m,
    char s[64]);

// general type ymd.
void
mmUIDataCalendarTimeCYMD(
    int y,
    int m,
    int d,
    char s[32]);

void
mmUIDataCalendarTimeNY(
    int y,
    char s[32]);

// special type ymd.
void
mmUIDataCalendarTimeNYMD(
    int y,
    int m,
    int d,
    char s[32]);

void
mmUIDataCalendarTimeNMD(
    int m,
    int d,
    char s[32]);

void
mmUIDataCalendarTimeWeek(
    int w,
    char s[16]);

void
mmUIDataCalendarTimeXing(
    int x,
    char s[16]);

void
mmUIDataCalendarTimeGZ(
    int x,
    const char* t,
    char s[16]);

void
mmUIDataCalendarTimeMLBZ(
    struct tm* tm,
    char s[64]);

void
mmUIDataCalendarTimeClock(
    struct tm* tm,
    char s[32]);

void
mmUIDataCalendarJQCalc(
    int y,
    struct csJD hJDs[24],
    int hJQs[24]);

void
mmUIDataCalendarGetPoetry(
    char s[256],
    const char** rows);

int
mmUIDataCalendarGetTwoDateDifferent(
    int sY, int sM, int sD,
    int tY, int tM, int tD);

struct mmUIDataCalendar
{
    struct csLunarSSQ* hSSQ;
    struct csLunarLun* hLun;

    int weekd[2];
    int weeki[2];
    int daydt[4];

    struct csJD hJDs[24];
    int hJQs[24];

    struct csLunarOBDay sDay;
    
    struct mmParseTSV tsv;

    // show month
    int hY;
    int hM;

    // select
    int sY;
    int sM;
    int sD;

    // today
    int tY;
    int tM;
    int tD;
    int hTodayJDi;
    time_t hTimecode;

    int sQH;
    int sJQ; 
    int sHI;

    int tQH;
    int tJQ; 
    int tHI;
};

void
mmUIDataCalendar_Init(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_Destroy(
    struct mmUIDataCalendar*                       p);

int
mmUIDataCalendar_Prepare(
    struct mmUIDataCalendar*                       p,
    struct mmFileContext*                          pFileContext);

void
mmUIDataCalendar_Discard(
    struct mmUIDataCalendar*                       p);

const struct csLunarOBDay*
mmUIDataCalendar_GetSelectToday(
    const struct mmUIDataCalendar*                 p);

const struct csLunarOBDay*
mmUIDataCalendar_GetVisibleDay0(
    const struct mmUIDataCalendar*                 p);

int
mmUIDataCalendar_GetSelectIsToday(
    const struct mmUIDataCalendar*                 p,
    struct tm*                                     tm);

int
mmUIDataCalendar_GetJQIndex(
    const struct mmUIDataCalendar*                 p,
    int                                            sJQ);

void
mmUIDataCalendar_UpdateToday(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_UpdateLun(
    struct mmUIDataCalendar*                       p);

int
mmUIDataCalendar_UpdateTime(
    struct mmUIDataCalendar*                       p,
    mmUInt8_t                                      index);

void
mmUIDataCalendar_UpdateTQH(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_UpdateSQH(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_UpdateYearJQ(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_PrevMonth(
    struct mmUIDataCalendar*                       p);

void
mmUIDataCalendar_NextMonth(
    struct mmUIDataCalendar*                       p);

int
mmUIDataCalendar_PrepareDataTerms(
    struct mmUIDataCalendar*                       p,
    struct mmFileContext*                          pFileContext);

void
mmUIDataCalendar_DiscardDataTerms(
    struct mmUIDataCalendar*                       p);

#include "core/mmSuffix.h"

#endif//__mmUIDataCalendar_h__
