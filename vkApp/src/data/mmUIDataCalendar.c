/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIDataCalendar.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"
#include "core/mmTime.h"
#include "core/mmTimeUtils.h"
#include "core/mmByte.h"

#include "dish/mmFileContext.h"

#include "sxwnl/mmSxwnlConst.h"
#include "sxwnl/mmSxwnlJD.h"
#include "sxwnl/mmSxwnlEph0.h"
#include "sxwnl/mmSxwnlLunarData.h"

// general type ym.
void
mmUIDataCalendarTimeCYM(
    int y,
    int m,
    char s[64])
{
    static const char* cNameY = "年";
    static const char* cNameM = "月";

    char hYString[16];
    char hMString[16];
    char* v;
    size_t l;

    mmValue_SInt32ToA(&y, hYString);
    sprintf(hMString, "%02d", m);

    v = s;
    l = strlen(hYString);
    mmMemcpy(v, hYString, l);
    v += l;
    l = strlen(cNameY);
    mmMemcpy(v, cNameY, l);
    v += l;
    l = strlen(hMString);
    mmCStrRAlignAppends(v, hMString, l, 2);
    v += 2;
    l = strlen(cNameM);
    mmMemcpy(v, cNameM, l);
    v += l;
    (*v) = 0;
}

// general type ymd.
void
mmUIDataCalendarTimeCYMD(
    int y,
    int m,
    int d,
    char s[32])
{
    static const char* cNameY = "年";
    static const char* cNameM = "月";
    static const char* cNameD = "日";

    char hYString[16];
    char hMString[16];
    char hDString[16];
    char* v;
    size_t l;

    mmValue_SInt32ToA(&y, hYString);
    sprintf(hMString, "%02d", m);
    sprintf(hDString, "%02d", d);

    v = s;
    l = strlen(hYString);
    mmMemcpy(v, hYString, l);
    v += l;
    l = strlen(cNameY);
    mmMemcpy(v, cNameY, l);
    v += l;
    l = strlen(hMString);
    mmCStrRAlignAppends(v, hMString, l, 2);
    v += 2;
    l = strlen(cNameM);
    mmMemcpy(v, cNameM, l);
    v += l;
    l = strlen(hDString);
    mmCStrRAlignAppends(v, hDString, l, 2);
    v += 2;
    l = strlen(cNameD);
    mmMemcpy(v, cNameD, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeNY(
    int y,
    char s[32])
{
    static const char* cNameY = "年";

    char hYString[16];
    char* v;
    size_t l;

    mmValue_SInt32ToA(&y, hYString);

    v = s;
    l = strlen(hYString);
    mmMemcpy(v, hYString, l);
    v += l;
    l = strlen(cNameY);
    mmMemcpy(v, cNameY, l);
    v += l;
    (*v) = 0;
}

// special type ymd.
void
mmUIDataCalendarTimeNYMD(
    int y,
    int m,
    int d,
    char s[32])
{
    static const char* cNameY = "年";
    static const char* cNameM = "月";

    char hYString[16];
    const char* hMString;
    const char* hDString;
    char* v;
    size_t l;

    mmValue_SInt32ToA(&y, hYString);
    hMString = csLunarOBB_ync[m];
    hDString = csLunarOBB_rmc[d];

    v = s;
    l = strlen(hYString);
    mmMemcpy(v, hYString, l);
    v += l;
    l = strlen(cNameY);
    mmMemcpy(v, cNameY, l);
    v += l;
    l = strlen(hMString);
    mmMemcpy(v, hMString, l);
    v += l;
    l = strlen(cNameM);
    mmMemcpy(v, cNameM, l);
    v += l;
    l = strlen(hDString);
    mmMemcpy(v, hDString, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeNMD(
    int m,
    int d,
    char s[32])
{
    static const char* cNameM = "月";

    const char* hMString;
    const char* hDString;
    char* v;
    size_t l;

    hMString = csLunarOBB_ync[m];
    hDString = csLunarOBB_rmc[d];

    v = s;
    l = strlen(hMString);
    mmMemcpy(v, hMString, l);
    v += l;
    l = strlen(cNameM);
    mmMemcpy(v, cNameM, l);
    v += l;
    l = strlen(hDString);
    mmMemcpy(v, hDString, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeWeek(
    int w,
    char s[16])
{
    static const char* cNameW = "星期";

    const char* hWString;
    char* v;
    size_t l;

    hWString = csLunarOBB_xqmc[w];

    v = s;
    l = strlen(cNameW);
    mmMemcpy(v, cNameW, l);
    v += l;
    l = strlen(hWString);
    mmMemcpy(v, hWString, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeXing(
    int x,
    char s[16])
{
    static const char* cNameW = "座";

    const char* hXString;
    char* v;
    size_t l;

    hXString = csLunarOBB_XiZ[x];

    v = s;
    l = strlen(hXString);
    mmMemcpy(v, hXString, l);
    v += l;
    l = strlen(cNameW);
    mmMemcpy(v, cNameW, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeGZ(
    int x,
    const char* t,
    char s[16])
{
    char* v;
    size_t l;

    int g = x % 10;
    int z = x % 12;

    const char* sg = csLunarOBB_Gan[g];
    const char* sz = csLunarOBB_Zhi[z];

    v = s;
    l = strlen(sg);
    mmMemcpy(v, sg, l);
    v += l;
    l = strlen(sz);
    mmMemcpy(v, sz, l);
    v += l;
    l = strlen(t);
    mmMemcpy(v, t, l);
    v += l;
    (*v) = 0;
}

void
mmUIDataCalendarTimeMLBZ(
    struct tm* tm,
    char s[64])
{
    double vJ = (116 + 23 / 60.0) / cs_radd;

    struct csJD JD;
    struct csLunarMLBZ ob;
    JD.h = tm->tm_hour; JD.m = tm->tm_min; JD.s = tm->tm_sec;
    JD.Y = tm->tm_year; JD.M = tm->tm_mon; JD.D = tm->tm_mday;
    double jd = csJD_toJD(&JD);
    csLunarOBB_mingLiBaZi(jd + (-8.0) / 24 - cs_J2000, vJ, &ob);

    sprintf(s, "%s%s%s %s%s%s %s%s%s %s%s%s",
        csLunarOBB_Gan[ob.bz_jn[0]], csLunarOBB_Zhi[ob.bz_jn[1]], "年",
        csLunarOBB_Gan[ob.bz_jy[0]], csLunarOBB_Zhi[ob.bz_jy[1]], "月",
        csLunarOBB_Gan[ob.bz_jr[0]], csLunarOBB_Zhi[ob.bz_jr[1]], "日",
        csLunarOBB_Gan[ob.bz_js[0]], csLunarOBB_Zhi[ob.bz_js[1]], "时");
}

void
mmUIDataCalendarTimeClock(
    struct tm* tm,
    char s[32])
{

    sprintf(s, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min, tm->tm_sec);
}

void
mmUIDataCalendarJQCalc(
    int                                            y,
    struct csJD                                    hJDs[24],
    int                                            hJQs[24])
{
    int i;
    int idx;
    int year;
    double T;
    double jd;
    struct csJD hJD;

    year = y;
    y -= 2000;

    // 从前一年，计算两年，确保数据完备
    y -= 1;

    idx = 0;

    for (i = 0; i < 48; i++)
    {
        T = XL_S_aLon_t((y + i * 15 / 360.0 + 1) * 2 * cs_pi);

        jd = T * 36525 + cs_J2000 + 8.0 / 24 - dt_T(T * 36525);
        csJD_DD(&hJD, jd);

        if (year == hJD.Y)
        {
            hJDs[idx] = hJD;
            hJQs[idx] = (i + 6) % 24;
            idx++;
        }
    }
}

void
mmUIDataCalendarGetPoetry(
    char s[256],
    const char** rows)
{
    sprintf(s, "%s\n%s\n%s\n%s\n%s\n%s\n",
        rows[6], rows[5],
        rows[7], rows[8], rows[9], rows[10]);
}

int
mmUIDataCalendarGetTwoDateDifferent(
    int sY, int sM, int sD,
    int tY, int tM, int tD)
{
    if (sY != tY)
    {
        return 3;
    }
    if (sM != tM)
    {
        return 2;
    }
    if (sD != tD)
    {
        return 1;
    }
    return 0;
}

void
mmUIDataCalendar_Init(
    struct mmUIDataCalendar*                       p)
{
    p->hSSQ = (struct csLunarSSQ*)mmMalloc(sizeof(struct csLunarSSQ));
    p->hLun = (struct csLunarLun*)mmMalloc(sizeof(struct csLunarLun) * 3);

    csLunarSSQ_Init(p->hSSQ);
    csLunarSSQ_initialize(p->hSSQ);

    csLunarLun_Init(&p->hLun[0]);
    csLunarLun_Init(&p->hLun[1]);
    csLunarLun_Init(&p->hLun[2]);

    mmMemset(p->weekd, 0, sizeof(p->weekd));
    mmMemset(p->weeki, 0, sizeof(p->weeki));
    mmMemset(p->daydt, 0, sizeof(p->daydt));

    mmMemset(p->hJDs, 0, sizeof(p->hJDs));
    mmMemset(p->hJQs, 0, sizeof(p->hJQs));

    csLunarOBDay_Init(&p->sDay);
    
    mmParseTSV_Init(&p->tsv);

    p->hY = 2022;
    p->hM = 1;

    p->sY = 2022;
    p->sM = 1;
    p->sD = 1;

    p->tY = 2022;
    p->tM = 1;
    p->tD = 1;
    p->hTodayJDi = -1;
    p->hTimecode = 0;

    p->sQH = 0;
    p->sJQ = 0;
    p->sHI = 0;

    p->tQH = 0;
    p->tJQ = 0;
    p->tHI = 0;
}

void
mmUIDataCalendar_Destroy(
    struct mmUIDataCalendar*                       p)
{
    p->tHI = 0;
    p->tJQ = 0;
    p->tQH = 0;

    p->sHI = 0;
    p->sJQ = 0;
    p->sQH = 0;

    p->hTimecode = 0;
    p->hTodayJDi = -1;
    p->tD = 1;
    p->tM = 1;
    p->tY = 2022;

    p->sD = 1;
    p->sM = 1;
    p->sY = 2022;

    p->hM = 1;
    p->hY = 2022;
    
    mmParseTSV_Destroy(&p->tsv);
    
    csLunarOBDay_Destroy(&p->sDay);

    mmMemset(p->hJQs, 0, sizeof(p->hJQs));
    mmMemset(p->hJDs, 0, sizeof(p->hJDs));

    mmMemset(p->daydt, 0, sizeof(p->daydt));
    mmMemset(p->weeki, 0, sizeof(p->weeki));
    mmMemset(p->weekd, 0, sizeof(p->weekd));

    csLunarLun_Destroy(&p->hLun[2]);
    csLunarLun_Destroy(&p->hLun[1]);
    csLunarLun_Destroy(&p->hLun[0]);

    csLunarSSQ_Destroy(p->hSSQ);

    mmFree(p->hLun);
    p->hLun = NULL;

    mmFree(p->hSSQ);
    p->hSSQ = NULL;
}

int
mmUIDataCalendar_Prepare(
    struct mmUIDataCalendar*                       p,
    struct mmFileContext*                          pFileContext)
{
    mmUIDataCalendar_UpdateToday(p);
    mmUIDataCalendar_UpdateLun(p);
    mmUIDataCalendar_UpdateYearJQ(p);
    mmUIDataCalendar_PrepareDataTerms(p, pFileContext);
    return 0;
}

void
mmUIDataCalendar_Discard(
    struct mmUIDataCalendar*                       p)
{
    mmUIDataCalendar_DiscardDataTerms(p);
}

const struct csLunarOBDay*
mmUIDataCalendar_GetSelectToday(
    const struct mmUIDataCalendar*                 p)
{
    int i;
    struct csLunarLun* pLun;
    
    for (i = 0; i < 3; ++i)
    {
        pLun = &p->hLun[i];
        if ((pLun->y == p->sY) &&
            (pLun->m == p->sM) &&
            (1 <= p->sD && p->sD <= pLun->dn))
        {
            return &pLun->day[p->sD - 1];
        }
    }

    return &p->sDay;
}

const struct csLunarOBDay*
mmUIDataCalendar_GetVisibleDay0(
    const struct mmUIDataCalendar*                 p)
{
    int index = -p->daydt[0];
    struct csLunarLun* pLun = &p->hLun[0];
    if (0 <= index && index < pLun->dn)
    {
        return &pLun->day[index];
    }
    else
    {
        // some time the part0 day is not visible.
        // the part1 day[0] is will be visible day0.
        return &p->hLun[1].day[0];
    }
}

int
mmUIDataCalendar_GetSelectIsToday(
    const struct mmUIDataCalendar*                 p,
    struct tm*                                     tm)
{
    const struct csLunarOBDay* pDay;

    time_t t;

    time(&t);
    mmLocaltimeR(&t, tm);

    pDay = mmUIDataCalendar_GetSelectToday(p);
    if (NULL == pDay)
    {
        return MM_FALSE;
    }
    else
    {
        return 
            pDay->y == tm->tm_year && 
            pDay->m == tm->tm_mon  && 
            pDay->d == tm->tm_mday;
    }
}

int
mmUIDataCalendar_GetJQIndex(
    const struct mmUIDataCalendar*                 p,
    int                                            sJQ)
{
    int idx;
    for (idx = 0; idx < 24; idx++)
    {
        if (p->hJQs[idx] == sJQ)
        {
            break;
        }
    }
    return idx;
}

void
mmUIDataCalendar_UpdateToday(
    struct mmUIDataCalendar*                       p)
{
    time_t t;
    struct tm tm;
    struct csJD JD;

    time(&t);
    mmLocaltimeR(&t, &tm);

    p->hY = tm.tm_year;
    p->hM = tm.tm_mon;

    p->sY = tm.tm_year;
    p->sM = tm.tm_mon;
    p->sD = tm.tm_mday;

    p->tY = tm.tm_year;
    p->tM = tm.tm_mon;
    p->tD = tm.tm_mday;

    JD.h = 12; JD.m = 0; JD.s = 0.1;
    JD.Y = p->tY; JD.M = p->tM; JD.D = p->tD;
    p->hTodayJDi = int2(csJD_toJD(&JD)) - cs_J2000;

    mmUIDataCalendar_UpdateSQH(p);
    mmUIDataCalendar_UpdateTQH(p);
}

void
mmUIDataCalendar_UpdateLun(
    struct mmUIDataCalendar*                       p)
{
    int ny, nm;
    int dn0, dn1, dn2;
    struct csLunarOBDay* pDayU;
    struct csLunarOBDay* pDayL;
    struct csLunarOBDay* pDayR;
    struct csLunarOBDay* pDayD;

    mmTimeYMNormalize(p->hY, p->hM - 1, &ny, &nm);
    csLunarLun_yueLiCalc(&p->hLun[0], p->hSSQ, ny, nm);
    mmTimeYMNormalize(p->hY, p->hM, &ny, &nm);
    csLunarLun_yueLiCalc(&p->hLun[1], p->hSSQ, ny, nm);
    mmTimeYMNormalize(p->hY, p->hM + 1, &ny, &nm);
    csLunarLun_yueLiCalc(&p->hLun[2], p->hSSQ, ny, nm);

    dn0 = p->hLun[0].dn;
    dn1 = p->hLun[1].dn;
    dn2 = p->hLun[2].dn;
    if (0 < dn0 && 0 < dn1 && 0 < dn2)
    {
        // part 0
        pDayU = &p->hLun[0].day[dn0 - 1];
        pDayL = &p->hLun[1].day[0];
        p->daydt[0] = pDayL->week - pDayU->d;
        p->weeki[0] = pDayL->weeki + (0 == p->daydt[0] ? -1 : 0);
        p->weekd[0] = pDayL->week;

        // part 1
        p->daydt[1] = 0;

        // part 2
        pDayR = &p->hLun[1].day[dn1 - 1];
        pDayD = &p->hLun[2].day[0];
        p->daydt[2] = pDayD->week;
        p->weeki[1] = pDayR->weeki + (0 == p->daydt[2] ? +1 : 0);
        p->weekd[1] = pDayD->week;

        // part 3
        pDayL = &p->hLun[1].day[0];
        p->daydt[3] = dn1 + pDayL->week;
    }
    else
    {
        mmMemset(p->weeki, 0, sizeof(p->weeki));
        mmMemset(p->daydt, 0, sizeof(p->daydt));
    }
    
    if (p->hY == p->sY && p->hM == p->sM && 1 < p->sD)
    {
        // cache current select day data.
        csLunarOBDay_Assign(&p->sDay, &p->hLun[1].day[p->sD - 1]);
    }
}

int
mmUIDataCalendar_UpdateTime(
    struct mmUIDataCalendar*                       p,
    mmUInt8_t                                      index)
{
    int change = 0;

    do
    {
        time_t t;
        struct tm tm;
        struct csJD JD;

        int hTodayJDi;

        time(&t);

        if (p->hTimecode == t)
        {
            // second is same.
            change = 0;
            break;
        }

        p->hTimecode = t;

        mmLocaltimeR(&t, &tm);

        JD.h = 12; JD.m = 0; JD.s = 0.1;
        JD.Y = tm.tm_year;
        JD.M = tm.tm_mon;
        JD.D = tm.tm_mday;
        hTodayJDi = int2(csJD_toJD(&JD)) - cs_J2000;

        if (p->hTodayJDi == hTodayJDi)
        {
            // day is same.
            change = 1;
            break;
        }

        p->hY = tm.tm_year;
        p->hM = tm.tm_mon;

        p->sY = tm.tm_year;
        p->sM = tm.tm_mon;
        p->sD = tm.tm_mday;

        p->tY = tm.tm_year;
        p->tM = tm.tm_mon;
        p->tD = tm.tm_mday;

        p->hTodayJDi = hTodayJDi;

        mmUIDataCalendar_UpdateSQH(p);
        mmUIDataCalendar_UpdateTQH(p);

        mmUIDataCalendar_UpdateLun(p);
        mmUIDataCalendar_UpdateYearJQ(p);

        // day is different.
        change = 2;
    } while (0);

    return change;
}

void
mmUIDataCalendar_UpdateTQH(
    struct mmUIDataCalendar*                       p)
{
    int qh, jq, hi;
    struct csJD JD;

    qh = csQH_IndexByYMD(p->sY, p->sM, p->sD, &JD);
    qh = (72 + qh) % 72;
    jq = qh / 3;
    hi = qh - jq * 3;

    p->tQH = qh;
    p->tJQ = jq;
    p->tHI = hi;

    p->sQH = qh;
    p->sJQ = jq;
    p->sHI = hi;
}

void
mmUIDataCalendar_UpdateSQH(
    struct mmUIDataCalendar*                       p)
{
    int qh, jq, hi;
    struct csJD JD;

    qh = csQH_IndexByYMD(p->sY, p->sM, p->sD, &JD);
    qh = (72 + qh) % 72;
    jq = qh / 3;
    hi = qh - jq * 3;

    p->sQH = qh;
    p->sJQ = jq;
    p->sHI = hi;
}

void
mmUIDataCalendar_UpdateYearJQ(
    struct mmUIDataCalendar*                       p)
{
    mmUIDataCalendarJQCalc(p->sY, p->hJDs, p->hJQs);
}

void
mmUIDataCalendar_PrevMonth(
    struct mmUIDataCalendar*                       p)
{
    mmTimeYMNormalize(p->hY, p->hM - 1, &p->hY, &p->hM);
    mmUIDataCalendar_UpdateLun(p);
}

void
mmUIDataCalendar_NextMonth(
    struct mmUIDataCalendar*                       p)
{
    mmTimeYMNormalize(p->hY, p->hM + 1, &p->hY, &p->hM);
    mmUIDataCalendar_UpdateLun(p);
}

int
mmUIDataCalendar_PrepareDataTerms(
    struct mmUIDataCalendar*                       p,
    struct mmFileContext*                          pFileContext)
{
    int err = MM_UNKNOWN;

    do
    {
        static const char* path = "data/24_solar_terms.tsv";

        struct mmByteBuffer bytes;
        int hExists;
        mmUInt8_t* data;
        size_t size;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmParseTSV_SetMaxNumber(&p->tsv, 25, 11);

        hExists = mmFileContext_IsFileExists(pFileContext, path);
        if (0 == hExists)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " path is not exists.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        mmFileContext_AcquireFileByteBuffer(pFileContext, path, &bytes);
        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes can not read.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;
        mmParseTSV_AnalysisBuffer(&p->tsv, data, size);
        mmParseTSV_MakePrimaryKeyTable(&p->tsv, 1, 0);

        mmFileContext_ReleaseFileByteBuffer(pFileContext, &bytes);

        err = MM_SUCCESS;
    } while (0);

    return err;
}

void
mmUIDataCalendar_DiscardDataTerms(
    struct mmUIDataCalendar*                       p)
{
    mmParseTSV_Clear(&p->tsv);
    mmParseTSV_Reset(&p->tsv);
}
