/*
    https://github.com/lieff/minimp3
    To the extent possible under law, the author(s) have dedicated all copyright and
    related and neighboring rights to this software to the public domain worldwide.
    This software is distributed without any warranty.
    See <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

#ifdef _MSC_VER
    #pragma warning(push, 4)
    #pragma warning(disable: 4244)
    #pragma warning(disable: 4267)
    #pragma warning(disable: 4456)
#endif

#ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wshorten-64-to-32"
#endif

#define MINIMP3_IMPLEMENTATION
#include "minimp3.h"
#include "minimp3_ex.h"

#ifdef __clang__
    #pragma clang diagnostic pop
#endif

#ifdef _MSC_VER
    #pragma warning(pop)
#endif
