/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestVKModel.h"

#include "mmActivityMaster.h"

#include "nwsi/mmContextMaster.h"

void mmTestVKModel_Func(struct mmActivityMaster* p)
{
    //struct mmContextMaster* pContextMaster = p->pContextMaster;
    //struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    //struct mmVKPhysicalDevice* pPhysicalDevice = &p->vRenderer.vPhysicalDevice;

    //struct mmVKModelLoader l;

    //mmVKModelLoader_Init(&l);

    //mmVKModelLoader_SetFileContext(&l, pFileContext);
    //mmVKModelLoader_SetUploader(&l, &p->vRenderer.vUploader);
    //mmVKModelLoader_SetTranscodeFmt(&l, pPhysicalDevice->transcodefmt);

    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Embedded_draco.gltf");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Embedded.gltf");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Binary_uastc_draco.glb");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Binary_uastc.glb");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Embedded_uastc_draco.gltf");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured_Embedded_uastc.gltf");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured.glb");
    //mmVKModelLoader_LoadFromFile(&l, "model/BoxTextured.gltf");

    //mmVKModelLoader_Destroy(&l);
}

