/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestHash.h"

#include "algorithm/mmCRC32.h"
#include "algorithm/mmSHA1.h"

void digest_to_hex(const uint8_t digest[20], char *output)
{
    int i,j;
    char *c = output;

    for (i = 0; i < 20/4; i++) {
        for (j = 0; j < 4; j++) {
            sprintf(c,"%02X", digest[i*4+j]);
            c += 2;
        }
        sprintf(c, " ");
        c += 1;
    }
    *(c - 1) = '\0';
}

static char *test_data[] = {
    "abc",
    "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
    "A million repetitions of 'a'"};
static char *test_results[] = {
    "A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D",
    "84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1",
    "34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F"};

int sha1X_main(void)
{
    int k;
    struct mmSHA1Context context;
    uint8_t digest[20];
    char output[80];

    fprintf(stdout, "verifying SHA-1 implementation... ");

    for (k = 0; k < 2; k++)
    {
        mmSHA1_Init(&context);
        mmSHA1_Update(&context, (uint8_t*)test_data[k], strlen(test_data[k]));
        mmSHA1_Final(&context, digest);
        digest_to_hex(digest, output);

        if (strcmp(output, test_results[k]))
        {
            fprintf(stdout, "FAIL\n");
            fprintf(stderr,"* hash of \"%s\" incorrect:\n", test_data[k]);
            fprintf(stderr,"\t%s returned\n", output);
            fprintf(stderr,"\t%s is correct\n", test_results[k]);
            return (1);
        }
    }
    /* million 'a' vector we feed separately */
    mmSHA1_Init(&context);
    for (k = 0; k < 1000000; k++)
    {
        mmSHA1_Update(&context, (uint8_t*)"a", 1);
    }
    
    mmSHA1_Final(&context, digest);
    digest_to_hex(digest, output);
    if (strcmp(output, test_results[2]))
    {
        fprintf(stdout, "FAIL\n");
        fprintf(stderr,"* hash of \"%s\" incorrect:\n", test_data[2]);
        fprintf(stderr,"\t%s returned\n", output);
        fprintf(stderr,"\t%s is correct\n", test_results[2]);
        return (1);
    }

    /* success */
    fprintf(stdout, "ok\n");
    return(0);
}

void mmTestHash_Func(struct mmActivityMaster* p)
{
    const char* s = "1234567890";
    char buffer0[16] = { 0 };
    char buffer1[16] = { 0 };
    mmUInt32_t c0 = mmCRC32B((const void*)s, (int)strlen(s), 0);
    mmUInt32_t c1 = mmCRC32C((const void*)s, (int)strlen(s), 0);
    sprintf(buffer0, "0x%08X", c0);
    sprintf(buffer1, "0x%08X", c1);
    // CRC32=0x261DAEE5
    
    sha1X_main();
}

