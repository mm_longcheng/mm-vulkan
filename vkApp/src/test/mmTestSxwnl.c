/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestSxwnl.h"

#include "sxwnl/mmSxwnlConst.h"
#include "sxwnl/mmSxwnlJD.h"
#include "sxwnl/mmSxwnlEph.h"
#include "sxwnl/mmSxwnlEph0.h"
#include "sxwnl/mmSxwnlEphB.h"
#include "sxwnl/mmSxwnlLunar.h"
#include "sxwnl/mmSxwnlLunarData.h"
#include "sxwnl/mmSxwnlLunarJNB.h"
#include "sxwnl/mmSxwnlSheet.h"

#include "core/mmLoggerManager.h"
#include "core/mmUTFConvert.h"
#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"

#include "mmActivityMaster.h"

FILE* fd;

//struct Date
//{//儒略历结构，包含: 年 月 日 时 分 秒 (浮点型)
//    int Y, M, D, h, m;
//    double s;
//};
//
//extern void rysCalc(Date d, bool is_utc, bool nasa_r);
//extern std::array<double, 3> parallax(std::array<double, 3> z, double H, double fa, double high);

void mmTestSxwnl_Func(struct mmActivityMaster* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    fd = gLoggerManager->stream;
    {
        //      // 2.3034288543513397,0.3118263306484729,151828439.54712752
        //      double z[3] = { 2.3034092990029764, 0.31181477692566817, 151833881.74077481 };
        //      parallax(z, z, 19753.875600590593, 0.00000000000000000, 0.00000000000000000);
              ////[0]   2.3034288543513397  double
              ////[1]   0.31182633064847293 double
              ////[2]   151828439.54712752  double

              //[0] 2.3034092990029764  double
              //[1] 0.31181477692566817 double
              //[2] 151833881.74077481  double
              //H   19753.875600645992  double
              //fa  0.00000000000000000 double
              //high    0.00000000000000000 double
        double z[3] = { 2.3034092990029764, 0.31181477692566817, 151833881.74077481 };
        parallax(z, z, 19753.875600645992, 0.00000000000000000, 0.00000000000000000);

        //std::array<double, 3> s = { 2.3034092990029764, 0.31181477692566817, 151833881.74077481 };
        //s = parallax(s, 19753.875600645992, 0.00000000000000000, 0.00000000000000000);

        double deltat = dt_T(3134.932083333377);
        double E = hcjj(0.0085829527731159580);

        double aa = 0.00009630743824562416;
        double ab = 2.303492714112906 - 2.3033964066746604;

        double ac = rad2rrad(-19753.111055253492);

        deltat = 0;
        E = 0;
        aa = 0;
        ab = 0;
        ac = 0;

        {
            double m0 = 2.3034927141129065;
            double s0 = 2.3033964066746604;
            double aa = m0 - s0;
            double bb = 0.95176343010411302;
            //double bb = 0.951763430104113;
            double ac1 = rad2rrad(aa);
            double ac = ac1 * bb;
            int a = 0;

            aa = 0;
            ac = 0;
            a = 0;
            // aa   9.6307438246068244e-05  double
            // ac1  9.6307438246068244e-05  double
            // ac   9.1661897769617953e-05  double

            //aa=0.00009630743824606824
            //ac1=0.00009630743824606824
            //ac=0.00009166189776961795 
        }

        {
            double d[] = { 2.3012427169666174, 0.3077215765169156, 151833881.7407748 };
            double A[] = { 5.290571279422482, 0.1027043968952641, 12396.410963914295 };
            double g = 19756.179009889594;
            struct csEphCoord pp;
            lineEar(&pp, d, A, g);
            int a = 0;
            a = 0;
        }

        {
            double d[] = { 2.3012427169666116, 0.30772157651691828, 151833881.74077481 };
            double A[] = { 5.2905712794224824, 0.10270439689526410, 12396.410963914295 };
            double g = 19756.179009889594;
            struct csEphCoord pp;
            lineEar(&pp, d, A, g);
            int a = 0;
            a = 0 ;
        }
        {
            double d[] = { 2.3012427169666116, 0.30772157651691828, 151833881.74077481 };
            double A[] = { 5.290571279422482, 0.1027043968952641, 12396.410963914295 };
            double g = 19756.179009889594;
            struct csEphCoord pp;
            lineEar(&pp, d, A, g);
            int a = 0;
            a = 0;
        }
        {
            double v1 = rad2rrad(0.00023355935985636123);
            double v2 = (rad2rrad(0.00023355935985636123) * 0.9517744849733032);
            int a = 0;
            v1 = 0;
            v2 = 0;
            a = 0;
        }
        int a = 0;
        a = 0;
    }
    //{
    //    struct Date JD = { 2008, 8, 1, 10, 22, 12.0 };
    //    rysCalc(JD, 0, 0);
    //}
    {
        //本影半径 46'20.85" 半影半径 78'28.49" 月亮地心视半径 16'39.75"
        //影月中心距 540'29.10" 影月半径和 63'0.60" 
        //距相切 e[31m477'28.50"e[0m 距第二相切 445'20.86"
        //
        //时间表(月食)
        //食分:0.00000
        //食分指月面直径被遮比例

        struct mmString s;
        mmString_Init(&s);

        double step = 29.5306;
        step = 0;

        //csEphRsPL_rs2Calc(1, 2454679.926741 - cs_J2000, &s);
        //csEphRsPL_rs2Calc(5, 2454679.926741 - cs_J2000, step, 3, &s);
        //csEphRsPL_rs2Jxb(2454679.926741 - cs_J2000, &s);
        //csEphRsPL_rsSearch(2008, 8, 200, 1, &s);
        // 2008-08-01 10:22:12
        struct csJD JD = { 2008, 8, 1, 10, 22, 12.0 };
        double vJ = (116 + 23 / 60.0) / cs_radd;
        double vW = (39.9) / cs_radd;
        //double vJ = (116.383333) / cs_radd;
        //double vW = (39.900000) / cs_radd;
        //double vJ = 2.0312723552949694;
        //double vW = 0.6963863715457375;

        //tianXiang(1, 0, &JD, 10, &s);
        //tianXiang(2, 0, &JD, 10, &s);
        //tianXiang(3, 0, &JD, 10, &s);
        //tianXiang(4, 0, &JD, 10, &s);
        //tianXiang(5, 0, &JD, 10, &s);
        //tianXiang(6, 0, &JD, 10, &s);
        //tianXiang(7, 0, &JD, 10, &s);
        //tianXiang(8, 0, &JD, 10, &s);
        //tianXiang(9, 0, &JD, 10, &s);
        //tianXiang(10, 0, &JD, 10, &s);

        //pCalc(1, &JD, vJ, vW, 1, 10, 1, &s);
        //suoCalc(2022, 24, 0, &s);
        //qiCalc(2022, 24, &s);
        //houCalc(2022, 24, &s);
        //dingQi_cmp(2022, 10, &s);
        //dingSuo_cmp(2022, 10, &s);
        //dingQi_v(&s);
        //dingSuo_v(&s);
        //ML_calc(&JD, vJ, &s);

        //schHXK("And", &s);
        //showHXK0(&s);

        //struct csSheetHX SHX;
        //csSheetHX_Init(&SHX);
        //SHX.idx = 1001;
        //SHX.n = 1;
        //SHX.lx = 1;
        //SHX.nsn = 1;
        //csSheetHX_updateHXKIdx(&SHX);
        //csSheetHX_updateHXKIdxData(&SHX, 0);
        ////csSheetHX_showHXK(&SHX, &s);
        //csSheetHX_aCalc(&SHX, &s);
        //csSheetHX_Destroy(&SHX);

        csEphRsPL_rysCalc(&JD, vJ, vW, 0, 0, &s);
        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);

        mmString_Destroy(&s);
    }
    {
        struct timeval tv;
        struct timezone tz;
        mmGettimeofday(&tv, &tz);

        struct csJD JD = { 2022, 1, 1, 21, 0, 49 };
        double jd = csJD_toJD(&JD);

        jd = 0;

        // -8为北京时
        // v.J/Math.PI*180 Cp11_J.value/radd
        int curTZ = tz.tz_minuteswest / 60;

        // JINGWEI jw = { 116 + 23 / 60.0, 39.9, "默认", "北京" };

        double vJ = (116 + 23 / 60.0) / cs_radd;
        double vW = (39.9) / cs_radd;

        vJ = 0;
        vW = 0;

        struct csEphSZJ SZJ;
        struct mmString s;
        csEphSZJ_Init(&SZJ);
        mmString_Init(&s);
        // 8200 2.031272361112734 0.6963863715457375 -8
        // time_t now = time(NULL);
        mmUInt64_t usec = mmTime_CurrentUSec();
        mmUInt64_t now = usec / 1000;
        // 1655177658863
        double curJD = now / 86400000 - 10957.5 - curTZ / 24; //J2000起算的儒略日数(当前本地时间)
        curJD = 0;

        //shengjiang2(2022);
        //csEphSZJ_shengjiang2(2022, vJ, vW, &s);
        csEphSZJ_shengjiang3(2022, &s);

        //shengjiang(2022, 1, 1);

        // SZJ.L = 2.0312723552949694 
        // SZJ.fa = 0.6963863715457375 
        // jd = 8036 
        // sq = 7.758888866666666
        //csEphSZJ_shengjiang(&SZJ, 2022, 1, 1.0, vJ, vW, &s);


        //csEphSZJ_RTS1(&SZJ, curJD, vJ, vW, curTZ, &s);

        //日出 20:45:22 日落 11:44:10 中天 04:14:42
        //月出 11:51:31 月落 20:58:21 月中 16:26:18
        //晨起天亮 20:12:40 晚上天黑 12:16:53
        //日照时间 14:58:48 白天时间 16:04:12
        //csEphSZJ_RTS1(&SZJ, 8200, 2.031272361112734, 0.6963863715457375, -8, &s);

        //日出 20:45:22 日落 11:44:10 中天 04:14:42
        //月出 11:51:31 月落 20:58:21 月中 16:26:18
        //晨起天亮 20:12:40 晚上天黑 12:16:53
        //日照时间 14:58:48 白天时间 16:04:12
        //csEphSZJ_RTS1_(8200, 2.031272361112734, 0.6963863715457375, -8, &s);


        char str[32] = { 0 };
        double v = 22.263145;
        int m = 2;
        char fmt[16] = { 0 };
        sprintf(fmt, "%%.%df", m);
        sprintf(str, fmt, v);
        sprintf(str, "%03.02f", v);
        sprintf(str, "%03.02f", v);
        mmCStrLAlignAppends(str, "22.26", strlen(str), 8);
        mmCStrMAlignAppends(str, "22.26", strlen(str), 8);
        mmCStrRAlignAppends(str, "22.26", strlen(str), 8);

        v = 22.263145;
        m2fmv2(str, v, 2, 2, 2);
        fwrite(str, 1, strlen(str), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);

        v = 2.263145;
        m2fmv2(str, v, 2, 2, 2);
        fwrite(str, 1, strlen(str), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);

        v = -2.2;
        m2fmv2(str, v, 2, 2, 2);
        fwrite(str, 1, strlen(str), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);

        v = -9380.2;
        m2fmv2(str, v, 2, 2, 2);
        fwrite(str, 1, strlen(str), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);

        fflush(gLoggerManager->stream);

        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&s);
        csEphSZJ_Destroy(&SZJ);
    }
    {
        struct csLunarSSQ SSQ;

        struct mmString s;
        csLunarSSQ_Init(&SSQ);
        csLunarSSQ_initialize(&SSQ);
        mmString_Init(&s);
        //csLunarLun_nianLiString(&SSQ, 2022, "", &s);
        csLunarLun_nianLi2String(&SSQ, 2022, &s);

        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&s);
        csLunarSSQ_Destroy(&SSQ);
    }
    {
        //long int a = timezone;
        struct timeval tv;
        struct timezone tz;
        mmGettimeofday(&tv, &tz);
        time_t t = time(NULL);
        t = 0;

        //mmLong_t tz = mmGettimezone();

        //curTZ = now.getTimezoneOffset() / 60; //时区 -8为北京时
        //curJD = now / 86400000 - 10957.5 - curTZ / 24; //J2000起算的儒略日数(当前本地时间)
        //JD.setFromJD(curJD + J2000);

        // 2459744
        char s[32];
        struct csLunarMLBZ bz;
        //struct csJD JD = { 2022, 6, 13, 21, 0, 49 };
        struct csJD JD = { 2022, 2, 13, 21, 0, 49 };
        double jd = csJD_toJD(&JD);
        // -8为北京时
        // v.J/Math.PI*180 Cp11_J.value/radd
        int curTZ = tz.tz_minuteswest / 60;
        // jd+curTZ/24-J2000
        csLunarOBB_mingLiBaZi(jd + curTZ / 24.0 - cs_J2000, 116.383333 / cs_radd, &bz);

        //curTZ = now.getTimezoneOffset()/60; //时区 -8为北京时
        //var jd = JD.JD(year2Ayear(Cml_y.value), Cml_m.value - 0, Cml_d.value - 0 + t / 24)
        //obb.mingLiBaZi(jd + curTZ / 24 - J2000, Cp11_J.value / radd, ob); //八字计算

        sprintf(s, "%s%s %s%s %s%s %s%s",
            csLunarOBB_Gan[bz.bz_jn[0]], csLunarOBB_Zhi[bz.bz_jn[1]],
            csLunarOBB_Gan[bz.bz_jy[0]], csLunarOBB_Zhi[bz.bz_jy[1]],
            csLunarOBB_Gan[bz.bz_jr[0]], csLunarOBB_Zhi[bz.bz_jr[1]],
            csLunarOBB_Gan[bz.bz_js[0]], csLunarOBB_Zhi[bz.bz_js[1]]);
        fwrite(s, 1, strlen(s), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);

        int a = 0;
        a = 1;
    }

    {
        int i;
        struct csLunarOBDay* ob;
        struct csLunarSSQ SSQ;
        struct csLunarLun LUN;
        csLunarSSQ_Init(&SSQ);
        csLunarLun_Init(&LUN);
        csLunarSSQ_initialize(&SSQ);
        //csLunarLun_yueLiCalc(&LUN, &SSQ, 2022, 6);
        csLunarLun_yueLiCalc(&LUN, &SSQ, 2022, 1);
        for (i = 0; i < LUN.dn; i++)
        {
            ob = &LUN.day[i];
            char s[128];

            //char d[16] = { 0 };
            //const char* ys = mmString_Size(&ob->Lmc) < 2 ? "月" : "";
            //const char* ys = mmUTF_GetUTF8StringLength((const mmUTF8*)mmString_CStr(&ob->Lmc)) < 2 ? "月" : "";
            //sprintf(d, "%s%s%s", mmString_CStr(&ob->Lmc), ys, mmString_CStr(&ob->Ldc));
            //sprintf(s, "%d %s %s %s A:%s B:%s C:%s %s %s", 
            //    ob->d, 
            //    mmString_CStr(&ob->Lday2), 
            //    d, 
            //    mmString_CStr(&ob->Ljq), 
            //    mmString_CStr(&ob->A),
            //    mmString_CStr(&ob->B),
            //    mmString_CStr(&ob->C),
            //    mmString_CStr(&ob->jqmc),
            //    mmString_CStr(&ob->Lmonth2));


            sprintf(s, "%d %d %s %s %s %s %s",
                ob->d,
                ob->week,
                csLunarOBB_ync[ob->Lmi],
                csLunarOBB_rmc[ob->Ldi],
                mmString_CStr(&ob->A),
                mmString_CStr(&ob->B),
                mmString_CStr(&ob->C));


            fwrite(s, 1, strlen(s), gLoggerManager->stream);
            fputc('\n', gLoggerManager->stream);
            fflush(gLoggerManager->stream);
        }

        for (i = 0; i < 25; i++)
        {
            char s[32];
            sprintf(s, "%d", SSQ.ZQ[i]);
            fwrite(s, 1, strlen(s), gLoggerManager->stream);
            fputc('\n', gLoggerManager->stream);
            fflush(gLoggerManager->stream);
        }

        int dz = SSQ.ZQ[0];     // 距冬至的天数
        int xz = SSQ.ZQ[12];    // 距夏至的天数
        int lq = SSQ.ZQ[15];    // 距立秋的天数
        int mz = SSQ.ZQ[11];    // 距芒种的天数
        int xs = SSQ.ZQ[13];    // 距小暑的天数

        char s[32];
        sprintf(s, "%d %d %d %d %d", dz, xz, lq, mz, xs);
        fwrite(s, 1, strlen(s), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);


        struct mmString ss;
        mmString_Init(&ss);
        csLunarLun_yxbString(&LUN, &ss);
        fwrite(mmString_CStr(&ss), 1, mmString_Size(&ss), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&ss);


        csLunarLun_Destroy(&LUN);
        csLunarSSQ_Destroy(&SSQ);
    }

    {
        struct mmString _SB;
        struct mmString _QB;
        mmString_Init(&_SB);
        mmString_Init(&_QB);
        mmString_Assigns(&_SB, csLunarSSQ_suoS);
        mmString_Assigns(&_QB, csLunarSSQ_qiS);
        csLunarSSQ_jieya(&_SB);
        csLunarSSQ_jieya(&_QB);
        fwrite(mmString_CStr(&_SB), 1, mmString_Size(&_SB), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);

        fwrite(mmString_CStr(&_QB), 1, mmString_Size(&_QB), gLoggerManager->stream);
        fputc('\n', gLoggerManager->stream);
        fflush(gLoggerManager->stream);

        mmString_Destroy(&_QB);
        mmString_Destroy(&_SB);
    }

    {
        struct mmString s;
        mmString_Init(&s);
        mmString_Assigns(&s, "ABA");
        mmString_ReplaceCStr(&s, "A", "--");
        mmString_ReplaceCStr(&s, "B", "+++");
        mmString_Destroy(&s);
    }
    {
        struct mmString s;
        mmString_Init(&s);
        csLunarOBB_getNH(1950, &s);
        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Reset(&s);
        csLunarOBB_getNH(1863, &s);
        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&s);
    }
    //xxx1();
    //xxx2();

    {
        struct timeval tv;
        struct timezone tz;
        mmGettimeofday(&tv, &tz);
        int curTZ = tz.tz_minuteswest / 60;

        struct csJD JD = { 2022, 2, 13, 21, 0, 49 };
        double jd = csJD_toJD(&JD) - cs_J2000;
        double vJ = (116 + 23 / 60.0) / cs_radd;
        double vW = (39.9) / cs_radd;
        // 力学时 2022-02-13 13:01:59
        // 8079.04304957667 2.0312723552949694 0.6963863715457375
        double jd2 = jd + curTZ / 24.0 + dt_T(jd);;
        struct mmString s;
        struct csEphMSC msc;
        mmString_Init(&s);
        csEphMSC_calc(&msc, jd2, vJ, vW, 0);
        csEphMSC_toString(&msc, 1, &s);
        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&s);
    }

    {
        struct timeval tv;
        struct timezone tz;
        mmGettimeofday(&tv, &tz);
        int curTZ = tz.tz_minuteswest / 60;

        struct csJD JD = { 2008, 1, 1, 16, 01, 06, };
        double vJ = (116 + 23 / 60.0) / cs_radd;
        double vW = (39.9) / cs_radd;
        double jd = csJD_toJD(&JD) - cs_J2000;
        double jd2 = jd + curTZ / 24.0 + dt_T(jd);

        struct mmString s;
        mmString_Init(&s);
        // xingX(&s, 1, jd, 116.383333, 39.900000);

        //  1 2921.834856483536 2.0312723552949694 0.6963863715457375
        xingX(&s, 1, jd2, vJ, vW);

        //Date date = { 2008, 1, 1, 16, 01, 06, };
        //double jd1 = toJD(date);

        //黄经一  309°07'06.37" 黄纬一 -  6°54'49.63" 向径一 0.42501147
        //视黄经  288°49'04.42" 视黄纬 -  2°08'29.91" 地心距 1.36903680
        //视赤经   19h22m48.40s 视赤纬 - 24°14'22.70" 光行距 1.36913822
        //站赤经   19h22m48.14s 站赤纬 - 24°14'27.84" 视距离 1.36911773
        //方位角   42°31'40.53" 高度角   12°46'00.75"
        //恒星时   14h42m32.09s(平)   14h42m32.62s(真)

        //std::string ss = xingX(1, jd1, 116.383333, 39.900000);
        //fwrite(ss.c_str(), 1, ss.size(), gLoggerManager->stream);
        //fflush(gLoggerManager->stream);

        printf("%s\n", mmString_CStr(&s));
        fwrite(mmString_CStr(&s), 1, mmString_Size(&s), gLoggerManager->stream);
        fflush(gLoggerManager->stream);
        mmString_Destroy(&s);
    }
}

