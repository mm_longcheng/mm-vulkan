/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTestShadow_h__
#define __mmTestShadow_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUploader.h"
#include "vk/mmVKUniformType.h"

#include "vk/mmVKPipeline.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKTFModel.h"
#include "vk/mmVKScene.h"
#include "vk/mmVKNode.h"
#include "vk/mmVKTFObject.h"
#include "vk/mmVKTFFileIO.h"
#include "vk/mmVKRenderPass.h"
#include "vk/mmVKFrame.h"

#include "vk/mmVKShadow.h"

#include "parse/mmParseGLTF.h"

#include "core/mmPrefix.h"

struct mmActivityMaster;
struct mmVKCommandBuffer;

struct mmEventUpdate;
struct mmEventCursor;

struct mmTestShadow
{
    struct mmActivityMaster* pActivity;

    struct mmVKScene vScene;
    struct mmVKTFObject vAsset1;
    struct mmString vModelPath;

    float l;
    float angle[2];

    // debug.
    struct mmVKPoolInfo* pPoolInfo;
    struct mmVKPipelineInfo* pPipelineInfo;
    struct mmVKDescriptorSet vDescriptorSet;
    int debug;
    int ColorCascades;

    float lightPos[3];
};

void mmTestShadow_Init(struct mmTestShadow* p);
void mmTestShadow_Destroy(struct mmTestShadow* p);

void mmTestShadow_Prepare(struct mmTestShadow* p, struct mmActivityMaster* pActivity);
void mmTestShadow_Discard(struct mmTestShadow* p, struct mmActivityMaster* pActivity);

void mmTestShadow_Resize(struct mmTestShadow* p, struct mmActivityMaster* pActivity);
void mmTestShadow_Update(struct mmTestShadow* p, struct mmActivityMaster* pActivity);

void mmTestShadow_RecordRenderPass(struct mmTestShadow* p, struct mmVKCommandBuffer* cmd);
void mmTestShadow_RecordCommandBuffer(struct mmTestShadow* p, struct mmVKCommandBuffer* cmd);

void mmTestShadow_OnCursorBegan(struct mmTestShadow* p, struct mmEventCursor* content);
void mmTestShadow_OnCursorMoved(struct mmTestShadow* p, struct mmEventCursor* content);
void mmTestShadow_OnCursorEnded(struct mmTestShadow* p, struct mmEventCursor* content);
void mmTestShadow_OnCursorBreak(struct mmTestShadow* p, struct mmEventCursor* content);
void mmTestShadow_OnCursorWheel(struct mmTestShadow* p, struct mmEventCursor* content);

#include "core/mmSuffix.h"

#endif//__mmTestShadow_h__
