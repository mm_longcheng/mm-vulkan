/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestGLTFSkin.h"

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"
#include "core/mmStringUtils.h"
#include "core/mmFilePath.h"
#include "core/mmBinarySerach.h"

#include "algorithm/mmBase64.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix3x3.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "vk/mmVKPipeline.h"
#include "vk/mmVKTFModel.h"
#include "vk/mmVKTFPrimitiveHelper.h"
#include "vk/mmVKNode.h"
#include "vk/mmVKCommandBuffer.h"

#include "parse/mmParseJSONHelper.h"

void mmTestGLTFSkin_Init(struct mmTestGLTFSkin* p)
{
    mmVKScene_Init(&p->vScene);
    mmVKTFObject_Init(&p->vAsset1);
    mmVKTFObject_Init(&p->vAsset2);
    mmString_Init(&p->vModelPath);

    float pos1[3] = { -0.6f, 0.0f, 0.0f };
    float pos2[3] = { +0.6f, 0.0f, 0.0f };
    mmVKNode_SetPosition(&p->vAsset1.node, pos1);
    mmVKNode_SetPosition(&p->vAsset2.node, pos2);
}
void mmTestGLTFSkin_Destroy(struct mmTestGLTFSkin* p)
{
    mmString_Destroy(&p->vModelPath);
    mmVKTFObject_Destroy(&p->vAsset2);
    mmVKTFObject_Destroy(&p->vAsset1);
    mmVKScene_Destroy(&p->vScene);
}

void mmTestGLTFSkin_RecordCommandBuffer(struct mmTestGLTFSkin* p, struct mmVKCommandBuffer* cmd)
{
    mmVKScene_SetPipelineIndex(&p->vScene, mmVKPipelineWhole, 0, 0);
    mmVKTFObject_OnRecordCommand(cmd->cmdBuffer, &p->vScene, &p->vAsset1);
    mmVKTFObject_OnRecordCommand(cmd->cmdBuffer, &p->vScene, &p->vAsset2);
}

void mmTestGLTFSkin_Prepare(struct mmTestGLTFSkin* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    //struct mmContextMaster* pContextMaster = pActivity->pContextMaster;
    //struct mmSurfaceMaster* pSurfaceMaster = pActivity->pSurfaceMaster;
    //struct mmVKShaderLoader* pShaderLoader = &pActivity->vAssets.vShaderLoader;
    //struct mmVKPipelineCache* pPipelineCache = &pActivity->vAssets.vPipelineCache;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    //struct mmVKAssetsDefault* pAssetsDefault = &pActivity->vAssets.vAssetsDefault;
    //struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    //struct mmVKUploader*   pUploader = &pActivity->vRenderer.vUploader;
    //struct mmVKDevice*       pDevice = &pActivity->vRenderer.vDevice;
    //struct mmVKLayoutLoader* pLayoutLoader = &pActivity->vAssets.vLayoutLoader;
    //ktx_transcode_fmt_e  transcodefmt = pActivity->vRenderer.vPhysicalDevice.transcodefmt;
    //struct mmVKPassLoader* pPassLoader = &pActivity->vAssets.vPassLoader;
    //struct mmVKPoolLoader* pPoolLoader = &pActivity->vAssets.vPoolLoader;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;

    // const char* path = "model/SimpleSkinning.glb";
    // const char* path = "model/SimpleSkinning.gltf";
    // const char* path = "model/rotation_test02.gltf";
    // const char* path = "model/Flamingo.glb";
    const char* path = "model/Cesium_Man.glb";
    // const char* path = "model/Cesium_ManX.glb";

    const char* pAnimationName = "";
    // const char* pAnimationName = "Anim_0";
    // const char* pAnimationName = "Take 01";

    struct mmVKLightInfo hLightInfos[] =
    {
        {
            { -30.0f, -40.0f, 0.0f },   // float direction[3];
            { 0.0f, 0.0f, 0.0f },       // float position[3];
            { 1.0f, 1.0f, 1.0f },       // float color[3];
            0.0f,                       // float range;
            0.4f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypeDirectional, // int type;
            0,                          // int castShadow;
            4,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
    };
    
    struct mmVectorValue hInfos = mmVectorValue_Make(hLightInfos, struct mmVKLightInfo);
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    p->vScene.uboSceneFragment.lightCount = 1;
    mmVKScene_Prepare(&p->vScene, pAssets, &hInfos, pSwapchain->depthFormat);
    
    mmString_Assigns(&p->vModelPath, path);
    mmVKTFModelLoader_LoadFromPath(pModelLoader, path);

    mmVKTFObject_Prepare(&p->vAsset1, pAssets, path);
    mmVKTFObject_Prepare(&p->vAsset2, pAssets, path);
    mmVKTFObject_PlayAnimation(&p->vAsset1, pAnimationName);
    mmVKTFObject_PlayAnimation(&p->vAsset2, pAnimationName);
}

void mmTestGLTFSkin_Discard(struct mmTestGLTFSkin* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;

    mmVKTFObject_Discard(&p->vAsset2, pAssets);
    mmVKTFObject_Discard(&p->vAsset1, pAssets);

    mmVKTFModelLoader_UnloadByPath(pModelLoader, mmString_CStr(&p->vModelPath));

    mmVKScene_Discard(&p->vScene, pAssets);
}

void mmTestGLTFSkin_Resize(struct mmTestGLTFSkin* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    mmVKScene_UpdateUBOBuffer(&p->vScene, pUploader);
}

void mmTestGLTFSkin_Update(struct mmTestGLTFSkin* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;

    struct mmSurfaceMaster* pSurfaceMaster = pActivity->pSurfaceMaster;
    struct mmEventUpdate* pUpdate = &pSurfaceMaster->hUpdate;

    float interval = (float)pUpdate->interval;
    mmVKTFObject_Update(&p->vAsset1, pUploader, interval);
    mmVKTFObject_Update(&p->vAsset2, pUploader, interval);

    //float model[4][4];
    //float spin_angle = 0.4f;
    //float angle = (float)(spin_angle * M_PI / 180.0);
    //mmMat4x4Duplicate(model, p->vScene.uboSceneVS.model);
    //mmMat4x4RotateY(model, model, angle);
    //mmUniformMat4Assign(p->vScene.uboSceneVS.model, model);

    float view[4][4];
    float eye[3] = { 3.8f, 1.0f, 3.0f };
    //float eye[3] = { 12.8f, 6.0f, 15.0f };
    //float eye[3] = { 0.0f, 10.0f, 10.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0 };
    mmMat4x4LookAtRH(view, eye, origin, up);
    mmUniformMat4Assign(p->vScene.uboSceneWholeVert.View, view);

    mmUniformVec3Assign(p->vScene.uboSceneFragment.camera, eye);

    mmVKScene_UpdateUBOBufferSceneWhole(&p->vScene, pUploader);
    mmVKScene_UpdateUBOBufferSceneFragment(&p->vScene, pUploader);
}

