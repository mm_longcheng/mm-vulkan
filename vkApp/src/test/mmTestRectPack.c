/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestRectPack.h"

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"
#include "core/mmFilePath.h"

#include "vk/mmVKImageUploader.h"

#define STB_RECT_PACK_IMPLEMENTATION
#include "ext/stb_rect_pack.h"

void FillRectX(unsigned char* data, int w, stbrp_rect* e)
{
    int x = 0;
    int y = 0;

    int c = w * 4;
    int idx = 0;

    uint8_t r = (uint8_t)(rand() % 255);
    uint8_t g = (uint8_t)(rand() % 255);
    uint8_t b = (uint8_t)(rand() % 255);
    uint8_t a = 255;

    for (x = 0; x < e->w; ++x)
    {
        for (y = 0; y < e->h; ++y)
        {
            idx = (e->x + x) * 4 + (e->y + y) * c;
            data[idx + 0] = r;
            data[idx + 1] = g;
            data[idx + 2] = b;
            data[idx + 3] = a;
        }
    }
}

static
void
TextLayout_MakeFilesPathX(
    struct mmPackageAssets*                        pPackageAssets,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &pPackageAssets->hExternalFilesDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &pPackageAssets->hTextureCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

void RectPackTest(struct mmContextMaster* pContextMaster)
{
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

    int w = 1024;
    int h = 1024;
    stbrp_context c;
    stbrp_node nodes[1024];
    stbrp_init_target(&c, w, h, nodes, w);

    int i = 0;
    enum { N = 45, };
    stbrp_rect rects[N];

    srand(0);

    mmMemset(rects, 0, sizeof(stbrp_rect) * N);
    for (i = 0; i < N; ++i)
    {
        stbrp_rect* e = &rects[i];
        e->id = i;
        e->w = 16 + rand() % 256;
        e->h = 16 + rand() % 256;
    }

    //rects[0].id = 0;
    //rects[0].w = 100;
    //rects[0].h = 100;
    //rects[1].id = 1;
    //rects[1].w = 200;
    //rects[1].h = 100;

    int code = stbrp_pack_rects(&c, rects, N);

    const char* cPath = "";
    struct mmString hPath;
    mmString_Init(&hPath);
    TextLayout_MakeFilesPathX(pPackageAssets, "RectPack.png", &hPath);
    cPath = mmString_CStr(&hPath);

    size_t sz = w * h * 4;
    unsigned char* data = mmMalloc(sz);
    mmMemset(data, 0, sz);

    for (i = 0; i < N; ++i)
    {
        stbrp_rect* e = &rects[i];
        printf("%3d (%3d, %3d, %3d, %3d) %s\n", e->id, e->x, e->y, e->w, e->h, (e->was_packed ? "o" : "x"));
        if (e->was_packed)
        {
            FillRectX(data, w, e);
        }
    }

    mmVKImageR8G8B8A8WriteToPNGFile(cPath, data, w, h);
    mmFree(data);

    mmString_Destroy(&hPath);

    printf("%d \n", code);
}

void mmTestRectPack_Func(struct mmActivityMaster* p)
{
    RectPackTest(p->pContextMaster);
}

