/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestEntity.h"

#include "core/mmPoolElement.h"
#include "core/mmAlloc.h"
#include "core/mmClock.h"

#include "container/mmBitset.h"
#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeString.h"

#include "entityx/mmEntityWorld.h"

#include "entityx/mmEntityMaskTuple.h"
#include "entityx/mmEntitySystemManager.h"
#include "entityx/mmEntityComponentManager.h"

struct mmEntityComponentAudio
{
    int a;
};

void 
mmEntityComponentAudio_Init(
    struct mmEntityComponentAudio*                 p)
{
    p->a = 123;
}
void 
mmEntityComponentAudio_Destroy(
    struct mmEntityComponentAudio*                 p)
{
    p->a = 0;
}

void 
mmEntityComponentFactoryAudio_OnAttachEntity(
    struct mmEntityWorld*                          w, 
    struct mmEntity*                               e, 
    struct mmEntityComponentType*                  c, 
    void*                                          d)
{

}

void 
mmEntityComponentFactoryAudio_OnDetachEntity(
    struct mmEntityWorld*                          w, 
    struct mmEntity*                               e, 
    struct mmEntityComponentType*                  c, 
    void*                                          d)
{

}

const struct mmEntityComponentMetadata mmEntityComponentT =
{
    MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT,
    sizeof(struct mmEntityComponentAudio),
    &mmEntityComponentAudio_Init,
    &mmEntityComponentAudio_Destroy,
    &mmEntityComponentFactoryAudio_OnAttachEntity,
    &mmEntityComponentFactoryAudio_OnDetachEntity,
};

const char* Types[] =
{
    "T0",
    "T1",
};

struct mmEntitySystemAudio
{
    int a;
};

void 
mmEntitySystemAudio_Init(
    struct mmEntitySystemAudio*                    p)
{
    p->a = 456;
}
void 
mmEntitySystemAudio_Destroy(
    struct mmEntitySystemAudio*                    p)
{
    p->a = 0;
}

void 
AttachMaskTupleFuncType(
    struct mmEntityWorld*                          w, 
    struct mmEntitySystemType*                     s, 
    struct mmEntitySystemAudio*                    d)
{

}

void 
DetachMaskTupleFuncType(
    struct mmEntityWorld*                          w, 
    struct mmEntitySystemType*                     s, 
    struct mmEntitySystemAudio*                    d)
{

}

void 
mmEntitySystemAudio_UpdateEntity(
    struct mmEntityWorld*                          w, 
    struct mmEntitySystemType*                     s, 
    struct mmEntity*                               entity, 
    void**                                         c, 
    void*                                          evt)
{
    struct mmEntityComponentAudio* c0 = (struct mmEntityComponentAudio*)c[0];
    struct mmEntityComponentAudio* c1 = (struct mmEntityComponentAudio*)c[1];
    printf("%d %d\n", c0->a, c1->a);
}

const struct mmEntitySystemMetadata es =
{
    Types,
    MM_ARRAY_SIZE(Types),
    sizeof(struct mmString),
    &mmString_Init,
    &mmString_Destroy,
    &AttachMaskTupleFuncType,
    &DetachMaskTupleFuncType,
};

void mmTestEntity_Func(void)
{
    // World w;
    // w.AddComponent("C0", &gC0Component);
    // w.AddComponent("C1", &gC1Component);
    // w.AddComponent("C2", &gC2Component);
    //
    // w.AddSystem("S0", &gS0System);
    // w.AddSystem("S1", &gS1System);
    //
    // Entity e0 = w.ProduceEntity();
    // Component c00 = w.AddComponent(e0, "C0");
    // Component c01 = w.AddComponent(e0, "C1");
    // w.AttachEntityMaskTuple(e0);
    //
    // c00.AddChild(c10);
    // c00.RmvChild(c10);
    //
    // w.DetachEntityMaskTuple(e0);
    // w.ClearEntityComponent(e0);
    // w.RecycleEntity(e0);
    //
    // w.RmvSystem("S1", &gS1System);
    // w.RmvSystem("S0", &gS0System);
    //
    // w.RmvComponent("C2", &gC2Component);
    // w.RmvComponent("C1", &gC1Component);
    // w.RmvComponent("C0", &gC0Component);

    // Component c00 = w.GetComponent(e0, "C0");

    {
        struct mmEntitySystemType* s = NULL;
        struct mmEntity* e0 = NULL;

        struct mmEntityWorld ew;

        // size_t ewsz = sizeof(struct mmEntity);

        mmEntityWorld_Init(&ew);

        // component
        mmEntityWorld_AddComponent(&ew, "T0", &mmEntityComponentT);
        mmEntityWorld_AddComponent(&ew, "T1", &mmEntityComponentT);

        // system
        s = mmEntityWorld_AddSystem(&ew, "S0", &es);

        e0 = mmEntityWorld_Produce(&ew);

        mmEntityWorld_AddEntityComponent(&ew, e0, "T0");
        mmEntityWorld_AddEntityComponent(&ew, e0, "T1");
        mmEntityWorld_AttachEntityMaskTuple(&ew, e0);

        mmEntityWorld_UpdateSystem(&ew, s, NULL, &mmEntitySystemAudio_UpdateEntity);

        mmEntityWorld_DetachEntityMaskTuple(&ew, e0);
        mmEntityWorld_ClearEntityComponent(&ew, e0);

        mmEntityWorld_Recycle(&ew, e0);

        mmEntityWorld_RmvSystem(&ew, "S0");

        mmEntityWorld_RmvComponent(&ew, "T1");
        mmEntityWorld_RmvComponent(&ew, "T0");

        mmEntityWorld_Destroy(&ew);
    }

    {
        int i = 0;
        int d = 1;
        int N = 10;

        struct mmClock c;

        mmUInt64_t t0;
        mmUInt64_t t1;
        mmUInt64_t ta = 0;

        mmClock_Init(&c);
        mmClock_Reset(&c);

        printf("Sleep(%d)  ---\n", d);

        for (i = 0; i < N; i++)
        {
            t0 = mmClock_Microseconds(&c);

            mmMSleep(d);

            // DWORD tc = GetTickCount();

            t1 = mmClock_Microseconds(&c);

            ta += t1 - t0;

            // sleep us = 1655
            printf("Sleep   us = %" PRIu64 "\n", t1 - t0);
        }

        printf("average us = %" PRIu64 "\n", ta / N);


        mmClock_Destroy(&c);
    }
}

