/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestGlslangValidator.h"

#include "mmActivityMaster.h"

#include "nwsi/mmContextMaster.h"

#include "glsl/mmGlslangValidator.h"

void mmTestGlslangValidator_Func(struct mmActivityMaster* p)
{
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;

    int code = -1;
    const char* o = NULL;
    struct mmString ostr;

    struct mmClock clock;
    mmUInt64_t t0, t1, t2, t3;

    struct mmString* dir = &pPackageAssets->hExternalFilesDirectory;

    mmString_Init(&ostr);
    mmString_Sprintf(&ostr, "%s/pbr.vert.spv", mmString_CStr(dir));

    mmClock_Init(&clock);
    mmClock_Reset(&clock);

    o = mmString_CStr(&ostr);

    mmGlslangValidatorSetFileContext(pFileContext);

    mmGlslangValidatorReset();

    t0 = mmClock_Microseconds(&clock);
    code = mmGlslangValidatorCompile("-Iprograms/spirv/pbr", "programs/spirv/pbr/pbr.vert", o);
    t1 = mmClock_Microseconds(&clock);

    mmGlslangValidatorReset();

    t2 = mmClock_Microseconds(&clock);
    code = mmGlslangValidatorCompile("-Iprograms/spirv/pbr", "programs/spirv/pbr/pbr.vert", o);
    t3 = mmClock_Microseconds(&clock);

    // debug
    //     t1 3044179l
    //     t2 3029273l
    // release
    //     t1 95916l
    //     t2 83870l
    printf("t1 %lu\n", (mmULong_t)(t1 - t0));
    printf("t2 %lu\n", (mmULong_t)(t3 - t2));

    printf("code %d\n", code);

    mmClock_Destroy(&clock);

    mmString_Destroy(&ostr);
}

