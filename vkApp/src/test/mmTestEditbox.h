/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTestEditbox_h__
#define __mmTestEditbox_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "core/mmPrefix.h"

//struct mmActivityMaster;
//
//int mmTextIsBlankA(char c);
//int mmTextIsBlankW(unsigned int c);
//int mmTextIsSeparator(unsigned int c);
//int mmTextIsWordBoundaryR(struct mmUtf16String* str, int idx);
//int mmTextIsWordBoundaryL(struct mmUtf16String* str, int idx);
//int mmTextIsWordBoundary(struct mmUtf16String* str, int idx);
//int mmTextMoveToWordPrev(struct mmUtf16String* str, int c);
//int mmTextMoveToWordNext(struct mmUtf16String* str, int c);
//
//struct mmClipboard;
//
//struct Editbox;
//
//void dump_utf16(struct Editbox* pEditbox);
//
//struct Editbox
//{
//    struct mmUIView* view;
//    struct mmUtf16String text;
//
//    // caret index
//    size_t c;
//
//    // selection left
//    size_t l;
//    // selection right
//    size_t r;
//
//    // drag anchor index
//    size_t anchor;
//};
//
//int Editbox_performCopy(struct Editbox* pEditbox, struct mmClipboard* clipboard);
//int Editbox_performCut(struct Editbox* pEditbox, struct mmClipboard* clipboard);
//int Editbox_performPaste(struct Editbox* pEditbox, struct mmClipboard* clipboard);
//
//size_t Editbox_getSelectionStartIndex(struct Editbox* pEditbox);
//size_t Editbox_getSelectionEndIndex(struct Editbox* pEditbox);
//size_t Editbox_getSelectionLength(struct Editbox* pEditbox);
//size_t Editbox_getCaretIndex(struct Editbox* pEditbox);
//
//void Editbox_setCaretIndex(struct Editbox* pEditbox, size_t caret_pos);
//void Editbox_setSelection(struct Editbox* pEditbox, size_t start_pos, size_t end_pos);
//void Editbox_clearSelection(struct Editbox* pEditbox);
//void Editbox_eraseSelectedText(struct Editbox* pEditbox);
//
//void Editbox_handleTextChanged(struct Editbox* pEditbox);
//
//void Editbox_handleBackspace(struct Editbox* pEditbox);
//void Editbox_handleDelete(struct Editbox* pEditbox);
//
//void Editbox_InsertUtf16(struct Editbox* pEditbox, mmUInt16_t* s, size_t l);
//void Editbox_ReplaceUtf16(struct Editbox* pEditbox, size_t o, size_t n, mmUInt16_t* s, size_t l);
//
//void Editbox_handleCharLeft(struct Editbox* pEditbox, mmUInt32_t sysKeys);
//void Editbox_handleWordLeft(struct Editbox* pEditbox, mmUInt32_t sysKeys);
//void Editbox_handleCharRight(struct Editbox* pEditbox, mmUInt32_t sysKeys);
//void Editbox_handleWordRight(struct Editbox* pEditbox, mmUInt32_t sysKeys);
//
//extern void mmTestEditbox_Func(struct mmActivityMaster* p);

#include "core/mmSuffix.h"

#endif//__mmTestEditbox_h__
