/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestTextLayout.h"

#include "mmActivityMaster.h"

#include "core/mmFilePath.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmTextLayout.h"

#include "vk/mmVKImageUploader.h"

static
void
TextLayout_MakeFilesPath(
    struct mmPackageAssets*                        pPackageAssets,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &pPackageAssets->hExternalFilesDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &pPackageAssets->hTextureCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

void mmTestTextLayout_Func(struct mmActivityMaster* p)
{
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    //struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    //struct mmVKImageLoader* pImageLoader = &p->vImageLoader;


    //const mmUInt16_t cText[] = { 'H', 'e', 'l', 'v', 'e', ' ', 'i', 'c', 'a', 'y', 'p', 'f', 0, };
    const mmUInt16_t cText[] = { 'H', 'p', 'y', 'f', 0x8f6c, 0x6362, 0x683c, 0x5f0f, 0, };
    //const mmUInt16_t cFamilyName[] = { 0x65b0, 0x5b8b, 0x4f53, 0, };
    //const mmUInt16_t cFamilyName[] = { 0x5fae, 0x8f6f, 0x96c5, 0x9ed1, 0, };
    //const mmUInt16_t cFamilyName[] = { 0x6977, 0x4f53, 0, };
    //const mmUInt16_t cFamilyName[] = { 0x5b8b, 0x4f53, 0, };

    enum mmFontFamilyChinese_t
    {
        mmFontFamilyChineseHeiTi = 0,
        mmFontFamilyChineseKaiTi = 1,
        mmFontFamilyChineseSongTi = 2,
        mmFontFamilyChineseFangSong = 3,
    };

    // 9ed1,4f53,6977,4f53,5b8b,4f53,4eff,5b8b
    const mmUInt16_t cFamilyName0[] = { 0x9ed1, 0x4f53, 0, };
    const mmUInt16_t cFamilyName1[] = { 0x6977, 0x4f53, 0, };
    const mmUInt16_t cFamilyName2[] = { 0x5b8b, 0x4f53, 0, };
    const mmUInt16_t cFamilyName3[] = { 0x4eff, 0x5b8b, 0, };
    const mmUInt16_t* cFamilyNameWindows[] =
    {
        cFamilyName0,
        cFamilyName1,
        cFamilyName2,
        cFamilyName3,
    };

    // windows
    // HeiTi
    const mmUInt16_t cFamilyName00[] = { 'S', 'i', 'm', 'H', 'e', 'i', 0, };
    // KaiTi
    const mmUInt16_t cFamilyName01[] = { 'K', 'a', 'i', 'T', 'i', 0, };
    // SongTi
    const mmUInt16_t cFamilyName02[] = { 'S', 'i', 'm', 'S', 'u', 'n', 0, };
    // FangSong
    const mmUInt16_t cFamilyName03[] = { 'F', 'a', 'n', 'g', 'S', 'o', 'n', 'g', 0, };

    const mmUInt16_t* cFamilyNameWindowsX[] =
    {
        cFamilyName00,
        cFamilyName01,
        cFamilyName02,
        cFamilyName03,
    };

    // ios
    // HeiTi
    const mmUInt16_t cFamilyName10[] = { 'S', 'T', 'H', 'e', 'i', 't', 'i', 0, };
    // KaiTi
    const mmUInt16_t cFamilyName11[] = { 'S', 'T', 'K', 'a', 'i', 't', 'i', 0, };
    // SongTi
    const mmUInt16_t cFamilyName12[] = { 'S', 'T', 'S', 'o', 'n', 'g', 0, };
    // FangSong
    const mmUInt16_t cFamilyName13[] = { 'S', 'T', 'F', 'a', 'n', 'g', 's', 'o', 'n', 'g', 0, };

    const mmUInt16_t* cFamilyNameApple[] =
    {
        cFamilyName10,
        cFamilyName11,
        cFamilyName12,
        cFamilyName13,
    };

    // android
    const mmUInt16_t cFamilyName20[] = { 's', 'a', 'n', 's', '-', 's', 'e', 'r', 'i', 'f', 0, };
    const mmUInt16_t cFamilyName21[] = { 's', 'a', 'n', 's', '-', 's', 'e', 'r', 'i', 'f', 0, };
    const mmUInt16_t cFamilyName22[] = { 's', 'a', 'n', 's', '-', 's', 'e', 'r', 'i', 'f', 0, };
    const mmUInt16_t cFamilyName23[] = { 's', 'a', 'n', 's', '-', 's', 'e', 'r', 'i', 'f', 0, };

    const mmUInt16_t* cFamilyNameAndroid[] =
    {
        cFamilyName20,
        cFamilyName21,
        cFamilyName22,
        cFamilyName23,
    };

    const mmUInt16_t** cFamilyNames[] =
    {
        cFamilyNameWindows,
        cFamilyNameApple,
        cFamilyNameAndroid,
        cFamilyNameWindowsX,
    };

    const mmUInt16_t* cFamilyName = cFamilyNames[0][mmFontFamilyChineseHeiTi];

    float hFrameW = 400;
    float hFrameH = 150;

    struct mmString hPath;
    struct mmTextLayout tl;
    const char* cPath = "";

    mmString_Init(&hPath);
    mmTextLayout_Init(&tl);

    TextLayout_MakeFilesPath(pPackageAssets, "TextLayout.png", &hPath);
    cPath = mmString_CStr(&hPath);

    mmTextLayout_SetGraphicsFactory(&tl, &pContextMaster->hGraphicsFactory);

    mmTextLayout_SetWindowSize(&tl, hFrameW, hFrameH);
    //mmTextLayout_SetLayoutRect(&tl, 50, 0, hFrameW - 100, hFrameH);
    mmTextLayout_SetLayoutRect(&tl, 0, 0, hFrameW, hFrameH);

    mmTextLayout_SetMasterFontFamilyName(&tl, cFamilyName);
    mmTextLayout_SetMasterFontSize(&tl, 60.0f);

    mmTextLayout_SetParagraphTextAlignment(&tl, mmTextAlignmentCenter);
    mmTextLayout_SetParagraphAlignment(&tl, mmTextParagraphAlignmentCenter);

    mmTextLayout_SetShadowBlur(&tl, 3);
    mmTextLayout_SetShadowOffset(&tl, 4, 4);
    mmTextLayout_SetShadowColor(&tl, 0xFF00FFF0);

    mmTextLayout_SetMasterForegroundColor(&tl, 0xFFAFCF0F);
    mmTextLayout_SetMasterBackgroundColor(&tl, 0xFF0FCFAF);

    mmTextLayout_SetMasterUnderlineWidth(&tl, -1);
    mmTextLayout_SetMasterUnderlineColor(&tl, 0xFFFF0000);
    mmTextLayout_SetMasterStrikeThruWidth(&tl, -1);
    mmTextLayout_SetMasterStrikeThruColor(&tl, 0xFF0000FF);

    mmTextLayout_SetTextWeaks(&tl, cText);

    mmTextLayout_OnFinishLaunching(&tl);

    mmTextLayout_Draw(&tl);

    const void* pBuffer = mmTextLayout_BufferLock(&tl);

    mmVKImageR8G8B8A8WriteToPNGFile(cPath, (unsigned char*)pBuffer, (int)hFrameW, (int)hFrameH);

    //size_t size = (uint32_t)hFrameW * (uint32_t)hFrameH * 4;
    //mmVKImageLoader_ReloadFromR8G8B8A8Memory(
    //    pImageLoader,
    //    "memory/TextLayout.png",
    //    (const uint8_t*)pBuffer,
    //    (size_t)size,
    //    (uint32_t)hFrameW,
    //    (uint32_t)hFrameH);

    mmTextLayout_BufferUnLock(&tl, pBuffer);

    mmTextLayout_OnBeforeTerminate(&tl);

    mmTextLayout_Destroy(&tl);
    mmString_Destroy(&hPath);
}

