/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestSheetLoader.h"

#include "mmActivityMaster.h"

#include "nwsi/mmContextMaster.h"

#include "vk/mmVKSheetLoader.h"

#include "core/mmFilePath.h"
#include "core/mmPropertySet.h"
#include "core/mmPropertyHelper.h"
#include "core/mmEventSet.h"
#include "core/mmCPUInfo.h"

#include "container/mmRbtreeString.h"
#include "container/mmRbtreeU32.h"
#include "container/mmRbtsetU32.h"

//#include "ext/target.h"

//#include <sal.h>
//#define _Null_                            _SAL2_Source_(_Null_, (), _Null_impl_)
//#define _Notnull_                         _SAL2_Source_(_Notnull_, (), _Notnull_impl_)
//#define _Maybenull_                       _SAL2_Source_(_Maybenull_, (), _Maybenull_impl_)

#define _mmNull_
#define _mmNullable_
#define _mmNonenull_

struct myType
{
    struct mmPropertySet s;
    int a;
};
void myType_Init(struct myType* p);
void myType_Destroy(struct myType* p);
void myType_SetValueA(struct myType* p, const int* value);
void myType_GetValueA(const struct myType* p, int* value);

void myType_Init(struct myType* p)
{
    mmPropertySet_Init(&p->s);
    p->a = 0;
    
    static const char cOrigin[] = "myType";
    // bind object for this.
    mmPropertySet_SetObject(&p->s, p);
    // bind member property.
    mmPropertyDefine(&p->s,
        "A", 0, struct myType,
        a, UInt, "0",
        &myType_SetValueA,
        &myType_GetValueA,
        cOrigin,
        "a value");
}
void myType_Destroy(struct myType* p)
{
    mmPropertyRemove(&p->s, "A");
    
    p->a = 0;
    mmPropertySet_Destroy(&p->s);
}

void myType_SetValueA(struct myType* p, const int* value)
{
    p->a = (*value);
}
void myType_GetValueA(const struct myType* p, int* value)
{
    (*value) = p->a;
}

void mmRedisHash_U32HmgetFormatX(struct mmRbtsetU32* rbtset, struct mmString* format)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    size_t sz = rbtset->size + 10 + rbtset->size * MM_INT32_LEN;
    //
    mmString_Clear(format);
    mmString_Reserve(format, sz);
    mmString_Sprintf(format, "hmget %%s");
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mmString_Sprintf(format, " %" PRIu32 "", it->k);
    }
}

static void mmRbtreeStringU32Func(void)
{
    {
        struct mmString format;
        struct mmRbtsetU32 t;
        mmString_Init(&format);
        mmRbtsetU32_Init(&t);
        
        mmRbtsetU32_Add(&t, 1);
        mmRbtsetU32_Add(&t, 12);
        mmRbtsetU32_Add(&t, 123);
        
        mmRedisHash_U32HmgetFormatX(&t, &format);
        printf("%s\n", mmString_CStr(&format));
        
        mmRbtsetU32_Destroy(&t);
        mmString_Destroy(&format);
    }
    
    {
        struct mmString a = mmString_Make("a");
        struct mmString b = mmString_Make("1");
        int v = mmString_Compare(&a, &b);
        printf("%d\n", v);
    }

    
    {
        struct mmRbtreeU32U32 t;
        mmRbtreeU32U32_Init(&t);
        
        mmRbtreeU32U32_Set(&t, 1, 1);
        mmRbtreeU32U32_Set(&t, 12, 1);
        mmRbtreeU32U32_Set(&t, 123, 1);
        
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32U32Iterator* it = NULL;
        //
        n = mmRb_First(&t.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
            n = mmRb_Next(n);
            printf("%u\n", it->k);
        }
        
        mmRbtreeU32U32_Destroy(&t);
    }
    {
        struct mmString w;
        struct mmRbtreeStringU32 t;
        mmRbtreeStringU32_Init(&t);
        
        mmString_MakeWeaks(&w, "1");
        mmRbtreeStringU32_Set(&t, &w, 1);
        
        mmString_MakeWeaks(&w, "12");
        mmRbtreeStringU32_Set(&t, &w, 1);
        
        mmString_MakeWeaks(&w, "a");
        mmRbtreeStringU32_Set(&t, &w, 1);
        
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringU32Iterator* it = NULL;
        //
        n = mmRb_First(&t.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeStringU32Iterator, n);
            n = mmRb_Next(n);
            printf("%s\n", mmString_CStr(&it->k));
        }
        
        mmRbtreeStringU32_Destroy(&t);
    }
}

static void PropertySetFunc(void)
{
    
    // 编译构建常量字符串，存储在全局区，没有堆分配
    const struct mmString c = mmString_Make("abc");
    const char* cs = mmString_Data(&c);
    size_t cn = mmString_Size(&c);
    printf("cs: %s %d\n", cs, (int)cn);
    
    // 动态构建常量字符串，存储在局部区，没有堆分配
    struct mmString w;
    mmString_MakeWeaks(&w, "123");
    // 能正常重置，以便于重新定义成可能的动态串
    mmString_Reset(&w);
    
    // 动态构建普通字符串，存储在局部区，可能堆分配
    // x64位 长度超过22就会触发堆分配
    struct mmString s;
    mmString_Init(&s);
    mmString_Assigns(&s, "456");
    mmString_Destroy(&s);
    
    int value = 0;
    // weak string not need Allocator.
    struct mmString v;
    struct myType t;
    mmString_MakeWeaks(&v, "123");
    myType_Init(&t);
    printf("a: %d\n", t.a);
    // a: 0
    mmPropertySet_SetValueString(&t.s, "A", &v);
    mmPropertySet_GetValueMember(&t.s, "A", &value);
    // a: 123 value: 123
    printf("a: %d value: %d\n", t.a, value);
    myType_Destroy(&t);
}


void HandlerFunc0(struct mmEventSet* p, struct mmEventArgs* evt)
{
    printf("handle 0\n");
}
void HandlerFunc1(struct mmEventSet* p, struct mmEventArgs* evt)
{
    mmEventSet_UnSubscribeEvent(p, "Name", &HandlerFunc0, p);
    printf("handle 1\n");
}
void HandlerFunc2(struct mmEventSet* p, struct mmEventArgs* evt)
{
    printf("handle 2\n");
}
static void EventSetFunc(void)
{
    struct mmEventArgs hArgs;
    struct mmEventSet s;
    mmEventSet_Init(&s);
    mmEventArgs_Reset(&hArgs);
    mmEventSet_SubscribeEvent(&s, "Name", &HandlerFunc0, &s);
    
    // handle 0
    mmEventSet_FireEvent(&s, "Name", &hArgs);
    
    mmEventSet_SubscribeEvent(&s, "Name", &HandlerFunc1, &s);
    mmEventSet_SubscribeEvent(&s, "Name", &HandlerFunc2, &s);
    
    // handle 0
    // handle 1 <handle 0 将在这里被安全接注册掉>
    // handle 2 后面的事件能正确处理
    mmEventSet_FireEvent(&s, "Name", &hArgs);
    
    // handle 1
    // handle 2
    mmEventSet_FireEvent(&s, "Name", &hArgs);
    
    mmEventSet_UnSubscribeEvent(&s, "Name", &HandlerFunc1, &s);
    
    // handle 2
    mmEventSet_FireEvent(&s, "Name", &hArgs);
    
    mmEventSet_Destroy(&s);
}

static void func_000(_mmNonenull_ void* a)
{

}

void mmTestSheetLoader_Func(struct mmActivityMaster* p)
{
    mmRbtreeStringU32Func();
    EventSetFunc();
    PropertySetFunc();
    func_000(NULL);

    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;

    struct mmVKSheetLoader l;

    mmVKSheetLoader_Init(&l);
    mmVKSheetLoader_SetFileContext(&l, pFileContext);
    mmVKSheetLoader_LoadFromFile(&l, "imagesets/Image_2.sheet");
    mmVKSheetLoader_LoadFromFile(&l, "imagesets/Image_2.sheet");
    mmVKSheetLoader_Destroy(&l);

    struct mmString hKTXPath;
    mmString_Init(&hKTXPath);
    mmPathReplaceSuffix("a/b/Image_2.png", ".ktx2", &hKTXPath);
    mmString_Destroy(&hKTXPath);
}

