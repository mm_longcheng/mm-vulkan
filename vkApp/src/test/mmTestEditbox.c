/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestEditbox.h"

#include "core/mmKeyCode.h"
#include "core/mmLogger.h"

#include "unicode/mmUTF16.h"

#include "nwsi/mmUTFHelper.h"
#include "nwsi/mmClipboard.h"

//void dump_utf16(struct Editbox* pEditbox)
//{
//    struct mmLogger* gLogger = mmLogger_Instance();
//
//    struct mmString hUTF8String;
//
//    struct mmUtf16String* s = &pEditbox->text;
//    mmString_Init(&hUTF8String);
//
//    mmUTFUtil_UTF16toUTF8(s, &hUTF8String);
//
//    const char* text = (const char*)mmString_CStr(&hUTF8String);
//    mmLogger_LogI(gLogger, "text: (l:%d, r:%d, c:%d)%s",
//        (int)pEditbox->l, (int)pEditbox->r, (int)pEditbox->c, text);
//
//    //    int i = 0;
//    //    for (i = 0; i < hUTF16Size; ++i)
//    //    {
//    //        uint16_t c = mmUtf16String_At(s, i);
//    //        printf("%d ", (int)c);
//    //    }
//    //    printf("\n");
//
//    mmString_Destroy(&hUTF8String);
//}
//
//int mmTextIsBlankA(char c) 
//{
//    return c == ' ' || c == '\t';
//}
//
//int mmTextIsBlankW(unsigned int c)
//{
//    return c == ' ' || c == '\t' || c == 0x3000;
//}
//
//int mmTextIsSeparator(unsigned int c)
//{
//    return
//        /* blank */
//        c == ' '    || 
//        c == '\t'   || 
//        c == 0x3000 ||
//        /* symbol */
//        c == ','    ||
//        c == ';'    ||
//        c == '('    || c == ')'    ||
//        c == '{'    || c == '}'    ||
//        c == '['    || c == ']'    ||
//        c == '|'    ||
//        c == '\n'   ||
//        c == '\r';
//}
//
//int mmTextIsWordBoundaryR(struct mmUtf16String* str, int idx)
//{
//    if (idx > 0)
//    {
//        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
//        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
//        int s0 = mmTextIsSeparator(idx0);
//        int s1 = mmTextIsSeparator(idx1);
//        return (s0 && !s1);
//    }
//    else
//    {
//        return 1;
//    }
//}
//int mmTextIsWordBoundaryL(struct mmUtf16String* str, int idx)
//{
//    if (idx > 0)
//    {
//        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
//        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
//        int s0 = mmTextIsSeparator(idx0);
//        int s1 = mmTextIsSeparator(idx1);
//        return (!s0 && s1);
//    }
//    else
//    {
//        return 1;
//    }
//}
//
//int mmTextIsWordBoundary(struct mmUtf16String* str, int idx)
//{
//    if (idx > 0)
//    {
//        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
//        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
//        int s0 = mmTextIsSeparator(idx0);
//        int s1 = mmTextIsSeparator(idx1);
//        return (s0 && !s1);
//    }
//    else
//    {
//        return 1;
//    }
//}
//
//int mmTextMoveToWordPrev(struct mmUtf16String* str, int c)
//{
//    // always move at least one character
//    --c;
//
//    while (c >= 0 && !mmTextIsWordBoundary(str, c))
//    {
//        --c;
//    }
//
//    return (c < 0) ? 0 : c;
//}
//int mmTextMoveToWordNext(struct mmUtf16String* str, int c)
//{
//    const int l = (int)mmUtf16String_Size(str);
//
//    // always move at least one character
//    ++c;
//
//    while (c < l && !mmTextIsWordBoundary(str, c))
//    {
//        ++c;
//    }
//
//    return (c > l) ? l : c;
//}
//
//int Editbox_performCopy(struct Editbox* pEditbox, struct mmClipboard* clipboard)
//{
//    if (Editbox_getSelectionLength(pEditbox) == 0)
//    {
//        return MM_FALSE;
//    }
//    else
//    {
//        struct mmUtf16String selectedText;
//        size_t start = Editbox_getSelectionStartIndex(pEditbox);
//        const mmUtf16_t* d = mmUtf16String_Data(&pEditbox->text);
//        const mmUtf16_t* s = d + start;
//        size_t n = Editbox_getSelectionLength(pEditbox);
//        mmUtf16String_MakeWeaksn(&selectedText, s, n);
//
//        mmClipboard_SetText(clipboard, &selectedText);
//        return MM_TRUE;
//    }
//}
//int Editbox_performCut(struct Editbox* pEditbox, struct mmClipboard* clipboard)
//{
//    //if (isReadOnly())
//    //    return false;
//
//    if (!Editbox_performCopy(pEditbox, clipboard))
//    {
//        return MM_FALSE;
//    }
//    else
//    {
//        Editbox_handleDelete(pEditbox);
//        return MM_TRUE;
//    }
//}
//int Editbox_performPaste(struct Editbox* pEditbox, struct mmClipboard* clipboard)
//{
//    //if (isReadOnly())
//    //    return false;
//
//    struct mmUtf16String clipboardText;
//    mmClipboard_GetText(clipboard, &clipboardText);
//
//    if (mmUtf16String_Empty(&clipboardText))
//    {
//        return MM_FALSE;
//    }
//    else
//    {
//        size_t start = Editbox_getSelectionStartIndex(pEditbox);
//        size_t n = Editbox_getSelectionLength(pEditbox);
//
//        mmUtf16String_Erase(&pEditbox->text, start, n);
//
//        mmUtf16String_Insert(&pEditbox->text, start, &clipboardText);
//
//        // erase selection using mode that does not modify getText()
//        // (we just want to update state)
//        Editbox_eraseSelectedText(pEditbox);
//
//        // advance caret (done first so we can "do stuff" in event
//        // handlers!)
//        pEditbox->c += mmUtf16String_Size(&clipboardText);
//
//        Editbox_handleTextChanged(pEditbox);
//        return MM_TRUE;
//    }
//}
//
//size_t Editbox_getSelectionStartIndex(struct Editbox* pEditbox)
//{
//    return (pEditbox->l != pEditbox->r) ? pEditbox->l : pEditbox->c;
//}
//
//size_t Editbox_getSelectionEndIndex(struct Editbox* pEditbox)
//{
//    return (pEditbox->l != pEditbox->r) ? pEditbox->r : pEditbox->c;
//}
//size_t Editbox_getSelectionLength(struct Editbox* pEditbox)
//{
//    return pEditbox->r - pEditbox->l;
//}
//size_t Editbox_getCaretIndex(struct Editbox* pEditbox)
//{
//    return pEditbox->c;
//}
//void Editbox_setCaretIndex(struct Editbox* pEditbox, size_t caret_pos)
//{
//    // make sure new position is valid
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//    if (caret_pos > length)
//    {
//        caret_pos = length;
//    }
//
//    pEditbox->c = caret_pos;
//}
//void Editbox_setSelection(struct Editbox* pEditbox, size_t start_pos, size_t end_pos)
//{
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//    // ensure selection start point is within the valid range
//    if (start_pos > length)
//    {
//        start_pos = length;
//    }
//
//    // ensure selection end point is within the valid range
//    if (end_pos > length)
//    {
//        end_pos = length;
//    }
//
//    // ensure start is before end
//    if (start_pos > end_pos)
//    {
//        size_t tmp = end_pos;
//        end_pos = start_pos;
//        start_pos = tmp;
//    }
//
//    pEditbox->l = start_pos;
//    pEditbox->r = end_pos;
//}
//void Editbox_clearSelection(struct Editbox* pEditbox)
//{
//    // perform action only if required.
//    if (Editbox_getSelectionLength(pEditbox) != 0)
//    {
//        Editbox_setSelection(pEditbox, 0, 0);
//    }
//}
//void Editbox_eraseSelectedText(struct Editbox* pEditbox)
//{
//    if (Editbox_getSelectionLength(pEditbox) != 0)
//    {
//        Editbox_setCaretIndex(pEditbox, pEditbox->l);
//        Editbox_clearSelection(pEditbox);
//    }
//}
//
//void Editbox_handleTextChanged(struct Editbox* pEditbox)
//{
//    // clear selection
//    Editbox_clearSelection(pEditbox);
//
//    // make sure caret is within the text
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//    if (pEditbox->c > length)
//    {
//        Editbox_setCaretIndex(pEditbox, length);
//    }
//}
//
//void Editbox_handleBackspace(struct Editbox* pEditbox)
//{
//    size_t sl = Editbox_getSelectionLength(pEditbox);
//    if (sl != 0)
//    {
//        size_t ssi = Editbox_getSelectionStartIndex(pEditbox);
//        mmUtf16String_Erase(&pEditbox->text, ssi, sl);
//
//        Editbox_eraseSelectedText(pEditbox);
//
//        Editbox_handleTextChanged(pEditbox);
//    }
//    else if (Editbox_getCaretIndex(pEditbox) > 0)
//    {
//        mmUtf16_t c = mmUtf16String_At(&pEditbox->text, pEditbox->c - 1);
//        int n = (U16_IS_SURROGATE(c) && U16_IS_TRAIL(c)) ? 2 : 1;
//
//        mmUtf16String_Erase(&pEditbox->text, pEditbox->c - n, n);
//
//        Editbox_setCaretIndex(pEditbox, pEditbox->c - n);
//
//        Editbox_handleTextChanged(pEditbox);
//    }
//}
//void Editbox_handleDelete(struct Editbox* pEditbox)
//{
//    size_t sl = Editbox_getSelectionLength(pEditbox);
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//    if (sl != 0)
//    {
//        size_t ssi = Editbox_getSelectionStartIndex(pEditbox);
//        mmUtf16String_Erase(&pEditbox->text, ssi, sl);
//
//        Editbox_eraseSelectedText(pEditbox);
//
//        Editbox_handleTextChanged(pEditbox);
//    }
//    else if (Editbox_getCaretIndex(pEditbox) < length)
//    {
//        mmUtf16_t c = mmUtf16String_At(&pEditbox->text, pEditbox->c);
//        int n = (U16_IS_SURROGATE(c) && U16_IS_LEAD(c)) ? 2 : 1;
//
//        mmUtf16String_Erase(&pEditbox->text, pEditbox->c, n);
//
//        Editbox_handleTextChanged(pEditbox);
//    }
//}
//void Editbox_InsertUtf16(struct Editbox* pEditbox, mmUInt16_t* s, size_t l)
//{
//    size_t ssi = Editbox_getSelectionStartIndex(pEditbox);
//    size_t sl = Editbox_getSelectionLength(pEditbox);
//
//    mmUtf16String_Replacesn(&pEditbox->text, ssi, sl, s, l);
//
//    // erase selection using mode that does not modify getText()
//    // (we just want to update state)
//    Editbox_eraseSelectedText(pEditbox);
//
//    // advance caret (done first so we can "do stuff" in event
//    // handlers!)
//    pEditbox->c += l;
//
//    Editbox_handleTextChanged(pEditbox);
//}
//void Editbox_ReplaceUtf16(struct Editbox* pEditbox, size_t o, size_t n, mmUInt16_t* s, size_t l)
//{
//    mmUtf16String_Replacesn(&pEditbox->text, o, n, s, l);
//
//    // Not trigger Editbox_handleTextChanged.
//    // We modify the cursor externally. 
//}
//void Editbox_handleCharLeft(struct Editbox* pEditbox, mmUInt32_t sysKeys)
//{
//    if (pEditbox->c > 0)
//    {
//        mmUtf16_t c = mmUtf16String_At(&pEditbox->text, pEditbox->c - 1);
//        int n = (U16_IS_SURROGATE(c) && U16_IS_TRAIL(c)) ? 2 : 1;
//        
//        Editbox_setCaretIndex(pEditbox, pEditbox->c - n);
//    }
//
//    if (sysKeys & MM_MODIFIER_SHIFT)
//    {
//        Editbox_setSelection(pEditbox, pEditbox->c, pEditbox->anchor);
//    }
//    else
//    {
//        Editbox_clearSelection(pEditbox);
//    }
//}
//
//void Editbox_handleWordLeft(struct Editbox* pEditbox, mmUInt32_t sysKeys)
//{
//    if (pEditbox->c > 0)
//    {
//        Editbox_setCaretIndex(pEditbox, mmTextMoveToWordPrev(&pEditbox->text, (int)pEditbox->c));
//    }
//
//    if (sysKeys & MM_MODIFIER_SHIFT)
//    {
//        Editbox_setSelection(pEditbox, pEditbox->c, pEditbox->anchor);
//    }
//    else
//    {
//        Editbox_clearSelection(pEditbox);
//    }
//}
//
//void Editbox_handleCharRight(struct Editbox* pEditbox, mmUInt32_t sysKeys)
//{
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//
//    if (pEditbox->c < length)
//    {
//        mmUtf16_t c = mmUtf16String_At(&pEditbox->text, pEditbox->c);
//        int n = (U16_IS_SURROGATE(c) && U16_IS_LEAD(c)) ? 2 : 1;
//        
//        Editbox_setCaretIndex(pEditbox, pEditbox->c + n);
//    }
//
//    if (sysKeys & MM_MODIFIER_SHIFT)
//    {
//        Editbox_setSelection(pEditbox, pEditbox->c, pEditbox->anchor);
//    }
//    else
//    {
//        Editbox_clearSelection(pEditbox);
//    }
//}
//
//void Editbox_handleWordRight(struct Editbox* pEditbox, mmUInt32_t sysKeys)
//{
//    size_t length = mmUtf16String_Size(&pEditbox->text);
//
//    if (pEditbox->c < length)
//    {
//        Editbox_setCaretIndex(pEditbox, mmTextMoveToWordNext(&pEditbox->text, (int)pEditbox->c));
//    }
//
//    if (sysKeys & MM_MODIFIER_SHIFT)
//    {
//        Editbox_setSelection(pEditbox, pEditbox->c, pEditbox->anchor);
//    }
//    else
//    {
//        Editbox_clearSelection(pEditbox);
//    }
//}
//
//void mmTestEditbox_Func(struct mmActivityMaster* p)
//{
//    struct mmUtf16String s;
//    mmUtf16String_Init(&s);
//    int prev = mmTextMoveToWordPrev(&s, 0);
//    int next = mmTextMoveToWordNext(&s, 0);
//    printf("prev: %d next: %d\n", prev, next);
//    mmUtf16String_Destroy(&s);
//}
//
