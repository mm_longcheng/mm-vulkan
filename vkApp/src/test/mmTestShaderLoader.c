/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestShaderLoader.h"

#include "mmActivityMaster.h"

#include "nwsi/mmContextMaster.h"

#include "vk/mmVKShaderLoader.h"

void mmTestShaderLoader_Func(struct mmActivityMaster* p)
{
    struct mmVKManager* pVKManager = &p->vVKManager;
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;

    struct mmVKShaderLoader l;

    mmVKShaderLoader_Init(&l);
    mmVKShaderLoader_SetFileContext(&l, pFileContext);
    mmVKShaderLoader_SetUploader(&l, &pVKManager->vRenderer.vUploader);
    mmVKShaderLoader_LoadFromFile(&l, "programs/spirv/spirv-ui.vert.spv");
    mmVKShaderLoader_LoadFromFile(&l, "programs/spirv/spirv-ui.frag.spv");
    mmVKShaderLoader_LoadFromFile(&l, "programs/spirv/spirv-ui.frag.spv");
    mmVKShaderLoader_Destroy(&l);
}

