/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTestGLTF_h__
#define __mmTestGLTF_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUploader.h"
#include "vk/mmVKUniformType.h"

#include "vk/mmVKPipeline.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKScene.h"
#include "vk/mmVKTFObject.h"
#include "vk/mmVKTFFileIO.h"

#include "parse/mmParseGLTF.h"

#include "core/mmPrefix.h"

struct mmActivityMaster;
struct mmVKCommandBuffer;

struct mmEventCursor;

struct mmTestGLTF
{
    struct mmActivityMaster* pActivity;
    struct mmVKScene vScene;
    struct mmVKTFObject vAsset1;
    struct mmString vModelPath;
};

void mmTestGLTF_Init(struct mmTestGLTF* p);
void mmTestGLTF_Destroy(struct mmTestGLTF* p);

void mmTestGLTF_Prepare(struct mmTestGLTF* p, struct mmActivityMaster* pActivity);
void mmTestGLTF_Discard(struct mmTestGLTF* p, struct mmActivityMaster* pActivity);

void mmTestGLTF_Resize(struct mmTestGLTF* p, struct mmActivityMaster* pActivity);
void mmTestGLTF_Update(struct mmTestGLTF* p, struct mmActivityMaster* pActivity);

void mmTestGLTF_RecordRenderPass(struct mmTestGLTF* p, struct mmVKCommandBuffer* cmd);
void mmTestGLTF_RecordCommandBuffer(struct mmTestGLTF* p, struct mmVKCommandBuffer* cmd);

void mmTestGLTF_OnCursorBegan(struct mmTestGLTF* p, struct mmEventCursor* content);
void mmTestGLTF_OnCursorMoved(struct mmTestGLTF* p, struct mmEventCursor* content);
void mmTestGLTF_OnCursorEnded(struct mmTestGLTF* p, struct mmEventCursor* content);
void mmTestGLTF_OnCursorBreak(struct mmTestGLTF* p, struct mmEventCursor* content);
void mmTestGLTF_OnCursorWheel(struct mmTestGLTF* p, struct mmEventCursor* content);

#include "core/mmSuffix.h"

#endif//__mmTestGLTF_h__
