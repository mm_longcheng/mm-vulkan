/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestShadow.h"

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"
#include "core/mmStringUtils.h"
#include "core/mmFilePath.h"
#include "core/mmBinarySerach.h"
#include "core/mmKeyCode.h"

#include "algorithm/mmBase64.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix3x3.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "vk/mmVKPipeline.h"
#include "vk/mmVKTFModel.h"
#include "vk/mmVKTFPrimitiveHelper.h"
#include "vk/mmVKNode.h"
#include "vk/mmVKCommandBuffer.h"
#include "vk/mmVKImageUploader.h"

#include "parse/mmParseJSONHelper.h"

void mmTestShadow_UpdateScene(struct mmTestShadow* p);

void mmTestShadow_Init(struct mmTestShadow* p)
{
    mmVKScene_Init(&p->vScene);
    mmVKTFObject_Init(&p->vAsset1);
    mmString_Init(&p->vModelPath);

    p->l = 1.0f;
    mmVec2Make(p->angle, 0.0f, 45.0f);

    p->pPoolInfo = NULL;
    p->pPipelineInfo = NULL;
    mmVKDescriptorSet_Init(&p->vDescriptorSet);
    p->debug = 0;
    p->ColorCascades = 0;

    p->l = 7.43628168f;
    mmVec2Make(p->angle, 92.0f, 60.0f);
    //p->debug = 1;

    p->vScene.hCamera.n = 0.1f;
    p->vScene.hCamera.f = 500.0f;
}
void mmTestShadow_Destroy(struct mmTestShadow* p)
{
    p->vScene.hCamera.n = 0.1f;
    p->vScene.hCamera.f = 500.0f;

    p->ColorCascades = 0;
    p->debug = 0;
    mmVKDescriptorSet_Destroy(&p->vDescriptorSet);
    p->pPipelineInfo = NULL;
    p->pPoolInfo = NULL;

    mmVec2Make(p->angle, 0.0f, 45.0f);
    p->l = 1.0f;

    mmString_Destroy(&p->vModelPath);
    mmVKTFObject_Destroy(&p->vAsset1);
    mmVKScene_Destroy(&p->vScene);
}

void mmTestShadow_RecordRenderPass(struct mmTestShadow* p, struct mmVKCommandBuffer* cmd)
{
    VkCommandBuffer cmdBuffer = cmd->cmdBuffer;
    
    mmVKShadow_OnRecordCommand(
        &p->vScene.hShadow,
        cmdBuffer,
        &p->vAsset1,
        &p->vScene);
}

void mmTestShadow_RecordCommandBuffer(struct mmTestShadow* p, struct mmVKCommandBuffer* cmd)
{
    VkCommandBuffer cmdBuffer = cmd->cmdBuffer;
    
    mmVKScene_SetPipelineIndex(&p->vScene, mmVKPipelineWhole, 0, 0);
    mmVKTFObject_OnRecordCommand(cmdBuffer, &p->vScene, &p->vAsset1);

    if (1 == p->debug)
    {
        VkPipelineLayout pipelineLayout = p->pPipelineInfo->vPipeline.pipelineLayout;
        VkPipeline pipeline = p->pPipelineInfo->vPipeline.pipeline;
        VkDescriptorSet descriptorSet = p->vDescriptorSet.descriptorSet;
        
        vkCmdBindDescriptorSets(
            cmdBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipelineLayout,
            0,
            1,
            &descriptorSet,
            0,
            NULL);
        
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
        vkCmdDraw(cmdBuffer, 3, 1, 0, 0);
    }
}

void mmTestShadow_OnCursorBegan(struct mmTestShadow* p, struct mmEventCursor* content)
{
    if ((1 << MM_MB_RBUTTON) & content->button_mask)
    {
        p->ColorCascades++;
        p->ColorCascades &= 1;
    }
    if ((1 << MM_MB_MBUTTON) & content->button_mask)
    {
        p->debug++;
        p->debug &= 1;
    }
}
void mmTestShadow_OnCursorMoved(struct mmTestShadow* p, struct mmEventCursor* content)
{
    int viewUpdated = 0;
    if ((1 << MM_MB_LBUTTON) & content->button_mask)
    {
        p->angle[0] += (float)content->rel_x;
        p->angle[1] -= (float)content->rel_y;

        p->angle[0] = fmodf(p->angle[0], 360.0f);
        p->angle[1] = mmMathClamp(p->angle[1], 1e-5f, 180.0f - 1e-5f);

        viewUpdated = 1;
    }
    if ((1 << MM_MB_RBUTTON) & content->button_mask)
    {
        viewUpdated = 1;
    }
    if ((1 << MM_MB_MBUTTON) & content->button_mask)
    {
        viewUpdated = 1;
    }

    if (viewUpdated)
    {
        mmTestShadow_UpdateScene(p);
    }
}
void mmTestShadow_OnCursorEnded(struct mmTestShadow* p, struct mmEventCursor* content)
{

}
void mmTestShadow_OnCursorBreak(struct mmTestShadow* p, struct mmEventCursor* content)
{

}
void mmTestShadow_OnCursorWheel(struct mmTestShadow* p, struct mmEventCursor* content)
{
    p->l = p->l + p->l * (float)(content->wheel_dy) * 0.1f;

    mmTestShadow_UpdateScene(p);
}

static const VkDescriptorSetLayoutBinding dslb_bindings1[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
};

const struct mmVectorValue mmDebugShadow_DSLB1 = mmVectorValue_Make(dslb_bindings1, VkDescriptorSetLayoutBinding);

void mmTestShadow_Prepare(struct mmTestShadow* p, struct mmActivityMaster* pActivity)
{
    p->pActivity = pActivity;

    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;
    // struct mmVKPassLoader* pPassLoader = &pAssets->vPassLoader;
    // struct mmVKUploader*   pUploader = &p->pActivity->vRenderer.vUploader;
    struct mmVKLayoutLoader* pLayoutLoader = &pAssets->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &pAssets->vPoolLoader;

    struct mmVKLayoutCreateInfo LayoutInfo =
    {
        &mmDebugShadow_DSLB1, 64,
    };
    struct mmVKLayoutInfo* pLayoutInfo = mmVKLayoutLoader_LoadFromInfo(pLayoutLoader, "mmDebugShadow",&LayoutInfo);
    mmVKPoolLoader_LoadFromInfo(pPoolLoader, "mmDebugShadow", pLayoutInfo);

    // const char* path = "model/samplescene.gltf";
    const char* path = "model/shadowscene_fire.gltf";
    // const char* path = "model/glowsphere.gltf";
    // const char* path = "F:/package/gltf/Vulkan/data/models/FlightHelmet/glTF/FlightHelmet.gltf";
    // const char* path = "F:/package/gltf/Vulkan/data/models/vulkanscene_shadow.gltf";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Duck/glTF-Binary/Duck.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/LittlestTokyo.glb";
    // const char* path = "model/Cesium_Man.glb";
    // const char* path = "model/torusknot.gltf";
    // const char* path = "F:/package/gltf/Vulkan/data/models/sphere.gltf";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Suzanne/glTF/Suzanne.gltf";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/collision-world.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/DamagedHelmet/glTF/DamagedHelmet.gltf";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/ferrari.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/ShadowmappableMesh.glb";
    // const char* path = "F:/package/gltf/sketchfab/randalierender-t-rex/source/Randalierender T-Rex.glb";
    // const char* path = "F:/package/gltf/sketchfab/randalierender-t-rex/source/T-Rex.glb";
    // const char* path = "F:/package/gltf/sketchfab/free_shar_pei_animated_dog/scene.gltf";
    // const char* path = "F:/package/gltf/Vulkan/data/models/shadowscene_fire.gltf";

    // const char* pAnimationName = "";
    // const char* pAnimationName = "run";
    const char* pAnimationName = "walking_cycle";
    
    struct mmVKLightInfo hLightInfos[] =
    {
        {
            { -30.0f, -40.0f, 0.0f },   // float direction[3];
            { 0.0f, 0.0f, 0.0f },       // float position[3];
            { 1.0f, 1.0f, 1.0f },       // float color[3];
            0.0f,                       // float range;
            0.4f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypeDirectional, // int type;
            1,                          // int castShadow;
            4,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
        {
            { 0.0f, 0.0f, 0.0f },       // float direction[3];
            { 6.0f, 8.0f, 0.0f },       // float position[3];
            { 1.0f, 0.0f, 0.0f },       // float color[3];
            0.0f,                       // float range;
            2.5f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypePoint,       // int type;
            1,                          // int castShadow;
            6,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
        {
            { 0.0f, 0.0f, 0.0f },       // float direction[3];
            { 6.0f, 8.0f, 0.0f },       // float position[3];
            { 0.0f, 1.0f, 0.0f },       // float color[3];
            0.0f,                       // float range;
            2.5f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypePoint,       // int type;
            0,                          // int castShadow;
            4,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
        {
            { -6.0f, -8.0f, 0.0f },     // float direction[3];
            { 6.0f, 8.0f, 0.0f },       // float position[3];
            { 0.0f, 0.0f, 1.0f },       // float color[3];
            0.0f,                       // float range;
            4.5f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            (float)MM_PI_DIV_4,         // float outerConeCos;
            mmGLTFLightTypeSpot,        // int type;
            1,                          // int castShadow;
            1,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
    };
    
    hLightInfos[0].castShadow = 1;
    hLightInfos[1].castShadow = 0;
    hLightInfos[2].castShadow = 0;
    hLightInfos[3].castShadow = 0;
    struct mmVectorValue hInfos = mmVectorValue_Make(hLightInfos, struct mmVKLightInfo);
    // hInfos.size = 1;
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    mmVKScene_Prepare(&p->vScene, pAssets, &hInfos, pSwapchain->depthFormat);
    // p->vScene.uboSceneFragment.lightCount = 1;
    
    mmString_Assigns(&p->vModelPath, path);
    mmVKTFModelLoader_LoadFromPath(pModelLoader, path);

    mmVKTFObject_Prepare(&p->vAsset1, pAssets, path);
    mmVKTFObject_PlayAnimation(&p->vAsset1, pAnimationName);

    mmTestShadow_UpdateScene(p);

    VkResult err = VK_ERROR_UNKNOWN;
    do
    {
        struct mmVKPipelineLoader* pPipelineLoader = NULL;
        struct mmVKDescriptorPool* pDescriptorPool = NULL;
        struct mmVKPoolLoader* pPoolLoader = NULL;
        const char* pool = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        pPipelineLoader = &pAssets->vPipelineLoader;

        p->pPipelineInfo = mmVKPipelineLoader_LoadFromFile(
            pPipelineLoader,
            "debugshadowmap",
            "pipeline/debugshadowmap.pipeline.json");
        if (mmVKPipelineInfo_Invalid(p->pPipelineInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineLoader_LoadFromFile pSamplerInfo failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPipelineInfo = NULL;
            break;
        }
        mmVKPipelineInfo_Increase(p->pPipelineInfo);

        pool = "mmDebugShadow";
        pPoolLoader = &pAssets->vPoolLoader;
        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, pool);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);
        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;

        err = mmVKDescriptorPool_DescriptorSetProduce(pDescriptorPool, &p->vDescriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    do
    {
        VkWriteDescriptorSet writes[1];

        VkDevice device = pAssets->pDevice->device;
        VkDescriptorSet descriptorSet = p->vDescriptorSet.descriptorSet;

        VkDescriptorImageInfo hDescriptorImageInfo[1];
        //hDescriptorImageInfo[0].sampler = p->hShadow.hCascade.pSamplerInfo->sampler;
        //hDescriptorImageInfo[0].imageView = p->hShadow.hCascade.hShadowMap.imageView;
        //hDescriptorImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        //hDescriptorImageInfo[0].sampler = p->hShadow.hOmni.pSamplerInfo->sampler;
        //hDescriptorImageInfo[0].imageView = p->hShadow.hOmni.hShadowMap.imageView;
        //hDescriptorImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        //hDescriptorImageInfo[0].sampler = p->hShadow.hSpot.pSamplerInfo->sampler;
        //hDescriptorImageInfo[0].imageView = p->hShadow.hSpot.hShadowMap.imageView;
        //hDescriptorImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        //hDescriptorImageInfo[0].sampler = p->hShadow.hOmni.pSamplerInfo->sampler;
        //hDescriptorImageInfo[0].imageView = p->pImageInfo->imageView;
        //hDescriptorImageInfo[0].imageLayout = p->pImageInfo->vImage.imageLayout;

        hDescriptorImageInfo[0].sampler = p->vScene.hShadow.hOmni.pSamplerInfo->sampler;
        hDescriptorImageInfo[0].imageView = p->vScene.hShadow.hOmni.hShadowMap.imageView;
        hDescriptorImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[0].pImageInfo = &hDescriptorImageInfo[0];
        writes[0].pBufferInfo = NULL;
        writes[0].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 1, writes, 0, NULL);

    } while (0);
}

void mmTestShadow_Discard(struct mmTestShadow* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;
    // struct mmVKUploader*   pUploader = &p->pActivity->vRenderer.vUploader;
    struct mmVKLayoutLoader* pLayoutLoader = &pAssets->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &pAssets->vPoolLoader;
    struct mmVKPipelineLoader* pPipelineLoader = &pAssets->vPipelineLoader;

    // struct mmVKImageLoader* pImageLoader = &pAssets->vImageLoader;

    struct mmVKDescriptorPool* pDescriptorPool = &p->pPoolInfo->vDescriptorPool;
    mmVKDescriptorPool_DescriptorSetRecycle(pDescriptorPool, &p->vDescriptorSet);

    mmVKPoolInfo_Decrease(p->pPoolInfo);
    p->pPoolInfo = NULL;

    mmVKPipelineInfo_Decrease(p->pPipelineInfo);
    p->pPipelineInfo = NULL;
    mmVKPipelineLoader_UnloadByName(pPipelineLoader, "debugshadowmap");

    mmVKTFObject_Discard(&p->vAsset1, pAssets);

    mmVKTFModelLoader_UnloadByPath(pModelLoader, mmString_CStr(&p->vModelPath));

    mmVKScene_Discard(&p->vScene, pAssets);

    mmVKPoolLoader_UnloadByName(pPoolLoader, "mmDebugShadow");
    mmVKLayoutLoader_UnloadByName(pLayoutLoader, "mmDebugShadow");
}

void mmTestShadow_Resize(struct mmTestShadow* p, struct mmActivityMaster* pActivity)
{
    mmTestShadow_UpdateScene(p);
}

void mmTestShadow_Update(struct mmTestShadow* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;

    struct mmSurfaceMaster* pSurfaceMaster = pActivity->pSurfaceMaster;
    struct mmEventUpdate* pUpdate = &pSurfaceMaster->hUpdate;

    float interval = (float)pUpdate->interval;
    mmVKTFObject_Update(&p->vAsset1, pUploader, interval);

    //static float angle = 0.0f;
    //float rfAngle = (float)(angle * MM_PI / 180.0);
    //angle += 0.03f;
    //mmQuatFromAngleAxis(p->vAsset1.node.quaternion, rfAngle, mmVec3PositiveUnitY);
    //mmVKNode_MarkCacheInvalidWorldTransform(&p->vAsset1.node);

    struct mmVKUBOLight* lights;
    lights = (struct mmVKUBOLight*)p->vScene.uboSceneLights.arrays;
    static float time = 0.0f;
    time += interval * 0.01f;
    float hAngle = mmMathRadian(time * 360.0f);
    float radius = 5.0f;
    float lightPos[3];
    lightPos[0] = cosf(hAngle) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle) * radius;
    mmUniformVec3Assign(lights[1].position, lightPos);
    lightPos[0] = cosf(hAngle + (float)MM_PI_DIV_2) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle + (float)MM_PI_DIV_2) * radius;
    mmUniformVec3Assign(lights[2].position, lightPos);
    lightPos[0] = -cosf(hAngle + (float)MM_PI) * 20.0f;
    lightPos[1] = -20.0f;
    lightPos[2] = -sinf(hAngle + (float)MM_PI) * 20.0f;
    mmUniformVec3Assign(lights[0].direction, lightPos);
    lightPos[0] = cosf(hAngle + (float)(MM_PI_DIV_2 * 3.0)) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle + (float)(MM_PI_DIV_2 * 3.0)) * radius;
    mmUniformVec3Assign(lights[3].position, lightPos);

    //glm::vec4 lightPos = glm::vec4(0.0f, -2.5f, 0.0f, 1.0);
    //lightPos.x = sin(glm::radians(timer * 360.0f)) * 0.15f;
    //lightPos.z = cos(glm::radians(timer * 360.0f)) * 0.15f;
    lightPos[0] = cosf(hAngle + (float)(MM_PI_DIV_2 * 3.0)) * 0.3f;
    lightPos[1] = 2.5f;
    lightPos[2] = sinf(hAngle + (float)(MM_PI_DIV_2 * 3.0)) * 0.3f;
    mmUniformVec3Assign(lights[1].position, lightPos);

    mmVKScene_UpdateUBODataLights(&p->vScene);

    lights[0].debug = p->ColorCascades;
    lights[1].debug = p->ColorCascades;

    mmVKScene_UpdateUBOBufferSceneLights(&p->vScene, pUploader);

    mmVKScene_UpdateTransforms(&p->vScene);

    mmVKScene_UpdateUBODataTransforms(&p->vScene);

    mmVKScene_UpdateUBOBufferShadow(&p->vScene, pUploader);

    // Animate the light source
    p->lightPos[0] = cosf(hAngle) * 20.0f;
    p->lightPos[1] = 25.0f + sinf(hAngle) * 10.0f;
    p->lightPos[2] = 10.0f + sinf(hAngle) * 5.0f;
}

void mmTestShadow_UpdateScene(struct mmTestShadow* p)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    uint32_t* windowSize = pSwapchain->windowSize;

    mmMat4x4Ref ProjectionView = p->vScene.uboSceneWholeVert.ProjectionView;
    mmMat4x4Ref Projection = p->vScene.uboSceneWholeVert.Projection;
    mmMat4x4Ref View = p->vScene.uboSceneWholeVert.View;

    float eye[3] = { 0.0f, 1.0f, 2.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0 };

    mmVec3MulK(eye, eye, p->l);

    float hAngleX = mmMathRadian(p->angle[0]);
    float hAngleY = mmMathRadian(p->angle[1]);
    float radius = mmVec3Length(eye);
    eye[0] = sinf(hAngleY) * cosf(hAngleX) * radius;
    eye[1] = cosf(hAngleY)                 * radius;
    eye[2] = sinf(hAngleY) * sinf(hAngleX) * radius;

    mmMat4x4LookAtRH(View, eye, origin, up);

    // z-fighting resolve need modify near to suitable.
    float aspect = (float)windowSize[0] / (float)windowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, p->vScene.hCamera.n, p->vScene.hCamera.f);

    mmMat4x4Inverse(p->vScene.uboSceneFragment.inverseView, View);
    mmMat4x4Mul(ProjectionView, Projection, View);
    mmUniformVec3Assign(p->vScene.uboSceneFragment.camera, eye);

    mmVKScene_UpdateUBOBufferSceneWhole(&p->vScene, pUploader);
    mmVKScene_UpdateUBOBufferSceneFragment(&p->vScene, pUploader);
}

