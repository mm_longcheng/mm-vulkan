/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestGLTF.h"

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"
#include "core/mmStringUtils.h"
#include "core/mmFilePath.h"
#include "core/mmKeyCode.h"

#include "algorithm/mmBase64.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "vk/mmVKPipeline.h"
#include "vk/mmVKTFModel.h"
#include "vk/mmVKTFPrimitiveHelper.h"
#include "vk/mmVKCommandBuffer.h"

void mmTestGLTF_Init(struct mmTestGLTF* p)
{
    mmVKScene_Init(&p->vScene);
    mmVKTFObject_Init(&p->vAsset1);
    mmString_Init(&p->vModelPath);
}
void mmTestGLTF_Destroy(struct mmTestGLTF* p)
{
    mmString_Destroy(&p->vModelPath);
    mmVKTFObject_Destroy(&p->vAsset1);
    mmVKScene_Destroy(&p->vScene);
}

void mmTestGLTF_RecordRenderPass(struct mmTestGLTF* p, struct mmVKCommandBuffer* cmd)
{
    VkCommandBuffer cmdBuffer = cmd->cmdBuffer;
    
    mmVKShadow_OnRecordCommand(
        &p->vScene.hShadow,
        cmdBuffer,
        &p->vAsset1,
        &p->vScene);
}
void mmTestGLTF_RecordCommandBuffer(struct mmTestGLTF* p, struct mmVKCommandBuffer* cmd)
{
    mmVKScene_SetPipelineIndex(&p->vScene, mmVKPipelineWhole, 0, 0);
    mmVKTFObject_OnRecordCommand(cmd->cmdBuffer, &p->vScene, &p->vAsset1);
}

void mmTestGLTF_OnCursorBegan(struct mmTestGLTF* p, struct mmEventCursor* content)
{

}
void mmTestGLTF_OnCursorMoved(struct mmTestGLTF* p, struct mmEventCursor* content)
{

}
void mmTestGLTF_OnCursorEnded(struct mmTestGLTF* p, struct mmEventCursor* content)
{
    const char* paths[] =
    {
        "model/BoxTextured_Binary.glb",
        "model/BoxTextured_Binary_uastc.glb",
        "model/BoxTextured_Binary_uastc_draco.glb",
        "model/BoxTextured_Embedded.gltf",
        "model/BoxTextured_Separate.gltf",
        "model/BoxTextured_Embedded_uastc_draco.gltf",
        "model/Flamingo.glb",
        "model/Stork.glb",
        "model/BoxTextured.glb",
        "model/cube.glb",
        "model/singleBlendshapeCube_sparse.glb",
        "model/sphere.glb",
        "model/Flower.glb",
        "model/RobotExpressive.glb",
        "model/glowsphere.gltf",
        "model/teapot.gltf",
        "model/torusknot.gltf",
#if MM_PLATFORM == MM_PLATFORM_WIN32
        "F:/package/gltf/Vulkan/data/models/chinesedragon.gltf",
        "F:/package/gltf/Vulkan/data/models/fireplace.gltf",
        "F:/package/gltf/Vulkan/data/models/glowsphere.gltf",
        "F:/package/gltf/Vulkan/data/models/lavaplanet.gltf",
        "F:/package/gltf/Vulkan/data/models/oaktree.gltf",
        "F:/package/gltf/Vulkan/data/models/plane.gltf",
        "F:/package/gltf/Vulkan/data/models/plants.gltf",
        "F:/package/gltf/Vulkan/data/models/reflection_scene.gltf",
        "F:/package/gltf/Vulkan/data/models/retroufo.gltf",
        "F:/package/gltf/Vulkan/data/models/retroufo_glow.gltf",
        "F:/package/gltf/Vulkan/data/models/retroufo_red_lowpoly.gltf",
        "F:/package/gltf/Vulkan/data/models/samplebuilding.gltf",
        "F:/package/gltf/Vulkan/data/models/sampleroom.gltf",
        "F:/package/gltf/Vulkan/data/models/samplescene.gltf",
        "F:/package/gltf/Vulkan/data/models/shadowscene_fire.gltf",
        "F:/package/gltf/Vulkan/data/models/sibenik.gltf",
        "F:/package/gltf/Vulkan/data/models/sponza.gltf",
        //"F:/package/gltf/Vulkan/data/models/sponza_b.gltf",                          // png format not support
        "F:/package/gltf/Vulkan/data/models/sponza_gltf_khronos_fixed_and_split.gltf",
        "F:/package/gltf/Vulkan/data/models/suzanne.gltf",
        "F:/package/gltf/Vulkan/data/models/teapot.gltf",
        "F:/package/gltf/Vulkan/data/models/terrain_gridlines.gltf",
        "F:/package/gltf/Vulkan/data/models/torusknot.gltf",
        "F:/package/gltf/Vulkan/data/models/treasure_glow.gltf",
        "F:/package/gltf/Vulkan/data/models/treasure_smooth.gltf",
        "F:/package/gltf/Vulkan/data/models/tunnel_cylinder.gltf",
        "F:/package/gltf/Vulkan/data/models/venus.gltf",
        "F:/package/gltf/Vulkan/data/models/voyager.gltf",
        "F:/package/gltf/Vulkan/data/models/vulkanscene_shadow.gltf",
        "F:/package/gltf/Vulkan/data/models/vulkanscenebackground.gltf",
        "F:/package/gltf/Vulkan/data/models/vulkanscenelogos.gltf",
        "F:/package/gltf/Vulkan/data/models/vulkanscenemodels.gltf",
        "F:/package/gltf/Vulkan/data/models/gltf/glTF-Embedded/Buggy.gltf",
        "F:/package/gltf/Vulkan/data/models/FlightHelmet/glTF/FlightHelmet.gltf",
        "F:/package/gltf/Vulkan/data/models/cerberus/cerberus.gltf",
        "F:/package/gltf/Vulkan/data/models/armor/armor.gltf",
        "F:/package/gltf/Sample/WaterBottle.glb",
        "F:/package/gltf/Sample/SimpleSparseAccessor.gltf",
        "E:/github/SaschaWillems/Vulkan-glTF-PBR/data/models/DamagedHelmet/glTF-Embedded/DamagedHelmet.gltf",
        "E:/github/mrdoob/three.js/examples/models/gltf/DamagedHelmet/glTF/DamagedHelmet.gltf",
        "E:/github/mrdoob/three.js/examples/models/gltf/ferrari.glb",
        //"E:/github/mrdoob/three.js/examples/models/gltf/coffeemat.glb",                                              // gltf format error   not need support
        "E:/github/mrdoob/three.js/examples/models/gltf/collision-world.glb",
        "E:/github/mrdoob/three.js/examples/models/gltf/Horse.glb",
        //"E:/github/mrdoob/three.js/examples/models/gltf/IridescentDishWithOlives.glb",                               // image format error  not need support
        "E:/github/mrdoob/three.js/examples/models/gltf/LittlestTokyo.glb",
        "E:/github/mrdoob/three.js/examples/models/gltf/PrimaryIonDrive.glb",
        "E:/github/mrdoob/three.js/examples/models/gltf/ShadowmappableMesh.glb",
        //"E:/github/mrdoob/three.js/examples/models/gltf/Soldier.glb",                                                // TEXCOORD number > 2 not need support
        "E:/github/mrdoob/three.js/examples/models/gltf/Xbot.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/AlphaBlendModeTest/glTF-Binary/AlphaBlendModeTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/AttenuationTest/glTF-Binary/AttenuationTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/BarramundiFish/glTF-Binary/BarramundiFish.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Avocado/glTF-Binary/Avocado.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/BoomBox/glTF-Binary/BoomBox.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/BoxInterleaved/glTF-Binary/BoxInterleaved.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/BoxVertexColors/glTF-Binary/BoxVertexColors.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/BrainStem/glTF-Binary/BrainStem.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Buggy/glTF-Binary/Buggy.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/ClearCoatTest/glTF-Binary/ClearCoatTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Corset/glTF-Binary/Corset.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/DamagedHelmet/glTF-Binary/DamagedHelmet.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/DragonAttenuation/glTF-Binary/DragonAttenuation.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Duck/glTF-Binary/Duck.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/EmissiveStrengthTest/glTF-Binary/EmissiveStrengthTest.glb",     // show error KHR_materials_emissive_strength
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/EnvironmentTest/glTF/EnvironmentTest.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/FlightHelmet/glTF/FlightHelmet.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Fox/glTF-Binary/Fox.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/GearboxAssy/glTF-Binary/GearboxAssy.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/GlamVelvetSofa/glTF-Binary/GlamVelvetSofa.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Lantern/glTF-Binary/Lantern.glb",
        //"E:/github/KhronosGroup/glTF-Sample-Models/2.0/MosquitoInAmber/glTF-Binary/MosquitoInAmber.glb",             // TEXCOORD number > 2
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/NormalTangentTest/glTF-Binary/NormalTangentTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/OrientationTest/glTF-Binary/OrientationTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/ReciprocatingSaw/glTF-Binary/ReciprocatingSaw.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/RiggedFigure/glTF-Binary/RiggedFigure.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/SciFiHelmet/glTF/SciFiHelmet.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/SheenChair/glTF-Binary/SheenChair.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/SheenCloth/glTF/SheenCloth.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/SimpleSparseAccessor/glTF-Embedded/SimpleSparseAccessor.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/StainedGlassLamp/glTF/StainedGlassLamp.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Suzanne/glTF/Suzanne.gltf",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/TextureCoordinateTest/glTF-Binary/TextureCoordinateTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/TextureEncodingTest/glTF-Binary/TextureEncodingTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/ToyCar/glTF-Binary/ToyCar.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/VC/glTF-Binary/VC.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/VertexColorTest/glTF-Binary/VertexColorTest.glb",
        "E:/github/KhronosGroup/glTF-Sample-Models/2.0/WaterBottle/glTF-Binary/WaterBottle.glb",
#endif//MM_PLATFORM
    };

    static int i = 0;
    // static int i = 50;
    // static int i = 75;
    // static int i = 46;
    // static int i = 54;

    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKDevice* pDevice = &pVKManager->vRenderer.vDevice;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;
    struct mmLogger* gLogger = mmLogger_Instance();
    // const char* path = paths[i];

    mmVKDevice_WaitIdle(pDevice);

    if (MM_MB_LBUTTON == content->button_id)
    {
        i++;
        i = i > MM_ARRAY_SIZE(paths) - 1 ? 0 : i;
    }
    else
    {
        i--;
        i = i < 0 ? MM_ARRAY_SIZE(paths) - 1 : i;
    }

    mmLogger_LogI(gLogger, "%d %s", i, paths[i]);

    mmVKTFObject_Discard(&p->vAsset1, pAssets);
    mmVKTFModelLoader_UnloadByPath(pModelLoader, mmString_CStr(&p->vModelPath));

    mmString_Assigns(&p->vModelPath, paths[i]);
    mmVKTFModelLoader_LoadFromPath(pModelLoader, paths[i]);
    mmVKTFObject_Prepare(&p->vAsset1, pAssets, paths[i]);
    mmVKTFObject_PlayAnimation(&p->vAsset1, "");
}
void mmTestGLTF_OnCursorBreak(struct mmTestGLTF* p, struct mmEventCursor* content)
{

}
void mmTestGLTF_OnCursorWheel(struct mmTestGLTF* p, struct mmEventCursor* content)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    uint32_t* windowSize = pSwapchain->windowSize;

    static float l = 1.0f;
    float view[4][4];
    float eye[3] = { 0.0f, 1.0f, 2.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0 };
    l = l + l * (float)(content->wheel_dy) * 0.1f;
    mmVec3MulK(eye, eye, l);
    mmMat4x4LookAtRH(view, eye, origin, up);

    // z-fighting resolve need modify near to suitable.
    float projection[4][4];
    float aspect = (float)windowSize[0] / (float)windowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    mmMat4x4PerspectiveVK(projection, (float)angle, aspect, l * 1.0f, 1000.0f);

    mmUniformMat4Assign(p->vScene.uboSceneWholeVert.Projection, projection);
    mmUniformMat4Assign(p->vScene.uboSceneWholeVert.View, view);
    mmMat4x4Mul(p->vScene.uboSceneWholeVert.ProjectionView, projection, view);
    mmUniformVec3Assign(p->vScene.uboSceneFragment.camera, eye);

    mmVKScene_UpdateUBOBufferSceneWhole(&p->vScene, pUploader);
    mmVKScene_UpdateUBOBufferSceneFragment(&p->vScene, pUploader);
}

void mmTestGLTF_Prepare(struct mmTestGLTF* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    //struct mmContextMaster* pContextMaster = pActivity->pContextMaster;
    //struct mmSurfaceMaster* pSurfaceMaster = pActivity->pSurfaceMaster;
    //struct mmVKShaderLoader* pShaderLoader = &pActivity->vAssets.vShaderLoader;
    //struct mmVKPipelineCache* pPipelineCache = &pActivity->vAssets.vPipelineCache;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    //struct mmVKAssetsDefault* pAssetsDefault = &pActivity->vAssets.vAssetsDefault;
    //struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    //struct mmVKUploader*   pUploader = &pActivity->vRenderer.vUploader;
    //struct mmVKDevice*       pDevice = &pActivity->vRenderer.vDevice;
    //struct mmVKLayoutLoader* pLayoutLoader = &pActivity->vAssets.vLayoutLoader;
    //ktx_transcode_fmt_e  transcodefmt = pActivity->vRenderer.vPhysicalDevice.transcodefmt;
    //struct mmVKPassLoader* pPassLoader = &pActivity->vAssets.vPassLoader;
    //struct mmVKPoolLoader* pPoolLoader = &pActivity->vAssets.vPoolLoader;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;

    p->pActivity = pActivity;

    // const char* path = "model/BoxTextured.glb";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/EmissiveStrengthTest/glTF-Binary/EmissiveStrengthTest.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/coffeemat.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/collision-world.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/Horse.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/IridescentDishWithOlives.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/LittlestTokyo.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/PrimaryIonDrive.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/ShadowmappableMesh.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/Soldier.glb";
    // const char* path = "E:/github/mrdoob/three.js/examples/models/gltf/Xbot.glb";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/AlphaBlendModeTest/glTF-Binary/AlphaBlendModeTest.glb";
    // const char* path = "H:/glTF-Transform/glTF-Transform/project_polly.glb";
    // const char* path = "H:/glTF-Transform/ktx2/ktx2/project_polly.glb";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Duck/glTF-Binary/Duck.glb";
    // const char* path = "F:/package/gltf/Vulkan/data/models/FlightHelmet/glTF/FlightHelmet.gltf";
    // const char* path = "E:/github/KhronosGroup/glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf";
    // const char* path = "model/glowsphere.gltf";
    // const char* path = "";
    // const char* path = "model/Flower.glb";
    // const char* path = "model/shadowscene_fire.gltf";
    const char* path = "model/Cesium_Man.glb";

    struct mmVKLightInfo hLightInfos[] =
    {
        {
            { -30.0f, -40.0f, 0.0f },   // float direction[3];
            { 0.0f, 0.0f, 0.0f },       // float position[3];
            { 1.0f, 1.0f, 1.0f },       // float color[3];
            0.0f,                       // float range;
            0.4f,                       // float intensity;
            0.0f,                       // float innerConeCos;
            0.0f,                       // float outerConeCos;
            mmGLTFLightTypeDirectional, // int type;
            0,                          // int castShadow;
            4,                          // int count;
            1,                          // int mode;
            0,                          // int debug;
            0.0f,                       // float ambient;
            0.005f,                     // float biasMax;
            0.001f,                     // float biasMin;
            0.95f,                      // float splitLambda;
        },
    };
    
    struct mmVectorValue hInfos = mmVectorValue_Make(hLightInfos, struct mmVKLightInfo);
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    p->vScene.uboSceneFragment.lightCount = 1;
    mmVKScene_Prepare(&p->vScene, pAssets, &hInfos, pSwapchain->depthFormat);

    mmString_Assigns(&p->vModelPath, path);
    mmVKTFModelLoader_LoadFromPath(pModelLoader, path);

    mmVKTFObject_Prepare(&p->vAsset1, pAssets, path);
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Running");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Run");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Take 001");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Main");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Idle");
    // mmVKTFObject_PlayAnimation(&p->vAsset1, "idle");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "walk");
    mmVKTFObject_PlayAnimation(&p->vAsset1, "Anim_0");
}

void mmTestGLTF_Discard(struct mmTestGLTF* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKAssets* pAssets = &pVKManager->vAssets;
    struct mmVKTFModelLoader* pModelLoader = &pAssets->vModelLoader;

    mmVKTFObject_Discard(&p->vAsset1, pAssets);

    mmVKTFModelLoader_UnloadByPath(pModelLoader, mmString_CStr(&p->vModelPath));

    mmVKScene_Discard(&p->vScene, pAssets);
}

void mmTestGLTF_Resize(struct mmTestGLTF* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;
    struct mmVKSwapchain* pSwapchain = &pVKManager->vSwapchain;
    mmVKScene_MakeData(&p->vScene, pSwapchain->windowSize);
    mmVKScene_UpdateUBOBuffer(&p->vScene, pUploader);
}

void mmTestGLTF_Update(struct mmTestGLTF* p, struct mmActivityMaster* pActivity)
{
    struct mmVKManager* pVKManager = &p->pActivity->vVKManager;
    struct mmVKUploader*   pUploader = &pVKManager->vRenderer.vUploader;

    struct mmSurfaceMaster* pSurfaceMaster = pActivity->pSurfaceMaster;
    struct mmEventUpdate* pUpdate = &pSurfaceMaster->hUpdate;

    float interval = (float)pUpdate->interval;
    mmVKTFObject_Update(&p->vAsset1, pUploader, interval);

    static float angle = 0.0f;
    float rfAngle = (float)(angle * MM_PI / 180.0);
    angle += 0.03f;
    mmQuatFromAngleAxis(p->vAsset1.node.quaternion, rfAngle, mmVec3PositiveUnitY);
    mmVKNode_MarkCacheInvalidWorldTransform(&p->vAsset1.node);

    struct mmVKUBOLight* lights;
    lights = (struct mmVKUBOLight*)p->vScene.uboSceneLights.arrays;
    static float time = 0.0f;
    time += interval * 0.1f;
    float hAngle = mmMathRadian(time * 360.0f);
    float radius = 5.0f;
    float lightPos[3];
    lightPos[0] = cosf(hAngle) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle) * radius;
    mmUniformVec3Assign(lights[1].position, lightPos);
    lightPos[0] = cosf(hAngle + (float)MM_PI_DIV_2) * radius;
    lightPos[1] = radius;
    lightPos[2] = sinf(hAngle + (float)MM_PI_DIV_2) * radius;
    mmUniformVec3Assign(lights[2].position, lightPos);

    // mmVKScene_UpdateUBOBufferSceneLights(&p->vScene, pUploader);

    
    mmVKScene_UpdateUBODataLights(&p->vScene);

    mmVKScene_UpdateUBOBufferSceneLights(&p->vScene, pUploader);

    mmVKScene_UpdateTransforms(&p->vScene);

    mmVKScene_UpdateUBODataTransforms(&p->vScene);

    mmVKScene_UpdateUBOBufferShadow(&p->vScene, pUploader);
    
    //float model[4][4];

    //float spin_angle = 0.4f;
    //float angle = (float)(spin_angle * MM_PI / 180.0);

    //mmMat4x4Duplicate(model, p->vScene.pushConstant.model);
    //mmMat4x4RotateY(model, model, angle);
    //mmUniformMat4Assign(p->vScene.pushConstant.model, model);

    //float view[4][4];
    //float eye[3] = { 0.0f, 0.1f, 0.1f };
    //float eye[3] = { 0.0f, 0.01f, 0.18f };
    //float eye[3] = { 0.0f, 0.25f, 0.06f };
    //float eye[3] = { 0.1f, 0.4f, 0.5f };
    //float eye[3] = { 0.5f, 1.0f, 2.0f };
    //float eye[3] = { 1.8f, 2.0f, 5.0f };
    //float eye[3] = { 5.0f, 10.0f, 25.0f };
    //float eye[3] = { 15.0f, 30.0f, 75.0f };
    //float eye[3] = { 30.0f, 40.0f, 100.0f };
    //float origin[3] = { 0, 0, 0 };
    //float up[3] = { 0.0f, 1.0f, 0.0 };
    //mmMat4x4LookAtRH(view, eye, origin, up);
    //mmUniformMat4Assign(p->vScene.uboSceneVert.view, view);
    //mmUniformVec3Assign(p->vScene.uboSceneFrag.camera, eye);
    //mmVKScene_UploadeUBOBuffer(&p->vScene, pUploader);


    //struct mmVKSwapchain* pSwapchain = &p->pActivity->vSwapchain;
    //uint32_t* windowSize = pSwapchain->windowSize;

    //float l = 1.0f;
    //float view[4][4];
    //float eye[3] = { 0.0f, 1.0f, 2.0f };
    //float origin[3] = { 0, 0, 0 };
    //float up[3] = { 0.0f, 1.0f, 0.0 };
    //l = 9.0f;
    //mmVec3MulK(eye, eye, l);
    //mmMat4x4LookAtRH(view, eye, origin, up);

    //// z-fighting resolve need modify near to suitable.
    //float projection[4][4];
    //float aspect = (float)windowSize[0] / (float)windowSize[1];
    //float anglex = (float)(45 * MM_PI / 180.0);
    //mmMat4x4PerspectiveVK(projection, (float)anglex, aspect, l * 1.0f, 1000.0f);

    //mmUniformMat4Assign(p->vScene.uboSceneVert.projection, projection);
    //mmUniformMat4Assign(p->vScene.uboSceneVert.view, view);
    //mmUniformVec3Assign(p->vScene.uboSceneFrag.camera, eye);

    //mmVKScene_UploadeUBOBuffer(&p->vScene, pUploader);
}

