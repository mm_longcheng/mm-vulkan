/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestNode.h"

#include "mmActivityMaster.h"

#include "nwsi/mmContextMaster.h"

#include "vk/mmVKNode.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix4x4.h"

void mmTestNode_Mat4x4Decomposition(struct mmActivityMaster* p)
{
    const float rkAxis[3] = { 0.0f, 1.0f, 0.0f };
    float rfAngle = mmMathRadian(30.0f);
    float t[3] = { 1.0f, 2.0f, 3.0f };
    float s[3] = { 1.1f, 2.2f, 3.3f };
    float q[4];
    float m[4][4];
    float dt[3];
    float ds[3];
    float dq[4];
    mmQuatFromAngleAxis(q, rfAngle, rkAxis);
    mmMat4x4MakeTransform(m, t, q, s);
    mmMat4x4Decomposition(m, ds, dq, dt);
    int a0 = mmVec3Equals(dt, t);
    int a1 = mmVec3Equals(ds, s);
    int a2 = mmQuatEquals(dq, q);
    printf("%d %d %d\n", a0, a1, a2);
}

void mmTestNode_Func(struct mmActivityMaster* p)
{
    struct mmVKNode node1;
    struct mmVKNode node2;

    mmTestNode_Mat4x4Decomposition(p);

    //size_t size = sizeof(struct mmVKNode);
    //size_t size1 = sizeof(struct mmVKNode1);

    mmVKNode_Init(&node1);
    mmVKNode_Init(&node2);

    float position1[3] = { 1.0f, 1.0f, 1.0f, };
    mmVKNode_SetPosition(&node1, position1);
    float scale1[3] = { 3.0f, 2.0f, 1.0f, };
    mmVKNode_SetScale(&node1, scale1);
    float quaternion1[4] = { 3.0f, 2.0f, 1.0f, 1.0f, };
    mmVKNode_SetQuaternion(&node1, quaternion1);

    float position2[3] = { 1.0f, 1.0f, 1.0f, };
    mmVKNode_SetPosition(&node2, position2);

    mmVKNode_AddChild(&node1, &node2);

    float worldTransform1[4][4];
    mmVKNode_GetWorldTransform(&node1, worldTransform1);
    mmVKNode_GetWorldTransform(&node1, worldTransform1);
    float worldTransform2[4][4];
    mmVKNode_GetWorldTransform(&node2, worldTransform2);
    mmVKNode_GetWorldTransform(&node2, worldTransform2);

    mmVKNode_RmvChild(&node1, &node2);

    mmVKNode_GetWorldTransform(&node2, worldTransform2);

    mmVKNode_Destroy(&node1);
    mmVKNode_Destroy(&node2);
}

