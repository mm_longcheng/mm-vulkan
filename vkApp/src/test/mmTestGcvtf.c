/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTestGcvtf.h"
#include "core/mmClock.h"
#include "core/mmValueTransform.h"

void mmTestGcvtf_Func(struct mmActivityMaster* p)
{
    {
        // 0100 0010 1101 0110 0101 0001 1100 1111
        //float fv = 107.1597824f;
        //uint32_t ifvh = 0x42D651CF;

        double fv = -1.2345678901234567890123456789e+308;
        // double fv = 107.1597824;
        // double fv = 107.1597824e+20;
        char valx[128] = { 0 };
        char valy[128] = { 0 };
        mmIEEE754GcvtD(fv, 18, valy);
        mmGcvt(fv, 18, valx);
        printf("%s\n", valx);
        printf("%s\n", valy);
    }

    {
        double fv = -1.2345678901234567890123456789e+308;
        char valx[128] = { 0 };
        char valy[128] = { 0 };

        int i = 0;
        int N = 999999;

        struct mmClock c;

        mmUInt64_t t0, t1, t2, t3;

        mmClock_Init(&c);
        mmClock_Reset(&c);

        t0 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            sprintf(valx, "%e", fv);
        }

        t1 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            mmGcvt(fv, 18, valx);
        }

        t2 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            mmIEEE754GcvtD(fv, 18, valy);
        }

        t3 = mmClock_Microseconds(&c);


        printf("Sleep   us = %" PRIu64 "\n", t1 - t0);
        printf("Sleep   us = %" PRIu64 "\n", t2 - t1);
        printf("Sleep   us = %" PRIu64 "\n", t3 - t2);

        mmClock_Destroy(&c);
    }

    {
        float fv = -1.2345678901234567890123456789e+38f;
        char valx[128] = { 0 };
        char valy[128] = { 0 };

        int i = 0;
        int N = 999999;

        struct mmClock c;

        mmUInt64_t t0, t1, t2, t3;

        mmClock_Init(&c);
        mmClock_Reset(&c);

        t0 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            sprintf(valx, "%e", fv);
        }

        t1 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            mmGcvt(fv, 18, valx);
        }

        t2 = mmClock_Microseconds(&c);

        for (i = 0; i < N; i++)
        {
            mmIEEE754GcvtF(fv, 18, valy);
        }

        t3 = mmClock_Microseconds(&c);

        printf("Sleep   us = %" PRIu64 "\n", t1 - t0);
        printf("Sleep   us = %" PRIu64 "\n", t2 - t1);
        printf("Sleep   us = %" PRIu64 "\n", t3 - t2);

        mmClock_Destroy(&c);
    }
}

