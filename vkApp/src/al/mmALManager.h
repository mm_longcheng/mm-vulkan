/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALManager_h__
#define __mmALManager_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtsetString.h"
#include "container/mmRbtsetVpt.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"
#include "al/mmALDevice.h"
#include "al/mmALContext.h"
#include "al/mmALListener.h"
#include "al/mmALSource.h"
#include "al/mmALSourceFactory.h"
#include "al/mmALAssetsFactory.h"
#include "al/mmALAssetsLoader.h"
#include "al/mmALBufferLoader.h"

#include "core/mmPrefix.h"

// OpenAL-soft limit source is default 256.
#define MM_ALSOURCE_MAX 256

MM_EXPORT_AL
void
mmALManagerEffectOnFinished(
    void*                                          obj,
    struct mmALSource*                             s);

struct mmALManager
{
    // AL device
    struct mmALDevice hDevice;
    
    // AL context
    struct mmALContext hContext;
    
    // Listener
    struct mmALListener hListener;
    
    // Assets factory.
    struct mmALAssetsFactory hAssetsFactory;
    
    // Source factory.
    struct mmALSourceFactory hSourceFactory;
    
    // Assets loader.
    struct mmALAssetsLoader hAssetsLoader;
    
    // Buffer loader.
    struct mmALBufferLoader hBufferLoader;
    
    // source set.
    struct mmRbtsetVpt hSources;
    
    // effects is auto destroy short audio source.
    // It is useful for none 3D mode, Suitable for ui situation.
    // effect set.
    struct mmRbtsetVpt hEffects;
};

MM_EXPORT_AL
void
mmALManager_Init(
    struct mmALManager*                            p);

MM_EXPORT_AL
void
mmALManager_Destroy(
    struct mmALManager*                            p);

MM_EXPORT_AL
void
mmALManager_SetFileContext(
    struct mmALManager*                            p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
void
mmALManager_SetDeviceName(
    struct mmALManager*                            p,
    const char*                                    pDeviceName);

MM_EXPORT_AL
void
mmALManager_SetMasterVolume(
    struct mmALManager*                            p,
    float                                          volume);

MM_EXPORT_AL
float
mmALManager_GetMasterVolume(
    const struct mmALManager*                      p);

MM_EXPORT_AL
void
mmALManager_EnumerateDevices(
    struct mmALManager*                            p);

MM_EXPORT_AL
int
mmALManager_Prepare(
    struct mmALManager*                            p);

MM_EXPORT_AL
void
mmALManager_Discard(
    struct mmALManager*                            p);

MM_EXPORT_AL
struct mmALBufferInfo*
mmALManager_LoadBufferFromPath(
    struct mmALManager*                            p,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALManager_UnloadBufferByPath(
    struct mmALManager*                            p,
    const char*                                    pFileName);

MM_EXPORT_AL
struct mmALSource*
mmALManager_ProduceSource(
    struct mmALManager*                            p,
    const char*                                    pFileName,
    int                                            hStream);

MM_EXPORT_AL
void
mmALManager_RecycleSource(
    struct mmALManager*                            p,
    struct mmALSource*                             pSource);

MM_EXPORT_AL
struct mmALSource*
mmALManager_PlayEffectDetail(
    struct mmALManager*                            p,
    const char*                                    pFileName,
    double                                         hPlayPosition,
    float                                          hVolume,
    float                                          hPitch);

MM_EXPORT_AL
struct mmALSource*
mmALManager_PlayEffect(
    struct mmALManager*                            p,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALManager_StopEffect(
    struct mmALManager*                            p,
    struct mmALSource*                             pSource);

MM_EXPORT_AL
void
mmALManager_StopEffectComplete(
    struct mmALManager*                            p);

MM_EXPORT_AL
void
mmALManager_Update(
    struct mmALManager*                            p,
    double                                         interval);

MM_EXPORT_AL
void
mmALManager_EnterBackground(
    struct mmALManager*                            p);

MM_EXPORT_AL
void
mmALManager_EnterForeground(
    struct mmALManager*                            p);

#include "core/mmSuffix.h"

#endif//__mmALManager_h__
