/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALListener.h"

#include "al/mmALError.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmQuaternion.h"

MM_EXPORT_AL
void
mmALListener_Init(
    struct mmALListener*                           p)
{
    p->hVolume = 1.0f;
    mmVec3AssignValue(p->hPosition, 0.0f);
    mmVec3AssignValue(p->hVelocity, 0.0f);
    mmQuatMakeIdentity(p->hQuaternion);
    mmVec3Make(&p->mOrientation[0], 0.0f, 0.0f, -1.0f);
    mmVec3Make(&p->mOrientation[3], 0.0f, 1.0f, 0.0f);
}

MM_EXPORT_AL
void
mmALListener_Destroy(
    struct mmALListener*                           p)
{
    mmVec3Make(&p->mOrientation[3], 0.0f, 1.0f, 0.0f);
    mmVec3Make(&p->mOrientation[0], 0.0f, 0.0f, -1.0f);
    mmQuatMakeIdentity(p->hQuaternion);
    mmVec3AssignValue(p->hVelocity, 0.0f);
    mmVec3AssignValue(p->hPosition, 0.0f);
    p->hVolume = 1.0f;
}

MM_EXPORT_AL
void
mmALListener_SetPosition(
    struct mmALListener*                           p,
    const float                                    position[3])
{
    mmVec3Assign(p->hPosition, position);
    alListenerfv(AL_POSITION, position);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_GetPosition(
    const struct mmALListener*                     p,
    float                                          position[3])
{
    alGetListenerfv(AL_POSITION, position);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_SetVelocity(
    struct mmALListener*                           p,
    const float                                    velocity[3])
{
    mmVec3Assign(p->hVelocity, velocity);
    alListenerfv(AL_VELOCITY, velocity);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_GetVelocity(
    const struct mmALListener*                     p,
    float                                          velocity[3])
{
    alGetListenerfv(AL_VELOCITY, velocity);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_SetQuaternion(
    struct mmALListener*                           p,
    const float                                    quaternion[4])
{
    float vAt[4];
    float vDirection[3];
    float vUp[3];
    
    mmQuatZAxis(quaternion, vDirection);
    mmQuatYAxis(quaternion, vUp);
    mmVec3Negate(vAt, vDirection);
    
    mmVec3Assign(&p->mOrientation[0], vAt);
    mmVec3Assign(&p->mOrientation[3], vUp);
    
    mmQuatAssign(p->hQuaternion, quaternion);
    
    alListenerfv(AL_ORIENTATION, p->mOrientation);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_GetQuaternion(
    const struct mmALListener*                     p,
    float                                          quaternion[4])
{
    /* right-hand coordinate */
    float N[3];
    float V[3];
    float U[3];
    float m[3][3];
    
    float vOrientation[6];
    
    alGetListenerfv(AL_ORIENTATION, vOrientation);
    mmALCheck(MM_LOG_ERROR, alGetError());
    
    /* This is copy from OpenAL-soft Orientation to Matrix
     *
     * We can also use LookAtRH to do this.
     */
    mmVec3Assign(N, &vOrientation[0]);
    mmVec3Assign(V, &vOrientation[3]);
    
    mmVec3Normalize(N, N);
    mmVec3Normalize(V, V);
    
    mmVec3CrossProduct(U, N, V);
    mmVec3Normalize(U, U);
    
    mmVec3Negate(N, N);
    
    /* Make a Rotate Matrix3x3 */
    m[0][0] = U[0]; m[0][1] = V[0]; m[0][2] = N[0];
    m[1][0] = U[1]; m[1][1] = V[1]; m[1][2] = N[1];
    m[2][0] = U[2]; m[2][1] = V[2]; m[2][2] = N[2];
    mmQuatFromMat3x3(quaternion, m);
}

MM_EXPORT_AL
void
mmALListener_SetVolume(
    struct mmALListener*                           p,
    float                                          volume)
{
    p->hVolume = mmMathClamp(volume, 0.0f, 1.0f);
    alListenerf(AL_GAIN, p->hVolume);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
float
mmALListener_GetVolume(
    const struct mmALListener*                     p)
{
    ALfloat volume = 0.0f;
    alGetListenerf(AL_GAIN, &volume);
    mmALCheck(MM_LOG_ERROR, alGetError());
    return volume;
}

MM_EXPORT_AL
void
mmALListener_EnterBackground(
    struct mmALListener*                           p)
{
    alListenerf(AL_GAIN, 0.0f);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

MM_EXPORT_AL
void
mmALListener_EnterForeground(
    struct mmALListener*                           p)
{
    alListenerf(AL_GAIN, p->hVolume);
    mmALCheck(MM_LOG_ERROR, alGetError());
}
