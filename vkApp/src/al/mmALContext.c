/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALContext.h"

#include "al/mmALError.h"

#include "core/mmLogger.h"

MM_EXPORT_AL
void
mmALContext_Init(
    struct mmALContext*                            p)
{
    p->context = NULL;
}

MM_EXPORT_AL
void
mmALContext_Destroy(
    struct mmALContext*                            p)
{
    p->context = NULL;
}

MM_EXPORT_AL
int
mmALContext_Prepare(
    struct mmALContext*                            p,
    ALCdevice*                                     device)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == device)
        {
            mmLogger_LogE(gLogger, "%s %d AL device is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        p->context = alcCreateContext(device, NULL);
        if (NULL == p->context)
        {
            mmLogger_LogE(gLogger, "%s %d AL context create failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d AL context create success.",
                __FUNCTION__, __LINE__);
        }
        
        err = MM_SUCCESS;
        
    } while (0);
    
    return err;
}

MM_EXPORT_AL
void
mmALContext_Discard(
    struct mmALContext*                            p)
{
    if (NULL != p->context)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        alcDestroyContext(p->context);
        // alGetError need context, not need check here.
        // mmALCheck(MM_LOG_ERROR, alGetError());
        p->context = NULL;
        mmLogger_LogI(gLogger, "%s %d AL context delete success.",
            __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_AL
int
mmALContext_MakeContextCurrent(
    struct mmALContext*                            p)
{
    if (NULL == p->context)
    {
        return MM_FAILURE;
    }
    else
    {
        alcMakeContextCurrent(p->context);
        mmALCheck(MM_LOG_TRACE, alGetError());

        return MM_SUCCESS;
    }
}
