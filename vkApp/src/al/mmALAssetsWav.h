/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALAssetsWav_h__
#define __mmALAssetsWav_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStream.h"

#include "al/mmALAssets.h"

#include "core/mmPrefix.h"

struct mmFileContext;

struct mmALWAVEHeader
{
    // RIFF
    char ChunkId[4];             // 'RIFF'
    mmUInt32_t ChunkSize;
    
    // WAVE
    char Format[4];              // 'WAVE'
    
    // fmt
    char SubChunk1Id[4];         // 'fmt '
    mmUInt32_t SubChunk1Size;
    // WAVEFORMATEX
    mmUInt16_t FormatTag;        // 0x0001 PCM 0xFFFE EXT
    mmUInt16_t Channels;         // 1, 2 for stereo data is (l,r) pairs
    mmUInt32_t SamplesPerSec;    // SampleRate
    mmUInt32_t AvgBytesPerSec;
    mmUInt16_t BlockAlign;
    mmUInt16_t BitsPerSample;
    
    // WAVEFORMATEXTENSIBLE
    mmUInt16_t Samples;
    mmUInt32_t ChannelMask;
    char SubFormat[16];
    
    // data
    char SubChunk2Id[4];         // 'data'
    mmUInt32_t SubChunk2Size;
    // data buffer offset.
    size_t DataOffset;           // data start offset.
    size_t DataFinish;           // data end   offset.
};

MM_EXPORT_AL
mmUInt64_t
mmALWAVEHeader_GetBitsPerSec(
    const struct mmALWAVEHeader*                   p);

MM_EXPORT_AL
mmUInt32_t
mmALWAVEHeader_GetBitsPerFrame(
    const struct mmALWAVEHeader*                   p);

MM_EXPORT_AL
void
mmALWAVEHeader_ParseData(
    struct mmALWAVEHeader*                         p,
    struct mmByteBuffer*                           s);

MM_EXPORT_AL
int
mmALWAVEHeader_ParseHead(
    struct mmALWAVEHeader*                         p,
    struct mmByteBuffer*                           s);

MM_EXPORT_AL
int
mmALWAVEHeader_ParseFromStream(
    struct mmALWAVEHeader*                         p,
    struct mmStream*                               pStream);

MM_EXPORT_AL
int
mmALAssetsWavPickFormat(
    struct mmALWAVEHeader*                         pHeader,
    struct mmALAssetsInfo*                         pAssetsInfo);

MM_EXPORT_AL
int
mmALAssetsWavQueryInfo(
    struct mmALWAVEHeader*                         pHeader,
    struct mmALAssetsInfo*                         pAssetsInfo);

MM_EXPORT_AL extern const struct mmALMetadataAssets mmALMetadataAssetsWav;

struct mmALAssetsWav
{
    struct mmALAssets Assets;
    struct mmStream Stream;
    struct mmALWAVEHeader Header;
};

MM_EXPORT_AL
void
mmALAssetsWav_Init(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
void
mmALAssetsWav_Destroy(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
void
mmALAssetsWav_Produce(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
void
mmALAssetsWav_Recycle(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
int
mmALAssetsWav_Prepare(
    struct mmALAssetsWav*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALAssetsWav_Discard(
    struct mmALAssetsWav*                          p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
double
mmALAssetsWav_GetTotalTime(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
int64_t
mmALAssetsWav_GetTotalFrame(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
int
mmALAssetsWav_GetIsSeekable(
    const struct mmALAssetsWav*                    p);

MM_EXPORT_AL
size_t
mmALAssetsWav_Read(
    struct mmALAssetsWav*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght);

MM_EXPORT_AL
int
mmALAssetsWav_SeekTime(
    struct mmALAssetsWav*                          p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALAssetsWav_TellTime(
    struct mmALAssetsWav*                          p);

MM_EXPORT_AL
int
mmALAssetsWav_SeekFrame(
    struct mmALAssetsWav*                          p,
    int64_t                                        hPosition);

MM_EXPORT_AL
int64_t
mmALAssetsWav_TellFrame(
    struct mmALAssetsWav*                          p);

#include "core/mmSuffix.h"

#endif//__mmALAssetsWav_h__
