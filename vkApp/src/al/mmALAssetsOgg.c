/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssetsOgg.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByteCoder.h"

#include "dish/mmFileContext.h"

MM_EXPORT_AL
int
mmALAssetsOggQueryInfo(
    OggVorbis_File*                                pVorbis,
    struct mmALAssetsInfo*                         pAssetsInfo)
{
    int err = MM_UNKNOWN;
    
    do
    {
        vorbis_info* pVorbisInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pVorbis)
        {
            mmLogger_LogE(gLogger, "mmALAssets OGG pVorbis is invalid!");
            err = MM_FAILURE;
            break;
        }
        
        pVorbisInfo = ov_info(pVorbis, -1);
        
        if (NULL == pVorbisInfo)
        {
            mmLogger_LogE(gLogger, "mmALAssets OGG pVorbisInfo is invalid!");
            err = MM_FAILURE;
            break;
        }
        
        pAssetsInfo->Channels = pVorbisInfo->channels;
        pAssetsInfo->SampleRate = pVorbisInfo->rate;
        pAssetsInfo->BitsPerSample = 16;
        
        err = mmALAssets16BitsPickFormat(pAssetsInfo);
        if (MM_FAILURE == err)
        {
            mmLogger_LogE(gLogger, "mmALAssets MP3 format not support.");
            break;
        }
        
        pAssetsInfo->Channels = pVorbisInfo->channels;
        pAssetsInfo->SampleRate = pVorbisInfo->rate;
        pAssetsInfo->BitsPerSample = 16;
        
        pAssetsInfo->BitsPerFrame = pAssetsInfo->Channels * pAssetsInfo->BitsPerSample;
        pAssetsInfo->BitsPerSec = pAssetsInfo->SampleRate * pAssetsInfo->BitsPerFrame;
        
        pAssetsInfo->TotalFrame = (mmUInt64_t)ov_pcm_total(pVorbis, -1);
        pAssetsInfo->ByteLength = pAssetsInfo->TotalFrame * pAssetsInfo->BitsPerFrame / 8;
        pAssetsInfo->TotalTime = ov_time_total(pVorbis, -1);
        pAssetsInfo->Seekable = (0 == ov_seekable(pVorbis)) ? MM_FALSE : MM_TRUE;
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

static
size_t
mmALAssetsOggRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return mmStreamRead(ptr, size, nmemb, s);
}

static
int
mmALAssetsOggSeek(
    void*                                          stream,
    ogg_int64_t                                    offset,
    int                                            whence)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return mmStreamSeeko64(s, offset, whence);
}

static
int
mmALAssetsOggClose(
    void*                                          stream)
{
    return 0;
}

static
long
mmALAssetsOggTell(
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return mmStreamTell(s);
}

static const ov_callbacks mmALAssetsOggCallbacks =
{
    mmALAssetsOggRead,
    mmALAssetsOggSeek,
    mmALAssetsOggClose,
    mmALAssetsOggTell,
};

const struct mmMetaAllocator mmALAllocatorAssetsOgg =
{
    "mmALAssetsOgg",
    sizeof(struct mmALAssetsOgg),
    &mmALAssetsOgg_Produce,
    &mmALAssetsOgg_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataAssets mmALMetadataAssetsOgg =
{
    &mmALAllocatorAssetsOgg,
    /*- Interface -*/
    &mmALAssetsOgg_Prepare,
    &mmALAssetsOgg_Discard,
    &mmALAssetsOgg_GetTotalTime,
    &mmALAssetsOgg_GetTotalFrame,
    &mmALAssetsOgg_GetIsSeekable,
    &mmALAssetsOgg_Read,
    &mmALAssetsOgg_SeekTime,
    &mmALAssetsOgg_TellTime,
    &mmALAssetsOgg_SeekFrame,
    &mmALAssetsOgg_TellFrame,
};

MM_EXPORT_AL
void
mmALAssetsOgg_Init(
    struct mmALAssetsOgg*                          p)
{
    mmALAssets_Init(&p->Assets);
    mmStream_Init(&p->Stream);
    mmMemset(&p->Vorbis, 0, sizeof(OggVorbis_File));
}

MM_EXPORT_AL
void
mmALAssetsOgg_Destroy(
    struct mmALAssetsOgg*                          p)
{
    mmMemset(&p->Vorbis, 0, sizeof(OggVorbis_File));
    mmStream_Destroy(&p->Stream);
    mmALAssets_Destroy(&p->Assets);
}

MM_EXPORT_AL
void
mmALAssetsOgg_Produce(
    struct mmALAssetsOgg*                          p)
{
    mmALAssetsOgg_Init(p);
    
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsOgg;
}

MM_EXPORT_AL
void
mmALAssetsOgg_Recycle(
    struct mmALAssetsOgg*                          p)
{
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsOgg;
    
    mmALAssetsOgg_Destroy(p);
}

MM_EXPORT_AL
int
mmALAssetsOgg_Prepare(
    struct mmALAssetsOgg*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d pFileContext is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmFileContext_AcquireStream(pFileContext, pFileName, "rb", &p->Stream);
        if (mmStream_Invalid(&p->Stream))
        {
            mmLogger_LogE(gLogger, "%s %d mmFileContext_AcquireStream failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        if(ov_open_callbacks(&p->Stream, &p->Vorbis, NULL, 0, mmALAssetsOggCallbacks) < 0)
        {
            mmLogger_LogE(gLogger, "%s %d ov_open_callbacks failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmALAssetsOggQueryInfo(&p->Vorbis, &p->Assets.hAssetsInfo);
        if (0 != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALAssetsOggQueryInfo failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALAssetsOgg_Discard(
    struct mmALAssetsOgg*                          p,
    struct mmFileContext*                          pFileContext)
{
    // Vorbis = { 0 } can safety ov_clear.
    ov_clear(&p->Vorbis);
    
    mmFileContext_ReleaseStream(pFileContext, &p->Stream);
}

MM_EXPORT_AL
double
mmALAssetsOgg_GetTotalTime(
    struct mmALAssetsOgg*                          p)
{
    return ov_time_total(&p->Vorbis, -1);
}

MM_EXPORT_AL
int64_t
mmALAssetsOgg_GetTotalFrame(
    struct mmALAssetsOgg*                          p)
{
    return (int64_t)ov_pcm_total(&p->Vorbis, -1);
}

MM_EXPORT_AL
int
mmALAssetsOgg_GetIsSeekable(
    struct mmALAssetsOgg*                          p)
{
    return (0 == ov_seekable(&p->Vorbis)) ? MM_FALSE : MM_TRUE;
}

MM_EXPORT_AL
size_t
mmALAssetsOgg_Read(
    struct mmALAssetsOgg*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght)
{
    int bitstream = 0;
    return (size_t)ov_read(&p->Vorbis, pBuffer, (int)(hLenght), 0, 2, 1, &bitstream);
}

MM_EXPORT_AL
int
mmALAssetsOgg_SeekTime(
    struct mmALAssetsOgg*                          p,
    double                                         hPosition)
{
    return ov_time_seek(&p->Vorbis, hPosition);
}

MM_EXPORT_AL
double
mmALAssetsOgg_TellTime(
    struct mmALAssetsOgg*                          p)
{
    return ov_time_tell(&p->Vorbis);
}

MM_EXPORT_AL
int
mmALAssetsOgg_SeekFrame(
    struct mmALAssetsOgg*                          p,
    int64_t                                        hPosition)
{
    return ov_pcm_seek(&p->Vorbis, (ogg_int64_t)hPosition);
}

MM_EXPORT_AL
int64_t
mmALAssetsOgg_TellFrame(
    struct mmALAssetsOgg*                          p)
{
    return (int64_t)ov_pcm_tell(&p->Vorbis);
}
