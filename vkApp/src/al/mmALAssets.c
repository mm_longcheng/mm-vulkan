/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssets.h"

#include "al/mmALError.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStreambuf.h"

MM_EXPORT_AL
void
mmALAssetsInfo_Reset(
    struct mmALAssetsInfo*                         p)
{
    mmMemset(p, 0, sizeof(struct mmALAssetsInfo));
}

MM_EXPORT_AL
int
mmALAssets16BitsPickFormat(
    struct mmALAssetsInfo*                         pAssetsInfo)
{
    int err = MM_UNKNOWN;
    
    // OpenAL format
    ALenum mFormat = 0;
    // Size of audio buffer (250ms)
    size_t mBufferSize = 0;
    
    switch(pAssetsInfo->Channels)
    {
    case 1:
        {
            mFormat = AL_FORMAT_MONO16;
            // Set BufferSize to 250ms (Frequency * 2 (16bit) divided by 4 (quarter of a second))
            mBufferSize = pAssetsInfo->SampleRate >> 1;
            // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
            mBufferSize -= (mBufferSize % 2);
        }
        break;
    case 2:
        {
            mFormat = AL_FORMAT_STEREO16;
            // Set BufferSize to 250ms (Frequency * 4 (16bit stereo) divided by 4 (quarter of a second))
            mBufferSize = pAssetsInfo->SampleRate;
            // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
            mBufferSize -= (mBufferSize % 4);
        }
        break;
    case 4:
        {
            mFormat = alGetEnumValue("AL_FORMAT_QUAD16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Set BufferSize to 250ms (Frequency * 8 (16bit 4-channel) divided by 4 (quarter of a second))
                mBufferSize = pAssetsInfo->SampleRate * 2;
                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % 8);
            }
        }
        break;
    case 6:
        {
            mFormat = alGetEnumValue("AL_FORMAT_51CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Set BufferSize to 250ms (Frequency * 12 (16bit 6-channel) divided by 4 (quarter of a second))
                mBufferSize = pAssetsInfo->SampleRate * 3;
                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % 12);
            }
        }
        break;
    case 7:
        {
            mFormat = alGetEnumValue("AL_FORMAT_61CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Set BufferSize to 250ms (Frequency * 16 (16bit 7-channel) divided by 4 (quarter of a second))
                mBufferSize = pAssetsInfo->SampleRate * 4;
                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % 16);
            }
        }
        break;
    case 8:
        {
            mFormat = alGetEnumValue("AL_FORMAT_71CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Set BufferSize to 250ms (Frequency * 20 (16bit 8-channel) divided by 4 (quarter of a second))
                mBufferSize = pAssetsInfo->SampleRate * 5;
                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % 20);
            }
        }
        break;
    default:
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            
            // Couldn't determine buffer format so log the error and default to mono
            mmLogger_LogW(gLogger, "mmALAssets channels invalid. Defaulting to MONO.");

            mFormat = AL_FORMAT_MONO16;
            // Set BufferSize to 250ms (Frequency * 2 (16bit) divided by 4 (quarter of a second))
            mBufferSize = pAssetsInfo->SampleRate >> 1;
            // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
            mBufferSize -= (mBufferSize % 2);
        }
        break;
    }
    
    if (MM_FAILURE != err)
    {
        pAssetsInfo->Format = mFormat;
        pAssetsInfo->Bytes250ms = mBufferSize;
        
        err = MM_SUCCESS;
    }
    
    return err;
}

static
int
mmALAssetsNone_Prepare(
    void*                                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName)
{
    return MM_FAILURE;
}

static
void
mmALAssetsNone_Discard(
    void*                                          p,
    struct mmFileContext*                          pFileContext)
{
    
}

static
double
mmALAssetsNone_GetTotalTime(
    void*                                          p)
{
    return 0.0;
}

static
int64_t
mmALAssetsNone_GetTotalFrame(
    void*                                          p)
{
    return 0;
}

static
int
mmALAssetsNone_GetIsSeekable(
    void*                                          p)
{
    return 0;
}

static
size_t
mmALAssetsNone_Read(
    void*                                          p,
    void*                                          pBuffer,
    size_t                                         hLenght)
{
    return 0;
}

static
int
mmALAssetsNone_SeekTime(
    void*                                          p,
    double                                         hPosition)
{
    return 0;
}

static
double
mmALAssetsNone_TellTime(
    void*                                          p)
{
    return 0.0;
}

static
int
mmALAssetsNone_SeekFrame(
    void*                                          p,
    int64_t                                        hPosition)
{
    return 0;
}

static
int64_t
mmALAssetsNone_TellFrame(
    void*                                          p)
{
    return 0;
}

const struct mmMetaAllocator mmALAllocatorAssetsNone =
{
    "mmALAssets",
    sizeof(struct mmALAssets),
    &mmALAssets_Produce,
    &mmALAssets_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataAssets mmALMetadataAssetsNone =
{
    &mmALAllocatorAssetsNone,
    /*- Interface -*/
    &mmALAssetsNone_Prepare,
    &mmALAssetsNone_Discard,
    &mmALAssetsNone_GetTotalTime,
    &mmALAssetsNone_GetTotalFrame,
    &mmALAssetsNone_GetIsSeekable,
    &mmALAssetsNone_Read,
    &mmALAssetsNone_SeekTime,
    &mmALAssetsNone_TellTime,
    &mmALAssetsNone_SeekFrame,
    &mmALAssetsNone_TellFrame,
};

MM_EXPORT_AL
void
mmALAssets_Init(
    struct mmALAssets*                             p)
{
    mmString_Init(&p->hFileName);
    mmALAssetsInfo_Reset(&p->hAssetsInfo);
    p->pMetadata = NULL;
}

MM_EXPORT_AL
void
mmALAssets_Destroy(
    struct mmALAssets*                             p)
{
    p->pMetadata = NULL;
    mmALAssetsInfo_Reset(&p->hAssetsInfo);
    mmString_Destroy(&p->hFileName);
}

MM_EXPORT_AL
void
mmALAssets_Produce(
    struct mmALAssets*                             p)
{
    mmALAssets_Init(p);
    p->pMetadata = &mmALMetadataAssetsNone;
}

MM_EXPORT_AL
void
mmALAssets_Recycle(
    struct mmALAssets*                             p)
{
    p->pMetadata = &mmALMetadataAssetsNone;
    mmALAssets_Destroy(p);
}

MM_EXPORT_AL
int
mmALAssets_Prepare(
    struct mmALAssets*                             p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName)
{
    typedef int
    (*Prepare)(
        void*                                          p,
        struct mmFileContext*                          pFileContext,
        const char*                                    pFileName);
    
    mmString_Assigns(&p->hFileName, pFileName);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((Prepare)(p->pMetadata->Prepare)))(p, pFileContext, pFileName);
}

MM_EXPORT_AL
void
mmALAssets_Discard(
    struct mmALAssets*                             p,
    struct mmFileContext*                          pFileContext)
{
    typedef void
    (*Discard)(
        void*                                          p,
        struct mmFileContext*                          pFileContext);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((Discard)(p->pMetadata->Discard)))(p, pFileContext);
}

MM_EXPORT_AL
double
mmALAssets_GetTotalTime(
    struct mmALAssets*                             p)
{
    typedef double
    (*GetTotalTime)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetTotalTime)(p->pMetadata->GetTotalTime)))(p);
}

MM_EXPORT_AL
int64_t
mmALAssets_GetTotalFrame(
    struct mmALAssets*                             p)
{
    typedef int64_t
    (*GetTotalFrame)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetTotalFrame)(p->pMetadata->GetTotalFrame)))(p);
}

MM_EXPORT_AL
int
mmALAssets_GetIsSeekable(
    struct mmALAssets*                             p)
{
    typedef int
    (*GetIsSeekable)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetIsSeekable)(p->pMetadata->GetIsSeekable)))(p);
}

MM_EXPORT_AL
size_t
mmALAssets_Read(
    struct mmALAssets*                             p,
    void*                                          pBuffer,
    size_t                                         hLenght)
{
    typedef size_t
    (*Read)(
        void*                                          p,
        void*                                          pBuffer,
        size_t                                         hLenght);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((Read)(p->pMetadata->Read)))(p, pBuffer, hLenght);
}

MM_EXPORT_AL
int
mmALAssets_SeekTime(
    struct mmALAssets*                             p,
    double                                         hPosition)
{
    typedef int
    (*SeekTime)(
        void*                                          p,
        double                                         hPosition);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((SeekTime)(p->pMetadata->SeekTime)))(p, hPosition);
}

MM_EXPORT_AL
double
mmALAssets_TellTime(
    struct mmALAssets*                             p)
{
    typedef double
    (*TellTime)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((TellTime)(p->pMetadata->TellTime)))(p);
}

MM_EXPORT_AL
int
mmALAssets_SeekFrame(
    struct mmALAssets*                             p,
    int64_t                                        hPosition)
{
    typedef int
    (*SeekFrame)(
        void*                                          p,
        int64_t                                        hPosition);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((SeekFrame)(p->pMetadata->SeekFrame)))(p, hPosition);
}

MM_EXPORT_AL
int64_t
mmALAssets_TellFrame(
    struct mmALAssets*                             p)
{
    typedef int64_t
    (*TellFrame)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((TellFrame)(p->pMetadata->TellFrame)))(p);
}

MM_EXPORT_AL
void
mmALAssets_ReadWholeData(
    struct mmALAssets*                             p,
    struct mmStreambuf*                            s)
{
    size_t size;
    mmUInt8_t* data = NULL;
    struct mmALAssetsInfo* pAssetsInfo = &p->hAssetsInfo;
    size_t hBytesBlock = pAssetsInfo->ByteLength;
    mmStreambuf_Reset(s);
    mmStreambuf_AlignedMemory(s, pAssetsInfo->ByteLength);
    
    do
    {
        data = s->buff + s->pptr;
        size = mmALAssets_Read(p, data, hBytesBlock - s->pptr);
        mmStreambuf_Pbump(s, size);
    }
    while(0 < size);
}

MM_EXPORT_AL
void
mmALAssets_ReadBlockData(
    struct mmALAssets*                             p,
    struct mmStreambuf*                            s,
    size_t                                         hBytesBlock,
    int                                            hLoop)
{
    size_t size;
    mmUInt8_t* data = NULL;
    struct mmALAssetsInfo* pAssetsInfo = &p->hAssetsInfo;
    int Seekable = pAssetsInfo->Seekable;
    int64_t TellFrame = mmALAssets_TellFrame(p);
    int hStreamEOF = (mmUInt64_t)TellFrame >= pAssetsInfo->TotalFrame;
    
    mmStreambuf_Reset(s);
    mmStreambuf_AlignedMemory(s, hBytesBlock);
    
    // Read only what was asked for
    while(!hStreamEOF && s->pptr < hBytesBlock)
    {
        data = s->buff + s->pptr;
        size = mmALAssets_Read(p, data, hBytesBlock - s->pptr);
        // EOF check
        if (0 == size)
        {
            // If set to loop wrap to start of stream
            if (hLoop && Seekable)
            {
                if (mmALAssets_SeekTime(p, 0.0))
                {
                    struct mmLogger* gLogger = mmLogger_Instance();
                    mmLogger_LogE(gLogger, "mmALAssets Looping stream failure.");
                    break;
                }
            }
            else
            {
                hStreamEOF = MM_TRUE;
                // Don't loop - finish.
                break;
            }
        }
        else
        {
            mmStreambuf_Pbump(s, size);
        }
    }
}

MM_EXPORT_AL
int
mmALAssets_LoadWholeData(
    struct mmALAssets*                             p,
    ALuint                                         hBuffer,
    struct mmStreambuf*                            s)
{
    int err = MM_UNKNOWN;
    
    mmUInt8_t* data;
    size_t size;
    
    struct mmALAssetsInfo* pAssetsInfo = &p->hAssetsInfo;
    
    mmALAssets_ReadWholeData(p, s);
    
    size = mmStreambuf_Size(s);
    data = s->buff + s->gptr;
    
    if (0 != size)
    {
        alBufferData(
            hBuffer,
            pAssetsInfo->Format,
            (const ALvoid*)data,
            (ALsizei)size,
            (ALsizei)pAssetsInfo->SampleRate);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        err = MM_SUCCESS;
    }
    else
    {
        err = MM_FAILURE;
    }
    
    return err;
}

MM_EXPORT_AL
int
mmALAssets_Load250msData(
    struct mmALAssets*                             p,
    ALuint                                         hBuffer,
    int                                            hLoop,
    struct mmStreambuf*                            s)
{
    int err = MM_UNKNOWN;
    
    mmUInt8_t* data;
    size_t size;
    
    struct mmALAssetsInfo* pAssetsInfo = &p->hAssetsInfo;
    
    mmALAssets_ReadBlockData(
        p,
        s,
        (size_t)pAssetsInfo->Bytes250ms,
        hLoop);
    
    size = mmStreambuf_Size(s);
    data = s->buff + s->gptr;
    
    if (0 != size)
    {
        alBufferData(
            hBuffer,
            pAssetsInfo->Format,
            (const ALvoid*)data,
            (ALsizei)size,
            (ALsizei)pAssetsInfo->SampleRate);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        err = MM_SUCCESS;
    }
    else
    {
        err = MM_FAILURE;
    }
    
    return err;
}

