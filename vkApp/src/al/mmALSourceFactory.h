/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALSourceFactory_h__
#define __mmALSourceFactory_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "al/mmALSource.h"

#include "core/mmPrefix.h"

struct mmALSourceFactory
{
    struct mmRbtreeStrVpt types;
};

MM_EXPORT_AL
void
mmALSourceFactory_Init(
    struct mmALSourceFactory*                      p);

MM_EXPORT_AL
void
mmALSourceFactory_Destroy(
    struct mmALSourceFactory*                      p);

MM_EXPORT_AL
void
mmALSourceFactory_AddType(
    struct mmALSourceFactory*                      p,
    const struct mmALMetadataSource*               m);

MM_EXPORT_AL
void
mmALSourceFactory_RmvType(
    struct mmALSourceFactory*                      p,
    const struct mmALMetadataSource*               m);

MM_EXPORT_AL
const struct mmALMetadataSource*
mmALSourceFactory_GetType(
    struct mmALSourceFactory*                      p,
    const char*                                    type);

MM_EXPORT_AL
struct mmALSource*
mmALSourceFactory_Produce(
    struct mmALSourceFactory*                      p,
    const char*                                    type);

MM_EXPORT_AL
void
mmALSourceFactory_Recycle(
    struct mmALSourceFactory*                      p,
    struct mmALSource*                             v);

#include "core/mmSuffix.h"

#endif//__mmALSourceFactory_h__
