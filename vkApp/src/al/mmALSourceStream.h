/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALSourceStream_h__
#define __mmALSourceStream_h__

#include "core/mmCore.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"
#include "al/mmALSource.h"

#include "core/mmPrefix.h"

struct mmALBufferLoader;

// Update interval 16ms, one buffer 250ms, 4 is enough.
#define MM_ALSOURCE_NUMBER_BUFFER 4

MM_EXPORT_AL extern const struct mmALMetadataSource mmALMetadataSourceStream;

struct mmALSourceStream
{
    // super type.
    struct mmALSource hSuper;
    
    // OpenAL buffer.
    ALuint hBuffer[MM_ALSOURCE_NUMBER_BUFFER];

    // Last buffer end time.
    double hLastTime;
    // Last buffer end frame number.
    mmUInt64_t hLastFrame;
    
    // Source assets.
    struct mmALAssets* pAssets;
};

MM_EXPORT_AL
void
mmALSourceStream_Init(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_Destroy(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_Produce(
    struct mmALSourceStream*                       p);

/* interface */

MM_EXPORT_AL
void
mmALSourceStream_Recycle(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
int
mmALSourceStream_Prepare(
    struct mmALSourceStream*                       p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALSourceStream_Discard(
    struct mmALSourceStream*                       p,
    struct mmALBufferLoader*                       pBufferLoader);

MM_EXPORT_AL
int
mmALSourceStream_GetIsStream(
    const struct mmALSourceStream*                 p);

MM_EXPORT_AL
struct mmALAssets*
mmALSourceStream_GetAssets(
    const struct mmALSourceStream*                 p);

MM_EXPORT_AL
void
mmALSourceStream_SetLoop(
    struct mmALSourceStream*                       p,
    int                                            hLoop);

MM_EXPORT_AL
void
mmALSourceStream_RewindImmediately(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_SetPlayPosition(
    struct mmALSourceStream*                       p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALSourceStream_GetPlayPosition(
    const struct mmALSourceStream*                 p);

MM_EXPORT_AL
void
mmALSourceStream_UpdatePlayPosition(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_Update(
    struct mmALSourceStream*                       p,
    double                                         interval);

/* special */

MM_EXPORT_AL
void
mmALSourceStream_UpdateTrackData(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_Enqueuebuffers(
    struct mmALSourceStream*                       p);

MM_EXPORT_AL
void
mmALSourceStream_UnqueueBuffers(
    struct mmALSourceStream*                       p);

#include "core/mmSuffix.h"

#endif//__mmALSourceStream_h__
