/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALSourceStream.h"

#include "al/mmALError.h"
#include "al/mmALAssets.h"
#include "al/mmALBufferLoader.h"
#include "al/mmALAssetsLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStreambuf.h"

const struct mmMetaAllocator mmALAllocatorSourceStream =
{
    "mmALSourceStream",
    sizeof(struct mmALSourceStream),
    &mmALSourceStream_Produce,
    &mmALSourceStream_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataSource mmALMetadataSourceStream =
{
    &mmALAllocatorSourceStream,
    /*- Interface -*/
    &mmALSourceStream_Prepare,
    &mmALSourceStream_Discard,
    &mmALSourceStream_GetIsStream,
    &mmALSourceStream_GetAssets,
    &mmALSourceStream_SetLoop,
    &mmALSourceStream_RewindImmediately,
    &mmALSourceStream_SetPlayPosition,
    &mmALSourceStream_GetPlayPosition,
    &mmALSourceStream_UpdatePlayPosition,
    &mmALSourceStream_Update,
};

MM_EXPORT_AL
void
mmALSourceStream_Init(
    struct mmALSourceStream*                       p)
{
    mmALSource_Init(&p->hSuper);
    mmMemset(p->hBuffer, 0, sizeof(p->hBuffer));
    p->hLastTime = 0.0;
    p->hLastFrame = 0;
    p->pAssets = NULL;
}

MM_EXPORT_AL
void
mmALSourceStream_Destroy(
    struct mmALSourceStream*                       p)
{
    p->pAssets = NULL;
    p->hLastFrame = 0;
    p->hLastTime = 0.0;    mmMemset(p->hBuffer, 0, sizeof(p->hBuffer));
    mmALSource_Destroy(&p->hSuper);
}

MM_EXPORT_AL
void
mmALSourceStream_Produce(
    struct mmALSourceStream*                       p)
{
    mmALSourceStream_Init(p);
    p->hSuper.pMetadata = &mmALMetadataSourceStream;
}

MM_EXPORT_AL
void
mmALSourceStream_Recycle(
    struct mmALSourceStream*                       p)
{
    p->hSuper.pMetadata = &mmALMetadataSourceStream;
    mmALSourceStream_Destroy(p);
}

MM_EXPORT_AL
int
mmALSourceStream_Prepare(
    struct mmALSourceStream*                       p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName)
{
    int err = MM_UNKNOWN;
    
    do
    {
        ALenum ALErr;
        
        struct mmALAssetsLoader* pAssetsLoader;
        
        struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pBufferLoader)
        {
            mmLogger_LogE(gLogger, "%s %d pBufferLoader is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        if (NULL == pFileName)
        {
            mmLogger_LogE(gLogger, "%s %d pFileName is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        pAssetsLoader = pBufferLoader->pAssetsLoader;
        if (NULL == pAssetsLoader)
        {
            mmLogger_LogE(gLogger, "%s %d pAssetsLoader is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        p->pAssets = mmALAssetsLoader_Produce(pAssetsLoader, pFileName);
        if (NULL == p->pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d pAssets is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        alGenBuffers(MM_ALSOURCE_NUMBER_BUFFER, p->hBuffer);
        ALErr = alGetError();
        mmALCheck(MM_LOG_ERROR, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d AL alGenBuffers failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmALSource_PrepareBase(&p->hSuper);
        if (MM_BUSY == err)
        {
            // driver busy not a error.
            break;
        }
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALSource_Prepare failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        // Notify listener
        (*(pCallback->OnLoaded))(pCallback->obj, &p->hSuper);
    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALSourceStream_Discard(
    struct mmALSourceStream*                       p,
    struct mmALBufferLoader*                       pBufferLoader)
{
    do
    {
        int i = 0;
        
        struct mmALAssetsLoader* pAssetsLoader;
        
        struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pBufferLoader)
        {
            mmLogger_LogE(gLogger, "%s %d pBufferLoader is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        pAssetsLoader = pBufferLoader->pAssetsLoader;
        if (NULL == pAssetsLoader)
        {
            mmLogger_LogE(gLogger, "%s %d pAssetsLoader is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        // Notify listener
        (*(pCallback->OnDestroyed))(pCallback->obj, &p->hSuper);
        
        mmALSource_DiscardBase(&p->hSuper);
        
        while (MM_ALSOURCE_NUMBER_BUFFER > i)
        {
            if (AL_NONE != p->hBuffer[i])
            {
                alDeleteBuffers(1, &p->hBuffer[i]);
                mmALCheck(MM_LOG_ERROR, alGetError());
                p->hBuffer[i] = AL_NONE;
            }
            i++;
        }
        
        mmALAssetsLoader_Recycle(pAssetsLoader, p->pAssets);
        p->pAssets = NULL;

    } while(0);
}

MM_EXPORT_AL
int
mmALSourceStream_GetIsStream(
    const struct mmALSourceStream*                 p)
{
    return MM_TRUE;
}

MM_EXPORT_AL
struct mmALAssets*
mmALSourceStream_GetAssets(
    const struct mmALSourceStream*                 p)
{
    return p->pAssets;
}

MM_EXPORT_AL
void
mmALSourceStream_SetLoop(
    struct mmALSourceStream*                       p,
    int                                            hLoop)
{
    p->hSuper.hLoop = hLoop;
}

MM_EXPORT_AL
void
mmALSourceStream_RewindImmediately(
    struct mmALSourceStream*                       p)
{
    do
    {
        enum mmALSourceState_t hState;
        struct mmALAssets* pAssets = p->pAssets;
        struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }

        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        // cache data.
        // AL_NONE != p->hSource here.
        hState = p->hSuper.hState;

        // Seek time.
        mmALSource_Pause(&p->hSuper);
        mmALAssets_SeekTime(pAssets, 0.0);

        // Unqueue all buffers
        mmALSourceStream_UnqueueBuffers(p);

        // Fill buffers
        mmALSourceStream_Enqueuebuffers(p);

        // Reset state..
        switch (hState)
        {
        case mmALSourceStatePlaying:
            mmALSource_Play(&p->hSuper);
            break;

        case mmALSourceStatePaused:
            mmALSource_Pause(&p->hSuper);
            break;

        default:
            break;
        }

        p->hLastFrame = 0;
        p->hLastTime = 0.0;
    } while(0);
}

MM_EXPORT_AL
void
mmALSourceStream_SetPlayPosition(
    struct mmALSourceStream*                       p,
    double                                         hPosition)
{
    do
    {
        double hSeconds;
        struct mmALAssets* pAssets = p->pAssets;
        struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;

        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        if (0.0 > hPosition)
        {
            break;
        }

        // time wrap.
        hSeconds = fmod(hPosition, pAssetsInfo->TotalTime);

        // Set position
        p->hSuper.hPlayPosition = hSeconds;

        // Set flag
        p->hSuper.hPlayPositionChanged = MM_TRUE;
    } while(0);
}

MM_EXPORT_AL
double
mmALSourceStream_GetPlayPosition(
    const struct mmALSourceStream*                 p)
{
    double hSeconds = -1.0;

    do
    {
        double hPosition;
        ALfloat hSourcePosition;
        struct mmALAssets* pAssets = p->pAssets;
        struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;

        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }

        hSourcePosition = (ALfloat)0.0f;
        alGetSourcef(p->hSuper.hSource, AL_SEC_OFFSET, &hSourcePosition);
        mmALCheck(MM_LOG_ERROR, alGetError());

        hPosition = p->hLastTime + hSourcePosition;
        // Wrap if necessary
        if (hPosition >= pAssetsInfo->TotalTime)
        {
            hSeconds = hPosition - pAssetsInfo->TotalTime;
        }
        else
        {
            hSeconds = hPosition;
        }
    } while(0);

    return hSeconds;
}

MM_EXPORT_AL
void
mmALSourceStream_UpdatePlayPosition(
    struct mmALSourceStream*                       p)
{
    do
    {
        enum mmALSourceState_t hState;
        struct mmALAssets* pAssets = p->pAssets;
        struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }

        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        // cache data.
        // AL_NONE != p->hSource here.
        hState = p->hSuper.hState;

        // Seek time.
        mmALSource_Pause(&p->hSuper);
        mmALAssets_SeekTime(pAssets, p->hSuper.hPlayPosition);

        // Unqueue all buffers
        mmALSourceStream_UnqueueBuffers(p);

        // Fill buffers
        mmALSourceStream_Enqueuebuffers(p);

        // Reset state..
        switch (hState)
        {
        case mmALSourceStatePlaying:
            mmALSource_Play(&p->hSuper);
            break;

        case mmALSourceStatePaused:
            mmALSource_Pause(&p->hSuper);
            break;

        default:
            break;
        }

        // Set flag
        p->hSuper.hPlayPositionChanged = MM_FALSE;
        p->hLastFrame = mmALAssets_TellFrame(pAssets);
        p->hLastTime = p->hSuper.hPlayPosition;
    } while(0);
}

MM_EXPORT_AL
void
mmALSourceStream_Update(
    struct mmALSourceStream*                       p,
    double                                         interval)
{
    ALenum state;

    if (!mmALSource_GetIsPlaying(&p->hSuper))
    {
        return;
    }

    alGetSourcei(p->hSuper.hSource, AL_SOURCE_STATE, &state);
    mmALCheck(MM_LOG_ERROR, alGetError());

    if (state == AL_PAUSED)
    {
        return;
    }

    // Ran out of buffer data?
    if (state == AL_STOPPED)
    {
        struct mmALAssets* pAssets = p->pAssets;
        struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;
        int64_t TellFrame = mmALAssets_TellFrame(pAssets);
        if((mmUInt64_t)TellFrame >= pAssetsInfo->TotalFrame)
        {
            struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
            struct mmALSourceFinished* pFinished = &p->hSuper.hFinished;
            
            // It is stream EOF.
            mmALSource_Stop(&p->hSuper);
            
            // Clear audio data already played...
            mmALSourceStream_UnqueueBuffers(p);

            // Finished callback
            // Notify listener
            (*(pCallback->OnFinished))(pCallback->obj, &p->hSuper);
            
            // Finished handler listener.
            (*(pFinished->OnFinished))(pFinished->obj, &p->hSuper);
            
            // return immediately.
            return;
        }
        else
        {
            // long time not update, all buffer is finish.
            // we need clear and rebind buffer play.

            // Clear audio data already played...
            mmALSourceStream_UnqueueBuffers(p);

            // Fill with next chunk of audio...
            mmALSourceStream_Enqueuebuffers(p);

            // Play...
            alSourcePlay(p->hSuper.hSource);
            mmALCheck(MM_LOG_ERROR, alGetError());
        }
    }

    // handle play position change
    if (p->hSuper.hPlayPositionChanged)
    {
        mmALSourceStream_UpdatePlayPosition(p);
    }
    else
    {
        mmALSourceStream_UpdateTrackData(p);
    }
}

MM_EXPORT_AL
void
mmALSourceStream_UpdateTrackData(
    struct mmALSourceStream*                       p)
{
    ALuint hBuffer;
    ALint size, bits, channels, freq;
    mmUInt64_t hFrameCount;
    int processed = 0;
    int err;

    struct mmStreambuf s;
    struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
    struct mmALAssets* pAssets = p->pAssets;
    struct mmALAssetsInfo* pAssetsInfo = &pAssets->hAssetsInfo;

    alGetSourcei(p->hSuper.hSource, AL_BUFFERS_PROCESSED, &processed);
    mmALCheck(MM_LOG_ERROR, alGetError());

    mmStreambuf_Init(&s);

    while (processed--)
    {
        alSourceUnqueueBuffers(p->hSuper.hSource, 1, &hBuffer);
        mmALCheck(MM_LOG_ERROR, alGetError());

        // Get buffer details
        alGetBufferi(hBuffer, AL_SIZE, &size);
        alGetBufferi(hBuffer, AL_BITS, &bits);
        alGetBufferi(hBuffer, AL_CHANNELS, &channels);
        alGetBufferi(hBuffer, AL_FREQUENCY, &freq);
        mmALCheck(MM_LOG_ERROR, alGetError());

        // Update offset (in seconds)
        hFrameCount = (size * 8) / (channels * bits);
        p->hLastFrame += hFrameCount;
        if (p->hLastFrame >= pAssetsInfo->TotalFrame)
        {
            // frame number wrap.
            p->hLastFrame -= pAssetsInfo->TotalFrame;
            p->hLastTime = (double)p->hLastFrame / (double)freq;

            // Notify listener
            (*(pCallback->OnLooping))(pCallback->obj, &p->hSuper);
        }
        else
        {
            p->hLastTime = (double)p->hLastFrame / (double)freq;
        }

        err = mmALAssets_Load250msData(
            pAssets,
            hBuffer,
            p->hSuper.hLoop,
            &s);
        if (MM_SUCCESS == err)
        {
            alSourceQueueBuffers(p->hSuper.hSource, 1, &hBuffer);
            mmALCheck(MM_LOG_ERROR, alGetError());
        }
    }

    mmStreambuf_Destroy(&s);
}

MM_EXPORT_AL
void
mmALSourceStream_Enqueuebuffers(
    struct mmALSourceStream*                       p)
{
    if (AL_NONE != p->hSuper.hSource)
    {
        int err;
        int i = 0;
        struct mmStreambuf s;
        struct mmALAssets* pAssets = p->pAssets;

        mmStreambuf_Init(&s);

        while (MM_ALSOURCE_NUMBER_BUFFER > i)
        {
            err = mmALAssets_Load250msData(
                pAssets,
                p->hBuffer[i],
                p->hSuper.hLoop,
                &s);

            if (MM_SUCCESS == err)
            {
                alSourceQueueBuffers(p->hSuper.hSource, 1, &p->hBuffer[i++]);
                mmALCheck(MM_LOG_ERROR, alGetError());
            }
            else
            {
                break;
            }
        }

        mmStreambuf_Destroy(&s);
    }
}

MM_EXPORT_AL
void
mmALSourceStream_UnqueueBuffers(
    struct mmALSourceStream*                       p)
{
    do
    {
        ALenum state;
        ALuint hBuffer;
        int processed = 0;

        if(AL_NONE == p->hSuper.hSource)
        {
            break;
        }

        alGetSourcei(p->hSuper.hSource, AL_SOURCE_STATE, &state);
        mmALCheck(MM_LOG_ERROR, alGetError());

        if (AL_INITIAL == state)
        {
            // Force mSource to change state so the call to alSourceStop()
            // will mark buffers correctly.
            alSourcePlay(p->hSuper.hSource);
            mmALCheck(MM_LOG_ERROR, alGetError());
        }

        // Stop source to allow unqueuing
        alSourceStop(p->hSuper.hSource);
        mmALCheck(MM_LOG_ERROR, alGetError());

        // Get number of buffers queued on source
        alGetSourcei(p->hSuper.hSource, AL_BUFFERS_PROCESSED, &processed);
        mmALCheck(MM_LOG_ERROR, alGetError());

        while (processed--)
        {
            // Remove number of buffers from source
            alSourceUnqueueBuffers(p->hSuper.hSource, 1, &hBuffer);
            mmALCheck(MM_LOG_ERROR, alGetError());
        }
    } while(0);
}
