/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALSource_h__
#define __mmALSource_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmMetadata.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"

#include "core/mmPrefix.h"

struct mmStreambuf;

struct mmALBufferLoader;

enum mmALSourceState_t
{
    mmALSourceStateNone      = 0,
    mmALSourceStatePlaying   = 1,
    mmALSourceStatePaused    = 2,
    mmALSourceStateStopped   = 3,
    mmALSourceStateDestroyed = 4,
};

struct mmALSource;

MM_EXPORT_AL
void
mmALSourceCallbackDefault(
    void*                                          obj,
    struct mmALSource*                             s);

struct mmALSourceCallback
{
    void(*OnLoaded   )(void* obj, struct mmALSource* s);
    void(*OnDestroyed)(void* obj, struct mmALSource* s);
    void(*OnPlayed   )(void* obj, struct mmALSource* s);
    void(*OnStopped  )(void* obj, struct mmALSource* s);
    void(*OnFinished )(void* obj, struct mmALSource* s);
    void(*OnPaused   )(void* obj, struct mmALSource* s);
    void(*OnLooping  )(void* obj, struct mmALSource* s);
    void* obj;
};

MM_EXPORT_AL
void
mmALSourceCallback_Reset(
    struct mmALSourceCallback*                     p);

struct mmALSourceFinished
{
    void(*OnFinished )(void* obj, struct mmALSource* s);
    void* obj;
};

MM_EXPORT_AL
void
mmALSourceFinished_Reset(
    struct mmALSourceFinished*                     p);

struct mmALMetadataSource
{
    // type allocator.
    const struct mmMetaAllocator* Allocator;
    
    // int
    // (*Prepare)(
    //     struct mmALSource*                             p,
    //     struct mmALBufferLoader*                       pBufferLoader,
    //     const char*                                    pFileName);
    void* Prepare;

    // void
    // (*Discard)(
    //     struct mmALSource*                             p,
    //     struct mmALBufferLoader*                       pBufferLoader);
    void* Discard;
    
    // int
    // (*GetIsStream)(
    //     const struct mmALSource*                       p);
    void* GetIsStream;
    
    // struct mmALAssets*
    // (*GetAssets)(
    //     const struct mmALSource*                       p);
    void* GetAssets;
    
    // void
    // (*SetLoop)(
    //     struct mmALSource*                             p,
    //     int                                            hLoop);
    void* SetLoop;
    
    // void
    // (*RewindImmediately)(
    //     struct mmALSource*                             p);
    void* RewindImmediately;
    
    // void
    // (*SetPlayPosition)(
    //     struct mmALSource*                             p,
    //     double                                         hPosition);
    void* SetPlayPosition;

    // double
    // (*GetPlayPosition)(
    //     const struct mmALSource*                       p);
    void* GetPlayPosition;
    
    // void
    // (*UpdatePlayPosition)(
    //     struct mmALSource*                             p);
    void* UpdatePlayPosition;
    
    // void
    // (*Update)(
    //     struct mmALSource*                             p,
    //     double                                         interval);
    void* Update;
};

MM_EXPORT_AL extern const struct mmALMetadataSource mmALMetadataSourceNone;

struct mmALSource
{
    // Source name.
    struct mmString hName;
    
    // Source state event callback.
    struct mmALSourceCallback hCallback;
    
    // Source finished handler handler.
    // Independent of the callback.
    // It is use for auto destroy handler.
    struct mmALSourceFinished hFinished;
    
    // Metadata.
    const struct mmALMetadataSource* pMetadata;
    
    // Source state enum mmALSourceState_t
    enum mmALSourceState_t hState;
    
    // OpenAL source.
    ALuint hSource;
    
    // Loop status
    int hLoop;
    
    // play position cache value whether changed.
    int hPlayPositionChanged;
    // play position cache value.
    double hPlayPosition;
    
    // OpenAL source status.
    // alSource will change, we need cache value.
    float hPosition[3];          // 3D position
    float hDirection[3];         // 3D direction
    float hVelocity[3];          // 3D velocity
    float hVolume;               // Current volume
    float hMaxVolume;            // Maximum volume
    float hMinVolume;            // Minimum volume
    float hMaxDistance;          // Maximum attenuation distance
    float hRolloffFactor;        // Rolloff factor for attenuation
    float hReferenceDistance;    // Half-volume distance for attenuation
    float hOuterConeVolume;      // Outer cone volume
    float hInnerConeAngle;       // Inner cone angle
    float hOuterConeAngle;       // outer cone angle
    float hPitch;                // Current pitch
    int hSourceRelative;         // Relative position flag
};

MM_EXPORT_AL
void
mmALSource_Init(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Destroy(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Produce(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Recycle(
    struct mmALSource*                             p);

MM_EXPORT_AL
int
mmALSource_PrepareBase(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_DiscardBase(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_SetName(
    struct mmALSource*                             p,
    const char*                                    pName);

MM_EXPORT_AL
void
mmALSource_SetCallback(
    struct mmALSource*                             p,
    const struct mmALSourceCallback*               pCallback);

MM_EXPORT_AL
void
mmALSource_SetFinished(
    struct mmALSource*                             p,
    const struct mmALSourceFinished*               pFinished);

MM_EXPORT_AL
int
mmALSource_GetIsPlaying(
    const struct mmALSource*                       p);

MM_EXPORT_AL
int
mmALSource_GetIsPaused(
    const struct mmALSource*                       p);

MM_EXPORT_AL
int
mmALSource_Invalid(
    const struct mmALSource*                       p);

// update current cache Propertys to alSource.
MM_EXPORT_AL
void
mmALSource_UpdatePropertys(
    const struct mmALSource*                       p);

/* OpenAL source status API */

MM_EXPORT_AL
void
mmALSource_SetPosition(
    struct mmALSource*                             p,
    const float                                    position[3]);

MM_EXPORT_AL
void
mmALSource_GetPosition(
    const struct mmALSource*                       p,
    float                                          position[3]);

MM_EXPORT_AL
void
mmALSource_SetDirection(
    struct mmALSource*                             p,
    const float                                    direction[3]);

MM_EXPORT_AL
void
mmALSource_GetDirection(
    const struct mmALSource*                       p,
    float                                          direction[3]);

MM_EXPORT_AL
void
mmALSource_SetVelocity(
    struct mmALSource*                             p,
    const float                                    velocity[3]);

MM_EXPORT_AL
void
mmALSource_GetVelocity(
    const struct mmALSource*                       p,
    float                                          velocity[3]);

MM_EXPORT_AL
void
mmALSource_SetVolume(
    struct mmALSource*                             p,
    float                                          volume);

MM_EXPORT_AL
float
mmALSource_GetVolume(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetMaxVolume(
    struct mmALSource*                             p,
    float                                          maxVolume);

MM_EXPORT_AL
float
mmALSource_GetMaxVolume(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetMinVolume(
    struct mmALSource*                             p,
    float                                          minVolume);

MM_EXPORT_AL
float
mmALSource_GetMinVolume(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetInnerConeAngle(
    struct mmALSource*                             p,
    float                                          innerConeAngle);

MM_EXPORT_AL
float
mmALSource_GetInnerConeAngle(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetOuterConeAngle(
    struct mmALSource*                             p,
    float                                          outerConeAngle);

MM_EXPORT_AL
float
mmALSource_GetOuterConeAngle(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetOuterConeVolume(
    struct mmALSource*                             p,
    float                                          volume);

MM_EXPORT_AL
float
mmALSource_GetOuterConeVolume(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetMaxDistance(
    struct mmALSource*                             p,
    float                                          maxDistance);

MM_EXPORT_AL
float
mmALSource_GetMaxDistance(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetRolloffFactor(
    struct mmALSource*                             p,
    float                                          rolloffFactor);

MM_EXPORT_AL
float
mmALSource_GetRolloffFactor(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetReferenceDistance(
    struct mmALSource*                             p,
    float                                          referenceDistance);

MM_EXPORT_AL
float
mmALSource_GetReferenceDistance(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetPitch(
    struct mmALSource*                             p,
    float                                          pitch);

MM_EXPORT_AL
float
mmALSource_GetPitch(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetRelativeToListener(
    struct mmALSource*                             p,
    int                                            relative);

MM_EXPORT_AL
int
mmALSource_GetRelativeToListener(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_Play(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Stop(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Rewind(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Pause(
    struct mmALSource*                             p);

/* interface */

MM_EXPORT_AL
int
mmALSource_Prepare(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALSource_Discard(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader);

MM_EXPORT_AL
int
mmALSource_GetIsStream(
    const struct mmALSource*                       p);

MM_EXPORT_AL
struct mmALAssets*
mmALSource_GetAssets(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_SetLoop(
    struct mmALSource*                             p,
    int                                            hLoop);

MM_EXPORT_AL
void
mmALSource_RewindImmediately(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_SetPlayPosition(
    struct mmALSource*                             p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALSource_GetPlayPosition(
    const struct mmALSource*                       p);

MM_EXPORT_AL
void
mmALSource_UpdatePlayPosition(
    struct mmALSource*                             p);

MM_EXPORT_AL
void
mmALSource_Update(
    struct mmALSource*                             p,
    double                                         interval);

#include "core/mmSuffix.h"

#endif//__mmALSource_h__
