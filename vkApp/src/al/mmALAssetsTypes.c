/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssetsTypes.h"

#include "al/mmALAssetsFactory.h"

#include "al/mmALAssets.h"
#include "al/mmALAssetsMp3.h"
#include "al/mmALAssetsOgg.h"
#include "al/mmALAssetsWav.h"

MM_EXPORT_AL
void
mmALAssetsFactoryAddTypes(
    struct mmALAssetsFactory*                      pFactory)
{
    mmALAssetsFactory_AddType(pFactory, &mmALMetadataAssetsMp3);
    mmALAssetsFactory_AddType(pFactory, &mmALMetadataAssetsOgg);
    mmALAssetsFactory_AddType(pFactory, &mmALMetadataAssetsWav);
}

MM_EXPORT_AL
void
mmALAssetsFactoryRmvTypes(
    struct mmALAssetsFactory*                      pFactory)
{
    mmALAssetsFactory_RmvType(pFactory, &mmALMetadataAssetsWav);
    mmALAssetsFactory_RmvType(pFactory, &mmALMetadataAssetsOgg);
    mmALAssetsFactory_RmvType(pFactory, &mmALMetadataAssetsMp3);
}

