/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALDevice.h"

#include "core/mmLogger.h"

MM_EXPORT_AL
void
mmALDevice_Init(
    struct mmALDevice*                             p)
{
    mmString_Init(&p->hDeviceName);
    mmRbtsetString_Init(&p->hDevices);
    p->device = NULL;
}

MM_EXPORT_AL
void
mmALDevice_Destroy(
    struct mmALDevice*                             p)
{
    p->device = NULL;
    mmRbtsetString_Destroy(&p->hDevices);
    mmString_Destroy(&p->hDeviceName);
}

MM_EXPORT_AL
void
mmALDevice_SetDeviceName(
    struct mmALDevice*                             p,
    const char*                                    pDeviceName)
{
    mmString_Assigns(&p->hDeviceName, pDeviceName);
}

MM_EXPORT_AL
void
mmALDevice_EnumerateDevices(
    struct mmALDevice*                             p)
{
    // alcGetString return a weak reference string, not need free.
    const ALCchar* pDevices = alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
    
    if(NULL != pDevices)
    {
        /*
        ** The list returned by the call to alcGetString has the names of the
        ** devices seperated by NULL characters and the list is terminated by
        ** two NULL characters, so we can cast the list into a string and it
        ** will automatically stop at the first NULL that it sees, then we
        ** can move the pointer ahead by the lenght of that string + 1 and we
        ** will be at the begining of the next string.  Once we hit an empty
        ** string we know that we've found the double NULL that terminates the
        ** list and we can stop there.
        */
        
        struct mmString hDeviceName;

        mmRbtsetString_Clear(&p->hDevices);

        while ((*pDevices) != 0)
        {
            mmString_MakeWeaks(&hDeviceName, pDevices);

            mmRbtsetString_Add(&p->hDevices, &hDeviceName);

            pDevices += strlen(pDevices) + 1;
        }
    }
}

MM_EXPORT_AL
int
mmALDevice_Prepare(
    struct mmALDevice*                             p)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmALDevice_EnumerateDevices(p);

        if (0 == p->hDevices.size)
        {
            // IOS alcGetString ALC_ALL_DEVICES_SPECIFIER devices may be empty.
            //
            // https://stackoverflow.com/questions/24067201/getting-an-audio-device-with-openal
            // Apple's implementation of alcOpenDevice() only returns the device once.
            // Every subsequent call returns NULL. This function can be called by a lot of
            // Apple audio code, so take out EVERY TRACE of audio code before using OpenAL
            // and manually calling that function yourself.
            mmLogger_LogI(gLogger, "%s %d AL enumerate devices is empty.",
                __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d AL enumerate devices size: %u.",
                __FUNCTION__, __LINE__, (unsigned int)p->hDevices.size);
        }

        if (mmString_Empty(&p->hDeviceName) ||
            0 == mmRbtsetString_Get(&p->hDevices, &p->hDeviceName))
        {
            // If the suggested device is in the list we use it,
            // otherwise select the default device.
            p->device = alcOpenDevice((const ALCchar*)NULL);

            if (NULL == p->device)
            {
                mmLogger_LogE(gLogger, "%s %d AL device: default open failure.",
                    __FUNCTION__, __LINE__);
                err = MM_FAILURE;
                break;
            }
            else
            {
                mmLogger_LogI(gLogger, "%s %d AL device: default open success.",
                    __FUNCTION__, __LINE__);
            }
        }
        else
        {
            // Use user select device.
            const char* pDeviceName = mmString_CStr(&p->hDeviceName);
            p->device = alcOpenDevice((const ALCchar*)pDeviceName);

            if (NULL == p->device)
            {
                mmLogger_LogE(gLogger, "%s %d AL device: %s open failure.",
                    __FUNCTION__, __LINE__, pDeviceName);
                err = MM_FAILURE;
                break;
            }
            else
            {
                mmLogger_LogI(gLogger, "%s %d AL device: %s open success.",
                    __FUNCTION__, __LINE__, pDeviceName);
            }
        }
        
        err = MM_SUCCESS;
        
    } while (0);
    
    return err;
}

MM_EXPORT_AL
void
mmALDevice_Discard(
    struct mmALDevice*                             p)
{
    if (NULL != p->device)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        alcCloseDevice(p->device);
        // alGetError need context, not need check here.
        // mmALCheck(MM_LOG_ERROR, alGetError());
        p->device = NULL;
        
        mmLogger_LogI(gLogger, "%s %d AL device delete success.",
            __FUNCTION__, __LINE__);
    }
}
