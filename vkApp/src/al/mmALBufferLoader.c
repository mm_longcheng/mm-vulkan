/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALBufferLoader.h"

#include "al/mmALError.h"
#include "al/mmALAssets.h"
#include "al/mmALAssetsLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStreambuf.h"

MM_EXPORT_AL
void
mmALBufferInfo_Init(
    struct mmALBufferInfo*                         p)
{
    mmString_Init(&p->name);
    p->vBuffer = AL_NONE;
    p->pAssets = NULL;
    p->reference = 0;
}

MM_EXPORT_AL
void
mmALBufferInfo_Destroy(
    struct mmALBufferInfo*                         p)
{
    p->reference = 0;
    p->pAssets = NULL;
    p->vBuffer = AL_NONE;
    mmString_Destroy(&p->name);
}

MM_EXPORT_AL
int
mmALBufferInfo_Invalid(
    const struct mmALBufferInfo*                   p)
{
    return
        (NULL == p) ||
        (AL_NONE == p->vBuffer) ||
        (NULL == p->pAssets);
}

MM_EXPORT_AL
void
mmALBufferInfo_Increase(
    struct mmALBufferInfo*                         p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

MM_EXPORT_AL
void
mmALBufferInfo_Decrease(
    struct mmALBufferInfo*                         p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

MM_EXPORT_AL
int
mmALBufferInfo_Prepare(
    struct mmALBufferInfo*                         p,
    struct mmALAssetsLoader*                       pAssetsLoader,
    const char*                                    path)
{
    int err = MM_UNKNOWN;
    
    do
    {
        ALenum ALErr;
        struct mmStreambuf s;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == path)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " path is invalid.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        if (NULL == pAssetsLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssetsLoader is invalid.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        alGenBuffers(1, &p->vBuffer);
        ALErr = alGetError();
        mmALCheck(MM_LOG_ERROR, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " AL alGenBuffers failure.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        p->pAssets = mmALAssetsLoader_Produce(pAssetsLoader, path);
        if (NULL == p->pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmALAssetsLoader_Produce failure.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmStreambuf_Init(&s);
        err = mmALAssets_LoadWholeData(p->pAssets, p->vBuffer, &s);
        mmStreambuf_Destroy(&s);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmALAssets_LoadWholeData failure.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALBufferInfo_Discard(
    struct mmALBufferInfo*                         p,
    struct mmALAssetsLoader*                       pAssetsLoader)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pAssetsLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssetsLoader is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL != p->pAssets)
        {
            mmALAssetsLoader_Recycle(pAssetsLoader, p->pAssets);
            p->pAssets = NULL;
        }

        if (AL_NONE != p->vBuffer)
        {
            alDeleteBuffers(1, &p->vBuffer);
            mmALCheck(MM_LOG_ERROR, alGetError());
            p->vBuffer = AL_NONE;
        }
    } while(0);
}

MM_EXPORT_AL
void
mmALBufferLoader_Init(
    struct mmALBufferLoader*                       p)
{
    mmRbtreeStringVpt_Init(&p->sets);
    p->pAssetsLoader = NULL;
}

MM_EXPORT_AL
void
mmALBufferLoader_Destroy(
    struct mmALBufferLoader*                       p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");
    
    p->pAssetsLoader = NULL;
    mmRbtreeStringVpt_Destroy(&p->sets);
}

MM_EXPORT_AL
void
mmALBufferLoader_SetAssetsLoader(
    struct mmALBufferLoader*                       p,
    struct mmALAssetsLoader*                       pAssetsLoader)
{
    p->pAssetsLoader = pAssetsLoader;
}

MM_EXPORT_AL
int
mmALBufferLoader_PrepareBufferInfo(
    struct mmALBufferLoader*                       p,
    const char*                                    path,
    struct mmALBufferInfo*                         pBufferInfo)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pBufferInfo->reference && "reference not zero.");

        if (NULL == path)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " path is invalid.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = mmALBufferInfo_Prepare(pBufferInfo, p->pAssetsLoader, path);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmALBufferInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        pBufferInfo->reference = 0;
    } while (0);

    return err;
}

MM_EXPORT_AL
void
mmALBufferLoader_DiscardBufferInfo(
    struct mmALBufferLoader*                       p,
    struct mmALBufferInfo*                         pBufferInfo)
{
    assert(0 == pBufferInfo->reference && "reference not zero.");
    mmALBufferInfo_Discard(pBufferInfo, p->pAssetsLoader);
}

MM_EXPORT_AL
void
mmALBufferLoader_UnloadByPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path)
{
    struct mmALBufferInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmALBufferInfo*)it->v;
        mmALBufferLoader_DiscardBufferInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmALBufferInfo_Destroy(s);
        mmFree(s);
    }
}

MM_EXPORT_AL
void
mmALBufferLoader_UnloadComplete(
    struct mmALBufferLoader*                       p)
{
    struct mmALBufferInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmALBufferInfo*)(it->v);
        mmALBufferLoader_DiscardBufferInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmALBufferInfo_Destroy(s);
        mmFree(s);
    }
}

MM_EXPORT_AL
struct mmALBufferInfo*
mmALBufferLoader_LoadFromPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path)
{
    struct mmALBufferInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmALBufferInfo*)mmMalloc(sizeof(struct mmALBufferInfo));
        mmALBufferInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmALBufferLoader_PrepareBufferInfo(p, path, s);
    }
    else
    {
        s = (struct mmALBufferInfo*)(it->v);
    }
    return s;
}

MM_EXPORT_AL
struct mmALBufferInfo*
mmALBufferLoader_GetFromPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path)
{
    struct mmALBufferInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmALBufferInfo*)(it->v);
    }
    return s;
}
