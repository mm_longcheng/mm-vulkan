/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALError.h"

#include "core/mmLogger.h"

MM_EXPORT_AL
void
mmALErrorCheck(
    int                                            lvl,
    ALenum                                         code,
    const char*                                    func,
    int                                            line)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    switch (code)
    {
    case AL_INVALID_NAME:
        mmLogger_Message(gLogger, lvl,
            "%s %d Invalid name paramater passed to AL call.",
            func, line);
        break;
    case AL_INVALID_ENUM:
        mmLogger_Message(gLogger, lvl,
            "%s %d Invalid enum parameter passed to AL call.",
            func, line);
        break;
    case AL_INVALID_VALUE:
        mmLogger_Message(gLogger, lvl,
            "%s %d Invalid value parameter passed to AL call.",
            func, line);
        break;
    case AL_INVALID_OPERATION:
        mmLogger_Message(gLogger, lvl,
            "%s %d Illegal AL call.",
            func, line);
        break;
    case AL_OUT_OF_MEMORY:
        mmLogger_Message(gLogger, lvl,
            "%s %d Not enough memory.",
            func, line);
        break;
    default:
        break;
    }
}
