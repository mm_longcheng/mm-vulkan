/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALBufferLoader_h__
#define __mmALBufferLoader_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"

#include "core/mmPrefix.h"

struct mmALAssets;
struct mmALAssetsLoader;

struct mmALBufferInfo
{
    // name for buffer info, read only.
    // string is weak reference value.
    struct mmString name;

    // OpenAL alBuffer.
    ALuint vBuffer;
    
    // AL assets pointer.
    struct mmALAssets* pAssets;

    // reference count.
    size_t reference;
};

MM_EXPORT_AL
void
mmALBufferInfo_Init(
    struct mmALBufferInfo*                         p);

MM_EXPORT_AL
void
mmALBufferInfo_Destroy(
    struct mmALBufferInfo*                         p);

MM_EXPORT_AL
int
mmALBufferInfo_Invalid(
    const struct mmALBufferInfo*                   p);

MM_EXPORT_AL
void
mmALBufferInfo_Increase(
    struct mmALBufferInfo*                         p);

MM_EXPORT_AL
void
mmALBufferInfo_Decrease(
    struct mmALBufferInfo*                         p);

MM_EXPORT_AL
int
mmALBufferInfo_Prepare(
    struct mmALBufferInfo*                         p,
    struct mmALAssetsLoader*                       pAssetsLoader,
    const char*                                    path);

MM_EXPORT_AL
void
mmALBufferInfo_Discard(
    struct mmALBufferInfo*                         p,
    struct mmALAssetsLoader*                       pAssetsLoader);

struct mmALBufferLoader
{
    // <filename, struct mmALBufferInfo*>
    struct mmRbtreeStringVpt sets;
    struct mmALAssetsLoader* pAssetsLoader;
};

MM_EXPORT_AL
void
mmALBufferLoader_Init(
    struct mmALBufferLoader*                       p);

MM_EXPORT_AL
void
mmALBufferLoader_Destroy(
    struct mmALBufferLoader*                       p);

MM_EXPORT_AL
void
mmALBufferLoader_SetAssetsLoader(
    struct mmALBufferLoader*                       p,
    struct mmALAssetsLoader*                       pAssetsLoader);

MM_EXPORT_AL
int
mmALBufferLoader_PrepareBufferInfo(
    struct mmALBufferLoader*                       p,
    const char*                                    path,
    struct mmALBufferInfo*                         pBufferInfo);

MM_EXPORT_AL
void
mmALBufferLoader_DiscardBufferInfo(
    struct mmALBufferLoader*                       p,
    struct mmALBufferInfo*                         pBufferInfo);

MM_EXPORT_AL
void
mmALBufferLoader_UnloadByPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path);

MM_EXPORT_AL
void
mmALBufferLoader_UnloadComplete(
    struct mmALBufferLoader*                       p);

MM_EXPORT_AL
struct mmALBufferInfo*
mmALBufferLoader_LoadFromPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path);

MM_EXPORT_AL
struct mmALBufferInfo*
mmALBufferLoader_GetFromPath(
    struct mmALBufferLoader*                       p,
    const char*                                    path);

#include "core/mmSuffix.h"

#endif//__mmALBufferLoader_h__
