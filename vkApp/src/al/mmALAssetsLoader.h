/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALAssetsLoader_h__
#define __mmALAssetsLoader_h__

#include "core/mmCore.h"

#include "al/mmALExport.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmALAssetsFactory;

struct mmALAssetsLoader
{
    struct mmFileContext* pFileContext;
    struct mmALAssetsFactory* pFactory;
};

MM_EXPORT_AL
void
mmALAssetsLoader_Init(
    struct mmALAssetsLoader*                       p);

MM_EXPORT_AL
void
mmALAssetsLoader_Destroy(
    struct mmALAssetsLoader*                       p);

MM_EXPORT_AL
void
mmALAssetsLoader_SetFileContext(
    struct mmALAssetsLoader*                       p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
void
mmALAssetsLoader_SetFactory(
    struct mmALAssetsLoader*                       p,
    struct mmALAssetsFactory*                      pFactory);

MM_EXPORT_AL
struct mmALAssets*
mmALAssetsLoader_Produce(
    struct mmALAssetsLoader*                       p,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALAssetsLoader_Recycle(
    struct mmALAssetsLoader*                       p,
    struct mmALAssets*                             pAssets);

#include "core/mmSuffix.h"

#endif//__mmALAssetsLoader_h__
