/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALSourceStatic.h"

#include "al/mmALError.h"
#include "al/mmALAssets.h"
#include "al/mmALBufferLoader.h"

#include "core/mmLogger.h"

const struct mmMetaAllocator mmALAllocatorSourceStatic =
{
    "mmALSourceStatic",
    sizeof(struct mmALSourceStatic),
    &mmALSourceStatic_Produce,
    &mmALSourceStatic_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataSource mmALMetadataSourceStatic =
{
    &mmALAllocatorSourceStatic,
    /*- Interface -*/
    &mmALSourceStatic_Prepare,
    &mmALSourceStatic_Discard,
    &mmALSourceStatic_GetIsStream,
    &mmALSourceStatic_GetAssets,
    &mmALSourceStatic_SetLoop,
    &mmALSourceStatic_RewindImmediately,
    &mmALSourceStatic_SetPlayPosition,
    &mmALSourceStatic_GetPlayPosition,
    &mmALSourceStatic_UpdatePlayPosition,
    &mmALSourceStatic_Update,
};

MM_EXPORT_AL
void
mmALSourceStatic_Init(
    struct mmALSourceStatic*                       p)
{
    mmALSource_Init(&p->hSuper);
    p->hLastBytes = 0;
    p->pBufferInfo = NULL;
}

MM_EXPORT_AL
void
mmALSourceStatic_Destroy(
    struct mmALSourceStatic*                       p)
{
    p->pBufferInfo = NULL;
    p->hLastBytes = 0;
    mmALSource_Destroy(&p->hSuper);
}

MM_EXPORT_AL
void
mmALSourceStatic_Produce(
    struct mmALSourceStatic*                       p)
{
    mmALSourceStatic_Init(p);
    p->hSuper.pMetadata = &mmALMetadataSourceStatic;
}

MM_EXPORT_AL
void
mmALSourceStatic_Recycle(
    struct mmALSourceStatic*                       p)
{
    p->hSuper.pMetadata = &mmALMetadataSourceStatic;
    mmALSourceStatic_Destroy(p);
}

MM_EXPORT_AL
int
mmALSourceStatic_Prepare(
    struct mmALSourceStatic*                       p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pBufferLoader)
        {
            mmLogger_LogE(gLogger, "%s %d pBufferLoader is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        if (NULL == pFileName)
        {
            mmLogger_LogE(gLogger, "%s %d pFileName is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        p->pBufferInfo = mmALBufferLoader_GetFromPath(pBufferLoader, pFileName);
        if (mmALBufferInfo_Invalid(p->pBufferInfo))
        {
            mmLogger_LogE(gLogger, "%s %d pBufferInfo is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        mmALBufferInfo_Increase(p->pBufferInfo);
        
        err = mmALSource_PrepareBase(&p->hSuper);
        if (MM_BUSY == err)
        {
            // driver busy not a error.
            break;
        }
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALSource_Prepare failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmALSourceStatic_Enqueuebuffers(p);
        
        // Notify listener
        (*(pCallback->OnLoaded))(pCallback->obj, &p->hSuper);
    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALSourceStatic_Discard(
    struct mmALSourceStatic*                       p,
    struct mmALBufferLoader*                       pBufferLoader)
{
    struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
    
    // Notify listener
    (*(pCallback->OnDestroyed))(pCallback->obj, &p->hSuper);
    
    mmALSourceStatic_UnqueueBuffers(p);
    
    mmALSource_DiscardBase(&p->hSuper);
    
    mmALBufferInfo_Decrease(p->pBufferInfo);
    p->pBufferInfo = NULL;
}

MM_EXPORT_AL
int
mmALSourceStatic_GetIsStream(
    const struct mmALSourceStatic*                 p)
{
    return MM_FALSE;
}

MM_EXPORT_AL
struct mmALAssets*
mmALSourceStatic_GetAssets(
    const struct mmALSourceStatic*                 p)
{
    if (NULL != p->pBufferInfo)
    {
        return p->pBufferInfo->pAssets;
    }
    else
    {
        return NULL;
    }
}

MM_EXPORT_AL
void
mmALSourceStatic_SetLoop(
    struct mmALSourceStatic*                       p,
    int                                            hLoop)
{
    if (AL_NONE != p->hSuper.hSource)
    {
        alSourcei(p->hSuper.hSource, AL_LOOPING, hLoop);
        mmALCheck(MM_LOG_ERROR, alGetError());
        p->hSuper.hLoop = hLoop;
    }
    else
    {
        p->hSuper.hLoop = hLoop;
    }
}

MM_EXPORT_AL
void
mmALSourceStatic_RewindImmediately(
    struct mmALSourceStatic*                       p)
{
    do
    {
        ALenum ALErr;
        ALfloat hSourcePosition;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }
        
        hSourcePosition = (ALfloat)0.0f;
        alSourcef(p->hSuper.hSource, AL_SEC_OFFSET, hSourcePosition);
        ALErr = alGetError();
        mmALCheck(MM_LOG_ERROR, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d AL set source offset failure.",
                __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
}

MM_EXPORT_AL
void
mmALSourceStatic_SetPlayPosition(
    struct mmALSourceStatic*                       p,
    double                                         hPosition)
{
    do
    {
        double hSeconds;
        struct mmALAssets* pAssets;
        struct mmALAssetsInfo* pAssetsInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (mmALBufferInfo_Invalid(p->pBufferInfo))
        {
            mmLogger_LogE(gLogger, "%s %d pBufferInfo is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        pAssets = p->pBufferInfo->pAssets;
        pAssetsInfo = &pAssets->hAssetsInfo;

        if (!pAssetsInfo->Seekable)
        {
            break;
        }
        
        // Invalid time - exit
        if (0.0 >= pAssetsInfo->TotalTime)
        {
            break;
        }

        if (0.0 > hPosition)
        {
            break;
        }
        
        // time wrap.
        hSeconds = fmod(hPosition, pAssetsInfo->TotalTime);
        
        // Set position
        p->hSuper.hPlayPosition = (double)hSeconds;

        // Set flag
        p->hSuper.hPlayPositionChanged = MM_TRUE;
    } while(0);
}

MM_EXPORT_AL
double
mmALSourceStatic_GetPlayPosition(
    const struct mmALSourceStatic*                 p)
{
    double hSeconds = -1.0;

    do
    {
        double hPosition;
        ALfloat hSourcePosition;
        struct mmALAssets* pAssets;
        struct mmALAssetsInfo* pAssetsInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmALBufferInfo_Invalid(p->pBufferInfo))
        {
            mmLogger_LogE(gLogger, "%s %d pBufferInfo is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        pAssets = p->pBufferInfo->pAssets;
        pAssetsInfo = &pAssets->hAssetsInfo;
        
        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }

        hSourcePosition = (ALfloat)0.0f;
        alGetSourcef(p->hSuper.hSource, AL_SEC_OFFSET, &hSourcePosition);
        mmALCheck(MM_LOG_ERROR, alGetError());

        hPosition = hSourcePosition;
        // Wrap if necessary
        if (hPosition >= pAssetsInfo->TotalTime)
        {
            hSeconds = hPosition - pAssetsInfo->TotalTime;
        }
        else
        {
            hSeconds = hPosition;
        }
    } while(0);

    return hSeconds;
}

MM_EXPORT_AL
void
mmALSourceStatic_UpdatePlayPosition(
    struct mmALSourceStatic*                       p)
{
    do
    {
        ALenum ALErr;
        ALfloat hSourcePosition;
        struct mmALAssets* pAssets;
        struct mmALAssetsInfo* pAssetsInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmALBufferInfo_Invalid(p->pBufferInfo))
        {
            mmLogger_LogE(gLogger, "%s %d pBufferInfo is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        pAssets = p->pBufferInfo->pAssets;
        pAssetsInfo = &pAssets->hAssetsInfo;
        
        if (!pAssetsInfo->Seekable)
        {
            break;
        }

        if (AL_NONE == p->hSuper.hSource)
        {
            break;
        }
        
        hSourcePosition = (ALfloat)p->hSuper.hPlayPosition;
        alSourcef(p->hSuper.hSource, AL_SEC_OFFSET, hSourcePosition);
        ALErr = alGetError();
        mmALCheck(MM_LOG_ERROR, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d AL set source offset failure.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        // Set flag
        p->hSuper.hPlayPositionChanged = MM_FALSE;
    } while(0);
}

MM_EXPORT_AL
void
mmALSourceStatic_Update(
    struct mmALSourceStatic*                       p,
    double                                         interval)
{
    ALenum state;

    if (!mmALSource_GetIsPlaying(&p->hSuper))
    {
        return;
    }

    alGetSourcei(p->hSuper.hSource, AL_SOURCE_STATE, &state);
    mmALCheck(MM_LOG_ERROR, alGetError());

    if (state == AL_PAUSED)
    {
        return;
    }

    // Ran out of buffer data?
    if (state == AL_STOPPED)
    {
        struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
        struct mmALSourceFinished* pFinished = &p->hSuper.hFinished;
        
        mmALSource_Stop(&p->hSuper);

        // Finished callback
        // Notify listener
        (*(pCallback->OnFinished))(pCallback->obj, &p->hSuper);
        
        // Finished handler listener.
        (*(pFinished->OnFinished))(pFinished->obj, &p->hSuper);
        
        // return immediately.
        return;
    }

    // handle play position change
    if (p->hSuper.hPlayPositionChanged)
    {
        mmALSourceStatic_UpdatePlayPosition(p);
    }
    else
    {
        mmALSourceStatic_UpdateTrackData(p);
    }
}

MM_EXPORT_AL
void
mmALSourceStatic_UpdateTrackData(
    struct mmALSourceStatic*                       p)
{
    ALint bytes = 0;
    
    struct mmALSourceCallback* pCallback = &p->hSuper.hCallback;
    
    // Use byte offset to work out current position
    alGetSourcei(p->hSuper.hSource, AL_BYTE_OFFSET, &bytes);
    mmALCheck(MM_LOG_ERROR, alGetError());

    // Has the audio looped?
    if (p->hLastBytes > bytes)
    {
        // Store current offset position
        p->hLastBytes = bytes;
        
        // Notify listener
        (*(pCallback->OnLooping))(pCallback->obj, &p->hSuper);
    }
    else
    {
        // Store current offset position
        p->hLastBytes = bytes;
    }
}

MM_EXPORT_AL
void
mmALSourceStatic_Enqueuebuffers(
    struct mmALSourceStatic*                       p)
{
    if (AL_NONE != p->hSuper.hSource &&
        !mmALBufferInfo_Invalid(p->pBufferInfo))
    {
        alSourcei(p->hSuper.hSource, AL_BUFFER, p->pBufferInfo->vBuffer);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
void
mmALSourceStatic_UnqueueBuffers(
    struct mmALSourceStatic*                       p)
{
    if (AL_NONE != p->hSuper.hSource)
    {
        alSourcei(p->hSuper.hSource, AL_BUFFER, AL_NONE);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}
