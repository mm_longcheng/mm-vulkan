/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALAssetsMp3_h__
#define __mmALAssetsMp3_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStream.h"

#include "al/mmALAssets.h"

#include "mp3/minimp3.h"
#include "mp3/minimp3_ex.h"

#include "core/mmPrefix.h"

struct mmFileContext;

MM_EXPORT_AL
int
mmALAssetsMp3QueryInfo(
    mp3dec_ex_t*                                   pMp3decEx,
    struct mmALAssetsInfo*                         pAssetsInfo);

MM_EXPORT_AL extern const struct mmALMetadataAssets mmALMetadataAssetsMp3;

struct mmALAssetsMp3
{
    struct mmALAssets Assets;
    struct mmStream Stream;
    mp3dec_ex_t Mp3decEx;
    mp3dec_io_t Mp3IO;
};

MM_EXPORT_AL
void
mmALAssetsMp3_Init(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
void
mmALAssetsMp3_Destroy(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
void
mmALAssetsMp3_Produce(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
void
mmALAssetsMp3_Recycle(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
int
mmALAssetsMp3_Prepare(
    struct mmALAssetsMp3*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALAssetsMp3_Discard(
    struct mmALAssetsMp3*                          p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
double
mmALAssetsMp3_GetTotalTime(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
int64_t
mmALAssetsMp3_GetTotalFrame(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
int
mmALAssetsMp3_GetIsSeekable(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
size_t
mmALAssetsMp3_Read(
    struct mmALAssetsMp3*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght);

MM_EXPORT_AL
int
mmALAssetsMp3_SeekTime(
    struct mmALAssetsMp3*                          p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALAssetsMp3_TellTime(
    struct mmALAssetsMp3*                          p);

MM_EXPORT_AL
int
mmALAssetsMp3_SeekFrame(
    struct mmALAssetsMp3*                          p,
    int64_t                                        hPosition);

MM_EXPORT_AL
int64_t
mmALAssetsMp3_TellFrame(
    struct mmALAssetsMp3*                          p);

#include "core/mmSuffix.h"

#endif//__mmALAssetsMp3_h__
