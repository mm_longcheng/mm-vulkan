/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALAssets_h__
#define __mmALAssets_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmMetadata.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmStreambuf;

// BitsPerSec = SampleRate * Channels * BitsPerSample;
// TotalTime = ((ByteLength) * 8.0 ) / (double)(BitsPerSec);
// TotalFrame = (ByteLength * 8) / (Channels * BitsPerSample)
// BytesPerFrame = BitsPerSample / 8;
// Bytes250ms = SampleRate * Channels * BitsPerSample / 4;
struct mmALAssetsInfo
{
    ALenum Format;                 // OpenAL format
    mmUInt64_t Bytes250ms;         // 250ms byte count.
    mmUInt32_t BitsPerFrame;       // bit count per frame.
    mmUInt64_t BitsPerSec;         // bit count per second.
    
    mmUInt32_t Channels;           // 1, 2 for stereo data is (l,r) pairs
    mmUInt64_t SampleRate;         // SamplesPerSec
    mmUInt32_t BitsPerSample;      // bit count per sample.
    
    mmUInt64_t TotalFrame;         // total frame count.
    double TotalTime;              // total play time.
    size_t ByteLength;             // total byte count.
    int Seekable;                  // seekable.
};

MM_EXPORT_AL
void
mmALAssetsInfo_Reset(
    struct mmALAssetsInfo*                         p);

MM_EXPORT_AL
int
mmALAssets16BitsPickFormat(
    struct mmALAssetsInfo*                         pAssetsInfo);

struct mmALMetadataAssets
{
    // type allocator.
    const struct mmMetaAllocator* Allocator;
    
    // int
    // (*Prepare)(
    //     struct mmALAssets*                             p,
    //     struct mmFileContext*                          pFileContext,
    //     const char*                                    pFileName);
    void* Prepare;

    // void
    // (*Discard)(
    //     struct mmALAssets*                             p,
    //     struct mmFileContext*                          pFileContext);
    void* Discard;

    // double
    // (*GetTotalTime)(
    //     struct mmALAssets*                             p);
    void* GetTotalTime;

    // int64_t
    // (*GetTotalFrame)(
    //     struct mmALAssets*                             p);
    void* GetTotalFrame;

    // int
    // (*GetIsSeekable)(
    //     struct mmALAssets*                             p);
    void* GetIsSeekable;

    // size_t
    // (*Read)(
    //     struct mmALAssets*                             p,
    //     void*                                          pBuffer,
    //     size_t                                         hLenght);
    void* Read;

    // int
    // (*SeekTime)(
    //     struct mmALAssets*                             p,
    //     double                                         hPosition);
    void* SeekTime;

    // double
    // (*TellTime)(
    //     struct mmALAssets*                             p);
    void* TellTime;

    // int
    // (*SeekFrame)(
    //     struct mmALAssets*                             p,
    //     int64_t                                        hPosition);
    void* SeekFrame;

    // int64_t
    // (*TellFrame)(
    //     struct mmALAssets*                             p);
    void* TellFrame;
};

MM_EXPORT_AL extern const struct mmALMetadataAssets mmALMetadataAssetsNone;

struct mmALAssets
{
    struct mmString hFileName;
    struct mmALAssetsInfo hAssetsInfo;
    const struct mmALMetadataAssets* pMetadata;
};

MM_EXPORT_AL
void
mmALAssets_Init(
    struct mmALAssets*                             p);

MM_EXPORT_AL
void
mmALAssets_Destroy(
    struct mmALAssets*                             p);

MM_EXPORT_AL
void
mmALAssets_Produce(
    struct mmALAssets*                             p);

MM_EXPORT_AL
void
mmALAssets_Recycle(
    struct mmALAssets*                             p);

MM_EXPORT_AL
int
mmALAssets_Prepare(
    struct mmALAssets*                             p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALAssets_Discard(
    struct mmALAssets*                             p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
double
mmALAssets_GetTotalTime(
    struct mmALAssets*                             p);

MM_EXPORT_AL
int64_t
mmALAssets_GetTotalFrame(
    struct mmALAssets*                             p);

MM_EXPORT_AL
int
mmALAssets_GetIsSeekable(
    struct mmALAssets*                             p);

MM_EXPORT_AL
size_t
mmALAssets_Read(
    struct mmALAssets*                             p,
    void*                                          pBuffer,
    size_t                                         hLenght);

MM_EXPORT_AL
int
mmALAssets_SeekTime(
    struct mmALAssets*                             p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALAssets_TellTime(
    struct mmALAssets*                             p);

MM_EXPORT_AL
int
mmALAssets_SeekFrame(
    struct mmALAssets*                             p,
    int64_t                                        hPosition);

MM_EXPORT_AL
int64_t
mmALAssets_TellFrame(
    struct mmALAssets*                             p);

/*
 * utils API.
 */

MM_EXPORT_AL
void
mmALAssets_ReadWholeData(
    struct mmALAssets*                             p,
    struct mmStreambuf*                            s);

MM_EXPORT_AL
void
mmALAssets_ReadBlockData(
    struct mmALAssets*                             p,
    struct mmStreambuf*                            s,
    size_t                                         hBytesBlock,
    int                                            hLoop);

MM_EXPORT_AL
int
mmALAssets_LoadWholeData(
    struct mmALAssets*                             p,
    ALuint                                         hBuffer,
    struct mmStreambuf*                            s);

MM_EXPORT_AL
int
mmALAssets_Load250msData(
    struct mmALAssets*                             p,
    ALuint                                         hBuffer,
    int                                            hLoop,
    struct mmStreambuf*                            s);

#include "core/mmSuffix.h"

#endif//__mmALAssets_h__
