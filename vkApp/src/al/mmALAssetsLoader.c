/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssetsLoader.h"

#include "al/mmALAssets.h"
#include "al/mmALAssetsFactory.h"

#include "core/mmLogger.h"
#include "core/mmFilePath.h"
#include "core/mmStringUtils.h"

#include "dish/mmFileContext.h"

MM_EXPORT_AL
void
mmALAssetsLoader_Init(
    struct mmALAssetsLoader*                       p)
{
    p->pFileContext = NULL;
    p->pFactory = NULL;
}

MM_EXPORT_AL
void
mmALAssetsLoader_Destroy(
    struct mmALAssetsLoader*                       p)
{
    p->pFactory = NULL;
    p->pFileContext = NULL;
}

MM_EXPORT_AL
void
mmALAssetsLoader_SetFileContext(
    struct mmALAssetsLoader*                       p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

MM_EXPORT_AL
void
mmALAssetsLoader_SetFactory(
    struct mmALAssetsLoader*                       p,
    struct mmALAssetsFactory*                      pFactory)
{
    p->pFactory = pFactory;
}

MM_EXPORT_AL
struct mmALAssets*
mmALAssetsLoader_Produce(
    struct mmALAssetsLoader*                       p,
    const char*                                    pFileName)
{
    struct mmALAssets* pAssets = NULL;
    
    do
    {
        int err = MM_UNKNOWN;
        
        char lower[64] = { 0 };
        char type[64] = { 0 };
        const char* extension = "";
        size_t len = 0;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        extension = mmPathFileExtension(pFileName);
        len = strlen(extension);
        len = mmCStringTolower(extension, len, lower, 63);
        
        lower[0] = (char)mmToupper(lower[0]);
        sprintf(type, "mmALAssets%s", lower);
        
        pAssets = mmALAssetsFactory_Produce(p->pFactory, type);
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmALAssetsFactory_Produce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmALAssets_Prepare(pAssets, p->pFileContext, pFileName);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmALAssets_PrepareData failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
    
    return pAssets;
}

MM_EXPORT_AL
void
mmALAssetsLoader_Recycle(
    struct mmALAssetsLoader*                       p,
    struct mmALAssets*                             pAssets)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pAssets)
        {
            break;
        }
        
        if (NULL == p->pFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        mmALAssets_Discard(pAssets, p->pFileContext);
        mmALAssetsFactory_Recycle(p->pFactory, pAssets);
    } while (0);
}
