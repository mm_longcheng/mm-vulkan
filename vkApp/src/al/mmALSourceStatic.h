/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALSourceStatic_h__
#define __mmALSourceStatic_h__

#include "core/mmCore.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"
#include "al/mmALSource.h"

#include "core/mmPrefix.h"

struct mmALBufferLoader;
struct mmALBufferInfo;

MM_EXPORT_AL extern const struct mmALMetadataSource mmALMetadataSourceStatic;

struct mmALSourceStatic
{
    // super type.
    struct mmALSource hSuper;
    
    // Last buffer player byte index.
    ALint hLastBytes;
    
    // Buffer info.
    struct mmALBufferInfo* pBufferInfo;
};

MM_EXPORT_AL
void
mmALSourceStatic_Init(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_Destroy(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_Produce(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_Recycle(
    struct mmALSourceStatic*                       p);

/* interface */

MM_EXPORT_AL
int
mmALSourceStatic_Prepare(
    struct mmALSourceStatic*                       p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALSourceStatic_Discard(
    struct mmALSourceStatic*                       p,
    struct mmALBufferLoader*                       pBufferLoader);

MM_EXPORT_AL
int
mmALSourceStatic_GetIsStream(
    const struct mmALSourceStatic*                 p);

MM_EXPORT_AL
struct mmALAssets*
mmALSourceStatic_GetAssets(
    const struct mmALSourceStatic*                 p);

MM_EXPORT_AL
void
mmALSourceStatic_SetLoop(
    struct mmALSourceStatic*                       p,
    int                                            hLoop);

MM_EXPORT_AL
void
mmALSourceStatic_RewindImmediately(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_SetPlayPosition(
    struct mmALSourceStatic*                       p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALSourceStatic_GetPlayPosition(
    const struct mmALSourceStatic*                 p);

MM_EXPORT_AL
void
mmALSourceStatic_UpdatePlayPosition(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_Update(
    struct mmALSourceStatic*                       p,
    double                                         interval);

/* special */

MM_EXPORT_AL
void
mmALSourceStatic_UpdateTrackData(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_Enqueuebuffers(
    struct mmALSourceStatic*                       p);

MM_EXPORT_AL
void
mmALSourceStatic_UnqueueBuffers(
    struct mmALSourceStatic*                       p);

#include "core/mmSuffix.h"

#endif//__mmALSourceStatic_h__
