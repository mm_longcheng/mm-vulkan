/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALListener_h__
#define __mmALListener_h__

#include "core/mmCore.h"

#include "al/mmALPrereqs.h"
#include "al/mmALExport.h"

#include "core/mmPrefix.h"

struct mmALListener
{
    // 3D volume
    float hVolume;
    
    // 3D position
    float hPosition[3];
    
    // 3D velocity
    float hVelocity[3];
    
    // 3D orientation as Quaternion
    float hQuaternion[4];
    
    // 3D OpenAL orientation
    float mOrientation[6];
};

MM_EXPORT_AL
void
mmALListener_Init(
    struct mmALListener*                           p);

MM_EXPORT_AL
void
mmALListener_Destroy(
    struct mmALListener*                           p);

MM_EXPORT_AL
void
mmALListener_SetPosition(
    struct mmALListener*                           p,
    const float                                    position[3]);

MM_EXPORT_AL
void
mmALListener_GetPosition(
    const struct mmALListener*                     p,
    float                                          position[3]);

MM_EXPORT_AL
void
mmALListener_SetVelocity(
    struct mmALListener*                           p,
    const float                                    velocity[3]);

MM_EXPORT_AL
void
mmALListener_GetVelocity(
    const struct mmALListener*                     p,
    float                                          velocity[3]);

MM_EXPORT_AL
void
mmALListener_SetQuaternion(
    struct mmALListener*                           p,
    const float                                    quaternion[4]);

MM_EXPORT_AL
void
mmALListener_GetQuaternion(
    const struct mmALListener*                     p,
    float                                          quaternion[4]);

MM_EXPORT_AL
void
mmALListener_SetVolume(
    struct mmALListener*                           p,
    float                                          volume);

MM_EXPORT_AL
float
mmALListener_GetVolume(
    const struct mmALListener*                     p);

MM_EXPORT_AL
void
mmALListener_EnterBackground(
    struct mmALListener*                           p);

MM_EXPORT_AL
void
mmALListener_EnterForeground(
    struct mmALListener*                           p);

#include "core/mmSuffix.h"

#endif//__mmALListener_h__
