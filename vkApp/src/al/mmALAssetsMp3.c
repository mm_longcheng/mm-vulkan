/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssetsMp3.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByteCoder.h"

#include "dish/mmFileContext.h"

MM_EXPORT_AL
int
mmALAssetsMp3QueryInfo(
    mp3dec_ex_t*                                   pMp3decEx,
    struct mmALAssetsInfo*                         pAssetsInfo)
{
    int err = MM_UNKNOWN;
    
    do
    {
        mp3dec_frame_info_t* pFrameInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pMp3decEx)
        {
            mmLogger_LogE(gLogger, "mmALAssets MP3 pMp3decEx is invalid!");
            err = MM_FAILURE;
            break;
        }
        
        pFrameInfo = &pMp3decEx->info;
        
        pAssetsInfo->Channels = pFrameInfo->channels;
        pAssetsInfo->SampleRate = pFrameInfo->hz;
        pAssetsInfo->BitsPerSample = 16;
        
        err = mmALAssets16BitsPickFormat(pAssetsInfo);
        if (MM_FAILURE == err)
        {
            mmLogger_LogE(gLogger, "mmALAssets MP3 format not support.");
            break;
        }
        
        pAssetsInfo->Channels = pFrameInfo->channels;
        pAssetsInfo->SampleRate = pFrameInfo->hz;
        pAssetsInfo->BitsPerSample = 16;
        
        pAssetsInfo->BitsPerFrame = pAssetsInfo->Channels * pAssetsInfo->BitsPerSample;
        pAssetsInfo->BitsPerSec = pAssetsInfo->SampleRate * pAssetsInfo->BitsPerFrame;
        
        // minimp3 samples not frames, need divide by Channels.
        pAssetsInfo->TotalFrame = (mmUInt64_t)(pMp3decEx->samples / pAssetsInfo->Channels);
        pAssetsInfo->ByteLength = pAssetsInfo->TotalFrame * pAssetsInfo->BitsPerFrame / 8;
        pAssetsInfo->TotalTime = (pAssetsInfo->ByteLength * 8.0) / (double)(pAssetsInfo->BitsPerSec);
        pAssetsInfo->Seekable = MM_TRUE;
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

static
size_t
mmALAssetsMp3Read(
    void*                                          ptr,
    size_t                                         size,
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return mmStreamRead(ptr, 1, size, s);
}

static
int
mmALAssetsMp3Seek(
    uint64_t                                       position,
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return mmStreamSeeko64(s, (int64_t)position, SEEK_SET);
}

const struct mmMetaAllocator mmALAllocatorAssetsMp3 =
{
    "mmALAssetsMp3",
    sizeof(struct mmALAssetsMp3),
    &mmALAssetsMp3_Produce,
    &mmALAssetsMp3_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataAssets mmALMetadataAssetsMp3 =
{
    &mmALAllocatorAssetsMp3,
    /*- Interface -*/
    &mmALAssetsMp3_Prepare,
    &mmALAssetsMp3_Discard,
    &mmALAssetsMp3_GetTotalTime,
    &mmALAssetsMp3_GetTotalFrame,
    &mmALAssetsMp3_GetIsSeekable,
    &mmALAssetsMp3_Read,
    &mmALAssetsMp3_SeekTime,
    &mmALAssetsMp3_TellTime,
    &mmALAssetsMp3_SeekFrame,
    &mmALAssetsMp3_TellFrame,
};

MM_EXPORT_AL
void
mmALAssetsMp3_Init(
    struct mmALAssetsMp3*                          p)
{
    mp3dec_io_t io =
    {
        mmALAssetsMp3Read,
        &p->Stream,
        mmALAssetsMp3Seek,
        &p->Stream,
    };
    
    mmALAssets_Init(&p->Assets);
    mmStream_Init(&p->Stream);
    mmMemset(&p->Mp3decEx, 0, sizeof(mp3dec_ex_t));
    
    p->Mp3IO = io;
}

MM_EXPORT_AL
void
mmALAssetsMp3_Destroy(
    struct mmALAssetsMp3*                          p)
{
    mmMemset(&p->Mp3decEx, 0, sizeof(mp3dec_ex_t));
    mmStream_Destroy(&p->Stream);
    mmALAssets_Destroy(&p->Assets);
}

MM_EXPORT_AL
void
mmALAssetsMp3_Produce(
    struct mmALAssetsMp3*                          p)
{
    mmALAssetsMp3_Init(p);
    
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsMp3;
}

MM_EXPORT_AL
void
mmALAssetsMp3_Recycle(
    struct mmALAssetsMp3*                          p)
{
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsMp3;
    
    mmALAssetsMp3_Destroy(p);
}

MM_EXPORT_AL
int
mmALAssetsMp3_Prepare(
    struct mmALAssetsMp3*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d pFileContext is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmFileContext_AcquireStream(pFileContext, pFileName, "rb", &p->Stream);
        if (mmStream_Invalid(&p->Stream))
        {
            mmLogger_LogE(gLogger, "%s %d mmFileContext_AcquireStream failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        if (mp3dec_ex_open_cb(&p->Mp3decEx, &p->Mp3IO, MP3D_SEEK_TO_SAMPLE))
        {
            mmLogger_LogE(gLogger, "%s %d mp3dec_ex_open_cb failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmALAssetsMp3QueryInfo(&p->Mp3decEx, &p->Assets.hAssetsInfo);
        if (0 != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALAssetsMp3QueryInfo failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALAssetsMp3_Discard(
    struct mmALAssetsMp3*                          p,
    struct mmFileContext*                          pFileContext)
{
    // Mp3decEx = { 0 } can safety mp3dec_ex_close.
    mp3dec_ex_close(&p->Mp3decEx);
    
    mmFileContext_ReleaseStream(pFileContext, &p->Stream);
}

MM_EXPORT_AL
double
mmALAssetsMp3_GetTotalTime(
    struct mmALAssetsMp3*                          p)
{
    return (double)p->Assets.hAssetsInfo.TotalTime;
}

MM_EXPORT_AL
int64_t
mmALAssetsMp3_GetTotalFrame(
    struct mmALAssetsMp3*                          p)
{
    return (int64_t)p->Assets.hAssetsInfo.TotalFrame;
}

MM_EXPORT_AL
int
mmALAssetsMp3_GetIsSeekable(
    struct mmALAssetsMp3*                          p)
{
    return MM_TRUE;
}

MM_EXPORT_AL
size_t
mmALAssetsMp3_Read(
    struct mmALAssetsMp3*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght)
{
    struct mmALAssetsInfo* pAssetsInfo = &p->Assets.hAssetsInfo;
    size_t samples = (hLenght * 8) / (pAssetsInfo->BitsPerSample);
    size_t readed = mp3dec_ex_read(&p->Mp3decEx, pBuffer, samples);
    return readed * (pAssetsInfo->BitsPerSample / 8);
}

MM_EXPORT_AL
int
mmALAssetsMp3_SeekTime(
    struct mmALAssetsMp3*                          p,
    double                                         hPosition)
{
    struct mmALAssetsInfo* pAssetsInfo = &p->Assets.hAssetsInfo;
    size_t BitsLength = (size_t)(hPosition * (double)pAssetsInfo->BitsPerSec);
    size_t samples = BitsLength / pAssetsInfo->BitsPerSample;
    return mp3dec_ex_seek(&p->Mp3decEx, (uint64_t)samples);
}

MM_EXPORT_AL
double
mmALAssetsMp3_TellTime(
    struct mmALAssetsMp3*                          p)
{
    struct mmALAssetsInfo* pAssetsInfo = &p->Assets.hAssetsInfo;
    size_t samples = p->Mp3decEx.cur_sample;
    size_t BitsLength = samples * pAssetsInfo->BitsPerSample;
    return (double)(BitsLength / pAssetsInfo->BitsPerSec);
}

MM_EXPORT_AL
int
mmALAssetsMp3_SeekFrame(
    struct mmALAssetsMp3*                          p,
    int64_t                                        hPosition)
{
    struct mmALAssetsInfo* pAssetsInfo = &p->Assets.hAssetsInfo;
    size_t samples = hPosition * pAssetsInfo->Channels;
    return mp3dec_ex_seek(&p->Mp3decEx, (uint64_t)samples);
}

MM_EXPORT_AL
int64_t
mmALAssetsMp3_TellFrame(
    struct mmALAssetsMp3*                          p)
{
    struct mmALAssetsInfo* pAssetsInfo = &p->Assets.hAssetsInfo;
    size_t samples = p->Mp3decEx.cur_sample;
    return (int64_t)(samples / pAssetsInfo->Channels);
}
