/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALManager.h"

#include "al/mmALError.h"
#include "al/mmALAssetsTypes.h"
#include "al/mmALSourceTypes.h"

#include "core/mmLogger.h"

MM_EXPORT_AL
void
mmALManagerEffectOnFinished(
    void*                                          obj,
    struct mmALSource*                             s)
{
    struct mmALManager* p = (struct mmALManager*)(obj);
    // Do not trigger stop repeat.
    // mmALSource_Stop(s);
    mmALManager_RecycleSource(p, s);
    mmRbtsetVpt_Rmv(&p->hEffects, s);
}

MM_EXPORT_AL
void
mmALManager_Init(
    struct mmALManager*                            p)
{
    mmALDevice_Init(&p->hDevice);
    mmALContext_Init(&p->hContext);
    mmALListener_Init(&p->hListener);
    mmALAssetsFactory_Init(&p->hAssetsFactory);
    mmALSourceFactory_Init(&p->hSourceFactory);
    mmALAssetsLoader_Init(&p->hAssetsLoader);
    mmALBufferLoader_Init(&p->hBufferLoader);
    mmRbtsetVpt_Init(&p->hSources);
    mmRbtsetVpt_Init(&p->hEffects);
    
    mmALAssetsLoader_SetFactory(&p->hAssetsLoader, &p->hAssetsFactory);
    mmALBufferLoader_SetAssetsLoader(&p->hBufferLoader, &p->hAssetsLoader);
}

MM_EXPORT_AL
void
mmALManager_Destroy(
    struct mmALManager*                            p)
{
    assert(0 == p->hSources.size && "assets is not destroy complete.");
    
    mmRbtsetVpt_Destroy(&p->hEffects);
    mmRbtsetVpt_Destroy(&p->hSources);
    mmALBufferLoader_Destroy(&p->hBufferLoader);
    mmALAssetsLoader_Destroy(&p->hAssetsLoader);
    mmALSourceFactory_Destroy(&p->hSourceFactory);
    mmALAssetsFactory_Destroy(&p->hAssetsFactory);
    mmALListener_Destroy(&p->hListener);
    mmALContext_Destroy(&p->hContext);
    mmALDevice_Destroy(&p->hDevice);
}

MM_EXPORT_AL
void
mmALManager_SetFileContext(
    struct mmALManager*                            p,
    struct mmFileContext*                          pFileContext)
{
    mmALAssetsLoader_SetFileContext(&p->hAssetsLoader, pFileContext);
}

MM_EXPORT_AL
void
mmALManager_SetDeviceName(
    struct mmALManager*                            p,
    const char*                                    pDeviceName)
{
    mmALDevice_SetDeviceName(&p->hDevice, pDeviceName);
}

MM_EXPORT_AL
void
mmALManager_SetMasterVolume(
    struct mmALManager*                            p,
    float                                          volume)
{
    mmALListener_SetVolume(&p->hListener, volume);
}

MM_EXPORT_AL
float
mmALManager_GetMasterVolume(
    const struct mmALManager*                      p)
{
    return mmALListener_GetVolume(&p->hListener);
}

MM_EXPORT_AL
void
mmALManager_EnumerateDevices(
    struct mmALManager*                            p)
{
    mmALDevice_EnumerateDevices(&p->hDevice);
}

MM_EXPORT_AL
int
mmALManager_Prepare(
    struct mmALManager*                            p)
{
    int err = MM_UNKNOWN;
    
    do
    {
        ALenum ALErr;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        err = mmALDevice_Prepare(&p->hDevice);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d AL mmALDevice_Prepare failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmALContext_Prepare(&p->hContext, p->hDevice.device);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d AL mmALContext_Prepare failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        // alcMakeContextCurrent is used when cross thread.
        // The code at ios may be AL_INVALID_OPERATION Illegal AL call.
        // It needs to be called once, otherwise there may be no sound.
        alcMakeContextCurrent(p->hContext.context);
        ALErr = alGetError();
        mmALCheck(MM_LOG_TRACE, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d AL alcMakeContextCurrent failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmALAssetsFactoryAddTypes(&p->hAssetsFactory);
        mmALSourceFactoryAddTypes(&p->hSourceFactory);
        
        err = MM_SUCCESS;
        
    } while (0);
    
    return err;
}

MM_EXPORT_AL
void
mmALManager_Discard(
    struct mmALManager*                            p)
{
    mmALManager_StopEffectComplete(p);

    mmALSourceFactoryRmvTypes(&p->hSourceFactory);
    mmALAssetsFactoryRmvTypes(&p->hAssetsFactory);
    
    mmALContext_Discard(&p->hContext);
    mmALDevice_Destroy(&p->hDevice);
}

MM_EXPORT_AL
struct mmALBufferInfo*
    mmALManager_LoadBufferFromPath(
    struct mmALManager*                            p,
    const char*                                    pFileName)
{
    return
    mmALBufferLoader_LoadFromPath(
        &p->hBufferLoader,
        pFileName);
}

MM_EXPORT_AL
void
mmALManager_UnloadBufferByPath(
    struct mmALManager*                            p,
    const char*                                    pFileName)
{
    mmALBufferLoader_UnloadByPath(
        &p->hBufferLoader,
        pFileName);
}

MM_EXPORT_AL
struct mmALSource*
mmALManager_ProduceSource(
    struct mmALManager*                            p,
    const char*                                    pFileName,
    int                                            hStream)
{
    // When source limit, we can produce mmALSource object, but it is no sound.
    struct mmALSource* pSource;
    const char* pType = hStream ? "mmALSourceStream" : "mmALSourceStatic";
    pSource = mmALSourceFactory_Produce(&p->hSourceFactory, pType);
    assert(NULL != pSource && "pSource is invalid.");
    mmALSource_Prepare(pSource, &p->hBufferLoader, pFileName);
    mmRbtsetVpt_Add(&p->hSources, pSource);
    return pSource;
}

MM_EXPORT_AL
void
mmALManager_RecycleSource(
    struct mmALManager*                            p,
    struct mmALSource*                             pSource)
{
    // Internal check NULL is better for this API.
    if (NULL != pSource)
    {
        mmRbtsetVpt_Rmv(&p->hSources, pSource);
        mmALSource_Discard(pSource, &p->hBufferLoader);
        mmALSourceFactory_Recycle(&p->hSourceFactory, pSource);
    }
}

MM_EXPORT_AL
struct mmALSource*
mmALManager_PlayEffectDetail(
    struct mmALManager*                            p,
    const char*                                    pFileName,
    double                                         hPlayPosition,
    float                                          hVolume,
    float                                          hPitch)
{
    struct mmALSource* pSource;

    // OpenAL-soft source limit is 256 - 1.
    // The source index is start with 1.
    if (MM_ALSOURCE_MAX - 1 > p->hSources.size)
    {
        const struct mmALSourceFinished hFinished =
        {
            &mmALManagerEffectOnFinished,
            p,
        };
        pSource = mmALManager_ProduceSource(p, pFileName, MM_FALSE);
        mmALSource_SetFinished(pSource, &hFinished);
        mmALSource_SetVolume(pSource, hVolume);
        mmALSource_SetPitch(pSource, hPitch);
        mmALSource_SetPlayPosition(pSource, hPlayPosition);
        mmALSource_UpdatePlayPosition(pSource);
        mmALSource_Play(pSource);

        if (AL_NONE == pSource->hSource)
        {
            // driver is busy. we ignore this effect.
            mmALManager_RecycleSource(p, pSource);
            pSource = NULL;
        }
        else
        {
            mmRbtsetVpt_Add(&p->hEffects, pSource);
        }
    }
    else
    {
        // source is limit.
        pSource = NULL;
    }
    
    return pSource;
}

MM_EXPORT_AL
struct mmALSource*
mmALManager_PlayEffect(
    struct mmALManager*                            p,
    const char*                                    pFileName)
{
    struct mmALSource* pSource;

    // OpenAL-soft source limit is 256 - 1.
    // The source index is start with 1.
    if (MM_ALSOURCE_MAX - 1 > p->hSources.size)
    {
        const struct mmALSourceFinished hFinished =
        {
            &mmALManagerEffectOnFinished,
            p,
        };
        pSource = mmALManager_ProduceSource(p, pFileName, MM_FALSE);
        mmALSource_SetFinished(pSource, &hFinished);
        mmALSource_Play(pSource);

        if (AL_NONE == pSource->hSource)
        {
            // driver is busy. we ignore this effect.
            mmALManager_RecycleSource(p, pSource);
            pSource = NULL;
        }
        else
        {
            mmRbtsetVpt_Add(&p->hEffects, pSource);
        }
    }
    else
    {
        // source is limit.
        pSource = NULL;
    }

    return pSource;
}

MM_EXPORT_AL
void
mmALManager_StopEffect(
    struct mmALManager*                            p,
    struct mmALSource*                             pSource)
{
    mmALSource_Stop(pSource);
    mmALManagerEffectOnFinished(p, pSource);
}

MM_EXPORT_AL
void
mmALManager_StopEffectComplete(
    struct mmALManager*                            p)
{
    struct mmALSource* s;
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    
    // stop and recycle running source.
    n = mmRb_First(&p->hEffects.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        n = mmRb_Next(n);
        s = it->k;
        mmRbtsetVpt_Erase(&p->hEffects, it);
        
        mmALSource_Stop(s);
        mmALManager_RecycleSource(p, s);
    }
}

MM_EXPORT_AL
void
mmALManager_Update(
    struct mmALManager*                            p,
    double                                         interval)
{
    struct mmALSource* s;
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    n = mmRb_First(&p->hSources.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        n = mmRb_Next(n);
        s = it->k;
        mmALSource_Update(s, interval);
    }
}

MM_EXPORT_AL
void
mmALManager_EnterBackground(
    struct mmALManager*                            p)
{
    mmALListener_EnterBackground(&p->hListener);
}

MM_EXPORT_AL
void
mmALManager_EnterForeground(
    struct mmALManager*                            p)
{
    mmALListener_EnterForeground(&p->hListener);
}
