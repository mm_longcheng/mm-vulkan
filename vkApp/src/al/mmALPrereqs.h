/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALPrereqs_h__
#define __mmALPrereqs_h__

#include "core/mmCore.h"

#include "al/mmALExport.h"

#ifndef MM_APENAL_SOFT
#define MM_APENAL_SOFT 1
#endif//MM_APENAL_SOFT

#if 1 == MM_APENAL_SOFT

#include "AL/al.h"
#include "AL/alc.h"
#include "AL/alext.h"
#include "AL/efx-creative.h"
#include "AL/efx-presets.h"
#include "AL/efx.h"

#else

#   if MM_PLATFORM == MM_PLATFORM_WIN32

// #    pragma warning( disable : 4244 )

/**
 * Specifies whether EFX enhancements are supported
 * 0 - EFX not supported
 * 1 - Enable EFX suport
 */
#    ifndef HAVE_EFX
#        define HAVE_EFX 1
#    endif

#    include "al.h"
#    include "alc.h"
#    if HAVE_EFX
#        include "efx.h"
#        include "efx-util.h"
#        include "efx-creative.h"
#        include "xram.h"
#    endif
#elif MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG
#    if MM_PLATFORM == MM_PLATFORM_APPLE
#        include <al.h>
#        include <alc.h>
#    else
#        include <AL/al.h>
#        include <AL/alc.h>
#    endif
#else // Other Compilers
#    include <OpenAL/al.h>
#    include <OpenAL/alc.h>
#    include "xram.h"
#endif

#endif

#endif//__mmALPrereqs_h__
