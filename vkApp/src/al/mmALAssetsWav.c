/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALAssetsWav.h"

#include "al/mmALError.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByteCoder.h"

#include "dish/mmFileContext.h"

MM_EXPORT_AL
mmUInt64_t
mmALWAVEHeader_GetBitsPerSec(
    const struct mmALWAVEHeader*                   p)
{
    return p->SamplesPerSec * p->Channels * p->BitsPerSample;
}

MM_EXPORT_AL
mmUInt32_t
mmALWAVEHeader_GetBitsPerFrame(
    const struct mmALWAVEHeader*                   p)
{
    return p->Channels * p->BitsPerSample;
}

MM_EXPORT_AL
void
mmALWAVEHeader_ParseData(
    struct mmALWAVEHeader*                         p,
    struct mmByteBuffer*                           s)
{
    do
    {
        // Read in chunk header
        mmByteCoderLittleDecodeBin(s, (mmUInt8_t*)p->SubChunk2Id, 4);
        if (0 != mmMemcmp(p->SubChunk2Id, "data", 4))
        {
            // Unsupported chunk...
            
            mmByteCoderLittleDecodeU32(s, &p->SubChunk2Size);
            
            // skip
            s->offset += p->SubChunk2Size;
        }
        else
        {
            // 'data' chunk...
            
            mmUInt32_t hFileCheck;
            
            mmByteCoderLittleDecodeU32(s, &p->SubChunk2Size);
            
            // Store byte offset of start of audio data
            p->DataOffset = s->offset;
            
            // Check data size
            hFileCheck = p->SubChunk2Size % p->BlockAlign;
            
            // Store end pos
            p->DataFinish = p->DataOffset + (p->SubChunk2Size - hFileCheck);
            
            break;
        }
    }
    while (s->offset <= s->length);
}

MM_EXPORT_AL
int
mmALWAVEHeader_ParseHead(
    struct mmALWAVEHeader*                         p,
    struct mmByteBuffer*                           s)
{
    int err = MM_UNKNOWN;
    
    do
    {
        mmUInt32_t hExtraBytes;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmByteCoderLittleDecodeBin(s, (mmUInt8_t*)p->ChunkId, 4);
        if (0 != mmMemcmp(p->ChunkId, "RIFF", 4))
        {
            mmLogger_LogE(gLogger, "%s %d Not support ChunkId.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmByteCoderLittleDecodeU32(s, &p->ChunkSize);
        
        mmByteCoderLittleDecodeBin(s, (mmUInt8_t*)p->Format, 4);
        if (0 != mmMemcmp(p->Format, "WAVE", 4))
        {
            mmLogger_LogE(gLogger, "%s %d Not support Format.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmByteCoderLittleDecodeBin(s, (mmUInt8_t*)p->SubChunk1Id, 4);
        if (0 != mmMemcmp(p->SubChunk1Id, "fmt ", 4))
        {
            mmLogger_LogE(gLogger, "%s %d Not support SubChunk1Id.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmByteCoderLittleDecodeU32(s, &p->SubChunk1Size);
        
        // SubChunk1Size be 16 unless compressed ( compressed NOT supported )
        if (p->SubChunk1Size < 16)
        {
            mmLogger_LogE(gLogger, "%s %d Not support SubChunk1Size.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmByteCoderLittleDecodeU16(s, &p->FormatTag);
        
        // PCM == 1
        if (0x0001 != p->FormatTag && 0xFFFE != p->FormatTag)
        {
            mmLogger_LogE(gLogger, "%s %d Not support FormatTag.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmByteCoderLittleDecodeU16(s, &p->Channels);
        mmByteCoderLittleDecodeU32(s, &p->SamplesPerSec);
        mmByteCoderLittleDecodeU32(s, &p->AvgBytesPerSec);
        mmByteCoderLittleDecodeU16(s, &p->BlockAlign);
        mmByteCoderLittleDecodeU16(s, &p->BitsPerSample);
        
        // Samples check..
        if ((16 != p->BitsPerSample) && (8 != p->BitsPerSample))
        {
            mmLogger_LogE(gLogger, "%s %d Not support BitsPerSample.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        // 16 is WAVE format base size.
        hExtraBytes = p->SubChunk1Size - 16;
        if (0xFFFE == p->FormatTag && 22 > hExtraBytes)
        {
            mmLogger_LogE(gLogger, "%s %d Header invalid WAVE_FORMAT_EXTENSIBLE.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        // Calculate extra WAV header info
        if (0xFFFE == p->FormatTag)
        {
            // extended size 22
            mmByteCoderLittleDecodeU16(s, &p->Samples);
            mmByteCoderLittleDecodeU32(s, &p->ChannelMask);
            mmByteCoderLittleDecodeBin(s, (mmUInt8_t*)p->SubFormat, 16);

            hExtraBytes -= 22;
        }
        else
        {
            p->Samples = 0;
            p->ChannelMask = 0;
            mmMemset(p->SubFormat, 0, 16);
        }
        
        // Skip
        s->offset += hExtraBytes;
        
        mmALWAVEHeader_ParseData(p, s);
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

MM_EXPORT_AL
int
mmALWAVEHeader_ParseFromStream(
    struct mmALWAVEHeader*                         p,
    struct mmStream*                               pStream)
{
    int err = MM_UNKNOWN;
    
    do
    {
        size_t n;
        long fdsize;
        
        struct mmByteBuffer s;
        mmUInt8_t b[sizeof(struct mmALWAVEHeader)] = { 0 };
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pStream)
        {
            mmLogger_LogE(gLogger, "%s %d Stream is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmStreamSeek(pStream, 0, SEEK_END);
        fdsize = mmStreamTell(pStream);
        mmStreamSeek(pStream, 0, SEEK_SET);
        
        if (fdsize < sizeof(struct mmALWAVEHeader))
        {
            mmLogger_LogE(gLogger, "%s %d FILE size failure, invalid WAV.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        n = mmStreamRead(b, sizeof(char), sizeof(struct mmALWAVEHeader), pStream);
        if (n < sizeof(struct mmALWAVEHeader))
        {
            mmLogger_LogE(gLogger, "%s %d FILE fread failure, invalid WAV.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        s.buffer = b;
        s.offset = 0;
        s.length = sizeof(struct mmALWAVEHeader);
        
        err = mmALWAVEHeader_ParseHead(p, &s);
    } while(0);
    
    return err;
}

MM_EXPORT_AL
int
mmALAssetsWavPickFormat(
    struct mmALWAVEHeader*                         pHeader,
    struct mmALAssetsInfo*                         pAssetsInfo)
{
    int err = MM_UNKNOWN;
    
    // OpenAL format
    ALenum mFormat = 0;
    // Size of audio buffer (250ms)
    size_t mBufferSize = 0;
    
    switch(pHeader->Channels)
    {
    case 1:
        {
            if (pHeader->BitsPerSample == 8)
            {
                // 8-bit mono
                mFormat = AL_FORMAT_MONO8;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize = pHeader->SamplesPerSec / 4;
            }
            else
            {
                // 16-bit mono
                mFormat = AL_FORMAT_MONO16;

                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    case 2:
        {
            if (pHeader->BitsPerSample == 8)
            {
                // 8-bit stereo
                mFormat = AL_FORMAT_STEREO8;

                // Set BufferSize to 250ms (Frequency * 2 (8bit stereo) divided by 4 (quarter of a second))
                mBufferSize = pHeader->SamplesPerSec >> 1;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % 2);
            }
            else
            {
                // 16-bit stereo
                mFormat = AL_FORMAT_STEREO16;

                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    case 4:
        {
            // 16-bit Quad surround
            mFormat = alGetEnumValue("AL_FORMAT_QUAD16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    case 6:
        {
            // 16-bit 5.1 surround
            mFormat = alGetEnumValue("AL_FORMAT_51CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    case 7:
        {
            // 16-bit 7.1 surround
            mFormat = alGetEnumValue("AL_FORMAT_61CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    case 8:
        {
            // 16-bit 8.1 surround
            mFormat = alGetEnumValue("AL_FORMAT_71CHN16");
            mmALCheck(MM_LOG_ERROR, alGetError());
            if (!mFormat)
            {
                err = MM_FAILURE;
            }
            else
            {
                // Queue 250ms of audio data
                mBufferSize = pHeader->AvgBytesPerSec >> 2;

                // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
                mBufferSize -= (mBufferSize % pHeader->BlockAlign);
            }
        }
        break;
    default:
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            
            // Error message
            mmLogger_LogW(gLogger, "mmALAssets WAV channels invalid. Defaulting to 16-bit stereo.");

            // 16-bit stereo
            mFormat = AL_FORMAT_STEREO16;

            // Queue 250ms of audio data
            mBufferSize = pHeader->AvgBytesPerSec >> 2;

            // IMPORTANT : The Buffer Size must be an exact multiple of the BlockAlignment ...
            mBufferSize -= (mBufferSize % pHeader->BlockAlign);
        }
        break;
    }
    
    if (MM_FAILURE != err)
    {
        pAssetsInfo->Format = mFormat;
        pAssetsInfo->Bytes250ms = mBufferSize;
        
        err = MM_SUCCESS;
    }
    
    return err;
}

MM_EXPORT_AL
int
mmALAssetsWavQueryInfo(
    struct mmALWAVEHeader*                         pHeader,
    struct mmALAssetsInfo*                         pAssetsInfo)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pHeader)
        {
            mmLogger_LogE(gLogger, "mmALAssets WAV pHeader is invalid!");
            err = MM_FAILURE;
            break;
        }
        
        pAssetsInfo->Channels = pHeader->Channels;
        pAssetsInfo->SampleRate = pHeader->SamplesPerSec;
        pAssetsInfo->BitsPerSample = pHeader->BitsPerSample;
        
        err = mmALAssetsWavPickFormat(pHeader, pAssetsInfo);
        if (MM_FAILURE == err)
        {
            mmLogger_LogE(gLogger, "mmALAssets WAV format not support.");
            break;
        }
        
        pAssetsInfo->BitsPerFrame = pAssetsInfo->Channels * pAssetsInfo->BitsPerSample;
        pAssetsInfo->BitsPerSec = pAssetsInfo->SampleRate * pAssetsInfo->BitsPerFrame;
        
        pAssetsInfo->ByteLength = pHeader->DataFinish - pHeader->DataOffset;
        pAssetsInfo->TotalFrame = (pAssetsInfo->ByteLength * 8) / pAssetsInfo->BitsPerFrame;
        pAssetsInfo->TotalTime = (pAssetsInfo->ByteLength * 8.0) / (double)(pAssetsInfo->BitsPerSec);
        pAssetsInfo->Seekable = MM_TRUE;
        
        err = MM_SUCCESS;
    } while (0);
    
    return err;
}

const struct mmMetaAllocator mmALAllocatorAssetsWav =
{
    "mmALAssetsWav",
    sizeof(struct mmALAssetsWav),
    &mmALAssetsWav_Produce,
    &mmALAssetsWav_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataAssets mmALMetadataAssetsWav =
{
    &mmALAllocatorAssetsWav,
    /*- Interface -*/
    &mmALAssetsWav_Prepare,
    &mmALAssetsWav_Discard,
    &mmALAssetsWav_GetTotalTime,
    &mmALAssetsWav_GetTotalFrame,
    &mmALAssetsWav_GetIsSeekable,
    &mmALAssetsWav_Read,
    &mmALAssetsWav_SeekTime,
    &mmALAssetsWav_TellTime,
    &mmALAssetsWav_SeekFrame,
    &mmALAssetsWav_TellFrame,
};

MM_EXPORT_AL
void
mmALAssetsWav_Init(
    struct mmALAssetsWav*                          p)
{
    mmALAssets_Init(&p->Assets);
    mmStream_Init(&p->Stream);
    mmMemset(&p->Header, 0, sizeof(struct mmALWAVEHeader));
}

MM_EXPORT_AL
void
mmALAssetsWav_Destroy(
    struct mmALAssetsWav*                          p)
{
    mmMemset(&p->Header, 0, sizeof(struct mmALWAVEHeader));
    mmStream_Destroy(&p->Stream);
    mmALAssets_Destroy(&p->Assets);
}

MM_EXPORT_AL
void
mmALAssetsWav_Produce(
    struct mmALAssetsWav*                          p)
{
    mmALAssetsWav_Init(p);
    
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsWav;
}

MM_EXPORT_AL
void
mmALAssetsWav_Recycle(
    struct mmALAssetsWav*                          p)
{
    // bind object metadata.
    p->Assets.pMetadata = &mmALMetadataAssetsWav;
    
    mmALAssetsWav_Destroy(p);
}

MM_EXPORT_AL
int
mmALAssetsWav_Prepare(
    struct mmALAssetsWav*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d pFileContext is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmFileContext_AcquireStream(pFileContext, pFileName, "rb", &p->Stream);
        if (mmStream_Invalid(&p->Stream))
        {
            mmLogger_LogE(gLogger, "%s %d mmFileContext_AcquireStream failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = mmALWAVEHeader_ParseFromStream(&p->Header, &p->Stream);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALWAVEHeader_ParseFromStream failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = mmStreamSeeko64(&p->Stream, p->Header.DataOffset, SEEK_SET);
        if (0 != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmStreamSeek failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmALAssetsWavQueryInfo(&p->Header, &p->Assets.hAssetsInfo);
        if (0 != err)
        {
            mmLogger_LogE(gLogger, "%s %d mmALAssetsWavQueryInfo failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = MM_SUCCESS;
    } while(0);

    return err;
}

MM_EXPORT_AL
void
mmALAssetsWav_Discard(
    struct mmALAssetsWav*                          p,
    struct mmFileContext*                          pFileContext)
{
    // reset header data.
    mmMemset(&p->Header, 0, sizeof(struct mmALWAVEHeader));
    
    mmFileContext_ReleaseStream(pFileContext, &p->Stream);
}

MM_EXPORT_AL
double
mmALAssetsWav_GetTotalTime(
    struct mmALAssetsWav*                          p)
{
    mmUInt64_t BitsPerSec = mmALWAVEHeader_GetBitsPerSec(&p->Header);
    size_t ByteLength = p->Header.DataFinish - p->Header.DataOffset;
    return (ByteLength * 8.0) / (double)(BitsPerSec);
}

MM_EXPORT_AL
int64_t
mmALAssetsWav_GetTotalFrame(
    struct mmALAssetsWav*                          p)
{
    size_t ByteLength = p->Header.DataFinish - p->Header.DataOffset;
    return (int64_t)((ByteLength * 8) / (p->Header.Channels * p->Header.BitsPerSample));
}

MM_EXPORT_AL
int
mmALAssetsWav_GetIsSeekable(
    const struct mmALAssetsWav*                    p)
{
    return MM_TRUE;
}

MM_EXPORT_AL
size_t
mmALAssetsWav_Read(
    struct mmALAssetsWav*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght)
{
    return mmStreamRead(pBuffer, 1, hLenght, &p->Stream);
}

MM_EXPORT_AL
int
mmALAssetsWav_SeekTime(
    struct mmALAssetsWav*                          p,
    double                                         hPosition)
{
    mmUInt64_t BitsPerSec = mmALWAVEHeader_GetBitsPerSec(&p->Header);
    size_t ByteLength = (size_t)(hPosition * (double)BitsPerSec / 8.0);
    return mmStreamSeeko64(&p->Stream, ByteLength, SEEK_SET);
}

MM_EXPORT_AL
double
mmALAssetsWav_TellTime(
    struct mmALAssetsWav*                          p)
{
    mmUInt64_t BitsPerSec = mmALWAVEHeader_GetBitsPerSec(&p->Header);
    size_t ByteLength = mmStreamTello64(&p->Stream);
    return (ByteLength * 8.0) / (double)(BitsPerSec);
}

MM_EXPORT_AL
int
mmALAssetsWav_SeekFrame(
    struct mmALAssetsWav*                          p,
    int64_t                                        hPosition)
{
    uint32_t BitsPerFrame = mmALWAVEHeader_GetBitsPerFrame(&p->Header);
    size_t ByteLength = hPosition * BitsPerFrame / 8;
    return mmStreamSeeko64(&p->Stream, ByteLength, SEEK_SET);
}

MM_EXPORT_AL
int64_t
mmALAssetsWav_TellFrame(
    struct mmALAssetsWav*                          p)
{
    uint32_t BitsPerFrame = mmALWAVEHeader_GetBitsPerFrame(&p->Header);
    size_t ByteLength = mmStreamTello64(&p->Stream);
    return (ByteLength * 8) / BitsPerFrame;
}
