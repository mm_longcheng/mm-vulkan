/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmALSource.h"

#include "al/mmALError.h"
#include "al/mmALAssets.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStreambuf.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"

MM_EXPORT_AL
void
mmALSourceCallbackDefault(
    void*                                          obj,
    struct mmALSource*                             s)
{
    // do nothing here.
}

MM_EXPORT_AL
void
mmALSourceCallback_Reset(
    struct mmALSourceCallback*                     p)
{
    p->OnLoaded    = &mmALSourceCallbackDefault;
    p->OnDestroyed = &mmALSourceCallbackDefault;
    p->OnPlayed    = &mmALSourceCallbackDefault;
    p->OnStopped   = &mmALSourceCallbackDefault;
    p->OnFinished  = &mmALSourceCallbackDefault;
    p->OnPaused    = &mmALSourceCallbackDefault;
    p->OnLooping   = &mmALSourceCallbackDefault;
    p->obj = NULL;
}

MM_EXPORT_AL
void
mmALSourceFinished_Reset(
    struct mmALSourceFinished*                     p)
{
    p->OnFinished  = &mmALSourceCallbackDefault;
    p->obj = NULL;
}

static
int
mmALSourceNone_Prepare(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName)
{
    return MM_FAILURE;
}

static
void
mmALSourceNone_Discard(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader)
{
    
}

static
int
mmALSourceNone_GetIsStream(
    const struct mmALSource*                       p)
{
    return MM_FALSE;
}

static
struct mmALAssets*
mmALSourceNone_GetAssets(
    const struct mmALSource*                       p)
{
    return NULL;
}

static
void
mmALSourceNone_SetLoop(
    struct mmALSource*                             p,
    int                                            hLoop)
{
    
}

static
void
mmALSourceNone_RewindImmediately(
    struct mmALSource*                             p)
{
    
}

static
void
mmALSourceNone_SetPlayPosition(
    struct mmALSource*                             p,
    double                                         hPosition)
{
    
}

static
double
mmALSourceNone_GetPlayPosition(
    const struct mmALSource*                       p)
{
    return 0.0;
}

static
void
mmALSourceNone_UpdatePlayPosition(
    struct mmALSource*                             p)
{
    
}

static
void
mmALSourceNone_Update(
    struct mmALSource*                             p,
    double                                         interval)
{
    
}

const struct mmMetaAllocator mmALAllocatorSourceNone =
{
    "mmALSource",
    sizeof(struct mmALSource),
    &mmALSource_Produce,
    &mmALSource_Recycle,
};

MM_EXPORT_AL const struct mmALMetadataSource mmALMetadataSourceNone =
{
    &mmALAllocatorSourceNone,
    /*- Interface -*/
    &mmALSourceNone_Prepare,
    &mmALSourceNone_Discard,
    &mmALSourceNone_GetIsStream,
    &mmALSourceNone_GetAssets,
    &mmALSourceNone_SetLoop,
    &mmALSourceNone_RewindImmediately,
    &mmALSourceNone_SetPlayPosition,
    &mmALSourceNone_GetPlayPosition,
    &mmALSourceNone_UpdatePlayPosition,
    &mmALSourceNone_Update,
};

MM_EXPORT_AL
void
mmALSource_Init(
    struct mmALSource*                             p)
{
    mmString_Init(&p->hName);
    mmALSourceCallback_Reset(&p->hCallback);
    mmALSourceFinished_Reset(&p->hFinished);
    p->pMetadata = NULL;
    p->hState = mmALSourceStateNone;
    p->hSource = AL_NONE;
    p->hLoop = MM_FALSE;
    p->hPlayPositionChanged = MM_FALSE;
    p->hPlayPosition = 0.0;
    
    mmVec3AssignValue(p->hPosition, 0.0f);
    mmVec3AssignValue(p->hDirection, 0.0f);
    mmVec3AssignValue(p->hVelocity, 0.0f);
    p->hVolume = 1.0f;
    p->hMaxVolume = 1.0f;
    p->hMinVolume = 0.0f;
    p->hMaxDistance = 1E10;
    p->hRolloffFactor = 1.0f;
    p->hReferenceDistance = 1.0f;
    p->hOuterConeVolume = 0.0f;
    p->hInnerConeAngle = 360.0f;
    p->hOuterConeAngle = 360.0f;
    p->hPitch = 1.0f;
    p->hSourceRelative = MM_FALSE;
}

MM_EXPORT_AL
void
mmALSource_Destroy(
    struct mmALSource*                             p)
{
    p->hSourceRelative = MM_FALSE;
    p->hPitch = 1.0f;
    p->hOuterConeAngle = 360.0f;
    p->hInnerConeAngle = 360.0f;
    p->hOuterConeVolume = 0.0f;
    p->hReferenceDistance = 1.0f;
    p->hRolloffFactor = 1.0f;
    p->hMaxDistance = 1E10;
    p->hMinVolume = 0.0f;
    p->hMaxVolume = 1.0f;
    p->hVolume = 1.0f;
    mmVec3AssignValue(p->hVelocity, 0.0f);
    mmVec3AssignValue(p->hDirection, 0.0f);
    mmVec3AssignValue(p->hPosition, 0.0f);
    
    p->hPlayPosition = 0.0;
    p->hPlayPositionChanged = MM_FALSE;
    p->hLoop = MM_FALSE;
    p->hSource = AL_NONE;
    p->hState = mmALSourceStateNone;
    p->pMetadata = NULL;
    mmALSourceFinished_Reset(&p->hFinished);
    mmALSourceCallback_Reset(&p->hCallback);
    mmString_Destroy(&p->hName);
}

MM_EXPORT_AL
void
mmALSource_Produce(
    struct mmALSource*                             p)
{
    mmALSource_Init(p);
    p->pMetadata = &mmALMetadataSourceNone;
}

MM_EXPORT_AL
void
mmALSource_Recycle(
    struct mmALSource*                             p)
{
    p->pMetadata = &mmALMetadataSourceNone;
    mmALSource_Destroy(p);
}

MM_EXPORT_AL
int
mmALSource_PrepareBase(
    struct mmALSource*                             p)
{
    int err = MM_UNKNOWN;
    
    do
    {
        ALenum ALErr;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        alGenSources(1, &p->hSource);
        ALErr = alGetError();
        if (AL_OUT_OF_MEMORY == ALErr)
        {
            // AL_OUT_OF_MEMORY is case source limit.
            // here we mark it for busy.
            err = MM_BUSY;
            break;
        }
        mmALCheck(MM_LOG_ERROR, ALErr);
        if (AL_NO_ERROR != ALErr)
        {
            mmLogger_LogE(gLogger, "%s %d AL alGenSources failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        p->hPlayPositionChanged = MM_FALSE;
        p->hPlayPosition = 0.0;
        
        err = MM_SUCCESS;
    } while(0);
    
    return err;
}

MM_EXPORT_AL
void
mmALSource_DiscardBase(
    struct mmALSource*                             p)
{
    p->hPlayPosition = 0.0;
    p->hPlayPositionChanged = MM_FALSE;
    
    if (AL_NONE != p->hSource)
    {
        alDeleteSources(1, &p->hSource);
        mmALCheck(MM_LOG_ERROR, alGetError());
        p->hSource = AL_NONE;
    }
}

MM_EXPORT_AL
void
mmALSource_SetName(
    struct mmALSource*                             p,
    const char*                                    pName)
{
    mmString_Assigns(&p->hName, pName);
}

MM_EXPORT_AL
void
mmALSource_SetCallback(
    struct mmALSource*                             p,
    const struct mmALSourceCallback*               pCallback)
{
    p->hCallback = (*pCallback);
}

MM_EXPORT_AL
void
mmALSource_SetFinished(
    struct mmALSource*                             p,
    const struct mmALSourceFinished*               pFinished)
{
    p->hFinished = (*pFinished);
}

MM_EXPORT_AL
int
mmALSource_GetIsPlaying(
    const struct mmALSource*                       p)
{
    return
        (p->hSource != AL_NONE) &&
        (p->hState == mmALSourceStatePlaying);
}

MM_EXPORT_AL
int
mmALSource_GetIsPaused(
    const struct mmALSource*                       p)
{
    return
        (p->hSource != AL_NONE) &&
        (p->hState == mmALSourceStatePaused);
}

MM_EXPORT_AL
int
mmALSource_Invalid(
    const struct mmALSource*                       p)
{
    return
        (NULL == p) ||
        (AL_NONE == p->hSource) ||
        (NULL == mmALSource_GetAssets(p));
}

MM_EXPORT_AL
void
mmALSource_UpdatePropertys(
    const struct mmALSource*                       p)
{
    int hStream = mmALSource_GetIsStream(p);
    int hLoop = hStream ? AL_FALSE : p->hLoop;
    
    alSourcefv(p->hSource, AL_POSITION          , p->hPosition);
    alSourcefv(p->hSource, AL_DIRECTION         , p->hDirection);
    alSourcefv(p->hSource, AL_VELOCITY          , p->hVelocity);
    alSourcef (p->hSource, AL_GAIN              , p->hVolume);
    alSourcef (p->hSource, AL_MAX_GAIN          , p->hMaxVolume);
    alSourcef (p->hSource, AL_MIN_GAIN          , p->hMinVolume);
    alSourcef (p->hSource, AL_MAX_DISTANCE      , p->hMaxDistance);
    alSourcef (p->hSource, AL_ROLLOFF_FACTOR    , p->hRolloffFactor);
    alSourcef (p->hSource, AL_REFERENCE_DISTANCE, p->hReferenceDistance);
    alSourcef (p->hSource, AL_CONE_OUTER_GAIN   , p->hOuterConeVolume);
    alSourcef (p->hSource, AL_CONE_INNER_ANGLE  , p->hInnerConeAngle);
    alSourcef (p->hSource, AL_CONE_OUTER_ANGLE  , p->hOuterConeAngle);
    alSourcef (p->hSource, AL_PITCH             , p->hPitch);
    alSourcei (p->hSource, AL_SOURCE_RELATIVE   , p->hSourceRelative);
    alSourcei (p->hSource, AL_LOOPING           , hLoop);
}

MM_EXPORT_AL
void
mmALSource_SetPosition(
    struct mmALSource*                             p,
    const float                                    position[3])
{
    mmVec3Assign(p->hPosition, position);
    
    if(AL_NONE != p->hSource)
    {
        alSourcefv(p->hSource, AL_POSITION, p->hPosition);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
void
mmALSource_GetPosition(
    const struct mmALSource*                       p,
    float                                          position[3])
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        mmVec3AssignValue(position, 0.0f);
        // have source.
        alGetSourcefv(p->hSource, AL_POSITION, position);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
    else
    {
        // If not - return requested setting.
        mmVec3Assign(position, p->hPosition);
    }
}

MM_EXPORT_AL
void
mmALSource_SetDirection(
    struct mmALSource*                             p,
    const float                                    direction[3])
{
    mmVec3Assign(p->hDirection, direction);
    
    if(AL_NONE != p->hSource)
    {
        alSourcefv(p->hSource, AL_DIRECTION, p->hDirection);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
void
mmALSource_GetDirection(
    const struct mmALSource*                       p,
    float                                          direction[3])
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        mmVec3AssignValue(direction, 0.0f);
        // have source.
        alGetSourcefv(p->hSource, AL_DIRECTION, direction);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
    else
    {
        // If not - return requested setting.
        mmVec3Assign(direction, p->hDirection);
    }
}

MM_EXPORT_AL
void
mmALSource_SetVelocity(
    struct mmALSource*                             p,
    const float                                    velocity[3])
{
    mmVec3Assign(p->hVelocity, velocity);
    
    if(AL_NONE != p->hSource)
    {
        alSourcefv(p->hSource, AL_VELOCITY, p->hVelocity);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
void
mmALSource_GetVelocity(
    const struct mmALSource*                       p,
    float                                          velocity[3])
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        mmVec3AssignValue(velocity, 0.0f);
        // have source.
        alGetSourcefv(p->hSource, AL_VELOCITY, velocity);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
    else
    {
        // If not - return requested setting.
        mmVec3Assign(velocity, p->hVelocity);
    }
}

MM_EXPORT_AL
void
mmALSource_SetVolume(
    struct mmALSource*                             p,
    float                                          volume)
{
    p->hVolume = mmMathMax(volume, 0.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_GAIN, p->hVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetVolume(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float volume = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_GAIN, &volume);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return volume;
    }
    else
    {
        // If not - return requested setting.
        return p->hVolume;
    }
}

MM_EXPORT_AL
void
mmALSource_SetMaxVolume(
    struct mmALSource*                             p,
    float                                          maxVolume)
{
    p->hMaxVolume = mmMathClamp(maxVolume, 0.0f, 1.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_MAX_GAIN, p->hMaxVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetMaxVolume(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float maxVolume = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_MAX_GAIN, &maxVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return maxVolume;
    }
    else
    {
        // If not - return requested setting.
        return p->hMaxVolume;
    }
}

MM_EXPORT_AL
void
mmALSource_SetMinVolume(
    struct mmALSource*                             p,
    float                                          minVolume)
{
    p->hMinVolume = mmMathClamp(minVolume, 0.0f, 1.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_MIN_GAIN, p->hMinVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetMinVolume(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float minVolume = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_MIN_GAIN, &minVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return minVolume;
    }
    else
    {
        // If not - return requested setting.
        return p->hMinVolume;
    }
}

MM_EXPORT_AL
void
mmALSource_SetInnerConeAngle(
    struct mmALSource*                             p,
    float                                          innerConeAngle)
{
    p->hInnerConeAngle = mmMathClamp(innerConeAngle, 0.0f, 360.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_CONE_INNER_ANGLE, p->hInnerConeAngle);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetInnerConeAngle(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float innerConeAngle = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_CONE_INNER_ANGLE, &innerConeAngle);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return innerConeAngle;
    }
    else
    {
        // If not - return requested setting.
        return p->hInnerConeAngle;
    }
}

MM_EXPORT_AL
void
mmALSource_SetOuterConeAngle(
    struct mmALSource*                             p,
    float                                          outerConeAngle)
{
    p->hOuterConeAngle = mmMathClamp(outerConeAngle, 0.0f, 360.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_CONE_OUTER_ANGLE, p->hOuterConeAngle);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetOuterConeAngle(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float outerConeAngle = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_CONE_OUTER_ANGLE, &outerConeAngle);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return outerConeAngle;
    }
    else
    {
        // If not - return requested setting.
        return p->hOuterConeAngle;
    }
}

MM_EXPORT_AL
void
mmALSource_SetOuterConeVolume(
    struct mmALSource*                             p,
    float                                          volume)
{
    p->hOuterConeVolume = mmMathClamp(volume, 0.0f, 1.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_CONE_OUTER_GAIN, p->hOuterConeVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetOuterConeVolume(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float outerConeVolume = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_CONE_OUTER_GAIN, &outerConeVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return outerConeVolume;
    }
    else
    {
        // If not - return requested setting.
        return p->hOuterConeVolume;
    }
}

MM_EXPORT_AL
void
mmALSource_SetMaxDistance(
    struct mmALSource*                             p,
    float                                          maxDistance)
{
    p->hMaxDistance = mmMathMax(maxDistance, 0.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_MAX_DISTANCE, p->hMaxDistance);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetMaxDistance(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float maxDistance = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_MAX_DISTANCE, &maxDistance);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return maxDistance;
    }
    else
    {
        // If not - return requested setting.
        return p->hMaxDistance;
    }
}

MM_EXPORT_AL
void
mmALSource_SetRolloffFactor(
    struct mmALSource*                             p,
    float                                          rolloffFactor)
{
    p->hRolloffFactor = mmMathMax(rolloffFactor, 0.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_ROLLOFF_FACTOR, p->hRolloffFactor);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetRolloffFactor(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float rolloffFactor = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_ROLLOFF_FACTOR, &rolloffFactor);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return rolloffFactor;
    }
    else
    {
        // If not - return requested setting.
        return p->hRolloffFactor;
    }
}

MM_EXPORT_AL
void
mmALSource_SetReferenceDistance(
    struct mmALSource*                             p,
    float                                          referenceDistance)
{
    p->hReferenceDistance = mmMathMax(referenceDistance, 0.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_REFERENCE_DISTANCE, p->hReferenceDistance);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetReferenceDistance(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float referenceDistance = 0.0f;
        // have source.
        alGetSourcef(p->hSource, AL_REFERENCE_DISTANCE, &referenceDistance);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return referenceDistance;
    }
    else
    {
        // If not - return requested setting.
        return p->hReferenceDistance;
    }
}

MM_EXPORT_AL
void
mmALSource_SetPitch(
    struct mmALSource*                             p,
    float                                          pitch)
{
    p->hPitch = mmMathMax(pitch, 0.0f);

    if(AL_NONE != p->hSource)
    {
        alSourcef(p->hSource, AL_PITCH, p->hPitch);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
float
mmALSource_GetPitch(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        float pitch = -1.0f;
        // have source.
        alGetSourcef(p->hSource, AL_PITCH, &pitch);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return pitch;
    }
    else
    {
        // If not - return requested setting.
        return p->hPitch;
    }
}

MM_EXPORT_AL
void
mmALSource_SetRelativeToListener(
    struct mmALSource*                             p,
    int                                            relative)
{
    p->hSourceRelative = relative;
    
    if(AL_NONE != p->hSource)
    {
        alSourcei(p->hSource, AL_SOURCE_RELATIVE, p->hSourceRelative);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_AL
int
mmALSource_GetRelativeToListener(
    const struct mmALSource*                       p)
{
    // Check if a source is available
    if(AL_NONE != p->hSource)
    {
        int relative = MM_FALSE;
        // have source.
        alGetSourcei(p->hSource, AL_SOURCE_RELATIVE, &relative);
        mmALCheck(MM_LOG_ERROR, alGetError());
        return relative;
    }
    else
    {
        // If not - return requested setting.
        return p->hSourceRelative;
    }
}

MM_EXPORT_AL
void
mmALSource_Play(
    struct mmALSource*                             p)
{
    do
    {
        struct mmALSourceCallback* pCallback = &p->hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (mmALSourceStateDestroyed == p->hState)
        {
            mmLogger_LogE(gLogger, "%s %d hState is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        if (AL_NONE == p->hSource)
        {
            break;
        }
        
        if (mmALSource_GetIsPlaying(p))
        {
            break;
        }
        
        alSourcePlay(p->hSource);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        // Set play flag
        p->hState = mmALSourceStatePlaying;
        
        // Notify listener
        (*(pCallback->OnPlayed))(pCallback->obj, p);
    } while(0);
}

MM_EXPORT_AL
void
mmALSource_Stop(
    struct mmALSource*                             p)
{
    do
    {
        struct mmALSourceCallback* pCallback = &p->hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (mmALSourceStateDestroyed == p->hState)
        {
            mmLogger_LogE(gLogger, "%s %d hState is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        if (AL_NONE == p->hSource)
        {
            break;
        }
        
        alSourceStop(p->hSource);
        mmALCheck(MM_LOG_ERROR, alGetError());

        // Stop playback
        p->hState = mmALSourceStateStopped;
    
        // Notify listener
        (*(pCallback->OnStopped))(pCallback->obj, p);
    } while(0);
}

MM_EXPORT_AL
void
mmALSource_Rewind(
    struct mmALSource*                             p)
{
    // call interface implement.
    mmALSource_RewindImmediately(p);
}

MM_EXPORT_AL
void
mmALSource_Pause(
    struct mmALSource*                             p)
{
    do
    {
        struct mmALSourceCallback* pCallback = &p->hCallback;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (mmALSourceStateDestroyed == p->hState)
        {
            mmLogger_LogE(gLogger, "%s %d hState is invalid.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        if (AL_NONE == p->hSource)
        {
            break;
        }
        
        alSourcePause(p->hSource);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        p->hState = mmALSourceStatePaused;

        // Notify listener
        (*(pCallback->OnPaused))(pCallback->obj, p);
        
    } while(0);
}

MM_EXPORT_AL
int
mmALSource_Prepare(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader,
    const char*                                    pFileName)
{
    typedef int
    (*Prepare)(
        void*                                          p,
        struct mmALBufferLoader*                       pBufferLoader,
        const char*                                    pFileName);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((Prepare)(p->pMetadata->Prepare)))(p, pBufferLoader, pFileName);
}

MM_EXPORT_AL
void
mmALSource_Discard(
    struct mmALSource*                             p,
    struct mmALBufferLoader*                       pBufferLoader)
{
    typedef void
    (*Discard)(
        void*                                          p,
        struct mmALBufferLoader*                       pBufferLoader);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((Discard)(p->pMetadata->Discard)))(p, pBufferLoader);
}

MM_EXPORT_AL
int
mmALSource_GetIsStream(
    const struct mmALSource*                       p)
{
    typedef int
    (*GetIsStream)(
        const void*                                    p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetIsStream)(p->pMetadata->GetIsStream)))(p);
}

MM_EXPORT_AL
struct mmALAssets*
mmALSource_GetAssets(
    const struct mmALSource*                       p)
{
    typedef struct mmALAssets*
    (*GetAssets)(
        const void*                                    p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetAssets)(p->pMetadata->GetAssets)))(p);
}

MM_EXPORT_AL
void
mmALSource_SetLoop(
    struct mmALSource*                             p,
    int                                            hLoop)
{
    typedef void
    (*SetLoop)(
        void*                                          p,
        int                                            hLoop);

    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((SetLoop)(p->pMetadata->SetLoop)))(p, hLoop);
}

MM_EXPORT_AL
void
mmALSource_RewindImmediately(
    struct mmALSource*                             p)
{
    typedef void
    (*RewindImmediately)(
        void*                                          p);

    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((RewindImmediately)(p->pMetadata->RewindImmediately)))(p);
}

MM_EXPORT_AL
void
mmALSource_SetPlayPosition(
    struct mmALSource*                             p,
    double                                         hPosition)
{
    typedef void
    (*SetPlayPosition)(
        void*                                          p,
        double                                         hPosition);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((SetPlayPosition)(p->pMetadata->SetPlayPosition)))(p, hPosition);
}

MM_EXPORT_AL
double
mmALSource_GetPlayPosition(
    const struct mmALSource*                       p)
{
    typedef double
    (*GetPlayPosition)(
        const void*                                    p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    return (*((GetPlayPosition)(p->pMetadata->GetPlayPosition)))(p);
}

MM_EXPORT_AL
void
mmALSource_UpdatePlayPosition(
    struct mmALSource*                             p)
{
    typedef void
    (*UpdatePlayPosition)(
        void*                                          p);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((UpdatePlayPosition)(p->pMetadata->UpdatePlayPosition)))(p);
}

MM_EXPORT_AL
void
mmALSource_Update(
    struct mmALSource*                             p,
    double                                         interval)
{
    typedef void
    (*Update)(
        void*                                          p,
        double                                         interval);
    
    assert(NULL != p->pMetadata && "p->pMetadata is invalid.");
    (*((Update)(p->pMetadata->Update)))(p, interval);
}
