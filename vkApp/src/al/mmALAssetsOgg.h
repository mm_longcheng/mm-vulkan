/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmALAssetsOgg_h__
#define __mmALAssetsOgg_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStream.h"

#include "al/mmALAssets.h"

#include "ogg/ogg.h"
#include "vorbis/codec.h"
#include "vorbis/vorbisfile.h"

#include "core/mmPrefix.h"

struct mmFileContext;

MM_EXPORT_AL
int
mmALAssetsOggQueryInfo(
    OggVorbis_File*                                pVorbis,
    struct mmALAssetsInfo*                         pAssetsInfo);

MM_EXPORT_AL extern const struct mmALMetadataAssets mmALMetadataAssetsOgg;

struct mmALAssetsOgg
{
    struct mmALAssets Assets;
    struct mmStream Stream;
    OggVorbis_File  Vorbis;
};

MM_EXPORT_AL
void
mmALAssetsOgg_Init(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
void
mmALAssetsOgg_Destroy(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
void
mmALAssetsOgg_Produce(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
void
mmALAssetsOgg_Recycle(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
int
mmALAssetsOgg_Prepare(
    struct mmALAssetsOgg*                          p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFileName);

MM_EXPORT_AL
void
mmALAssetsOgg_Discard(
    struct mmALAssetsOgg*                          p,
    struct mmFileContext*                          pFileContext);

MM_EXPORT_AL
double
mmALAssetsOgg_GetTotalTime(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
int64_t
mmALAssetsOgg_GetTotalFrame(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
int
mmALAssetsOgg_GetIsSeekable(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
size_t
mmALAssetsOgg_Read(
    struct mmALAssetsOgg*                          p,
    void*                                          pBuffer,
    size_t                                         hLenght);

MM_EXPORT_AL
int
mmALAssetsOgg_SeekTime(
    struct mmALAssetsOgg*                          p,
    double                                         hPosition);

MM_EXPORT_AL
double
mmALAssetsOgg_TellTime(
    struct mmALAssetsOgg*                          p);

MM_EXPORT_AL
int
mmALAssetsOgg_SeekFrame(
    struct mmALAssetsOgg*                          p,
    int64_t                                        hPosition);

MM_EXPORT_AL
int64_t
mmALAssetsOgg_TellFrame(
    struct mmALAssetsOgg*                          p);

#include "core/mmSuffix.h"

#endif//__mmALAssetsOgg_h__
