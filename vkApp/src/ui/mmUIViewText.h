/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewText_h__
#define __mmUIViewText_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIText.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKAssets.h"

#include "nwsi/mmTextLayout.h"

#include "core/mmPrefix.h"

struct mmVKAssets;
struct mmVKCommandBuffer;

void
mmUIViewText_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIViewText_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorViewText;
extern const struct mmUIMetadata mmUIMetadataViewText;

/* struct mmEventUIViewArgs */
extern const char* mmUIViewText_EventTextChanged;

struct mmUIViewText
{
    struct mmUIView view;
    struct mmUIDrawable drawable;
    struct mmUIText text;
};

void
mmUIViewText_Init(
    struct mmUIViewText*                           p);

void
mmUIViewText_Destroy(
    struct mmUIViewText*                           p);

void
mmUIViewText_Produce(
    struct mmUIViewText*                           p);

void
mmUIViewText_Recycle(
    struct mmUIViewText*                           p);

struct mmUtf16String*
mmUIViewText_GetTextUtf16String(
    struct mmUIViewText*                           p);

void
mmUIViewText_SetRangeParameter(
    struct mmUIViewText*                           p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

void
mmUIViewText_SetRangeProperty(
    struct mmUIViewText*                           p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v);

void
mmUIViewText_SpannableReset(
    struct mmUIViewText*                           p);

void
mmUIViewText_UpdateColor(
    struct mmUIViewText*                           p);

void
mmUIViewText_UpdateText(
    struct mmUIViewText*                           p);

void
mmUIViewText_UpdateSize(
    struct mmUIViewText*                           p);

void
mmUIViewText_HandleTextChanged(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnPrepare(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnDiscard(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnUpdate(
    struct mmUIViewText*                           p,
    struct mmEventUpdate*                          content);

void
mmUIViewText_OnInvalidate(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnSized(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnChanged(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnDescriptorSetProduce(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnDescriptorSetRecycle(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnDescriptorSetUpdate(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnUpdatePushConstants(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnUpdatePipeline(
    struct mmUIViewText*                           p);

void
mmUIViewText_OnRecord(
    struct mmUIViewText*                           p,
    struct mmVKCommandBuffer*                      cmd);

#include "core/mmSuffix.h"

#endif//__mmUIViewText_h__
