/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIManager_h__
#define __mmUIManager_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "ui/mmUISchemeLoader.h"
#include "ui/mmUIViewFactory.h"
#include "ui/mmUIViewLoader.h"
#include "ui/mmUIViewContext.h"
#include "ui/mmUIViewDirector.h"

#include "core/mmPrefix.h"

struct mmEventMetrics;
struct mmEventKeypad;
struct mmEventCursor;
struct mmEventTouchs;
struct mmEventEditText;
struct mmEventEditString;

struct mmContextMaster;
struct mmSurfaceMaster;

struct mmVKSwapchain;
struct mmVKAssets;

struct mmVKCommandBuffer;

struct mmUIManager
{
    struct mmUISchemeLoader vSchemeLoader;
    struct mmUIViewFactory vViewFactory;
    struct mmUIViewLoader vViewLoader;
    struct mmUIViewContext vViewContext;
    struct mmUIViewDirector vViewDirector;
    struct mmString vViewMaster;

    struct mmContextMaster* pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster;

    struct mmVKSwapchain* pSwapchain;
    struct mmVKAssets* pAssets;
};

void
mmUIManager_Init(
    struct mmUIManager*                            p);

void
mmUIManager_Destroy(
    struct mmUIManager*                            p);

void
mmUIManager_SetContextMaster(
    struct mmUIManager*                            p,
    struct mmContextMaster*                        pContextMaster);

void
mmUIManager_SetSurfaceMaster(
    struct mmUIManager* p,
    struct mmSurfaceMaster*                        pSurfaceMaster);

void
mmUIManager_SetSwapchain(
    struct mmUIManager* p,
    struct mmVKSwapchain*                          pSwapchain);

void
mmUIManager_SetAssets(
    struct mmUIManager*                            p,
    struct mmVKAssets*                             pAssets);

void
mmUIManager_SetViewMaster(
    struct mmUIManager*                            p,
    const char*                                    pViewMaster);

void
mmUIManager_SetScheme(
    struct mmUIManager*                            p,
    const char*                                    pScheme);

void
mmUIManager_AddSchemePath(
    struct mmUIManager*                            p,
    const char*                                    path);

void
mmUIManager_RmvSchemePath(
    struct mmUIManager*                            p,
    const char*                                    path);

void
mmUIManager_ClearSchemePath(
    struct mmUIManager*                            p);

void
mmUIManager_AddModule(
    struct mmUIManager*                            p,
    const char*                                    name,
    void*                                          module);

void
mmUIManager_RmvModule(
    struct mmUIManager*                            p,
    const char*                                    name);

void
mmUIManager_ClearModule(
    struct mmUIManager*                            p);

void
mmUIManager_Prepare(
    struct mmUIManager*                            p);

void
mmUIManager_Discard(
    struct mmUIManager*                            p);

void
mmUIManager_OnSurfacePrepare(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content);

void
mmUIManager_OnSurfaceDiscard(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content);

void
mmUIManager_OnSurfaceChanged(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content);

void
mmUIManager_Resize(
    struct mmUIManager*                            p);

void
mmUIManager_Update(
    struct mmUIManager*                            p,
    struct mmEventUpdate*                          content);

void
mmUIManager_RecordBeforePass(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIManager_RecordBehindPass(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIManager_RecordBeforeView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIManager_RecordBasicsView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIManager_RecordBehindView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIManager_EnterBackground(
    struct mmUIManager*                            p);

void
mmUIManager_EnterForeground(
    struct mmUIManager*                            p);

void
mmUIManager_OnKeypadPressed(
    struct mmUIManager*                            p,
    struct mmEventKeypad*                          content);

void
mmUIManager_OnKeypadRelease(
    struct mmUIManager*                            p,
    struct mmEventKeypad*                          content);

void
mmUIManager_OnKeypadStatus(
    struct mmUIManager*                            p,
    struct mmEventKeypadStatus*                    content);

void
mmUIManager_OnCursorBegan(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content);

void
mmUIManager_OnCursorMoved(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content);

void
mmUIManager_OnCursorEnded(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content);

void
mmUIManager_OnCursorBreak(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content);

void
mmUIManager_OnCursorWheel(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content);

void
mmUIManager_OnTouchsBegan(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content);

void
mmUIManager_OnTouchsMoved(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content);

void
mmUIManager_OnTouchsEnded(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content);

void
mmUIManager_OnTouchsBreak(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content);

void
mmUIManager_OnGetText(
    struct mmUIManager*                            p,
    struct mmEventEditText*                        content);

void
mmUIManager_OnSetText(
    struct mmUIManager*                            p,
    struct mmEventEditText*                        content);

void
mmUIManager_OnGetTextEditRect(
    struct mmUIManager*                            p,
    double                                         rect[4]);

void
mmUIManager_OnInsertTextUtf16(
    struct mmUIManager*                            p,
    struct mmEventEditString*                      content);

void
mmUIManager_OnReplaceTextUtf16(
    struct mmUIManager*                            p,
    struct mmEventEditString*                      content);

void
mmUIManager_OnInjectCopy(
    struct mmUIManager*                            p);

void
mmUIManager_OnInjectCut(
    struct mmUIManager*                            p);

void
mmUIManager_OnInjectPaste(
    struct mmUIManager*                            p);

#include "core/mmSuffix.h"

#endif//__mmUIManager_h__
