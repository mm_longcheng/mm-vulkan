/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIImage.h"

#include "ui/mmUIQuad.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIPropertyHelper.h"
#include "ui/mmUIView.h"
#include "ui/mmUIViewContext.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"

static const VkDescriptorSetLayoutBinding dslb_bindings0[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
};

static const VkPushConstantRange dsl_pushs[] =
{
    {
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(struct mmUIDrawablePushConstant),
    },
};

const struct mmVectorValue mmUIImage_DSLB0 = mmVectorValue_Make(dslb_bindings0, VkDescriptorSetLayoutBinding);
const struct mmVectorValue mmUIImage_DSLP = mmVectorValue_Make(dsl_pushs, VkPushConstantRange);

static const VkVertexInputBindingDescription vibd[] =
{
    {
        .binding = 0,
        .stride = sizeof(struct mmUIVertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    },
};
static const VkVertexInputAttributeDescription viad[] =
{
    {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, position),
    },
    {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, texcoord),
    },
    {
        .location = 2,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = mmOffsetof(struct mmUIVertex, color),
    },
};

const struct mmVKPipelineVertexInputInfo mmUIImage_PVII =
{
    mmVectorValue_Make(vibd, VkVertexInputBindingDescription),
    mmVectorValue_Make(viad, VkVertexInputAttributeDescription),
};

void
mmUIImage_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIImage";

    mmPropertyDefineMutable(propertys,
        "Image.ImagePath", offset, struct mmUIImage,
        hImagePath, String, "",
        NULL,
        NULL,
        cOrigin,
        "Image ImagePath string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Image.SheetName", offset, struct mmUIImage,
        hSheetName, String, "",
        NULL,
        NULL,
        cOrigin,
        "Image SheetName string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Image.FrameName", offset, struct mmUIImage,
        hFrameName, String, "",
        NULL,
        NULL,
        cOrigin,
        "Image FrameName string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Image.Frame", offset, struct mmUIImage,
        hFrame, SIntVec4, "(0, 0, -1, -1)",
        NULL,
        NULL,
        cOrigin,
        "Image Frame SInt[4] -1 will use image original size default (0, 0, -1, -1).");

    mmPropertyDefineMutable(propertys,
        "Image.Rotated", offset, struct mmUIImage,
        hRotated, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Image Rotated SInt default (0).");

    mmPropertyDefineMutable(propertys,
        "Image.Filter", offset, struct mmUIImage,
        hFilter, UInt8Vec2, "(0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Image Filter UInt8[2] [minFilter, magFilter] default (0, 0).");

    mmPropertyDefineMutable(propertys,
        "Image.MipmapMode", offset, struct mmUIImage,
        hMipmapMode, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Image MipmapMode UInt8 default 0.");

    mmPropertyDefineMutable(propertys,
        "Image.AddressMode", offset, struct mmUIImage,
        hAddressMode, UInt8Vec3, "(2, 2, 2)",
        NULL,
        NULL,
        cOrigin,
        "Image AddressMode UInt8[3] default (2, 2, 2).");

    mmPropertyDefineMutable(propertys,
        "Image.BorderColor", offset, struct mmUIImage,
        hBorderColor, SInt, "4",
        NULL,
        NULL,
        cOrigin,
        "Image BorderColor UInt8 default 4.");
}

void
mmUIImage_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmPropertyRemove(propertys, "Image.ImagePath");
    mmPropertyRemove(propertys, "Image.SheetName");
    mmPropertyRemove(propertys, "Image.FrameName");
    mmPropertyRemove(propertys, "Image.Frame");
    mmPropertyRemove(propertys, "Image.Rotated");
    mmPropertyRemove(propertys, "Image.Filter");
    mmPropertyRemove(propertys, "Image.MipmapMode");
    mmPropertyRemove(propertys, "Image.AddressMode");
    mmPropertyRemove(propertys, "Image.BorderColor");
}

void
mmUIImage_Init(
    struct mmUIImage*                              p)
{
    p->pImageInfo = NULL;
    p->pSamplerInfo = NULL;

    p->pSheetImage = NULL;
    p->pSheetFrame = NULL;

    mmString_Init(&p->hImagePath);
    mmString_Init(&p->hSheetName);
    mmString_Init(&p->hFrameName);

    mmMemset(p->hFrame, 0, sizeof(p->hFrame));
    p->hRotated = 0;

    mmMemset(p->hFilter, VK_FILTER_NEAREST, sizeof(p->hFilter));
    p->hMipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    mmMemset(p->hAddressMode, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, sizeof(p->hAddressMode));
    p->hBorderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
}

void
mmUIImage_Destroy(
    struct mmUIImage*                              p)
{
    p->hBorderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    mmMemset(p->hAddressMode, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, sizeof(p->hAddressMode));
    p->hMipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    mmMemset(p->hFilter, VK_FILTER_NEAREST, sizeof(p->hFilter));

    p->hRotated = 0;
    mmMemset(p->hFrame, 0, sizeof(p->hFrame));

    mmString_Destroy(&p->hFrameName);
    mmString_Destroy(&p->hSheetName);
    mmString_Destroy(&p->hImagePath);

    p->pSheetFrame = NULL;
    p->pSheetImage = NULL;

    p->pSamplerInfo = NULL;
    p->pImageInfo = NULL;
}

VkResult
mmUIImage_PrepareFromImageInfo(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable,
    struct mmVKAssets*                             pAssets,
    struct mmVKImageInfo*                          pImageInfo,
    const int                                      frame[4],
    int                                            rotated)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        size_t imageSize[2];

        struct mmVKSamplerCreateInfo hSamplerInfo = mmVKSamplerNearestEdge;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pSamplerPool = &pAssets->vSamplerPool;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pImageInfo = pImageInfo;
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);

        imageSize[0] = p->pImageInfo->vImage.width;
        imageSize[1] = p->pImageInfo->vImage.height;
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, rotated);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hSamplerInfo.addressModeU = p->hAddressMode[0];
        hSamplerInfo.addressModeV = p->hAddressMode[1];
        hSamplerInfo.addressModeW = p->hAddressMode[2];
        p->pSamplerInfo = mmVKSamplerPool_Add(pSamplerPool, &hSamplerInfo);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

        err = mmUIDrawable_PreparePipeline(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePool(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIImage_PrepareFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmString hImageName;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    mmString_Init(&hImageName);

    do
    {
        struct mmVKSamplerCreateInfo hSamplerInfo;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKImageLoader* pImageLoader = NULL;
        struct mmVKSheetLoader* pSheetLoader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;
        const char* pImageName = "";

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pImageLoader = &pAssets->vImageLoader;
        pSheetLoader = &pAssets->vSheetLoader;
        pSamplerPool = &pAssets->vSamplerPool;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pSheetImage = mmVKSheetLoader_LoadFromFile(pSheetLoader, mmString_CStr(&p->hSheetName));
        if (mmVKSheetImage_Invalid(p->pSheetImage))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSheetImage is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSheetImage = NULL;
            break;
        }
        mmVKSheetImage_Increase(p->pSheetImage);

        p->pSheetFrame = mmVKSheetImage_GetFrame(p->pSheetImage, mmString_CStr(&p->hFrameName));
        if (mmVKSheetFrame_Invalid(p->pSheetFrame))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSheetFrame is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSheetFrame = NULL;
            break;
        }
        mmVKSheetFrame_Increase(p->pSheetFrame);

        mmVKSheetImage_GetImagePath(p->pSheetImage, &hImageName);

        pImageName = mmString_CStr(&hImageName);
        p->pImageInfo = mmVKImageLoader_LoadFromFilePriority(pImageLoader, pImageName, ".ktx2");
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);

        mmUIImageFrameFromSheet(&pDrawable->vIframe, p->pSheetFrame);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hSamplerInfo.minFilter = p->hFilter[0];
        hSamplerInfo.magFilter = p->hFilter[1];
        hSamplerInfo.mipmapMode = p->hMipmapMode;
        hSamplerInfo.addressModeU = p->hAddressMode[0];
        hSamplerInfo.addressModeV = p->hAddressMode[1];
        hSamplerInfo.addressModeW = p->hAddressMode[2];
        hSamplerInfo.borderColor = p->hBorderColor;
        p->pSamplerInfo = mmVKSamplerPool_Add(pSamplerPool, &hSamplerInfo);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

        err = mmUIDrawable_PreparePipeline(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePool(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    mmString_Destroy(&hImageName);

    return err;
}

VkResult
mmUIImage_PrepareFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int frame[4];
        size_t imageSize[2];

        struct mmVKSamplerCreateInfo hSamplerInfo;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKImageLoader* pImageLoader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;
        const char* pImagePath = "";

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pImageLoader = &pAssets->vImageLoader;
        pSamplerPool = &pAssets->vSamplerPool;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pImagePath = mmString_CStr(&p->hImagePath);
        p->pImageInfo = mmVKImageLoader_LoadFromFilePriority(pImageLoader, pImagePath, ".ktx2");
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);

        imageSize[0] = p->pImageInfo->vImage.width;
        imageSize[1] = p->pImageInfo->vImage.height;
        frame[0] = (-1 == p->hFrame[0]) ?                 0 : p->hFrame[0];
        frame[1] = (-1 == p->hFrame[1]) ?                 0 : p->hFrame[1];
        frame[2] = (-1 == p->hFrame[2]) ? (int)imageSize[0] : p->hFrame[2];
        frame[3] = (-1 == p->hFrame[3]) ? (int)imageSize[1] : p->hFrame[3];
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, p->hRotated);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hSamplerInfo.minFilter = p->hFilter[0];
        hSamplerInfo.magFilter = p->hFilter[1];
        hSamplerInfo.mipmapMode = p->hMipmapMode;
        hSamplerInfo.addressModeU = p->hAddressMode[0];
        hSamplerInfo.addressModeV = p->hAddressMode[1];
        hSamplerInfo.addressModeW = p->hAddressMode[2];
        hSamplerInfo.borderColor = p->hBorderColor;
        p->pSamplerInfo = mmVKSamplerPool_Add(pSamplerPool, &hSamplerInfo);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

        err = mmUIDrawable_PreparePipeline(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePool(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIImage_Prepare(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    if (mmString_Empty(&p->hImagePath))
    {
        return mmUIImage_PrepareFromSheetName(p, pView, pDrawable);
    }
    else
    {
        return mmUIImage_PrepareFromImagePath(p, pView, pDrawable);
    }
}

void
mmUIImage_Discard(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmUIDrawable_DiscardPool(pDrawable);

        mmUIDrawable_DiscardPipeline(pDrawable);

        mmVKSamplerInfo_Decrease(p->pSamplerInfo);
        p->pSamplerInfo = NULL;

        mmUIDrawable_DiscardBuffer(pDrawable, pAssets->pUploader);

        mmVKImageInfo_Decrease(p->pImageInfo);
        p->pImageInfo = NULL;

        mmVKSheetFrame_Decrease(p->pSheetFrame);
        p->pSheetFrame = NULL;

        mmVKSheetImage_Decrease(p->pSheetImage);
        p->pSheetImage = NULL;
    } while (0);
}

VkResult
mmUIImage_UpdateFrameFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmString hImageName;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    mmString_Init(&hImageName);

    do
    {
        struct mmVKUploader* pUploader = NULL;
        struct mmVKImageLoader* pImageLoader = NULL;
        struct mmVKSheetLoader* pSheetLoader = NULL;
        const char* pImageName = "";

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pSheetFrame)
        {
            // image is not ready, not need update frame.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pImageLoader = &pAssets->vImageLoader;
        pSheetLoader = &pAssets->vSheetLoader;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*- Discard -*/
        mmUIDrawable_DiscardBuffer(pDrawable, pAssets->pUploader);

        mmVKImageInfo_Decrease(p->pImageInfo);
        p->pImageInfo = NULL;

        mmVKSheetFrame_Decrease(p->pSheetFrame);
        p->pSheetFrame = NULL;

        mmVKSheetImage_Decrease(p->pSheetImage);
        p->pSheetImage = NULL;

        /*- Prepare -*/
        p->pSheetImage = mmVKSheetLoader_LoadFromFile(pSheetLoader, mmString_CStr(&p->hSheetName));
        if (mmVKSheetImage_Invalid(p->pSheetImage))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSheetImage is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSheetImage = NULL;
            break;
        }
        mmVKSheetImage_Increase(p->pSheetImage);

        p->pSheetFrame = mmVKSheetImage_GetFrame(p->pSheetImage, mmString_CStr(&p->hFrameName));
        if (mmVKSheetFrame_Invalid(p->pSheetFrame))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSheetFrame is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSheetFrame = NULL;
            break;
        }
        mmVKSheetFrame_Increase(p->pSheetFrame);

        mmVKSheetImage_GetImagePath(p->pSheetImage, &hImageName);

        pImageName = mmString_CStr(&hImageName);
        p->pImageInfo = mmVKImageLoader_LoadFromFilePriority(pImageLoader, pImageName, ".ktx2");
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);

        mmUIImageFrameFromSheet(&pDrawable->vIframe, p->pSheetFrame);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    mmString_Destroy(&hImageName);

    return err;
}

VkResult
mmUIImage_UpdateFrameFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmString hImageName;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    mmString_Init(&hImageName);

    do
    {
        int frame[4];
        size_t imageSize[2];

        struct mmVKUploader* pUploader = NULL;
        struct mmVKImageLoader* pImageLoader = NULL;
        struct mmVKSheetLoader* pSheetLoader = NULL;
        const char* pImagePath = "";

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            // image is not ready, not need update frame.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pImageLoader = &pAssets->vImageLoader;
        pSheetLoader = &pAssets->vSheetLoader;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*- Discard -*/
        mmUIDrawable_DiscardBuffer(pDrawable, pAssets->pUploader);

        mmVKImageInfo_Decrease(p->pImageInfo);
        p->pImageInfo = NULL;

        /*- Prepare -*/
        pImagePath = mmString_CStr(&p->hImagePath);
        p->pImageInfo = mmVKImageLoader_LoadFromFilePriority(pImageLoader, pImagePath, ".ktx2");
        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageInfo = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageInfo);

        imageSize[0] = p->pImageInfo->vImage.width;
        imageSize[1] = p->pImageInfo->vImage.height;
        frame[0] = (-1 == p->hFrame[0]) ?                 0 : p->hFrame[0];
        frame[1] = (-1 == p->hFrame[1]) ?                 0 : p->hFrame[1];
        frame[2] = (-1 == p->hFrame[2]) ? (int)imageSize[0] : p->hFrame[2];
        frame[3] = (-1 == p->hFrame[3]) ? (int)imageSize[1] : p->hFrame[3];
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, p->hRotated);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    mmString_Destroy(&hImageName);

    return err;
}

VkResult
mmUIImage_UpdateFrame(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    if (mmString_Empty(&p->hImagePath))
    {
        return mmUIImage_UpdateFrameFromSheetName(p, pView, pDrawable);
    }
    else
    {
        return mmUIImage_UpdateFrameFromImagePath(p, pView, pDrawable);
    }
}

VkResult
mmUIImage_UpdateSizeFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pSheetFrame)
        {
            // image is not ready, not need update size.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*- Update -*/ 
        mmUIImageFrameFromSheet(&pDrawable->vIframe, p->pSheetFrame);
        err = mmUIDrawable_UpdateBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIImage_UpdateSizeFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        int frame[4];
        size_t imageSize[2];

        struct mmVKUploader* pUploader = NULL;

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKImageInfo_Invalid(p->pImageInfo))
        {
            // image is not ready, not need update size.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*- Update -*/
        imageSize[0] = p->pImageInfo->vImage.width;
        imageSize[1] = p->pImageInfo->vImage.height;
        frame[0] = (-1 == p->hFrame[0]) ?                 0 : p->hFrame[0];
        frame[1] = (-1 == p->hFrame[1]) ?                 0 : p->hFrame[1];
        frame[2] = (-1 == p->hFrame[2]) ? (int)imageSize[0] : p->hFrame[2];
        frame[3] = (-1 == p->hFrame[3]) ? (int)imageSize[1] : p->hFrame[3];
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, p->hRotated);
        err = mmUIDrawable_UpdateBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIImage_UpdateSize(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    if (mmString_Empty(&p->hImagePath))
    {
        return mmUIImage_UpdateSizeFromSheetName(p, pView, pDrawable);
    }
    else
    {
        return mmUIImage_UpdateSizeFromImagePath(p, pView, pDrawable);
    }
}

