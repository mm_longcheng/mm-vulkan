/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIView_h__
#define __mmUIView_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmPropertySet.h"
#include "core/mmEventSet.h"
#include "core/mmMetadata.h"

#include "container/mmListVpt.h"

#include "ui/mmUICube.h"
#include "ui/mmUIAspect.h"
#include "ui/mmUIWidget.h"
#include "ui/mmUIResponder.h"

#include "core/mmPrefix.h"

struct mmEventUpdate;
struct mmEventKeypad;
struct mmEventCursor;
struct mmEventTouchs;
struct mmEventEditText;
struct mmEventEditString;
struct mmEventKeypadStatus;
struct mmEventMetrics;

struct mmEventUpdateArgs;
struct mmEventKeypadArgs;
struct mmEventCursorArgs;
struct mmEventTouchsArgs;
struct mmEventKeypadStatusArgs;
struct mmEventMetricsArgs;

struct mmEventUIClickedArgs;
struct mmEventUIFocusArgs;

struct mmUIView;
struct mmUIViewContext;

struct mmVKCommandBuffer;

enum mmUIAlignment
{
    mmUIAlignmentMinimum = 0,
    mmUIAlignmentMiddled = 1,
    mmUIAlignmentMaximum = 2,
};

const char*
mmUIAlignment_EncodeEnumerate(
    int                                            v);

int
mmUIAlignment_DecodeEnumerate(
    const char*                                    w);

// Hit filter.
enum mmUIFilter
{
    mmUIFilterBoundary = 1 << 0, // ignore Boundary check.
    mmUIFilterChildren = 1 << 1, // ignore Children check.
    mmUIFilterObstruct = 1 << 2, // ignore Obstruct check.
};

struct mmUIMetadata
{
    // type allocator.
    const struct mmMetaAllocator* Allocator;
    
    // Widget Operable event handler.
    const struct mmUIWidgetOperable* WidgetOperable;
    // Widget Variable event handler.
    const struct mmUIWidgetVariable* WidgetVariable;
    // Widget Drawable event handler.
    const struct mmUIWidgetDrawable* WidgetDrawable;

    // Responder Cursor event handler.
    const struct mmUIResponderCursor* ResponderCursor;
    // Responder Keypad event handler.
    const struct mmUIResponderKeypad* ResponderKeypad;
    // Responder Editor event handler.
    const struct mmUIResponderEditor* ResponderEditor;
    // Responder Touchs event handler.
    const struct mmUIResponderTouchs* ResponderTouchs;
};

void
mmUIView_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIView_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorView;
extern const struct mmUIMetadata mmUIMetadataView;

/* struct mmEventKeypadArgs */
extern const char* mmUIView_EventKeypadPressed;
extern const char* mmUIView_EventKeypadRelease;
/* struct mmEventKeypadStatusArgs */
extern const char* mmUIView_EventKeypadStatus;

/* struct mmEventCursorArgs */
extern const char* mmUIView_EventCursorBegan;
extern const char* mmUIView_EventCursorMoved;
extern const char* mmUIView_EventCursorEnded;
extern const char* mmUIView_EventCursorBreak;
extern const char* mmUIView_EventCursorWheel;

/* struct mmEventTouchsArgs */
extern const char* mmUIView_EventTouchsBegan;
extern const char* mmUIView_EventTouchsMoved;
extern const char* mmUIView_EventTouchsEnded;
extern const char* mmUIView_EventTouchsBreak;

/* struct mmEventUIViewArgs */
extern const char* mmUIView_EventSized;
extern const char* mmUIView_EventGainActive;
extern const char* mmUIView_EventLossActive;

/* struct mmEventUpdateArgs */
extern const char* mmUIView_EventUpdate;

/* struct mmEventArgs */
extern const char* mmUIView_EventInvalidate;

/* struct mmEventUIClickedArgs */
extern const char* mmUIView_EventClicked;

/* struct mmEventUIFocusArgs */
extern const char* mmUIView_EventEnterSurface;
extern const char* mmUIView_EventLeaveSurface;
extern const char* mmUIView_EventEnterArea;
extern const char* mmUIView_EventLeaveArea;

struct mmUIView
{
    // name
    struct mmString name;

    // children
    struct mmListVpt children;

    // propertys
    struct mmPropertySet propertys;
    
    // events
    struct mmEventSet events;
    
    // metadata.
    const struct mmUIMetadata* metadata;

    // context handle
    struct mmUIViewContext* context;

    // parent node
    struct mmUIView* parent;

    // [w, h, d]
    struct mmUIDim size[3];

    // [x, y, z]
    struct mmUIDim position[3];

    // [x, y, z]
    float scale[3];

    // [x, y, z, w]
    float quaternion[4];
    
    // opacity
    float opacity[4];

    // anchor point. 
    // [x, y, z] normalise [0, 1].
    float anchor[3];

    // cache world value.
    float worldTransform[4][4];
    float worldSize[3];
    float worldOpacity[4];

    // aspect ratio, default is 0.0f.
    float aspectRatio;

    // enum mmUIAspectMode
    // aspect mode, default is mmUIAspectModeIgnore.
    mmUInt8_t aspectMode;

    // enum mmUIAlignment
    // Alignment, default is mmUIAlignmentMinimum.
    // [x, y, z]
    mmUInt8_t align[3];

    // view visible.
    mmUInt8_t visible;

    // flag Event Filter. default is 0.
    mmUInt8_t filter;

    // flag clipping. default is 0.
    mmUInt8_t clipping;

    // valid world cache.
    mmUInt8_t validWorldTransform;
    mmUInt8_t validWorldSize;
    mmUInt8_t validWorldOpacity;

    // flag Active.
    mmUInt8_t active;

    // view node depth.
    mmUInt32_t depth;

    // identity.
    mmUInt32_t identity;
};

void 
mmUIView_Init(
    struct mmUIView*                               p);

void 
mmUIView_Destroy(
    struct mmUIView*                               p);

void
mmUIView_Produce(
    struct mmUIView*                               p);

void
mmUIView_Recycle(
    struct mmUIView*                               p);

void
mmUIView_SetContext(
    struct mmUIView*                               p,
    struct mmUIViewContext*                        context);

void
mmUIView_GetContextCube(
    const struct mmUIView*                         p,
    float                                          hCube[3]);

float
mmUIView_GetContextDensity(
    const struct mmUIView*                         p);

void
mmUIView_GetSchemeColor(
    const struct mmUIView*                         p,
    const struct mmString*                         pValue,
    float                                          hColor[4]);

void 
mmUIView_GetLocalTransform(
    struct mmUIView*                               p, 
    float                                          hLocalTransform[4][4]);

void 
mmUIView_GetWorldTransform(
    struct mmUIView*                               p, 
    float                                          hWorldTransform[4][4]);

void 
mmUIView_GetWorldSize(
    struct mmUIView*                               p, 
    float                                          hWorldSize[3]);

void 
mmUIView_GetWorldOpacity(
    struct mmUIView*                               p, 
    float                                          hWorldOpacity[4]);

void
mmUIView_GetWorldRect(
    struct mmUIView*                               p,
    float                                          hWorldRect[4]);

void 
mmUIView_GetLocalSize(
    struct mmUIView*                               p, 
    float                                          hLocalSize[3]);

void 
mmUIView_GetWorldPosition(
    struct mmUIView*                               p, 
    float                                          position[3]);

void 
mmUIView_GetWorldCubeScalar(
    struct mmUIView*                               p, 
    struct mmUICubeScalar*                         pCube);

void 
mmUIView_AddChild(
    struct mmUIView*                               p, 
    struct mmUIView*                               child);

void 
mmUIView_RmvChild(
    struct mmUIView*                               p, 
    struct mmUIView*                               child);

struct mmUIView*
mmUIView_GetChildByName(
    struct mmUIView*                               p,
    const struct mmString*                         name);

struct mmUIView*
mmUIView_GetChildByCStr(
    struct mmUIView*                               p,
    const char*                                    name);

struct mmUIView*
mmUIView_GetChildAssert(
    struct mmUIView*                               p,
    const char*                                    name);

void*
mmUIView_GetModuleAssert(
    const struct mmUIView*                         p,
    const char*                                    name);

void 
mmUIView_SetValueMember(
    struct mmUIView*                               p, 
    const char*                                    pName, 
    const void*                                    pValue);

void 
mmUIView_GetValueMember(
    const struct mmUIView*                         p, 
    const char*                                    pName, 
    void*                                          pValue);

void 
mmUIView_SetValueDirect(
    struct mmUIView*                               p, 
    const char*                                    pName, 
    const void*                                    pValue);

void 
mmUIView_GetValueDirect(
    const struct mmUIView*                         p, 
    const char*                                    pName, 
    void*                                          pValue);

void 
mmUIView_SetValueString(
    struct mmUIView*                               p, 
    const char*                                    pName, 
    const struct mmString*                         pValue);

void 
mmUIView_GetValueString(
    const struct mmUIView*                         p, 
    const char*                                    pName, 
    struct mmString*                               pValue);

void
mmUIView_SetPointerDirect(
    const struct mmUIView*                         p,
    const char*                                    pName,
    const void*                                    ptr);

void*
mmUIView_GetPointerDirect(
    const struct mmUIView*                         p,
    const char*                                    pName);

void 
mmUIView_SetValueCStr(
    struct mmUIView*                               p, 
    const char*                                    pName, 
    const char*                                    pValue);

void
mmUIView_SetName(
    struct mmUIView*                               p,
    const struct mmString*                         name);

void
mmUIView_GetName(
    const struct mmUIView*                         p,
    struct mmString*                               name);

void 
mmUIView_SetAlignment(
    struct mmUIView*                               p, 
    const mmUInt8_t                                align[3]);

void 
mmUIView_GetAlignment(
    const struct mmUIView*                         p, 
    mmUInt8_t                                      align[3]);

void 
mmUIView_SetAspectMode(
    struct mmUIView*                               p, 
    mmUInt8_t                                      aspectMode);

mmUInt8_t 
mmUIView_GetAspectMode(
    const struct mmUIView*                         p);

void 
mmUIView_SetAspectRatio(
    struct mmUIView*                               p, 
    float                                          aspectRatio);

float 
mmUIView_GetAspectRatio(
    const struct mmUIView*                         p);

void 
mmUIView_SetAnchor(
    struct mmUIView*                               p, 
    const float                                    anchor[3]);

void 
mmUIView_GetAnchor(
    const struct mmUIView*                         p, 
    float                                          anchor[3]);

void 
mmUIView_SetSize(
    struct mmUIView*                               p, 
    const struct mmUIDim                           size[3]);

void 
mmUIView_GetSize(
    const struct mmUIView*                         p, 
    struct mmUIDim                                 size[3]);

void 
mmUIView_SetPosition(
    struct mmUIView*                               p, 
    const struct mmUIDim                           position[3]);

void 
mmUIView_GetPosition(
    const struct mmUIView*                         p, 
    struct mmUIDim                                 position[3]);

void 
mmUIView_SetScale(
    struct mmUIView*                               p, 
    const float                                    scale[3]);

void 
mmUIView_GetScale(
    const struct mmUIView*                         p, 
    float                                          scale[3]);

void 
mmUIView_SetQuaternion(
    struct mmUIView*                               p, 
    const float                                    quaternion[4]);

void 
mmUIView_GetQuaternion(
    const struct mmUIView*                         p, 
    float                                          quaternion[4]);

void 
mmUIView_SetOpacity(
    struct mmUIView*                               p, 
    const float                                    opacity[4]);

void 
mmUIView_GetOpacity(
    const struct mmUIView*                         p, 
    float                                          opacity[4]);

void 
mmUIView_SetVisible(
    struct mmUIView*                               p, 
    int                                            visible);

int 
mmUIView_GetVisible(
    const struct mmUIView*                         p);

void 
mmUIView_SetFilter(
    struct mmUIView*                               p, 
    int                                            filter);

int 
mmUIView_GetFilter(
    const struct mmUIView*                         p);

void 
mmUIView_SetClipping(
    struct mmUIView*                               p, 
    int                                            clipping);

int 
mmUIView_GetClipping(
    const struct mmUIView*                         p);

void
mmUIView_SetIdentity(
    struct mmUIView*                               p,
    mmUInt32_t                                     identity);

mmUInt32_t
mmUIView_GetIdentity(
    const struct mmUIView*                         p);

void
mmUIView_HandleSized(
    struct mmUIView*                               p);

void 
mmUIView_MarkCacheInvalidWhole(
    struct mmUIView*                               p);

void 
mmUIView_MarkCacheInvalidWorldTransform(
    struct mmUIView*                               p);

void 
mmUIView_MarkCacheInvalidWorldSize(
    struct mmUIView*                               p);

void 
mmUIView_MarkCacheInvalidWorldOpacity(
    struct mmUIView*                               p);

void
mmUIView_Prepare(
    struct mmUIView*                               p);

void
mmUIView_Discard(
    struct mmUIView*                               p);

void
mmUIView_PrepareChildren(
    struct mmUIView*                               p);

void
mmUIView_DiscardChildren(
    struct mmUIView*                               p);

void
mmUIView_Update(
    struct mmUIView*                               p,
    struct mmEventUpdate*                          content);

void
mmUIView_Invalidate(
    struct mmUIView*                               p);

void
mmUIView_Sized(
    struct mmUIView*                               p);

void
mmUIView_Changed(
    struct mmUIView*                               p);

int
mmUIView_GetIsWidgetDrawable(
    const struct mmUIView*                         p);

int
mmUIView_GetIsResponderCursor(
    const struct mmUIView*                         p);

int
mmUIView_GetIsResponderEditor(
    const struct mmUIView*                         p);

void
mmUIView_GetLocalPoint(
    struct mmUIView*                               p,
    const float                                    point[2],
    float                                          local[2]);

int 
mmUIView_GetIsHit(
    struct mmUIView*                               p, 
    const float                                    point[2]);

struct mmUIView* 
mmUIView_GetHitView(
    struct mmUIView*                               p, 
    const float                                    point[2]);

void
mmUIView_OnHandleSized(
    struct mmUIView*                               p);

void
mmUIView_OnRecordCommand(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIView_OnRecordDrawable(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIView_OnRecordChildren(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIView_OnEventParentArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args);

void
mmUIView_OnEventRTraversalArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args);

void
mmUIView_OnEventPTraversalArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args);

void
mmUIView_OnEventContainArgs(
    struct mmUIView*                               p,
    const struct mmUIView*                         ancestor,
    const void*                                    func,
    struct mmEventArgs*                            args);

void
mmUIView_OnUpdateArgs(
    struct mmUIView*                               p,
    struct mmEventUpdateArgs*                      args);

void
mmUIView_OnInvalidateArgs(
    struct mmUIView*                               p,
    struct mmEventArgs*                            args);

void 
mmUIView_OnKeypadPressedArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadArgs*                      args);

void 
mmUIView_OnKeypadReleaseArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadArgs*                      args);

void
mmUIView_OnKeypadStatusArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadStatusArgs*                args);

void
mmUIView_OnCursorBeganArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args);

void
mmUIView_OnCursorMovedArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args);

void
mmUIView_OnCursorEndedArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args);

void
mmUIView_OnCursorBreakArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args);

void
mmUIView_OnCursorWheelArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args);

void
mmUIView_OnTouchsBeganArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args);

void
mmUIView_OnTouchsMovedArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args);

void
mmUIView_OnTouchsEndedArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args);

void
mmUIView_OnTouchsBreakArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args);

void
mmUIView_OnClickedArgs(
    struct mmUIView*                               p,
    struct mmEventUIClickedArgs*                   args);

void
mmUIView_OnEnterSurfaceArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args);

void
mmUIView_OnLeaveSurfaceArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args);

void
mmUIView_OnEnterAreaArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args);

void
mmUIView_OnLeaveAreaArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args);

void
mmUIView_OnAspect(
    struct mmUIView*                               p,
    const float                                    rect[4],
    const float                                    size[2],
    mmUInt8_t                                      mode,
    float                                          ratio);

const struct mmUIView*
mmUIView_GetAncestor(
    const struct mmUIView*                         p,
    const struct mmUIView*                         q);

void
mmUIView_SetBranchActive(
    struct mmUIView*                               p,
    const struct mmUIView*                         ancestor,
    mmUInt8_t                                      active);

void
mmUIView_PrepareViewFromFile(
    struct mmUIView*                               p,
    const char*                                    path);

void
mmUIView_DiscardView(
    struct mmUIView*                               p);

void
mmUIView_OnPrepare(
    struct mmUIView*                               p);

void
mmUIView_OnDiscard(
    struct mmUIView*                               p);

void
mmUIView_OnUpdate(
    struct mmUIView*                               p,
    struct mmEventUpdate*                          content);

void
mmUIView_OnInvalidate(
    struct mmUIView*                               p);

void
mmUIView_OnSized(
    struct mmUIView*                               p);

void
mmUIView_OnChanged(
    struct mmUIView*                               p);

void
mmUIView_OnDescriptorSetProduce(
    struct mmUIView*                               p);

void
mmUIView_OnDescriptorSetRecycle(
    struct mmUIView*                               p);

void
mmUIView_OnDescriptorSetUpdate(
    struct mmUIView*                               p);

void
mmUIView_OnUpdatePushConstants(
    struct mmUIView*                               p);

void
mmUIView_OnUpdatePipeline(
    struct mmUIView*                               p);

void
mmUIView_OnRecord(
    struct mmUIView*                               p,
    void*                                          cmd);

void
mmUIView_OnKeypadPressed(
    struct mmUIView*                               p,
    struct mmEventKeypad*                          content);

void
mmUIView_OnKeypadRelease(
    struct mmUIView*                               p,
    struct mmEventKeypad*                          content);

void
mmUIView_OnKeypadStatus(
    struct mmUIView*                               p,
    struct mmEventKeypadStatus*                    content);

void
mmUIView_OnCursorBegan(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content);

void
mmUIView_OnCursorMoved(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content);

void
mmUIView_OnCursorEnded(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content);

void
mmUIView_OnCursorBreak(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content);

void
mmUIView_OnCursorWheel(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content);

void
mmUIView_OnTouchsBegan(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content);

void
mmUIView_OnTouchsMoved(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content);

void
mmUIView_OnTouchsEnded(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content);

void
mmUIView_OnTouchsBreak(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content);

void
mmUIView_OnGetText(
    struct mmUIView*                               p,
    struct mmEventEditText*                        content);

void
mmUIView_OnSetText(
    struct mmUIView*                               p,
    struct mmEventEditText*                        content);


void
mmUIView_OnGetTextEditRect(
    struct mmUIView*                               p,
    double                                         rect[4]);

void
mmUIView_OnInsertTextUtf16(
    struct mmUIView*                               p,
    struct mmEventEditString*                      content);

void
mmUIView_OnReplaceTextUtf16(
    struct mmUIView*                               p,
    struct mmEventEditString*                      content);

void
mmUIView_OnInjectCopy(
    struct mmUIView*                               p);

void
mmUIView_OnInjectCut(
    struct mmUIView*                               p);

void
mmUIView_OnInjectPaste(
    struct mmUIView*                               p);

void
mmUIView_OnFocusCapture(
    struct mmUIView*                               p);

void
mmUIView_OnFocusRelease(
    struct mmUIView*                               p);

#include "core/mmSuffix.h"

#endif//__mmUIView_h__
