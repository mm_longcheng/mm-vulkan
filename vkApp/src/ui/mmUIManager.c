/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIManager.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmSurfaceMaster.h"

#include "vk/mmVKSwapchain.h"
#include "vk/mmVKAssets.h"

#include "ui/mmUIImage.h"

void
mmUIManager_Init(
    struct mmUIManager*                            p)
{
    mmUISchemeLoader_Init(&p->vSchemeLoader);
    mmUIViewFactory_Init(&p->vViewFactory);
    mmUIViewLoader_Init(&p->vViewLoader);
    mmUIViewContext_Init(&p->vViewContext);
    mmUIViewDirector_Init(&p->vViewDirector);
    mmString_Init(&p->vViewMaster);
    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;
    p->pSwapchain = NULL;
    p->pAssets = NULL;
}

void
mmUIManager_Destroy(
    struct mmUIManager*                            p)
{
    p->pAssets = NULL;
    p->pSwapchain = NULL;
    p->pSurfaceMaster = NULL;
    p->pContextMaster = NULL;
    mmString_Destroy(&p->vViewMaster);
    mmUIViewDirector_Destroy(&p->vViewDirector);
    mmUIViewContext_Destroy(&p->vViewContext);
    mmUIViewLoader_Destroy(&p->vViewLoader);
    mmUIViewFactory_Destroy(&p->vViewFactory);
    mmUISchemeLoader_Destroy(&p->vSchemeLoader);
}

void
mmUIManager_SetContextMaster(
    struct mmUIManager*                            p,
    struct mmContextMaster*                        pContextMaster)
{
    p->pContextMaster = pContextMaster;
}

void
mmUIManager_SetSurfaceMaster(
    struct mmUIManager* p,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    p->pSurfaceMaster = pSurfaceMaster;
}

void
mmUIManager_SetSwapchain(
    struct mmUIManager* p,
    struct mmVKSwapchain*                          pSwapchain)
{
    p->pSwapchain = pSwapchain;
}

void
mmUIManager_SetAssets(
    struct mmUIManager*                            p,
    struct mmVKAssets*                             pAssets)
{
    p->pAssets = pAssets;
}

void
mmUIManager_SetViewMaster(
    struct mmUIManager*                            p,
    const char*                                    pViewMaster)
{
    mmString_Assigns(&p->vViewMaster, pViewMaster);
}

void
mmUIManager_SetScheme(
    struct mmUIManager*                            p,
    const char*                                    pScheme)
{
    mmUIViewContext_SetScheme(&p->vViewContext, pScheme);
}

void
mmUIManager_AddSchemePath(
    struct mmUIManager*                            p,
    const char*                                    path)
{
    mmUISchemeLoader_AddPath(&p->vSchemeLoader, path);
}

void
mmUIManager_RmvSchemePath(
    struct mmUIManager*                            p,
    const char*                                    path)
{
    mmUISchemeLoader_RmvPath(&p->vSchemeLoader, path);
}

void
mmUIManager_ClearSchemePath(
    struct mmUIManager*                            p)
{
    mmUISchemeLoader_ClearPath(&p->vSchemeLoader);
}

void
mmUIManager_AddModule(
    struct mmUIManager*                            p,
    const char*                                    name,
    void*                                          module)
{
    mmUIViewContext_AddModule(&p->vViewContext, name, module);
}

void
mmUIManager_RmvModule(
    struct mmUIManager*                            p,
    const char*                                    name)
{
    mmUIViewContext_RmvModule(&p->vViewContext, name);
}

void
mmUIManager_ClearModule(
    struct mmUIManager*                            p)
{
    mmUIViewContext_ClearModule(&p->vViewContext);
}

void
mmUIManager_Prepare(
    struct mmUIManager*                            p)
{
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    struct mmVKAssets* pAssets = p->pAssets;
    struct mmVKSwapchain* pSwapchain = p->pSwapchain;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    struct mmGraphicsFactory* pGraphicsFactory = &pContextMaster->hGraphicsFactory;

    const char* pViewMaster = mmString_CStr(&p->vViewMaster);

    float density;
    float cube[3];
    float safe[4];

    mmVKAssets_LoadPipelineFromName(pAssets, "mmUIImageOriginal");
    mmVKAssets_LoadPipelineFromName(pAssets, "mmUIImageDisabled");

    mmUISchemeLoader_SetGraphicsFactory(&p->vSchemeLoader, pGraphicsFactory);
    mmUISchemeLoader_SetFileContext(&p->vSchemeLoader, pFileContext);

    mmUIViewLoader_SetFileContext(&p->vViewLoader, pFileContext);
    mmUIViewLoader_SetViewFactory(&p->vViewLoader, &p->vViewFactory);

    mmUIViewDirector_SetViewContext(&p->vViewDirector, &p->vViewContext);
    mmUIViewDirector_SetViewLoader(&p->vViewDirector, &p->vViewLoader);

    mmUISchemeLoader_Prepare(&p->vSchemeLoader);

    // SafeRect
    // ViewSize

    density = (float)pSurfaceMaster->hDisplayDensity;
    cube[0] = (float)pSurfaceMaster->hPhysicalViewSize[0];
    cube[1] = (float)pSurfaceMaster->hPhysicalViewSize[1];
    cube[2] = 0.0f;
    safe[0] = (float)pSurfaceMaster->hPhysicalSafeRect[0];
    safe[1] = (float)pSurfaceMaster->hPhysicalSafeRect[1];
    safe[2] = (float)pSurfaceMaster->hPhysicalSafeRect[2];
    safe[3] = (float)pSurfaceMaster->hPhysicalSafeRect[3];
    mmUIViewContext_SetDisplayDensity(&p->vViewContext, density);
    mmUIViewContext_SetCubeSize(&p->vViewContext, cube);
    mmUIViewContext_SetSafeRect(&p->vViewContext, safe);
    mmUIViewContext_SetGraphicsFactory(&p->vViewContext, pGraphicsFactory);
    mmUIViewContext_SetAssets(&p->vViewContext, pAssets);
    mmUIViewContext_SetSwapchain(&p->vViewContext, pSwapchain);
    mmUIViewContext_SetContext(&p->vViewContext, &pContextMaster->hSuper);
    mmUIViewContext_SetSurface(&p->vViewContext, &pSurfaceMaster->hSuper);
    mmUIViewContext_SetSurfaceKeypad(&p->vViewContext, pSurfaceMaster->pISurfaceKeypad);
    mmUIViewContext_SetClipboard(&p->vViewContext, &pContextMaster->hClipboard);
    mmUIViewContext_SetSchemeLoader(&p->vViewContext, &p->vSchemeLoader);
    mmUIViewContext_SetViewLoader(&p->vViewContext, &p->vViewLoader);
    mmUIViewContext_SetViewDirector(&p->vViewContext, &p->vViewDirector);

    mmUIViewContext_OrthogonalVK(&p->vViewContext, 0.0f, cube[0], cube[1], 0.0f, -10, 10);
    mmUIViewContext_Prepare(&p->vViewContext);

    mmUIViewDirector_Prepare(&p->vViewDirector, "mmUIViewRoot", pViewMaster);
}

void
mmUIManager_Discard(
    struct mmUIManager*                            p)
{
    struct mmVKAssets* pAssets = p->pAssets;
    
    mmUIViewDirector_Discard(&p->vViewDirector);

    mmUIViewLoader_DestroyInvalid(&p->vViewLoader);

    mmUIViewContext_Discard(&p->vViewContext);
    
    mmUISchemeLoader_Discard(&p->vSchemeLoader);

    mmVKAssets_UnloadPipelineByName(pAssets, "mmUIImageDisabled");
    mmVKAssets_UnloadPipelineByName(pAssets, "mmUIImageOriginal");
}

void
mmUIManager_OnSurfacePrepare(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content)
{
    mmUIManager_Resize(p);
}

void
mmUIManager_OnSurfaceDiscard(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content)
{

}

void
mmUIManager_OnSurfaceChanged(
    struct mmUIManager*                            p,
    struct mmEventMetrics*                         content)
{
    mmUIManager_Resize(p);
}

void
mmUIManager_Resize(
    struct mmUIManager*                            p)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;

    float cube[3];
    float safe[4];
    cube[0] = (float)pSurfaceMaster->hPhysicalViewSize[0];
    cube[1] = (float)pSurfaceMaster->hPhysicalViewSize[1];
    cube[2] = 0.0f;
    safe[0] = (float)pSurfaceMaster->hPhysicalSafeRect[0];
    safe[1] = (float)pSurfaceMaster->hPhysicalSafeRect[1];
    safe[2] = (float)pSurfaceMaster->hPhysicalSafeRect[2];
    safe[3] = (float)pSurfaceMaster->hPhysicalSafeRect[3];
    mmUIViewContext_Resize(&p->vViewContext, cube, safe);
}

void
mmUIManager_Update(
    struct mmUIManager*                            p,
    struct mmEventUpdate*                          content)
{
    mmUIViewContext_OnUpdate(&p->vViewContext, content);

    mmUIViewLoader_DestroyInvalid(&p->vViewLoader);
}

void
mmUIManager_RecordBeforePass(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIViewContext_RecordBeforePass(&p->vViewContext, cmd);
}

void
mmUIManager_RecordBehindPass(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIViewContext_RecordBehindPass(&p->vViewContext, cmd);
}

void
mmUIManager_RecordBeforeView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIViewContext_RecordBeforeView(&p->vViewContext, cmd);
}

void
mmUIManager_RecordBasicsView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIViewContext_RecordBasicsView(&p->vViewContext, cmd);
}

void
mmUIManager_RecordBehindView(
    struct mmUIManager*                            p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIViewContext_RecordBehindView(&p->vViewContext, cmd);
}

void
mmUIManager_EnterBackground(
    struct mmUIManager*                            p)
{
    mmUIViewContext_EnterBackground(&p->vViewContext);
}

void
mmUIManager_EnterForeground(
    struct mmUIManager*                            p)
{
    mmUIViewContext_EnterForeground(&p->vViewContext);
}

void
mmUIManager_OnKeypadPressed(
    struct mmUIManager*                            p,
    struct mmEventKeypad*                          content)
{
    mmUIViewContext_OnKeypadPressed(&p->vViewContext, content);
}

void
mmUIManager_OnKeypadRelease(
    struct mmUIManager*                            p,
    struct mmEventKeypad*                          content)
{
    mmUIViewContext_OnKeypadRelease(&p->vViewContext, content);
}

void
mmUIManager_OnKeypadStatus(
    struct mmUIManager*                            p,
    struct mmEventKeypadStatus*                    content)
{
    mmUIViewContext_OnKeypadStatus(&p->vViewContext, content);
}

void
mmUIManager_OnCursorBegan(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content)
{
    mmUIViewContext_OnCursorBegan(&p->vViewContext, content);
}

void
mmUIManager_OnCursorMoved(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content)
{
    mmUIViewContext_OnCursorMoved(&p->vViewContext, content);
}

void
mmUIManager_OnCursorEnded(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content)
{
    mmUIViewContext_OnCursorEnded(&p->vViewContext, content);
}

void
mmUIManager_OnCursorBreak(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content)
{
    mmUIViewContext_OnCursorBreak(&p->vViewContext, content);
}

void
mmUIManager_OnCursorWheel(
    struct mmUIManager*                            p,
    struct mmEventCursor*                          content)
{
    mmUIViewContext_OnCursorWheel(&p->vViewContext, content);
}

void
mmUIManager_OnTouchsBegan(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content)
{
    mmUIViewContext_OnTouchsBegan(&p->vViewContext, content);
}

void
mmUIManager_OnTouchsMoved(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content)
{
    mmUIViewContext_OnTouchsMoved(&p->vViewContext, content);
}

void
mmUIManager_OnTouchsEnded(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content)
{
    mmUIViewContext_OnTouchsEnded(&p->vViewContext, content);
}

void
mmUIManager_OnTouchsBreak(
    struct mmUIManager*                            p,
    struct mmEventTouchs*                          content)
{
    mmUIViewContext_OnTouchsBreak(&p->vViewContext, content);
}

void
mmUIManager_OnGetText(
    struct mmUIManager*                            p,
    struct mmEventEditText*                        content)
{
    mmUIViewContext_OnGetText(&p->vViewContext, content);
}

void
mmUIManager_OnSetText(
    struct mmUIManager*                            p,
    struct mmEventEditText*                        content)
{
    mmUIViewContext_OnSetText(&p->vViewContext, content);
}

void
mmUIManager_OnGetTextEditRect(
    struct mmUIManager*                            p,
    double                                         rect[4])
{
    mmUIViewContext_OnGetTextEditRect(&p->vViewContext, rect);
}

void
mmUIManager_OnInsertTextUtf16(
    struct mmUIManager*                            p,
    struct mmEventEditString*                      content)
{
    mmUIViewContext_OnInsertTextUtf16(&p->vViewContext, content);
}

void
mmUIManager_OnReplaceTextUtf16(
    struct mmUIManager*                            p,
    struct mmEventEditString*                      content)
{
    mmUIViewContext_OnReplaceTextUtf16(&p->vViewContext, content);
}

void
mmUIManager_OnInjectCopy(
    struct mmUIManager*                            p)
{
    mmUIViewContext_OnInjectCopy(&p->vViewContext);
}

void
mmUIManager_OnInjectCut(
    struct mmUIManager*                            p)
{
    mmUIViewContext_OnInjectCut(&p->vViewContext);
}

void
mmUIManager_OnInjectPaste(
    struct mmUIManager*                            p)
{
    mmUIViewContext_OnInjectPaste(&p->vViewContext);
}


