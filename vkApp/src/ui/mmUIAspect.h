/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIAspect_h__
#define __mmUIAspect_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

enum mmUIAspectMode
{
    mmUIAspectModeIgnore = 0,
    mmUIAspectModeShrink = 1,
    mmUIAspectModeExpand = 2,
    mmUIAspectModeEntire = 3,
    mmUIAspectModeBasedX = 4,
    mmUIAspectModeBasedY = 5,
};

const char*
mmUIAspectMode_EncodeEnumerate(
    int                                            v);

int
mmUIAspectMode_DecodeEnumerate(
    const char*                                    w);

void
mmVec2SizeScaleToAspect(
    const float                                    vsize[2],
    const float                                    fsize[2],
    mmUInt8_t                                      mode,
    float                                          ratio,
    float                                          r[2]);

void
mmVec2SizeAspectToScale(
    const float                                    vsize[2],
    const float                                    fsize[2],
    mmUInt8_t                                      mode,
    float                                          ratio,
    float                                          s[2]);

#include "core/mmSuffix.h"

#endif//__mmUIAspect_h__
