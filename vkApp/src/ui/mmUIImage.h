/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIImage_h__
#define __mmUIImage_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKAssets.h"
#include "vk/mmVKDescriptorLayout.h"

#include "core/mmPrefix.h"

struct mmPropertySet;
struct mmUIView;
struct mmUIDrawable;

extern const struct mmVKPipelineVertexInputInfo mmUIImage_PVII;
extern const struct mmVectorValue mmUIImage_DSLB0;
extern const struct mmVectorValue mmUIImage_DSLP;

void
mmUIImage_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIImage_RemovePropertys(
    struct mmPropertySet*                          propertys);

struct mmUIImage
{
    struct mmVKImageInfo* pImageInfo;
    struct mmVKSamplerInfo* pSamplerInfo;

    struct mmVKSheetImage* pSheetImage;
    struct mmVKSheetFrame* pSheetFrame;

    struct mmString hImagePath;
    struct mmString hSheetName;
    struct mmString hFrameName;

    int hFrame[4];
    int hRotated;

    // [minFilter, magFilter]
    mmUInt8_t hFilter[2];
    mmUInt8_t hMipmapMode;
    mmUInt8_t hAddressMode[3];
    mmUInt8_t hBorderColor;
};

void
mmUIImage_Init(
    struct mmUIImage*                              p);

void
mmUIImage_Destroy(
    struct mmUIImage*                              p);

VkResult
mmUIImage_PrepareFromImageInfo(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable,
    struct mmVKAssets*                             pAssets,
    struct mmVKImageInfo*                          pImageInfo,
    const int                                      frame[4],
    int                                            rotated);

VkResult
mmUIImage_PrepareFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_PrepareFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_Prepare(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

void
mmUIImage_Discard(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateFrameFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateFrameFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateFrame(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateSizeFromSheetName(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateSizeFromImagePath(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIImage_UpdateSize(
    struct mmUIImage*                              p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

#include "core/mmSuffix.h"

#endif//__mmUIImage_h__
