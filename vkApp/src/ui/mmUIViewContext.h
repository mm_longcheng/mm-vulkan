/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewContext_h__
#define __mmUIViewContext_h__

#include "core/mmCore.h"
#include "core/mmEventSet.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKCamera.h"

#include "mmUIDrawable.h"

#include "core/mmPrefix.h"

struct mmVKAssets;
struct mmVKSwapchain;
struct mmVKCommandBuffer;

struct mmGraphicsFactory;
struct mmIContext;
struct mmISurface;
struct mmISurfaceKeypad;
struct mmClipboard;

struct mmUIView;
struct mmUIViewLoader;
struct mmUIViewDirector;

struct mmUISchemeLoader;

struct mmUIParameterStyle;

struct mmEventKeypad;
struct mmEventCursor;
struct mmEventTouchs;
struct mmEventUpdate;
struct mmEventEditText;
struct mmEventEditString;
struct mmEventKeypadStatus;

struct mmEventCursorArgs;

/* struct mmEventUIViewContextArgs */
extern const char* mmUIViewContext_EventCubeSizeChanged;
extern const char* mmUIViewContext_EventSafeRectChanged;

/* struct mmEventUIViewContextArgs */
extern const char* mmUIViewContext_EventEnterBackground;
extern const char* mmUIViewContext_EventEnterForeground;

/* struct mmEventUIViewCommandArgs */
extern const char* mmUIViewContext_EventRecordBeforePass;
extern const char* mmUIViewContext_EventRecordBehindPass;
extern const char* mmUIViewContext_EventRecordBeforeView;
extern const char* mmUIViewContext_EventRecordBasicsView;
extern const char* mmUIViewContext_EventRecordBehindView;

struct mmUIViewContext
{
    // events
    struct mmEventSet events;

    // camera.
    struct mmVKCamera camera;

    // Uniform Block Object.
    struct mmVKBuffer ubo;

    // ubo vs;
    struct mmUIDrawableUBOVS ubovs;
    
    // module rbtree. weak reference.
    struct mmRbtreeStringVpt modules;

    // scheme
    struct mmString scheme;

    // view cube normalise.
    float cube[3];

    // safe area rect.
    float safe[4];

    // font display density.
    float density;

    // click tolerance. default is 12.
    float tolerance;

    // click squared distance max.
    float distance;

    // capture point.
    float point[2];

    // root view.
    struct mmUIView* root;

    // capture view.
    struct mmUIView* capture;

    // active view. first responder.
    struct mmUIView* active;

    // contain cursor view.
    struct mmUIView* contain;

    // text graphics factory.
    struct mmGraphicsFactory* graphics;

    // assets.
    struct mmVKAssets* assets;
    
    // swapchain.
    struct mmVKSwapchain* swapchain;

    // context.
    struct mmIContext* context;
    
    // surface.
    struct mmISurface* surface;

    // surface keypad.
    struct mmISurfaceKeypad* keypad;

    // clipboard.
    struct mmClipboard* clipboard;

    // scheme loader.
    struct mmUISchemeLoader* sloader;

    // view loader.
    struct mmUIViewLoader* vloader;

    // view director.
    struct mmUIViewDirector* director;
};

void 
mmUIViewContext_Init(
    struct mmUIViewContext*                        p);

void 
mmUIViewContext_Destroy(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_SetDisplayDensity(
    struct mmUIViewContext*                        p,
    float                                          hDisplayDensity);

void
mmUIViewContext_SetCubeSize(
    struct mmUIViewContext*                        p,
    float                                          cube[3]);

void
mmUIViewContext_SetSafeRect(
    struct mmUIViewContext*                        p,
    float                                          safe[4]);

void
mmUIViewContext_SetGraphicsFactory(
    struct mmUIViewContext*                        p,
    struct mmGraphicsFactory*                      pGraphicsFactory);

void
mmUIViewContext_SetAssets(
    struct mmUIViewContext*                        p,
    struct mmVKAssets*                             pAssets);

void
mmUIViewContext_SetSwapchain(
    struct mmUIViewContext*                        p,
    struct mmVKSwapchain*                          pSwapchain);

void
mmUIViewContext_SetContext(
    struct mmUIViewContext*                        p,
    struct mmIContext*                             context);

void
mmUIViewContext_SetSurface(
    struct mmUIViewContext*                        p,
    struct mmISurface*                             surface);

void
mmUIViewContext_SetSurfaceKeypad(
    struct mmUIViewContext*                        p,
    struct mmISurfaceKeypad*                       keypad);

void
mmUIViewContext_SetClipboard(
    struct mmUIViewContext*                        p,
    struct mmClipboard*                            clipboard);

void
mmUIViewContext_SetSchemeLoader(
    struct mmUIViewContext*                        p,
    struct mmUISchemeLoader*                       sloader);

void
mmUIViewContext_SetViewLoader(
    struct mmUIViewContext*                        p,
    struct mmUIViewLoader*                         vloader);

void
mmUIViewContext_SetViewDirector(
    struct mmUIViewContext*                        p,
    struct mmUIViewDirector*                       director);

void
mmUIViewContext_SetRoot(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               root);

void
mmUIViewContext_SetActive(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               active);

void
mmUIViewContext_SetContain(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               contain);

void
mmUIViewContext_SetScheme(
    struct mmUIViewContext*                        p,
    const char*                                    pScheme);

void
mmUIViewContext_GetSchemeColor(
    const struct mmUIViewContext*                  p,
    const struct mmString*                         pName,
    float                                          hColor[4]);

const struct mmUIParameterStyle*
mmUIViewContext_GetSchemeStyle(
    const struct mmUIViewContext*                  p,
    const struct mmString*                         pName);

void
mmUIViewContext_AddModule(
    struct mmUIViewContext*                        p,
    const char*                                    name,
    void*                                          module);

void
mmUIViewContext_RmvModule(
    struct mmUIViewContext*                        p,
    const char*                                    name);

void*
mmUIViewContext_GetModule(
    const struct mmUIViewContext*                  p,
    const char*                                    name);

void
mmUIViewContext_ClearModule(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_OrthogonalVK(
    struct mmUIViewContext*                        p,
    float l, float r,
    float b, float t,
    float n, float f);

VkResult
mmUIViewContext_PrepareUBOBuffer(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_DiscardUBOBuffer(
    struct mmUIViewContext*                        p);

VkResult
mmUIViewContext_UpdateUBOBuffer(
    struct mmUIViewContext*                        p);

VkResult
mmUIViewContext_Prepare(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_Discard(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_Update(
    struct mmUIViewContext*                        p,
    struct mmEventUpdate*                          content);

void
mmUIViewContext_Invalidate(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_Sized(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_Changed(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_RecordBeforePass(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_RecordBehindPass(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_RecordBeforeView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_RecordBasicsView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_RecordBehindView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_OnRecordCommand(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewContext_DeviceWaitIdle(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_MarkCacheInvalidWhole(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_HandleSized(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_Resize(
    struct mmUIViewContext*                        p,
    float                                          cube[3],
    float                                          safe[4]);

void
mmUIViewContext_EnterBackground(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_EnterForeground(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_OnUpdateClickedDistance(
    struct mmUIViewContext*                        p,
    const float                                    point[2]);

void
mmUIViewContext_OnEventClickedArgs(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               v,
    struct mmEventCursorArgs*                      args);

void
mmUIViewContext_OnEventContainArgs(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               v,
    struct mmEventCursorArgs*                      args);

void
mmUIViewContext_OnKeypadProvider(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnUpdate(
    struct mmUIViewContext*                        p,
    struct mmEventUpdate*                          content);

void
mmUIViewContext_OnInvalidate(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_OnKeypadPressed(
    struct mmUIViewContext*                        p,
    struct mmEventKeypad*                          content);

void
mmUIViewContext_OnKeypadRelease(
    struct mmUIViewContext*                        p,
    struct mmEventKeypad*                          content);

void
mmUIViewContext_OnKeypadStatus(
    struct mmUIViewContext*                        p,
    struct mmEventKeypadStatus*                    content);

void
mmUIViewContext_OnCursorBegan(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnCursorMoved(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnCursorEnded(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnCursorBreak(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnCursorWheel(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewContext_OnTouchsBegan(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content);

void
mmUIViewContext_OnTouchsMoved(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content);

void
mmUIViewContext_OnTouchsEnded(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content);

void
mmUIViewContext_OnTouchsBreak(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content);

void
mmUIViewContext_OnGetText(
    struct mmUIViewContext*                        p,
    struct mmEventEditText*                        content);

void
mmUIViewContext_OnSetText(
    struct mmUIViewContext*                        p,
    struct mmEventEditText*                        content);

void
mmUIViewContext_OnGetTextEditRect(
    struct mmUIViewContext*                        p,
    double                                         rect[4]);

void
mmUIViewContext_OnInsertTextUtf16(
    struct mmUIViewContext*                        p,
    struct mmEventEditString*                      content);

void
mmUIViewContext_OnReplaceTextUtf16(
    struct mmUIViewContext*                        p,
    struct mmEventEditString*                      content);

void
mmUIViewContext_OnInjectCopy(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_OnInjectCut(
    struct mmUIViewContext*                        p);

void
mmUIViewContext_OnInjectPaste(
    struct mmUIViewContext*                        p);

#include "core/mmSuffix.h"

#endif//__mmUIViewContext_h__
