/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIParameter.h"

void
mmUIParameterProperty_Init(
    struct mmUIParameterProperty*                  p)
{
    mmString_Init(&p->Name);
    mmString_Init(&p->Value);
}

void
mmUIParameterProperty_Destroy(
    struct mmUIParameterProperty*                  p)
{
    mmString_Destroy(&p->Value);
    mmString_Destroy(&p->Name);
}

void
mmUIParameterPropertys_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmUIParameterProperty_Init;
    hValueAllocator.Recycle = &mmUIParameterProperty_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmUIParameterProperty));
}

void
mmUIParameterPropertys_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmUIParameterView_Init(
    struct mmUIParameterView*                      p)
{
    mmString_Init(&p->Type);
    mmString_Init(&p->Style);
    mmUIParameterPropertys_Init(&p->Property);
    mmUIParameterViews_Init(&p->Children);
}

void
mmUIParameterView_Destroy(
    struct mmUIParameterView*                      p)
{
    mmUIParameterViews_Destroy(&p->Children);
    mmUIParameterPropertys_Destroy(&p->Property);
    mmString_Destroy(&p->Style);
    mmString_Destroy(&p->Type);
}

void
mmUIParameterViews_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmUIParameterView_Init;
    hValueAllocator.Recycle = &mmUIParameterView_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmUIParameterView));
}

void
mmUIParameterViews_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmUIParameterStyle_Init(
    struct mmUIParameterStyle*                     p)
{
    mmString_Init(&p->Name);
    mmString_Init(&p->Style);
    mmString_Init(&p->Type);
    mmUIParameterPropertys_Init(&p->Property);
}

void
mmUIParameterStyle_Destroy(
    struct mmUIParameterStyle*                     p)
{
    mmUIParameterPropertys_Destroy(&p->Property);
    mmString_Destroy(&p->Type);
    mmString_Destroy(&p->Style);
    mmString_Destroy(&p->Name);
}

void
mmUIParameterStyles_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmUIParameterStyle_Init;
    hValueAllocator.Recycle = &mmUIParameterStyle_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmUIParameterStyle));
}

void
mmUIParameterStyles_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmUIParameterFont_Init(
    struct mmUIParameterFont*                      p)
{
    mmString_Init(&p->Family);
    mmString_Init(&p->Path);
}

void
mmUIParameterFont_Destroy(
    struct mmUIParameterFont*                      p)
{
    mmString_Destroy(&p->Path);
    mmString_Destroy(&p->Family);
}

void
mmUIParameterFonts_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmUIParameterFont_Init;
    hValueAllocator.Recycle = &mmUIParameterFont_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmUIParameterFont));
}

void
mmUIParameterFonts_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmUIParameterScheme_Init(
    struct mmUIParameterScheme*                    p)
{
    mmString_Init(&p->Name);
    mmUIParameterFonts_Init(&p->Font);
    mmUIParameterPropertys_Init(&p->Color);
    mmUIParameterStyles_Init(&p->Style);
}

void
mmUIParameterScheme_Destroy(
    struct mmUIParameterScheme*                    p)
{
    mmUIParameterStyles_Destroy(&p->Style);
    mmUIParameterPropertys_Destroy(&p->Color);
    mmUIParameterFonts_Destroy(&p->Font);
    mmString_Destroy(&p->Name);
}
