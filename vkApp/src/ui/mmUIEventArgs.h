/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIEventArgs_h__
#define __mmUIEventArgs_h__

#include "core/mmCore.h"
#include "core/mmEventSet.h"

#include "core/mmPrefix.h"

struct mmVKCommandBuffer;
struct mmUIView;
struct mmUIViewContext;

struct mmEventUIViewArgs
{
    struct mmEventArgs hSuper;

    struct mmUIView* pView;
};

void 
mmEventUIViewArgs_Reset(
    struct mmEventUIViewArgs*                      p);

void 
mmEventUIViewArgs_Assign(
    struct mmEventUIViewArgs*                      p, 
    long                                           handled, 
    struct mmUIView*                               pView);

struct mmEventUIClickedArgs
{
    struct mmEventArgs hSuper;
    struct mmEventCursor* content;
    struct mmUIView* pView;
    float point[2];
};

void 
mmEventUIClickedArgs_Reset(
    struct mmEventUIClickedArgs*                   p);

struct mmEventUIFocusArgs
{
    struct mmEventArgs hSuper;
    struct mmEventCursor* content;
    struct mmUIView* pView;
};

void 
mmEventUIFocusArgs_Reset(
    struct mmEventUIFocusArgs*                     p);

struct mmEventUIViewContextArgs
{
    struct mmEventArgs hSuper;
    struct mmUIViewContext* pViewContext;
};

void 
mmEventUIViewContextArgs_Reset(
    struct mmEventUIViewContextArgs*               p);

struct mmEventUIViewCommandArgs
{
    struct mmEventArgs hSuper;
    struct mmUIViewContext* pViewContext;
    struct mmVKCommandBuffer* pCmd;
};

void
mmEventUIViewCommandArgs_Reset(
    struct mmEventUIViewCommandArgs*               p);

#include "core/mmSuffix.h"

#endif//__mmUIEventArgs_h__
