/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewFactory_h__
#define __mmUIViewFactory_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "ui/mmUIView.h"

#include "core/mmPrefix.h"

struct mmUIViewFactory
{
    struct mmRbtreeStrVpt types;
};

void 
mmUIViewFactory_Init(
    struct mmUIViewFactory*                        p);

void 
mmUIViewFactory_Destroy(
    struct mmUIViewFactory*                        p);

void 
mmUIViewFactory_AddType(
    struct mmUIViewFactory*                        p, 
    const struct mmUIMetadata*                     m);

void 
mmUIViewFactory_RmvType(
    struct mmUIViewFactory*                        p, 
    const struct mmUIMetadata*                     m);

const struct mmUIMetadata*
mmUIViewFactory_GetType(
    struct mmUIViewFactory*                        p,
    const char*                                    type);

struct mmUIView* 
mmUIViewFactory_Produce(
    struct mmUIViewFactory*                        p, 
    const char*                                    type);

void 
mmUIViewFactory_Recycle(
    struct mmUIViewFactory*                        p, 
    struct mmUIView*                               v);

#include "core/mmSuffix.h"

#endif//__mmUIViewFactory_h__
