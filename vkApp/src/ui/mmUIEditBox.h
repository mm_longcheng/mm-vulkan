/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIEditBox_h__
#define __mmUIEditBox_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "core/mmPrefix.h"

int 
mmTextIsWordBoundaryR(
    const struct mmUtf16String*                    str,
    int                                            idx);

int 
mmTextIsWordBoundaryL(
    const struct mmUtf16String*                    str,
    int                                            idx);

int 
mmTextIsWordBoundary(
    const struct mmUtf16String*                    str,
    int                                            idx);

int 
mmTextMoveToWordPrev(
    const struct mmUtf16String*                    str,
    int                                            c);

int 
mmTextMoveToWordNext(
    const struct mmUtf16String*                    str,
    int                                            c);

struct mmClipboard;

struct mmUIEditBox
{
    // utf16 text pointer, must be valid.
    struct mmUtf16String* text;

    // caret index
    size_t c;

    // selection left
    size_t l;
    // selection right
    size_t r;

    // drag anchor index
    size_t anchor;
};

void 
mmUIEditBox_Init(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_Destroy(
    struct mmUIEditBox*                            p);

void
mmUIEditBox_SetUtf16String(
    struct mmUIEditBox*                            p,
    struct mmUtf16String*                          text);

int 
mmUIEditBox_PerformCopy(
    struct mmUIEditBox*                            p, 
    struct mmClipboard*                            clipboard);

int 
mmUIEditBox_PerformCut(
    struct mmUIEditBox*                            p, 
    struct mmClipboard*                            clipboard);

int 
mmUIEditBox_PerformPaste(
    struct mmUIEditBox*                            p, 
    struct mmClipboard*                            clipboard);

size_t 
mmUIEditBox_GetSelectionStartIndex(
    const struct mmUIEditBox*                      p);

size_t 
mmUIEditBox_GetSelectionEndIndex(
    const struct mmUIEditBox*                      p);

size_t 
mmUIEditBox_GetCaretIndex(
    const struct mmUIEditBox*                      p);

size_t 
mmUIEditBox_GetSelectionLength(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_SetCaretIndex(
    struct mmUIEditBox*                            p, 
    size_t                                         caret);

void 
mmUIEditBox_SetSelection(
    struct mmUIEditBox*                            p, 
    size_t                                         start, 
    size_t                                         end);

void 
mmUIEditBox_ClearSelection(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_EraseSelectedText(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_HandleTextChanged(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_HandleBackspace(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_HandleDelete(
    struct mmUIEditBox*                            p);

void 
mmUIEditBox_InsertUtf16(
    struct mmUIEditBox*                            p, 
    mmUInt16_t*                                    s, 
    size_t                                         l);

void 
mmUIEditBox_ReplaceUtf16(
    struct mmUIEditBox*                            p, 
    size_t                                         o, 
    size_t                                         n, 
    mmUInt16_t*                                    s, 
    size_t                                         l);

void 
mmUIEditBox_HandleCharLeft(
    struct mmUIEditBox*                            p, 
    mmUInt32_t                                     sysKeys);

void 
mmUIEditBox_HandleWordLeft(
    struct mmUIEditBox*                            p, 
    mmUInt32_t                                     sysKeys);

void 
mmUIEditBox_HandleCharRight(
    struct mmUIEditBox*                            p, 
    mmUInt32_t                                     sysKeys);

void 
mmUIEditBox_HandleWordRight(
    struct mmUIEditBox*                            p, 
    mmUInt32_t                                     sysKeys);

#include "core/mmSuffix.h"

#endif//__mmUIEditBox_h__
