/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIEventDriver.h"

#include "ui/mmUIViewContext.h"

void
mmUIEventDriver_Init(
    struct mmUIEventDriver*                        p)
{
    mmListVpt_Init(&p->list);
}

void
mmUIEventDriver_Destroy(
    struct mmUIEventDriver*                        p)
{
    mmListVpt_Destroy(&p->list);
}

void
mmUIEventDriver_Add(
    struct mmUIEventDriver*                        p,
    struct mmUIViewContext*                        pViewContext)
{
    mmListVpt_AddTail(&p->list, (void*)pViewContext);
}

void
mmUIEventDriver_Rmv(
    struct mmUIEventDriver*                        p,
    struct mmUIViewContext*                        pViewContext)
{
    mmListVpt_Remove(&p->list, (void*)pViewContext);
}

void
mmUIEventDriver_OnHandlerArgs0(
    struct mmUIEventDriver*                        p,
    void*                                          func)
{
    typedef void(*EventFuncType)(void* p);
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    const struct mmListVpt* list = &p->list;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        (*((EventFuncType)func))(it->v);
    }
}

void
mmUIEventDriver_OnHandlerArgs1(
    struct mmUIEventDriver*                        p,
    void*                                          func,
    void*                                          content)
{
    typedef void(*EventFuncType)(void* p, void* content);
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    const struct mmListVpt* list = &p->list;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        (*((EventFuncType)func))(it->v, content);
    }
}

void
mmUIEventDriver_OnKeypadPressed(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypad*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnKeypadPressed,
        content);
}

void
mmUIEventDriver_OnKeypadRelease(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypad*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnKeypadRelease,
        content);
}

void
mmUIEventDriver_OnKeypadStatus(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypadStatus*                    content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnKeypadStatus,
        content);
}

void
mmUIEventDriver_OnCursorBegan(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnCursorBegan,
        content);
}

void
mmUIEventDriver_OnCursorMoved(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnCursorMoved,
        content);
}

void
mmUIEventDriver_OnCursorEnded(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnCursorEnded,
        content);
}

void
mmUIEventDriver_OnCursorBreak(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnCursorBreak,
        content);
}

void
mmUIEventDriver_OnCursorWheel(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnCursorWheel,
        content);
}

void
mmUIEventDriver_OnTouchsBegan(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnTouchsBegan,
        content);
}

void
mmUIEventDriver_OnTouchsMoved(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnTouchsMoved,
        content);
}

void
mmUIEventDriver_OnTouchsEnded(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnTouchsEnded,
        content);
}

void
mmUIEventDriver_OnTouchsBreak(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnTouchsBreak,
        content);
}

void
mmUIEventDriver_OnGetText(
    struct mmUIEventDriver*                        p,
    struct mmEventEditText*                        content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnGetText,
        content);
}

void
mmUIEventDriver_OnSetText(
    struct mmUIEventDriver*                        p,
    struct mmEventEditText*                        content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnSetText,
        content);
}

void
mmUIEventDriver_OnGetTextEditRect(
    struct mmUIEventDriver*                        p,
    double                                         rect[4])
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnGetTextEditRect,
        rect);
}

void
mmUIEventDriver_OnInsertTextUtf16(
    struct mmUIEventDriver*                        p,
    struct mmEventEditString*                      content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnInsertTextUtf16,
        content);
}

void
mmUIEventDriver_OnReplaceTextUtf16(
    struct mmUIEventDriver*                        p,
    struct mmEventEditString*                      content)
{
    mmUIEventDriver_OnHandlerArgs1(
        p,
        &mmUIViewContext_OnReplaceTextUtf16,
        content);
}

void
mmUIEventDriver_OnInjectCopy(
    struct mmUIEventDriver*                        p)
{
    mmUIEventDriver_OnHandlerArgs0(
        p,
        &mmUIViewContext_OnInjectCopy);
}

void
mmUIEventDriver_OnInjectCut(
    struct mmUIEventDriver*                        p)
{
    mmUIEventDriver_OnHandlerArgs0(
        p,
        &mmUIViewContext_OnInjectCut);
}

void
mmUIEventDriver_OnInjectPaste(
    struct mmUIEventDriver*                        p)
{
    mmUIEventDriver_OnHandlerArgs0(
        p,
        &mmUIViewContext_OnInjectPaste);
}
