/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewEditBox.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIPropertyHelper.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"
#include "core/mmKeyCode.h"

#include "nwsi/mmUTFHelper.h"
#include "nwsi/mmISurface.h"

#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKCommandBuffer.h"

void
mmUIViewEditBox_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEditBox, view));
    mmUIDrawable_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEditBox, drawable));
    mmUIText_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewEditBox, text));
}

void
mmUIViewEditBox_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIText_RemovePropertys(propertys);
    mmUIDrawable_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewEditBox =
{
    &mmUIViewEditBox_OnPrepare,
    &mmUIViewEditBox_OnDiscard,
};

const struct mmUIWidgetVariable mmUIWidgetVariableViewEditBox =
{
    &mmUIViewEditBox_OnUpdate,
    &mmUIViewEditBox_OnInvalidate,
    &mmUIViewEditBox_OnSized,
    &mmUIViewEditBox_OnChanged,
};

const struct mmUIWidgetDrawable mmUIWidgetDrawableViewEditBox =
{
    &mmUIViewEditBox_OnDescriptorSetProduce,
    &mmUIViewEditBox_OnDescriptorSetRecycle,
    &mmUIViewEditBox_OnDescriptorSetUpdate,
    &mmUIViewEditBox_OnUpdatePushConstants,
    &mmUIViewEditBox_OnUpdatePipeline,
    &mmUIViewEditBox_OnRecord,
};

const struct mmUIResponderKeypad mmUIResponderKeypadViewEditBox =
{
    &mmUIViewEditBox_OnKeypadPressed,
    &mmUIViewEditBox_OnKeypadRelease,
    &mmUIViewEditBox_OnKeypadStatus,
};

const struct mmUIResponderCursor mmUIResponderCursorViewEditBox =
{
    &mmUIViewEditBox_OnCursorBegan,
    &mmUIViewEditBox_OnCursorMoved,
    &mmUIViewEditBox_OnCursorEnded,
    &mmUIViewEditBox_OnCursorBreak,
    &mmUIViewEditBox_OnCursorWheel,
};

const struct mmUIResponderEditor mmUIResponderEditorViewEditBox =
{
    &mmUIViewEditBox_OnGetText,
    &mmUIViewEditBox_OnSetText,
    &mmUIViewEditBox_OnGetTextEditRect,
    &mmUIViewEditBox_OnInsertTextUtf16,
    &mmUIViewEditBox_OnReplaceTextUtf16,
    &mmUIViewEditBox_OnInjectCopy,
    &mmUIViewEditBox_OnInjectCut,
    &mmUIViewEditBox_OnInjectPaste,
    &mmUIViewEditBox_OnFocusCapture,
    &mmUIViewEditBox_OnFocusRelease,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewEditBox =
{
    "mmUIViewEditBox",
    sizeof(struct mmUIViewEditBox),
    &mmUIViewEditBox_Produce,
    &mmUIViewEditBox_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewEditBox =
{
    &mmUIMetaAllocatorViewEditBox,
    /*- Widget -*/
    &mmUIWidgetOperableViewEditBox,
    &mmUIWidgetVariableViewEditBox,
    &mmUIWidgetDrawableViewEditBox,
    /*- Responder -*/
    &mmUIResponderCursorViewEditBox,
    &mmUIResponderKeypadViewEditBox,
    &mmUIResponderEditorViewEditBox,
    NULL,
};

const char* mmUIViewEditBox_EventTextChanged = "EventTextChanged";
const char* mmUIViewEditBox_EventCaretChanged = "EventCaretChanged";
const char* mmUIViewEditBox_EventSelectionChanged = "EventSelectionChanged";
const char* mmUIViewEditBox_EventTextAccepted = "EventTextAccepted";
const char* mmUIViewEditBox_EventFocusCapture = "EventFocusCapture";
const char* mmUIViewEditBox_EventFocusRelease = "EventFocusRelease";

void
mmUIViewEditBox_Init(
    struct mmUIViewEditBox*                        p)
{
    mmUIView_Init(&p->view);
    mmUIDrawable_Init(&p->drawable);
    mmUIText_Init(&p->text);
    mmUIEditBox_Init(&p->editbox);

    // Text will apply display density.
    // The AspectMode must be mmUIAspectModeEntire.
    p->drawable.hAspectMode = mmUIAspectModeEntire;

    mmUIEditBox_SetUtf16String(&p->editbox, &p->text.vLayout.hText.hString);
}

void
mmUIViewEditBox_Destroy(
    struct mmUIViewEditBox*                        p)
{
    mmUIEditBox_Destroy(&p->editbox);
    mmUIText_Destroy(&p->text);
    mmUIDrawable_Destroy(&p->drawable);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewEditBox_Produce(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_Init(p);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewEditBox;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);
    
    mmUIViewEditBox_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewEditBox_Recycle(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_RemovePropertys(&p->view.propertys);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewEditBox;
    
    mmUIViewEditBox_Destroy(p);
}

void 
mmUIViewEditBox_UpdateText(
    struct mmUIViewEditBox*                        p)
{
    struct mmUIEditBox* editbox = &p->editbox;
    
    mmUIText_SetCaretIndex(&p->text, editbox->c);
    mmUIText_SetSelection(&p->text, editbox->l, editbox->r);
    
    // Note: we must wait idle before update content.
    mmUIViewContext_DeviceWaitIdle(p->view.context);
    mmUIText_UpdateText(&p->text, &p->view, &p->drawable);
    mmUIViewEditBox_OnDescriptorSetUpdate(p);
}

void
mmUIViewEditBox_UpdateSize(
    struct mmUIViewEditBox*                        p)
{
    // Note: we must wait idle before update content.
    mmUIViewContext_DeviceWaitIdle(p->view.context);
    mmUIText_UpdateSize(&p->text, &p->view, &p->drawable);
    mmUIViewEditBox_OnDescriptorSetUpdate(p);
}

void
mmUIViewEditBox_HandleTextChanged(
    struct mmUIViewEditBox*                        p)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;

    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
}

void
mmUIViewEditBox_OnPrepare(
    struct mmUIViewEditBox*                        p)
{
    mmUIText_Prepare(
        &p->text,
        &p->view,
        &p->drawable);

    mmUIViewEditBox_OnDescriptorSetProduce(p);
    mmUIViewEditBox_OnDescriptorSetUpdate(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);
}

void
mmUIViewEditBox_OnDiscard(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_OnDescriptorSetRecycle(p);

    mmUIText_Discard(
        &p->text,
        &p->view,
        &p->drawable);
}

void
mmUIViewEditBox_OnUpdate(
    struct mmUIViewEditBox*                        p,
    struct mmEventUpdate*                          content)
{
    do
    {
        struct mmUIViewContext* pViewContext = NULL;

        pViewContext = p->view.context;
        
        if (NULL == pViewContext)
        {
            break;
        }
        
        if (pViewContext->active != &p->view)
        {
            // first responder is not self.
            break;
        }
        
        if (0 != content->index)
        {
            // index is cycle [0x00, 0x3F].
            // we use (0 == index) for simple toggle.
            break;
        }
        
        mmUIText_ToggleCaretStatus(&p->text);
        
        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

    } while (0);
}

void
mmUIViewEditBox_OnInvalidate(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_OnUpdatePushConstants(p);
}

void
mmUIViewEditBox_OnSized(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_UpdateSize(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);
}

void
mmUIViewEditBox_OnChanged(
    struct mmUIViewEditBox*                        p)
{
    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);
}

void
mmUIViewEditBox_OnDescriptorSetProduce(
    struct mmUIViewEditBox*                        p)
{
    mmUIDrawable_DescriptorSetProduce(&p->drawable);
}

void
mmUIViewEditBox_OnDescriptorSetRecycle(
    struct mmUIViewEditBox*                        p)
{
    mmUIDrawable_DescriptorSetRecycle(&p->drawable);
}

void
mmUIViewEditBox_OnDescriptorSetUpdate(
    struct mmUIViewEditBox*                        p)
{
    mmUIDrawable_DescriptorSetUpdate(
        &p->drawable,
        &p->view,
        &p->text.vImageInfo,
        p->text.pSamplerInfo);
}

void
mmUIViewEditBox_OnUpdatePushConstants(
    struct mmUIViewEditBox*                        p)
{
    mmUIDrawable_UpdatePushConstants(&p->drawable, &p->view);
}

void
mmUIViewEditBox_OnUpdatePipeline(
    struct mmUIViewEditBox*                        p)
{
    mmUIDrawable_UpdatePool(&p->drawable, &p->view);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void
mmUIViewEditBox_OnRecord(
    struct mmUIViewEditBox*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIDrawable_OnRecord(&p->drawable, cmd->cmdBuffer);
    mmUIView_OnRecordChildren(&p->view, cmd);
}

void
mmUIViewEditBox_OnKeypadPressed(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypad*                          content)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    struct mmUIEditBox* editbox = &p->editbox;

    switch (content->key)
    {
    case MM_KC_LSHIFT:
    case MM_KC_RSHIFT:
        if (mmUIEditBox_GetSelectionLength(editbox) == 0)
        {
            editbox->anchor = editbox->c;
        }
        break;

    case MM_KC_BACKSPACE:
    {
        mmUIEditBox_HandleBackspace(editbox);

        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
    }
    break;

    case MM_KC_DELETE:
    {
        mmUIEditBox_HandleDelete(editbox);

        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
    }
    break;

    case MM_KC_LEFTARROW:
    {
        if (content->modifier_mask & MM_MODIFIER_CONTROL)
        {
            mmUIEditBox_HandleWordLeft(editbox, content->modifier_mask);
        }
        else
        {
            mmUIEditBox_HandleCharLeft(editbox, content->modifier_mask);
        }
        
        mmUIText_SetCaretStatus(&p->text, 1);
        
        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    }
    break;

    case MM_KC_RIGHTARROW:
    {
        if (content->modifier_mask & MM_MODIFIER_CONTROL)
        {
            mmUIEditBox_HandleWordRight(editbox, content->modifier_mask);
        }
        else
        {
            mmUIEditBox_HandleCharRight(editbox, content->modifier_mask);
        }
        
        mmUIText_SetCaretStatus(&p->text, 1);
        
        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    }
    break;

    case MM_KC_TAB:
    case MM_KC_RETURN:
    case MM_KC_NUMPADENTER:
    {
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextAccepted, &hArgs);
    }
    break;

    default:
        break;
    }
}

void
mmUIViewEditBox_OnKeypadRelease(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypad*                          content)
{
    // do noting here.
}

void
mmUIViewEditBox_OnKeypadStatus(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypadStatus*                    content)
{
    // do noting here.
}

void
mmUIViewEditBox_OnCursorBegan(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    struct mmUIEditBox* editbox = &p->editbox;

    float point[2] = { (float)content->abs_x, (float)content->abs_y };

    mmUInt32_t hCaretIndex = 0;
    int hCaretWrapping = 0;
    mmUIText_GetPointCaret(&p->text, &p->view, point, &hCaretIndex, &hCaretWrapping);

    mmUIEditBox_ClearSelection(editbox);

    editbox->anchor = hCaretIndex;
    editbox->c = hCaretIndex;
    
    mmUIText_SetCaretStatus(&p->text, 1);
    mmUIText_SetCaretWrapping(&p->text, hCaretWrapping);
    
    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
}

void
mmUIViewEditBox_OnCursorMoved(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content)
{
    do
    {
        struct mmEventUIViewArgs hArgs;
        struct mmEventSet* events = &p->view.events;
        struct mmUIEditBox* editbox = &p->editbox;
        struct mmTextLayout* pTextLayout = &p->text.vLayout;
        struct mmTextParagraphFormat* pParagraphFormat = &pTextLayout->hParagraphFormat;
        float point[2] = { (float)content->abs_x, (float)content->abs_y };
        mmUInt32_t hCaretIndex = 0;
        int hCaretWrapping = 0;

        struct mmUIViewContext* pViewContext = NULL;

        pViewContext = p->view.context;
        
        if (NULL == pViewContext)
        {
            break;
        }
        
        if (pViewContext->capture != &p->view)
        {
            break;
        }
        
        mmUIText_GetPointCaret(&p->text, &p->view, point, &hCaretIndex, &hCaretWrapping);

        mmUIEditBox_SetSelection(editbox, editbox->anchor, hCaretIndex);
        mmUIEditBox_SetCaretIndex(editbox, hCaretIndex);
        
        if (editbox->l == pParagraphFormat->hSelection[0] &&
            editbox->r == pParagraphFormat->hSelection[1] &&
            editbox->c == pParagraphFormat->hCaretIndex)
        {
            break;
        }
        
        mmUIText_SetCaretWrapping(&p->text, hCaretWrapping);

        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
        
        if (editbox->c == pParagraphFormat->hCaretIndex)
        {
            break;
        }
        
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
    } while (0);
}

void
mmUIViewEditBox_OnCursorEnded(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content)
{

}

void
mmUIViewEditBox_OnCursorBreak(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content)
{

}

void
mmUIViewEditBox_OnCursorWheel(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content)
{

}

void 
mmUIViewEditBox_OnGetText(
    struct mmUIViewEditBox*                        p, 
    struct mmEventEditText*                        content)
{
    struct mmUIEditBox* editbox = &p->editbox;

    if (0 != (mmEditTextWhole & content->v))
    {
        content->t = (mmUInt16_t*)mmUtf16String_Data(editbox->text);
        content->c = editbox->c;
        content->l = editbox->l;
        content->r = editbox->r;
    }
    if (0 != (mmEditTextRsize & content->v))
    {
        content->size = mmUtf16String_Size(editbox->text);
    }
    if (0 != (mmEditTextCsize & content->v))
    {
        content->capacity = mmUtf16String_Capacity(editbox->text);
    }
    if (0 != (mmEditTextRange & content->v))
    {
        content->l = editbox->l;
        content->r = editbox->r;
    }
    if (0 != (mmEditTextCaret & content->v))
    {
        content->c = editbox->c;
    }
    if (0 != (mmEditTextDirty & content->v))
    {
        // need do nothing here.
    }
}

void 
mmUIViewEditBox_OnSetText(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditText*                        content)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    struct mmUIEditBox* editbox = &p->editbox;

    if (0 != (mmEditTextWhole & content->v))
    {
        const mmUtf16_t* t = mmUtf16String_Data(editbox->text);
        // Try to avoid copying.
        mmUtf16String_Resize(editbox->text, content->size);
        mmMemcpy((void*)t, content->t, content->size * sizeof(mmUInt16_t));

        editbox->c = content->c;
        editbox->l = content->l;
        editbox->r = content->r;

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    }
    if (0 != (mmEditTextRsize & content->v))
    {
        mmUtf16String_Resize(editbox->text, content->size);
        content->t = (mmUInt16_t*)mmUtf16String_Data(editbox->text);
    }
    if (0 != (mmEditTextCsize & content->v))
    {
        size_t hTextSize = mmUtf16String_Size(editbox->text);
        size_t hNeedSize = (hTextSize < content->capacity) ? content->capacity : hTextSize;
        mmUtf16String_Resize(editbox->text, hNeedSize);
        content->t = (mmUInt16_t*)mmUtf16String_Data(editbox->text);
    }
    if (0 != (mmEditTextRange & content->v))
    {
        editbox->l = content->l;
        editbox->r = content->r;

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    }
    if (0 != (mmEditTextCaret & content->v))
    {
        editbox->c = content->c;

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
    }
    if (0 != (mmEditTextDirty & content->v))
    {
        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
    }
}

void 
mmUIViewEditBox_OnGetTextEditRect(
    struct mmUIViewEditBox*                        p,
    double                                         rect[4])
{
    struct mmUIView* view = &p->view;

    struct mmUICubeScalar hCubeScalar;
    mmUIView_GetWorldCubeScalar(view, &hCubeScalar);

    rect[0] = (hCubeScalar.min[0]);                       // x
    rect[1] = (hCubeScalar.min[1]);                       // y
    rect[2] = (hCubeScalar.max[0] - hCubeScalar.min[0]);  // w
    rect[3] = (hCubeScalar.max[1] - hCubeScalar.min[1]);  // h
}

void 
mmUIViewEditBox_OnInsertTextUtf16(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditString*                      content)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    struct mmUIEditBox* editbox = &p->editbox;

    mmUIEditBox_InsertUtf16(
        editbox,
        content->s,
        content->l);

    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
}

void 
mmUIViewEditBox_OnReplaceTextUtf16(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditString*                      content)
{
    struct mmUIEditBox* editbox = &p->editbox;

    mmUIEditBox_ReplaceUtf16(
        editbox,
        content->o,
        content->n,
        content->s,
        content->l);

    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    // Not trigger event.
}

void 
mmUIViewEditBox_OnInjectCopy(
    struct mmUIViewEditBox*                        p)
{
    do
    {
        struct mmUIEditBox* editbox = &p->editbox;
        struct mmUIViewContext* pViewContext = NULL;
        struct mmClipboard* clipboard = NULL;

        pViewContext = p->view.context;
        if (NULL == pViewContext)
        {
            break;
        }
        clipboard = pViewContext->clipboard;
        if (NULL == clipboard)
        {
            break;
        }

        mmUIEditBox_PerformCopy(editbox, clipboard);
    } while (0);
}

void 
mmUIViewEditBox_OnInjectCut(
    struct mmUIViewEditBox*                        p)
{
    do
    {
        struct mmEventUIViewArgs hArgs;
        struct mmEventSet* events = &p->view.events;
        struct mmUIEditBox* editbox = &p->editbox;
        struct mmUIViewContext* pViewContext = NULL;
        struct mmClipboard* clipboard = NULL;

        pViewContext = p->view.context;
        if (NULL == pViewContext)
        {
            break;
        }
        clipboard = pViewContext->clipboard;
        if (NULL == clipboard)
        {
            break;
        }

        mmUIEditBox_PerformCut(editbox, clipboard);

        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
    } while (0);
}

void 
mmUIViewEditBox_OnInjectPaste(
    struct mmUIViewEditBox*                        p)
{
    do
    {
        struct mmEventUIViewArgs hArgs;
        struct mmEventSet* events = &p->view.events;
        struct mmUIEditBox* editbox = &p->editbox;
        struct mmUIViewContext* pViewContext = NULL;
        struct mmClipboard* clipboard = NULL;

        pViewContext = p->view.context;
        if (NULL == pViewContext)
        {
            break;
        }
        clipboard = pViewContext->clipboard;
        if (NULL == clipboard)
        {
            break;
        }

        mmUIEditBox_PerformPaste(editbox, clipboard);

        mmUIViewEditBox_UpdateText(p);
        mmUIViewEditBox_OnUpdatePushConstants(p);

        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventCaretChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventSelectionChanged, &hArgs);
        mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
        mmEventSet_FireEvent(events, mmUIViewEditBox_EventTextChanged, &hArgs);
    } while (0);
}

void
mmUIViewEditBox_OnFocusCapture(
    struct mmUIViewEditBox*                        p)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    
    mmUIText_SetCaretStatus(&p->text, 1);
    mmUIText_SetSelectionStatus(&p->text, 1);
    
    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventFocusCapture, &hArgs);
}

void
mmUIViewEditBox_OnFocusRelease(
    struct mmUIViewEditBox*                        p)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;
    
    mmUIText_SetCaretStatus(&p->text, 0);
    mmUIText_SetSelectionStatus(&p->text, 0);
    
    mmUIViewEditBox_UpdateText(p);
    mmUIViewEditBox_OnUpdatePushConstants(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewEditBox_EventFocusRelease, &hArgs);
}
