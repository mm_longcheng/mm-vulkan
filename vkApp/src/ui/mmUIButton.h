/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIButton_h__
#define __mmUIButton_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

struct mmPropertySet;
struct mmUIView;

void
mmUIButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIButton_RemovePropertys(
    struct mmPropertySet*                          propertys);

enum mmUIButtonState
{
    mmUIButtonStateNormal   = 0,
    mmUIButtonStateDisabled = 1,
    mmUIButtonStateHovering = 2,
    mmUIButtonStatePushed   = 3,
    mmUIButtonStateDraging  = 4,

    mmUIButtonStateMax,
};

enum mmUIButtonPipeline
{
    mmUIButtonPipelineOriginal = 0,
    mmUIButtonPipelineDisabled = 1,

    mmUIButtonPipelineMax,
};

struct mmUIButton
{
    // All frame must at same sheet.
    struct mmString hSheetName;
    struct mmString hFrameName[mmUIButtonStateMax];
    struct mmString hPipelineName[mmUIButtonPipelineMax];

    float hOpacity[mmUIButtonStateMax][4];

    int hDisabled;

    int hHovering;
    int hPushed;
};

void
mmUIButton_Init(
    struct mmUIButton*                             p);

void
mmUIButton_Destroy(
    struct mmUIButton*                             p);

int
mmUIButton_GetState(
    const struct mmUIButton*                       p);

int
mmUIButton_UpdateStateFrameName(
    const struct mmUIButton*                       p,
    int                                            state,
    struct mmString*                               pFrameName);

int
mmUIButton_UpdateStateOpacity(
    const struct mmUIButton*                       p,
    int                                            state,
    float                                          hOpacity[4]);

void
mmUIButton_UpdateViewFilter(
    const struct mmUIButton*                       p,
    struct mmUIView*                               pView);

void
mmUIButton_UpdatePipelineName(
    const struct mmUIButton*                       p,
    struct mmString*                               pPipelineName);

#include "core/mmSuffix.h"

#endif//__mmUIButton_h__
