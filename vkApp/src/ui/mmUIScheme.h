/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIScheme_h__
#define __mmUIScheme_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"
#include "container/mmRbtreeString.h"

#include "ui/mmUIParameter.h"

#include "core/mmPrefix.h"

struct mmGraphicsFactory;
struct mmFileContext;

struct mmUIScheme
{
    struct mmString hPath;
    struct mmUIParameterScheme hScheme;
    struct mmRbtreeStrVpt hColor;
    struct mmRbtreeStrVpt hStyle;
};

void
mmUIScheme_Init(
    struct mmUIScheme*                             p);

void
mmUIScheme_Destroy(
    struct mmUIScheme*                             p);

void
mmUIScheme_SetPath(
    struct mmUIScheme*                             p,
    const char*                                    pPath);

void
mmUIScheme_PrepareFont(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext);

void
mmUIScheme_DiscardFont(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext);

void
mmUIScheme_Prepare(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext);

void
mmUIScheme_Discard(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext);

void
mmUIScheme_GetColor(
    struct mmUIScheme*                             p,
    const struct mmString*                         pName,
    float                                          hColor[4]);

const struct mmUIParameterStyle*
mmUIScheme_GetStyle(
    struct mmUIScheme*                             p,
    const struct mmString*                         pName);

#include "core/mmSuffix.h"

#endif//__mmUIScheme_h__
