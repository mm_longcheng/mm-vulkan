/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewText.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIPropertyHelper.h"
#include "ui/mmUIEventArgs.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"

#include "nwsi/mmUTFHelper.h"

#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKCommandBuffer.h"

void
mmUIViewText_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewText, view));
    mmUIDrawable_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewText, drawable));
    mmUIText_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewText, text));
}

void
mmUIViewText_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIText_RemovePropertys(propertys);
    mmUIDrawable_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewText =
{
    &mmUIViewText_OnPrepare,
    &mmUIViewText_OnDiscard,
};

const struct mmUIWidgetVariable mmUIWidgetVariableViewText =
{
    &mmUIViewText_OnUpdate,
    &mmUIViewText_OnInvalidate,
    &mmUIViewText_OnSized,
    &mmUIViewText_OnChanged,
};

const struct mmUIWidgetDrawable mmUIWidgetDrawableViewText =
{
    &mmUIViewText_OnDescriptorSetProduce,
    &mmUIViewText_OnDescriptorSetRecycle,
    &mmUIViewText_OnDescriptorSetUpdate,
    &mmUIViewText_OnUpdatePushConstants,
    &mmUIViewText_OnUpdatePipeline,
    &mmUIViewText_OnRecord,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewText =
{
    "mmUIViewText",
    sizeof(struct mmUIViewText),
    &mmUIViewText_Produce,
    &mmUIViewText_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewText =
{
    &mmUIMetaAllocatorViewText,
    /*- Widget -*/
    &mmUIWidgetOperableViewText,
    &mmUIWidgetVariableViewText,
    &mmUIWidgetDrawableViewText,
    /*- Responder -*/
    NULL,
    NULL,
    NULL,
    NULL,
};

const char* mmUIViewText_EventTextChanged = "EventTextChanged";

void
mmUIViewText_Init(
    struct mmUIViewText*                           p)
{
    mmUIView_Init(&p->view);
    mmUIDrawable_Init(&p->drawable);
    mmUIText_Init(&p->text);

    // Text will apply display density.
    // The AspectMode must be mmUIAspectModeEntire.
    p->drawable.hAspectMode = mmUIAspectModeEntire;
}

void
mmUIViewText_Destroy(
    struct mmUIViewText*                           p)
{
    mmUIText_Destroy(&p->text);
    mmUIDrawable_Destroy(&p->drawable);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewText_Produce(
    struct mmUIViewText*                           p)
{
    mmUIViewText_Init(p);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewText;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);
    
    mmUIViewText_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewText_Recycle(
    struct mmUIViewText*                           p)
{
    mmUIViewText_RemovePropertys(&p->view.propertys);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewText;
    
    mmUIViewText_Destroy(p);
}

struct mmUtf16String*
mmUIViewText_GetTextUtf16String(
    struct mmUIViewText*                           p)
{
    return &p->text.vLayout.hText.hString;
}

void
mmUIViewText_SetRangeParameter(
    struct mmUIViewText*                           p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    mmTextLayout_SetRangeParameter(&p->text.vLayout, r, o, l, v);
}

void
mmUIViewText_SetRangeProperty(
    struct mmUIViewText*                           p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v)
{
    mmTextLayout_SetRangeProperty(&p->text.vLayout, r, t, v);
}

void
mmUIViewText_SpannableReset(
    struct mmUIViewText*                           p)
{
    struct mmTextLayout* pLayout;
    pLayout = &p->text.vLayout;
    mmUIText_UpdateColor(&p->text, &p->view);
    mmTextLayout_UpdateTextRange(pLayout);
    mmTextLayout_SpannableReset(pLayout);
}

void
mmUIViewText_UpdateColor(
    struct mmUIViewText*                           p)
{
    mmUIText_UpdateColor(&p->text, &p->view);
}

void
mmUIViewText_UpdateText(
    struct mmUIViewText*                           p)
{
    // Note: we must wait idle before update content.
    mmUIViewContext_DeviceWaitIdle(p->view.context);
    mmUIText_UpdateText(&p->text, &p->view, &p->drawable);
    mmUIViewText_OnDescriptorSetUpdate(p);
}

void
mmUIViewText_UpdateSize(
    struct mmUIViewText*                           p)
{
    // Note: we must wait idle before update content.
    mmUIViewContext_DeviceWaitIdle(p->view.context);
    mmUIText_UpdateSize(&p->text, &p->view, &p->drawable);
    mmUIViewText_OnDescriptorSetUpdate(p);
}

void
mmUIViewText_HandleTextChanged(
    struct mmUIViewText*                           p)
{
    struct mmEventUIViewArgs hArgs;
    struct mmEventSet* events = &p->view.events;

    mmUIViewText_OnChanged(p);

    mmEventUIViewArgs_Assign(&hArgs, 0, &p->view);
    mmEventSet_FireEvent(events, mmUIViewText_EventTextChanged, &hArgs);
}

void
mmUIViewText_OnPrepare(
    struct mmUIViewText*                           p)
{
    mmUIText_Prepare(
        &p->text,
        &p->view,
        &p->drawable);

    mmUIViewText_OnDescriptorSetProduce(p);
    mmUIViewText_OnDescriptorSetUpdate(p);
    mmUIViewText_OnUpdatePushConstants(p);
}

void
mmUIViewText_OnDiscard(
    struct mmUIViewText*                           p)
{
    mmUIViewText_OnDescriptorSetRecycle(p);

    mmUIText_Discard(
        &p->text,
        &p->view,
        &p->drawable);
}

void
mmUIViewText_OnUpdate(
    struct mmUIViewText*                           p,
    struct mmEventUpdate*                          content)
{
    // do nothing here, interface api.
}

void
mmUIViewText_OnInvalidate(
    struct mmUIViewText*                           p)
{
    mmUIViewText_OnUpdatePushConstants(p);
}

void
mmUIViewText_OnSized(
    struct mmUIViewText*                           p)
{
    mmUIViewText_UpdateSize(p);
    mmUIViewText_OnUpdatePushConstants(p);
}

void
mmUIViewText_OnChanged(
    struct mmUIViewText*                           p)
{
    mmUIViewText_UpdateText(p);
    mmUIViewText_OnUpdatePushConstants(p);
}

void
mmUIViewText_OnDescriptorSetProduce(
    struct mmUIViewText*                           p)
{
    mmUIDrawable_DescriptorSetProduce(&p->drawable);
}

void
mmUIViewText_OnDescriptorSetRecycle(
    struct mmUIViewText*                           p)
{
    mmUIDrawable_DescriptorSetRecycle(&p->drawable);
}

void
mmUIViewText_OnDescriptorSetUpdate(
    struct mmUIViewText*                           p)
{
    mmUIDrawable_DescriptorSetUpdate(
        &p->drawable,
        &p->view,
        &p->text.vImageInfo,
        p->text.pSamplerInfo);
}

void
mmUIViewText_OnUpdatePushConstants(
    struct mmUIViewText*                           p)
{
    mmUIDrawable_UpdatePushConstants(&p->drawable, &p->view);
}

void
mmUIViewText_OnUpdatePipeline(
    struct mmUIViewText*                           p)
{
    mmUIDrawable_UpdatePool(&p->drawable, &p->view);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void
mmUIViewText_OnRecord(
    struct mmUIViewText*                           p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIDrawable_OnRecord(&p->drawable, cmd->cmdBuffer);
    mmUIView_OnRecordChildren(&p->view, cmd);
}
