/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUISchemeLoader_h__
#define __mmUISchemeLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"
#include "container/mmRbtreeString.h"
#include "container/mmListString.h"

#include "ui/mmUIParameter.h"
#include "ui/mmUIScheme.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

struct mmGraphicsFactory;
struct mmFileContext;

struct mmUISchemeLoader
{
    struct mmGraphicsFactory* pGraphicsFactory;
    struct mmFileContext* pFileContext;
    struct mmRbtreeStringVpt sets;
    struct mmRbtreeStrVpt names;
    struct mmListString lists;
    struct mmParseJSON parse;
};

void
mmUISchemeLoader_Init(
    struct mmUISchemeLoader*                       p);

void
mmUISchemeLoader_Destroy(
    struct mmUISchemeLoader*                       p);

void
mmUISchemeLoader_SetGraphicsFactory(
    struct mmUISchemeLoader*                       p,
    struct mmGraphicsFactory*                      pGraphicsFactory);

void
mmUISchemeLoader_SetFileContext(
    struct mmUISchemeLoader*                       p,
    struct mmFileContext*                          pFileContext);

void
mmUISchemeLoader_AddPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path);

void
mmUISchemeLoader_RmvPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path);

void
mmUISchemeLoader_ClearPath(
    struct mmUISchemeLoader*                       p);

void
mmUISchemeLoader_Prepare(
    struct mmUISchemeLoader*                       p);

void
mmUISchemeLoader_Discard(
    struct mmUISchemeLoader*                       p);

void
mmUISchemeLoader_UnloadByPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path);

void
mmUISchemeLoader_UnloadComplete(
    struct mmUISchemeLoader*                       p);

struct mmUIScheme*
mmUISchemeLoader_LoadFromPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path);

struct mmUIScheme*
mmUISchemeLoader_GetFromPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path);

struct mmUIScheme*
mmUISchemeLoader_GetFromName(
    struct mmUISchemeLoader*                       p,
    const char*                                    name);

#include "core/mmSuffix.h"

#endif//__mmUISchemeLoader_h__
