/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewLayer.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIImage.h"

void
mmUIViewLayer_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewLayer, view));
}

void
mmUIViewLayer_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewLayer =
{
    &mmUIViewLayer_OnPrepare,
    &mmUIViewLayer_OnDiscard,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewLayer =
{
    "mmUIViewLayer",
    sizeof(struct mmUIViewLayer),
    &mmUIViewLayer_Produce,
    &mmUIViewLayer_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewLayer =
{
    &mmUIMetaAllocatorViewLayer,
    /*- Widget -*/
    &mmUIWidgetOperableViewLayer,
    NULL,
    NULL,
    /*- Responder -*/
    NULL,
    NULL,
    NULL,
    NULL,
};

void
mmUIViewLayer_Init(
    struct mmUIViewLayer*                          p)
{
    mmUIView_Init(&p->view);
}

void
mmUIViewLayer_Destroy(
    struct mmUIViewLayer*                          p)
{
    mmUIView_Destroy(&p->view);
}

void
mmUIViewLayer_Produce(
    struct mmUIViewLayer*                          p)
{
    mmUIViewLayer_Init(p);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewLayer;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);

    mmUIViewLayer_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewLayer_Recycle(
    struct mmUIViewLayer*                          p)
{
    mmUIViewLayer_RemovePropertys(&p->view.propertys);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewLayer;
    
    mmUIViewLayer_Destroy(p);
}

void
mmUIViewLayer_OnPrepare(
    struct mmUIViewLayer*                          p)
{

}

void
mmUIViewLayer_OnDiscard(
    struct mmUIViewLayer*                          p)
{

}
