/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewLoader.h"

#include "ui/mmUIJSON.h"
#include "ui/mmUIViewFactory.h"
#include "ui/mmUIParameter.h"
#include "ui/mmUIViewContext.h"

#include "core/mmLogger.h"

#include "dish/mmFileContext.h"

static
void 
mmUIViewLoader_ApplyStyle(
    struct mmUIView*                               pView,
    const struct mmUIViewContext*                  pViewContext,
    const struct mmString*                         pStyleName)
{
    do
    {
        size_t i;
        const struct mmUIParameterStyle* pStyle;
        const struct mmUIParameterProperty* u;
        const struct mmVectorValue* pProperty;
        const char* pName;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmString_Empty(pStyleName))
        {
            // the style name is empty.
            break;
        }

        pStyle = mmUIViewContext_GetSchemeStyle(pViewContext, pStyleName);
        if (NULL == pStyle)
        {
            // the style is invalid.
            mmLogger_LogW(gLogger, "%s %d"
                " pStyle is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        // Note: style configuration file must avoid circular dependency.
        mmUIViewLoader_ApplyStyle(pView, pViewContext, &pStyle->Style);

        pProperty = &pStyle->Property;
        for (i = 0; i < pProperty->size; ++i)
        {
            u = (const struct mmUIParameterProperty*)mmVectorValue_At(pProperty, i);
            pName = mmString_CStr(&u->Name);
            mmUIView_SetValueString(pView, pName, &u->Value);
        }
    } while (0);
}

void
mmUIViewLoader_Init(
    struct mmUIViewLoader*                         p)
{
    p->pFileContext = NULL;
    p->pViewFactory = NULL;
    mmParseJSON_Init(&p->parse);
    mmListVpt_Init(&p->invalid);
}

void
mmUIViewLoader_Destroy(
    struct mmUIViewLoader*                         p)
{
    assert(0 == p->invalid.size && "assets is not destroy complete.");
    mmListVpt_Destroy(&p->invalid);
    mmParseJSON_Destroy(&p->parse);
    p->pViewFactory = NULL;
    p->pFileContext = NULL;
}

void
mmUIViewLoader_SetFileContext(
    struct mmUIViewLoader*                         p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmUIViewLoader_SetViewFactory(
    struct mmUIViewLoader*                         p,
    struct mmUIViewFactory*                        pViewFactory)
{
    p->pViewFactory = pViewFactory;
}

struct mmUIView*
mmUIViewLoader_ProduceViewFromInfo(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    struct mmUIParameterView*                      pParameter)
{
    struct mmUIView* pView = NULL;
    
    do
    {
        size_t i;
        const char* pName = "";
        struct mmUIParameterProperty* u = NULL;
        struct mmUIParameterView* e = NULL;
        struct mmUIView* c = NULL;
        
        struct mmVectorValue* pProperty = NULL;
        struct mmVectorValue* pChildren = NULL;

        const char* pType = "";
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == pParameter)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pParameter is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        pType = mmString_CStr(&pParameter->Type);
        pView = mmUIViewFactory_Produce(p->pViewFactory, pType);
        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView Type: %s is invalid.", __FUNCTION__, __LINE__, pType);
            break;
        }
        
        mmUIView_SetContext(pView, pViewContext);
        
        pProperty = &pParameter->Property;
        pChildren = &pParameter->Children;
        
        mmUIViewLoader_ApplyStyle(pView, pViewContext, &pParameter->Style);

        for (i = 0; i < pProperty->size; ++i)
        {
            u = (struct mmUIParameterProperty*)mmVectorValue_At(pProperty, i);
            pName = mmString_CStr(&u->Name);
            mmUIView_SetValueString(pView, pName, &u->Value);
        }
        
        for (i = 0; i < pChildren->size; ++i)
        {
            e = (struct mmUIParameterView*)mmVectorValue_At(pChildren, i);
            c = mmUIViewLoader_ProduceViewFromInfo(p, pViewContext, e);
            mmUIView_AddChild(pView, c);
        }
        
        // Do not OnPrepare view here, we do this outside.
        // mmUIView_OnPrepare(pView);
    } while (0);
    
    return pView;
}

struct mmUIView*
mmUIViewLoader_ProduceViewFromFile(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    const char*                                    path)
{
    struct mmUIView* pView = NULL;
    
    do
    {
        struct mmUIParameterView hArgs;
        
        int i;
        
        const uint8_t* data = NULL;
        size_t size = 0;
        struct mmByteBuffer bytes;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            break;
        }
        
        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);
        
        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        data = bytes.buffer + bytes.offset;
        size = bytes.length;
        
        mmUIParameterView_Init(&hArgs);
        
        mmParseJSON_Reset(&p->parse);
        mmParseJSON_SetJsonBuffer(&p->parse, (const char*)data, (size_t)size);
        mmParseJSON_Analysis(&p->parse);
        i = mmParseJSON_DecodeObject(&p->parse, 0, &hArgs, &mmParseJSON_DecodeUIView);
        if (i <= 0)
        {
            if (p->parse.code <= 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON AnalysisMetadata failure.", __FUNCTION__, __LINE__);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON root is invalid.", __FUNCTION__, __LINE__);
            }

            // do not break here.
        }
        else
        {
            pView = mmUIViewLoader_ProduceViewFromInfo(p, pViewContext, &hArgs);
        }
        
        mmUIParameterView_Destroy(&hArgs);
        
        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);
    
    return pView;
}

void
mmUIViewLoader_RecycleView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView)
{
    do
    {
        struct mmUIView* child = NULL;
        struct mmListHead* prev = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        
        struct mmListVpt* children = NULL;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        children = &pView->children;
        
        // Do not OnDiscard view here, we do this outside.
        // mmUIView_OnDiscard(pView);

        prev = children->l.prev;
        while (prev != &children->l)
        {
            curr = prev;
            prev = prev->prev;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            child = (struct mmUIView*)it->v;

            mmUIView_RmvChild(pView, child);
            mmUIViewLoader_RecycleView(p, child);
        }
        
        mmUIViewLoader_InvalidView(p, pView);
    } while (0);
}

void
mmUIViewLoader_PrepareViewFromInfo(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView,
    struct mmUIParameterView*                      pParameter)
{
    do
    {
        size_t i;
        const char* pName = "";
        struct mmUIParameterProperty* u = NULL;
        struct mmUIParameterView* e = NULL;
        struct mmUIView* c = NULL;

        struct mmVectorValue* pProperty = NULL;
        struct mmVectorValue* pChildren = NULL;

        const char* pTypeParameter = "";
        const char* pTypeAllocator = "";

        const struct mmUIMetadata* metadata = NULL;
        const struct mmMetaAllocator* Allocator = NULL;

        struct mmUIViewContext* pViewContext = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pParameter)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pParameter is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pViewContext = pView->context;
        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        metadata = pView->metadata;
        Allocator = metadata->Allocator;

        assert(NULL != metadata && "metadata is invalid.");
        assert(NULL != Allocator && "metadata is invalid.");

        pTypeParameter = mmString_CStr(&pParameter->Type);
        pTypeAllocator = Allocator->TypeName;
        if (0 != mmString_CompareCStr(&pParameter->Type, pTypeAllocator))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " View TypeParameter: %s  TypeAllocator: %s are not the same.",
                __FUNCTION__, __LINE__, pTypeParameter, pTypeAllocator);
            break;
        }

        pProperty = &pParameter->Property;
        pChildren = &pParameter->Children;

        mmUIViewLoader_ApplyStyle(pView, pViewContext, &pParameter->Style);

        for (i = 0; i < pProperty->size; ++i)
        {
            u = (struct mmUIParameterProperty*)mmVectorValue_At(pProperty, i);
            pName = mmString_CStr(&u->Name);
            mmUIView_SetValueString(pView, pName, &u->Value);
        }

        for (i = 0; i < pChildren->size; ++i)
        {
            e = (struct mmUIParameterView*)mmVectorValue_At(pChildren, i);
            c = mmUIViewLoader_ProduceViewFromInfo(p, pViewContext, e);
            mmUIView_AddChild(pView, c);
        }

    } while (0);
}

void
mmUIViewLoader_PrepareViewFromFile(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView,
    const char*                                    path)
{
    do
    {
        struct mmUIParameterView hArgs;

        int i;

        const uint8_t* data = NULL;
        size_t size = 0;
        struct mmByteBuffer bytes;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        mmUIParameterView_Init(&hArgs);

        mmParseJSON_Reset(&p->parse);
        mmParseJSON_SetJsonBuffer(&p->parse, (const char*)data, (size_t)size);
        mmParseJSON_Analysis(&p->parse);
        i = mmParseJSON_DecodeObject(&p->parse, 0, &hArgs, &mmParseJSON_DecodeUIView);
        if (i <= 0)
        {
            if (p->parse.code <= 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON AnalysisMetadata failure.", __FUNCTION__, __LINE__);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON root is invalid.", __FUNCTION__, __LINE__);
            }

            // do not break here.
        }
        else
        {
            mmUIViewLoader_PrepareViewFromInfo(p, pView, &hArgs);
        }

        mmUIParameterView_Destroy(&hArgs);

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);
}

void
mmUIViewLoader_DiscardView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView)
{
    do
    {
        struct mmUIView* child = NULL;
        struct mmListHead* prev = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;

        struct mmListVpt* children = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        children = &pView->children;

        prev = children->l.prev;
        while (prev != &children->l)
        {
            curr = prev;
            prev = prev->prev;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            child = (struct mmUIView*)it->v;

            mmUIView_RmvChild(pView, child);
            mmUIViewLoader_RecycleView(p, child);
        }

    } while (0);
}

struct mmUIView*
mmUIViewLoader_ProduceViewType(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    const char*                                    pType,
    const char*                                    pName)
{
    struct mmUIView* pView = NULL;
    
    do
    {
        struct mmString hNameWeak;
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        
        pView = mmUIViewFactory_Produce(p->pViewFactory, pType);
        if (NULL == pView)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pView Type: %s is invalid.", __FUNCTION__, __LINE__, pType);
            break;
        }
        
        mmString_MakeWeaks(&hNameWeak, pName);
        mmUIView_SetContext(pView, pViewContext);
        mmUIView_SetName(pView, &hNameWeak);

        // Do not OnPrepare view here, we do this outside.
        // mmUIView_OnPrepare(pView);
    } while (0);
    
    return pView;
}

void
mmUIViewLoader_RecycleViewType(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pViewFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pView)
        {
            // pView is NULL, not need Recycle.
            // we need logger nothing.
            break;
        }

        // Do not OnDiscard view here, we do this outside.
        // mmUIView_OnDiscard(pView);

        mmUIViewLoader_InvalidView(p, pView);
    } while (0);
}

void
mmUIViewLoader_InvalidView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView)
{
    mmListVpt_AddTail(&p->invalid, (void*)pView);
}

void
mmUIViewLoader_DestroyInvalid(
    struct mmUIViewLoader*                         p)
{
    struct mmUIView* pView = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    struct mmListVpt* list = &p->invalid;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        pView = (struct mmUIView*)it->v;
        mmListVpt_Erase(list, it);
        mmUIViewFactory_Recycle(p->pViewFactory, pView);
    }
}
