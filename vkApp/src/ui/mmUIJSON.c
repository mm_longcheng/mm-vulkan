/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIJSON.h"

#include "ui/mmUIParameter.h"

int
mmParseJSON_DecodeUIProperty(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     m,
    void*                                          o)
{
    struct mmUIParameterProperty* v = (struct mmUIParameterProperty*)(o);

    i = mmParseJSON_DecodeString(p, i, &v->Name, "");

    if (i < 0)
    {
        return i;
    }

    i = mmParseJSON_DecodeString(p, i, &v->Value, "");

    if (i < 0)
    {
        return i;
    }

    return i;
}

int
mmParseJSON_DecodeUIView(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmUIParameterView* v = (struct mmUIParameterView*)(o);
    switch (h)
    {
    case 0xCAA43ABB:
        // 0xCAA43ABB Type
        i = mmParseJSON_DecodeString(p, ++i, &v->Type, "");
        break;
    case 0x656A0E2B:
        // 0x656A0E2B Style
        i = mmParseJSON_DecodeString(p, ++i, &v->Style, "");
        break;
    case 0x837DD2FF:
        // 0x837DD2FF Property
        i = mmParseJSON_DecodeMember(p, ++i, &v->Property, &mmParseJSON_DecodeUIProperty);
        break;
    case 0xF361DB51:
        // 0xF361DB51 Children
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->Children, &mmParseJSON_DecodeUIView);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeUIStyle(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmUIParameterStyle* v = (struct mmUIParameterStyle*)(o);
    switch (h)
    {
    case 0xFD1FD10B:
        // 0xFD1FD10B Name
        i = mmParseJSON_DecodeString(p, ++i, &v->Name, "");
        break;
    case 0x656A0E2B:
        // 0x656A0E2B Style
        i = mmParseJSON_DecodeString(p, ++i, &v->Style, "");
        break;
    case 0xCAA43ABB:
        // 0xCAA43ABB Type
        i = mmParseJSON_DecodeString(p, ++i, &v->Type, "");
        break;
    case 0x837DD2FF:
        // 0x837DD2FF Property
        i = mmParseJSON_DecodeMember(p, ++i, &v->Property, &mmParseJSON_DecodeUIProperty);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeUIFont(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmUIParameterFont* v = (struct mmUIParameterFont*)(o);
    switch (h)
    {
    case 0xB65BC395:
        // 0xB65BC395 Family
        i = mmParseJSON_DecodeString(p, ++i, &v->Family, "");
        break;
    case 0xB6556A17:
        // 0xB6556A17 Path
        i = mmParseJSON_DecodeString(p, ++i, &v->Path, "");
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeUIScheme(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmUIParameterScheme* v = (struct mmUIParameterScheme*)(o);
    switch (h)
    {
    case 0xFD1FD10B:
        // 0xFD1FD10B Name
        i = mmParseJSON_DecodeString(p, ++i, &v->Name, "");
        break;
    case 0x8944C440:
        // 0x8944C440 Font
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->Font, &mmParseJSON_DecodeUIFont);
        break;
    case 0xB5A59043:
        // 0xB5A59043 Color
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->Color, &mmParseJSON_DecodeUIProperty);
        break;
    case 0x656A0E2B:
        // 0x656A0E2B Style
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->Style, &mmParseJSON_DecodeUIStyle);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}
