/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIDrawable.h"

#include "ui/mmUIPropertyHelper.h"
#include "ui/mmUIView.h"
#include "ui/mmUIViewContext.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKDevice.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKSamplerPool.h"
#include "vk/mmVKImageLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"

#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix4x4.h"

#include "algorithm/mmMurmurHash.h"

const char*
mmUIImageMode_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case mmUIImageModeBasic:
        return "Basic";
    case mmUIImageModeSlice:
        return "Slice";
    case mmUIImageModeTiled:
        return "Tiled";
    case mmUIImageModeBrush:
        return "Brush";
    default:
        return "Basic";
    }
}

int
mmUIImageMode_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x6FE77119:
        // 0x6FE77119 Basic
        return mmUIImageModeBasic;
    case 0xC54C8768:
        // 0xC54C8768 Slice
        return mmUIImageModeSlice;
    case 0xD824AC25:
        // 0xD824AC25 Tiled
        return mmUIImageModeTiled;
    case 0xAA800B12:
        // 0xAA800B12 Brush
        return mmUIImageModeBrush;
    default:
        return mmUIImageModeBasic;
    }
}

const char*
mmUIImageBrushMode_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case mmUIImageBrushLinear:
        return "Linear";
    case mmUIImageBrushCircle:
        return "Circle";
    default:
        return "Linear";
    }
}

int
mmUIImageBrushMode_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x4170F1D3:
        // 0x4170F1D3 Linear
        return mmUIImageBrushLinear;
    case 0x12D7EE0B:
        // 0x12D7EE0B Circle
        return mmUIImageBrushCircle;
    default:
        return mmUIImageBrushLinear;
    }
}

typedef
VkResult
(*mmUIDrawableBufferFunc)(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               view,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

static const mmUIDrawableBufferFunc gUIDrawablePrepareBufferFuncArray[] =
{
    &mmUIDrawable_PrepareBufferBasic,
    &mmUIDrawable_PrepareBufferSlice,
    &mmUIDrawable_PrepareBufferTiled,
    &mmUIDrawable_PrepareBufferBrush,
};

static const mmUIDrawableBufferFunc gUIDrawableUpdateBufferFuncArray[] =
{
    &mmUIDrawable_UpdateBufferBasic,
    &mmUIDrawable_UpdateBufferSlice,
    &mmUIDrawable_UpdateBufferTiled,
    &mmUIDrawable_UpdateBufferBrush,
};

static const mmUIDrawableBufferFunc gUIDrawablePrepareBufferBrushFuncArray[] =
{
    &mmUIDrawable_PrepareBufferBrushLinear,
    &mmUIDrawable_PrepareBufferBrushCircle,
};

static const mmUIDrawableBufferFunc gUIDrawableUpdateBufferBrushFuncArray[] =
{
    &mmUIDrawable_UpdateBufferBrushLinear,
    &mmUIDrawable_UpdateBufferBrushCircle,
};

void
mmUIDrawable_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIDrawable";

    mmPropertyDefineMutable(propertys,
        "Drawable.Amount", offset, struct mmUIDrawable,
        hAmount, Float, "1",
        NULL,
        NULL,
        cOrigin,
        "Drawable Amount float range[0, 1] default 1.");

    mmPropertyDefineMutable(propertys,
        "Drawable.Opacity", offset, struct mmUIDrawable,
        hOpacity, Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Drawable Opacity float[4] default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Drawable.Color", offset, struct mmUIDrawable,
        hValueColor, String, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Drawable Color float[4] or #AARRGGBB #RRGGBB default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Drawable.AspectRatio", offset, struct mmUIDrawable,
        hAspectRatio, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable AspectRatio float default 0.");

    mmPropertyDefineMutable(propertys,
        "Drawable.AspectMode", offset, struct mmUIDrawable,
        hAspectMode, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable AspectMode UInt8 default mmUIAspectModeIgnore(0).");

    mmPropertyDefineMutable(propertys,
        "Drawable.Alignment", offset, struct mmUIDrawable,
        hAlign, UInt8Vec3, "(1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Drawable Align UInt8[3] default mmUIAlignmentCentrum(1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Drawable.ImageMode", offset, struct mmUIDrawable,
        hImageMode, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable ImageMode UInt8 default mmUIImageModeBasic(0).");

    mmPropertyDefineMutable(propertys,
        "Drawable.BrushMode", offset, struct mmUIDrawable,
        hBrushMode, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable BrushMode UInt8 default mmUIImageBrushHorizontal(0).");

    mmPropertyDefineMutable(propertys,
        "Drawable.Direction", offset, struct mmUIDrawable,
        hDirection, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable Direction UInt8 default 0.");

    mmPropertyDefineMutable(propertys,
        "Drawable.Reverse", offset, struct mmUIDrawable,
        hReverse, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Drawable Reverse UInt8 default 0.");

    mmPropertyDefineMutable(propertys,
        "Drawable.Flipped", offset, struct mmUIDrawable,
        hFlipped, UInt8Vec2, "(0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Drawable Flipped UInt8[2] default (0, 0).");

    mmPropertyDefineMutable(propertys,
        "Drawable.PipelineName", offset, struct mmUIDrawable,
        hPipelineName, String, "mmUIImageOriginal",
        NULL,
        NULL,
        cOrigin,
        "Drawable PipelineName string default \"mmUIImageOriginal\".");

    mmPropertyDefineMutable(propertys,
        "Drawable.PoolName", offset, struct mmUIDrawable,
        hPoolName, String, "mmUIImageOriginal",
        NULL,
        NULL,
        cOrigin,
        "Drawable PoolName string default \"mmUIImageOriginal\".");
}

void
mmUIDrawable_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmPropertyRemove(propertys, "Drawable.Amount");
    mmPropertyRemove(propertys, "Drawable.Opacity");
    mmPropertyRemove(propertys, "Drawable.Color");
    mmPropertyRemove(propertys, "Drawable.AspectRatio");
    mmPropertyRemove(propertys, "Drawable.AspectMode");
    mmPropertyRemove(propertys, "Drawable.Alignment");
    mmPropertyRemove(propertys, "Drawable.ImageMode");
    mmPropertyRemove(propertys, "Drawable.BrushMode");
    mmPropertyRemove(propertys, "Drawable.Direction");
    mmPropertyRemove(propertys, "Drawable.Reverse");
    mmPropertyRemove(propertys, "Drawable.Flipped");
    mmPropertyRemove(propertys, "Drawable.PipelineName");
    mmPropertyRemove(propertys, "Drawable.PoolName");
}

void
mmUIDrawable_Init(
    struct mmUIDrawable*                           p)
{
    mmVKBuffer_Init(&p->vIdxBuffer);
    mmVKBuffer_Init(&p->vVtxBuffer);

    mmUIImageFrameReset(&p->vIframe);
    mmMemset(&p->vConstant, 0, sizeof(struct mmUIDrawablePushConstant));

    mmVKDescriptorSet_Init(&p->vDescriptorSet);

    p->pPipelineInfo = NULL;
    p->pPoolInfo = NULL;

    p->hIdxCount = 0;
    p->hVtxCount = 0;

    p->hIdxType = VK_INDEX_TYPE_UINT16;

    p->hAmount = 1.0f;
    mmVec4Make(p->hOpacity, 1.0f, 1.0f, 1.0f, 1.0f);
    mmVec4Make(p->hColor, 1.0f, 1.0f, 1.0f, 1.0f);

    p->hAspectRatio = 0.0f;

    p->hAspectMode = (mmUInt8_t)mmUIAspectModeIgnore;
    mmMemset(p->hAlign, (mmUInt8_t)mmUIAlignmentMiddled, sizeof(p->hAlign));
    p->hImageMode = (mmUInt8_t)mmUIImageModeBasic;

    p->hBrushMode = (mmUInt8_t)mmUIImageBrushLinear;
    p->hDirection = (mmUInt8_t)0;
    p->hReverse = (mmUInt8_t)0;

    p->hFlipped[0] = 0;
    p->hFlipped[1] = 0;

    mmString_Init(&p->hPipelineName);
    mmString_Init(&p->hPoolName);

    mmString_Init(&p->hValueColor);

    mmString_Assigns(&p->hPipelineName, "mmUIImageOriginal");
    mmString_Assigns(&p->hPoolName, "mmUIImageOriginal");
}

void
mmUIDrawable_Destroy(
    struct mmUIDrawable*                           p)
{
    mmString_Destroy(&p->hValueColor);

    mmString_Destroy(&p->hPoolName);
    mmString_Destroy(&p->hPipelineName);

    p->hFlipped[0] = 0;
    p->hFlipped[1] = 0;

    p->hReverse = (mmUInt8_t)0;
    p->hDirection = (mmUInt8_t)0;
    p->hBrushMode = (mmUInt8_t)mmUIImageBrushLinear;

    p->hImageMode = (mmUInt8_t)mmUIImageModeBasic;
    mmMemset(p->hAlign, (mmUInt8_t)mmUIAlignmentMiddled, sizeof(p->hAlign));
    p->hAspectMode = (mmUInt8_t)mmUIAspectModeIgnore;

    p->hAspectRatio = 0.0f;

    mmVec4Make(p->hColor, 1.0f, 1.0f, 1.0f, 1.0f);
    mmVec4Make(p->hOpacity, 1.0f, 1.0f, 1.0f, 1.0f);
    p->hAmount = 1.0f;

    p->hIdxType = VK_INDEX_TYPE_UINT16;

    p->hVtxCount = 0;
    p->hIdxCount = 0;

    p->pPoolInfo = NULL;
    p->pPipelineInfo = NULL;

    mmVKDescriptorSet_Destroy(&p->vDescriptorSet);

    mmMemset(&p->vConstant, 0, sizeof(struct mmUIDrawablePushConstant));
    mmUIImageFrameReset(&p->vIframe);

    mmVKBuffer_Destroy(&p->vVtxBuffer);
    mmVKBuffer_Destroy(&p->vIdxBuffer);
}

void
mmUIDrawable_SetPipelineCStr(
    struct mmUIDrawable*                           p,
    const char*                                    name)
{
    mmString_Assigns(&p->hPipelineName, name);
}

VkResult
mmUIDrawable_PrepareBufferBasic(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        mmUIVertex4AssignFrameBasic(hVtx, p->hFlipped, iframe);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        p->hIdxCount = MM_ARRAY_SIZE(hIdx);
        p->hVtxCount = MM_ARRAY_SIZE(hVtx);

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            (uint8_t*)hIdx,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)hVtx,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIDrawable_PrepareBufferSlice(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[16];
        uint16_t hIdx[54];
        float size[3];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);
        mmUIView_GetWorldSize(pView, size);

        mmUIVertex16AssignFrameSlice(hVtx, p->hFlipped, iframe, size);
        mmUIVertex16AssignColor(hVtx, p->hColor);
        mmUIVertex16AssignIndex16(hIdx, 0);

        p->hIdxCount = MM_ARRAY_SIZE(hIdx);
        p->hVtxCount = MM_ARRAY_SIZE(hVtx);

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            (uint8_t*)hIdx,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)hVtx,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIDrawable_PrepareBufferTiled(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];
        float size[3];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);
        mmUIView_GetWorldSize(pView, size);

        mmUIVertex4AssignFrameTiled(hVtx, p->hFlipped, iframe, size);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        p->hIdxCount = MM_ARRAY_SIZE(hIdx);
        p->hVtxCount = MM_ARRAY_SIZE(hVtx);

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            (uint8_t*)hIdx,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)hVtx,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}


VkResult
mmUIDrawable_PrepareBufferBrush(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    if (0 <= p->hBrushMode && p->hBrushMode < MM_ARRAY_SIZE(gUIDrawablePrepareBufferBrushFuncArray))
    {
        const mmUIDrawableBufferFunc func = gUIDrawablePrepareBufferBrushFuncArray[p->hBrushMode];
        return (*(func))(p, pView, pUploader, iframe);
    }
    else
    {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }
}

VkResult
mmUIDrawable_PrepareBufferBrushLinear(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        float ox[2] = { 0, 1 };
        float oy[2] = { 0, 1 };

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        if (0 == p->hDirection)
        {
            ox[0] = (0 == p->hReverse) ? /*  */0.0f : (1.0f - p->hAmount);
            ox[1] = (0 == p->hReverse) ? p->hAmount : /*           */1.0f;
        }
        else
        {
            oy[0] = (0 == p->hReverse) ? /*  */0.0f : (1.0f - p->hAmount);
            oy[1] = (0 == p->hReverse) ? p->hAmount : /*           */1.0f;
        }

        mmUIVertex4AssignFrameBrush(hVtx, p->hFlipped, iframe, ox, oy);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        p->hIdxCount = MM_ARRAY_SIZE(hIdx);
        p->hVtxCount = MM_ARRAY_SIZE(hVtx);

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            (uint8_t*)hIdx,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)hVtx,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIDrawable_PrepareBufferBrushCircle(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        int segment;

        struct mmUIVertex hVtx[10];
        uint16_t hIdx[24];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        mmUIVertex10AssignFrameBrush(
            hVtx, 
            p->hFlipped, 
            iframe, 
            p->hAmount, 
            p->hDirection, 
            p->hReverse, 
            &segment);

        mmUIVertex10AssignColor(hVtx, p->hColor);
        mmUIVertex10AssignIndex16(hIdx, 0, segment, p->hDirection, p->hReverse);

        p->hIdxCount = MM_ARRAY_SIZE(hIdx);
        p->hVtxCount = MM_ARRAY_SIZE(hVtx);

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            (uint8_t*)hIdx,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)hVtx,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIDrawable_PrepareBuffer(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    if (0 <= p->hImageMode && p->hImageMode < MM_ARRAY_SIZE(gUIDrawablePrepareBufferFuncArray))
    {
        const mmUIDrawableBufferFunc func = gUIDrawablePrepareBufferFuncArray[p->hImageMode];
        return (*(func))(p, pView, pUploader, iframe);
    }
    else
    {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }
}

void
mmUIDrawable_DiscardBuffer(
    struct mmUIDrawable*                           p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteBuffer(pUploader, &p->vVtxBuffer);
    mmVKUploader_DeleteBuffer(pUploader, &p->vIdxBuffer);
}

VkResult
mmUIDrawable_UpdateBufferBasic(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        assert(p->hIdxType == VK_INDEX_TYPE_UINT16);

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        mmUIVertex4AssignFrameBasic(hVtx, p->hFlipped, iframe);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        assert(p->hIdxCount == MM_ARRAY_SIZE(hIdx));
        assert(p->hVtxCount == MM_ARRAY_SIZE(hVtx));

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hIdx,
            (size_t)0,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hVtx,
            (size_t)0,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdateBufferSlice(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[16];
        uint16_t hIdx[54];
        float size[3];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        assert(p->hIdxType == VK_INDEX_TYPE_UINT16);

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);
        mmUIView_GetWorldSize(pView, size);

        mmUIVertex16AssignFrameSlice(hVtx, p->hFlipped, iframe, size);
        mmUIVertex16AssignColor(hVtx, p->hColor);
        mmUIVertex16AssignIndex16(hIdx, 0);

        assert(p->hIdxCount == MM_ARRAY_SIZE(hIdx));
        assert(p->hVtxCount == MM_ARRAY_SIZE(hVtx));

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hIdx,
            (size_t)0,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hVtx,
            (size_t)0,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdateBufferTiled(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];
        float size[3];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        assert(p->hIdxType == VK_INDEX_TYPE_UINT16);

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);
        mmUIView_GetWorldSize(pView, size);

        mmUIVertex4AssignFrameTiled(hVtx, p->hFlipped, iframe, size);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        assert(p->hIdxCount == MM_ARRAY_SIZE(hIdx));
        assert(p->hVtxCount == MM_ARRAY_SIZE(hVtx));

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hIdx,
            (size_t)0,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hVtx,
            (size_t)0,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdateBufferBrush(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    if (0 <= p->hBrushMode && p->hBrushMode < MM_ARRAY_SIZE(gUIDrawableUpdateBufferBrushFuncArray))
    {
        const mmUIDrawableBufferFunc func = gUIDrawableUpdateBufferBrushFuncArray[p->hBrushMode];
        return (*(func))(p, pView, pUploader, iframe);
    }
    else
    {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }
}

VkResult
mmUIDrawable_UpdateBufferBrushLinear(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        float ox[2] = { 0, 1 };
        float oy[2] = { 0, 1 };

        struct mmUIVertex hVtx[4];
        uint16_t hIdx[6];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        p->hIdxType = VK_INDEX_TYPE_UINT16;

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        if (0 == p->hDirection)
        {
            ox[0] = (0 == p->hReverse) ? /*  */0.0f : (1.0f - p->hAmount);
            ox[1] = (0 == p->hReverse) ? p->hAmount : /*           */1.0f;
        }
        else
        {
            oy[0] = (0 == p->hReverse) ? /*  */0.0f : (1.0f - p->hAmount);
            oy[1] = (0 == p->hReverse) ? p->hAmount : /*           */1.0f;
        }

        mmUIVertex4AssignFrameBrush(hVtx, p->hFlipped, iframe, ox, oy);
        mmUIVertex4AssignColor(hVtx, p->hColor);
        mmUIVertex4AssignIndex16(hIdx, 0);

        assert(p->hIdxCount == MM_ARRAY_SIZE(hIdx));
        assert(p->hVtxCount == MM_ARRAY_SIZE(hVtx));

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hIdx,
            (size_t)0,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hVtx,
            (size_t)0,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdateBufferBrushCircle(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDeviceSize hIdxBufferSize = 0;
        VkDeviceSize hVtxBufferSize = 0;

        int segment;

        struct mmUIVertex hVtx[10];
        uint16_t hIdx[24];

        struct mmLogger* gLogger = mmLogger_Instance();

        p->vIframe = (*iframe);

        assert(p->hIdxType == VK_INDEX_TYPE_UINT16);

        mmUIView_GetSchemeColor(pView, &p->hValueColor, p->hColor);

        mmUIVertex10AssignFrameBrush(
            hVtx, 
            p->hFlipped, 
            iframe, 
            p->hAmount,
            p->hDirection, 
            p->hReverse, 
            &segment);

        mmUIVertex10AssignColor(hVtx, p->hColor);
        mmUIVertex10AssignIndex16(hIdx, 0, segment, p->hDirection, p->hReverse);

        assert(p->hIdxCount == MM_ARRAY_SIZE(hIdx));
        assert(p->hVtxCount == MM_ARRAY_SIZE(hVtx));

        hIdxBufferSize = p->hIdxCount * mmVKIndexTypeSize(p->hIdxType);
        hVtxBufferSize = p->hVtxCount * sizeof(struct mmUIVertex);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hIdx,
            (size_t)0,
            (size_t)hIdxBufferSize,
            &p->vIdxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)hVtx,
            (size_t)0,
            (size_t)hVtxBufferSize,
            &p->vVtxBuffer,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdateBuffer(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe)
{
    if (0 <= p->hImageMode && p->hImageMode < MM_ARRAY_SIZE(gUIDrawableUpdateBufferFuncArray))
    {
        const mmUIDrawableBufferFunc func = gUIDrawableUpdateBufferFuncArray[p->hImageMode];
        return (*(func))(p, pView, pUploader, iframe);
    }
    else
    {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }
}

VkResult
mmUIDrawable_PreparePipeline(
    struct mmUIDrawable*                           p,
    struct mmVKAssets*                             pAssets)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineLoader* pPipelineLoader = NULL;
        const char* pPipelineName = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        assert(mmVKPipelineInfo_Invalid(p->pPipelineInfo) && "assets is not destroy complete.");

        pPipelineLoader = &pAssets->vPipelineLoader;
        pPipelineName = mmString_CStr(&p->hPipelineName);
        p->pPipelineInfo = mmVKPipelineLoader_GetFromName(pPipelineLoader, pPipelineName);
        if (mmVKPipelineInfo_Invalid(p->pPipelineInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPipelineInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPipelineInfo = NULL;
            break;
        }
        mmVKPipelineInfo_Increase(p->pPipelineInfo);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmUIDrawable_DiscardPipeline(
    struct mmUIDrawable*                           p)
{
    mmVKPipelineInfo_Decrease(p->pPipelineInfo);
    p->pPipelineInfo = NULL;
}

VkResult
mmUIDrawable_PreparePool(
    struct mmUIDrawable*                           p,
    struct mmVKAssets*                             pAssets)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPoolLoader* pPoolLoader = NULL;
        const char* pPoolName = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        assert(mmVKPoolInfo_Invalid(p->pPoolInfo) && "assets is not destroy complete.");

        pPoolLoader = &pAssets->vPoolLoader;
        pPoolName = mmString_CStr(&p->hPoolName);
        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, pPoolName);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmUIDrawable_DiscardPool(
    struct mmUIDrawable*                           p)
{
    mmVKPoolInfo_Decrease(p->pPoolInfo);
    p->pPoolInfo = NULL;
}

VkResult
mmUIDrawable_UpdatePool(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (!mmVKPoolInfo_Invalid(p->pPoolInfo) && 
            0 == mmString_Compare(&p->pPoolInfo->name, &p->hPoolName))
        {
            // not need update.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmUIDrawable_DiscardPool(p);

        err = mmUIDrawable_PreparePool(p, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmUIDrawable_UpdatePipeline(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (!mmVKPipelineInfo_Invalid(p->pPipelineInfo) &&
            0 == mmString_Compare(&p->pPipelineInfo->name, &p->hPipelineName))
        {
            // not need update.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmUIDrawable_DiscardPipeline(p);

        err = mmUIDrawable_PreparePipeline(p, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmUIDrawable_GetMat4Model(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    float                                          model[4][4])
{
    int* original = p->vIframe.original;

    float hAspectRatio = p->hAspectRatio;
    mmUInt8_t hAspectMode = p->hAspectMode;
    mmUInt8_t hImageMode = p->hImageMode;
    const mmUInt8_t* pAlign = p->hAlign;

    float s[3] = { 1, 1, 1 };
    float osize[3] = { (float)original[0], (float)original[1], 0 };
    float rsize[3];
    float wsize[3];

    float offset0[3];
    float offset1[3];
    float offset2[3];
    float offset3[3];

    /* frame size sliced mode is window size */
    float* fsize = (mmUIImageModeBasic == hImageMode ||
                    mmUIImageModeBrush == hImageMode) ? osize : wsize;

    assert(NULL != pView && "pView is invalid.");

    // Note:
    // Not WorldSize, it is LocalSize.
    // WorldTransform already include scale.
    mmUIView_GetLocalSize(pView, wsize);
    mmUIView_GetWorldTransform(pView, model);

    mmVec2SizeAspectToScale(wsize, fsize, hAspectMode, hAspectRatio, s);
    mmVec3Mul(rsize, fsize, s);

    offset0[0] = -rsize[0] * mmVec3UnitHalf[0];
    offset0[1] = -rsize[1] * mmVec3UnitHalf[1];
    offset0[2] = -rsize[2] * mmVec3UnitHalf[2];

    offset2[0] = (wsize[0] - rsize[0]) * (pAlign[0] - 1) * 0.5f;
    offset2[1] = (wsize[1] - rsize[1]) * (pAlign[1] - 1) * 0.5f;
    offset2[2] = (wsize[2] - rsize[2]) * (pAlign[2] - 1) * 0.5f;

    offset1[0] = wsize[0] * (pView->anchor[0] - mmVec3UnitHalf[0]);
    offset1[1] = wsize[1] * (pView->anchor[1] - mmVec3UnitHalf[1]);
    offset1[2] = wsize[2] * (pView->anchor[2] - mmVec3UnitHalf[2]);

    offset3[0] = offset0[0] - offset1[0] + offset2[0];
    offset3[1] = offset0[1] - offset1[1] + offset2[1];
    offset3[2] = offset0[2] - offset1[2] + offset2[2];

    mmMat4x4MulTranslate(model, model, offset3);
    mmMat4x4MulScale(model, model, s);
}

void
mmUIDrawable_GetVec4Color(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    float                                          color[4])
{
    assert(NULL != pView && "pView is invalid.");

    mmUIView_GetWorldOpacity(pView, color);
    mmVec4Mul(color, color, p->hOpacity);
}

void
mmUIDrawable_UpdatePushConstants(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView)
{
    float model[4][4];
    float color[4];

    assert(NULL != pView && "pView is invalid.");

    mmUIDrawable_GetMat4Model(p, pView, model);
    mmUIDrawable_GetVec4Color(p, pView, color);

    mmUniformMat4Assign(p->vConstant.model, model);
    mmUniformVec4Assign(p->vConstant.color, color);
}

VkResult
mmUIDrawable_DescriptorSetProduce(
    struct mmUIDrawable*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(
            &p->pPoolInfo->vDescriptorPool,
            &p->vDescriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmUIDrawable_DescriptorSetRecycle(
    struct mmUIDrawable*                           p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKDescriptorPool_DescriptorSetRecycle(
            &p->pPoolInfo->vDescriptorPool,
            &p->vDescriptorSet);
    } while (0);
}

VkResult
mmUIDrawable_DescriptorSetUpdate(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKImageInfo*                          pImageInfo,
    struct mmVKSamplerInfo*                        pSamplerInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");

    do
    {
        VkDescriptorImageInfo hDescriptorImageInfo[1];
        VkWriteDescriptorSet writes[2];

        VkDescriptorBufferInfo hDescriptorBufferInfo[1];

        struct mmUIViewContext* pViewContext = pView->context;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;
        struct mmVKBuffer* ubo = NULL;;

        VkDevice device = VK_NULL_HANDLE;
        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        ubo = &pViewContext->ubo;

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == ubo->buffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " ubo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSamplerInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pImageInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        descriptorSet = p->vDescriptorSet.descriptorSet;

        hDescriptorBufferInfo[0].buffer = ubo->buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmUIDrawableUBOVS);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        hDescriptorImageInfo[0].sampler = pSamplerInfo->sampler;
        hDescriptorImageInfo[0].imageView = pImageInfo->imageView;
        hDescriptorImageInfo[0].imageLayout = pImageInfo->vImage.imageLayout;
        
        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 1;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[1].pImageInfo = &hDescriptorImageInfo[0];
        writes[1].pBufferInfo = NULL;
        writes[1].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 2, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmUIDrawable_OnRecord(
    struct mmUIDrawable*                           p,
    VkCommandBuffer                                cmdBuffer)
{
    struct mmVKDescriptorSet* pDescriptorSet = &p->vDescriptorSet;
    struct mmVKPipelineInfo* pPipelineInfo = p->pPipelineInfo;
    struct mmVKPipeline* pPipeline = NULL;
    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkDeviceSize offset = 0;

    assert(NULL != pPipelineInfo && "pPipelineInfo is a invalid.");

    pPipeline = &pPipelineInfo->vPipeline;
    pipelineLayout = pPipeline->pipelineLayout;
    pipeline = pPipeline->pipeline;
    descriptorSet = pDescriptorSet->descriptorSet;

    assert(VK_NULL_HANDLE != pipelineLayout && "pipelineLayout is a invalid.");
    assert(VK_NULL_HANDLE != pipeline && "pipeline is a invalid.");
    assert(VK_NULL_HANDLE != descriptorSet && "descriptorSet is a invalid.");

    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
    vkCmdBindIndexBuffer(cmdBuffer, p->vIdxBuffer.buffer, 0, p->hIdxType);
    vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &p->vVtxBuffer.buffer, &offset);

    vkCmdBindDescriptorSets(
        cmdBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        NULL);

    vkCmdPushConstants(
        cmdBuffer,
        pipelineLayout,
        VK_SHADER_STAGE_VERTEX_BIT,
        0,
        sizeof(struct mmUIDrawablePushConstant),
        &p->vConstant);

    vkCmdDrawIndexed(cmdBuffer, p->hIdxCount, 1, 0, 0, 0);
}


