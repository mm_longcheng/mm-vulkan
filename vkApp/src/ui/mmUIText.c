/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIText.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIViewContext.h"
#include "ui/mmUIPropertyHelper.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertySet.h"
#include "core/mmPropertyHelper.h"
#include "core/mmUTFConvert.h"

#include "math/mmVector3.h"
#include "math/mmMatrix4x4.h"

#include "nwsi/mmUTFHelper.h"

void
mmUIText_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIText";

    /*- MasterFormat -*/
    
    mmPropertyDefineMutable(propertys,
        "Text.FontFamilyName", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hFontFamilyName, Utf16String, "Helvetica",
        NULL,
        NULL,
        cOrigin,
        "Text FontFamilyName string default \"Helvetica\".");

    mmPropertyDefineMutable(propertys,
        "Text.FontSize", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hFontSize, Float, "12",
        NULL,
        NULL,
        cOrigin,
        "Text FontSize float default 12.");

    mmPropertyDefineMutable(propertys,
        "Text.FontStyle", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hFontStyle, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text FontStyle int default mmFontStyleNormal(0).");

    mmPropertyDefineMutable(propertys,
        "Text.FontWeight", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hFontWeight, SInt, "400",
        NULL,
        NULL,
        cOrigin,
        "Text FontWeight float default mmFontWeightNormal(400).");

    mmPropertyDefineMutable(propertys,
        "Text.ForegroundColor", offset, struct mmUIText,
        vColor.hValueForegroundColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text ForegroundColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");

    mmPropertyDefineMutable(propertys,
        "Text.BackgroundColor", offset, struct mmUIText,
        vColor.hValueBackgroundColor, String, "(0, 0, 0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Text BackgroundColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 0).");

    mmPropertyDefineMutable(propertys,
        "Text.StrokeWidth", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hStrokeWidth, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text StrokeWidth float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.StrokeColor", offset, struct mmUIText,
        vColor.hValueStrokeColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text StrokeColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");

    mmPropertyDefineMutable(propertys,
        "Text.UnderlineWidth", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hUnderlineWidth, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text UnderlineWidth float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.UnderlineColor", offset, struct mmUIText,
        vColor.hValueUnderlineColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text UnderlineColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");

    mmPropertyDefineMutable(propertys,
        "Text.StrikeThruWidth", offset, struct mmUIText,
        vLayout.hParagraphFormat.hMasterFormat.hStrikeThruWidth, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text StrikeThruWidth float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.StrikeThruColor", offset, struct mmUIText,
        vColor.hValueStrikeThruColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text StrikeThruColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");

    /*- ParagraphFormat -*/
    
    mmPropertyDefineMutable(propertys,
        "Text.TextAlignment", offset, struct mmUIText,
        vLayout.hParagraphFormat.hTextAlignment, SInt, "4",
        NULL,
        NULL,
        cOrigin,
        "Text TextAlignment int default mmTextAlignmentNatural(4).");

    mmPropertyDefineMutable(propertys,
        "Text.ParagraphAlignment", offset, struct mmUIText,
        vLayout.hParagraphFormat.hParagraphAlignment, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text ParagraphAlignment int default mmTextParagraphAlignmentTop(0).");

    mmPropertyDefineMutable(propertys,
        "Text.LineBreakMode", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineBreakMode, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text LineBreakMode int default mmTextLineBreakByWordWrapping(0).");

    mmPropertyDefineMutable(propertys,
        "Text.WritingDirection", offset, struct mmUIText,
        vLayout.hParagraphFormat.hWritingDirection, SInt, "-1",
        NULL,
        NULL,
        cOrigin,
        "Text WritingDirection int default mmTextWritingDirectionNatural(-1).");

    mmPropertyDefineMutable(propertys,
        "Text.FirstLineHeadIndent", offset, struct mmUIText,
        vLayout.hParagraphFormat.hFirstLineHeadIndent, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text FirstLineHeadIndent float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.HeadIndent", offset, struct mmUIText,
        vLayout.hParagraphFormat.hHeadIndent, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text HeadIndent float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.TailIndent", offset, struct mmUIText,
        vLayout.hParagraphFormat.hTailIndent, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text TailIndent float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.LineSpacingAddition", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineSpacingAddition, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text LineSpacingAddition float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.LineSpacingMultiple", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineSpacingMultiple, Float, "1",
        NULL,
        NULL,
        cOrigin,
        "Text LineSpacingMultiple float default 1.");

    mmPropertyDefineMutable(propertys,
        "Text.ShadowOffset", offset, struct mmUIText,
        vLayout.hParagraphFormat.hShadowOffset, Vec2, "(0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Text ShadowOffset float[2] default (0, 0).");

    mmPropertyDefineMutable(propertys,
        "Text.ShadowBlur", offset, struct mmUIText,
        vLayout.hParagraphFormat.hShadowBlur, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Text ShadowBlur float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.ShadowColor", offset, struct mmUIText,
        vColor.hValueShadowColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text ShadowColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");

    mmPropertyDefineMutable(propertys,
        "Text.LineCap", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineCap, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text LineCap int default mmLineCapButt(0).");

    mmPropertyDefineMutable(propertys,
        "Text.LineJoin", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineJoin, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text LineJoin int default mmLineJoinMiter(0).");

    mmPropertyDefineMutable(propertys,
        "Text.LineMiterLimit", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineMiterLimit, Float, "10",
        NULL,
        NULL,
        cOrigin,
        "Text LineMiterLimit float default 10.");

    mmPropertyDefineMutable(propertys,
        "Text.AlignBaseline", offset, struct mmUIText,
        vLayout.hParagraphFormat.hAlignBaseline, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text AlignBaseline int default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.LineDescent", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLineDescent, Float, "10",
        NULL,
        NULL,
        cOrigin,
        "Text LineDescent float default 0.");

    mmPropertyDefineMutable(propertys,
        "Text.SuitableSize", offset, struct mmUIText,
        vLayout.hParagraphFormat.hSuitableSize, Vec2, "(0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Text SuitableSize float[2] default (0, 0).");

    mmPropertyDefineMutable(propertys,
        "Text.WindowSize", offset, struct mmUIText,
        vLayout.hParagraphFormat.hWindowSize, Vec2, "(20, 40)",
        NULL,
        NULL,
        cOrigin,
        "Text WindowSize float[2] default (20, 40).");

    mmPropertyDefineMutable(propertys,
        "Text.LayoutRect", offset, struct mmUIText,
        vLayout.hParagraphFormat.hLayoutRect, Vec4, "(0, 0, 20, 40)",
        NULL,
        NULL,
        cOrigin,
        "Text LayoutRect float[4] default (0, 0, 20, 40).");

    mmPropertyDefineMutable(propertys,
        "Text.DisplayDensity", offset, struct mmUIText,
        vLayout.hParagraphFormat.hDisplayDensity, Float, "1",
        NULL,
        NULL,
        cOrigin,
        "Text DisplayDensity float default 1.");
    
    /*- EditBox -*/
    
    mmPropertyDefineMutable(propertys,
        "Text.CaretIndex", offset, struct mmUIText,
        vLayout.hParagraphFormat.hCaretIndex, Size, "0",
        NULL,
        NULL,
        cOrigin,
        "Text CaretIndex size_t default 0.");
    
    mmPropertyDefineMutable(propertys,
        "Text.Selection", offset, struct mmUIText,
        vLayout.hParagraphFormat.hSelection, SizeVec2, "(0, 0)",
        NULL,
        NULL,
        cOrigin,
        "Text Selection size_t[2] default (0, 0).");
    
    mmPropertyDefineMutable(propertys,
        "Text.CaretWrapping", offset, struct mmUIText,
        vLayout.hParagraphFormat.hCaretWrapping, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text CaretWrapping int default 0.");
    
    mmPropertyDefineMutable(propertys,
        "Text.CaretStatus", offset, struct mmUIText,
        vLayout.hParagraphFormat.hCaretStatus, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text CaretStatus int default 0.");
    
    mmPropertyDefineMutable(propertys,
        "Text.SelectionStatus", offset, struct mmUIText,
        vLayout.hParagraphFormat.hSelectionStatus, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Text SelectionStatus int default 0.");
    
    mmPropertyDefineMutable(propertys,
        "Text.CaretWidth", offset, struct mmUIText,
        vLayout.hParagraphFormat.hCaretWidth, Float, "1",
        NULL,
        NULL,
        cOrigin,
        "Text CaretWidth float default 1.");
    
    mmPropertyDefineMutable(propertys,
        "Text.CaretColor", offset, struct mmUIText,
        vColor.hValueCaretColor, String, "(0, 0, 0, 1)",
        NULL,
        NULL,
        cOrigin,
        "Text CaretColor float[4] or #AARRGGBB #RRGGBB default (0, 0, 0, 1).");
    
    mmPropertyDefineMutable(propertys,
        "Text.SelectionCaptureColor", offset, struct mmUIText,
        vColor.hValueSelectionCaptureColor, String, "#FF4080FF",
        NULL,
        NULL,
        cOrigin,
        "Text SelectionCaptureColor float[4] or #AARRGGBB #RRGGBB default ARGB #FF4080FF ( 64, 128, 255).");
    
    mmPropertyDefineMutable(propertys,
        "Text.SelectionReleaseColor", offset, struct mmUIText,
        vColor.hValueSelectionReleaseColor, String, "#FFDCDCDC",
        NULL,
        NULL,
        cOrigin,
        "Text SelectionReleaseColor float[4] or #AARRGGBB #RRGGBB default ARGB #FFDCDCDC (220, 220, 220).");
    
    /*- Text -*/
    
    mmPropertyDefineMutable(propertys,
        "Text.String", offset, struct mmUIText,
        vLayout.hText.hString, Utf16String, "",
        NULL,
        NULL,
        cOrigin,
        "Text String ValueDirect(Utf16String) ValueString(Utf8String) default \"\".");
}

void
mmUIText_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    /*- MasterFormat -*/
    mmPropertyRemove(propertys, "Text.FontFamilyName");
    mmPropertyRemove(propertys, "Text.FontSize");
    mmPropertyRemove(propertys, "Text.FontStyle");
    mmPropertyRemove(propertys, "Text.FontWeight");
    mmPropertyRemove(propertys, "Text.ForegroundColor");
    mmPropertyRemove(propertys, "Text.BackgroundColor");
    mmPropertyRemove(propertys, "Text.StrokeWidth");
    mmPropertyRemove(propertys, "Text.StrokeColor");
    mmPropertyRemove(propertys, "Text.UnderlineWidth");
    mmPropertyRemove(propertys, "Text.UnderlineColor");
    mmPropertyRemove(propertys, "Text.StrikeThruWidth");
    mmPropertyRemove(propertys, "Text.StrikeThruColor");
    /*- ParagraphFormat -*/
    mmPropertyRemove(propertys, "Text.TextAlignment");
    mmPropertyRemove(propertys, "Text.ParagraphAlignment");
    mmPropertyRemove(propertys, "Text.LineBreakMode");
    mmPropertyRemove(propertys, "Text.WritingDirection");
    mmPropertyRemove(propertys, "Text.FirstLineHeadIndent");
    mmPropertyRemove(propertys, "Text.HeadIndent");
    mmPropertyRemove(propertys, "Text.TailIndent");
    mmPropertyRemove(propertys, "Text.LineSpacingAddition");
    mmPropertyRemove(propertys, "Text.LineSpacingMultiple");
    mmPropertyRemove(propertys, "Text.ShadowOffset");
    mmPropertyRemove(propertys, "Text.ShadowBlur");
    mmPropertyRemove(propertys, "Text.ShadowColor");
    mmPropertyRemove(propertys, "Text.LineCap");
    mmPropertyRemove(propertys, "Text.LineJoin");
    mmPropertyRemove(propertys, "Text.LineMiterLimit");
    mmPropertyRemove(propertys, "Text.AlignBaseline");
    mmPropertyRemove(propertys, "Text.LineDescent");
    mmPropertyRemove(propertys, "Text.SuitableSize");
    mmPropertyRemove(propertys, "Text.WindowSize");
    mmPropertyRemove(propertys, "Text.LayoutRect");
    mmPropertyRemove(propertys, "Text.DisplayDensity");
    /*- EditBox -*/
    mmPropertyRemove(propertys, "Text.CaretIndex");
    mmPropertyRemove(propertys, "Text.Selection");
    mmPropertyRemove(propertys, "Text.CaretWrapping");
    mmPropertyRemove(propertys, "Text.CaretStatus");
    mmPropertyRemove(propertys, "Text.SelectionStatus");
    mmPropertyRemove(propertys, "Text.CaretWidth");
    mmPropertyRemove(propertys, "Text.CaretColor");
    mmPropertyRemove(propertys, "Text.SelectionCaptureColor");
    mmPropertyRemove(propertys, "Text.SelectionReleaseColor");
    /*- Text -*/
    mmPropertyRemove(propertys, "Text.String");
}

void
mmTextColor_Init(
    struct mmTextColor*                            p)
{
    mmString_Init(&p->hValueForegroundColor);
    mmString_Init(&p->hValueBackgroundColor);
    mmString_Init(&p->hValueStrokeColor);
    mmString_Init(&p->hValueUnderlineColor);
    mmString_Init(&p->hValueStrikeThruColor);
    mmString_Init(&p->hValueShadowColor);
    mmString_Init(&p->hValueCaretColor);
    mmString_Init(&p->hValueSelectionCaptureColor);
    mmString_Init(&p->hValueSelectionReleaseColor);
}

void
mmTextColor_Destroy(
    struct mmTextColor*                            p)
{
    mmString_Destroy(&p->hValueSelectionReleaseColor);
    mmString_Destroy(&p->hValueSelectionCaptureColor);
    mmString_Destroy(&p->hValueCaretColor);
    mmString_Destroy(&p->hValueShadowColor);
    mmString_Destroy(&p->hValueStrikeThruColor);
    mmString_Destroy(&p->hValueUnderlineColor);
    mmString_Destroy(&p->hValueStrokeColor);
    mmString_Destroy(&p->hValueBackgroundColor);
    mmString_Destroy(&p->hValueForegroundColor);
}

void
mmTextOriginal_Init(
    struct mmTextOriginal*                         p)
{
    mmUIDimVec3Reset(p->size);
    mmVec3Make(p->anchor, 0.5f, 0.5f, 0.5f);
    mmMemset(p->align, (mmUInt8_t)mmUIAlignmentMinimum, sizeof(p->align));
}

void
mmTextOriginal_Destroy(
    struct mmTextOriginal*                         p)
{
    mmMemset(p->align, (mmUInt8_t)mmUIAlignmentMinimum, sizeof(p->align));
    mmVec3Make(p->anchor, 0.5f, 0.5f, 0.5f);
    mmUIDimVec3Reset(p->size);
}

int
mmTextOriginal_WidthIsZero(
    const struct mmTextOriginal*                   p)
{
    return (0.0f == p->size[0].o && 0.0f == p->size[0].o);
}

void
mmTextOriginal_Storage(
    struct mmTextOriginal*                         p,
    const struct mmUIView*                         pView)
{
    mmUIDimVec3Assign(p->size, pView->size);
    mmMemcpy(p->align, pView->align, sizeof(p->align));
    mmMemcpy(p->anchor, pView->anchor, sizeof(p->anchor));
}

void
mmTextOriginal_Restore(
    const struct mmTextOriginal*                   p,
    struct mmUIView*                               pView)
{
    mmUIDimVec3Assign(pView->size, p->size);
    mmMemcpy(pView->align, p->align, sizeof(p->align));
    mmMemcpy(pView->anchor, p->anchor, sizeof(p->anchor));
    mmUIView_MarkCacheInvalidWorldSize(pView);
    mmUIView_MarkCacheInvalidWorldTransform(pView);
}

void
mmUIText_Init(
    struct mmUIText*                               p)
{
    p->pSamplerInfo = NULL;
    mmVKImageInfo_Init(&p->vImageInfo);
    mmVKBuffer_Init(&p->vBuffer);
    mmTextLayout_Init(&p->vLayout);
    mmTextColor_Init(&p->vColor);
}

void
mmUIText_Destroy(
    struct mmUIText*                               p)
{
    mmTextColor_Destroy(&p->vColor);
    mmTextLayout_Destroy(&p->vLayout);
    mmVKBuffer_Destroy(&p->vBuffer);
    mmVKImageInfo_Destroy(&p->vImageInfo);
    p->pSamplerInfo = NULL;
}

void
mmUIText_SetCaretIndex(
    struct mmUIText*                               p,
    size_t                                         hCaretIndex)
{
    mmTextLayout_SetCaretIndex(&p->vLayout, hCaretIndex);
}

void
mmUIText_SetSelection(
    struct mmUIText*                               p,
    size_t                                         l,
    size_t                                         r)
{
    mmTextLayout_SetSelection(&p->vLayout, l, r);
}

void
mmUIText_SetCaretWrapping(
    struct mmUIText*                               p,
    int                                            hCaretWrapping)
{
    mmTextLayout_SetCaretWrapping(&p->vLayout, hCaretWrapping);
}

void
mmUIText_SetCaretStatus(
    struct mmUIText*                               p,
    int                                            hCaretStatus)
{
    mmTextLayout_SetCaretStatus(&p->vLayout, hCaretStatus);
}

void
mmUIText_SetSelectionStatus(
    struct mmUIText*                               p,
    int                                            hSelectionStatus)
{
    mmTextLayout_SetSelectionStatus(&p->vLayout, hSelectionStatus);
}

void
mmUIText_ToggleCaretStatus(
    struct mmUIText*                               p)
{
    mmTextLayout_ToggleCaretStatus(&p->vLayout);
}

VkResult
mmUIText_PrepareLayout(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        VkBufferCreateInfo hBufferInfo;
        VkImageCreateInfo hImageInfo;

        size_t size;
        size_t imageSize[2];
        float wsize[3] = { 0 };
        float density = 1.0f;
        int frame[4];
        int rotated = 0;
        float* pSuitableSize = NULL;

        VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageLayout finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        const void* pBuffer = NULL;

        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;

        struct mmTextParagraphFormat* pParagraphFormat = &p->vLayout.hParagraphFormat;
        struct mmUIViewContext* pViewContext = pView->context;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        density = pViewContext->density;

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmTextOriginal_Restore(&p->vOriginal, pView);

        mmUIView_GetWorldSize(pView, wsize);
        mmVec2Scale(wsize, wsize, density);

        mmUIText_UpdateColor(p, pView);

        mmTextLayout_SetWindowSize(&p->vLayout, wsize[0], wsize[1]);
        mmTextLayout_SetLayoutRect(&p->vLayout, 0, 0, wsize[0], wsize[1]);
        mmTextLayout_SetDisplayDensity(&p->vLayout, density);
        mmTextLayout_UpdateText(&p->vLayout);

        mmTextLayout_OnFinishLaunching(&p->vLayout);

        mmTextLayout_Draw(&p->vLayout);

        if (pParagraphFormat->hAlignBaseline)
        {
            // if align to baseline.
            // we will ignore and reset size[0] size[1].
            pSuitableSize = pParagraphFormat->hSuitableSize;
            mmVec2Scale(wsize, pSuitableSize, 1.0f / density);
            mmUIDim_Make(&pView->size[0], wsize[0], 0.0f);
            mmUIDim_Make(&pView->size[1], wsize[1], 0.0f);
            mmUIView_MarkCacheInvalidWorldSize(pView);
            mmUIView_MarkCacheInvalidWorldTransform(pView);
            // we will ignore and reset align[1] and anchor[1].
            pView->align[1] = mmUIAlignmentMinimum;
            pView->anchor[1] = 1.0f - (pParagraphFormat->hLineDescent / pSuitableSize[1]);
            // reset window size.
            mmVec2Assign(wsize, pParagraphFormat->hWindowSize);
        }
        else
        {
            // reset window size.
            mmVec2Assign(wsize, pParagraphFormat->hWindowSize);
        }

        // calculate pixel window size.
        wsize[0] = ceilf(wsize[0]);
        wsize[1] = ceilf(wsize[1]);
        // Avoid empty rectangles.
        wsize[0] = (0 == wsize[0]) ? 1 : wsize[0];
        wsize[1] = (0 == wsize[1]) ? 1 : wsize[1];
        // format is VK_FORMAT_R8G8B8A8_UNORM
        size = (uint32_t)wsize[0] * (uint32_t)wsize[1] * 4;

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = size;
        hBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->vBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        pBuffer = mmTextLayout_BufferLock(&p->vLayout);

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (const uint8_t*)pBuffer,
            0,
            size,
            &p->vBuffer,
            0);

        mmTextLayout_BufferUnLock(&p->vLayout, pBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        hImageInfo.pNext = NULL;
        hImageInfo.flags = 0;
        hImageInfo.imageType = VK_IMAGE_TYPE_2D;
        hImageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
        hImageInfo.extent.width = (uint32_t)wsize[0];
        hImageInfo.extent.height = (uint32_t)wsize[1];
        hImageInfo.extent.depth = 1;
        hImageInfo.mipLevels = 1;
        hImageInfo.arrayLayers = 1;
        hImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        hImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        hImageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        hImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hImageInfo.queueFamilyIndexCount = 0;
        hImageInfo.pQueueFamilyIndices = NULL;
        hImageInfo.initialLayout = initialLayout;

        err = mmVKImageInfo_Prepare(
            &p->vImageInfo,
            &hImageInfo,
            0,
            VMA_MEMORY_USAGE_GPU_ONLY,
            VK_IMAGE_VIEW_TYPE_2D,
            VK_IMAGE_ASPECT_COLOR_BIT,
            pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }
        mmVKImageInfo_Increase(&p->vImageInfo);

        err = mmVKUploader_CopyBufferToImage(
            pUploader,
            &p->vBuffer,
            &p->vImageInfo.vImage,
            finalLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_CopyBufferToImage failure.", __FUNCTION__, __LINE__);
            break;
        }

        frame[0] = 0;
        frame[1] = 0;
        frame[2] = (int)wsize[0];
        frame[3] = (int)wsize[1];
        imageSize[0] = p->vImageInfo.vImage.width;
        imageSize[1] = p->vImageInfo.vImage.height;
        mmUIImageFrameFromImage(&pDrawable->vIframe, imageSize, frame, rotated);
        err = mmUIDrawable_PrepareBuffer(pDrawable, pView, pUploader, &pDrawable->vIframe);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmUIText_DiscardLayout(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;
        struct mmUIViewContext* pViewContext = pView->context;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;

        mmUIDrawable_DiscardBuffer(pDrawable, pUploader);

        mmVKImageInfo_Decrease(&p->vImageInfo);
        mmVKImageInfo_Discard(&p->vImageInfo, pUploader);

        mmVKUploader_DeleteBuffer(pUploader, &p->vBuffer);

        mmTextLayout_OnBeforeTerminate(&p->vLayout);
    } while (0);
}

VkResult
mmUIText_Prepare(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmVKAssets* pAssets = NULL;
        struct mmGraphicsFactory* pGraphicsFactory = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;

        struct mmUIViewContext* pViewContext = pView->context;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pGraphicsFactory = pViewContext->graphics;

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pSamplerPool = &pAssets->vSamplerPool;

        mmTextOriginal_Storage(&p->vOriginal, pView);

        mmTextLayout_SetGraphicsFactory(&p->vLayout, pGraphicsFactory);

        err = mmUIText_PrepareLayout(p, pView, pDrawable);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIText_PrepareLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->pSamplerInfo = mmVKSamplerPool_Add(pSamplerPool, &mmVKSamplerNearestEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

        err = mmUIDrawable_PreparePipeline(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIDrawable_PreparePool(pDrawable, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmUIText_Discard(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    mmUIDrawable_DiscardPool(pDrawable);

    mmUIDrawable_DiscardPipeline(pDrawable);

    mmVKSamplerInfo_Decrease(p->pSamplerInfo);
    p->pSamplerInfo = NULL;

    mmUIText_DiscardLayout(p, pView, pDrawable);
}

VkResult
mmUIText_UpdateTextContent(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmUIViewContext* pViewContext = NULL;
        struct mmVKAssets* pAssets = NULL;
        struct mmVKUploader* pUploader = NULL;

        size_t size;
        float wsize[3] = { 0 };
        float density = 1.0f;

        const void* pBuffer = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == p->vBuffer.buffer)
        {
            // buffer is not ready, not need update text.
            err = VK_SUCCESS;
            break;
        }

        pViewContext = pView->context;
        if (NULL == pViewContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        density = pViewContext->density;

        pAssets = pViewContext->assets;
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmUIView_GetWorldSize(pView, wsize);
        mmVec2Scale(wsize, wsize, density);
        // calculate pixel window size.
        wsize[0] = ceilf(wsize[0]);
        wsize[1] = ceilf(wsize[1]);
        // Avoid empty rectangles.
        wsize[0] = (0 == wsize[0]) ? 1 : wsize[0];
        wsize[1] = (0 == wsize[1]) ? 1 : wsize[1];
        // format is VK_FORMAT_R8G8B8A8_UNORM
        size = (uint32_t)wsize[0] * (uint32_t)wsize[1] * 4;

        mmUIText_UpdateColor(p, pView);

        mmTextLayout_UpdateText(&p->vLayout);
        mmTextLayout_UpdateParagraph(&p->vLayout);

        mmTextLayout_Draw(&p->vLayout);

        pBuffer = mmTextLayout_BufferLock(&p->vLayout);

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            pBuffer,
            0,
            size,
            &p->vBuffer,
            0);

        mmTextLayout_BufferUnLock(&p->vLayout, pBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_CopyBufferToImage(
            pUploader,
            &p->vBuffer,
            &p->vImageInfo.vImage,
            p->vImageInfo.vImage.imageLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CopyBufferToImage failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIText_UpdateSizeContent(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pView && "pView is invalid.");
    assert(NULL != pDrawable && "pDrawable is invalid.");

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == p->vBuffer.buffer)
        {
            // buffer is not ready, not need update size.
            err = VK_SUCCESS;
            break;
        }

        /*- Discard -*/
        mmUIText_DiscardLayout(p, pView, pDrawable);

        /*- Prepare -*/
        err = mmUIText_PrepareLayout(p, pView, pDrawable);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIText_PrepareLayout failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIText_UpdateText(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    struct mmTextParagraphFormat* pParagraphFormat = &p->vLayout.hParagraphFormat;
    if (pParagraphFormat->hAlignBaseline && 
        mmTextOriginal_WidthIsZero(&p->vOriginal))
    {
        // need update size content.
        return mmUIText_UpdateSizeContent(p, pView, pDrawable);
    }
    else
    {
        // only update text content.
        return mmUIText_UpdateTextContent(p, pView, pDrawable);
    }
}

VkResult
mmUIText_UpdateSize(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable)
{
    if (mmUIDimVec3Equals(p->vOriginal.size, pView->size))
    {
        // only update text content.
        return mmUIText_UpdateTextContent(p, pView, pDrawable);
    }
    else
    {
        // need update size content.
        return mmUIText_UpdateSizeContent(p, pView, pDrawable);
    }
}

void
mmUIText_UpdateColor(
    struct mmUIText*                               p,
    struct mmUIView*                               pView)
{
    struct mmTextParagraphFormat* pParagraphFormat = &p->vLayout.hParagraphFormat;
    struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
    struct mmTextColor* pColor = &p->vColor;

    mmUIView_GetSchemeColor(pView, &pColor->hValueForegroundColor      , pMasterFormat->hForegroundColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueBackgroundColor      , pMasterFormat->hBackgroundColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueStrokeColor          , pMasterFormat->hStrokeColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueUnderlineColor       , pMasterFormat->hUnderlineColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueStrikeThruColor      , pMasterFormat->hStrikeThruColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueShadowColor          , pParagraphFormat->hShadowColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueCaretColor           , pParagraphFormat->hCaretColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueSelectionCaptureColor, pParagraphFormat->hSelectionCaptureColor);
    mmUIView_GetSchemeColor(pView, &pColor->hValueSelectionReleaseColor, pParagraphFormat->hSelectionReleaseColor);
}

void
mmUIText_GetPointCaret(
    const struct mmUIText*                         p,
    struct mmUIView*                               pView,
    const float                                    point[2],
    mmUInt32_t*                                    pCaretIndex,
    int*                                           pCaretWrapping)
{
    mmUInt32_t caret = 0;

    float hLocal[2];
    float hCaretPoint[2];

    int isTrailingHit;
    int isInside;

    struct mmTextHitMetrics metrics0;
    struct mmTextHitMetrics metrics1;

    const struct mmTextLayout* pTextLayout = &p->vLayout;
    float density = pTextLayout->hParagraphFormat.hDisplayDensity;

    assert(NULL != pView && "pView is invalid.");

    mmUIView_GetLocalPoint(pView, point, hLocal);
    mmVec2Scale(hLocal, hLocal, density);

    mmTextLayout_HitTestPoint(
        pTextLayout, 
        hLocal, 
        &isTrailingHit, 
        &isInside, 
        &metrics0);

    if (isTrailingHit)
    {
        caret = metrics0.offset + metrics0.length;
    }
    else
    {
        caret = metrics0.offset;
    }

    mmTextLayout_HitTestTextPosition(
        pTextLayout, 
        caret, 
        0, 
        hCaretPoint, 
        &metrics1);

    (*pCaretIndex) = caret;
    (*pCaretWrapping) = metrics0.rect[1] != metrics1.rect[1];
    
    //
    //struct mmLogger* gLogger = mmLogger_Instance();
    //
    //mmLogger_LogI(gLogger, "xxx rect:(%f, %f, %f, %f) isText:%d "
    //    "isInside:%d o:%d l:%d bidiLevel:%d isTrailingHit:%d",
    //    metrics0.rect[0],
    //    metrics0.rect[1],
    //    metrics0.rect[2],
    //    metrics0.rect[3],
    //    metrics0.isText,
    //    isInside,
    //    (int)metrics0.offset,
    //    (int)metrics0.length,
    //    (int)metrics0.bidiLevel,
    //    (int)isTrailingHit);
    //
    //mmLogger_LogI(gLogger, "xxx rect:(%f, %f, %f, %f) point1(%f, %f)",
    //    metrics1.rect[0],
    //    metrics1.rect[1],
    //    metrics1.rect[2],
    //    metrics1.rect[3],
    //    hCaretPoint[0],
    //    hCaretPoint[1]);
    //
    //mmLogger_LogI(gLogger, "xxx CaretIndex:%d CaretWrapping:%d",
    //    (int)(*pCaretIndex),
    //    (int)(*pCaretWrapping));
}
