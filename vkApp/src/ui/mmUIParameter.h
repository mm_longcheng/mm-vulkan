/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIParameter_h__
#define __mmUIParameter_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "core/mmPrefix.h"

struct mmUIParameterProperty
{
    struct mmString Name;
    struct mmString Value;
};

void
mmUIParameterProperty_Init(
    struct mmUIParameterProperty*                  p);

void
mmUIParameterProperty_Destroy(
    struct mmUIParameterProperty*                  p);

void
mmUIParameterPropertys_Init(
    struct mmVectorValue*                          p);

void
mmUIParameterPropertys_Destroy(
    struct mmVectorValue*                          p);

struct mmUIParameterView
{
    struct mmString Type;
    struct mmString Style;
    struct mmVectorValue Property;
    struct mmVectorValue Children;
};

void
mmUIParameterView_Init(
    struct mmUIParameterView*                      p);

void
mmUIParameterView_Destroy(
    struct mmUIParameterView*                      p);

void
mmUIParameterViews_Init(
    struct mmVectorValue*                          p);

void
mmUIParameterViews_Destroy(
    struct mmVectorValue*                          p);

struct mmUIParameterStyle
{
    struct mmString Name;
    struct mmString Style;
    struct mmString Type;
    struct mmVectorValue Property;
};

void
mmUIParameterStyle_Init(
    struct mmUIParameterStyle*                     p);

void
mmUIParameterStyle_Destroy(
    struct mmUIParameterStyle*                     p);

void
mmUIParameterStyles_Init(
    struct mmVectorValue*                          p);

void
mmUIParameterStyles_Destroy(
    struct mmVectorValue*                          p);

struct mmUIParameterFont
{
    struct mmString Family;
    struct mmString Path;
};

void
mmUIParameterFont_Init(
    struct mmUIParameterFont*                      p);

void
mmUIParameterFont_Destroy(
    struct mmUIParameterFont*                      p);

void
mmUIParameterFonts_Init(
    struct mmVectorValue*                          p);

void
mmUIParameterFonts_Destroy(
    struct mmVectorValue*                          p);

struct mmUIParameterScheme
{
    struct mmString Name;
    struct mmVectorValue Font;
    struct mmVectorValue Color;
    struct mmVectorValue Style;
};

void
mmUIParameterScheme_Init(
    struct mmUIParameterScheme*                    p);

void
mmUIParameterScheme_Destroy(
    struct mmUIParameterScheme*                    p);

#include "core/mmSuffix.h"

#endif//__mmUIParameter_h__
