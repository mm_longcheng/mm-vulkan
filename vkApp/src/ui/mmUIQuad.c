/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIQuad.h"

#include "core/mmAlloc.h"

#include "math/mmVector4.h"
#include "math/mmMathConst.h"

void
mmUIImageFrameReset(
    struct mmUIImageFrame*                         iframe)
{
    mmMemset(iframe, 0, sizeof(struct mmUIImageFrame));
}

void
mmUIImageFrameFromSheet(
    struct mmUIImageFrame*                         iframe,
    const struct mmVKSheetFrame*                   sframe)
{
    struct mmVKSheetImage* simage = sframe->image;
    mmMemcpy(iframe->imageSize, simage->imageSize, sizeof(iframe->imageSize));
    mmMemcpy(iframe->frame    , sframe->frame    , sizeof(iframe->frame    ));
    mmMemcpy(iframe->source   , sframe->source   , sizeof(iframe->source   ));
    mmMemcpy(iframe->offset   , sframe->offset   , sizeof(iframe->offset   ));
    mmMemcpy(iframe->original , sframe->original , sizeof(iframe->original ));
    iframe->r = sframe->r;
    iframe->t = sframe->t;
    mmMemcpy(iframe->anchor   , sframe->anchor   , sizeof(iframe->anchor   ));
    mmMemcpy(iframe->borders  , sframe->borders  , sizeof(iframe->borders  ));
}

void
mmUIImageFrameFromImage(
    struct mmUIImageFrame*                         iframe,
    const size_t                                   imageSize[2],
    const int                                      frame[4],
    int                                            rotated)
{
    iframe->imageSize[0] = imageSize[0];
    iframe->imageSize[1] = imageSize[1];

    iframe->frame[0] = frame[0];
    iframe->frame[1] = frame[1];
    iframe->frame[2] = frame[2];
    iframe->frame[3] = frame[3];

    iframe->source[0] = frame[0];
    iframe->source[1] = frame[1];
    iframe->source[2] = frame[2];
    iframe->source[3] = frame[3];

    iframe->offset[0] = 0;
    iframe->offset[1] = 0;

    iframe->original[0] = frame[2];
    iframe->original[1] = frame[3];

    iframe->r = rotated;
    iframe->t = 0;

    iframe->anchor[0] = 0;
    iframe->anchor[1] = 0;

    iframe->borders[0] = 0;
    iframe->borders[1] = 0;
    iframe->borders[2] = 0;
    iframe->borders[3] = 0;
}

void
mmUIVertex4AssignCornerColor(
    struct mmUIVertex                              vtx[4],
    const float                                    color[4][4])
{
    mmVec4Assign(vtx[0].color, color[0]);
    mmVec4Assign(vtx[1].color, color[1]);
    mmVec4Assign(vtx[2].color, color[2]);
    mmVec4Assign(vtx[3].color, color[3]);
}

void
mmUIVertex4AssignFrameBasic(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe)
{
    const int*        frame = iframe->frame;
    const int*       offset = iframe->offset;
    const size_t* imageSize = iframe->imageSize;

    int    hFlippedX = flipped[0];
    int    hFlippedY = flipped[1];
    int hRectRotated = iframe->r;

    float fu0, fu1;
    float fv0, fv1;

    float x0, x1;
    float y0, y1;
    float z0 = 0.0f;

    float u0, u1;
    float v0, v1;

    fu0 = (float)(frame[0]/*       */) / imageSize[0];
    fu1 = (float)(frame[0] + frame[2]) / imageSize[0];
    fv0 = (float)(frame[1]/*       */) / imageSize[1];
    fv1 = (float)(frame[1] + frame[3]) / imageSize[1];

    if (hRectRotated)
    {
        x0 = (float)(offset[0]/*       */);
        x1 = (float)(offset[0] + frame[3]);

        y0 = (float)(offset[1]/*       */);
        y1 = (float)(offset[1] + frame[2]);
    }
    else
    {
        x0 = (float)(offset[0]/*       */);
        x1 = (float)(offset[0] + frame[2]);

        y0 = (float)(offset[1]/*       */);
        y1 = (float)(offset[1] + frame[3]);
    }

    // position
    // row 0
    vtx[0].position[0] = x0; vtx[1].position[0] = x1;
    vtx[0].position[1] = y0; vtx[1].position[1] = y0;
    vtx[0].position[2] = z0; vtx[1].position[2] = z0;
    // row 1
    vtx[2].position[0] = x0; vtx[3].position[0] = x1;
    vtx[2].position[1] = y1; vtx[3].position[1] = y1;
    vtx[2].position[2] = z0; vtx[3].position[2] = z0;

    if ((!hRectRotated && hFlippedX) || (hRectRotated && hFlippedY))
    {
        u0 = fu1; u1 = fu0;
    }
    else
    {
        u0 = fu0; u1 = fu1;
    }

    if ((!hRectRotated && hFlippedY) || (hRectRotated && hFlippedX))
    {
        v0 = fv1; v1 = fv0;
    }
    else
    {
        v0 = fv0; v1 = fv1;
    }

    if (hRectRotated)
    {
        // row 0
        vtx[0].texcoord[0] = u1; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v1;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u0;
        vtx[2].texcoord[1] = v0; vtx[3].texcoord[1] = v1;
    }
    else
    {
        // row 0
        vtx[0].texcoord[0] = u0; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v0;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u1;
        vtx[2].texcoord[1] = v1; vtx[3].texcoord[1] = v1;
    }
}

void
mmUIVertex4AssignFrameTiled(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    const float                                    size[2])
{
    const int*        frame = iframe->frame;
    const int*       offset = iframe->offset;
    const int*     original = iframe->original;
    const size_t* imageSize = iframe->imageSize;

    int    hFlippedX = flipped[0];
    int    hFlippedY = flipped[1];
    int hRectRotated = iframe->r;

    float fu0, fu1;
    float fv0, fv1;

    float x0, x1;
    float y0, y1;
    float z0 = 0.0f;

    float u0, u1;
    float v0, v1;

    fu0 = (float)(frame[0]/*      */) / imageSize[0];
    fu1 = (float)(frame[0] + size[0]) / imageSize[0];
    fv0 = (float)(frame[1]/*      */) / imageSize[1];
    fv1 = (float)(frame[1] + size[1]) / imageSize[1];

    if (hRectRotated)
    {
        x0 = (float)(/*                    */offset[0]/*      */);
        x1 = (float)(size[0] - original[0] + offset[0] + frame[3]);

        y0 = (float)(/*                    */offset[1]/*      */);
        y1 = (float)(size[1] - original[1] + offset[1] + frame[2]);
    }
    else
    {
        x0 = (float)(/*                    */offset[0]/*      */);
        x1 = (float)(size[0] - original[0] + offset[0] + frame[2]);

        y0 = (float)(/*                    */offset[1]/*      */);
        y1 = (float)(size[1] - original[1] + offset[1] + frame[3]);
    }

    // position
    // row 0
    vtx[0].position[0] = x0; vtx[1].position[0] = x1;
    vtx[0].position[1] = y0; vtx[1].position[1] = y0;
    vtx[0].position[2] = z0; vtx[1].position[2] = z0;
    // row 1
    vtx[2].position[0] = x0; vtx[3].position[0] = x1;
    vtx[2].position[1] = y1; vtx[3].position[1] = y1;
    vtx[2].position[2] = z0; vtx[3].position[2] = z0;

    if ((!hRectRotated && hFlippedX) || (hRectRotated && hFlippedY))
    {
        u0 = fu1; u1 = fu0;
    }
    else
    {
        u0 = fu0; u1 = fu1;
    }

    if ((!hRectRotated && hFlippedY) || (hRectRotated && hFlippedX))
    {
        v0 = fv1; v1 = fv0;
    }
    else
    {
        v0 = fv0; v1 = fv1;
    }

    if (hRectRotated)
    {
        // row 0
        vtx[0].texcoord[0] = u1; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v1;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u0;
        vtx[2].texcoord[1] = v0; vtx[3].texcoord[1] = v1;
    }
    else
    {
        // row 0
        vtx[0].texcoord[0] = u0; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v0;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u1;
        vtx[2].texcoord[1] = v1; vtx[3].texcoord[1] = v1;
    }
}

void
mmUIVertex4AssignFrameBrush(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    float                                          ox[2],
    float                                          oy[2])
{
    const int*        frame = iframe->frame;
    const int*       offset = iframe->offset;
    const size_t* imageSize = iframe->imageSize;

    int    hFlippedX = flipped[0];
    int    hFlippedY = flipped[1];
    int hRectRotated = iframe->r;

    float fu0, fu1;
    float fv0, fv1;

    float x0, x1;
    float y0, y1;
    float z0 = 0.0f;

    float u0, u1;
    float v0, v1;

    float cx0, cx1;
    float cy0, cy1;

    if (hFlippedX)
    {
        cx0 = 1.0f - ox[1];
        cx1 = 1.0f - ox[0];
    }
    else
    {
        cx0 = ox[0];
        cx1 = ox[1];
    }

    if (hFlippedY)
    {
        cy0 = 1.0f - oy[1];
        cy1 = 1.0f - oy[0];
    }
    else
    {
        cy0 = oy[0];
        cy1 = oy[1];
    }

    if (hRectRotated)
    {
        fu1 = (float)(frame[0] + frame[2] * (1.0f - cy0)) / imageSize[0];
        fu0 = (float)(frame[0] + frame[2] * (1.0f - cy1)) / imageSize[0];

        fv0 = (float)(frame[1] + frame[3] * (/*   */cx0)) / imageSize[1];
        fv1 = (float)(frame[1] + frame[3] * (/*   */cx1)) / imageSize[1];

        x0 = (float)(offset[0] + frame[3] * ox[0]);
        x1 = (float)(offset[0] + frame[3] * ox[1]);

        y0 = (float)(offset[1] + frame[2] * oy[0]);
        y1 = (float)(offset[1] + frame[2] * oy[1]);
    }
    else
    {
        fu0 = (float)(frame[0] + frame[2] * (/*   */cx0)) / imageSize[0];
        fu1 = (float)(frame[0] + frame[2] * (/*   */cx1)) / imageSize[0];

        fv0 = (float)(frame[1] + frame[3] * (/*   */cy0)) / imageSize[1];
        fv1 = (float)(frame[1] + frame[3] * (/*   */cy1)) / imageSize[1];

        x0 = (float)(offset[0] + frame[2] * ox[0]);
        x1 = (float)(offset[0] + frame[2] * ox[1]);

        y0 = (float)(offset[1] + frame[3] * oy[0]);
        y1 = (float)(offset[1] + frame[3] * oy[1]);
    }

    // position
    // row 0
    vtx[0].position[0] = x0; vtx[1].position[0] = x1;
    vtx[0].position[1] = y0; vtx[1].position[1] = y0;
    vtx[0].position[2] = z0; vtx[1].position[2] = z0;
    // row 1
    vtx[2].position[0] = x0; vtx[3].position[0] = x1;
    vtx[2].position[1] = y1; vtx[3].position[1] = y1;
    vtx[2].position[2] = z0; vtx[3].position[2] = z0;

    if ((!hRectRotated && hFlippedX) || (hRectRotated && hFlippedY))
    {
        u0 = fu1; u1 = fu0;
    }
    else
    {
        u0 = fu0; u1 = fu1;
    }

    if ((!hRectRotated && hFlippedY) || (hRectRotated && hFlippedX))
    {
        v0 = fv1; v1 = fv0;
    }
    else
    {
        v0 = fv0; v1 = fv1;
    }

    if (hRectRotated)
    {
        // row 0
        vtx[0].texcoord[0] = u1; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v1;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u0;
        vtx[2].texcoord[1] = v0; vtx[3].texcoord[1] = v1;
    }
    else
    {
        // row 0
        vtx[0].texcoord[0] = u0; vtx[1].texcoord[0] = u1;
        vtx[0].texcoord[1] = v0; vtx[1].texcoord[1] = v0;
        // row 1
        vtx[2].texcoord[0] = u0; vtx[3].texcoord[0] = u1;
        vtx[2].texcoord[1] = v1; vtx[3].texcoord[1] = v1;
    }
}

void
mmUIVertex4AssignColor(
    struct mmUIVertex                              vtx[4],
    const float                                    color[4])
{
    mmVec4Assign(vtx[0].color, color);
    mmVec4Assign(vtx[1].color, color);
    mmVec4Assign(vtx[2].color, color);
    mmVec4Assign(vtx[3].color, color);
}

void
mmUIVertex4AssignIndex16(
    uint16_t                                       idx[6],
    uint16_t                                       i)
{
    // 0---1
    // | \ |  
    // 2---3

    idx[0] = i + 0;
    idx[1] = i + 2;
    idx[2] = i + 3;

    idx[3] = i + 0;
    idx[4] = i + 3;
    idx[5] = i + 1;
}

void
mmUIVertex4AssignIndex32(
    uint32_t                                       idx[6],
    uint32_t                                       i)
{
    // 0---1
    // | \ |  
    // 2---3

    idx[0] = i + 0;
    idx[1] = i + 2;
    idx[2] = i + 3;

    idx[3] = i + 0;
    idx[4] = i + 3;
    idx[5] = i + 1;
}

void
mmUIVertex10AssignFrameBrush(
    struct mmUIVertex                              vtx[10],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    float                                          amount,
    int                                            direction,
    int                                            reverse,
    int*                                           pSegment)
{
    const int*        frame = iframe->frame;
    const int*       offset = iframe->offset;
    const size_t* imageSize = iframe->imageSize;

    int    hFlippedX = flipped[0];
    int    hFlippedY = flipped[1];
    int hRectRotated = iframe->r;

    int dir, quadrant, filled;
    float rotate, radian;

    float fu0, fu1, fu2;
    float fv0, fv1, fv2;

    float x0, x1, x2;
    float y0, y1, y2;
    float z0 = 0.0f;

    float u0, u1, u2;
    float v0, v1, v2;

    fu0 = (float)(frame[0]/*              */) / imageSize[0];
    fu1 = (float)(frame[0] + frame[2] * 0.5f) / imageSize[0];
    fu2 = (float)(frame[0] + frame[2]/*   */) / imageSize[0];
    fv0 = (float)(frame[1]/*              */) / imageSize[1];
    fv1 = (float)(frame[1] + frame[3] * 0.5f) / imageSize[1];
    fv2 = (float)(frame[1] + frame[3]/*   */) / imageSize[1];

    if (hRectRotated)
    {
        x0 = (float)(offset[0]/*              */);
        x1 = (float)(offset[0] + frame[3] * 0.5f);
        x2 = (float)(offset[0] + frame[3]/*   */);

        y0 = (float)(offset[1]/*              */);
        y1 = (float)(offset[1] + frame[2] * 0.5f);
        y2 = (float)(offset[1] + frame[2]/*   */);
    }
    else
    {
        x0 = (float)(offset[0]/*              */);
        x1 = (float)(offset[0] + frame[2] * 0.5f);
        x2 = (float)(offset[0] + frame[2]/*   */);

        y0 = (float)(offset[1]/*              */);
        y1 = (float)(offset[1] + frame[3] * 0.5f);
        y2 = (float)(offset[1] + frame[3]/*   */);
    }

    // position
    // row 0
    vtx[8].position[0] = x0; vtx[1].position[0] = x1; vtx[2].position[0] = x2;
    vtx[8].position[1] = y0; vtx[1].position[1] = y0; vtx[2].position[1] = y0;
    vtx[8].position[2] = z0; vtx[1].position[2] = z0; vtx[2].position[2] = z0;
    // row 1
    vtx[7].position[0] = x0; vtx[0].position[0] = x1; vtx[3].position[0] = x2;
    vtx[7].position[1] = y1; vtx[0].position[1] = y1; vtx[3].position[1] = y1;
    vtx[7].position[2] = z0; vtx[0].position[2] = z0; vtx[3].position[2] = z0;
    // row 2
    vtx[6].position[0] = x0; vtx[5].position[0] = x1; vtx[4].position[0] = x2;
    vtx[6].position[1] = y2; vtx[5].position[1] = y2; vtx[4].position[1] = y2;
    vtx[6].position[2] = z0; vtx[5].position[2] = z0; vtx[4].position[2] = z0;

    if ((!hRectRotated && hFlippedX) || (hRectRotated && hFlippedY))
    {
        u0 = fu2; u1 = fu1; u2 = fu0;
    }
    else
    {
        u0 = fu0; u1 = fu1; u2 = fu2;
    }

    if ((!hRectRotated && hFlippedY) || (hRectRotated && hFlippedX))
    {
        v0 = fv2; v1 = fv1; v2 = fv0;
    }
    else
    {
        v0 = fv0; v1 = fv1; v2 = fv2;
    }

    if (hRectRotated)
    {
        // row 0
        vtx[8].texcoord[0] = u2; vtx[1].texcoord[0] = u2; vtx[2].texcoord[0] = u2;
        vtx[8].texcoord[1] = v0; vtx[1].texcoord[1] = v1; vtx[2].texcoord[1] = v2;
        // row 1
        vtx[7].texcoord[0] = u1; vtx[0].texcoord[0] = u1; vtx[3].texcoord[0] = u1;
        vtx[7].texcoord[1] = v0; vtx[0].texcoord[1] = v1; vtx[3].texcoord[1] = v2;
        // row 1
        vtx[6].texcoord[0] = u0; vtx[5].texcoord[0] = u0; vtx[4].texcoord[0] = u0;
        vtx[6].texcoord[1] = v0; vtx[5].texcoord[1] = v1; vtx[4].texcoord[1] = v2;
    }
    else
    {
        // row 0
        vtx[8].texcoord[0] = u0; vtx[1].texcoord[0] = u1; vtx[2].texcoord[0] = u2;
        vtx[8].texcoord[1] = v0; vtx[1].texcoord[1] = v0; vtx[2].texcoord[1] = v0;
        // row 1
        vtx[7].texcoord[0] = u0; vtx[0].texcoord[0] = u1; vtx[3].texcoord[0] = u2;
        vtx[7].texcoord[1] = v1; vtx[0].texcoord[1] = v1; vtx[3].texcoord[1] = v1;
        // row 1
        vtx[6].texcoord[0] = u0; vtx[5].texcoord[0] = u1; vtx[4].texcoord[0] = u2;
        vtx[6].texcoord[1] = v2; vtx[5].texcoord[1] = v2; vtx[4].texcoord[1] = v2;
    }

    /* Transform phase and period */
    if (reverse)
    {
        dir = ((8 - direction) % 8);
        rotate = fmodf(2.0f + (amount + dir * 0.125f), 1.0f);
        quadrant = (int)(rotate * 4.0f) % 4;
        radian = (float)(2.0f * MM_PI * rotate);
    }
    else
    {
        dir = direction;
        rotate = fmodf(2.0f - (amount + dir * 0.125f), 1.0f);
        quadrant = (int)(rotate * 4.0f) % 4;
        radian = (float)(2.0f * MM_PI * rotate);
    }

    /* Detection threshold */
    filled = ((1.0f == amount && 0 == reverse) || (0.0f == amount && 1 == reverse));

    /* Cut line calculation */
    switch (quadrant)
    {
    case 0:
    {
        float r0 = (float)(radian);
        float t0 = tanf(r0);
        float w = x1 - x0;
        float h = y1 - y0;
        if (t0 < w / h)
        {
            float r1 = (float)(radian);
            float t1 = tanf(r1);
            float h0 = h * t1;
            float k = h0 / w;

            vtx[9].position[0] = x1 - h0;
            vtx[9].position[1] = y0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (v1 - v0);
                vtx[9].texcoord[0] = u2;
                vtx[9].texcoord[1] = v1 - h1;
            }
            else
            {
                float h1 = k * (u1 - u0);
                vtx[9].texcoord[0] = u1 - h1;
                vtx[9].texcoord[1] = v0;
            }

            (*pSegment) = (filled || 0.0f != rotate) ? 0 : 7;
        }
        else
        {
            float r1 = (float)(MM_PI_DIV_2 - radian);
            float t1 = tanf(r1);
            float h0 = w * t1;
            float k = h0 / h;

            vtx[9].position[0] = x0;
            vtx[9].position[1] = y1 - h0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (u2 - u1);
                vtx[9].texcoord[0] = u1 + h1;
                vtx[9].texcoord[1] = v0;
            }
            else
            {
                float h1 = k * (v1 - v0);
                vtx[9].texcoord[0] = u0;
                vtx[9].texcoord[1] = v1 - h1;
            }

            (*pSegment) = 1;
        }
    }
    break;
    case 1:
    {
        float r0 = (float)(-radian);
        float t0 = tanf(r0);
        float w = x1 - x0;
        float h = y2 - y1;
        if (t0 > w / h)
        {
            float r1 = (float)(radian - MM_PI_DIV_2);
            float t1 = tanf(r1);
            float h0 = w * t1;
            float k = h0 / h;

            vtx[9].position[0] = x0;
            vtx[9].position[1] = y1 + h0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (u1 - u0);
                vtx[9].texcoord[0] = u1 - h1;
                vtx[9].texcoord[1] = v0;
            }
            else
            {
                float h1 = k * (v2 - v1);
                vtx[9].texcoord[0] = u0;
                vtx[9].texcoord[1] = v1 + h1;
            }

            (*pSegment) = (filled || 0.25f != rotate) ? 2 : 1;
        }
        else
        {
            float r1 = (float)(-radian);
            float t1 = tanf(r1);
            float h0 = h * t1;
            float k = h0 / w;

            vtx[9].position[0] = x1 - h0;
            vtx[9].position[1] = y2;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (v1 - v0);
                vtx[9].texcoord[0] = u0;
                vtx[9].texcoord[1] = v1 - h1;
            }
            else
            {
                float h1 = k * (u1 - u0);
                vtx[9].texcoord[0] = u1 - h1;
                vtx[9].texcoord[1] = v2;
            }

            (*pSegment) = 3;
        }
    }
    break;
    case 2:
    {
        float r0 = (float)(radian);
        float t0 = tanf(r0);
        float w = x2 - x1;
        float h = y2 - y1;
        if (t0 < w / h)
        {
            float r1 = (float)(radian);
            float t1 = tanf(r1);
            float h0 = h * t1;
            float k = h0 / w;

            vtx[9].position[0] = x1 + h0;
            vtx[9].position[1] = y2;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (v2 - v1);
                vtx[9].texcoord[0] = u0;
                vtx[9].texcoord[1] = v1 + h1;
            }
            else
            {
                float h1 = k * (u2 - u1);
                vtx[9].texcoord[0] = u1 + h1;
                vtx[9].texcoord[1] = v2;
            }

            (*pSegment) = (filled || 0.5f != rotate) ? 4 : 3;
        }
        else
        {
            float r1 = (float)(MM_PI_DIV_2 - radian);
            float t1 = tanf(r1);
            float h0 = w * t1;
            float k = h0 / h;

            vtx[9].position[0] = x2;
            vtx[9].position[1] = y1 + h0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (u1 - u0);
                vtx[9].texcoord[0] = u1 - h1;
                vtx[9].texcoord[1] = v2;
            }
            else
            {
                float h1 = k * (v2 - v1);
                vtx[9].texcoord[0] = u2;
                vtx[9].texcoord[1] = v1 + h1;
            }

            (*pSegment) = 5;
        }
    }
    break;
    case 3:
    {
        float r0 = (float)(-radian);
        float t0 = tanf(r0);
        float w = x2 - x1;
        float h = y1 - y0;
        if (t0 > w / h)
        {
            float r1 = (float)(MM_PI_DIV_2 + radian);
            float t1 = tanf(r1);
            float h0 = w * t1;
            float k = h0 / h;

            vtx[9].position[0] = x2;
            vtx[9].position[1] = y1 - h0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (u2 - u1);
                vtx[9].texcoord[0] = u1 + h1;
                vtx[9].texcoord[1] = v2;
            }
            else
            {
                float h1 = k * (v1 - v0);
                vtx[9].texcoord[0] = u2;
                vtx[9].texcoord[1] = v1 - h1;
            }

            (*pSegment) = (filled || 0.75f != rotate) ? 6 : 5;
        }
        else
        {
            float r1 = (float)(-radian);
            float t1 = tanf(r1);
            float h0 = h * t1;
            float k = h0 / w;

            vtx[9].position[0] = x1 + h0;
            vtx[9].position[1] = y0;
            vtx[9].position[2] = z0;

            if (hRectRotated)
            {
                float h1 = k * (v2 - v1);
                vtx[9].texcoord[0] = u2;
                vtx[9].texcoord[1] = v1 + h1;
            }
            else
            {
                float h1 = k * (u2 - u1);
                vtx[9].texcoord[0] = u1 + h1;
                vtx[9].texcoord[1] = v0;
            }

            (*pSegment) = 7;
        }
    }
    break;
    default:
    break;
    }
}

void
mmUIVertex10AssignColor(
    struct mmUIVertex                              vtx[10],
    const float                                    color[4])
{
    mmVec4Assign(vtx[0x0].color, color);
    mmVec4Assign(vtx[0x1].color, color);
    mmVec4Assign(vtx[0x2].color, color);

    mmVec4Assign(vtx[0x3].color, color);
    mmVec4Assign(vtx[0x4].color, color);
    mmVec4Assign(vtx[0x5].color, color);

    mmVec4Assign(vtx[0x6].color, color);
    mmVec4Assign(vtx[0x7].color, color);
    mmVec4Assign(vtx[0x8].color, color);

    mmVec4Assign(vtx[0x9].color, color);
}

void
mmUIVertex10AssignIndex16(
    uint16_t                                       idx[24],
    uint16_t                                       i,
    int                                            segment,
    int                                            direction,
    int                                            reverse)
{
    /*
     *    Radial360
     *
     *       0x9
     *   0x8-0x1-0x2
     *    | \ | / |
     *   0x7-0x0-0x3
     *    | / | \ |
     *   0x6-0x5-0x4
     *
     *    direction
     *
     *    7---0---1
     *    | \ | / |
     *    6---+---2
     *    | / | \ |
     *    5---4---3
     */

    idx[0x00] = i + 0x0; idx[0x06] = i + 0x0;
    idx[0x01] = i + 0x2; idx[0x07] = i + 0x4;
    idx[0x02] = i + 0x1; idx[0x08] = i + 0x3;
    idx[0x03] = i + 0x0; idx[0x09] = i + 0x0;
    idx[0x04] = i + 0x3; idx[0x0A] = i + 0x5;
    idx[0x05] = i + 0x2; idx[0x0B] = i + 0x4;

    idx[0x0C] = i + 0x0; idx[0x12] = i + 0x0;
    idx[0x0D] = i + 0x6; idx[0x13] = i + 0x8;
    idx[0x0E] = i + 0x5; idx[0x14] = i + 0x7;
    idx[0x0F] = i + 0x0; idx[0x15] = i + 0x0;
    idx[0x10] = i + 0x7; idx[0x16] = i + 0x1;
    idx[0x11] = i + 0x6; idx[0x17] = i + 0x8;

    if (reverse)
    {
        int m, u;
        int sid = (8 - segment) % 8 + 1;
        int n = 7 - (16 + segment - (8 - direction)) % 8;
        u = (16 + 7 - segment) % 8;
        idx[u * 3 + 0] = i + 0x0;
        idx[u * 3 + 1] = i + sid;
        idx[u * 3 + 2] = i + 0x9;
        for (m = 0; m < n; m++)
        {
            u = (16 + direction + m) % 8;
            idx[u * 3 + 0] = i + 0x0;
            idx[u * 3 + 1] = i + 0x0;
            idx[u * 3 + 2] = i + 0x0;
        }
    }
    else
    {
        int m, u;
        int sid = (8 - segment);
        int n = (16 + segment - (8 - direction)) % 8;
        u = (16 + (direction - 1) - n) % 8;
        idx[u * 3 + 0] = i + 0x0;
        idx[u * 3 + 1] = i + 0x9;
        idx[u * 3 + 2] = i + sid;
        for (m = 0; m < n; m++)
        {
            u = (16 + direction - (1 + m)) % 8;
            idx[u * 3 + 0] = i + 0x0;
            idx[u * 3 + 1] = i + 0x0;
            idx[u * 3 + 2] = i + 0x0;
        }
    }
}

void
mmUIVertex10AssignIndex32(
    uint32_t                                       idx[24],
    uint32_t                                       i,
    int                                            segment,
    int                                            direction,
    int                                            reverse)
{
    /*
     *    Radial360
     *
     *       0x9
     *   0x8-0x1-0x2
     *    | \ | / |
     *   0x7-0x0-0x3
     *    | / | \ |
     *   0x6-0x5-0x4
     *
     *    direction
     *
     *    7---0---1
     *    | \ | / |
     *    6---+---2
     *    | / | \ |
     *    5---4---3
     */

    idx[0x00] = i + 0x0; idx[0x06] = i + 0x0;
    idx[0x01] = i + 0x2; idx[0x07] = i + 0x4;
    idx[0x02] = i + 0x1; idx[0x08] = i + 0x3;
    idx[0x03] = i + 0x0; idx[0x09] = i + 0x0;
    idx[0x04] = i + 0x3; idx[0x0A] = i + 0x5;
    idx[0x05] = i + 0x2; idx[0x0B] = i + 0x4;

    idx[0x0C] = i + 0x0; idx[0x12] = i + 0x0;
    idx[0x0D] = i + 0x6; idx[0x13] = i + 0x8;
    idx[0x0E] = i + 0x5; idx[0x14] = i + 0x7;
    idx[0x0F] = i + 0x0; idx[0x15] = i + 0x0;
    idx[0x10] = i + 0x7; idx[0x16] = i + 0x1;
    idx[0x11] = i + 0x6; idx[0x17] = i + 0x8;

    if (reverse)
    {
        int m, u;
        int sid = (8 - segment) % 8 + 1;
        int n = 7 - (16 + segment - (8 - direction)) % 8;
        u = (16 + 7 - segment) % 8;
        idx[u * 3 + 0] = i + 0x0;
        idx[u * 3 + 1] = i + sid;
        idx[u * 3 + 2] = i + 0x9;
        for (m = 0; m < n; m++)
        {
            u = (16 + direction + m) % 8;
            idx[u * 3 + 0] = i + 0x0;
            idx[u * 3 + 1] = i + 0x0;
            idx[u * 3 + 2] = i + 0x0;
        }
    }
    else
    {
        int m, u;
        int sid = (8 - segment);
        int n = (16 + segment - (8 - direction)) % 8;
        u = (16 + (direction - 1) - n) % 8;
        idx[u * 3 + 0] = i + 0x0;
        idx[u * 3 + 1] = i + 0x9;
        idx[u * 3 + 2] = i + sid;
        for (m = 0; m < n; m++)
        {
            u = (16 + direction - (1 + m)) % 8;
            idx[u * 3 + 0] = i + 0x0;
            idx[u * 3 + 1] = i + 0x0;
            idx[u * 3 + 2] = i + 0x0;
        }
    }
}

void
mmUIVertex16AssignFrameSlice(
    struct mmUIVertex                              vtx[16],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    const float                                    size[2])
{
    const int*        frame = iframe->frame;
    const int*       offset = iframe->offset;
    const int*     original = iframe->original;
    const int*      borders = iframe->borders;
    const size_t* imageSize = iframe->imageSize;

    int    hFlippedX = flipped[0];
    int    hFlippedY = flipped[1];
    int hRectRotated = iframe->r;

    int ofl, ofb, ofr, oft;
    int fow, foh;
    int fbl, fbb, fbr, fbt;

    float fu0, fu1, fu2, fu3;
    float fv0, fv1, fv2, fv3;

    float x0, x1, x2, x3;
    float y0, y1, y2, y3;
    float z0 = 0.0f;

    float u0, u1, u2, u3;
    float v0, v1, v2, v3;

    if (hRectRotated)
    {
        fow = original[1];
        foh = original[0];

        fbl = borders[1];
        fbb = borders[2];
        fbr = borders[3];
        fbt = borders[0];

        ofl = original[1] - (offset[1] + frame[2]);
        ofb = original[0] - (offset[0] + frame[3]);
        ofr = offset[1];
        oft = offset[0];
    }
    else
    {
        fow = original[0];
        foh = original[1];

        fbl = borders[0];
        fbb = borders[1];
        fbr = borders[2];
        fbt = borders[3];

        ofl = offset[0];
        ofb = original[1] - (offset[1] + frame[2]);
        ofr = original[0] - (offset[0] + frame[3]);
        oft = offset[1];
    }

    fu0 = (float)(frame[0]/*              */) / imageSize[0];
    fu1 = (float)(frame[0] - ofl + fbl/*  */) / imageSize[0];
    fu2 = (float)(frame[0] - ofl - fbr + fow) / imageSize[0];
    fu3 = (float)(frame[0] + frame[2]/*   */) / imageSize[0];

    fv0 = (float)(frame[1]/*              */) / imageSize[1];
    fv1 = (float)(frame[1] - oft + fbt/*  */) / imageSize[1];
    fv2 = (float)(frame[1] - oft - fbb + foh) / imageSize[1];
    fv3 = (float)(frame[1] + frame[3]/*   */) / imageSize[1];

    if (hRectRotated)
    {
        x0 = (float)(/*                    */offset[0]/*       */);
        x1 = (float)(/*      */borders[0]/*                    */);
        x2 = (float)(size[0] - borders[2]/*                    */);
        x3 = (float)(size[0] - original[0] + offset[0] + frame[3]);

        y0 = (float)(/*                    */offset[1]/*       */);
        y1 = (float)(/*      */borders[3]/*                    */);
        y2 = (float)(size[1] - borders[1]/*                    */);
        y3 = (float)(size[1] - original[1] + offset[1] + frame[2]);
    }
    else
    {
        x0 = (float)(/*                    */offset[0]/*       */);
        x1 = (float)(/*      */borders[0]/*                    */);
        x2 = (float)(size[0] - borders[2]/*                    */);
        x3 = (float)(size[0] - original[0] + offset[0] + frame[2]);

        y0 = (float)(/*                    */offset[1]/*       */);
        y1 = (float)(/*      */borders[3]/*                    */);
        y2 = (float)(size[1] - borders[1]/*                    */);
        y3 = (float)(size[1] - original[1] + offset[1] + frame[3]);
    }

    // position
    // row 0
    vtx[0x0].position[0] = x0; vtx[0x1].position[0] = x1; vtx[0x2].position[0] = x2; vtx[0x3].position[0] = x3;
    vtx[0x0].position[1] = y0; vtx[0x1].position[1] = y0; vtx[0x2].position[1] = y0; vtx[0x3].position[1] = y0;
    vtx[0x0].position[2] = z0; vtx[0x1].position[2] = z0; vtx[0x2].position[2] = z0; vtx[0x3].position[2] = z0;
    // row 1
    vtx[0x4].position[0] = x0; vtx[0x5].position[0] = x1; vtx[0x6].position[0] = x2; vtx[0x7].position[0] = x3;
    vtx[0x4].position[1] = y1; vtx[0x5].position[1] = y1; vtx[0x6].position[1] = y1; vtx[0x7].position[1] = y1;
    vtx[0x4].position[2] = z0; vtx[0x5].position[2] = z0; vtx[0x6].position[2] = z0; vtx[0x7].position[2] = z0;
    // row 2
    vtx[0x8].position[0] = x0; vtx[0x9].position[0] = x1; vtx[0xA].position[0] = x2; vtx[0xB].position[0] = x3;
    vtx[0x8].position[1] = y2; vtx[0x9].position[1] = y2; vtx[0xA].position[1] = y2; vtx[0xB].position[1] = y2;
    vtx[0x8].position[2] = z0; vtx[0x9].position[2] = z0; vtx[0xA].position[2] = z0; vtx[0xB].position[2] = z0;
    // row 3
    vtx[0xC].position[0] = x0; vtx[0xD].position[0] = x1; vtx[0xE].position[0] = x2; vtx[0xF].position[0] = x3;
    vtx[0xC].position[1] = y3; vtx[0xD].position[1] = y3; vtx[0xE].position[1] = y3; vtx[0xF].position[1] = y3;
    vtx[0xC].position[2] = z0; vtx[0xD].position[2] = z0; vtx[0xE].position[2] = z0; vtx[0xF].position[2] = z0;

    if ((!hRectRotated && hFlippedX) || (hRectRotated && hFlippedY))
    {
        u0 = fu3; u1 = fu2; u2 = fu1; u3 = fu0;
    }
    else
    {
        u0 = fu0; u1 = fu1; u2 = fu2; u3 = fu3;
    }

    if ((!hRectRotated && hFlippedY) || (hRectRotated && hFlippedX))
    {
        v0 = fv3; v1 = fv2; v2 = fv1; v3 = fv0;
    }
    else
    {
        v0 = fv0; v1 = fv1; v2 = fv2; v3 = fv3;
    }

    if (hRectRotated)
    {
        // row 0
        vtx[0x0].texcoord[0] = u3; vtx[0x1].texcoord[0] = u3; vtx[0x2].texcoord[0] = u3; vtx[0x3].texcoord[0] = u3;
        vtx[0x0].texcoord[1] = v0; vtx[0x1].texcoord[1] = v1; vtx[0x2].texcoord[1] = v2; vtx[0x3].texcoord[1] = v3;
        // row 1
        vtx[0x4].texcoord[0] = u2; vtx[0x5].texcoord[0] = u2; vtx[0x6].texcoord[0] = u2; vtx[0x7].texcoord[0] = u2;
        vtx[0x4].texcoord[1] = v0; vtx[0x5].texcoord[1] = v1; vtx[0x6].texcoord[1] = v2; vtx[0x7].texcoord[1] = v3;
        // row 2
        vtx[0x8].texcoord[0] = u1; vtx[0x9].texcoord[0] = u1; vtx[0xA].texcoord[0] = u1; vtx[0xB].texcoord[0] = u1;
        vtx[0x8].texcoord[1] = v0; vtx[0x9].texcoord[1] = v1; vtx[0xA].texcoord[1] = v2; vtx[0xB].texcoord[1] = v3;
        // row 3
        vtx[0xC].texcoord[0] = u0; vtx[0xD].texcoord[0] = u0; vtx[0xE].texcoord[0] = u0; vtx[0xF].texcoord[0] = u0;
        vtx[0xC].texcoord[1] = v0; vtx[0xD].texcoord[1] = v1; vtx[0xE].texcoord[1] = v2; vtx[0xF].texcoord[1] = v3;
    }
    else
    {
        // row 0
        vtx[0x0].texcoord[0] = u0; vtx[0x1].texcoord[0] = u1; vtx[0x2].texcoord[0] = u2; vtx[0x3].texcoord[0] = u3;
        vtx[0x0].texcoord[1] = v0; vtx[0x1].texcoord[1] = v0; vtx[0x2].texcoord[1] = v0; vtx[0x3].texcoord[1] = v0;
        // row 1
        vtx[0x4].texcoord[0] = u0; vtx[0x5].texcoord[0] = u1; vtx[0x6].texcoord[0] = u2; vtx[0x7].texcoord[0] = u3;
        vtx[0x4].texcoord[1] = v1; vtx[0x5].texcoord[1] = v1; vtx[0x6].texcoord[1] = v1; vtx[0x7].texcoord[1] = v1;
        // row 2
        vtx[0x8].texcoord[0] = u0; vtx[0x9].texcoord[0] = u1; vtx[0xA].texcoord[0] = u2; vtx[0xB].texcoord[0] = u3;
        vtx[0x8].texcoord[1] = v2; vtx[0x9].texcoord[1] = v2; vtx[0xA].texcoord[1] = v2; vtx[0xB].texcoord[1] = v2;
        // row 3
        vtx[0xC].texcoord[0] = u0; vtx[0xD].texcoord[0] = u1; vtx[0xE].texcoord[0] = u2; vtx[0xF].texcoord[0] = u3;
        vtx[0xC].texcoord[1] = v3; vtx[0xD].texcoord[1] = v3; vtx[0xE].texcoord[1] = v3; vtx[0xF].texcoord[1] = v3;
    }
}

void
mmUIVertex16AssignColor(
    struct mmUIVertex                              vtx[16],
    const float                                    color[4])
{
    mmVec4Assign(vtx[0x0].color, color); 
    mmVec4Assign(vtx[0x1].color, color); 
    mmVec4Assign(vtx[0x2].color, color); 
    mmVec4Assign(vtx[0x3].color, color);

    mmVec4Assign(vtx[0x4].color, color); 
    mmVec4Assign(vtx[0x5].color, color); 
    mmVec4Assign(vtx[0x6].color, color); 
    mmVec4Assign(vtx[0x7].color, color);

    mmVec4Assign(vtx[0x8].color, color); 
    mmVec4Assign(vtx[0x9].color, color); 
    mmVec4Assign(vtx[0xA].color, color); 
    mmVec4Assign(vtx[0xB].color, color);

    mmVec4Assign(vtx[0xC].color, color); 
    mmVec4Assign(vtx[0xD].color, color); 
    mmVec4Assign(vtx[0xE].color, color); 
    mmVec4Assign(vtx[0xF].color, color);
}

void
mmUIVertex16AssignIndex16(
    uint16_t                                       idx[54],
    uint16_t                                       i)
{
    // 0x0-0x1-0x2-0x3
    //  | \ | \ | \ |  
    // 0x4-0x5-0x6-0x7
    //  | \ | \ | \ |  
    // 0x8-0x9-0xA-0xB
    //  | \ | \ | \ |  
    // 0xC-0xD-0xE-0xF

    // 0x0-0x1-0x2-0x3
    //  | \ | \ | \ |  
    // 0x4-0x5-0x6-0x7
    //  (0, 0)                  (0, 1)                  (0, 2)
    idx[0x00] = i + 0x0; idx[0x06] = i + 0x1; idx[0x0C] = i + 0x2;
    idx[0x01] = i + 0x4; idx[0x07] = i + 0x5; idx[0x0D] = i + 0x6;
    idx[0x02] = i + 0x5; idx[0x08] = i + 0x6; idx[0x0E] = i + 0x7;
    idx[0x03] = i + 0x0; idx[0x09] = i + 0x1; idx[0x0F] = i + 0x2;
    idx[0x04] = i + 0x5; idx[0x0A] = i + 0x6; idx[0x10] = i + 0x7;
    idx[0x05] = i + 0x1; idx[0x0B] = i + 0x2; idx[0x11] = i + 0x3;

    // 0x4-0x5-0x6-0x7
    //  | \ | \ | \ |  
    // 0x8-0x9-0xA-0xB
    //  (1, 0)                  (1, 1)                  (1, 2)
    idx[0x12] = i + 0x4; idx[0x18] = i + 0x5; idx[0x1E] = i + 0x6;
    idx[0x13] = i + 0x8; idx[0x19] = i + 0x9; idx[0x1F] = i + 0xA;
    idx[0x14] = i + 0x9; idx[0x1A] = i + 0xA; idx[0x20] = i + 0xB;
    idx[0x15] = i + 0x4; idx[0x1B] = i + 0x5; idx[0x21] = i + 0x6;
    idx[0x16] = i + 0x9; idx[0x1C] = i + 0xA; idx[0x22] = i + 0xB;
    idx[0x17] = i + 0x5; idx[0x1D] = i + 0x6; idx[0x23] = i + 0x7;

    // 0x8-0x9-0xA-0xB
    //  | \ | \ | \ |  
    // 0xC-0xD-0xE-0xF
    //  (1, 0)                  (1, 1)                  (1, 2)
    idx[0x24] = i + 0x8; idx[0x2A] = i + 0x9; idx[0x30] = i + 0xA;
    idx[0x25] = i + 0xC; idx[0x2B] = i + 0xD; idx[0x31] = i + 0xE;
    idx[0x26] = i + 0xD; idx[0x2C] = i + 0xE; idx[0x32] = i + 0xF;
    idx[0x27] = i + 0x8; idx[0x2D] = i + 0x9; idx[0x33] = i + 0xA;
    idx[0x28] = i + 0xD; idx[0x2E] = i + 0xE; idx[0x34] = i + 0xF;
    idx[0x29] = i + 0x9; idx[0x2F] = i + 0xA; idx[0x35] = i + 0xB;
}

void
mmUIVertex16AssignIndex32(
    uint32_t                                       idx[54],
    uint32_t                                       i)
{
    // 0x0-0x1-0x2-0x3
    //  | \ | \ | \ |  
    // 0x4-0x5-0x6-0x7
    //  | \ | \ | \ |  
    // 0x8-0x9-0xA-0xB
    //  | \ | \ | \ |  
    // 0xC-0xD-0xE-0xF

    // 0x0-0x1-0x2-0x3
    //  | \ | \ | \ |  
    // 0x4-0x5-0x6-0x7
    //  (0, 0)                  (0, 1)                  (0, 2)
    idx[0x00] = i + 0x0; idx[0x06] = i + 0x1; idx[0x0C] = i + 0x2;
    idx[0x01] = i + 0x4; idx[0x07] = i + 0x5; idx[0x0D] = i + 0x6;
    idx[0x02] = i + 0x5; idx[0x08] = i + 0x6; idx[0x0E] = i + 0x7;
    idx[0x03] = i + 0x0; idx[0x09] = i + 0x1; idx[0x0F] = i + 0x2;
    idx[0x04] = i + 0x5; idx[0x0A] = i + 0x6; idx[0x10] = i + 0x7;
    idx[0x05] = i + 0x1; idx[0x0B] = i + 0x2; idx[0x11] = i + 0x3;

    // 0x4-0x5-0x6-0x7
    //  | \ | \ | \ |  
    // 0x8-0x9-0xA-0xB
    //  (1, 0)                  (1, 1)                  (1, 2)
    idx[0x12] = i + 0x4; idx[0x18] = i + 0x5; idx[0x1E] = i + 0x6;
    idx[0x13] = i + 0x8; idx[0x19] = i + 0x9; idx[0x1F] = i + 0xA;
    idx[0x14] = i + 0x9; idx[0x1A] = i + 0xA; idx[0x20] = i + 0xB;
    idx[0x15] = i + 0x4; idx[0x1B] = i + 0x5; idx[0x21] = i + 0x6;
    idx[0x16] = i + 0x9; idx[0x1C] = i + 0xA; idx[0x22] = i + 0xB;
    idx[0x17] = i + 0x5; idx[0x1D] = i + 0x6; idx[0x23] = i + 0x7;

    // 0x8-0x9-0xA-0xB
    //  | \ | \ | \ |  
    // 0xC-0xD-0xE-0xF
    //  (1, 0)                  (1, 1)                  (1, 2)
    idx[0x24] = i + 0x8; idx[0x2A] = i + 0x9; idx[0x30] = i + 0xA;
    idx[0x25] = i + 0xC; idx[0x2B] = i + 0xD; idx[0x31] = i + 0xE;
    idx[0x26] = i + 0xD; idx[0x2C] = i + 0xE; idx[0x32] = i + 0xF;
    idx[0x27] = i + 0x8; idx[0x2D] = i + 0x9; idx[0x33] = i + 0xA;
    idx[0x28] = i + 0xD; idx[0x2E] = i + 0xE; idx[0x34] = i + 0xF;
    idx[0x29] = i + 0x9; idx[0x2F] = i + 0xA; idx[0x35] = i + 0xB;
}
