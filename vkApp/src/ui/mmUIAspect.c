/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIAspect.h"

#include "algorithm/mmMurmurHash.h"

const char*
mmUIAspectMode_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case mmUIAspectModeIgnore:
        return "Ignore";
    case mmUIAspectModeShrink:
        return "Shrink";
    case mmUIAspectModeExpand:
        return "Expand";
    case mmUIAspectModeEntire:
        return "Entire";
    case mmUIAspectModeBasedX:
        return "BasedX";
    case mmUIAspectModeBasedY:
        return "BasedY";
    default:
        return "Ignore";
    }
}

int
mmUIAspectMode_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x23B10ABE:
        // 0x23B10ABE Ignore
        return mmUIAspectModeIgnore;
    case 0xEEAA6389:
        // 0xEEAA6389 Shrink
        return mmUIAspectModeShrink;
    case 0x92640171:
        // 0x92640171 Expand
        return mmUIAspectModeExpand;
    case 0xDC5B2334:
        // 0xDC5B2334 Entire
        return mmUIAspectModeEntire;
    case 0x0EB4CE85:
        // 0x0EB4CE85 BasedX
        return mmUIAspectModeBasedX;
    case 0x1234FF52:
        // 0x1234FF52 BasedY
        return mmUIAspectModeBasedY;
    default:
        return mmUIAspectModeIgnore;
    }
}

void 
mmVec2SizeScaleToAspect(
    const float                                    vsize[2], 
    const float                                    fsize[2], 
    mmUInt8_t                                      mode, 
    float                                          ratio,
    float                                          r[2])
{
    float vw = vsize[0];
    float vh = vsize[1];
    float fw = fsize[0];
    float fh = fsize[1];

    switch (mode)
    {
    case mmUIAspectModeIgnore:
    {
        r[0] = fw;
        r[1] = fh;
    }
    break;

    case mmUIAspectModeShrink:
    {
        if (0 == ratio)
        {
            // depend for the v.
            if (fw * vh > fh * vw)
            {
                // x base.
                float sx = (0 == fw) ? 1.0f : vw / fw;
                r[0] = fw * sx;
                r[1] = fh * sx;
            }
            else
            {
                // y base.
                float sy = (0 == fh) ? 1.0f : vh / fh;
                r[0] = fw * sy;
                r[1] = fh * sy;
            }
        }
        else
        {
            // use ratio.
            float rw = fh * ratio;
            float rh = fw / ratio;
            if (rw <= fw)
            {
                r[0] = rw;
                r[1] = fh;
            }
            else
            {
                r[0] = fw;
                r[1] = rh;
            }
        }
    }
    break;

    case mmUIAspectModeExpand:
    {
        if (0 == ratio)
        {
            if (fw * vh < fh * vw)
            {
                // x base.
                float sx = (0 == fw) ? 1.0f : vw / fw;
                r[0] = fw * sx;
                r[1] = fh * sx;
            }
            else
            {
                // y base.
                float sy = (0 == fh) ? 1.0f : vh / fh;
                r[0] = fw * sy;
                r[1] = fh * sy;
            }
        }
        else
        {
            // use ratio.
            float rw = fh * ratio;
            float rh = fw / ratio;
            if (rw >= fw)
            {
                r[0] = rw;
                r[1] = fh;
            }
            else
            {
                r[0] = fw;
                r[1] = rh;
            }
        }
    }
    break;

    case mmUIAspectModeEntire:
    {
        r[0] = vw;
        r[1] = vh;
    }
    break;

    case mmUIAspectModeBasedX:
    {
        // x base.
        float sx = (0 == fw) ? 1.0f : vw / fw;
        r[0] = fw * sx;
        r[1] = fh * sx;
    }
    break;

    case mmUIAspectModeBasedY:
    {
        // y base.
        float sy = (0 == fh) ? 1.0f : vh / fh;
        r[0] = fw * sy;
        r[1] = fh * sy;
    }
    break;

    default:
    {
        r[0] = fw;
        r[1] = fh;
    }
    break;
    }
}

void 
mmVec2SizeAspectToScale(
    const float                                    vsize[2],
    const float                                    fsize[2],
    mmUInt8_t                                      mode,
    float                                          ratio,
    float                                          s[2])
{
    float vw = vsize[0];
    float vh = vsize[1];
    float fw = fsize[0];
    float fh = fsize[1];

    switch (mode)
    {
    case mmUIAspectModeIgnore:
    {
        s[0] = 1.0f;
        s[1] = 1.0f;
    }
    break;

    case mmUIAspectModeShrink:
    {
        if (0 == ratio)
        {
            if (fw * vh > fh * vw)
            {
                // x base.
                float sx = (0 == fw) ? 1.0f : vw / fw;
                s[0] = sx;
                s[1] = sx;
            }
            else
            {
                // y base.
                float sy = (0 == fh) ? 1.0f : vh / fh;
                s[0] = sy;
                s[1] = sy;
            }
        }
        else
        {
            // use ratio.
            float rw = fh * ratio;
            float rh = fw / ratio;
            if (rw <= fw)
            {
                s[0] = (0 == fw) ? 1.0f : rw / fw;
                s[1] = 1.0f;
            }
            else
            {
                s[0] = 1.0f;
                s[1] = (0 == fh) ? 1.0f : rh / fh;
            }
        }
    }
    break;

    case mmUIAspectModeExpand:
    {
        if (0 == ratio)
        {
            if (fw * vh < fh * vw)
            {
                // x base.
                float sx = (0 == fw) ? 1.0f : vw / fw;
                s[0] = sx;
                s[1] = sx;
            }
            else
            {
                // y base.
                float sy = (0 == fh) ? 1.0f : vh / fh;
                s[0] = sy;
                s[1] = sy;
            }
        }
        else
        {
            // use ratio.
            float rw = fh * ratio;
            float rh = fw / ratio;
            if (rw >= fw)
            {
                s[0] = (0 == fw) ? 1.0f : rw / fw;
                s[1] = 1.0f;
            }
            else
            {
                s[0] = 1.0f;
                s[1] = (0 == fh) ? 1.0f : rh / fh;
            }
        }
    }
    break;

    case mmUIAspectModeEntire:
    {
        s[0] = (0 == fw) ? 1.0f : vw / fw;
        s[1] = (0 == fh) ? 1.0f : vh / fh;
    }
    break;

    case mmUIAspectModeBasedX:
    {
        // x base.
        float sx = (0 == fw) ? 1.0f : vw / fw;
        s[0] = sx;
        s[1] = sx;
    }
    break;

    case mmUIAspectModeBasedY:
    {
        // y base.
        float sy = (0 == fh) ? 1.0f : vh / fh;
        s[0] = sy;
        s[1] = sy;
    }
    break;

    default:
    {
        s[0] = 1.0f;
        s[1] = 1.0f;
    }
    break;
    }
}
