/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIDrawable_h__
#define __mmUIDrawable_h__

#include "core/mmCore.h"

#include "ui/mmUIQuad.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUploader.h"
#include "vk/mmVKUniformType.h"
#include "vk/mmVKDescriptorSet.h"

#include "core/mmPrefix.h"

struct mmPropertySet;
struct mmVKAssets;
struct mmUIView;
struct mmVKPipeline;
struct mmVKPipelineInfo;
struct mmVKPoolInfo;
struct mmVKImageInfo;
struct mmVKSamplerInfo;

struct mmUIDrawableUBOVS
{
    mmUniformMat4 viewProjection;
};

struct mmUIDrawablePushConstant
{
    mmUniformMat4 model;
    mmUniformVec4 color;
};

/* mmUIImageFrame Vertex mode. */
enum mmUIImageMode
{
    mmUIImageModeBasic = 0,
    mmUIImageModeSlice = 1,
    mmUIImageModeTiled = 2,
    mmUIImageModeBrush = 3,
};

const char*
mmUIImageMode_EncodeEnumerate(
    int                                            v);

int
mmUIImageMode_DecodeEnumerate(
    const char*                                    w);

/* mmUIImageFrame Vertex Brush mode. */
enum mmUIImageBrushMode
{
    /*
     *   Direction 0 Horizontal    left to right.
     *             1 Vertical      top to bottom.
     *
     *   Reverse   0 positive  
     *             1 negative
     */
    mmUIImageBrushLinear = 0,

    /*
     *   Direction 0 Origin Top    clockwise.
     *             2 Origin Left   clockwise.
     *             4 Origin Bottom clockwise.
     *             6 Origin Right  clockwise.
     *
     *   Reverse   0 clockwise  
     *             1 counterclockwise
     *
     *   Note: The Direction(1, 3, 5, 7) is also valid.
     *         But the image should be square.
     */
    mmUIImageBrushCircle = 1,
};

const char*
mmUIImageBrushMode_EncodeEnumerate(
    int                                            v);

int
mmUIImageBrushMode_DecodeEnumerate(
    const char*                                    w);

void
mmUIDrawable_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIDrawable_RemovePropertys(
    struct mmPropertySet*                          propertys);

struct mmUIDrawable
{
    struct mmVKBuffer vIdxBuffer;
    struct mmVKBuffer vVtxBuffer;

    struct mmUIImageFrame vIframe;
    struct mmUIDrawablePushConstant vConstant;

    struct mmVKDescriptorSet vDescriptorSet;

    struct mmVKPipelineInfo* pPipelineInfo;
    struct mmVKPoolInfo* pPoolInfo;

    uint32_t hIdxCount;
    uint32_t hVtxCount;

    VkIndexType hIdxType;

    float hAmount;
    float hOpacity[4];
    float hColor[4];

    // aspect ratio, default is 0.0f.
    float hAspectRatio;

    // enum mmUIAspectMode
    // aspect mode, default is mmUIAspectModeIgnore.
    mmUInt8_t hAspectMode;

    mmUInt8_t hAlign[3];
    mmUInt8_t hImageMode;

    mmUInt8_t hBrushMode;
    mmUInt8_t hDirection;
    mmUInt8_t hReverse;

    mmUInt8_t hFlipped[2];

    struct mmString hPipelineName;
    struct mmString hPoolName;

    struct mmString hValueColor;
};

void
mmUIDrawable_Init(
    struct mmUIDrawable*                           p);

void
mmUIDrawable_Destroy(
    struct mmUIDrawable*                           p);

void
mmUIDrawable_SetPipelineCStr(
    struct mmUIDrawable*                           p,
    const char*                                    name);

VkResult
mmUIDrawable_PrepareBufferBasic(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBufferSlice(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBufferTiled(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBufferBrush(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBufferBrushLinear(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBufferBrushCircle(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PrepareBuffer(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

void
mmUIDrawable_DiscardBuffer(
    struct mmUIDrawable*                           p,
    struct mmVKUploader*                           pUploader);


VkResult
mmUIDrawable_UpdateBufferBasic(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBufferSlice(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBufferTiled(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBufferBrush(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBufferBrushLinear(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBufferBrushCircle(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_UpdateBuffer(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKUploader*                           pUploader,
    struct mmUIImageFrame*                         iframe);

VkResult
mmUIDrawable_PreparePipeline(
    struct mmUIDrawable*                           p,
    struct mmVKAssets*                             pAssets);

void
mmUIDrawable_DiscardPipeline(
    struct mmUIDrawable*                           p);

VkResult
mmUIDrawable_PreparePool(
    struct mmUIDrawable*                           p,
    struct mmVKAssets*                             pAssets);

void
mmUIDrawable_DiscardPool(
    struct mmUIDrawable*                           p);

VkResult
mmUIDrawable_UpdatePool(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView);

VkResult
mmUIDrawable_UpdatePipeline(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView);

void
mmUIDrawable_GetMat4Model(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    float                                          model[4][4]);

void
mmUIDrawable_GetVec4Color(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    float                                          color[4]);

void
mmUIDrawable_UpdatePushConstants(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView);

VkResult
mmUIDrawable_DescriptorSetProduce(
    struct mmUIDrawable*                           p);

void
mmUIDrawable_DescriptorSetRecycle(
    struct mmUIDrawable*                           p);

VkResult
mmUIDrawable_DescriptorSetUpdate(
    struct mmUIDrawable*                           p,
    struct mmUIView*                               pView,
    struct mmVKImageInfo*                          pImageInfo,
    struct mmVKSamplerInfo*                        pSamplerInfo);

void
mmUIDrawable_OnRecord(
    struct mmUIDrawable*                           p,
    VkCommandBuffer                                cmdBuffer);

#include "core/mmSuffix.h"

#endif//__mmUIDrawable_h__
