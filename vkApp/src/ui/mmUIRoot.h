/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIRoot_h__
#define __mmUIRoot_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmPropertySet;

void
mmUIRoot_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIRoot_RemovePropertys(
    struct mmPropertySet*                          propertys);

struct mmUIRoot
{
    // root canvas size. use for aspect.
    // default is (378, 672).
    float hCanvasSize[2];

    // aspect ratio, default is 0.0f.
    float hAspectRatio;

    // enum mmUIAspectMode
    // aspect mode, default is mmUIAspectModeExpand.
    mmUInt8_t hAspectMode;
};

void
mmUIRoot_Init(
    struct mmUIRoot*                               p);

void
mmUIRoot_Destroy(
    struct mmUIRoot*                               p);

#include "core/mmSuffix.h"

#endif//__mmUIRoot_h__
