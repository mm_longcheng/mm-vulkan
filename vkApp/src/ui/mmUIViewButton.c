/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewButton.h"

#include "ui/mmUIViewContext.h"

#include "core/mmLogger.h"
#include "core/mmKeyCode.h"

#include "nwsi/mmISurface.h"

#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKCommandBuffer.h"

void
mmUIViewButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewButton, view));
    mmUIDrawable_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewButton, drawable));
    mmUIImage_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewButton, image));
    mmUIButton_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewButton, button));
}

void
mmUIViewButton_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIButton_RemovePropertys(propertys);
    mmUIImage_RemovePropertys(propertys);
    mmUIDrawable_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewButton =
{
    &mmUIViewButton_OnPrepare,
    &mmUIViewButton_OnDiscard,
};

const struct mmUIWidgetVariable mmUIWidgetVariableViewButton =
{
    &mmUIViewButton_OnUpdate,
    &mmUIViewButton_OnInvalidate,
    &mmUIViewButton_OnSized,
    &mmUIViewButton_OnChanged,
};

const struct mmUIWidgetDrawable mmUIWidgetDrawableViewButton =
{
    &mmUIViewButton_OnDescriptorSetProduce,
    &mmUIViewButton_OnDescriptorSetRecycle,
    &mmUIViewButton_OnDescriptorSetUpdate,
    &mmUIViewButton_OnUpdatePushConstants,
    &mmUIViewButton_OnUpdatePipeline,
    &mmUIViewButton_OnRecord,
};

const struct mmUIResponderCursor mmUIResponderCursorViewButton = 
{
    &mmUIViewButton_OnCursorBegan,
    &mmUIViewButton_OnCursorMoved,
    &mmUIViewButton_OnCursorEnded,
    &mmUIViewButton_OnCursorBreak,
    &mmUIViewButton_OnCursorWheel,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewButton =
{
    "mmUIViewButton",
    sizeof(struct mmUIViewButton),
    &mmUIViewButton_Produce,
    &mmUIViewButton_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewButton =
{
    &mmUIMetaAllocatorViewButton,
    /*- Widget -*/
    &mmUIWidgetOperableViewButton,
    &mmUIWidgetVariableViewButton,
    &mmUIWidgetDrawableViewButton,
    /*- Responder -*/
    &mmUIResponderCursorViewButton,
    NULL,
    NULL,
    NULL,
};

void
mmUIViewButton_Init(
    struct mmUIViewButton*                         p)
{
    mmUIView_Init(&p->view);
    mmUIDrawable_Init(&p->drawable);
    mmUIImage_Init(&p->image);
    mmUIButton_Init(&p->button);
}

void
mmUIViewButton_Destroy(
    struct mmUIViewButton*                         p)
{
    mmUIButton_Destroy(&p->button);
    mmUIImage_Destroy(&p->image);
    mmUIDrawable_Destroy(&p->drawable);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewButton_Produce(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_Init(p);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewButton;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);

    mmUIViewButton_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewButton_Recycle(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_RemovePropertys(&p->view.propertys);

    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewButton;
    
    mmUIViewButton_Destroy(p);
}

int
mmUIViewButton_UpdateState(
    struct mmUIViewButton*                         p)
{
    int state = mmUIButton_GetState(&p->button);
    mmString_Assign(&p->image.hSheetName, &p->button.hSheetName);
    mmUIButton_UpdateViewFilter(&p->button, &p->view);
    mmUIButton_UpdateStateOpacity(&p->button, state, p->drawable.hOpacity);
    return mmUIButton_UpdateStateFrameName(&p->button, state, &p->image.hFrameName);
}

void
mmUIViewButton_UpdateImage(
    struct mmUIViewButton*                         p)
{
    if (mmUIViewButton_UpdateState(p))
    {
        // Note: we must wait idle before update content.
        mmUIViewContext_DeviceWaitIdle(p->view.context);
        mmUIImage_UpdateFrame(&p->image, &p->view, &p->drawable);
        mmUIViewButton_OnDescriptorSetUpdate(p);
    }
    mmUIButton_UpdatePipelineName(&p->button, &p->drawable.hPipelineName);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void
mmUIViewButton_UpdateSize(
    struct mmUIViewButton*                         p)
{
    mmUIImage_UpdateSize(&p->image, &p->view, &p->drawable);
}

void
mmUIViewButton_OnPrepare(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_UpdateState(p);

    mmUIImage_Prepare(
        &p->image,
        &p->view,
        &p->drawable);

    mmUIViewButton_OnDescriptorSetProduce(p);
    mmUIViewButton_OnDescriptorSetUpdate(p);
    mmUIViewButton_OnUpdatePushConstants(p);
}

void
mmUIViewButton_OnDiscard(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_OnDescriptorSetRecycle(p);

    mmUIImage_Discard(
        &p->image,
        &p->view,
        &p->drawable);
}

void
mmUIViewButton_OnUpdate(
    struct mmUIViewButton*                         p,
    struct mmEventUpdate*                          content)
{
    // do nothing here, interface api.
}

void
mmUIViewButton_OnInvalidate(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_OnUpdatePushConstants(p);
}

void
mmUIViewButton_OnSized(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_UpdateSize(p);
    mmUIViewButton_OnUpdatePushConstants(p);
}

void
mmUIViewButton_OnChanged(
    struct mmUIViewButton*                         p)
{
    mmUIViewButton_UpdateImage(p);
    mmUIViewButton_OnUpdatePushConstants(p);
}

void
mmUIViewButton_OnDescriptorSetProduce(
    struct mmUIViewButton*                         p)
{
    mmUIDrawable_DescriptorSetProduce(&p->drawable);
}

void
mmUIViewButton_OnDescriptorSetRecycle(
    struct mmUIViewButton*                         p)
{
    mmUIDrawable_DescriptorSetRecycle(&p->drawable);
}

void
mmUIViewButton_OnDescriptorSetUpdate(
    struct mmUIViewButton*                         p)
{
    mmUIDrawable_DescriptorSetUpdate(
        &p->drawable,
        &p->view,
        p->image.pImageInfo,
        p->image.pSamplerInfo);
}

void
mmUIViewButton_OnUpdatePushConstants(
    struct mmUIViewButton*                         p)
{
    mmUIDrawable_UpdatePushConstants(&p->drawable, &p->view);
}

void
mmUIViewButton_OnUpdatePipeline(
    struct mmUIViewButton*                         p)
{
    mmUIDrawable_UpdatePool(&p->drawable, &p->view);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void
mmUIViewButton_OnRecord(
    struct mmUIViewButton*                         p,
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIDrawable_OnRecord(&p->drawable, cmd->cmdBuffer);
    mmUIView_OnRecordChildren(&p->view, cmd);
}

void
mmUIViewButton_OnCursorBegan(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content)
{
    do
    {
        struct mmUIViewContext* pViewContext = NULL;

        if (MM_MB_LBUTTON != content->button_id)
        {
            break;
        }

        pViewContext = p->view.context;

        if (NULL == pViewContext)
        {
            break;
        }

        if (pViewContext->capture != &p->view)
        {
            break;
        }

        p->button.hPushed = 1;
        p->button.hHovering = 1;

        mmUIViewButton_OnChanged(p);

    } while (0);
}

void
mmUIViewButton_OnCursorMoved(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content)
{
    do
    {
        int hHovering;

        struct mmUIViewContext* pViewContext = NULL;

        pViewContext = p->view.context;

        if (NULL == pViewContext)
        {
            break;
        }

        hHovering = (pViewContext->contain == &p->view);

        if (p->button.hHovering == hHovering)
        {
            break;
        }

        p->button.hHovering = hHovering;

        mmUIViewButton_OnChanged(p);

    } while (0);
}

void
mmUIViewButton_OnCursorEnded(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content)
{
    do
    {
        struct mmUIViewContext* pViewContext = NULL;

        if (MM_MB_LBUTTON != content->button_id)
        {
            break;
        }

        pViewContext = p->view.context;

        if (NULL == pViewContext)
        {
            break;
        }

        if (pViewContext->capture != &p->view)
        {
            break;
        }

        p->button.hPushed = 0;
        p->button.hHovering = 0;

        mmUIViewButton_OnChanged(p);

    } while (0);
}

void
mmUIViewButton_OnCursorBreak(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content)
{
    mmUIViewButton_OnCursorEnded(p, content);
}

void
mmUIViewButton_OnCursorWheel(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content)
{
    // do nothing here, interface api.
}
