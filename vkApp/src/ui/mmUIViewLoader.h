/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewLoader_h__
#define __mmUIViewLoader_h__

#include "core/mmCore.h"

#include "container/mmListVpt.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmUIViewFactory;
struct mmUIViewContext;
struct mmUIView;

struct mmUIParameterView;

struct mmUIViewLoader
{
    struct mmFileContext* pFileContext;
    struct mmUIViewFactory* pViewFactory;
    struct mmParseJSON parse;
    struct mmListVpt invalid;
};

void
mmUIViewLoader_Init(
    struct mmUIViewLoader*                         p);

void
mmUIViewLoader_Destroy(
    struct mmUIViewLoader*                         p);

void
mmUIViewLoader_SetFileContext(
    struct mmUIViewLoader*                         p,
    struct mmFileContext*                          pFileContext);

void
mmUIViewLoader_SetViewFactory(
    struct mmUIViewLoader*                         p,
    struct mmUIViewFactory*                        pViewFactory);

struct mmUIView*
mmUIViewLoader_ProduceViewFromInfo(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    struct mmUIParameterView*                      pParameter);

struct mmUIView*
mmUIViewLoader_ProduceViewFromFile(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    const char*                                    path);

void
mmUIViewLoader_RecycleView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView);

void
mmUIViewLoader_PrepareViewFromInfo(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView,
    struct mmUIParameterView*                      pParameter);

void
mmUIViewLoader_PrepareViewFromFile(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView,
    const char*                                    path);

void
mmUIViewLoader_DiscardView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView);

struct mmUIView*
mmUIViewLoader_ProduceViewType(
    struct mmUIViewLoader*                         p,
    struct mmUIViewContext*                        pViewContext,
    const char*                                    pType,
    const char*                                    pName);

void
mmUIViewLoader_RecycleViewType(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView);

void
mmUIViewLoader_InvalidView(
    struct mmUIViewLoader*                         p,
    struct mmUIView*                               pView);

void
mmUIViewLoader_DestroyInvalid(
    struct mmUIViewLoader*                         p);

#include "core/mmSuffix.h"

#endif//__mmUIViewLoader_h__
