/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUICube.h"

#include "core/mmAlloc.h"

void
mmUIDim_Reset(
    struct mmUIDim*                                p)
{
    p->o = 0.0f;
    p->s = 0.0f;
}

void
mmUIDim_Make(
    struct mmUIDim*                                p,
    float                                          o,
    float                                          s)
{
    p->o = o;
    p->s = s;
}

int
mmUIDim_Equals(
    const struct mmUIDim*                          p,
    const struct mmUIDim*                          q)
{
    return p->o == q->o && p->s == q->s;
}

void
mmUIDim_Assign(
    struct mmUIDim*                                p,
    const struct mmUIDim*                          q)
{
    p->o = q->o;
    p->s = q->s;
}

void
mmUIDim_Add(
    struct mmUIDim*                                r,
    const struct mmUIDim*                          a,
    const struct mmUIDim*                          b)
{
    r->o = a->o + b->o;
    r->s = a->s + b->s;
}

void
mmUIDim_Sub(
    struct mmUIDim*                                r,
    const struct mmUIDim*                          a,
    const struct mmUIDim*                          b)
{
    r->o = a->o - b->o;
    r->s = a->s - b->s;
}

int
mmUIDimVec3Equals(
    struct mmUIDim                                 p[3],
    const struct mmUIDim                           q[3])
{
    return
        mmUIDim_Equals(&p[0], &q[0]) &&
        mmUIDim_Equals(&p[1], &q[1]) &&
        mmUIDim_Equals(&p[2], &q[2]);
}

void
mmUIDimVec3Assign(
    struct mmUIDim                                 v[3],
    const struct mmUIDim                           r[3])
{
    mmMemcpy(v, r, sizeof(struct mmUIDim) * 3);
}

void
mmUIDimVec3Reset(
    struct mmUIDim                                 v[3])
{
    mmMemset(v, 0, sizeof(struct mmUIDim) * 3);
}

void
mmUIDimVec3Make(
    struct mmUIDim                                 v[3],
    float                                          xo,
    float                                          xs,
    float                                          yo,
    float                                          ys,
    float                                          zo,
    float                                          zs)
{
    mmUIDim_Make(&v[0], xo, xs);
    mmUIDim_Make(&v[1], yo, ys);
    mmUIDim_Make(&v[2], zo, zs);
}

void
mmUIDimVec3MakeXY(
    struct mmUIDim                                 v[3],
    float                                          xo,
    float                                          xs,
    float                                          yo,
    float                                          ys)
{
    mmUIDim_Make(&v[0], xo, xs);
    mmUIDim_Make(&v[1], yo, ys);
    mmUIDim_Make(&v[2],  0,  0);
}

void
mmUIDimVec3Add(
    struct mmUIDim                                 r[3],
    const struct mmUIDim                           a[3],
    const struct mmUIDim                           b[3])
{
    mmUIDim_Add(&r[0], &a[0], &b[0]);
    mmUIDim_Add(&r[1], &a[1], &b[1]);
    mmUIDim_Add(&r[2], &a[2], &b[2]);
}

void
mmUIDimVec3Sub(
    struct mmUIDim                                 r[3],
    const struct mmUIDim                           a[3],
    const struct mmUIDim                           b[3])
{
    mmUIDim_Sub(&r[0], &a[0], &b[0]);
    mmUIDim_Sub(&r[1], &a[1], &b[1]);
    mmUIDim_Sub(&r[2], &a[2], &b[2]);
}

void
mmUIRect_Reset(
    struct mmUIRect*                               p)
{
    mmMemset(p, 0, sizeof(struct mmUIRect));
}

void
mmUIRect_Assign(
    struct mmUIRect*                               p,
    const struct mmUIRect*                         q)
{
    mmMemcpy(p, q, sizeof(struct mmUIRect));
}

void
mmUICube_Reset(
    struct mmUICube*                               p)
{
    mmMemset(p, 0, sizeof(struct mmUICube));
}

void
mmUICube_Assign(
    struct mmUICube*                               p,
    const struct mmUICube*                         q)
{
    mmMemcpy(p, q, sizeof(struct mmUICube));
}

void
mmUICubeDim3ToValue(
    float                                          cube[3],
    struct mmUIDim                                 dim[3],
    float                                          value[3])
{
    value[0] = dim[0].s * cube[0] + dim[0].o;
    value[1] = dim[1].s * cube[1] + dim[1].o;
    value[2] = dim[2].s * cube[2] + dim[2].o;
}

int
mmUICubeScalar_ContainsVec3(
    struct mmUICubeScalar*                         p,
    const float                                    q[3])
{
    return
        q[0] >= p->min[0] && q[0] <= p->max[0] &&
        q[1] >= p->min[1] && q[1] <= p->max[1] &&
        q[2] >= p->min[2] && q[2] <= p->max[2];
}

int
mmUICubeScalar_ContainsVec2(
    struct mmUICubeScalar*                         p,
    const float                                    q[2])
{
    return
        q[0] >= p->min[0] && q[0] <= p->max[0] &&
        q[1] >= p->min[1] && q[1] <= p->max[1];
}

int
mmUICubeScalar_Overlap(
    const struct mmUICubeScalar*                   p,
    const struct mmUICubeScalar*                   q)
{
    return
        p->min[0] <= q->max[0] &&
        q->min[0] <= p->max[0] &&
        p->min[1] <= q->max[1] &&
        q->min[1] <= p->max[1] &&
        p->min[2] <= q->max[2] &&
        q->min[2] <= p->max[2];
}

int
mmUICubeScalar_OverlapMax3(
    const struct mmUICubeScalar*                   p,
    const float                                    qmax[3])
{
    return
        p->min[0] <=   qmax[0] &&
                0 <= p->max[0] &&
        p->min[1] <=   qmax[1] &&
                0 <= p->max[1] &&
        p->min[2] <=   qmax[2] &&
                0 <= p->max[2];
}
