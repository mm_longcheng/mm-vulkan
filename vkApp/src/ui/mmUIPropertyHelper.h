/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIPropertyHelper_h__
#define __mmUIPropertyHelper_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmPropertySet.h"

#include "ui/mmUICube.h"

#include "core/mmPrefix.h"

/* ui type */

void
mmPropertyHelper_SetUIDim3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

void
mmPropertyHelper_GetUIDim3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

/* base type vector */

void
mmPropertyHelper_SetUInt8Vec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

void
mmPropertyHelper_GetUInt8Vec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

void
mmPropertyHelper_SetUInt8Vec3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

void
mmPropertyHelper_GetUInt8Vec3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

void
mmPropertyHelper_SetSIntVec4(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

void
mmPropertyHelper_GetSIntVec4(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

void
mmPropertyHelper_SetSizeVec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

void
mmPropertyHelper_GetSizeVec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

#include "core/mmSuffix.h"

#endif//__mmUIPropertyHelper_h__
