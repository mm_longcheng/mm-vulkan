/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewDirector_h__
#define __mmUIViewDirector_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"
#include "container/mmListVpt.h"

#include "ui/mmUIView.h"

struct mmUIViewContext;
struct mmUIViewLoader;

struct mmUIViewDirector
{
    struct mmRbtreeStringVpt hExclusivePool;
    struct mmListVpt hExclusiveUsed;
    struct mmListVpt hExclusiveIdle;

    struct mmRbtreeStringVpt hMultiplexPool;
    struct mmListVpt hMultiplexUsed;
    struct mmListVpt hMultiplexIdle;

    struct mmRbtreeStringVpt hLayerPool;

    // current window.
    struct mmUIView* pCurrentWindow;
    // root window.
    struct mmUIView* pRootWindow;
    // root safety.
    struct mmUIView* pRootSafety;
    
    // context handle
    struct mmUIViewContext* pViewContext;
    
    // view loader.
    struct mmUIViewLoader* pViewLoader;
};

void
mmUIViewDirector_Init(
    struct mmUIViewDirector*                       p);

void
mmUIViewDirector_Destroy(
    struct mmUIViewDirector*                       p);

void
mmUIViewDirector_SetViewContext(
    struct mmUIViewDirector* p,
    struct mmUIViewContext*                        pViewContext);

void
mmUIViewDirector_SetViewLoader(
    struct mmUIViewDirector*                       p,
    struct mmUIViewLoader*                         pViewLoader);

void
mmUIViewDirector_AssignmentViewContext(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w);

void
mmUIViewDirector_SetRootSafety(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               pRootSafety);

void
mmUIViewDirector_SetRootWindow(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               pRootWindow);

void
mmUIViewDirector_PrepareRootWindow(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

void
mmUIViewDirector_DiscardRootWindow(
    struct mmUIViewDirector*                       p);

void
mmUIViewDirector_Prepare(
    struct mmUIViewDirector*                       p,
    const char*                                    root,
    const char*                                    master);

void
mmUIViewDirector_Discard(
    struct mmUIViewDirector*                       p);

struct mmUIView*
mmUIViewDirector_SceneExclusiveBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_SceneExclusiveForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

void
mmUIViewDirector_SceneExclusiveEnterBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

void
mmUIViewDirector_SceneExclusiveEnterForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_SceneMultiplexForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_SceneMultiplexBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

void
mmUIViewDirector_SceneMultiplexTerminate(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_ScenePush(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_ScenePop(
    struct mmUIViewDirector*                       p);

struct mmUIView*
mmUIViewDirector_SceneGoto(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_SceneGet(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_SceneGetCurrent(
    struct mmUIViewDirector*                       p);

void
mmUIViewDirector_SceneSetCurrent(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w);

void
mmUIViewDirector_ScenePopUntil(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

void
mmUIViewDirector_SceneClear(
    struct mmUIViewDirector*                       p);

struct mmUIView*
mmUIViewDirector_LayerAdd(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_LayerRmv(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

struct mmUIView*
mmUIViewDirector_LayerGet(
    struct mmUIViewDirector*                       p,
    const char*                                    name);

void
mmUIViewDirector_LayerClear(
    struct mmUIViewDirector* p);

struct mmUIView*
mmUIViewDirector_ProduceLayerContext(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name);

void
mmUIViewDirector_RecycleLayerContext(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w);

#endif//__mmUIViewDirector_h__

