/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIResponder_h__
#define __mmUIResponder_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmUIResponderKeypad
{
    // typedef void(*OnKeypadPressed)(void* p, struct mmEventKeypad* content);
    void* OnKeypadPressed;
    // typedef void(*OnKeypadRelease)(void* p, struct mmEventKeypad* content);
    void* OnKeypadRelease;
    // typedef void(*OnKeypadStatus)(void* p, struct mmEventKeypadStatus* content);
    void* OnKeypadStatus;
};

struct mmUIResponderCursor
{
    // typedef void(*OnCursorBegan)(void* p, struct mmEventCursor* content);
    void* OnCursorBegan;
    // typedef void(*OnCursorMoved)(void* p, struct mmEventCursor* content);
    void* OnCursorMoved;
    // typedef void(*OnCursorEnded)(void* p, struct mmEventCursor* content);
    void* OnCursorEnded;
    // typedef void(*OnCursorBreak)(void* p, struct mmEventCursor* content);
    void* OnCursorBreak;
    // typedef void(*OnCursorWheel)(void* p, struct mmEventCursor* content);
    void* OnCursorWheel;
};

struct mmUIResponderTouchs
{
    // typedef void(*OnTouchsBegan)(void* p, struct mmEventTouchs* content);
    void* OnTouchsBegan;
    // typedef void(*OnTouchsMoved)(void* p, struct mmEventTouchs* content);
    void* OnTouchsMoved;
    // typedef void(*OnTouchsEnded)(void* p, struct mmEventTouchs* content);
    void* OnTouchsEnded;
    // typedef void(*OnTouchsBreak)(void* p, struct mmEventTouchs* content);
    void* OnTouchsBreak;
};

struct mmUIResponderEditor
{
    // typedef void(*OnGetText)(void* p, struct mmEventEditText* content);
    void* OnGetText;
    // typedef void(*OnSetText)(void* p, struct mmEventEditText* content);
    void* OnSetText;
    // typedef void(*OnGetTextEditRect)(void* p, double rect[4]);
    void* OnGetTextEditRect;
    // typedef void(*OnInsertTextUtf16)(void* p, struct mmEventEditString* content);
    void* OnInsertTextUtf16;
    // typedef void(*OnReplaceTextUtf16)(void* p, struct mmEventEditString* content);
    void* OnReplaceTextUtf16;

    // typedef void(*OnInjectCopy)(void* p);
    void* OnInjectCopy;
    // typedef void(*OnInjectCut)(void* p);
    void* OnInjectCut;
    // typedef void(*OnInjectPaste)(void* p);
    void* OnInjectPaste;

    // typedef void(*OnFocusCapture)(void* p);
    void* OnFocusCapture;
    // typedef void(*OnFocusRelease)(void* p);
    void* OnFocusRelease;
};

#include "core/mmSuffix.h"

#endif//__mmUIResponder_h__
