/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIEventDriver_h__
#define __mmUIEventDriver_h__

#include "core/mmCore.h"

#include "container/mmListVpt.h"

#include "core/mmPrefix.h"

struct mmEventKeypad;
struct mmEventCursor;
struct mmEventTouchs;
struct mmEventEditText;
struct mmEventEditString;
struct mmEventKeypadStatus;

struct mmUIViewContext;

struct mmUIEventDriver
{
    struct mmListVpt list;
};

void
mmUIEventDriver_Init(
    struct mmUIEventDriver*                        p);

void
mmUIEventDriver_Destroy(
    struct mmUIEventDriver*                        p);

void
mmUIEventDriver_Add(
    struct mmUIEventDriver*                        p,
    struct mmUIViewContext*                        pViewContext);

void
mmUIEventDriver_Rmv(
    struct mmUIEventDriver*                        p,
    struct mmUIViewContext*                        pViewContext);

void
mmUIEventDriver_OnHandlerArgs0(
    struct mmUIEventDriver*                        p,
    void*                                          func);

void
mmUIEventDriver_OnHandlerArgs1(
    struct mmUIEventDriver*                        p,
    void*                                          func,
    void*                                          content);

void
mmUIEventDriver_OnKeypadPressed(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypad*                          content);

void
mmUIEventDriver_OnKeypadRelease(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypad*                          content);

void
mmUIEventDriver_OnKeypadStatus(
    struct mmUIEventDriver*                        p,
    struct mmEventKeypadStatus*                    content);

void
mmUIEventDriver_OnCursorBegan(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content);

void
mmUIEventDriver_OnCursorMoved(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content);

void
mmUIEventDriver_OnCursorEnded(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content);

void
mmUIEventDriver_OnCursorBreak(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content);

void
mmUIEventDriver_OnCursorWheel(
    struct mmUIEventDriver*                        p,
    struct mmEventCursor*                          content);

void
mmUIEventDriver_OnTouchsBegan(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content);

void
mmUIEventDriver_OnTouchsMoved(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content);

void
mmUIEventDriver_OnTouchsEnded(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content);

void
mmUIEventDriver_OnTouchsBreak(
    struct mmUIEventDriver*                        p,
    struct mmEventTouchs*                          content);

void
mmUIEventDriver_OnGetText(
    struct mmUIEventDriver*                        p,
    struct mmEventEditText*                        content);

void
mmUIEventDriver_OnSetText(
    struct mmUIEventDriver*                        p,
    struct mmEventEditText*                        content);

void
mmUIEventDriver_OnGetTextEditRect(
    struct mmUIEventDriver*                        p,
    double                                         rect[4]);

void
mmUIEventDriver_OnInsertTextUtf16(
    struct mmUIEventDriver*                        p,
    struct mmEventEditString*                      content);

void
mmUIEventDriver_OnReplaceTextUtf16(
    struct mmUIEventDriver*                        p,
    struct mmEventEditString*                      content);

void
mmUIEventDriver_OnInjectCopy(
    struct mmUIEventDriver*                        p);

void
mmUIEventDriver_OnInjectCut(
    struct mmUIEventDriver*                        p);

void
mmUIEventDriver_OnInjectPaste(
    struct mmUIEventDriver*                        p);

#include "core/mmSuffix.h"

#endif//__mmUIEventDriver_h__
