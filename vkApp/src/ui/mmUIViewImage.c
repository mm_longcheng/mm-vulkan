/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewImage.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIPropertyHelper.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmPropertyHelper.h"

#include "container/mmVectorValue.h"

#include "vk/mmVKDescriptorLayout.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKCommandBuffer.h"

void
mmUIViewImage_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    mmUIView_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewImage, view));
    mmUIDrawable_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewImage, drawable));
    mmUIImage_DefinePropertys(propertys, offset + mmOffsetof(struct mmUIViewImage, image));
}

void
mmUIViewImage_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmUIImage_RemovePropertys(propertys);
    mmUIDrawable_RemovePropertys(propertys);
    mmUIView_RemovePropertys(propertys);
}

const struct mmUIWidgetOperable mmUIWidgetOperableViewImage =
{
    &mmUIViewImage_OnPrepare,
    &mmUIViewImage_OnDiscard,
};

const struct mmUIWidgetVariable mmUIWidgetVariableViewImage =
{
    &mmUIViewImage_OnUpdate,
    &mmUIViewImage_OnInvalidate,
    &mmUIViewImage_OnSized,
    &mmUIViewImage_OnChanged,
};

const struct mmUIWidgetDrawable mmUIWidgetDrawableViewImage =
{
    &mmUIViewImage_OnDescriptorSetProduce,
    &mmUIViewImage_OnDescriptorSetRecycle,
    &mmUIViewImage_OnDescriptorSetUpdate,
    &mmUIViewImage_OnUpdatePushConstants,
    &mmUIViewImage_OnUpdatePipeline,
    &mmUIViewImage_OnRecord,
};

const struct mmMetaAllocator mmUIMetaAllocatorViewImage =
{
    "mmUIViewImage",
    sizeof(struct mmUIViewImage),
    &mmUIViewImage_Produce,
    &mmUIViewImage_Recycle,
};

const struct mmUIMetadata mmUIMetadataViewImage =
{
    &mmUIMetaAllocatorViewImage,
    /*- Widget -*/
    &mmUIWidgetOperableViewImage,
    &mmUIWidgetVariableViewImage,
    &mmUIWidgetDrawableViewImage,
    /*- Responder -*/
    NULL,
    NULL,
    NULL,
    NULL,
};

void
mmUIViewImage_Init(
    struct mmUIViewImage*                          p)
{
    mmUIView_Init(&p->view);
    mmUIDrawable_Init(&p->drawable);
    mmUIImage_Init(&p->image);
}

void
mmUIViewImage_Destroy(
    struct mmUIViewImage*                          p)
{
    mmUIImage_Destroy(&p->image);
    mmUIDrawable_Destroy(&p->drawable);
    mmUIView_Destroy(&p->view);
}

void
mmUIViewImage_Produce(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_Init(p);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewImage;
    // bind object for this.
    mmPropertySet_SetObject(&p->view.propertys, p);
    
    mmUIViewImage_DefinePropertys(&p->view.propertys, 0);
}

void
mmUIViewImage_Recycle(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_RemovePropertys(&p->view.propertys);
    
    // bind object metadata.
    p->view.metadata = &mmUIMetadataViewImage;
    
    mmUIViewImage_Destroy(p);
}

VkResult
mmUIViewImage_PrepareFromImageInfo(
    struct mmUIViewImage*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmVKImageInfo*                          pImageInfo,
    const int                                      frame[4],
    int                                            rotated)
{
    return mmUIImage_PrepareFromImageInfo(
        &p->image,
        &p->view, 
        &p->drawable, 
        pAssets, 
        pImageInfo, 
        frame, 
        rotated);
}

VkResult
mmUIViewImage_PrepareFromSheetName(
    struct mmUIViewImage*                          p)
{
    return mmUIImage_PrepareFromSheetName(
        &p->image,
        &p->view, 
        &p->drawable);
}

VkResult
mmUIViewImage_PrepareFromImagePath(
    struct mmUIViewImage*                          p)
{
    return mmUIImage_PrepareFromImagePath(
        &p->image,
        &p->view, 
        &p->drawable);
}

void
mmUIViewImage_UpdateImage(
    struct mmUIViewImage*                          p)
{
    // Note: we must wait idle before update content.
    mmUIViewContext_DeviceWaitIdle(p->view.context);
    mmUIImage_UpdateFrame(&p->image, &p->view, &p->drawable);
    mmUIViewImage_OnDescriptorSetUpdate(p);
}

void
mmUIViewImage_UpdateSize(
    struct mmUIViewImage*                          p)
{
    mmUIImage_UpdateSize(&p->image, &p->view, &p->drawable);
}

void
mmUIViewImage_OnPrepare(
    struct mmUIViewImage*                          p)
{
    mmUIImage_Prepare(
        &p->image, 
        &p->view, 
        &p->drawable);

    mmUIViewImage_OnDescriptorSetProduce(p);
    mmUIViewImage_OnDescriptorSetUpdate(p);
    mmUIViewImage_OnUpdatePushConstants(p);
}

void
mmUIViewImage_OnDiscard(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_OnDescriptorSetRecycle(p);

    mmUIImage_Discard(
        &p->image, 
        &p->view, 
        &p->drawable);
}

void
mmUIViewImage_OnUpdate(
    struct mmUIViewImage*                          p,
    struct mmEventUpdate*                          content)
{
    // do nothing here, interface api.
}

void
mmUIViewImage_OnInvalidate(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_OnUpdatePushConstants(p);
}

void
mmUIViewImage_OnSized(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_UpdateSize(p);
    mmUIViewImage_OnUpdatePushConstants(p);
}

void
mmUIViewImage_OnChanged(
    struct mmUIViewImage*                          p)
{
    mmUIViewImage_UpdateImage(p);
    mmUIViewImage_OnUpdatePushConstants(p);
}

void
mmUIViewImage_OnDescriptorSetProduce(
    struct mmUIViewImage*                          p)
{
    mmUIDrawable_DescriptorSetProduce(&p->drawable);
}

void
mmUIViewImage_OnDescriptorSetRecycle(
    struct mmUIViewImage*                          p)
{
    mmUIDrawable_DescriptorSetRecycle(&p->drawable);
}

void
mmUIViewImage_OnDescriptorSetUpdate(
    struct mmUIViewImage*                          p)
{
    mmUIDrawable_DescriptorSetUpdate(
        &p->drawable,
        &p->view,
        p->image.pImageInfo,
        p->image.pSamplerInfo);
}

void
mmUIViewImage_OnUpdatePushConstants(
    struct mmUIViewImage*                          p)
{
    mmUIDrawable_UpdatePushConstants(&p->drawable, &p->view);
}

void
mmUIViewImage_OnUpdatePipeline(
    struct mmUIViewImage*                          p)
{
    mmUIDrawable_UpdatePool(&p->drawable, &p->view);
    mmUIDrawable_UpdatePipeline(&p->drawable, &p->view);
}

void 
mmUIViewImage_OnRecord(
    struct mmUIViewImage*                          p, 
    struct mmVKCommandBuffer*                      cmd)
{
    mmUIDrawable_OnRecord(&p->drawable, cmd->cmdBuffer);
    mmUIView_OnRecordChildren(&p->view, cmd);
}
