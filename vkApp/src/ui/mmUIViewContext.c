/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewContext.h"

#include "ui/mmUIView.h"
#include "ui/mmUIEventArgs.h"
#include "ui/mmUISchemeLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmVector2.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix4x4.h"

#include "nwsi/mmEventSurface.h"
#include "nwsi/mmISurface.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKUploader.h"

const char* mmUIViewContext_EventCubeSizeChanged = "EventCubeSizeChanged";
const char* mmUIViewContext_EventSafeRectChanged = "EventSafeRectChanged";

const char* mmUIViewContext_EventEnterBackground = "EventEnterBackground";
const char* mmUIViewContext_EventEnterForeground = "EventEnterForeground";

const char* mmUIViewContext_EventRecordBeforePass = "EventRecordBeforePass";
const char* mmUIViewContext_EventRecordBehindPass = "EventRecordBehindPass";
const char* mmUIViewContext_EventRecordBeforeView = "EventRecordBeforeView";
const char* mmUIViewContext_EventRecordBasicsView = "EventRecordBasicsView";
const char* mmUIViewContext_EventRecordBehindView = "EventRecordBehindView";

void
mmUIViewContext_Init(
    struct mmUIViewContext*                        p)
{
    mmEventSet_Init(&p->events);
    mmVKCamera_Init(&p->camera);
    mmVKBuffer_Init(&p->ubo);
    mmRbtreeStringVpt_Init(&p->modules);
    mmString_Init(&p->scheme);
    mmMemset(&p->ubovs, 0, sizeof(p->ubovs));
    mmVec3AssignValue(p->cube, 0.0f);
    mmVec4AssignValue(p->safe, 0.0f);
    p->density = 1.0f;
    p->tolerance = 12.0f;
    p->distance = 0.0f;
    mmVec2Make(p->point, 0.0f, 0.0f);
    p->root = NULL;
    p->capture = NULL;
    p->active = NULL;
    p->contain = NULL;
    p->graphics = NULL;
    p->assets = NULL;
    p->swapchain = NULL;
    p->context = NULL;
    p->surface = NULL;
    p->keypad = NULL;
    p->clipboard = NULL;
    p->sloader = NULL;
    p->vloader = NULL;
    p->director = NULL;
}

void
mmUIViewContext_Destroy(
    struct mmUIViewContext*                        p)
{
    p->director = NULL;
    p->vloader = NULL;
    p->sloader = NULL;
    p->clipboard = NULL;
    p->keypad = NULL;
    p->surface = NULL;
    p->context = NULL;
    p->swapchain = NULL;
    p->assets = NULL;
    p->graphics = NULL;
    p->contain = NULL;
    p->active = NULL;
    p->capture = NULL;
    p->root = NULL;
    mmVec2Make(p->point, 0.0f, 0.0f);
    p->distance = 0.0f;
    p->tolerance = 12.0f;
    p->density = 1.0f;
    mmVec4AssignValue(p->safe, 0.0f);
    mmVec3AssignValue(p->cube, 0.0f);
    mmMemset(&p->ubovs, 0, sizeof(p->ubovs));
    mmString_Destroy(&p->scheme);
    mmRbtreeStringVpt_Destroy(&p->modules);
    mmVKBuffer_Destroy(&p->ubo);
    mmVKCamera_Destroy(&p->camera);
    mmEventSet_Destroy(&p->events);
}

void
mmUIViewContext_SetDisplayDensity(
    struct mmUIViewContext*                        p,
    float                                          hDisplayDensity)
{
    p->density = hDisplayDensity;
}

void
mmUIViewContext_SetCubeSize(
    struct mmUIViewContext*                        p,
    float                                          cube[3])
{
    struct mmEventUIViewContextArgs hArgs;

    mmVec3Assign(p->cube, cube);

    mmEventUIViewContextArgs_Reset(&hArgs);
    hArgs.pViewContext = p;

    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventCubeSizeChanged, &hArgs);
}

void
mmUIViewContext_SetSafeRect(
    struct mmUIViewContext*                        p,
    float                                          safe[4])
{
    struct mmEventUIViewContextArgs hArgs;

    mmVec4Assign(p->safe, safe);

    mmEventUIViewContextArgs_Reset(&hArgs);
    hArgs.pViewContext = p;

    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventSafeRectChanged, &hArgs);
}

void
mmUIViewContext_SetGraphicsFactory(
    struct mmUIViewContext*                        p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->graphics = pGraphicsFactory;
}

void
mmUIViewContext_SetAssets(
    struct mmUIViewContext*                        p,
    struct mmVKAssets*                             pAssets)
{
    p->assets = pAssets;
}

void
mmUIViewContext_SetSwapchain(
    struct mmUIViewContext*                        p,
    struct mmVKSwapchain*                          pSwapchain)
{
    p->swapchain = pSwapchain;
}

void
mmUIViewContext_SetContext(
    struct mmUIViewContext*                        p,
    struct mmIContext*                             context)
{
    p->context = context;
}

void
mmUIViewContext_SetSurface(
    struct mmUIViewContext*                        p,
    struct mmISurface*                             surface)
{
    p->surface = surface;
}

void
mmUIViewContext_SetSurfaceKeypad(
    struct mmUIViewContext*                        p,
    struct mmISurfaceKeypad*                       keypad)
{
    p->keypad = keypad;
}

void
mmUIViewContext_SetClipboard(
    struct mmUIViewContext*                        p,
    struct mmClipboard*                            clipboard)
{
    p->clipboard = clipboard;
}

void
mmUIViewContext_SetSchemeLoader(
    struct mmUIViewContext*                        p,
    struct mmUISchemeLoader*                       sloader)
{
    p->sloader = sloader;
}

void
mmUIViewContext_SetViewLoader(
    struct mmUIViewContext*                        p,
    struct mmUIViewLoader*                         vloader)
{
    p->vloader = vloader;
}

void
mmUIViewContext_SetViewDirector(
    struct mmUIViewContext*                        p,
    struct mmUIViewDirector*                       director)
{
    p->director = director;
}

void
mmUIViewContext_SetRoot(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               root)
{
    p->root = root;
}

void
mmUIViewContext_SetActive(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               active)
{
    p->active = active;
}

void
mmUIViewContext_SetContain(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               contain)
{
    p->contain = contain;
}

void
mmUIViewContext_SetScheme(
    struct mmUIViewContext*                        p,
    const char*                                    pScheme)
{
    mmString_Assigns(&p->scheme, pScheme);
}

void
mmUIViewContext_GetSchemeColor(
    const struct mmUIViewContext*                  p,
    const struct mmString*                         pName,
    float                                          hColor[4])
{
    const char* cSchemeName;
    struct mmUIScheme* scheme;
    cSchemeName = mmString_CStr(&p->scheme);
    scheme = mmUISchemeLoader_GetFromName(p->sloader, cSchemeName);
    if (NULL != scheme)
    {
        mmUIScheme_GetColor(scheme, pName, hColor);
    }
    else
    {
        mmVec4Make(hColor, 0.0f, 0.0f, 0.0f, 1.0f);
    }
}

const struct mmUIParameterStyle*
mmUIViewContext_GetSchemeStyle(
    const struct mmUIViewContext*                  p,
    const struct mmString*                         pName)
{
    const char* cSchemeName;
    struct mmUIScheme* scheme;
    cSchemeName = mmString_CStr(&p->scheme);
    scheme = mmUISchemeLoader_GetFromName(p->sloader, cSchemeName);
    if (NULL != scheme)
    {
        return mmUIScheme_GetStyle(scheme, pName);
    }
    else
    {
        return NULL;
    }
}

void
mmUIViewContext_AddModule(
    struct mmUIViewContext*                        p,
    const char*                                    name,
    void*                                          module)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    mmRbtreeStringVpt_Set(&p->modules, &hWeakKey, module);
}

void
mmUIViewContext_RmvModule(
    struct mmUIViewContext*                        p,
    const char*                                    name)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    mmRbtreeStringVpt_Rmv(&p->modules, &hWeakKey);
}

void*
mmUIViewContext_GetModule(
    const struct mmUIViewContext*                  p,
    const char*                                    name)
{
    struct mmString hWeakKey;
    struct mmRbtreeStringVptIterator* it;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->modules, &hWeakKey);
    return (NULL != it) ? it->v : NULL;
}

void
mmUIViewContext_ClearModule(
    struct mmUIViewContext*                        p)
{
    mmRbtreeStringVpt_Clear(&p->modules);
}

void
mmUIViewContext_OrthogonalVK(
    struct mmUIViewContext*                        p,
    float l, float r,
    float b, float t,
    float n, float f)
{
    mmVKCamera_OrthogonalVK(&p->camera, l, r, b, t, n, f);
}

VkResult
mmUIViewContext_PrepareUBOBuffer(
    struct mmUIViewContext*                        p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = NULL;

        struct mmVKUploader* pUploader = NULL;

        VkBufferCreateInfo hBufferInfo;

        struct mmVKAssets* pAssets = p->assets;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = sizeof(struct mmUIDrawableUBOVS);
        hBufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateGPUOnlyBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->ubo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmUIViewContext_DiscardUBOBuffer(
    struct mmUIViewContext*                        p)
{
    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmVKAssets* pAssets = p->assets;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKUploader_DeleteBuffer(pUploader, &p->ubo);
    } while (0);
}

VkResult
mmUIViewContext_UpdateUBOBuffer(
    struct mmUIViewContext*                        p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = NULL;

        struct mmVKUploader* pUploader = NULL;

        float viewProjection[4][4];

        struct mmVKAssets* pAssets = p->assets;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == p->ubo.buffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " ubo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmMat4x4Mul(viewProjection, p->camera.projection, p->camera.view);
        mmUniformMat4Assign(p->ubovs.viewProjection, viewProjection);

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)&p->ubovs,
            (size_t)0,
            (size_t)sizeof(struct mmUIDrawableUBOVS),
            &p->ubo,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmUIViewContext_Prepare(
    struct mmUIViewContext*                        p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmUIViewContext_PrepareUBOBuffer(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIViewContext_PrepareUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmUIViewContext_UpdateUBOBuffer(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmUIViewContext_UpdateUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmUIViewContext_Discard(
    struct mmUIViewContext*                        p)
{
    mmUIViewContext_DiscardUBOBuffer(p);
}

void
mmUIViewContext_Update(
    struct mmUIViewContext*                        p,
    struct mmEventUpdate*                          content)
{
    if (NULL != p->root)
    {
        mmUIView_Update(p->root, content);
    }
}

void
mmUIViewContext_Invalidate(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        mmUIView_Invalidate(p->root);
    }
}

void
mmUIViewContext_Sized(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        mmUIView_Sized(p->root);
    }
}

void
mmUIViewContext_Changed(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        mmUIView_Changed(p->root);
    }
}

void
mmUIViewContext_RecordBeforePass(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmEventUIViewCommandArgs hArgs;
    
    mmEventUIViewCommandArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    hArgs.pCmd = cmd;
    
    mmEventSet_FireEvent(
        &p->events,
        mmUIViewContext_EventRecordBeforePass,
        &hArgs);
}

void
mmUIViewContext_RecordBehindPass(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmEventUIViewCommandArgs hArgs;
    
    mmEventUIViewCommandArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    hArgs.pCmd = cmd;
    
    mmEventSet_FireEvent(
        &p->events,
        mmUIViewContext_EventRecordBehindPass,
        &hArgs);
}

void
mmUIViewContext_RecordBeforeView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmEventUIViewCommandArgs hArgs;
    
    mmEventUIViewCommandArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    hArgs.pCmd = cmd;
    
    mmEventSet_FireEvent(
        &p->events,
        mmUIViewContext_EventRecordBeforeView,
        &hArgs);
}

void
mmUIViewContext_RecordBasicsView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmEventUIViewCommandArgs hArgs;
    
    mmEventUIViewCommandArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    hArgs.pCmd = cmd;
    
    mmEventSet_FireEvent(
        &p->events,
        mmUIViewContext_EventRecordBasicsView,
        &hArgs);
    
    mmUIViewContext_OnRecordCommand(p, cmd);
}

void
mmUIViewContext_RecordBehindView(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmEventUIViewCommandArgs hArgs;
    
    mmEventUIViewCommandArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    hArgs.pCmd = cmd;
    
    mmEventSet_FireEvent(
        &p->events,
        mmUIViewContext_EventRecordBehindView,
        &hArgs);
}

void
mmUIViewContext_OnRecordCommand(
    struct mmUIViewContext*                        p,
    struct mmVKCommandBuffer*                      cmd)
{
    if (NULL != p->root)
    {
        mmUIView_OnRecordCommand(p->root, cmd);
    }
}

void
mmUIViewContext_DeviceWaitIdle(
    struct mmUIViewContext*                        p)
{
    do
    {
        struct mmVKAssets* assets;
        struct mmVKDevice* pDevice;

        if (NULL == p)
        {
            break;
        }

        assets = p->assets;
        if (NULL == assets)
        {
            break;
        }

        pDevice = assets->pDevice;
        if (NULL == pDevice)
        {
            break;
        }

        mmVKDevice_WaitIdle(pDevice);
    } while (0);
}

void
mmUIViewContext_MarkCacheInvalidWhole(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        mmUIView_MarkCacheInvalidWhole(p->root);
    }
}

void
mmUIViewContext_HandleSized(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        mmUIView_HandleSized(p->root);
    }
}

void
mmUIViewContext_Resize(
    struct mmUIViewContext*                        p,
    float                                          cube[3],
    float                                          safe[4])
{
    struct mmEventUIViewContextArgs hArgs;

    mmVec3Assign(p->cube, cube);
    mmVec4Assign(p->safe, safe);

    mmEventUIViewContextArgs_Reset(&hArgs);
    hArgs.pViewContext = p;

    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventCubeSizeChanged, &hArgs);
    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventSafeRectChanged, &hArgs);

    mmUIViewContext_MarkCacheInvalidWhole(p);
    mmUIViewContext_HandleSized(p);
    mmUIViewContext_OnInvalidate(p);

    mmUIViewContext_OrthogonalVK(p, 0.0f, cube[0], cube[1], 0.0f, -10, 10);
    mmUIViewContext_UpdateUBOBuffer(p);
}

void
mmUIViewContext_EnterBackground(
    struct mmUIViewContext*                        p)
{
    struct mmEventUIViewContextArgs hArgs;
    mmEventUIViewContextArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventEnterBackground, &hArgs);
}

void
mmUIViewContext_EnterForeground(
    struct mmUIViewContext*                        p)
{
    struct mmEventUIViewContextArgs hArgs;
    mmEventUIViewContextArgs_Reset(&hArgs);
    hArgs.pViewContext = p;
    mmEventSet_FireEvent(&p->events, mmUIViewContext_EventEnterForeground, &hArgs);
}

void
mmUIViewContext_OnUpdateClickedDistance(
    struct mmUIViewContext*                        p,
    const float                                    point[2])
{
    float dt[2];
    float sl;
    mmVec2Sub(dt, point, p->point);
    sl = mmVec2SquaredLength(dt);
    p->distance = mmMathMax(p->distance, sl);
}

void
mmUIViewContext_OnEventClickedArgs(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               v,
    struct mmEventCursorArgs*                      args)
{
    if (v == p->capture && p->distance <= p->tolerance * p->tolerance)
    {
        struct mmEventUIClickedArgs hArgs;

        mmEventUIClickedArgs_Reset(&hArgs);
        hArgs.content = args->content;
        hArgs.pView = v;
        mmVec2Assign(hArgs.point, p->point);

        mmUIView_OnClickedArgs(v, &hArgs);
    }
}

void
mmUIViewContext_OnEventContainArgs(
    struct mmUIViewContext*                        p,
    struct mmUIView*                               v,
    struct mmEventCursorArgs*                      args)
{
    if (v != p->contain)
    {
        struct mmEventUIFocusArgs hArgs;
        const struct mmUIView* ancestor = NULL;
        const void* func = NULL;
        struct mmUIView* o = p->contain;

        mmEventUIFocusArgs_Reset(&hArgs);
        hArgs.content = args->content;

        if (NULL != o)
        {
            hArgs.pView = o;
            hArgs.hSuper.handled = 0;
            mmUIView_OnLeaveSurfaceArgs(o, &hArgs);
        }

        if (NULL != v)
        {
            hArgs.pView = v;
            hArgs.hSuper.handled = 0;
            mmUIView_OnEnterSurfaceArgs(v, &hArgs);
        }

        hArgs.pView = v;
        ancestor = mmUIView_GetAncestor(o, v);
        func = &mmUIView_OnLeaveAreaArgs;
        mmUIView_OnEventContainArgs(o, ancestor, func, (struct mmEventArgs*)&hArgs);
        func = &mmUIView_OnEnterAreaArgs;
        mmUIView_OnEventContainArgs(v, ancestor, func, (struct mmEventArgs*)&hArgs);

        p->contain = v;
    }
}

void
mmUIViewContext_OnKeypadProvider(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    if (NULL != p->keypad && NULL != p->surface && 0 == content->handle)
    {
        if (mmUIView_GetIsResponderEditor(p->active))
        {
            (*(p->keypad->OnKeypadStatusShow))(p->surface);
        }
        else
        {
            (*(p->keypad->OnKeypadStatusHide))(p->surface);
        }
    }
}

void
mmUIViewContext_OnUpdate(
    struct mmUIViewContext*                        p,
    struct mmEventUpdate*                          content)
{
    if (NULL != p->root)
    {
        struct mmEventUpdateArgs hArgs;

        mmEventUpdateArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnUpdateArgs(p->root, &hArgs);
    }
}

void
mmUIViewContext_OnInvalidate(
    struct mmUIViewContext*                        p)
{
    if (NULL != p->root)
    {
        struct mmEventArgs hArgs;

        mmEventArgs_Reset(&hArgs);

        mmUIView_OnInvalidateArgs(p->root, &hArgs);
    }
}

void
mmUIViewContext_OnKeypadPressed(
    struct mmUIViewContext*                        p,
    struct mmEventKeypad*                          content)
{
    if (NULL != p->active)
    {
        struct mmEventKeypadArgs hArgs;

        mmEventKeypadArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnKeypadPressedArgs(p->active, &hArgs);
        
        mmUIView_OnKeypadPressed(p->active, content);
    }
}

void
mmUIViewContext_OnKeypadRelease(
    struct mmUIViewContext*                        p,
    struct mmEventKeypad*                          content)
{
    if (NULL != p->active)
    {
        struct mmEventKeypadArgs hArgs;

        mmEventKeypadArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnKeypadReleaseArgs(p->active, &hArgs);
        
        mmUIView_OnKeypadRelease(p->active, content);
    }
}

void
mmUIViewContext_OnKeypadStatus(
    struct mmUIViewContext*                        p,
    struct mmEventKeypadStatus*                    content)
{
    if (NULL != p->active)
    {
        struct mmEventKeypadStatusArgs hArgs;

        mmEventKeypadStatusArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnKeypadStatusArgs(p->active, &hArgs);
        
        mmUIView_OnKeypadStatus(p->active, content);
    }
}

void
mmUIViewContext_OnCursorBegan(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventCursorArgs hArgs;
    
    struct mmUIView* a = p->active;
    const struct mmUIView* ancestor = NULL;
    float point[2] = { (float)content->abs_x, (float)content->abs_y };
    struct mmUIView* v = mmUIView_GetHitView(p->root, point);

    p->active = v;
    p->capture = v;

    ancestor = mmUIView_GetAncestor(a, v);
    mmUIView_SetBranchActive(a, ancestor, 0);
    mmUIView_SetBranchActive(v, ancestor, 1);
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    
    if (NULL != v)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorBeganArgs(v, &hArgs);
        
        mmUIView_OnCursorBegan(v, content);
    }
    
    if (NULL != p->active && v != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorBeganArgs(p->active, &hArgs);
        
        mmUIView_OnCursorBegan(p->active, content);
    }

    if (a != v)
    {
        mmUIView_OnFocusRelease(a);

        mmUIView_OnFocusCapture(v);
    }

    mmVec2Assign(p->point, point);
    p->distance = 0.0f;
}

void
mmUIViewContext_OnCursorMoved(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventCursorArgs hArgs;
    
    struct mmUIView* c = p->contain;
    float point[2] = { (float)content->abs_x, (float)content->abs_y };
    struct mmUIView* v = mmUIView_GetHitView(p->root, point);

    mmUIViewContext_OnUpdateClickedDistance(p, point);

    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    
    // Contain check event.
    hArgs.hSuper.handled = 0;
    mmUIViewContext_OnEventContainArgs(p, v, &hArgs);
    
    if (NULL != v)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorMovedArgs(v, &hArgs);
        
        mmUIView_OnCursorMoved(v, content);
    }

    if (NULL != p->active && v != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorMovedArgs(p->active, &hArgs);
        
        mmUIView_OnCursorMoved(p->active, content);
    }

    if (p->contain != c && NULL != c && c != v && c != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorMovedArgs(c, &hArgs);
        
        mmUIView_OnCursorMoved(c, content);
    }
}

void
mmUIViewContext_OnCursorEnded(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventCursorArgs hArgs;
    
    float point[2] = { (float)content->abs_x, (float)content->abs_y };
    struct mmUIView* v = mmUIView_GetHitView(p->root, point);
    
    mmUIViewContext_OnUpdateClickedDistance(p, point);

    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    
    if (NULL != v)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorEndedArgs(v, &hArgs);
        
        mmUIView_OnCursorEnded(v, content);

        hArgs.hSuper.handled = 0;
        mmUIViewContext_OnEventClickedArgs(p, v, &hArgs);
    }

    if (NULL != p->active && v != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorEndedArgs(p->active, &hArgs);
        
        mmUIView_OnCursorEnded(p->active, content);
    }

    // handle keypad pop up or put away.
    mmUIViewContext_OnKeypadProvider(p, content);

    p->capture = NULL;
    mmVec2Make(p->point, 0.0f, 0.0f);
}

void
mmUIViewContext_OnCursorBreak(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventCursorArgs hArgs;
    
    float point[2] = { (float)content->abs_x, (float)content->abs_y };
    struct mmUIView* v = mmUIView_GetHitView(p->root, point);
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    
    if (NULL != v)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorBreakArgs(v, &hArgs);
        
        mmUIView_OnCursorBreak(v, content);
    }
    
    if (NULL != p->active && v != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorBreakArgs(p->active, &hArgs);
        
        mmUIView_OnCursorBreak(p->active, content);
    }

    p->capture = NULL;
    mmVec2Make(p->point, 0.0f, 0.0f);
}

void
mmUIViewContext_OnCursorWheel(
    struct mmUIViewContext*                        p,
    struct mmEventCursor*                          content)
{
    struct mmEventCursorArgs hArgs;
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    
    if (NULL != p->capture)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorWheelArgs(p->capture, &hArgs);
        
        mmUIView_OnCursorWheel(p->capture, content);
    }
    
    if (NULL != p->active && p->capture != p->active)
    {
        hArgs.hSuper.handled = 0;
        mmUIView_OnCursorWheelArgs(p->active, &hArgs);
        
        mmUIView_OnCursorWheel(p->active, content);
    }
}

void
mmUIViewContext_OnTouchsBegan(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content)
{
    if (NULL != p->root)
    {
        struct mmEventTouchsArgs hArgs;

        mmEventTouchsArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnTouchsBeganArgs(p->root, &hArgs);
    }

    mmUIView_OnTouchsBegan(p->active, content);
}

void
mmUIViewContext_OnTouchsMoved(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content)
{
    if (NULL != p->root)
    {
        struct mmEventTouchsArgs hArgs;

        mmEventTouchsArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnTouchsMovedArgs(p->root, &hArgs);
    }

    mmUIView_OnTouchsMoved(p->active, content);
}

void
mmUIViewContext_OnTouchsEnded(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content)
{
    if (NULL != p->root)
    {
        struct mmEventTouchsArgs hArgs;

        mmEventTouchsArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnTouchsEndedArgs(p->root, &hArgs);
    }


    mmUIView_OnTouchsEnded(p->active, content);
}

void
mmUIViewContext_OnTouchsBreak(
    struct mmUIViewContext*                        p,
    struct mmEventTouchs*                          content)
{
    if (NULL != p->root)
    {
        struct mmEventTouchsArgs hArgs;

        mmEventTouchsArgs_Reset(&hArgs);
        hArgs.content = content;

        mmUIView_OnTouchsBreakArgs(p->root, &hArgs);
    }

    mmUIView_OnTouchsBreak(p->active, content);
}

void
mmUIViewContext_OnGetText(
    struct mmUIViewContext*                        p,
    struct mmEventEditText*                        content)
{
    mmUIView_OnGetText(p->active, content);
}

void
mmUIViewContext_OnSetText(
    struct mmUIViewContext*                        p,
    struct mmEventEditText*                        content)
{
    mmUIView_OnSetText(p->active, content);
}

void
mmUIViewContext_OnGetTextEditRect(
    struct mmUIViewContext*                        p,
    double                                         rect[4])
{
    mmUIView_OnGetTextEditRect(p->active, rect);
}

void
mmUIViewContext_OnInsertTextUtf16(
    struct mmUIViewContext*                        p,
    struct mmEventEditString*                      content)
{
    mmUIView_OnInsertTextUtf16(p->active, content);
}

void
mmUIViewContext_OnReplaceTextUtf16(
    struct mmUIViewContext*                        p,
    struct mmEventEditString*                      content)
{
    mmUIView_OnReplaceTextUtf16(p->active, content);
}

void
mmUIViewContext_OnInjectCopy(
    struct mmUIViewContext*                        p)
{
    mmUIView_OnInjectCopy(p->active);
}

void
mmUIViewContext_OnInjectCut(
    struct mmUIViewContext*                        p)
{
    mmUIView_OnInjectCut(p->active);
}

void
mmUIViewContext_OnInjectPaste(
    struct mmUIViewContext*                        p)
{
    mmUIView_OnInjectPaste(p->active);
}

void
mmUIViewContext_OnSafetySizeChanged(
    struct mmUIView*                               pWindow,
    struct mmEventMetrics*                         content)
{
    if (NULL != pWindow)
    {
        struct mmUIDim position[3];
        struct mmUIDim size[3];

        float _safety_x = (float)content->safety_area[0];
        float _safety_y = (float)content->safety_area[1];
        float _safety_w = (float)content->safety_area[2];
        float _safety_h = (float)content->safety_area[3];

        // The RootSafety must special layout.
        // URect: {"pos" :{"x":{"rel":0, "abs":0}, "y":{"rel":0, "abs":0}},
        //         "size":{"w":{"rel":1, "abs":0}, "h":{"rel":1, "abs":0}}}
        mmUIDimVec3MakeXY(position, _safety_x, 0.0f, _safety_y, 0.0f);
        mmUIDimVec3MakeXY(size, _safety_w, 0.0f, _safety_h, 0.0f);
        mmUIView_SetPosition(pWindow, position);
        mmUIView_SetSize(pWindow, size);
    }
}
