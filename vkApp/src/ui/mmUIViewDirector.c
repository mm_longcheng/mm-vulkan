/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewDirector.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIViewLoader.h"

void
mmUIViewDirector_Init(
    struct mmUIViewDirector*                       p)
{
    mmRbtreeStringVpt_Init(&p->hExclusivePool);
    mmListVpt_Init(&p->hExclusiveUsed);
    mmListVpt_Init(&p->hExclusiveIdle);

    mmRbtreeStringVpt_Init(&p->hMultiplexPool);
    mmListVpt_Init(&p->hMultiplexUsed);
    mmListVpt_Init(&p->hMultiplexIdle);

    mmRbtreeStringVpt_Init(&p->hLayerPool);

    p->pCurrentWindow = NULL;
    
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
    
    p->pViewContext = NULL;
    p->pViewLoader = NULL;
}

void
mmUIViewDirector_Destroy(
    struct mmUIViewDirector*                       p)
{
    mmRbtreeStringVpt_Destroy(&p->hExclusivePool);
    mmListVpt_Destroy(&p->hExclusiveUsed);
    mmListVpt_Destroy(&p->hExclusiveIdle);

    mmRbtreeStringVpt_Destroy(&p->hMultiplexPool);
    mmListVpt_Destroy(&p->hMultiplexUsed);
    mmListVpt_Destroy(&p->hMultiplexIdle);

    mmRbtreeStringVpt_Destroy(&p->hLayerPool);

    p->pCurrentWindow = NULL;
    
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
    
    p->pViewContext = NULL;
    p->pViewLoader = NULL;
}

void
mmUIViewDirector_SetViewContext(
    struct mmUIViewDirector* p,
    struct mmUIViewContext*                        pViewContext)
{
    p->pViewContext = pViewContext;
}

void
mmUIViewDirector_SetViewLoader(
    struct mmUIViewDirector*                       p,
    struct mmUIViewLoader*                         pViewLoader)
{
    p->pViewLoader = pViewLoader;
}

void
mmUIViewDirector_AssignmentViewContext(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w)
{
    assert(NULL != w && "w is a invalid null pointer.");
    mmUIView_SetContext(w, p->pViewContext);
}

void
mmUIViewDirector_SetRootSafety(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               pRootSafety)
{
    p->pRootSafety = pRootSafety;
}

void
mmUIViewDirector_SetRootWindow(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               pRootWindow)
{
    p->pRootWindow = pRootWindow;
    mmUIViewContext_SetRoot(p->pViewContext, p->pRootWindow);
}

void
mmUIViewDirector_PrepareRootWindow(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmUIView* w;
    
    w = mmUIViewLoader_ProduceViewType(
        p->pViewLoader,
        p->pViewContext,
        type,
        name);

    if (NULL != w)
    {
        mmUIViewDirector_SetRootWindow(p, w);
        
        mmUIView_Prepare(w);
    }
}

void
mmUIViewDirector_DiscardRootWindow(
    struct mmUIViewDirector*                       p)
{
    struct mmUIView* w = p->pRootWindow;
    
    if (NULL != w)
    {
        mmUIView_Discard(w);
        
        mmUIViewDirector_SetRootWindow(p, NULL);
    }
    
    mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
}

void
mmUIViewDirector_Prepare(
    struct mmUIViewDirector*                       p,
    const char*                                    root,
    const char*                                    master)
{
    mmUIViewDirector_PrepareRootWindow(p, root, root);
    mmUIViewDirector_SceneExclusiveForeground(p, master, master);
}

void
mmUIViewDirector_Discard(
    struct mmUIViewDirector*                       p)
{
    mmUIViewDirector_LayerClear(p);
    mmUIViewDirector_SceneClear(p);
    mmUIViewDirector_DiscardRootWindow(p);
}

struct mmUIView*
mmUIViewDirector_SceneExclusiveBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmUIView* w =
    mmUIViewDirector_ScenePush(p, type, name);
    mmUIViewDirector_SceneExclusiveEnterBackground(p, name);
    return w;
}

struct mmUIView*
mmUIViewDirector_SceneExclusiveForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    return mmUIViewDirector_ScenePush(p, type, name);
}

void
mmUIViewDirector_SceneExclusiveEnterBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;

    struct mmUIView* w = NULL;
    struct mmUIView* window = NULL;

    struct mmListHead* prev = NULL;
    struct mmListVptIterator* it = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusivePool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;

        mmListVpt_Remove(&p->hExclusiveUsed, w);
        mmListVpt_AddTail(&p->hExclusiveIdle, w);

        mmUIView_RmvChild(p->pRootSafety, w);
        mmUIView_SetVisible(w, 0);
        
        // reverse begin.
        prev = p->hExclusiveUsed.l.prev;
        it = mmList_Entry(prev, struct mmListVptIterator, n);
        if (prev != &p->hExclusiveUsed.l)
        {
            window = (struct mmUIView*)it->v;
            mmUIViewDirector_SceneSetCurrent(p, window);
        }
        else
        {
            mmUIViewDirector_SceneSetCurrent(p, NULL);
        }
    }
}

void
mmUIViewDirector_SceneExclusiveEnterForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusivePool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;

        mmListVpt_AddTail(&p->hExclusiveUsed, w);
        mmListVpt_Remove(&p->hExclusiveIdle, w);

        mmUIViewDirector_SceneSetCurrent(p, w);
    }
}

struct mmUIView*
mmUIViewDirector_SceneMultiplexForeground(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexPool, &hNameWeak);
    if (NULL == itor)
    {
        w = mmUIViewLoader_ProduceViewType(
            p->pViewLoader,
            p->pViewContext,
            type,
            name);
        
        mmUIViewDirector_AssignmentViewContext(p, w);

        mmRbtreeStringVpt_Set(&p->hMultiplexPool, &hNameWeak, w);
        mmListVpt_AddTail(&p->hMultiplexUsed, w);
        
        mmUIView_AddChild(p->pRootSafety, w);
        mmUIView_SetVisible(w, 1);
        mmUIView_Invalidate(w);

        mmUIView_Prepare(w);
    }
    else
    {
        w = (struct mmUIView*)itor->v;

        mmListVpt_Remove(&p->hMultiplexUsed, w);
        mmListVpt_AddTail(&p->hMultiplexUsed, w);
        mmListVpt_Remove(&p->hMultiplexIdle, w);

        mmUIView_RmvChild(p->pRootSafety, w);
        mmUIView_AddChild(p->pRootSafety, w);
        mmUIView_SetVisible(w, 1);
        mmUIView_Invalidate(w);
    }

    return w;
}

struct mmUIView*
mmUIViewDirector_SceneMultiplexBackground(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;

        mmUIView_RmvChild(p->pRootSafety, w);
        mmUIView_SetVisible(w, 0);

        mmListVpt_Remove(&p->hMultiplexUsed, w);
        mmListVpt_AddTail(&p->hMultiplexIdle, w);
    }
    return w;
}

void
mmUIViewDirector_SceneMultiplexTerminate(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;
        
        mmUIView_Discard(w);
        
        mmUIView_RmvChild(p->pRootSafety, w);

        mmListVpt_Remove(&p->hMultiplexUsed, w);
        mmListVpt_Remove(&p->hMultiplexIdle, w);
        mmRbtreeStringVpt_Erase(&p->hMultiplexPool, itor);
        
        mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
    }
}

struct mmUIView*
mmUIViewDirector_ScenePush(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusivePool, &hNameWeak);
    if (NULL == itor)
    {
        w = mmUIViewLoader_ProduceViewType(
            p->pViewLoader,
            p->pViewContext,
            type,
            name);
        
        mmUIViewDirector_AssignmentViewContext(p, w);

        mmRbtreeStringVpt_Set(&p->hExclusivePool, &hNameWeak, w);
        mmListVpt_AddTail(&p->hExclusiveUsed, w);
        
        mmUIViewDirector_SceneSetCurrent(p, w);

        mmUIView_Prepare(w);
    }
    else
    {
        w = (struct mmUIView*)itor->v;

        mmListVpt_Remove(&p->hExclusiveUsed, w);
        mmListVpt_AddTail(&p->hExclusiveUsed, w);
        mmListVpt_Remove(&p->hExclusiveIdle, w);

        mmUIViewDirector_SceneSetCurrent(p, w);
    }
    return w;
}

struct mmUIView*
mmUIViewDirector_ScenePop(
    struct mmUIViewDirector*                       p)
{
    struct mmUIView* w = NULL;
    struct mmListHead* prev = NULL;
    struct mmListVptIterator* it = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    if (NULL != p->pCurrentWindow)
    {
        mmUIView_Discard(p->pCurrentWindow);
        
        mmUIView_RmvChild(p->pRootSafety, p->pCurrentWindow);

        mmRbtreeStringVpt_Rmv(&p->hExclusivePool, &p->pCurrentWindow->name);
        mmListVpt_Remove(&p->hExclusiveUsed, p->pCurrentWindow);
        mmListVpt_Remove(&p->hExclusiveIdle, p->pCurrentWindow);

        mmUIViewLoader_RecycleViewType(p->pViewLoader, p->pCurrentWindow);
        p->pCurrentWindow = NULL;
    }

    // reverse begin.
    prev = p->hExclusiveUsed.l.prev;
    it = mmList_Entry(prev, struct mmListVptIterator, n);
    if (prev != &p->hExclusiveUsed.l)
    {
        w = (struct mmUIView*)it->v;
    }

    mmUIViewDirector_SceneSetCurrent(p, w);
    return w;
}

struct mmUIView*
mmUIViewDirector_SceneGoto(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmUIView* w =
    mmUIViewDirector_SceneGet(p, name);
    mmUIViewDirector_SceneSetCurrent(p, w);
    return w;
}

struct mmUIView*
mmUIViewDirector_SceneGet(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusivePool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;
    }
    return w;
}

struct mmUIView*
mmUIViewDirector_SceneGetCurrent(
    struct mmUIViewDirector*                       p)
{
    return p->pCurrentWindow;
}

void
mmUIViewDirector_SceneSetCurrent(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w)
{
    assert(p->pRootSafety && "p->pRootSafety is a null.");

    if (NULL != p->pCurrentWindow)
    {
        mmUIView_RmvChild(p->pRootSafety, p->pCurrentWindow);
        mmUIView_SetVisible(p->pCurrentWindow, 0);
        mmUIView_SetBranchActive(p->pCurrentWindow, NULL, 0);
    }
    p->pCurrentWindow = w;
    if (NULL != p->pCurrentWindow)
    {        
        mmUIView_AddChild(p->pRootSafety, p->pCurrentWindow);
        mmUIView_SetVisible(p->pCurrentWindow, 1);
        mmUIView_SetBranchActive(p->pCurrentWindow, NULL, 1);
        mmUIView_Invalidate(p->pCurrentWindow);
    }
    // when set current, we mark current view become active.
    mmUIViewContext_SetActive(p->pViewContext, p->pCurrentWindow);
}

void
mmUIViewDirector_ScenePopUntil(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    while (p->pCurrentWindow &&
           0 != mmString_CompareCStr(&p->pCurrentWindow->name, name))
    {
        mmUIViewDirector_ScenePop(p);
    }
}

void
mmUIViewDirector_SceneClear(
    struct mmUIViewDirector*                       p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmUIView* w = NULL;

    assert(p->pRootSafety && "p->pRootSafety is a null.");

    n = mmRb_First(&p->hExclusivePool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (struct mmUIView*)(it->v);
        
        mmUIView_Discard(w);
        
        mmRbtreeStringVpt_Erase(&p->hExclusivePool, it);

        mmListVpt_Remove(&p->hExclusiveUsed, w);
        mmListVpt_Remove(&p->hExclusiveIdle, w);

        mmUIView_RmvChild(p->pRootSafety, w);
        
        mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
    }

    n = mmRb_First(&p->hMultiplexPool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (struct mmUIView*)(it->v);
        
        mmUIView_Discard(w);
        
        mmRbtreeStringVpt_Erase(&p->hMultiplexPool, it);

        mmListVpt_Remove(&p->hMultiplexUsed, w);
        mmListVpt_Remove(&p->hMultiplexIdle, w);

        mmUIView_RmvChild(p->pRootSafety, w);
        
        mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
    }
}

struct mmUIView*
mmUIViewDirector_LayerAdd(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL == itor)
    {
        w = mmUIViewLoader_ProduceViewType(
            p->pViewLoader,
            p->pViewContext,
            type,
            name);
        
        mmUIViewDirector_AssignmentViewContext(p, w);

        mmRbtreeStringVpt_Set(&p->hLayerPool, &hNameWeak, w);

        assert(p->pCurrentWindow && "this->pCurrentWindow is a null.");
        mmUIView_AddChild(p->pCurrentWindow, w);
        mmUIView_Invalidate(w);

        mmUIView_Prepare(w);
    }
    else
    {
        w = (struct mmUIView*)itor->v;
        mmUIView_RmvChild(p->pCurrentWindow, w);
        mmUIView_AddChild(p->pCurrentWindow, w);
        mmUIView_SetVisible(w, 1);
        mmUIView_Invalidate(w);
    }
    return w;
}

struct mmUIView*
mmUIViewDirector_LayerRmv(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;
        
        mmUIView_Discard(w);
        
        mmUIView_RmvChild(p->pCurrentWindow, w);
        
        mmUIViewLoader_RecycleViewType(p->pViewLoader, w);

        mmRbtreeStringVpt_Erase(&p->hLayerPool, itor);
    }
    return w;
}

struct mmUIView*
mmUIViewDirector_LayerGet(
    struct mmUIViewDirector*                       p,
    const char*                                    name)
{
    struct mmString hNameWeak;
    struct mmUIView* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeaks(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (struct mmUIView*)itor->v;
    }
    return w;
}

void
mmUIViewDirector_LayerClear(
    struct mmUIViewDirector* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmUIView* w = NULL;

    n = mmRb_First(&p->hLayerPool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (struct mmUIView*)(it->v);
        
        mmUIView_Discard(w);
        
        mmRbtreeStringVpt_Erase(&p->hLayerPool, it);

        mmUIView_RmvChild(p->pCurrentWindow, w);
        
        mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
    }
}

struct mmUIView*
mmUIViewDirector_ProduceLayerContext(
    struct mmUIViewDirector*                       p,
    const char*                                    type,
    const char*                                    name)
{
    struct mmUIView* w = NULL;
    
    w = mmUIViewLoader_ProduceViewType(
        p->pViewLoader,
        p->pViewContext,
        type,
        name);
    
    mmUIViewDirector_AssignmentViewContext(p, w);

    mmUIView_Prepare(w);
    
    return w;
}

void
mmUIViewDirector_RecycleLayerContext(
    struct mmUIViewDirector*                       p,
    struct mmUIView*                               w)
{
    mmUIView_Discard(w);
    mmUIViewLoader_RecycleViewType(p->pViewLoader, w);
}

