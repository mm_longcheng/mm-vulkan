/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIButton.h"

#include "ui/mmUIView.h"

#include "core/mmAlloc.h"
#include "core/mmPropertyHelper.h"

#include "math/mmVector4.h"

void
mmUIButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIButton";

    /*- Sheet Name -*/
    mmPropertyDefineMutable(propertys,
        "Button.SheetName", offset, struct mmUIButton,
        hSheetName, String, "",
        NULL,
        NULL,
        cOrigin,
        "Button SheetName string default \"\".");

    /*- Frame Name -*/
    mmPropertyDefineMutable(propertys,
        "Button.FrameNormal", offset, struct mmUIButton,
        hFrameName[mmUIButtonStateNormal], String, "",
        NULL,
        NULL,
        cOrigin,
        "Button FrameNormal string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Button.FrameDisabled", offset, struct mmUIButton,
        hFrameName[mmUIButtonStateDisabled], String, "",
        NULL,
        NULL,
        cOrigin,
        "Button FrameDisabled string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Button.FrameHovering", offset, struct mmUIButton,
        hFrameName[mmUIButtonStateHovering], String, "",
        NULL,
        NULL,
        cOrigin,
        "Button FrameHovering string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Button.FramePushed", offset, struct mmUIButton,
        hFrameName[mmUIButtonStatePushed], String, "",
        NULL,
        NULL,
        cOrigin,
        "Button FramePushed string default \"\".");

    mmPropertyDefineMutable(propertys,
        "Button.FrameDraging", offset, struct mmUIButton,
        hFrameName[mmUIButtonStateDraging], String, "",
        NULL,
        NULL,
        cOrigin,
        "Button FrameDraging string default \"\".");

    /*- Pipeline -*/
    mmPropertyDefineMutable(propertys,
        "Button.PipelineOriginal", offset, struct mmUIButton,
        hPipelineName[mmUIButtonPipelineOriginal], String, "mmUIImageOriginal",
        NULL,
        NULL,
        cOrigin,
        "Button PipelineOriginal string default \"mmUIImageOriginal\".");

    mmPropertyDefineMutable(propertys,
        "Button.PipelineDisabled", offset, struct mmUIButton,
        hPipelineName[mmUIButtonPipelineDisabled], String, "mmUIImageOriginal",
        NULL,
        NULL,
        cOrigin,
        "Button PipelineDisabled string default \"mmUIImageOriginal\".");

    /*- Opacity -*/
    mmPropertyDefineMutable(propertys,
        "Button.OpacityNormal", offset, struct mmUIButton,
        hOpacity[mmUIButtonStateNormal], Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Button OpacityNormal float[4] default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Button.OpacityDisabled", offset, struct mmUIButton,
        hOpacity[mmUIButtonStateDisabled], Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Button OpacityDisabled float[4] default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Button.OpacityHovering", offset, struct mmUIButton,
        hOpacity[mmUIButtonStateHovering], Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Button OpacityHovering float[4] default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Button.OpacityPushed", offset, struct mmUIButton,
        hOpacity[mmUIButtonStatePushed], Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Button OpacityPushed float[4] default (1, 1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "Button.OpacityDraging", offset, struct mmUIButton,
        hOpacity[mmUIButtonStateDraging], Vec4, "(1, 1, 1, 1)",
        NULL,
        NULL,
        cOrigin,
        "Button OpacityDraging float[4] default (1, 1, 1, 1).");

    /*- Status -*/
    mmPropertyDefineMutable(propertys,
        "Button.Disabled", offset, struct mmUIButton,
        hDisabled, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Button Disabled SInt default 0.");

    mmPropertyDefineMutable(propertys,
        "Button.Hovering", offset, struct mmUIButton,
        hHovering, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Button Hovering SInt default 0.");

    mmPropertyDefineMutable(propertys,
        "Button.Pushed", offset, struct mmUIButton,
        hPushed, SInt, "0",
        NULL,
        NULL,
        cOrigin,
        "Button Pushed SInt default 0.");
}

void
mmUIButton_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    /*- Sheet Name -*/
    mmPropertyRemove(propertys, "Button.SheetName");
    /*- Frame Name -*/
    mmPropertyRemove(propertys, "Button.FrameNormal");
    mmPropertyRemove(propertys, "Button.FrameDisabled");
    mmPropertyRemove(propertys, "Button.FrameHovering");
    mmPropertyRemove(propertys, "Button.FramePushed");
    mmPropertyRemove(propertys, "Button.FrameDraging");
    /*- Pipeline Name -*/
    mmPropertyRemove(propertys, "Button.PipelineOriginal");
    mmPropertyRemove(propertys, "Button.PipelineDisabled");
    /*- Opacity -*/
    mmPropertyRemove(propertys, "Button.OpacityNormal");
    mmPropertyRemove(propertys, "Button.OpacityDisabled");
    mmPropertyRemove(propertys, "Button.OpacityHovering");
    mmPropertyRemove(propertys, "Button.OpacityPushed");
    mmPropertyRemove(propertys, "Button.OpacityDraging");
    /*- Status -*/
    mmPropertyRemove(propertys, "Button.Disabled");
    mmPropertyRemove(propertys, "Button.Hovering");
    mmPropertyRemove(propertys, "Button.Pushed");
}

void
mmUIButton_Init(
    struct mmUIButton*                             p)
{
    mmString_Init(&p->hSheetName);

    mmString_Init(&p->hFrameName[mmUIButtonStateNormal]);
    mmString_Init(&p->hFrameName[mmUIButtonStateDisabled]);
    mmString_Init(&p->hFrameName[mmUIButtonStateHovering]);
    mmString_Init(&p->hFrameName[mmUIButtonStatePushed]);
    mmString_Init(&p->hFrameName[mmUIButtonStateDraging]);

    mmString_Init(&p->hPipelineName[mmUIButtonPipelineOriginal]);
    mmString_Init(&p->hPipelineName[mmUIButtonPipelineDisabled]);

    mmVec4AssignValue(p->hOpacity[mmUIButtonStateNormal], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateDisabled], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateHovering], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStatePushed], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateDraging], 1.0f);

    p->hDisabled = 0;
    p->hHovering = 0;
    p->hPushed = 0;

    mmString_Assigns(&p->hPipelineName[mmUIButtonPipelineOriginal], "mmUIImageOriginal");
    mmString_Assigns(&p->hPipelineName[mmUIButtonPipelineDisabled], "mmUIImageOriginal");
}

void
mmUIButton_Destroy(
    struct mmUIButton*                             p)
{
    p->hPushed = 0;
    p->hHovering = 0;
    p->hDisabled = 0;

    mmVec4AssignValue(p->hOpacity[mmUIButtonStateDraging], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStatePushed], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateHovering], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateDisabled], 1.0f);
    mmVec4AssignValue(p->hOpacity[mmUIButtonStateNormal], 1.0f);

    mmString_Destroy(&p->hPipelineName[mmUIButtonPipelineDisabled]);
    mmString_Destroy(&p->hPipelineName[mmUIButtonPipelineOriginal]);

    mmString_Destroy(&p->hFrameName[mmUIButtonStateDraging]);
    mmString_Destroy(&p->hFrameName[mmUIButtonStatePushed]);
    mmString_Destroy(&p->hFrameName[mmUIButtonStateHovering]);
    mmString_Destroy(&p->hFrameName[mmUIButtonStateDisabled]);
    mmString_Destroy(&p->hFrameName[mmUIButtonStateNormal]);

    mmString_Destroy(&p->hSheetName);
}

int
mmUIButton_GetState(
    const struct mmUIButton*                       p)
{
    int state = mmUIButtonStateNormal;

    if (p->hDisabled)
    {
        state = mmUIButtonStateDisabled;
    }
    else if (p->hPushed)
    {
        state = p->hHovering ? mmUIButtonStatePushed : mmUIButtonStateDraging;
    }
    else if (p->hHovering)
    {
        state = mmUIButtonStateHovering;
    }
    else
    {
        state = mmUIButtonStateNormal;
    }

    return state;
}

int
mmUIButton_UpdateStateFrameName(
    const struct mmUIButton*                       p,
    int                                            state,
    struct mmString*                               pFrameName)
{
    int change;
    int idx;

    if (mmUIButtonStateNormal != state && mmString_Empty(&p->hFrameName[state]))
    {
        idx = mmUIButtonStateNormal;
    }
    else
    {
        idx = state;
    }

    change = (0 != mmString_Compare(pFrameName, &p->hFrameName[idx]));
    mmString_Assign(pFrameName, &p->hFrameName[idx]);

    return change;
}

int
mmUIButton_UpdateStateOpacity(
    const struct mmUIButton*                       p,
    int                                            state,
    float                                          hOpacity[4])
{
    int change = !mmVec4Equals(hOpacity, p->hOpacity[state]);

    mmVec4Assign(hOpacity, p->hOpacity[state]);

    return change;
}

void
mmUIButton_UpdateViewFilter(
    const struct mmUIButton*                       p,
    struct mmUIView*                               pView)
{
    if (p->hDisabled)
    {
        pView->filter |=  (mmUIFilterObstruct);
    }
    else
    {
        pView->filter &= ~(mmUIFilterObstruct);
    }
}

void
mmUIButton_UpdatePipelineName(
    const struct mmUIButton*                       p,
    struct mmString*                               pPipelineName)
{
    mmString_Assign(pPipelineName, &p->hPipelineName[p->hDisabled & 1]);
}