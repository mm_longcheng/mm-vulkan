/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUICube_h__
#define __mmUICube_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// The Unified Co-ordinate System
struct mmUIDim
{
    // offset
    float o;
    // scale
    float s;
};

void 
mmUIDim_Reset(
    struct mmUIDim*                                p);

void 
mmUIDim_Make(
    struct mmUIDim*                                p, 
    float                                          o, 
    float                                          s);

int 
mmUIDim_Equals(
    const struct mmUIDim*                          p, 
    const struct mmUIDim*                          q);

void 
mmUIDim_Assign(
    struct mmUIDim*                                p, 
    const struct mmUIDim*                          q);

void 
mmUIDim_Add(
    struct mmUIDim*                                r, 
    const struct mmUIDim*                          a, 
    const struct mmUIDim*                          b);

void 
mmUIDim_Sub(
    struct mmUIDim*                                r, 
    const struct mmUIDim*                          a, 
    const struct mmUIDim*                          b);

int
mmUIDimVec3Equals(
    struct mmUIDim                                 p[3],
    const struct mmUIDim                           q[3]);

void 
mmUIDimVec3Assign(
    struct mmUIDim                                 v[3], 
    const struct mmUIDim                           r[3]);

void 
mmUIDimVec3Reset(
    struct mmUIDim                                 v[3]);

void 
mmUIDimVec3Make(
    struct mmUIDim                                 v[3], 
    float                                          xo, 
    float                                          xs, 
    float                                          yo, 
    float                                          ys, 
    float                                          zo, 
    float                                          zs);

void 
mmUIDimVec3MakeXY(
    struct mmUIDim                                 v[3], 
    float                                          xo, 
    float                                          xs, 
    float                                          yo, 
    float                                          ys);

void 
mmUIDimVec3Add(
    struct mmUIDim                                 r[3], 
    const struct mmUIDim                           a[3], 
    const struct mmUIDim                           b[3]);

void 
mmUIDimVec3Sub(
    struct mmUIDim                                 r[3], 
    const struct mmUIDim                           a[3], 
    const struct mmUIDim                           b[3]);

struct mmUIRect
{
    struct mmUIDim min[2];
    struct mmUIDim max[2];
};

void 
mmUIRect_Reset(
    struct mmUIRect*                               p);

void 
mmUIRect_Assign(
    struct mmUIRect*                               p, 
    const struct mmUIRect*                         q);

struct mmUICube
{
    struct mmUIDim min[3];
    struct mmUIDim max[3];
};

void 
mmUICube_Reset(
    struct mmUICube*                               p);

void 
mmUICube_Assign(
    struct mmUICube*                               p, 
    const struct mmUICube*                         q);

void 
mmUICubeDim3ToValue(
    float                                          cube[3], 
    struct mmUIDim                                 dim[3], 
    float                                          value[3]);

struct mmUICubeScalar
{
    float min[3];
    float max[3];
};

int 
mmUICubeScalar_ContainsVec3(
    struct mmUICubeScalar*                         p, 
    const float                                    q[3]);

int 
mmUICubeScalar_ContainsVec2(
    struct mmUICubeScalar*                         p, 
    const float                                    q[2]);

int 
mmUICubeScalar_Overlap(
    const struct mmUICubeScalar*                   p, 
    const struct mmUICubeScalar*                   q);

int 
mmUICubeScalar_OverlapMax3(
    const struct mmUICubeScalar*                   p, 
    const float                                    qmax[3]);

#include "core/mmSuffix.h"

#endif//__mmUICube_h__
