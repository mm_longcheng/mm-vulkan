/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewMacro_h__
#define __mmUIViewMacro_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"

#include "core/mmPrefix.h"

/*
 * @brief Use for view type quick define include implement.
 *
 * @param Type       The view type for define.
 */
#define mmUIViewTypeDefine(Type)                                    \
                                                                    \
struct mmUIView ## Type;                                            \
                                                                    \
extern                                                              \
const struct mmMetaAllocator                                        \
mmUIMetaAllocatorView ## Type;                                      \
                                                                    \
extern                                                              \
const struct mmUIMetadata                                           \
mmUIMetadataView ## Type;                                           \
                                                                    \
void                                                                \
mmUIView ## Type ## _Produce(                                       \
    struct mmUIView ## Type*                           p);          \
                                                                    \
void                                                                \
mmUIView ## Type ## _Recycle(                                       \
    struct mmUIView ## Type*                           p);

/*
 * @brief Use for view type quick define source implement Propertys.
 *
 * @param Type       The view type for define.
 */
#define mmUIViewTypeImplementPropertys(Type)                        \
                                                                    \
void                                                                \
mmUIView ## Type ## _DefinePropertys(                               \
    struct mmPropertySet*                          propertys,       \
    size_t                                         offset)          \
{                                                                   \
    mmUIView_DefinePropertys(                                       \
        propertys,                                                  \
        offset + mmOffsetof(struct mmUIView ## Type, view));        \
}                                                                   \
                                                                    \
void                                                                \
mmUIView ## Type ## _RemovePropertys(                               \
    struct mmPropertySet*                          propertys)       \
{                                                                   \
    mmUIView_RemovePropertys(propertys);                            \
}                                                                   

/*
 * @brief Use for view type quick define source implement Allocator.
 *
 * @param Type       The view type for define.
 */
#define mmUIViewTypeImplementAllocator(Type)                        \
                                                                    \
void                                                                \
mmUIView ## Type ## _Produce(                                       \
    struct mmUIView ## Type*                           p)           \
{                                                                   \
    mmUIView ## Type ## _Init(p);                                   \
                                                                    \
    /* bind object metadata. */                                     \
    p->view.metadata = &mmUIMetadataView ## Type;                   \
    /* bind object for this. */                                     \
    mmPropertySet_SetObject(&p->view.propertys, p);                 \
                                                                    \
    mmUIView ## Type ## _DefinePropertys(&p->view.propertys, 0);    \
}                                                                   \
                                                                    \
void                                                                \
mmUIView ## Type ## _Recycle(                                       \
    struct mmUIView ## Type*                           p)           \
{                                                                   \
    mmUIView ## Type ## _RemovePropertys(&p->view.propertys);       \
                                                                    \
    /* bind object metadata. */                                     \
    p->view.metadata = &mmUIMetadataView ## Type;                   \
                                                                    \
    mmUIView ## Type ## _Destroy(p);                                \
}                                                                   \
                                                                    \
const struct mmUIWidgetOperable                                     \
mmUIWidgetOperableView ## Type =                                    \
{                                                                   \
    &mmUIView ## Type ## _OnPrepare,                                \
    &mmUIView ## Type ## _OnDiscard,                                \
};                                                                  \
                                                                    \
const struct mmMetaAllocator                                        \
mmUIMetaAllocatorView ## Type =                                     \
{                                                                   \
    "mmUIView" # Type,                                              \
    sizeof(struct mmUIView ## Type),                                \
    &mmUIView ## Type ## _Produce,                                  \
    &mmUIView ## Type ## _Recycle,                                  \
};                                                                  \
                                                                    \
const struct mmUIMetadata                                           \
mmUIMetadataView ## Type =                                          \
{                                                                   \
    &mmUIMetaAllocatorView ## Type,                                 \
    /*- Widget -*/                                                  \
    &mmUIWidgetOperableView ## Type,                                \
    NULL,                                                           \
    NULL,                                                           \
    /*- Responder -*/                                               \
    NULL,                                                           \
    NULL,                                                           \
    NULL,                                                           \
    NULL,                                                           \
};

 /*
  * @brief Use for view type quick define source implement.
  *
  * @param Type       The view type for define.
  */
#define mmUIViewTypeImplement(Type)                                 \
mmUIViewTypeImplementPropertys(Type);                               \
mmUIViewTypeImplementAllocator(Type);

#include "core/mmSuffix.h"

#endif//__mmUIViewMacro_h__
