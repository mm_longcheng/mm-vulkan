/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIRoot.h"

#include "ui/mmUIAspect.h"

#include "core/mmPropertyHelper.h"

#include "math/mmVector2.h"

void
mmUIRoot_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIRoot";

    /*- Root -*/
    mmPropertyDefineMutable(propertys,
        "Root.CanvasSize", offset, struct mmUIRoot,
        hCanvasSize, Vec2, "(378, 672)",
        NULL,
        NULL,
        cOrigin,
        "Root CanvasSize Vec2 default (378, 672).");

    mmPropertyDefineMutable(propertys,
        "Root.AspectRatio", offset, struct mmUIRoot,
        hAspectRatio, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "Root AspectRatio Vec2 default 0.");

    mmPropertyDefineMutable(propertys,
        "Root.AspectMode", offset, struct mmUIRoot,
        hAspectMode, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "Root AspectMode UInt8 default mmUIAspectModeExpand(2).");
}

void
mmUIRoot_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    /*- Root -*/
    mmPropertyRemove(propertys, "Root.CanvasSize");
    mmPropertyRemove(propertys, "Root.AspectRatio");
    mmPropertyRemove(propertys, "Root.AspectMode");
}

void
mmUIRoot_Init(
    struct mmUIRoot*                               p)
{
    mmVec2Make(p->hCanvasSize, 378, 672);
    p->hAspectRatio = 0.0f;
    p->hAspectMode = mmUIAspectModeExpand;
}

void
mmUIRoot_Destroy(
    struct mmUIRoot*                               p)
{
    p->hAspectMode = mmUIAspectModeExpand;
    p->hAspectRatio = 0.0f;
    mmVec2Make(p->hCanvasSize, 378, 672);
}
