/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIQuad_h__
#define __mmUIQuad_h__

#include "core/mmCore.h"

#include "vk/mmVKSheetLoader.h"

#include "core/mmPrefix.h"

struct mmUIVertex
{
    float position[3];
    float texcoord[2];
    float color[4];
};

struct mmUIImageFrame
{
    size_t imageSize[2];     /* (w, h)                 */
    int frame[4];            /* (x, y, w, h)           */
    int source[4];           /* (x, y, w, h)           */
    int offset[2];           /* (x, y)                 */
    int original[2];         /* (w, h)                 */
    int r;                   /* rotated clockwise      */
    int t;                   /* trimmed                */
    int b;                   /* border enable          */
    float anchor[2];         /* (x, y)                 */
    int borders[4];          /* (l, b, r, t)           */
};

void
mmUIImageFrameReset(
    struct mmUIImageFrame*                         iframe);

void
mmUIImageFrameFromSheet(
    struct mmUIImageFrame*                         iframe,
    const struct mmVKSheetFrame*                   sframe);

void
mmUIImageFrameFromImage(
    struct mmUIImageFrame*                         iframe,
    const size_t                                   imageSize[2],
    const int                                      frame[4],
    int                                            rotated);

void
mmUIVertex4AssignCornerColor(
    struct mmUIVertex                              vtx[4],
    const float                                    color[4][4]);

void
mmUIVertex4AssignFrameBasic(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe);

void
mmUIVertex4AssignFrameTiled(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    const float                                    size[2]);

void
mmUIVertex4AssignFrameBrush(
    struct mmUIVertex                              vtx[4],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    float                                          ox[2],
    float                                          oy[2]);

void 
mmUIVertex4AssignColor(
    struct mmUIVertex                              vtx[4],
    const float                                    color[4]);

void
mmUIVertex4AssignIndex16(
    uint16_t                                       idx[6],
    uint16_t                                       i);

void
mmUIVertex4AssignIndex32(
    uint32_t                                       idx[6],
    uint32_t                                       i);


void
mmUIVertex10AssignFrameBrush(
    struct mmUIVertex                              vtx[10],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    float                                          amount,
    int                                            direction,
    int                                            reverse,
    int*                                           pSegment);

void
mmUIVertex10AssignColor(
    struct mmUIVertex                              vtx[10],
    const float                                    color[4]);

void
mmUIVertex10AssignIndex16(
    uint16_t                                       idx[24],
    uint16_t                                       i,
    int                                            segment,
    int                                            direction,
    int                                            reverse);

void
mmUIVertex10AssignIndex32(
    uint32_t                                       idx[24],
    uint32_t                                       i,
    int                                            segment,
    int                                            direction,
    int                                            reverse);

void
mmUIVertex16AssignFrameSlice(
    struct mmUIVertex                              vtx[16],
    const mmUInt8_t                                flipped[2],
    const struct mmUIImageFrame*                   iframe,
    const float                                    size[2]);

void
mmUIVertex16AssignColor(
    struct mmUIVertex                              vtx[16],
    const float                                    color[4]);

void
mmUIVertex16AssignIndex16(
    uint16_t                                       idx[54],
    uint16_t                                       i);

void
mmUIVertex16AssignIndex32(
    uint32_t                                       idx[54],
    uint32_t                                       i);

#include "core/mmSuffix.h"

#endif//__mmUIQuad_h__
