/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIEditBox.h"

#include "core/mmKeyCode.h"
#include "core/mmText.h"

#include "unicode/mmUTF16.h"

#include "nwsi/mmClipboard.h"

int
mmTextIsWordBoundaryR(
    const struct mmUtf16String*                    str,
    int                                            idx)
{
    if (idx > 0)
    {
        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
        int s0 = mmTextIsSeparator(idx0);
        int s1 = mmTextIsSeparator(idx1);
        return (s0 && !s1);
    }
    else
    {
        return 1;
    }
}

int
mmTextIsWordBoundaryL(
    const struct mmUtf16String*                    str,
    int                                            idx)
{
    if (idx > 0)
    {
        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
        int s0 = mmTextIsSeparator(idx0);
        int s1 = mmTextIsSeparator(idx1);
        return (!s0 && s1);
    }
    else
    {
        return 1;
    }
}

int
mmTextIsWordBoundary(
    const struct mmUtf16String*                    str,
    int                                            idx)
{
    if (idx > 0)
    {
        mmUtf16_t idx0 = mmUtf16String_At(str, idx - 1);
        mmUtf16_t idx1 = mmUtf16String_At(str, idx);
        int s0 = mmTextIsSeparator(idx0);
        int s1 = mmTextIsSeparator(idx1);
        return (s0 && !s1);
    }
    else
    {
        return 1;
    }
}

int
mmTextMoveToWordPrev(
    const struct mmUtf16String*                    str,
    int                                            c)
{
    // always move at least one character
    --c;

    while (c >= 0 && !mmTextIsWordBoundary(str, c))
    {
        --c;
    }

    return (c < 0) ? 0 : c;
}

int
mmTextMoveToWordNext(
    const struct mmUtf16String*                    str,
    int                                            c)
{
    const int l = (int)mmUtf16String_Size(str);

    // always move at least one character
    ++c;

    while (c < l && !mmTextIsWordBoundary(str, c))
    {
        ++c;
    }

    return (c > l) ? l : c;
}

void
mmUIEditBox_Init(
    struct mmUIEditBox*                            p)
{
    p->text = NULL;
    p->c = 0;
    p->l = 0;
    p->r = 0;
    p->anchor = 0;
}

void
mmUIEditBox_Destroy(
    struct mmUIEditBox*                            p)
{
    p->anchor = 0;
    p->r = 0;
    p->l = 0;
    p->c = 0;
    p->text = NULL;
}

void
mmUIEditBox_SetUtf16String(
    struct mmUIEditBox*                            p,
    struct mmUtf16String*                          text)
{
    p->text = text;
}

int
mmUIEditBox_PerformCopy(
    struct mmUIEditBox*                            p,
    struct mmClipboard*                            clipboard)
{
    if (mmUIEditBox_GetSelectionLength(p) == 0)
    {
        return MM_FALSE;
    }
    else
    {
        struct mmUtf16String selectedText;
        size_t start = mmUIEditBox_GetSelectionStartIndex(p);
        const mmUtf16_t* d = mmUtf16String_Data(p->text);
        const mmUtf16_t* s = d + start;
        size_t n = mmUIEditBox_GetSelectionLength(p);
        mmUtf16String_MakeWeaksn(&selectedText, s, n);

        mmClipboard_SetText(clipboard, &selectedText);
        return MM_TRUE;
    }
}

int
mmUIEditBox_PerformCut(
    struct mmUIEditBox*                            p,
    struct mmClipboard*                            clipboard)
{
    if (!mmUIEditBox_PerformCopy(p, clipboard))
    {
        return MM_FALSE;
    }
    else
    {
        mmUIEditBox_HandleDelete(p);
        return MM_TRUE;
    }
}

int
mmUIEditBox_PerformPaste(
    struct mmUIEditBox*                            p,
    struct mmClipboard*                            clipboard)
{
    struct mmUtf16String clipboardText;
    mmClipboard_GetText(clipboard, &clipboardText);

    if (mmUtf16String_Empty(&clipboardText))
    {
        return MM_FALSE;
    }
    else
    {
        size_t start = mmUIEditBox_GetSelectionStartIndex(p);
        size_t n = mmUIEditBox_GetSelectionLength(p);

        mmUtf16String_Erase(p->text, start, n);

        mmUtf16String_Insert(p->text, start, &clipboardText);

        // erase selection using mode that does not modify getText()
        // (we just want to update state)
        mmUIEditBox_EraseSelectedText(p);

        // advance caret (done first so we can "do stuff" in event
        // handlers!)
        p->c += mmUtf16String_Size(&clipboardText);

        mmUIEditBox_HandleTextChanged(p);
        return MM_TRUE;
    }
}

size_t
mmUIEditBox_GetSelectionStartIndex(
    const struct mmUIEditBox*                      p)
{
    return (p->l != p->r) ? p->l : p->c;
}

size_t
mmUIEditBox_GetSelectionEndIndex(
    const struct mmUIEditBox*                      p)
{
    return (p->l != p->r) ? p->r : p->c;
}

size_t
mmUIEditBox_GetCaretIndex(
    const struct mmUIEditBox*                      p)
{
    return p->c;
}

size_t
mmUIEditBox_GetSelectionLength(
    struct mmUIEditBox*                            p)
{
    return p->r - p->l;
}

void
mmUIEditBox_SetCaretIndex(
    struct mmUIEditBox*                            p,
    size_t                                         caret)
{
    // make sure new position is valid
    size_t length = mmUtf16String_Size(p->text);
    if (caret > length)
    {
        caret = length;
    }

    p->c = caret;
}

void
mmUIEditBox_SetSelection(
    struct mmUIEditBox*                            p,
    size_t                                         start,
    size_t                                         end)
{
    size_t length = mmUtf16String_Size(p->text);
    // ensure selection start point is within the valid range
    if (start > length)
    {
        start = length;
    }

    // ensure selection end point is within the valid range
    if (end > length)
    {
        end = length;
    }

    // ensure start is before end
    if (start > end)
    {
        size_t tmp = end;
        end = start;
        start = tmp;
    }

    p->l = start;
    p->r = end;
}

void
mmUIEditBox_ClearSelection(
    struct mmUIEditBox*                            p)
{
    // perform action only if required.
    if (mmUIEditBox_GetSelectionLength(p) != 0)
    {
        mmUIEditBox_SetSelection(p, 0, 0);
    }
}

void
mmUIEditBox_EraseSelectedText(
    struct mmUIEditBox*                            p)
{
    if (mmUIEditBox_GetSelectionLength(p) != 0)
    {
        mmUIEditBox_SetCaretIndex(p, p->l);
        mmUIEditBox_ClearSelection(p);
    }
}

void
mmUIEditBox_HandleTextChanged(
    struct mmUIEditBox*                            p)
{
    size_t length = mmUtf16String_Size(p->text);

    // clear selection
    mmUIEditBox_ClearSelection(p);

    // make sure caret is within the text
    if (p->c > length)
    {
        mmUIEditBox_SetCaretIndex(p, length);
    }
}

void
mmUIEditBox_HandleBackspace(
    struct mmUIEditBox*                            p)
{
    size_t sl = mmUIEditBox_GetSelectionLength(p);
    if (sl != 0)
    {
        size_t ssi = mmUIEditBox_GetSelectionStartIndex(p);
        mmUtf16String_Erase(p->text, ssi, sl);

        mmUIEditBox_EraseSelectedText(p);

        mmUIEditBox_HandleTextChanged(p);
    }
    else if (mmUIEditBox_GetCaretIndex(p) > 0)
    {
        mmUtf16_t c = mmUtf16String_At(p->text, p->c - 1);
        int n = (U16_IS_SURROGATE(c) && U16_IS_TRAIL(c)) ? 2 : 1;

        mmUtf16String_Erase(p->text, p->c - n, n);

        mmUIEditBox_SetCaretIndex(p, p->c - n);

        mmUIEditBox_HandleTextChanged(p);
    }
}

void
mmUIEditBox_HandleDelete(
    struct mmUIEditBox*                            p)
{
    size_t sl = mmUIEditBox_GetSelectionLength(p);
    size_t length = mmUtf16String_Size(p->text);
    if (sl != 0)
    {
        size_t ssi = mmUIEditBox_GetSelectionStartIndex(p);
        mmUtf16String_Erase(p->text, ssi, sl);

        mmUIEditBox_EraseSelectedText(p);

        mmUIEditBox_HandleTextChanged(p);
    }
    else if (mmUIEditBox_GetCaretIndex(p) < length)
    {
        mmUtf16_t c = mmUtf16String_At(p->text, p->c);
        int n = (U16_IS_SURROGATE(c) && U16_IS_LEAD(c)) ? 2 : 1;

        mmUtf16String_Erase(p->text, p->c, n);

        mmUIEditBox_HandleTextChanged(p);
    }
}

void
mmUIEditBox_InsertUtf16(
    struct mmUIEditBox*                            p,
    mmUInt16_t*                                    s,
    size_t                                         l)
{
    size_t ssi = mmUIEditBox_GetSelectionStartIndex(p);
    size_t sl = mmUIEditBox_GetSelectionLength(p);

    mmUtf16String_Replacesn(p->text, ssi, sl, s, l);

    // erase selection using mode that does not modify getText()
    // (we just want to update state)
    mmUIEditBox_EraseSelectedText(p);

    // advance caret (done first so we can "do stuff" in event
    // handlers!)
    p->c += l;

    mmUIEditBox_HandleTextChanged(p);
}

void
mmUIEditBox_ReplaceUtf16(
    struct mmUIEditBox*                            p,
    size_t                                         o,
    size_t                                         n,
    mmUInt16_t*                                    s,
    size_t                                         l)
{
    mmUtf16String_Replacesn(p->text, o, n, s, l);

    // Not trigger mmUIEditBox_handleTextChanged.
    // We modify the cursor externally. 
}

void
mmUIEditBox_HandleCharLeft(
    struct mmUIEditBox*                            p,
    mmUInt32_t                                     sysKeys)
{
    if (p->c > 0)
    {
        mmUtf16_t c = mmUtf16String_At(p->text, p->c - 1);
        int n = (U16_IS_SURROGATE(c) && U16_IS_TRAIL(c)) ? 2 : 1;
        
        mmUIEditBox_SetCaretIndex(p, p->c - n);
    }

    if (sysKeys & MM_MODIFIER_SHIFT)
    {
        mmUIEditBox_SetSelection(p, p->c, p->anchor);
    }
    else
    {
        mmUIEditBox_ClearSelection(p);
    }
}

void
mmUIEditBox_HandleWordLeft(
    struct mmUIEditBox*                            p,
    mmUInt32_t                                     sysKeys)
{
    if (p->c > 0)
    {
        mmUIEditBox_SetCaretIndex(p, mmTextMoveToWordPrev(p->text, (int)p->c));
    }

    if (sysKeys & MM_MODIFIER_SHIFT)
    {
        mmUIEditBox_SetSelection(p, p->c, p->anchor);
    }
    else
    {
        mmUIEditBox_ClearSelection(p);
    }
}

void
mmUIEditBox_HandleCharRight(
    struct mmUIEditBox*                            p,
    mmUInt32_t                                     sysKeys)
{
    size_t length = mmUtf16String_Size(p->text);

    if (p->c < length)
    {
        mmUtf16_t c = mmUtf16String_At(p->text, p->c);
        int n = (U16_IS_SURROGATE(c) && U16_IS_LEAD(c)) ? 2 : 1;
        
        mmUIEditBox_SetCaretIndex(p, p->c + n);
    }

    if (sysKeys & MM_MODIFIER_SHIFT)
    {
        mmUIEditBox_SetSelection(p, p->c, p->anchor);
    }
    else
    {
        mmUIEditBox_ClearSelection(p);
    }
}

void
mmUIEditBox_HandleWordRight(
    struct mmUIEditBox*                            p,
    mmUInt32_t                                     sysKeys)
{
    size_t length = mmUtf16String_Size(p->text);

    if (p->c < length)
    {
        mmUIEditBox_SetCaretIndex(p, mmTextMoveToWordNext(p->text, (int)p->c));
    }

    if (sysKeys & MM_MODIFIER_SHIFT)
    {
        mmUIEditBox_SetSelection(p, p->c, p->anchor);
    }
    else
    {
        mmUIEditBox_ClearSelection(p);
    }
}
