/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIScheme.h"

#include "core/mmLogger.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"

#include "math/mmVector4.h"

#include "graphics/mmColor.h"

#include "nwsi/mmGraphicsFactory.h"

void
mmUIScheme_Init(
    struct mmUIScheme*                             p)
{
    mmString_Init(&p->hPath);
    mmUIParameterScheme_Init(&p->hScheme);
    mmRbtreeStrVpt_Init(&p->hColor);
    mmRbtreeStrVpt_Init(&p->hStyle);
}

void
mmUIScheme_Destroy(
    struct mmUIScheme*                             p)
{
    mmRbtreeStrVpt_Destroy(&p->hStyle);
    mmRbtreeStrVpt_Destroy(&p->hColor);
    mmUIParameterScheme_Destroy(&p->hScheme);
    mmString_Destroy(&p->hPath);
}

void
mmUIScheme_SetPath(
    struct mmUIScheme*                             p,
    const char*                                    pPath)
{
    mmString_Assigns(&p->hPath, pPath);
}

void
mmUIScheme_PrepareFont(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext)
{
    size_t i;
    int code;
    const char* cFamily;
    const char* cPath;
    struct mmVectorValue* pFont;
    struct mmUIParameterFont* f;

    struct mmLogger* gLogger = mmLogger_Instance();

    pFont = &p->hScheme.Font;

    for (i = 0; i < pFont->size; ++i)
    {
        f = (struct mmUIParameterFont*)mmVectorValue_At(pFont, i);
        cFamily = mmString_CStr(&f->Family);
        cPath = mmString_CStr(&f->Path);
        code = mmGraphicsFactory_LoadFontFromResource(
            pGraphicsFactory, 
            pFileContext, 
            cFamily, 
            cPath);
        if (MM_SUCCESS != code)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_LoadFontFromResource failure. Family: %s Path: %s", 
                __FUNCTION__, __LINE__, cFamily, cPath);
            continue;
        }
    }
}

void
mmUIScheme_DiscardFont(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext)
{
    size_t i;
    int code;
    const char* cFamily;
    const char* cPath;
    struct mmVectorValue* pFont;
    struct mmUIParameterFont* f;

    struct mmLogger* gLogger = mmLogger_Instance();

    pFont = &p->hScheme.Font;

    for (i = pFont->size - 1; (size_t)(-1) != i; --i)
    {
        f = (struct mmUIParameterFont*)mmVectorValue_At(pFont, i);
        cFamily = mmString_CStr(&f->Family);
        cPath = mmString_CStr(&f->Path);
        code = mmGraphicsFactory_UnloadFontByResource(
            pGraphicsFactory,
            pFileContext,
            cFamily,
            cPath);
        if (MM_SUCCESS != code)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_UnloadFontByResource failure. Family: %s Path: %s",
                __FUNCTION__, __LINE__, cFamily, cPath);
            continue;
        }
    }
}

void
mmUIScheme_Prepare(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext)
{
    size_t i;

    struct mmVectorValue* pColor;
    struct mmVectorValue* pStyle;

    struct mmUIParameterProperty* e;
    struct mmUIParameterStyle* u;

    pColor = &p->hScheme.Color;
    pStyle = &p->hScheme.Style;

    mmUIScheme_PrepareFont(p, pGraphicsFactory, pFileContext);

    for (i = 0; i < pColor->size; ++i)
    {
        e = (struct mmUIParameterProperty*)mmVectorValue_At(pColor, i);
        mmRbtreeStrVpt_Set(&p->hColor, mmString_CStr(&e->Name), e);
    }

    for (i = 0; i < pStyle->size; ++i)
    {
        u = (struct mmUIParameterStyle*)mmVectorValue_At(pStyle, i);
        mmRbtreeStrVpt_Set(&p->hStyle, mmString_CStr(&u->Name), u);
    }
}

void
mmUIScheme_Discard(
    struct mmUIScheme*                             p,
    struct mmGraphicsFactory*                      pGraphicsFactory,
    struct mmFileContext*                          pFileContext)
{
    mmRbtreeStrVpt_Clear(&p->hStyle);
    mmRbtreeStrVpt_Clear(&p->hColor);

    mmUIScheme_DiscardFont(p, pGraphicsFactory, pFileContext);
}

void
mmUIScheme_GetColor(
    struct mmUIScheme*                             p,
    const struct mmString*                         pName,
    float                                          hColor[4])
{
    struct mmUIParameterProperty* e;
    const char* cName = mmString_CStr(pName);
    e = (struct mmUIParameterProperty*)mmRbtreeStrVpt_Get(&p->hColor, cName);
    if (NULL != e)
    {
        const char* str = mmString_CStr(&e->Value);
        size_t l = mmString_Size(&e->Value);
        char c = mmCStringLSidePrint(str, l);

        switch (c)
        {
        case '(':
            // (R, G, B, A) float[4]
            mmValueStringToTypeArray(
                &e->Value,
                hColor,
                sizeof(float),
                "(,,,)",
                4,
                &mmValue_AToFloat);
            break;
        case '#':
            // "#RRGGBBAA" or "#RRGGBB"(AA = FF)
            mmValue_AToFloatRGBA(str, hColor);
            break;
        default:
            // scheme color name.
            mmVec4Make(hColor, 0.0f, 0.0f, 0.0f, 1.0f);
            break;
        }
    }
    else
    {
        mmVec4Make(hColor, 0.0f, 0.0f, 0.0f, 1.0f);
    }
}

const struct mmUIParameterStyle*
mmUIScheme_GetStyle(
    struct mmUIScheme*                             p,
    const struct mmString*                         pName)
{
    const char* cName = mmString_CStr(pName);
    return (const struct mmUIParameterStyle*)mmRbtreeStrVpt_Get(&p->hStyle, cName);
}
