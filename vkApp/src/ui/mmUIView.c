/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIView.h"

#include "ui/mmUIViewContext.h"
#include "ui/mmUIPropertyHelper.h"
#include "ui/mmUIEventArgs.h"
#include "ui/mmUIViewLoader.h"

#include "core/mmAlloc.h"
#include "core/mmPropertyHelper.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"

#include "math/mmVector2.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix4x4.h"

#include "graphics/mmColor.h"

#include "algorithm/mmMurmurHash.h"

#include "nwsi/mmISurface.h"
#include "nwsi/mmEventSurface.h"

#include "vk/mmVKCommandBuffer.h"

static 
void 
mmUIView_MarkCacheInvalid(
    struct mmUIView*                               p, 
    size_t                                         o, 
    size_t                                         l, 
    void*                                          func)
{
    typedef void(*ValidFuncType)(struct mmUIView* p);
    ValidFuncType Invalid = (ValidFuncType)func;
    void* pointer = (void*)((char*)p + o);

    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    mmMemset(pointer, 0, l);

    next = p->children.l.next;
    while (next != &p->children.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        (*(Invalid))(child);
    }
}

const char*
mmUIAlignment_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case mmUIAlignmentMinimum:
        return "Minimum";
    case mmUIAlignmentMiddled:
        return "Middled";
    case mmUIAlignmentMaximum:
        return "Maximum";
    default:
        return "Middled";
    }
}

int
mmUIAlignment_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xD1EAA0E4:
        // 0xD1EAA0E4 Minimum
        return mmUIAlignmentMinimum;
    case 0xB4F0AD67:
        // 0xB4F0AD67 Middled
        return mmUIAlignmentMiddled;
    case 0x750CE67F:
        // 0x750CE67F Maximum
        return mmUIAlignmentMaximum;
    default:
        return mmUIAlignmentMiddled;
    }
}

void
mmUIView_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset)
{
    static const char cOrigin[] = "mmUIView";

    mmPropertyDefineMutable(propertys,
        "View.Name", offset, struct mmUIView,
        name, String, "",
        &mmUIView_SetName,
        &mmUIView_GetName,
        cOrigin,
        "View name string default \"\".");

    mmPropertyDefineMutable(propertys,
        "View.Alignment", offset, struct mmUIView,
        align, UInt8Vec3, "(0, 0, 0)",
        &mmUIView_SetAlignment,
        &mmUIView_GetAlignment,
        cOrigin,
        "View align UInt8[3] default mmUIAlignmentMinimum(0, 0, 0).");

    mmPropertyDefineMutable(propertys,
        "View.AspectMode", offset, struct mmUIView,
        aspectMode, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "View AspectMode UInt8 default mmUIAspectModeIgnore(0).");

    mmPropertyDefineMutable(propertys,
        "View.AspectRatio", offset, struct mmUIView,
        aspectRatio, Float, "0",
        NULL,
        NULL,
        cOrigin,
        "View AspectRatio float default 0.");

    mmPropertyDefineMutable(propertys,
        "View.Anchor", offset, struct mmUIView,
        anchor, Vec3, "(0.5f, 0.5f, 0.5f)",
        &mmUIView_SetAnchor,
        &mmUIView_GetAnchor,
        cOrigin,
        "View Anchor float[3] default (0.5f, 0.5f, 0.5f).");

    mmPropertyDefineMutable(propertys,
        "View.Size", offset, struct mmUIView,
        size, UIDim3, "((0, 0), (0, 0), (0, 0))",
        &mmUIView_SetSize,
        &mmUIView_GetSize,
        cOrigin,
        "View Size UIDim[3] default ((0, 0), (0, 0), (0, 0)).");

    mmPropertyDefineMutable(propertys,
        "View.Position", offset, struct mmUIView,
        position, UIDim3, "((0, 0), (0, 0), (0, 0))",
        &mmUIView_SetPosition,
        &mmUIView_GetPosition,
        cOrigin,
        "View Position UIDim[3] default ((0, 0), (0, 0), (0, 0)).");

    mmPropertyDefineMutable(propertys,
        "View.Scale", offset, struct mmUIView,
        scale, Vec3, "(1, 1, 1)",
        &mmUIView_SetScale,
        &mmUIView_GetScale,
        cOrigin,
        "View Scale float[3] default (1, 1, 1).");

    mmPropertyDefineMutable(propertys,
        "View.Quaternion", offset, struct mmUIView,
        quaternion, Quat, "(0, 0, 0, 1)",
        &mmUIView_SetQuaternion,
        &mmUIView_GetQuaternion,
        cOrigin,
        "View Quaternion float[4] default (0, 0, 0, 1).");

    mmPropertyDefineMutable(propertys,
        "View.Opacity", offset, struct mmUIView,
        opacity, Vec4, "(1, 1, 1, 1)",
        &mmUIView_SetOpacity,
        &mmUIView_GetOpacity,
        cOrigin,
        "View Opacity float[4] default (1, 1, 1, 1).");
    
    mmPropertyDefineMutable(propertys,
        "View.Visible", offset, struct mmUIView,
        visible, UInt8, "1",
        NULL,
        NULL,
        cOrigin,
        "View Visible UInt8 default 1.");
    
    mmPropertyDefineMutable(propertys,
        "View.Filter", offset, struct mmUIView,
        filter, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "View Filter UInt8 default 0.");
    
    mmPropertyDefineMutable(propertys,
        "View.Clipping", offset, struct mmUIView,
        clipping, UInt8, "0",
        NULL,
        NULL,
        cOrigin,
        "View Clipping UInt8 default 0.");
    
    mmPropertyDefineMutable(propertys,
        "View.Identity", offset, struct mmUIView,
        identity, UInt32, "-1",
        NULL,
        NULL,
        cOrigin,
        "View Clipping UInt32 default -1.");
}

void
mmUIView_RemovePropertys(
    struct mmPropertySet*                          propertys)
{
    mmPropertyRemove(propertys, "View.Name");
    mmPropertyRemove(propertys, "View.Alignment");
    mmPropertyRemove(propertys, "View.AspectMode");
    mmPropertyRemove(propertys, "View.AspectRatio");
    mmPropertyRemove(propertys, "View.Anchor");
    mmPropertyRemove(propertys, "View.Size");
    mmPropertyRemove(propertys, "View.Position");
    mmPropertyRemove(propertys, "View.Scale");
    mmPropertyRemove(propertys, "View.Quaternion");
    mmPropertyRemove(propertys, "View.Opacity");
    mmPropertyRemove(propertys, "View.Visible");
    mmPropertyRemove(propertys, "View.Filter");
    mmPropertyRemove(propertys, "View.Clipping");
    mmPropertyRemove(propertys, "View.Identity");
}

const struct mmMetaAllocator mmUIMetaAllocatorView =
{
    "mmUIView",
    sizeof(struct mmUIView),
    &mmUIView_Produce,
    &mmUIView_Recycle,
};

const struct mmUIMetadata mmUIMetadataView =
{
    &mmUIMetaAllocatorView,
    NULL,
    NULL,
};

const char* mmUIView_EventKeypadPressed = "EventKeypadPressed";
const char* mmUIView_EventKeypadRelease = "EventKeypadRelease";

const char* mmUIView_EventKeypadStatus = "EventKeypadStatus";

const char* mmUIView_EventCursorBegan = "EventCursorBegan";
const char* mmUIView_EventCursorMoved = "EventCursorMoved";
const char* mmUIView_EventCursorEnded = "EventCursorEnded";
const char* mmUIView_EventCursorBreak = "EventCursorBreak";
const char* mmUIView_EventCursorWheel = "EventCursorWheel";

const char* mmUIView_EventTouchsBegan = "EventTouchsBegan";
const char* mmUIView_EventTouchsMoved = "EventTouchsMoved";
const char* mmUIView_EventTouchsEnded = "EventTouchsEnded";
const char* mmUIView_EventTouchsBreak = "EventTouchsBreak";

const char* mmUIView_EventSized = "EventSized";
const char* mmUIView_EventGainActive = "EventGainActive";
const char* mmUIView_EventLossActive = "EventLossActive";

const char* mmUIView_EventUpdate = "EventUpdate";

const char* mmUIView_EventInvalidate = "EventInvalidate";

const char* mmUIView_EventClicked = "EventClicked";

const char* mmUIView_EventEnterSurface = "EventEnterSurface";
const char* mmUIView_EventLeaveSurface = "EventLeaveSurface";
const char* mmUIView_EventEnterArea = "EventEnterArea";
const char* mmUIView_EventLeaveArea = "EventLeaveArea";

void
mmUIView_Init(
    struct mmUIView*                               p)
{
    mmString_Init(&p->name);
    mmListVpt_Init(&p->children);
    mmPropertySet_Init(&p->propertys);
    mmEventSet_Init(&p->events);
    
    p->metadata = NULL;
    p->context = NULL;
    p->parent = NULL;

    mmUIDimVec3Reset(p->size);
    mmUIDimVec3Reset(p->position);

    mmVec3Make(p->scale, 1.0f, 1.0f, 1.0f);
    mmQuatMakeIdentity(p->quaternion);

    mmVec4Make(p->opacity, 1.0f, 1.0f, 1.0f, 1.0f);
    
    mmVec3Make(p->anchor, 0.5f, 0.5f, 0.5f);

    mmMat4x4MakeIdentity(p->worldTransform);
    mmVec3Make(p->worldSize, 0.0f, 0.0f, 0.0f);
    mmVec4Make(p->worldOpacity, 1.0f, 1.0f, 1.0f, 1.0f);

    p->aspectRatio = 0.0f;
    p->aspectMode = (mmUInt8_t)mmUIAspectModeIgnore;

    mmMemset(p->align, (mmUInt8_t)mmUIAlignmentMinimum, sizeof(p->align));

    p->visible = MM_TRUE;

    p->filter = 0;
    p->clipping = 0;

    p->validWorldTransform = MM_FALSE;
    p->validWorldSize = MM_FALSE;
    p->validWorldOpacity = MM_FALSE;

    p->active = MM_FALSE;

    p->depth = 0;

    p->identity = (mmUInt32_t)(-1);
}

void
mmUIView_Destroy(
    struct mmUIView*                               p)
{
    p->identity = (mmUInt32_t)(-1);

    p->depth = 0;

    p->active = MM_FALSE;

    p->validWorldOpacity = MM_FALSE;
    p->validWorldSize = MM_FALSE;
    p->validWorldTransform = MM_FALSE;

    p->clipping = 0;
    p->filter = 0;

    p->visible = MM_TRUE;

    mmMemset(p->align, (mmUInt8_t)mmUIAlignmentMinimum, sizeof(p->align));

    p->aspectMode = (mmUInt8_t)mmUIAspectModeIgnore;
    p->aspectRatio = 0.0f;

    mmVec4Make(p->worldOpacity, 1.0f, 1.0f, 1.0f, 1.0f);
    mmVec3Make(p->worldSize, 0.0f, 0.0f, 0.0f);
    mmMat4x4MakeIdentity(p->worldTransform);

    mmVec3Make(p->anchor, 0.5f, 0.5f, 0.5f);
    
    mmVec4Make(p->opacity, 1.0f, 1.0f, 1.0f, 1.0f);

    mmQuatMakeIdentity(p->quaternion);
    mmVec3Make(p->scale, 1.0f, 1.0f, 1.0f);

    mmUIDimVec3Reset(p->position);
    mmUIDimVec3Reset(p->size);

    p->parent = NULL;
    p->context = NULL;
    p->metadata = NULL;

    mmEventSet_Destroy(&p->events);
    mmPropertySet_Destroy(&p->propertys);
    mmListVpt_Destroy(&p->children);
    mmString_Destroy(&p->name);
}

void
mmUIView_Produce(
    struct mmUIView*                               p)
{
    mmUIView_Init(p);
    
    // bind object metadata.
    p->metadata = &mmUIMetadataView;
    // bind object for this.
    mmPropertySet_SetObject(&p->propertys, p);
    
    mmUIView_DefinePropertys(&p->propertys, 0);
}

void
mmUIView_Recycle(
    struct mmUIView*                               p)
{
    mmUIView_RemovePropertys(&p->propertys);
    
    // bind object metadata.
    p->metadata = &mmUIMetadataView;
    
    mmUIView_Destroy(p);
}

void
mmUIView_SetContext(
    struct mmUIView*                               p,
    struct mmUIViewContext*                        context)
{
    p->context = context;
}

void
mmUIView_GetContextCube(
    const struct mmUIView*                         p,
    float                                          hCube[3])
{
    if (NULL == p->context)
    {
        mmVec3Make(hCube, 0.0f, 0.0f, 0.0f);
    }
    else
    {
        mmVec3Assign(hCube, p->context->cube);
    }
}

float
mmUIView_GetContextDensity(
    const struct mmUIView*                         p)
{
    return (NULL != p->context) ? p->context->density : 1.0f;
}

void
mmUIView_GetSchemeColor(
    const struct mmUIView*                         p,
    const struct mmString*                         pValue,
    float                                          hColor[4])
{
    // Note: if pValue is empty, we can not modify the hColor.
    if (!mmString_Empty(pValue))
    {
        const char* str = mmString_CStr(pValue);
        size_t l = mmString_Size(pValue);
        char c = mmCStringLSidePrint(str, l);

        switch (c)
        {
        case '(':
            // (R, G, B, A) float[4]
            mmValueStringToTypeArray(
                pValue,
                hColor,
                sizeof(float),
                "(,,,)",
                4,
                &mmValue_AToFloat);
            break;
        case '#':
            // "#RRGGBBAA" or "#RRGGBB"(AA = FF)
            mmValue_AToFloatRGBA(str, hColor);
            break;
        default:
            // scheme color name.
            if (NULL == p->context)
            {
                mmVec4Make(hColor, 0.0f, 0.0f, 0.0f, 1.0f);
            }
            else
            {
                mmUIViewContext_GetSchemeColor(
                    p->context,
                    pValue,
                    hColor);
            }
            break;
        }
    }
}

void
mmUIView_GetLocalTransform(
    struct mmUIView*                               p,
    float                                          hLocalTransform[4][4])
{
    float hLocalPosition[3];
    float hParentCube[3];

    if (NULL != p->parent)
    {
        mmUIView_GetWorldSize(p->parent, hParentCube);
    }
    else
    {
        mmUIView_GetContextCube(p, hParentCube);
    }

    mmUICubeDim3ToValue(hParentCube, p->position, hLocalPosition);

    hLocalPosition[0] += hParentCube[0] * p->align[0] * 0.5f;
    hLocalPosition[1] += hParentCube[1] * p->align[1] * 0.5f;
    hLocalPosition[2] += hParentCube[2] * p->align[2] * 0.5f;

    mmMat4x4MakeTransform(hLocalTransform, hLocalPosition, p->quaternion, p->scale);
}

void
mmUIView_GetWorldTransform(
    struct mmUIView*                               p,
    float                                          hWorldTransform[4][4])
{
    if (p->validWorldTransform)
    {
        mmMat4x4Assign(hWorldTransform, p->worldTransform);
    }
    else
    {
        if (NULL != p->parent)
        {
            float hParentTransform[4][4];
            float hLocalTransform[4][4];
            float hLocalSize[3];
            float hTranslate[3];

            mmUIView_GetWorldTransform(p->parent, hParentTransform);

            // ignore the parent anchor. 
            // child coordinate system always base on parent anchor(0, 0, 0).
            mmUIView_GetLocalSize(p->parent, hLocalSize);
            hTranslate[0] = -hLocalSize[0] * p->parent->anchor[0];
            hTranslate[1] = -hLocalSize[1] * p->parent->anchor[1];
            hTranslate[2] = -hLocalSize[2] * p->parent->anchor[2];
            mmMat4x4MulTranslate(hParentTransform, hParentTransform, hTranslate);

            mmUIView_GetLocalTransform(p, hLocalTransform);

            mmMat4x4Mul(hWorldTransform, hParentTransform, hLocalTransform);
        }
        else
        {
            mmUIView_GetLocalTransform(p, hWorldTransform);
        }

        mmMat4x4Assign(p->worldTransform, hWorldTransform);
        p->validWorldTransform = MM_TRUE;
    }
}

void
mmUIView_GetWorldSize(
    struct mmUIView*                               p,
    float                                          hWorldSize[3])
{
    if (p->validWorldSize)
    {
        mmVec3Assign(hWorldSize, p->worldSize);
    }
    else
    {
        float v[3];

        if (NULL != p->parent)
        {
            // Note:
            // Not WorldSize, it is LocalSize.
            // Cannot include parent node scale.
            mmUIView_GetLocalSize(p->parent, v);
        }
        else
        {
            mmUIView_GetContextCube(p, v);
        }

        mmUICubeDim3ToValue(v, p->size, hWorldSize);

        mmVec3Mul(hWorldSize, hWorldSize, p->scale);

        // avoid negative size.
        mmVec3Max(hWorldSize, hWorldSize, mmVec3Zero);

        mmVec2SizeScaleToAspect(v, hWorldSize, p->aspectMode, p->aspectRatio, hWorldSize);

        mmVec3Assign(p->worldSize, hWorldSize);
        p->validWorldSize = MM_TRUE;
    }
}

void
mmUIView_GetWorldOpacity(
    struct mmUIView*                               p,
    float                                          hWorldOpacity[4])
{
    if (p->validWorldOpacity)
    {
        mmVec4Assign(hWorldOpacity, p->worldOpacity);
    }
    else
    {
        if (NULL != p->parent)
        {
            float hParentOpacity[4];
            mmUIView_GetWorldOpacity(p->parent, hParentOpacity);
            mmVec4Mul(hWorldOpacity, p->opacity, hParentOpacity);
        }
        else
        {
            mmVec4Assign(hWorldOpacity, p->opacity);
        }

        mmVec4Assign(p->worldOpacity, hWorldOpacity);
        p->validWorldOpacity = MM_TRUE;
    }
}

void
mmUIView_GetWorldRect(
    struct mmUIView*                               p,
    float                                          hWorldRect[4])
{
    struct mmUICubeScalar hCubeScalar;

    mmUIView_GetWorldCubeScalar(p, &hCubeScalar);

    hWorldRect[0] = hCubeScalar.min[0];
    hWorldRect[1] = hCubeScalar.min[1];
    hWorldRect[2] = hCubeScalar.max[0];
    hWorldRect[3] = hCubeScalar.max[1];
}

void
mmUIView_GetLocalSize(
    struct mmUIView*                               p,
    float                                          hLocalSize[3])
{
    mmUIView_GetWorldSize(p, hLocalSize);
    mmVec3Div(hLocalSize, hLocalSize, p->scale);
}

void
mmUIView_GetWorldPosition(
    struct mmUIView*                               p,
    float                                          position[3])
{
    float hWorldTransform[4][4];
    mmUIView_GetWorldTransform(p, hWorldTransform);
    mmMat4x4MulVec3(position, hWorldTransform, mmVec3Zero);
}

void
mmUIView_GetWorldCubeScalar(
    struct mmUIView*                               p,
    struct mmUICubeScalar*                         pCube)
{
    // Note: The world CubeScalar will be ignore rotation.

    float lmin[3];
    float lmax[3];
    float negate[3];

    float hLocalSize[3];
    float hWorldTransform[4][4];
    mmUIView_GetLocalSize(p, hLocalSize);
    mmUIView_GetWorldTransform(p, hWorldTransform);

    mmVec3Negate(negate, p->anchor);

    // min = (  - anchor) * hLocalSize
    mmVec3Mul(lmin, negate, hLocalSize);
    mmMat4x4MulVec3(pCube->min, hWorldTransform, lmin);

    // max = (1 - anchor) * hLocalSize
    mmVec3Sub(lmax, mmVec3Unit, p->anchor);
    mmVec3Mul(lmax, lmax, hLocalSize);
    mmMat4x4MulVec3(pCube->max, hWorldTransform, lmax);
}

void
mmUIView_AddChild(
    struct mmUIView*                               p,
    struct mmUIView*                               child)
{
    // Internal check NULL is better for this API.
    if (NULL != child)
    {
        child->parent = p;
        child->depth = p->depth + 1;
        mmUIView_MarkCacheInvalidWhole(child);
        mmUIView_HandleSized(child);
        mmListVpt_AddTail(&p->children, child);
    }
}

void
mmUIView_RmvChild(
    struct mmUIView*                               p,
    struct mmUIView*                               child)
{
    // Internal check NULL is better for this API.
    if (NULL != child)
    {
        child->parent = NULL;
        child->depth = 0;
        mmUIView_MarkCacheInvalidWhole(child);
        mmUIView_HandleSized(child);
        mmListVpt_Remove(&p->children, child);
    }
}

struct mmUIView*
mmUIView_GetChildByName(
    struct mmUIView*                               p,
    const struct mmString*                         name)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    next = p->children.l.next;
    while (next != &p->children.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;
        if (0 == mmString_Compare(&child->name, name))
        {
            return child;
        }
    }

    return NULL;
}

struct mmUIView*
mmUIView_GetChildByCStr(
    struct mmUIView*                               p,
    const char*                                    name)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    return mmUIView_GetChildByName(p, &hWeakKey);
}

struct mmUIView*
mmUIView_GetChildAssert(
    struct mmUIView*                               p,
    const char*                                    name)
{
    struct mmUIView* v =
    mmUIView_GetChildByCStr(p, name);
    assert(NULL != v && "v is invalid.");
    return v;
}

void*
mmUIView_GetModuleAssert(
    const struct mmUIView*                         p,
    const char*                                    name)
{
    void* module;
    
    if (NULL == p->context)
    {
        module = NULL;
        assert(NULL != p->context && "p->context is invalid.");
    }
    else
    {
        module = mmUIViewContext_GetModule(p->context, name);
        assert(NULL != module && "module is invalid.");
    }
    
    return module;
}

void
mmUIView_SetValueMember(
    struct mmUIView*                               p,
    const char*                                    pName,
    const void*                                    pValue)
{
    mmPropertySet_SetValueMember(&p->propertys, pName, pValue);
}

void
mmUIView_GetValueMember(
    const struct mmUIView*                         p,
    const char*                                    pName,
    void*                                          pValue)
{
    mmPropertySet_GetValueMember(&p->propertys, pName, pValue);
}

void
mmUIView_SetValueDirect(
    struct mmUIView*                               p,
    const char*                                    pName,
    const void*                                    pValue)
{
    mmPropertySet_SetValueDirect(&p->propertys, pName, pValue);
}

void
mmUIView_GetValueDirect(
    const struct mmUIView*                         p,
    const char*                                    pName,
    void*                                          pValue)
{
    mmPropertySet_GetValueDirect(&p->propertys, pName, pValue);
}

void
mmUIView_SetValueString(
    struct mmUIView*                               p,
    const char*                                    pName,
    const struct mmString*                         pValue)
{
    mmPropertySet_SetValueString(&p->propertys, pName, pValue);
}

void
mmUIView_GetValueString(
    const struct mmUIView*                         p,
    const char*                                    pName,
    struct mmString*                               pValue)
{
    mmPropertySet_GetValueString(&p->propertys, pName, pValue);
}

void
mmUIView_SetPointerDirect(
    const struct mmUIView*                         p,
    const char*                                    pName,
    const void*                                    ptr)
{
    mmPropertySet_SetPointerDirect(&p->propertys, pName, ptr);
}

void*
mmUIView_GetPointerDirect(
    const struct mmUIView*                         p,
    const char*                                    pName)
{
    return mmPropertySet_GetPointerDirect(&p->propertys, pName);
}

void
mmUIView_SetValueCStr(
    struct mmUIView*                               p,
    const char*                                    pName,
    const char*                                    pValue)
{
    mmPropertySet_SetValueCStr(&p->propertys, pName, pValue);
}

void
mmUIView_SetName(
    struct mmUIView*                               p,
    const struct mmString*                         name)
{
    mmString_Assign(&p->name, name);
}

void
mmUIView_GetName(
    const struct mmUIView*                         p,
    struct mmString*                               name)
{
    mmString_Assign(name, &p->name);
}

void
mmUIView_SetAlignment(
    struct mmUIView*                               p,
    const mmUInt8_t                                align[3])
{
    p->align[0] = align[0];
    p->align[1] = align[1];
    p->align[2] = align[2];
    mmUIView_MarkCacheInvalidWorldTransform(p);
}

void
mmUIView_GetAlignment(
    const struct mmUIView*                         p,
    mmUInt8_t                                      align[3])
{
    align[0] = p->align[0];
    align[1] = p->align[1];
    align[2] = p->align[2];
}

void
mmUIView_SetAspectMode(
    struct mmUIView*                               p,
    mmUInt8_t                                      aspectMode)
{
    p->aspectMode = (mmUInt8_t)aspectMode;
    mmUIView_MarkCacheInvalidWorldSize(p);
    mmUIView_MarkCacheInvalidWorldTransform(p);
    mmUIView_HandleSized(p);
}

mmUInt8_t
mmUIView_GetAspectMode(
    const struct mmUIView*                         p)
{
    return p->aspectMode;
}

void
mmUIView_SetAspectRatio(
    struct mmUIView*                               p,
    float                                          aspectRatio)
{
    p->aspectRatio = aspectRatio;
    mmUIView_MarkCacheInvalidWorldSize(p);
    mmUIView_MarkCacheInvalidWorldTransform(p);
    mmUIView_HandleSized(p);
}

float
mmUIView_GetAspectRatio(
    const struct mmUIView*                         p)
{
    return p->aspectRatio;
}

void
mmUIView_SetAnchor(
    struct mmUIView*                               p,
    const float                                    anchor[3])
{
    mmVec3Assign(p->anchor, anchor);
}

void
mmUIView_GetAnchor(
    const struct mmUIView*                         p,
    float                                          anchor[3])
{
    mmVec3Assign(anchor, p->anchor);
}

void
mmUIView_SetSize(
    struct mmUIView*                               p,
    const struct mmUIDim                           size[3])
{
    mmUIDimVec3Assign(p->size, size);
    mmUIView_MarkCacheInvalidWorldSize(p);
    mmUIView_MarkCacheInvalidWorldTransform(p);
    mmUIView_HandleSized(p);
}

void
mmUIView_GetSize(
    const struct mmUIView*                         p,
    struct mmUIDim                                 size[3])
{
    mmUIDimVec3Assign(size, p->size);
}

void
mmUIView_SetPosition(
    struct mmUIView*                               p,
    const struct mmUIDim                           position[3])
{
    mmUIDimVec3Assign(p->position, position);
    mmUIView_MarkCacheInvalidWorldTransform(p);
}

void
mmUIView_GetPosition(
    const struct mmUIView*                         p,
    struct mmUIDim                                 position[3])
{
    mmUIDimVec3Assign(position, p->position);
}

void
mmUIView_SetScale(
    struct mmUIView*                               p,
    const float                                    scale[3])
{
    mmVec3Assign(p->scale, scale);
    mmUIView_MarkCacheInvalidWorldSize(p);
    mmUIView_MarkCacheInvalidWorldTransform(p);
    mmUIView_HandleSized(p);
}

void
mmUIView_GetScale(
    const struct mmUIView*                         p,
    float                                          scale[3])
{
    mmVec3Assign(scale, p->scale);
}

void
mmUIView_SetQuaternion(
    struct mmUIView*                               p,
    const float                                    quaternion[4])
{
    mmQuatAssign(p->quaternion, quaternion);
    mmUIView_MarkCacheInvalidWorldTransform(p);
}

void
mmUIView_GetQuaternion(
    const struct mmUIView*                         p,
    float                                          quaternion[4])
{
    mmQuatAssign(quaternion, p->quaternion);
}

void
mmUIView_SetOpacity(
    struct mmUIView*                               p,
    const float                                    opacity[4])
{
    mmVec4Assign(p->opacity, opacity);
    mmUIView_MarkCacheInvalidWorldOpacity(p);
}

void
mmUIView_GetOpacity(
    const struct mmUIView*                         p,
    float                                          opacity[4])
{
    mmVec4Assign(opacity, p->opacity);
}

void
mmUIView_SetVisible(
    struct mmUIView*                               p,
    int                                            visible)
{
    p->visible = visible;
}

int
mmUIView_GetVisible(
    const struct mmUIView*                         p)
{
    return p->visible;
}
void
mmUIView_SetFilter(
    struct mmUIView*                               p,
    int                                            filter)
{
    p->filter = filter;
}

int
mmUIView_GetFilter(
    const struct mmUIView*                         p)
{
    return p->filter;
}

void
mmUIView_SetClipping(
    struct mmUIView*                               p,
    int                                            clipping)
{
    p->clipping = clipping;
}

int
mmUIView_GetClipping(
    const struct mmUIView*                         p)
{
    return p->clipping;
}

void
mmUIView_SetIdentity(
    struct mmUIView*                               p,
    mmUInt32_t                                     identity)
{
    p->identity = identity;
}

mmUInt32_t
mmUIView_GetIdentity(
    const struct mmUIView*                         p)
{
    return p->identity;
}

void
mmUIView_HandleSized(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    mmUIView_OnHandleSized(p);

    next = p->children.l.next;
    while (next != &p->children.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_HandleSized(child);
    }
}

void
mmUIView_MarkCacheInvalidWhole(
    struct mmUIView*                               p)
{
    enum
    {
        offset = mmOffsetof(struct mmUIView, validWorldTransform),
        length = sizeof(uint8_t) * 3,
    };
    mmUIView_MarkCacheInvalid(p, offset, length, &mmUIView_MarkCacheInvalidWhole);
}

void
mmUIView_MarkCacheInvalidWorldTransform(
    struct mmUIView*                               p)
{
    enum
    {
        offset = mmOffsetof(struct mmUIView, validWorldTransform),
        length = mmMemberSizeof(struct mmUIView, validWorldTransform),
    };
    mmUIView_MarkCacheInvalid(p, offset, length, &mmUIView_MarkCacheInvalidWorldTransform);
}

void
mmUIView_MarkCacheInvalidWorldSize(
    struct mmUIView*                               p)
{
    enum
    {
        offset = mmOffsetof(struct mmUIView, validWorldSize),
        length = mmMemberSizeof(struct mmUIView, validWorldSize),
    };
    mmUIView_MarkCacheInvalid(p, offset, length, &mmUIView_MarkCacheInvalidWorldSize);
}

void
mmUIView_MarkCacheInvalidWorldOpacity(
    struct mmUIView*                               p)
{
    enum
    {
        offset = mmOffsetof(struct mmUIView, validWorldOpacity),
        length = mmMemberSizeof(struct mmUIView, validWorldOpacity),
    };
    mmUIView_MarkCacheInvalid(p, offset, length, &mmUIView_MarkCacheInvalidWorldOpacity);
}

void
mmUIView_Prepare(
    struct mmUIView*                               p)
{
    mmUIView_PrepareChildren(p);
    mmUIView_OnPrepare(p);
}

void
mmUIView_Discard(
    struct mmUIView*                               p)
{
    mmUIView_OnDiscard(p);
    mmUIView_DiscardChildren(p);
}

void
mmUIView_PrepareChildren(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Prepare(child);
    }
}

void
mmUIView_DiscardChildren(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* prev = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    prev = children->l.prev;
    while (prev != &children->l)
    {
        curr = prev;
        prev = prev->prev;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Discard(child);
    }
}

void
mmUIView_Update(
    struct mmUIView*                               p,
    struct mmEventUpdate*                          content)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    mmUIView_OnUpdate(p, content);

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Update(child, content);
    }
}

void
mmUIView_Invalidate(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    mmUIView_OnInvalidate(p);

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Invalidate(child);
    }
}

void
mmUIView_Sized(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    mmUIView_OnSized(p);

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Sized(child);
    }
}

void
mmUIView_Changed(
    struct mmUIView*                               p)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    mmUIView_OnChanged(p);

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_Changed(child);
    }
}

int
mmUIView_GetIsWidgetDrawable(
    const struct mmUIView*                         p)
{
    int hIsWidget = 0;

    do
    {
        const struct mmUIMetadata* metadata;
        if (NULL == p)
        {
            break;
        }
        metadata = p->metadata;
        if (NULL == metadata)
        {
            break;
        }
        hIsWidget = (NULL != metadata->WidgetDrawable);
    } while (0);

    return hIsWidget;
}

int
mmUIView_GetIsResponderCursor(
    const struct mmUIView*                         p)
{
    int hIsResponder = 0;

    do
    {
        const struct mmUIMetadata* metadata;
        if (NULL == p)
        {
            break;
        }
        metadata = p->metadata;
        if (NULL == metadata)
        {
            break;
        }
        hIsResponder = (NULL != metadata->ResponderCursor);
    } while (0);

    return hIsResponder;
}

int
mmUIView_GetIsResponderEditor(
    const struct mmUIView*                         p)
{
    int hIsResponder = 0;
    
    do
    {
        const struct mmUIMetadata* metadata;
        if (NULL == p)
        {
            break;
        }
        metadata = p->metadata;
        if (NULL == metadata)
        {
            break;
        }
        hIsResponder = (NULL != metadata->ResponderEditor);
    } while (0);
    
    return hIsResponder;
}

void
mmUIView_GetLocalPoint(
    struct mmUIView*                               p,
    const float                                    point[2],
    float                                          local[2])
{
    float cpos[3];
    float apos[3];
    float ppos[3];

    float vpos[3] = { point[0], point[1], 0.0f, };

    float hLocalSize[3];
    float hWorldSize[3];
    float hViewToWorldTransform[4][4];
    float hWorldToViewTransform[4][4];

    mmUIView_GetLocalSize(p, hLocalSize);
    mmUIView_GetWorldSize(p, hWorldSize);
    mmUIView_GetWorldTransform(p, hViewToWorldTransform);
    mmMat4x4Inverse(hWorldToViewTransform, hViewToWorldTransform);

    // point = anchor * hLocalSize + cursor
    mmMat4x4MulVec3(cpos, hWorldToViewTransform, vpos);
    mmVec3Mul(apos, hLocalSize, p->anchor);
    mmVec3Add(ppos, cpos, apos);

    mmVec2Assign(local, ppos);
}

int
mmUIView_GetIsHit(
    struct mmUIView*                               p,
    const float                                    point[2])
{
    float hLocalPos[2];
    float hLocalSize[3];
    mmUIView_GetLocalPoint(p, point, hLocalPos);
    mmUIView_GetLocalSize(p, hLocalSize);
    return
        hLocalPos[0] >= 0.0f          &&
        hLocalPos[0] <= hLocalSize[0] &&
        hLocalPos[1] >= 0.0f          &&
        hLocalPos[1] <= hLocalSize[1];
}

struct mmUIView*
mmUIView_GetHitView(
    struct mmUIView*                               p,
    const float                                    point[2])
{
    // Internal check NULL is better for this API.
    
    struct mmUIView* v = NULL;
    
    do
    {
        struct mmUIView* child;
        struct mmListHead* prev;
        struct mmListHead* curr;
        struct mmListVptIterator* it;

        // note: opacity transparent not a condition.
        
        if (NULL == p)
        {
            // check self pointer is better for use.
            break;
        }

        // test self.
        if (!p->visible)
        {
            // can not visible.
            break;
        }

        if (0 == (p->filter & mmUIFilterBoundary) && 
            !mmUIView_GetIsHit(p, point))
        {
            // cube not contains point.
            break;
        }
        
        if (0 == (p->filter & mmUIFilterChildren))
        {
            // test children.
            // reverse order.
            prev = p->children.l.prev;
            while (prev != &p->children.l)
            {
                curr = prev;
                prev = prev->prev;
                it = mmList_Entry(curr, struct mmListVptIterator, n);
                child = (struct mmUIView*)it->v;

                v = mmUIView_GetHitView(child, point);
                if (NULL != v)
                {
                    // find a better child.
                    break;
                }
            }
        }

        if (0 == (p->filter & mmUIFilterObstruct))
        {
            // check and pick suitable.
            v = (NULL == v) ? p : v;
        }

    } while(0);
    
    return v;
}

void
mmUIView_OnHandleSized(
    struct mmUIView*                               p)
{
    do
    {
        // handle size value change, when size not change will do nothing.
        // this process will fire size update event.
        typedef void(*SizedFuncType)(void* p);

        float o[3];
        float n[3];
        struct mmEventUIViewArgs hArgs;
        const struct mmUIWidgetVariable* WidgetVariable;
        assert(NULL != p->metadata && "metadata is invalid.");

        mmVec3Assign(o, p->worldSize);
        mmUIView_GetWorldSize(p, n);
        if (mmVec3Equals(o, n))
        {
            break;
        }

        mmEventUIViewArgs_Assign(&hArgs, 0, p);
        mmEventSet_FireEvent(&p->events, mmUIView_EventSized, &hArgs);

        WidgetVariable = p->metadata->WidgetVariable;
        if (NULL == WidgetVariable)
        {
            break;
        }

        // We only fire world size changed event.
        (*((SizedFuncType)(WidgetVariable->OnSized)))(p);

    } while (0);
}

void
mmUIView_OnRecordCommand(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd)
{
    if (p->clipping)
    {
        uint32_t hScissorCache[4];
        uint32_t hScissorValue[4];
        
        float hWorldRect[4];
        float density = (NULL != p->context) ? p->context->density : 1.0f;

        // hWorldRect [minx, miny, maxx, maxy]
        mmUIView_GetWorldRect(p, hWorldRect);
        
        // apply density to swapchain window size coordinate space.
        mmVec4MulK(hWorldRect, hWorldRect, density);
        
        // clip negative.
        mmVec4Max(hWorldRect, hWorldRect, mmVec4Zero);

        mmVKCommandBuffer_ScissorValueClamp(cmd, hWorldRect, hScissorValue);

        mmVKCommandBuffer_ScissorStorage(cmd, hScissorCache, hScissorValue);

        mmUIView_OnRecordDrawable(p, cmd);

        mmVKCommandBuffer_ScissorRestore(cmd, hScissorCache);
    }
    else
    {
        mmUIView_OnRecordDrawable(p, cmd);
    }
}

void
mmUIView_OnRecordDrawable(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd)
{
    if (p->visible)
    {
        const struct mmUIMetadata* metadata;
        metadata = p->metadata;
        if (NULL == metadata || NULL == metadata->WidgetDrawable)
        {
            mmUIView_OnRecordChildren(p, cmd);
        }
        else
        {
            typedef void(*RecordFuncType)(void* p, void* cmd);
            const struct mmUIWidgetDrawable* WidgetDrawable;
            WidgetDrawable = metadata->WidgetDrawable;
            (*((RecordFuncType)(WidgetDrawable->OnRecord)))(p, cmd);
        }
    }
}

void
mmUIView_OnRecordChildren(
    struct mmUIView*                               p,
    struct mmVKCommandBuffer*                      cmd)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_OnRecordCommand(child, cmd);
    }
}

void
mmUIView_OnEventParentArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args)
{
    typedef void(*EventArgsFunc)(struct mmUIView*, struct mmEventArgs*);

    mmEventSet_FireEvent(&p->events, name, args);

    if (NULL != p->parent && 0 == args->handled)
    {
        (*((EventArgsFunc)func))(p->parent, args);
    }

    ++args->handled;
}

void
mmUIView_OnEventRTraversalArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args)
{
    typedef void(*EventArgsFunc)(struct mmUIView*, struct mmEventArgs*);

    struct mmUIView* child = NULL;
    struct mmListHead* prev = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    prev = children->l.prev;
    while (prev != &children->l)
    {
        curr = prev;
        prev = prev->prev;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        (*((EventArgsFunc)func))(child, args);
    }

    mmEventSet_FireEvent(&p->events, name, args);
    ++args->handled;
}

void
mmUIView_OnEventPTraversalArgs(
    struct mmUIView*                               p,
    const char*                                    name,
    const void*                                    func,
    struct mmEventArgs*                            args)
{
    typedef void(*EventArgsFunc)(struct mmUIView*, struct mmEventArgs*);

    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &p->children;

    mmEventSet_FireEvent(&p->events, name, args);
    ++args->handled;

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        (*((EventArgsFunc)func))(child, args);
    }
}

void
mmUIView_OnEventContainArgs(
    struct mmUIView*                               p,
    const struct mmUIView*                         ancestor,
    const void*                                    func,
    struct mmEventArgs*                            args)
{
    // Internal check NULL is better for this API.
    // self pointer p can be NULL.
    
    typedef void(*EventArgsFunc)(void* p, struct mmEventArgs* args);

    struct mmUIView* v = p;

    while (NULL != v && v != ancestor)
    {
        args->handled = 0;
        (*((EventArgsFunc)func))(v, args);
        v = v->parent;
    }
}

void
mmUIView_OnUpdateArgs(
    struct mmUIView*                               p,
    struct mmEventUpdateArgs*                      args)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    
    struct mmListVpt* children = &p->children;
    
    mmUIView_OnUpdate(p, args->content);

    mmEventSet_FireEvent(&p->events, mmUIView_EventUpdate, args);
    ++args->hSuper.handled;

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_OnUpdateArgs(child, args);
    }
}

void
mmUIView_OnInvalidateArgs(
    struct mmUIView*                               p,
    struct mmEventArgs*                            args)
{
    struct mmUIView* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    
    struct mmListVpt* children = &p->children;
    
    mmUIView_OnInvalidate(p);

    mmEventSet_FireEvent(&p->events, mmUIView_EventInvalidate, args);
    ++args->handled;

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmUIView*)it->v;

        mmUIView_OnInvalidateArgs(child, args);
    }
}

void
mmUIView_OnKeypadPressedArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p,
        mmUIView_EventKeypadPressed,
        &mmUIView_OnKeypadPressedArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnKeypadReleaseArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p,
        mmUIView_EventKeypadRelease,
        &mmUIView_OnKeypadReleaseArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnKeypadStatusArgs(
    struct mmUIView*                               p,
    struct mmEventKeypadStatusArgs*                args)
{
    mmUIView_OnEventParentArgs(
        p,
        mmUIView_EventKeypadStatus,
        &mmUIView_OnKeypadStatusArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnCursorBeganArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p,
        mmUIView_EventCursorBegan,
        &mmUIView_OnCursorBeganArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnCursorMovedArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p, 
        mmUIView_EventCursorMoved, 
        &mmUIView_OnCursorMovedArgs, 
        (struct mmEventArgs*)args);
}

void
mmUIView_OnCursorEndedArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p, 
        mmUIView_EventCursorEnded, 
        &mmUIView_OnCursorEndedArgs, 
        (struct mmEventArgs*)args);
}

void
mmUIView_OnCursorBreakArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p, 
        mmUIView_EventCursorBreak, 
        &mmUIView_OnCursorBreakArgs, 
        (struct mmEventArgs*)args);
}

void
mmUIView_OnCursorWheelArgs(
    struct mmUIView*                               p,
    struct mmEventCursorArgs*                      args)
{
    mmUIView_OnEventParentArgs(
        p, 
        mmUIView_EventCursorWheel, 
        &mmUIView_OnCursorWheelArgs, 
        (struct mmEventArgs*)args);
}

void
mmUIView_OnTouchsBeganArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args)
{
    mmUIView_OnEventRTraversalArgs(
        p,
        mmUIView_EventTouchsBegan,
        &mmUIView_OnTouchsBeganArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnTouchsMovedArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args)
{
    mmUIView_OnEventRTraversalArgs(
        p,
        mmUIView_EventTouchsMoved,
        &mmUIView_OnTouchsMovedArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnTouchsEndedArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args)
{
    mmUIView_OnEventRTraversalArgs(
        p,
        mmUIView_EventTouchsEnded,
        &mmUIView_OnTouchsEndedArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnTouchsBreakArgs(
    struct mmUIView*                               p,
    struct mmEventTouchsArgs*                      args)
{
    mmUIView_OnEventRTraversalArgs(
        p,
        mmUIView_EventTouchsBreak,
        &mmUIView_OnTouchsBreakArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnClickedArgs(
    struct mmUIView*                               p,
    struct mmEventUIClickedArgs*                   args)
{
    mmUIView_OnEventParentArgs(
        p,
        mmUIView_EventClicked,
        &mmUIView_OnClickedArgs,
        (struct mmEventArgs*)args);
}

void
mmUIView_OnEnterSurfaceArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args)
{
    mmEventSet_FireEvent(&p->events, mmUIView_EventEnterSurface, args);
}

void
mmUIView_OnLeaveSurfaceArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args)
{
    mmEventSet_FireEvent(&p->events, mmUIView_EventLeaveSurface, args);
}

void
mmUIView_OnEnterAreaArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args)
{
    mmEventSet_FireEvent(&p->events, mmUIView_EventEnterArea, args);
}

void
mmUIView_OnLeaveAreaArgs(
    struct mmUIView*                               p,
    struct mmEventUIFocusArgs*                     args)
{
    mmEventSet_FireEvent(&p->events, mmUIView_EventLeaveArea, args);
}

void
mmUIView_OnAspect(
    struct mmUIView*                               p,
    const float                                    rect[4],
    const float                                    size[2],
    mmUInt8_t                                      mode,
    float                                          ratio)
{
    // Internal check NULL is better for this API.
    if (NULL != p)
    {
        struct mmUIDim uPosition[3];
        struct mmUIDim uSize[3];

        float r[2];
        float s[3] = { 1, 1, 1 };
        const float* f = rect;

        mmVec2SizeAspectToScale(size, &f[2], mode, ratio, s);

        r[0] = rect[2] * s[0];
        r[1] = rect[3] * s[1];

        // 1 / s
        mmVec3Div(s, mmVec3Unit, s);

        mmUIDimVec3MakeXY(uPosition, f[0], 0.0f, f[1], 0.0f);
        mmUIDimVec3MakeXY(/**/uSize, r[0], 0.0f, r[1], 0.0f);
        mmUIView_SetPosition(p, uPosition);
        mmUIView_SetSize(p, uSize);
        mmUIView_SetScale(p, s);
    }
}

const struct mmUIView*
mmUIView_GetAncestor(
    const struct mmUIView*                         p,
    const struct mmUIView*                         q)
{
    if (NULL == p || NULL == q)
    {
        return NULL;
    }
    else
    {
        mmUInt32_t i, d;
        const struct mmUIView* v0 = NULL;
        const struct mmUIView* v1 = NULL;
        if (p->depth < q->depth)
        {
            d = p->depth;
            v0 = p;
            v1 = q;
        }
        else
        {
            d = q->depth;
            v0 = q;
            v1 = p;
        }

        for (i = d; i < v1->depth; i++)
        {
            v1 = v1->parent;
        }

        i = 0;
        while (i < d && v0 != v1)
        {
            v0 = v0->parent;
            v1 = v1->parent;
            i++;
        }

        return (v0 == v1 ? v0 : NULL);
    }
}

void
mmUIView_SetBranchActive(
    struct mmUIView*                               p,
    const struct mmUIView*                         ancestor,
    mmUInt8_t                                      active)
{
    struct mmEventUIViewArgs hArgs;

    struct mmUIView* v = p;

    const char* name = NULL;

    if (active)
    {
        name = mmUIView_EventGainActive;
    }
    else
    {
        name = mmUIView_EventLossActive;
    }

    while (NULL != v && v != ancestor)
    {
        v->active = active;

        mmEventUIViewArgs_Assign(&hArgs, 0, v);
        mmEventSet_FireEvent(&v->events, name, &hArgs);

        v = v->parent;
    }
}

void
mmUIView_PrepareViewFromFile(
    struct mmUIView*                               p,
    const char*                                    path)
{
    do
    {
        struct mmUIViewLoader* pViewLoader;
        
        if (NULL == p->context)
        {
            break;
        }
        
        pViewLoader = p->context->vloader;
        if (NULL == pViewLoader)
        {
            break;
        }
        
        mmUIViewLoader_PrepareViewFromFile(
            pViewLoader,
            p,
            path);
    } while(0);
}

void
mmUIView_DiscardView(
    struct mmUIView*                               p)
{
    do
    {
        struct mmUIViewLoader* pViewLoader;
        
        if (NULL == p->context)
        {
            break;
        }
        
        pViewLoader = p->context->vloader;
        if (NULL == pViewLoader)
        {
            break;
        }
        
        mmUIViewLoader_DiscardView(
            pViewLoader,
            p);
    } while(0);
}

void
mmUIView_OnPrepare(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetOperable),
        func = mmOffsetof(struct mmUIWidgetOperable, OnPrepare),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnDiscard(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetOperable),
        func = mmOffsetof(struct mmUIWidgetOperable, OnDiscard),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnUpdate(
    struct mmUIView*                               p,
    struct mmEventUpdate*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetVariable),
        func = mmOffsetof(struct mmUIWidgetVariable, OnUpdate),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnInvalidate(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetVariable),
        func = mmOffsetof(struct mmUIWidgetVariable, OnInvalidate),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnSized(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetVariable),
        func = mmOffsetof(struct mmUIWidgetVariable, OnSized),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnChanged(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetVariable),
        func = mmOffsetof(struct mmUIWidgetVariable, OnChanged),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnDescriptorSetProduce(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnDescriptorSetProduce),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnDescriptorSetRecycle(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnDescriptorSetRecycle),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnDescriptorSetUpdate(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnDescriptorSetUpdate),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnUpdatePushConstants(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnUpdatePushConstants),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnUpdatePipeline(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnUpdatePipeline),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnRecord(
    struct mmUIView*                               p,
    void*                                          cmd)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, WidgetDrawable),
        func = mmOffsetof(struct mmUIWidgetDrawable, OnRecord),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, cmd);
}

void
mmUIView_OnKeypadPressed(
    struct mmUIView*                               p,
    struct mmEventKeypad*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderKeypad),
        func = mmOffsetof(struct mmUIResponderKeypad, OnKeypadPressed),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnKeypadRelease(
    struct mmUIView*                               p,
    struct mmEventKeypad*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderKeypad),
        func = mmOffsetof(struct mmUIResponderKeypad, OnKeypadRelease),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnKeypadStatus(
    struct mmUIView*                               p,
    struct mmEventKeypadStatus*                    content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderKeypad),
        func = mmOffsetof(struct mmUIResponderKeypad, OnKeypadStatus),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnCursorBegan(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderCursor),
        func = mmOffsetof(struct mmUIResponderCursor, OnCursorBegan),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnCursorMoved(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderCursor),
        func = mmOffsetof(struct mmUIResponderCursor, OnCursorMoved),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnCursorEnded(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderCursor),
        func = mmOffsetof(struct mmUIResponderCursor, OnCursorEnded),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnCursorBreak(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderCursor),
        func = mmOffsetof(struct mmUIResponderCursor, OnCursorBreak),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnCursorWheel(
    struct mmUIView*                               p,
    struct mmEventCursor*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderCursor),
        func = mmOffsetof(struct mmUIResponderCursor, OnCursorWheel),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnTouchsBegan(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderTouchs),
        func = mmOffsetof(struct mmUIResponderTouchs, OnTouchsBegan),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnTouchsMoved(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderTouchs),
        func = mmOffsetof(struct mmUIResponderTouchs, OnTouchsMoved),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnTouchsEnded(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderTouchs),
        func = mmOffsetof(struct mmUIResponderTouchs, OnTouchsEnded),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnTouchsBreak(
    struct mmUIView*                               p,
    struct mmEventTouchs*                          content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderTouchs),
        func = mmOffsetof(struct mmUIResponderTouchs, OnTouchsBreak),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnGetText(
    struct mmUIView*                               p,
    struct mmEventEditText*                        content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnGetText),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnSetText(
    struct mmUIView*                               p,
    struct mmEventEditText*                        content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnSetText),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnGetTextEditRect(
    struct mmUIView*                               p,
    double                                         rect[4])
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnGetTextEditRect),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, rect);
}

void
mmUIView_OnInsertTextUtf16(
    struct mmUIView*                               p,
    struct mmEventEditString*                      content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnInsertTextUtf16),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnReplaceTextUtf16(
    struct mmUIView*                               p,
    struct mmEventEditString*                      content)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnReplaceTextUtf16),
    };
    mmMetadataHandleArgs1(p, meta, elem, func, content);
}

void
mmUIView_OnInjectCopy(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnInjectCopy),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnInjectCut(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnInjectCut),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnInjectPaste(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnInjectPaste),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnFocusCapture(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnFocusCapture),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}

void
mmUIView_OnFocusRelease(
    struct mmUIView*                               p)
{
    enum
    {
        meta = mmOffsetof(struct mmUIView, metadata),
        elem = mmOffsetof(struct mmUIMetadata, ResponderEditor),
        func = mmOffsetof(struct mmUIResponderEditor, OnFocusRelease),
    };
    mmMetadataHandleArgs0(p, meta, elem, func);
}
