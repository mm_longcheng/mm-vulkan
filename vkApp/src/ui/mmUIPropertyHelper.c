/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIPropertyHelper.h"

#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"

void
mmPropertyHelper_SetUIDim3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    struct mmUIDim hArgs[3] = { { 0 } };
    static const int x[] = { 1, 2, 5, 6, 9, 10 };
    static const char s[] = "((,),(,),(,))";
    float a[6] = { 0 };
    mmValueStringFormatToTypeArray(v, a, sizeof(float), s, x, 6, &mmValue_AToFloat);
    hArgs[0].o = a[0]; hArgs[0].s = a[1];
    hArgs[1].o = a[2]; hArgs[1].s = a[3];
    hArgs[2].o = a[4]; hArgs[2].s = a[5];
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

void
mmPropertyHelper_GetUIDim3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    struct mmUIDim hArgs[3] = { { 0 } };
    static const int x[] = { 1, 2, 5, 6, 9, 10 };
    static const char s[] = "((,),(,),(,))";
    float a[6] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    a[0] = hArgs[0].o; a[1] = hArgs[0].s;
    a[2] = hArgs[1].o; a[3] = hArgs[1].s;
    a[4] = hArgs[2].o; a[5] = hArgs[2].s;
    mmValueTypeArrayToStringFormat(a, sizeof(float), v, s, x, 6, &mmValue_FloatToA);
}

void
mmPropertyHelper_SetUInt8Vec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    mmUInt8_t hArgs[2] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(mmUInt8_t), "(,)", 2, &mmValue_AToUInt8);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

void
mmPropertyHelper_GetUInt8Vec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    mmUInt8_t hArgs[2] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(mmUInt8_t), v, "(,)", 2, &mmValue_UInt8ToA);
}

void
mmPropertyHelper_SetUInt8Vec3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    mmUInt8_t hArgs[3] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(mmUInt8_t), "(,,)", 3, &mmValue_AToUInt8);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

void
mmPropertyHelper_GetUInt8Vec3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    mmUInt8_t hArgs[3] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(mmUInt8_t), v, "(,,)", 3, &mmValue_UInt8ToA);
}

void
mmPropertyHelper_SetSIntVec4(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    mmSInt_t hArgs[4] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(mmSInt_t), "(,,,)", 4, &mmValue_AToSInt);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

void
mmPropertyHelper_GetSIntVec4(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    mmSInt_t hArgs[4] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(mmSInt_t), v, "(,,,)", 4, &mmValue_SIntToA);
}

void
mmPropertyHelper_SetSizeVec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    mmSize_t hArgs[2] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(mmUInt8_t), "(,)", 2, &mmValue_AToSize);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

void
mmPropertyHelper_GetSizeVec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    mmSize_t hArgs[2] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(mmUInt8_t), v, "(,)", 2, &mmValue_SizeToA);
}
