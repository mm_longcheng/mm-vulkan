/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewButton_h__
#define __mmUIViewButton_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIImage.h"
#include "ui/mmUIButton.h"

#include "core/mmPrefix.h"

struct mmVKCommandBuffer;

struct mmEventCursor;

void
mmUIViewButton_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIViewButton_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorViewButton;
extern const struct mmUIMetadata mmUIMetadataViewButton;

struct mmUIViewButton
{
    struct mmUIView view;
    struct mmUIDrawable drawable;
    struct mmUIImage image;
    struct mmUIButton button;
};

void
mmUIViewButton_Init(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_Destroy(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_Produce(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_Recycle(
    struct mmUIViewButton*                         p);

int
mmUIViewButton_UpdateState(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_UpdateImage(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_UpdateSize(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnPrepare(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnDiscard(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnUpdate(
    struct mmUIViewButton*                         p,
    struct mmEventUpdate*                          content);

void
mmUIViewButton_OnInvalidate(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnSized(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnChanged(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnDescriptorSetProduce(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnDescriptorSetRecycle(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnDescriptorSetUpdate(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnUpdatePushConstants(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnUpdatePipeline(
    struct mmUIViewButton*                         p);

void
mmUIViewButton_OnRecord(
    struct mmUIViewButton*                         p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewButton_OnCursorBegan(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content);

void
mmUIViewButton_OnCursorMoved(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content);

void
mmUIViewButton_OnCursorEnded(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content);

void
mmUIViewButton_OnCursorBreak(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content);

void
mmUIViewButton_OnCursorWheel(
    struct mmUIViewButton*                         p,
    struct mmEventCursor*                          content);

#include "core/mmSuffix.h"

#endif//__mmUIViewButton_h__
