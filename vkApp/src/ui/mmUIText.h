/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIText_h__
#define __mmUIText_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "nwsi/mmTextLayout.h"

#include "ui/mmUICube.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKAssets.h"

#include "core/mmPrefix.h"

struct mmPropertySet;
struct mmUIView;
struct mmUIDrawable;

void
mmUIText_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIText_RemovePropertys(
    struct mmPropertySet*                          propertys);

struct mmTextColor
{
    struct mmString hValueForegroundColor;
    struct mmString hValueBackgroundColor;
    struct mmString hValueStrokeColor;
    struct mmString hValueUnderlineColor;
    struct mmString hValueStrikeThruColor;
    struct mmString hValueShadowColor;
    struct mmString hValueCaretColor;
    struct mmString hValueSelectionCaptureColor;
    struct mmString hValueSelectionReleaseColor;
};

void
mmTextColor_Init(
    struct mmTextColor*                            p);

void
mmTextColor_Destroy(
    struct mmTextColor*                            p);

// Text mmUIView Property Original value.
struct mmTextOriginal
{
    // [w, h, d]
    struct mmUIDim size[3];

    // anchor point. 
    // [x, y, z] normalise [0, 1].
    float anchor[3];

    // enum mmUIAlignment
    // Alignment, default is mmUIAlignmentMinimum.
    // [x, y, z]
    mmUInt8_t align[3];
};

void
mmTextOriginal_Init(
    struct mmTextOriginal*                         p);

void
mmTextOriginal_Destroy(
    struct mmTextOriginal*                         p);

int
mmTextOriginal_WidthIsZero(
    const struct mmTextOriginal*                   p);

void
mmTextOriginal_Storage(
    struct mmTextOriginal*                         p,
    const struct mmUIView*                         pView);

void
mmTextOriginal_Restore(
    const struct mmTextOriginal*                   p,
    struct mmUIView*                               pView);

struct mmUIText
{
    struct mmVKSamplerInfo* pSamplerInfo;
    struct mmVKImageInfo vImageInfo;
    struct mmVKBuffer vBuffer;
    struct mmTextLayout vLayout;
    struct mmTextColor vColor;
    struct mmTextOriginal vOriginal;
};

void
mmUIText_Init(
    struct mmUIText*                               p);

void
mmUIText_Destroy(
    struct mmUIText*                               p);

void
mmUIText_SetCaretIndex(
    struct mmUIText*                               p,
    size_t                                         hCaretIndex);

void
mmUIText_SetSelection(
    struct mmUIText*                               p,
    size_t                                         l,
    size_t                                         r);

void
mmUIText_SetCaretWrapping(
    struct mmUIText*                               p,
    int                                            hCaretWrapping);

void
mmUIText_SetCaretStatus(
    struct mmUIText*                               p,
    int                                            hCaretStatus);

void
mmUIText_SetSelectionStatus(
    struct mmUIText*                               p,
    int                                            hSelectionStatus);

void
mmUIText_ToggleCaretStatus(
    struct mmUIText*                               p);

VkResult
mmUIText_PrepareLayout(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

void
mmUIText_DiscardLayout(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIText_Prepare(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

void
mmUIText_Discard(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIText_UpdateTextContent(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIText_UpdateSizeContent(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIText_UpdateText(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

VkResult
mmUIText_UpdateSize(
    struct mmUIText*                               p,
    struct mmUIView*                               pView,
    struct mmUIDrawable*                           pDrawable);

void
mmUIText_UpdateColor(
    struct mmUIText*                               p,
    struct mmUIView*                               pView);

void
mmUIText_GetPointCaret(
    const struct mmUIText*                         p,
    struct mmUIView*                               pView,
    const float                                    point[2],
    mmUInt32_t*                                    pCaretIndex,
    int*                                           pCaretWrapping);

#include "core/mmSuffix.h"

#endif//__mmUIText_h__
