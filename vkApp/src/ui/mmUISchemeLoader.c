/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUISchemeLoader.h"

#include "ui/mmUIJSON.h"

#include "core/mmAlloc.h"
#include "core/mmByte.h"
#include "core/mmLogger.h"

#include "dish/mmFileContext.h"

void
mmUISchemeLoader_Init(
    struct mmUISchemeLoader*                       p)
{
    p->pGraphicsFactory = NULL;
    p->pFileContext = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    mmRbtreeStrVpt_Init(&p->names);
    mmListString_Init(&p->lists);
    mmParseJSON_Init(&p->parse);
}

void
mmUISchemeLoader_Destroy(
    struct mmUISchemeLoader*                       p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmParseJSON_Destroy(&p->parse);
    mmListString_Destroy(&p->lists);
    mmRbtreeStrVpt_Destroy(&p->names);
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pFileContext = NULL;
    p->pGraphicsFactory = NULL;
}

void
mmUISchemeLoader_SetGraphicsFactory(
    struct mmUISchemeLoader*                       p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->pGraphicsFactory = pGraphicsFactory;
}

void
mmUISchemeLoader_SetFileContext(
    struct mmUISchemeLoader*                       p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmUISchemeLoader_AddPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    mmListString_AddTail(&p->lists, &hWeakKey);
}

void
mmUISchemeLoader_RmvPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    mmListString_Remove(&p->lists, &hWeakKey);
}

void
mmUISchemeLoader_ClearPath(
    struct mmUISchemeLoader*                       p)
{
    mmListString_Clear(&p->lists);
}

void
mmUISchemeLoader_Prepare(
    struct mmUISchemeLoader*                       p)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListStringIterator* it = NULL;
    next = p->lists.l.next;
    while (next != &p->lists.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListStringIterator, n);
        mmUISchemeLoader_LoadFromPath(p, mmString_CStr(&it->v));
    }
}

void
mmUISchemeLoader_Discard(
    struct mmUISchemeLoader*                       p)
{
    struct mmListHead* prev = NULL;
    struct mmListHead* curr = NULL;
    struct mmListStringIterator* it = NULL;
    prev = p->lists.l.prev;
    while (prev != &p->lists.l)
    {
        curr = prev;
        prev = prev->prev;
        it = mmList_Entry(curr, struct mmListStringIterator, n);
        mmUISchemeLoader_UnloadByPath(p, mmString_CStr(&it->v));
    }
}

void
mmUISchemeLoader_UnloadByPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path)
{
    struct mmUIScheme* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmUIScheme*)it->v;
        mmRbtreeStrVpt_Rmv(&p->names, mmString_CStr(&s->hScheme.Name));
        mmUIScheme_Discard(s, p->pGraphicsFactory, p->pFileContext);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmUIScheme_Destroy(s);
        mmFree(s);
    }
}

void
mmUISchemeLoader_UnloadComplete(
    struct mmUISchemeLoader*                       p)
{
    struct mmUIScheme* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;

    mmRbtreeStrVpt_Clear(&p->names);

    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmUIScheme*)(it->v);
        mmUIScheme_Discard(s, p->pGraphicsFactory, p->pFileContext);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmUIScheme_Destroy(s);
        mmFree(s);
    }
}

struct mmUIScheme*
mmUISchemeLoader_LoadFromPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path)
{
    struct mmUIScheme* s = NULL;

    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmString hWeakKey;

        int i;

        const uint8_t* data = NULL;
        size_t size = 0;

        struct mmByteBuffer bytes;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakKey, path);
        it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
        if (NULL != it)
        {
            s = (struct mmUIScheme*)(it->v);
            break;
        }

        if (NULL == p->pGraphicsFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pGraphicsFactory is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        s = (struct mmUIScheme*)mmMalloc(sizeof(struct mmUIScheme));
        mmUIScheme_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, s);

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        mmParseJSON_Reset(&p->parse);
        mmParseJSON_SetJsonBuffer(&p->parse, (const char*)data, (size_t)size);
        mmParseJSON_Analysis(&p->parse);
        i = mmParseJSON_DecodeObject(&p->parse, 0, &s->hScheme, &mmParseJSON_DecodeUIScheme);
        if (i <= 0)
        {
            if (p->parse.code <= 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON AnalysisMetadata failure.", __FUNCTION__, __LINE__);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON root is invalid.", __FUNCTION__, __LINE__);
            }

            // do not break here.
        }
        else
        {
            mmUIScheme_SetPath(s, path);
            mmUIScheme_Prepare(s, p->pGraphicsFactory, p->pFileContext);
            mmRbtreeStrVpt_Set(&p->names, mmString_CStr(&s->hScheme.Name), s);
        }

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);

    return s;
}

struct mmUIScheme*
mmUISchemeLoader_GetFromPath(
    struct mmUISchemeLoader*                       p,
    const char*                                    path)
{
    struct mmUIScheme* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmUIScheme*)(it->v);
    }
    return s;
}

struct mmUIScheme*
mmUISchemeLoader_GetFromName(
    struct mmUISchemeLoader*                       p,
    const char*                                    name)
{
    struct mmUIScheme* s = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->names, name);
    if (NULL != it)
    {
        s = (struct mmUIScheme*)(it->v);
    }
    return s;
}
