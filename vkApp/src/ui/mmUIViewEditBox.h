/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewEditBox_h__
#define __mmUIViewEditBox_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIText.h"
#include "ui/mmUIEditBox.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKAssets.h"

#include "nwsi/mmTextLayout.h"

#include "core/mmPrefix.h"

struct mmVKAssets;
struct mmVKCommandBuffer;

struct mmEventEditText;
struct mmEventEditString;
struct mmEventKeypad;
struct mmEventCursor;
struct mmEventKeypadStatus;

void
mmUIViewEditBox_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIViewEditBox_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorViewEditBox;
extern const struct mmUIMetadata mmUIMetadataViewEditBox;

/* struct mmEventUIViewArgs */
extern const char* mmUIViewEditBox_EventTextChanged;
extern const char* mmUIViewEditBox_EventCaretChanged;
extern const char* mmUIViewEditBox_EventSelectionChanged;
extern const char* mmUIViewEditBox_EventTextAccepted;
extern const char* mmUIViewEditBox_EventFocusCapture;
extern const char* mmUIViewEditBox_EventFocusRelease;

struct mmUIViewEditBox
{
    struct mmUIView view;
    struct mmUIDrawable drawable;
    struct mmUIText text;
    struct mmUIEditBox editbox;
};

void
mmUIViewEditBox_Init(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_Destroy(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_Produce(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_Recycle(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_UpdateText(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_UpdateSize(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_HandleTextChanged(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnPrepare(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnDiscard(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnUpdate(
    struct mmUIViewEditBox*                        p,
    struct mmEventUpdate*                          content);

void
mmUIViewEditBox_OnInvalidate(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnSized(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnChanged(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnDescriptorSetProduce(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnDescriptorSetRecycle(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnDescriptorSetUpdate(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnUpdatePushConstants(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnUpdatePipeline(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnRecord(
    struct mmUIViewEditBox*                        p,
    struct mmVKCommandBuffer*                      cmd);

void
mmUIViewEditBox_OnKeypadPressed(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypad*                          content);

void
mmUIViewEditBox_OnKeypadRelease(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypad*                          content);

void
mmUIViewEditBox_OnKeypadStatus(
    struct mmUIViewEditBox*                        p,
    struct mmEventKeypadStatus*                    content);

void
mmUIViewEditBox_OnCursorBegan(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewEditBox_OnCursorMoved(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewEditBox_OnCursorEnded(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewEditBox_OnCursorBreak(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewEditBox_OnCursorWheel(
    struct mmUIViewEditBox*                        p,
    struct mmEventCursor*                          content);

void
mmUIViewEditBox_OnGetText(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditText*                        content);

void
mmUIViewEditBox_OnSetText(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditText*                        content);

void
mmUIViewEditBox_OnGetTextEditRect(
    struct mmUIViewEditBox*                        p,
    double                                         rect[4]);

void
mmUIViewEditBox_OnInsertTextUtf16(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditString*                      content);

void
mmUIViewEditBox_OnReplaceTextUtf16(
    struct mmUIViewEditBox*                        p,
    struct mmEventEditString*                      content);

void
mmUIViewEditBox_OnInjectCopy(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnInjectCut(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnInjectPaste(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnFocusCapture(
    struct mmUIViewEditBox*                        p);

void
mmUIViewEditBox_OnFocusRelease(
    struct mmUIViewEditBox*                        p);

#include "core/mmSuffix.h"

#endif//__mmUIViewEditBox_h__
