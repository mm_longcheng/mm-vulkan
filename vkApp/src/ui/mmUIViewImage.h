/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewImage_h__
#define __mmUIViewImage_h__

#include "core/mmCore.h"

#include "ui/mmUIView.h"
#include "ui/mmUIDrawable.h"
#include "ui/mmUIImage.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKAssets.h"

#include "core/mmPrefix.h"

struct mmVKAssets;
struct mmVKImageInfo;
struct mmVKCommandBuffer;

void
mmUIViewImage_DefinePropertys(
    struct mmPropertySet*                          propertys,
    size_t                                         offset);

void
mmUIViewImage_RemovePropertys(
    struct mmPropertySet*                          propertys);

extern const struct mmMetaAllocator mmUIMetaAllocatorViewImage;
extern const struct mmUIMetadata mmUIMetadataViewImage;

struct mmUIViewImage
{
    struct mmUIView view;
    struct mmUIDrawable drawable;
    struct mmUIImage image;
};

void
mmUIViewImage_Init(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_Destroy(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_Produce(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_Recycle(
    struct mmUIViewImage*                          p);

VkResult
mmUIViewImage_PrepareFromImageInfo(
    struct mmUIViewImage*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmVKImageInfo*                          pImageInfo,
    const int                                      frame[4],
    int                                            rotated);

VkResult
mmUIViewImage_PrepareFromSheetName(
    struct mmUIViewImage*                          p);

VkResult
mmUIViewImage_PrepareFromImagePath(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_UpdateImage(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_UpdateSize(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnPrepare(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnDiscard(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnUpdate(
    struct mmUIViewImage*                          p,
    struct mmEventUpdate*                          content);

void
mmUIViewImage_OnInvalidate(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnSized(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnChanged(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnDescriptorSetProduce(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnDescriptorSetRecycle(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnDescriptorSetUpdate(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnUpdatePushConstants(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnUpdatePipeline(
    struct mmUIViewImage*                          p);

void
mmUIViewImage_OnRecord(
    struct mmUIViewImage*                          p,
    struct mmVKCommandBuffer*                      cmd);

#include "core/mmSuffix.h"

#endif//__mmUIViewImage_h__
