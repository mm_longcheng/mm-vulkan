/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewFactory.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmUIViewFactory_Init(
    struct mmUIViewFactory*                        p)
{
    mmRbtreeStrVpt_Init(&p->types);
}

void
mmUIViewFactory_Destroy(
    struct mmUIViewFactory*                        p)
{
    mmRbtreeStrVpt_Destroy(&p->types);
}

void
mmUIViewFactory_AddType(
    struct mmUIViewFactory*                        p,
    const struct mmUIMetadata*                     m)
{
    assert(NULL != m->Allocator && "metadata Allocator is invalid.");
    mmRbtreeStrVpt_Set(&p->types, m->Allocator->TypeName, (void*)m);
}

void
mmUIViewFactory_RmvType(
    struct mmUIViewFactory*                        p,
    const struct mmUIMetadata*                     m)
{
    assert(NULL != m->Allocator && "metadata Allocator is invalid.");
    mmRbtreeStrVpt_Rmv(&p->types, m->Allocator->TypeName);
}

const struct mmUIMetadata*
mmUIViewFactory_GetType(
    struct mmUIViewFactory*                        p,
    const char*                                    type)
{
    const struct mmUIMetadata* m = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->types, type);
    if (NULL != it)
    {
        m = (struct mmUIMetadata*)it->v;
    }
    return m;
}

struct mmUIView*
mmUIViewFactory_Produce(
    struct mmUIViewFactory*                        p,
    const char*                                    type)
{
    struct mmUIView* v = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->types, type);
    if (NULL != it)
    {
        typedef void(*ProduceFuncType)(void* e);
        const struct mmUIMetadata* metadata = (struct mmUIMetadata*)it->v;
        const struct mmMetaAllocator* Allocator = metadata->Allocator;
        ProduceFuncType Produce = (ProduceFuncType)Allocator->Produce;
        v = (struct mmUIView*)mmMalloc(Allocator->TypeSize);
        (*Produce)(v);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d type: %s is not exist.", __FUNCTION__, __LINE__, type);
    }
    return v;
}

void
mmUIViewFactory_Recycle(
    struct mmUIViewFactory*                        p,
    struct mmUIView*                               v)
{
    const char* type = "";
    const struct mmUIMetadata* metadata = NULL;
    const struct mmMetaAllocator* Allocator = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    assert(NULL != v && "view is invalid.");
    metadata = v->metadata;
    assert(NULL != metadata && "metadata is invalid.");
    Allocator = metadata->Allocator;
    assert(NULL != Allocator && "Allocator is invalid.");
    type = Allocator->TypeName;
    it = mmRbtreeStrVpt_GetIterator(&p->types, type);
    if (NULL != it)
    {
        typedef void(*RecycleFuncType)(void* e);
        const struct mmUIMetadata* metadata = (struct mmUIMetadata*)it->v;
        const struct mmMetaAllocator* Allocator = metadata->Allocator;
        RecycleFuncType Recycle = (RecycleFuncType)Allocator->Recycle;
        (*Recycle)(v);
        mmFree(v);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d type: %s is not exist.", __FUNCTION__, __LINE__, type);
    }
}

