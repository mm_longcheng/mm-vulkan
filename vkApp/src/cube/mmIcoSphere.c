/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIcoSphere.h"

#include "container/mmRbtreeF64.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"

void
mmIcoSphere_ClearArrays(
    struct mmIcoSphere*                            p)
{
    mmVectorU32_Reset(&p->vIdxs);
    mmVectorF32_Reset(&p->vVtxs);
}

/*
 * compute 12 vertices of icosahedron using spherical coordinates
 * The north pole is at (0, 0, r) and the south pole is at (0,0,-r).
 * 5 vertices are placed by rotating 72 deg at elevation 26.57 deg (=atan(1/2))
 * 5 vertices are placed by rotating 72 deg at elevation -26.57 deg
 *
 * right-hand +y up coordinate space.
 *
 */
void
mmIcoSphere_ComputeIcosahedronVertices(
    float                                          radius,
    float                                          vertices[36])
{
    // vertices is 12 vertices 12 * 3.
    
    const float PI = (float)MM_PI;
    const float H_ANGLE = PI / 180.0f * 72.0f;   // 72 degree = 360 / 5
    const float V_ANGLE = atanf(1.0f / 2.0f);    // elevation = 26.565 degree

    int i;
    int i1, i2;                                  // indices
    float y, k;                                  // coords
    float hAngle1 = -PI / 2.0f - H_ANGLE / 2.0f; // start from -126 deg at 2nd row
    float hAngle2 = -PI / 2.0f;                  // start from -90 deg at 3rd row

    // the first top vertex (0, 0, r)
    vertices[0] = 0;
    vertices[1] = +radius;
    vertices[2] = 0;

    // 10 vertices at 2nd and 3rd rows
    for(i = 1; i <= 5; ++i)
    {
        i1 = (i    ) * 3;   // for 2nd row
        i2 = (i + 5) * 3;   // for 3rd row
        
        y = radius * sinf(V_ANGLE);              // elevaton
        k = radius * cosf(V_ANGLE);
        
        vertices[i1    ] = +k * cosf(hAngle1);   // x
        vertices[i2    ] = +k * cosf(hAngle2);
        vertices[i1 + 1] = +y;                   // y
        vertices[i2 + 1] = -y;
        vertices[i1 + 2] = -k * sinf(hAngle1);   // z
        vertices[i2 + 2] = -k * sinf(hAngle2);

        // next horizontal angles
        hAngle1 += H_ANGLE;
        hAngle2 += H_ANGLE;
    }

    // the last bottom vertex (0, 0, -r)
    vertices[33] = 0;
    vertices[34] = -radius;
    vertices[35] = 0;
}

/*
 * find middle point of 2 vertices
 * NOTE: new vertex must be resized, so the length is equal to the given length
 */
void
mmIcoSphere_ComputeHalfPosition(
    const float                                    v1[3],
    const float                                    v2[3],
    float                                          length,
    float                                          v[3])
{
    float k;
    mmVec3Add(v, v1, v2);
    k = length / mmVec3Length(v);
    mmVec3Scale(v, v, k);
}

/*
 * find middle texcoords of 2 tex coords and return new coord (3rd param)
*/
void
mmIcoSphere_ComputeHalfTexCoord(
    const float                                    t1[2],
    const float                                    t2[2],
    float                                          t[2])
{
    t[0] = (t1[0] + t2[0]) * 0.5f;
    t[1] = (t1[1] + t2[1]) * 0.5f;
}

/*
 * determine a point c is on the line segment a-b
*/
int
mmIcoSphere_IsOnLineSegment(
    const float                                    a[2],
    const float                                    b[2],
    const float                                    c[2])
{
    static const float EPSILON = 0.0001f;

    // cross product must be 0 if c is on the line
    float cross = ((b[0] - a[0]) * (c[1] - a[1])) - ((b[1] - a[1]) * (c[0] - a[0]));
    if(cross > EPSILON || cross < -EPSILON)
    {
        return MM_FALSE;
    }
    
    // c must be within a-b
    if((c[0] > a[0] && c[0] > b[0]) || (c[0] < a[0] && c[0] < b[0]))
    {
        return MM_FALSE;
    }
    
    if((c[1] > a[1] && c[1] > b[1]) || (c[1] < a[1] && c[1] < b[1]))
    {
        return MM_FALSE;
    }

    // all passed, it is on the line segment
    return MM_TRUE;
}

/*
 * This function used 20 non-shared line segments to determine if the given
 * texture coordinate is shared or no. If it is on the line segments, it is also
 * non-shared point
 *   00  01  02  03  04         //
 *   /\  /\  /\  /\  /\         //
 *  /  \/  \/  \/  \/  \        //
 * 05  06  07  08  09   \       //
 *   \   10  11  12  13  14     //
 *    \  /\  /\  /\  /\  /      //
 *     \/  \/  \/  \/  \/       //
 *      15  16  17  18  19      //
*/
int
mmIcoSphere_IsSharedTexCoord(
    const float                                    t[2])
{
    // 20 non-shared line segments
    // S = 186 / 2048.0f horizontal texture step
    // T = 322 / 1024.0f vertical   texture step
#define S 0.0908203125f
#define T 0.3144531250f 

    // static const float S = 186 / 2048.0f;     // horizontal texture step
    // static const float T = 322 / 1024.0f;     // vertical texture step
    static const float segments[] =
    {
        S  ,   0,   0, T  ,     // 00 - 05
        S  ,   0, S*2, T  ,     // 00 - 06
        S*3,   0, S*2, T  ,     // 01 - 06
        S*3,   0, S*4, T  ,     // 01 - 07
        S*5,   0, S*4, T  ,     // 02 - 07
        S*5,   0, S*6, T  ,     // 02 - 08
        S*7,   0, S*6, T  ,     // 03 - 08
        S*7,   0, S*8, T  ,     // 03 - 09
        S*9,   0, S*8, T  ,     // 04 - 09
        S*9,   0,   1, T*2,     // 04 - 14
          0, T  , S*2,   1,     // 05 - 15
        S*3, T*2, S*2,   1,     // 10 - 15
        S*3, T*2, S*4,   1,     // 10 - 16
        S*5, T*2, S*4,   1,     // 11 - 16
        S*5, T*2, S*6,   1,     // 11 - 17
        S*7, T*2, S*6,   1,     // 12 - 17
        S*7, T*2, S*8,   1,     // 12 - 18
        S*9, T*2, S*8,   1,     // 13 - 18
        S*9, T*2, S*10,  1,     // 13 - 19
          1, T*2, S*10,  1,     // 14 - 19
    };

#undef S
#undef T

    int i, j;
    // check the point with all 20 line segments
    // if it is on the line segment, it is non-shared
    int count = (int)(sizeof(segments) / sizeof(segments[0]));
    for(i = 0, j = 2; i < count; i+=4, j+=4)
    {
        if(mmIcoSphere_IsOnLineSegment(&segments[i], &segments[j], t))
        {
            // not shared
            return MM_FALSE;
        }
    }
    return MM_TRUE;
}

/*
 * add a subdivided vertex attribs (vertex, normal, texCoord) to arrays, then
 * return its index value
 * If it is a shared vertex, remember its index, so it can be re-used
 */
uint32_t
mmIcoSphere_AddSubVertexAttribs(
    struct mmIcoSphere*                            p,
    const float                                    v[3],
    const float                                    n[3],
    const float                                    t[2],
    uint32_t*                                      vid,
    struct mmRbtreeF64U32*                         sIdxs)
{
    uint32_t index;

    // check if is shared vertex or not first
    if(mmIcoSphere_IsSharedTexCoord(t))
    {
        // find if it does already exist in sharedIndices map using (s,t) key
        // if not in the list, add the vertex attribs to arrays and return its index
        // if exists, return the current index
        mmFloat64_t k;
        struct mmRbtreeF64U32Iterator* it;
        memcpy(&k, t, sizeof(mmFloat64_t));
        it = mmRbtreeF64U32_GetIterator(sIdxs, k);
        if (NULL == it)
        {
            float* vtx;
            
            vtx = (float*)p->vVtxs.arrays + (*vid) * 8;
            
            (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
            (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
            (*vtx++) = t[0]; (*vtx++) = t[1];

            index = (*vid);
            mmRbtreeF64U32_Set(sIdxs, k, index);
            (*vid)++;
        }
        else
        {
            index = it->v;
        }
    }
    else
    {
        // not shared
        float* vtx;
        
        vtx = (float*)p->vVtxs.arrays + (*vid) * 8;
        
        (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
        (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
        (*vtx++) = t[0]; (*vtx++) = t[1];
        
        index = (*vid);
        (*vid)++;
    }

    return index;
}

/*
 * divide a trianlge (v1-v2-v3) into 4 sub triangles by adding middle vertices
 * (newV1, newV2, newV3) and repeat N times
 * If subdivision=0, do nothing.
 *         v1           //
 *        / \           //
 * newV1 *---* newV3    //
 *      / \ / \         //
 *    v2---*---v3       //
 *        newV2         //
 */
void
mmIcoSphere_SubdivideVerticesSmooth(
    struct mmIcoSphere*                            p,
    struct mmRbtreeF64U32*                         sIdxs)
{
    float radius = p->radius;
    int subdivision = p->subdivision;
    
    unsigned int i1, i2, i3;            // indices from original triangle
    const float *v1, *v2, *v3;          // ptr to original vertices of a triangle
    const float *t1, *t2, *t3;          // ptr to original texcoords of a triangle
    float newV1[3], newV2[3], newV3[3]; // new subdivided vertex positions
    float newN1[3], newN2[3], newN3[3]; // new subdivided normals
    float newT1[2], newT2[2], newT3[2]; // new subdivided texture coords
    unsigned int newI1, newI2, newI3;   // new subdivided indices
    int i, j;
    
    struct mmVectorU32 tIdxs;
    size_t idxCount;
    size_t vtxCount;
    
    float* vtx;
    uint32_t* idx;
    uint32_t* udx;
    uint32_t vid;
    
    size_t count;
    
    // regular icosahedron topology properties.
    size_t lastTriangleCount = 20;
    size_t lastSharedEdgeCount = 19;
    size_t lastEdgeCount = 41;
    
    size_t thisTriangleCount;
    size_t thisSharedEdgeCount;
    size_t thisEdgeCount;
    
    mmVectorU32_Init(&tIdxs);

    // iteration for subdivision
    for(i = 1; i <= subdivision; ++i)
    {
        // copy prev indices
        mmVectorU32_CopyFromValue(&tIdxs, &p->vIdxs);

        idxCount = mmVectorU32_Size(&p->vIdxs);
        vtxCount = mmVectorF32_Size(&p->vVtxs);
        
        // clear prev arrays
        mmVectorU32_Clear(&p->vIdxs);
        
        vid = (uint32_t)mmVectorF32_Size(&p->vVtxs) / 8;
        
        thisTriangleCount = lastTriangleCount * 4;
        thisSharedEdgeCount = lastSharedEdgeCount * 2 + lastTriangleCount * 3;
        thisEdgeCount = thisTriangleCount * 3 - thisSharedEdgeCount;

        count = (vtxCount / 8) + lastEdgeCount;
        mmVectorF32_AllocatorMemory(&p->vVtxs, count * 8);
        vtx = (float*)p->vVtxs.arrays;
        p->vVtxs.size = count * 8;
        
        count = (idxCount / 3) * 4;
        mmVectorU32_AllocatorMemory(&p->vIdxs, count * 3);
        idx = (uint32_t*)p->vIdxs.arrays;
        p->vIdxs.size = count * 3;
        
        lastTriangleCount = thisTriangleCount;
        lastSharedEdgeCount = thisSharedEdgeCount;
        lastEdgeCount = thisEdgeCount;
        
        udx = (uint32_t*)tIdxs.arrays;

        for(j = 0; j < idxCount; j += 3)
        {
            // get 3 indices of each triangle
            i1 = udx[j  ];
            i2 = udx[j+1];
            i3 = udx[j+2];

            // get 3 vertex attribs from prev triangle
            // position
            v1 = &vtx[i1 * 8    ];
            v2 = &vtx[i2 * 8    ];
            v3 = &vtx[i3 * 8    ];
            // texcoord
            t1 = &vtx[i1 * 8 + 6];
            t2 = &vtx[i2 * 8 + 6];
            t3 = &vtx[i3 * 8 + 6];

            // get 3 new vertex attribs by spliting half on each edge
            mmIcoSphere_ComputeHalfPosition(v1, v2, radius, newV1);
            mmIcoSphere_ComputeHalfPosition(v2, v3, radius, newV2);
            mmIcoSphere_ComputeHalfPosition(v1, v3, radius, newV3);
            mmIcoSphere_ComputeHalfTexCoord(t1, t2, newT1);
            mmIcoSphere_ComputeHalfTexCoord(t2, t3, newT2);
            mmIcoSphere_ComputeHalfTexCoord(t1, t3, newT3);
            mmVec3Normalize(newN1, newV1);
            mmVec3Normalize(newN2, newV2);
            mmVec3Normalize(newN3, newV3);

            // add new vertices/normals/texcoords to arrays
            // It will check if it is shared/non-shared and return index
            newI1 = mmIcoSphere_AddSubVertexAttribs(p, newV1, newN1, newT1, &vid, sIdxs);
            newI2 = mmIcoSphere_AddSubVertexAttribs(p, newV2, newN2, newT2, &vid, sIdxs);
            newI3 = mmIcoSphere_AddSubVertexAttribs(p, newV3, newN3, newT3, &vid, sIdxs);

            // add 4 new triangle indices
            (*idx++) =    i1; (*idx++) = newI1; (*idx++) = newI3;
            (*idx++) = newI1; (*idx++) =    i2; (*idx++) = newI2;
            (*idx++) = newI3; (*idx++) = newI2; (*idx++) =    i3;
            (*idx++) = newI1; (*idx++) = newI2; (*idx++) = newI3;
        }
    }
    
    mmVectorU32_Destroy(&tIdxs);
}

void
mmIcoSphere_Init(
    struct mmIcoSphere*                            p)
{
    mmVectorU32_Init(&p->vIdxs);
    mmVectorF32_Init(&p->vVtxs);
    
    p->radius = 1.0f;
    p->subdivision = 0;
    p->smooth = MM_TRUE;
}

void
mmIcoSphere_Destroy(
    struct mmIcoSphere*                            p)
{
    p->smooth = MM_TRUE;
    p->subdivision = 0;
    p->radius = 1.0f;
    
    mmVectorF32_Destroy(&p->vVtxs);
    mmVectorU32_Destroy(&p->vIdxs);
}

int
mmIcoSphere_Prepare(
    struct mmIcoSphere*                            p,
    float                                          radius,
    int                                            subdivision)
{
    p->radius = radius;
    p->subdivision = subdivision;
    mmIcoSphere_BuildVerticesSmooth(p);
    return 0;
}

void
mmIcoSphere_Discard(
    struct mmIcoSphere*                            p)
{
    mmIcoSphere_ClearArrays(p);
}

/*
 * build vertices of sphere with smooth shading using parametric equation
 *
 * right-hand +y up coordinate space.
 *
 *     x = +r * cos(u) * cos(v)
 *     y = +r * sin(u)
 *     z = -r * cos(u) * sin(v)
 *
 * where u: stacks( latitude) angle (-90 <= u <=  90)
 *       v: slices(longitude) angle (  0 <= v <= 360)
 */
void
mmIcoSphere_BuildVerticesSmooth(
    struct mmIcoSphere*                            p)
{
    static const float S_STEP = 186 / 2048.0f;     // horizontal texture step
    static const float T_STEP = 322 / 1024.0f;     // vertical texture step

    // compute 12 vertices of icosahedron
    // NOTE: v0 (top), v11(bottom), v1, v6(first vert on each row) cannot be
    // shared for smooth shading (they have different texcoords)
    float vPos[36] = { 0 };

    float v[3];                             // vertex
    float n[3];                             // normal
    
    float* vtx;
    size_t count;
    
    uint32_t* idx;
    
    int i;
    
    mmFloat64_t k;
    
    struct mmRbtreeF64U32 sIdxs;
    
    mmRbtreeF64U32_Init(&sIdxs);
    
    // clear memory of prev arrays
    mmVectorU32_Reset(&p->vIdxs);
    mmVectorF32_Reset(&p->vVtxs);
    
    count = 22;
    mmVectorF32_AllocatorMemory(&p->vVtxs, count * 8);
    vtx = (float*)p->vVtxs.arrays;
    p->vVtxs.size = count * 8;
    
    // suture coincide, one quad two triangle.
    count = 20;
    mmVectorU32_AllocatorMemory(&p->vIdxs, count * 3);
    idx = (uint32_t*)p->vIdxs.arrays;
    p->vIdxs.size = count * 3;
    
    mmIcoSphere_ComputeIcosahedronVertices(p->radius, vPos);

    // smooth icosahedron has 14 non-shared (0 to 13) and
    // 8 shared vertices (14 to 21) (total 22 vertices)
    //   00  01  02  03  04          //
    //   /\  /\  /\  /\  /\          //
    //  /  \/  \/  \/  \/  \         //
    // 10--14--15--16--17--11        //
    //  \  /\  /\  /\  /\  /\        //
    //   \/  \/  \/  \/  \/  \       //
    //   12--18--19--20--21--13      //
    //    \  /\  /\  /\  /\  /       //
    //     \/  \/  \/  \/  \/        //
    //     05  06  07  08  09        //
    // add 14 non-shared vertices first (index from 0 to 13)
    
    // (top   ) v0 v1 v2 v3 v4
    for (i = 0; i < 5; ++i)
    {
        (*vtx++) = vPos[0]; (*vtx++) = vPos[1]; (*vtx++) = vPos[2];
        (*vtx++) = 0.0f; (*vtx++) = 0.0f; (*vtx++) = +1.0f;
        (*vtx++) = S_STEP * ((i * 2) + 1); (*vtx++) = 0.0f;
    }

    // (bottom) v5 v6 v7 v8 v9
    for (i = 0; i < 5; ++i)
    {
        (*vtx++) = vPos[33]; (*vtx++) = vPos[34]; (*vtx++) = vPos[35];
        (*vtx++) = 0.0f; (*vtx++) = 0.0f; (*vtx++) = -1.0f;
        (*vtx++) = S_STEP * ((i + 1) * 2); (*vtx++) = T_STEP * 3;
    }
    
    v[0] = vPos[3];  v[1] = vPos[4];  v[2] = vPos[5];
    mmVec3Normalize(n, v);
    // v10 (left)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = 0.0f; (*vtx++) = T_STEP;
    // v11 (right)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 10; (*vtx++) = T_STEP;

    v[0] = vPos[18];  v[1] = vPos[19];  v[2] = vPos[20];
    mmVec3Normalize(n, v);
    // v12 (left)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP; (*vtx++) = T_STEP * 2;
    // v13 (right)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 11; (*vtx++) = T_STEP * 2;
    
    // add 8 shared vertices to array (index from 14 to 21)
    v[0] = vPos[6];  v[1] = vPos[7];  v[2] = vPos[8];
    mmVec3Normalize(n, v);
    // v14 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 2; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 14);

    v[0] = vPos[9];  v[1] = vPos[10];  v[2] = vPos[11];
    mmVec3Normalize(n, v);
    // v15 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 4; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 15);

    v[0] = vPos[12];  v[1] = vPos[13];  v[2] = vPos[14];
    mmVec3Normalize(n, v);
    // v16 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 6; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 16);

    v[0] = vPos[15];  v[1] = vPos[16];  v[2] = vPos[17];
    mmVec3Normalize(n, v);
    // v17 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 8; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 17);
    
    v[0] = vPos[21];  v[1] = vPos[22];  v[2] = vPos[23];
    mmVec3Normalize(n, v);
    // v18 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 3; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 18);

    v[0] = vPos[24];  v[1] = vPos[25];  v[2] = vPos[26];
    mmVec3Normalize(n, v);
    // v19 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 5; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 19);

    v[0] = vPos[27];  v[1] = vPos[28];  v[2] = vPos[29];
    mmVec3Normalize(n, v);
    // v20 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 7; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 20);

    v[0] = vPos[30];  v[1] = vPos[31];  v[2] = vPos[32];
    mmVec3Normalize(n, v);
    // v20 (shared)
    (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
    (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
    (*vtx++) = S_STEP * 9; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 21);

    // build index list for icosahedron (20 triangles)
    // 1st row (5 tris)
    (*idx++) =  0; (*idx++) = 10; (*idx++) = 14;
    (*idx++) =  1; (*idx++) = 14; (*idx++) = 15;
    (*idx++) =  2; (*idx++) = 15; (*idx++) = 16;
    (*idx++) =  3; (*idx++) = 16; (*idx++) = 17;
    (*idx++) =  4; (*idx++) = 17; (*idx++) = 11;
    // 2nd row (10 tris)
    (*idx++) = 10; (*idx++) = 12; (*idx++) = 14;
    (*idx++) = 12; (*idx++) = 18; (*idx++) = 14;
    (*idx++) = 14; (*idx++) = 18; (*idx++) = 15;
    (*idx++) = 18; (*idx++) = 19; (*idx++) = 15;
    (*idx++) = 15; (*idx++) = 19; (*idx++) = 16;
    (*idx++) = 19; (*idx++) = 20; (*idx++) = 16;
    (*idx++) = 16; (*idx++) = 20; (*idx++) = 17;
    (*idx++) = 20; (*idx++) = 21; (*idx++) = 17;
    (*idx++) = 17; (*idx++) = 21; (*idx++) = 11;
    (*idx++) = 21; (*idx++) = 13; (*idx++) = 11;
    // 3rd row (5 tris)
    (*idx++) =  5; (*idx++) = 18; (*idx++) = 12;
    (*idx++) =  6; (*idx++) = 19; (*idx++) = 18;
    (*idx++) =  7; (*idx++) = 20; (*idx++) = 19;
    (*idx++) =  8; (*idx++) = 21; (*idx++) = 20;
    (*idx++) =  9; (*idx++) = 13; (*idx++) = 21;

    // subdivide icosahedron
    mmIcoSphere_SubdivideVerticesSmooth(p, &sIdxs);
    
    mmRbtreeF64U32_Destroy(&sIdxs);
}

size_t
mmIcoSphere_GetIdxCount(
    const struct mmIcoSphere*                      p)
{
    return (p->vIdxs.size);
}

size_t
mmIcoSphere_GetVtxCount(
    const struct mmIcoSphere*                      p)
{
    return (p->vVtxs.size / 8);
}

size_t
mmIcoSphere_GetIdxBufferSize(
    const struct mmIcoSphere*                      p)
{
    return p->vIdxs.size * sizeof(uint32_t);
}

size_t
mmIcoSphere_GetVtxBufferSize(
    const struct mmIcoSphere*                      p)
{
    return p->vVtxs.size * sizeof(float);
}

const uint8_t*
mmIcoSphere_GetIdxBuffer(
    const struct mmIcoSphere*                      p)
{
    return (const uint8_t*)p->vIdxs.arrays;
}

const uint8_t*
mmIcoSphere_GetVtxBuffer(
    const struct mmIcoSphere*                      p)
{
    return (const uint8_t*)p->vVtxs.arrays;
}
