/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUVSphere_h__
#define __mmUVSphere_h__

#include "core/mmCore.h"

#include "container/mmVectorF32.h"
#include "container/mmVectorU32.h"

#include "core/mmPrefix.h"

#define MM_UVSPHERE_MIN_SLICES 3
#define MM_UVSPHERE_MIN_STACKS 2

struct mmUVSphere;

void
mmUVSphere_ClearArrays(
    struct mmUVSphere*                             p);

struct mmUVSphere
{
    /* Vertex
     *     float position[3];
     *     float normal[3];
     *     float texcoord[2];
     */
    struct mmVectorU32 vIdxs;
    struct mmVectorF32 vVtxs;
    
    // radius for sphere
    // default 1.0f.
    float radius;
    // longitude, # of slices.
    // default 36
    int slices;
    // latitude,  # of stacks.
    // default 18
    int stacks;
    // smooth flag.
    // default MM_TRUE.
    int smooth;
};

void
mmUVSphere_Init(
    struct mmUVSphere*                             p);

void
mmUVSphere_Destroy(
    struct mmUVSphere*                             p);

int
mmUVSphere_Prepare(
    struct mmUVSphere*                             p,
    float                                          radius,
    int                                            slices,
    int                                            stacks);

void
mmUVSphere_Discard(
    struct mmUVSphere*                             p);

void
mmUVSphere_BuildVerticesSmooth(
    struct mmUVSphere*                             p);

size_t
mmUVSphere_GetIdxCount(
    const struct mmUVSphere*                       p);

size_t
mmUVSphere_GetVtxCount(
    const struct mmUVSphere*                       p);

size_t
mmUVSphere_GetIdxBufferSize(
    const struct mmUVSphere*                       p);

size_t
mmUVSphere_GetVtxBufferSize(
    const struct mmUVSphere*                       p);

const uint8_t*
mmUVSphere_GetIdxBuffer(
    const struct mmUVSphere*                       p);

const uint8_t*
mmUVSphere_GetVtxBuffer(
    const struct mmUVSphere*                       p);

#include "core/mmSuffix.h"

#endif//__mmUVSphere_h__
