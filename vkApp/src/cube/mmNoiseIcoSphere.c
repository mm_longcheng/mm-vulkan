/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNoiseIcoSphere.h"

#include "cube/mmNoise3DImage.h"
#include "cube/mmIcoSphere.h"

#include "container/mmRbtreeF64.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"

#include "vk/mmVKImageUploader.h"

float
mmNoiseIcoSphere_RandomRadius(
    const struct mmNoiseIcoSphere*                 p,
    const float                                    position[3],
    float                                          normal[3])
{
    struct mmNoise3DImage* pNoise3DImage = p->pNoise3DImage;
    float height = p->height;
    float offset = p->offset;
    int detail = p->detail;
    
    float v[3];
    float d[3];
    
    float f0, fx, fy, fz;
    
    assert(NULL != pNoise3DImage && "pNoise3DImage is invalid");

    /* Calculate the normal resulting from the bump surface */
    mmVec3Assign(v, position);
    f0 = mmNoise3DImage_FBMNoise(pNoise3DImage, v, detail) * height;

    mmVec3Make(v, offset, 0.0f, 0.0f);
    mmVec3Add(v, v, position);
    fx = mmNoise3DImage_FBMNoise(pNoise3DImage, v, detail) * height;

    mmVec3Make(v, 0.0f, offset, 0.0f);
    mmVec3Add(v, v, position);
    fy = mmNoise3DImage_FBMNoise(pNoise3DImage, v, detail) * height;

    mmVec3Make(v, 0.0f, 0.0f, offset);
    mmVec3Add(v, v, position);
    fz = mmNoise3DImage_FBMNoise(pNoise3DImage, v, detail) * height;

    d[0] = (fx - f0) / offset;
    d[1] = (fy - f0) / offset;
    d[2] = (fz - f0) / offset;

    mmVec3Normalize(normal, position);
    mmVec3Sub(v, normal, d);
    mmVec3Normalize(normal, v);

    return p->radius * (1.0f + f0);
}

void
mmNoiseIcoSphere_ClearArrays(
    struct mmNoiseIcoSphere*                       p)
{
    mmVectorU32_Reset(&p->vIdxs);
    mmVectorF32_Reset(&p->vVtxs);
}

void
mmNoiseIcoSphere_ComputeIcosahedronVertices(
    const struct mmNoiseIcoSphere*                 p,
    float                                          radius,
    float                                          v[36],
    float                                          n[36])
{
    float r;
    
    // vertices is 12 v 12 * 3, n 12 * 3.
    
    const float PI = (float)MM_PI;
    const float H_ANGLE = PI / 180.0f * 72.0f;   // 72 degree = 360 / 5
    const float V_ANGLE = atanf(1.0f / 2.0f);    // elevation = 26.565 degree

    int i;
    int i1, i2;                                  // indices
    float y, k;                                  // coords
    float hAngle1 = -PI / 2.0f - H_ANGLE / 2.0f; // start from -126 deg at 2nd row
    float hAngle2 = -PI / 2.0f;                  // start from -90 deg at 3rd row

    // the first top vertex (0, 0, r)
    v[0] = 0;
    v[1] = +radius;
    v[2] = 0;
    
    r = mmNoiseIcoSphere_RandomRadius(p, &v[0], &n[0]);
    
    v[1] = +r;

    // 10 vertices at 2nd and 3rd rows
    for(i = 1; i <= 5; ++i)
    {
        i1 = (i    ) * 3;   // for 2nd row
        i2 = (i + 5) * 3;   // for 3rd row
        
        y = radius * sinf(V_ANGLE);              // elevaton
        k = radius * cosf(V_ANGLE);
        
        v[i1    ] = +k * cosf(hAngle1);   // x
        v[i2    ] = +k * cosf(hAngle2);
        v[i1 + 1] = +y;                   // y
        v[i2 + 1] = -y;
        v[i1 + 2] = -k * sinf(hAngle1);   // z
        v[i2 + 2] = -k * sinf(hAngle2);
        
        r = mmNoiseIcoSphere_RandomRadius(p, &v[i1    ], &n[i1    ]);
        
        y = r * sinf(V_ANGLE);
        k = r * cosf(V_ANGLE);
        v[i1    ] = +k * cosf(hAngle1);   // x
        v[i1 + 1] = +y;                   // y
        v[i1 + 2] = -k * sinf(hAngle1);   // z
        
        r = mmNoiseIcoSphere_RandomRadius(p, &v[i2    ], &v[i2    ]);
        
        y = r * sinf(V_ANGLE);
        k = r * cosf(V_ANGLE);
        v[i2    ] = +k * cosf(hAngle2);   // x
        v[i2 + 1] = -y;                   // y
        v[i2 + 2] = -k * sinf(hAngle2);   // z

        // next horizontal angles
        hAngle1 += H_ANGLE;
        hAngle2 += H_ANGLE;
    }

    // the last bottom vertex (0, 0, -r)
    v[33] = 0;
    v[34] = -radius;
    v[35] = 0;
    
    r = mmNoiseIcoSphere_RandomRadius(p, &v[33], &n[33]);
    
    v[34] = -r;
}

void
mmNoiseIcoSphere_ComputeHalfVertex(
    const struct mmNoiseIcoSphere*                 p,
    const float                                    v1[3],
    const float                                    v2[3],
    float                                          length,
    float                                          v[3],
    float                                          n[3])
{
    float sum[3];
    float r;
    float k;
    float l;
    
    mmVec3Add(sum, v1, v2);
    l = mmVec3Length(sum);
    
    k = length / l;
    mmVec3Scale(v, sum, k);
    
    r = mmNoiseIcoSphere_RandomRadius(p, v, n);
    
    k = r / l;
    mmVec3Scale(v, sum, k);
}

/*
 * add a subdivided vertex attribs (vertex, normal, texCoord) to arrays, then
 * return its index value
 * If it is a shared vertex, remember its index, so it can be re-used
 */
uint32_t
mmNoiseIcoSphere_AddSubVertexAttribs(
    struct mmNoiseIcoSphere*                       p,
    const float                                    v[3],
    const float                                    n[3],
    const float                                    t[2],
    uint32_t*                                      vid,
    struct mmRbtreeF64U32*                         sIdxs)
{
    uint32_t index;

    // check if is shared vertex or not first
    if(mmIcoSphere_IsSharedTexCoord(t))
    {
        // find if it does already exist in sharedIndices map using (s,t) key
        // if not in the list, add the vertex attribs to arrays and return its index
        // if exists, return the current index
        mmFloat64_t k;
        struct mmRbtreeF64U32Iterator* it;
        memcpy(&k, t, sizeof(mmFloat64_t));
        it = mmRbtreeF64U32_GetIterator(sIdxs, k);
        if (NULL == it)
        {
            float* vtx;
            
            vtx = (float*)p->vVtxs.arrays + (*vid) * 8;
            
            (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
            (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
            (*vtx++) = t[0]; (*vtx++) = t[1];

            index = (*vid);
            mmRbtreeF64U32_Set(sIdxs, k, index);
            (*vid)++;
        }
        else
        {
            index = it->v;
        }
    }
    else
    {
        // not shared
        float* vtx;
        
        vtx = (float*)p->vVtxs.arrays + (*vid) * 8;
        
        (*vtx++) = v[0]; (*vtx++) = v[1]; (*vtx++) = v[2];
        (*vtx++) = n[0]; (*vtx++) = n[1]; (*vtx++) = n[2];
        (*vtx++) = t[0]; (*vtx++) = t[1];
        
        index = (*vid);
        (*vid)++;
    }

    return index;
}

/*
 * divide a trianlge (v1-v2-v3) into 4 sub triangles by adding middle vertices
 * (newV1, newV2, newV3) and repeat N times
 * If subdivision=0, do nothing.
 *         v1           //
 *        / \           //
 * newV1 *---* newV3    //
 *      / \ / \         //
 *    v2---*---v3       //
 *        newV2         //
 */
void
mmNoiseIcoSphere_SubdivideVerticesSmooth(
    struct mmNoiseIcoSphere*                       p,
    struct mmRbtreeF64U32*                         sIdxs)
{
    float radius = p->radius;
    int subdivision = p->subdivision;
    
    unsigned int i1, i2, i3;            // indices from original triangle
    const float *v1, *v2, *v3;          // ptr to original vertices of a triangle
    const float *t1, *t2, *t3;          // ptr to original texcoords of a triangle
    float newV1[3], newV2[3], newV3[3]; // new subdivided vertex positions
    float newN1[3], newN2[3], newN3[3]; // new subdivided normals
    float newT1[2], newT2[2], newT3[2]; // new subdivided texture coords
    unsigned int newI1, newI2, newI3;   // new subdivided indices
    int i, j;
    
    struct mmVectorU32 tIdxs;
    size_t idxCount;
    size_t vtxCount;
    
    float* vtx;
    uint32_t* idx;
    uint32_t* udx;
    uint32_t vid;
    
    size_t count;
    
    // regular icosahedron topology properties.
    size_t lastTriangleCount = 20;
    size_t lastSharedEdgeCount = 19;
    size_t lastEdgeCount = 41;
    
    size_t thisTriangleCount;
    size_t thisSharedEdgeCount;
    size_t thisEdgeCount;
    
    mmVectorU32_Init(&tIdxs);

    // iteration for subdivision
    for(i = 1; i <= subdivision; ++i)
    {
        // copy prev indices
        mmVectorU32_CopyFromValue(&tIdxs, &p->vIdxs);

        idxCount = mmVectorU32_Size(&p->vIdxs);
        vtxCount = mmVectorF32_Size(&p->vVtxs);
        
        // clear prev arrays
        mmVectorU32_Clear(&p->vIdxs);
        
        vid = (uint32_t)mmVectorF32_Size(&p->vVtxs) / 8;
        
        thisTriangleCount = lastTriangleCount * 4;
        thisSharedEdgeCount = lastSharedEdgeCount * 2 + lastTriangleCount * 3;
        thisEdgeCount = thisTriangleCount * 3 - thisSharedEdgeCount;

        count = (vtxCount / 8) + lastEdgeCount;
        mmVectorF32_AllocatorMemory(&p->vVtxs, count * 8);
        vtx = (float*)p->vVtxs.arrays;
        p->vVtxs.size = count * 8;
        
        count = (idxCount / 3) * 4;
        mmVectorU32_AllocatorMemory(&p->vIdxs, count * 3);
        idx = (uint32_t*)p->vIdxs.arrays;
        p->vIdxs.size = count * 3;
        
        lastTriangleCount = thisTriangleCount;
        lastSharedEdgeCount = thisSharedEdgeCount;
        lastEdgeCount = thisEdgeCount;
        
        udx = (uint32_t*)tIdxs.arrays;

        for(j = 0; j < idxCount; j += 3)
        {
            // get 3 indices of each triangle
            i1 = udx[j  ];
            i2 = udx[j+1];
            i3 = udx[j+2];

            // get 3 vertex attribs from prev triangle
            // position
            v1 = &vtx[i1 * 8    ];
            v2 = &vtx[i2 * 8    ];
            v3 = &vtx[i3 * 8    ];
            // texcoord
            t1 = &vtx[i1 * 8 + 6];
            t2 = &vtx[i2 * 8 + 6];
            t3 = &vtx[i3 * 8 + 6];

            // get 3 new vertex attribs by spliting half on each edge
            mmIcoSphere_ComputeHalfTexCoord(t1, t2, newT1);
            mmIcoSphere_ComputeHalfTexCoord(t2, t3, newT2);
            mmIcoSphere_ComputeHalfTexCoord(t1, t3, newT3);
            
            mmNoiseIcoSphere_ComputeHalfVertex(p, v1, v2, radius, newV1, newN1);
            mmNoiseIcoSphere_ComputeHalfVertex(p, v2, v3, radius, newV2, newN2);
            mmNoiseIcoSphere_ComputeHalfVertex(p, v1, v3, radius, newV3, newN3);

            // add new vertices/normals/texcoords to arrays
            // It will check if it is shared/non-shared and return index
            newI1 = mmNoiseIcoSphere_AddSubVertexAttribs(p, newV1, newN1, newT1, &vid, sIdxs);
            newI2 = mmNoiseIcoSphere_AddSubVertexAttribs(p, newV2, newN2, newT2, &vid, sIdxs);
            newI3 = mmNoiseIcoSphere_AddSubVertexAttribs(p, newV3, newN3, newT3, &vid, sIdxs);

            // add 4 new triangle indices
            (*idx++) =    i1; (*idx++) = newI1; (*idx++) = newI3;
            (*idx++) = newI1; (*idx++) =    i2; (*idx++) = newI2;
            (*idx++) = newI3; (*idx++) = newI2; (*idx++) =    i3;
            (*idx++) = newI1; (*idx++) = newI2; (*idx++) = newI3;
        }
    }
    
    mmVectorU32_Destroy(&tIdxs);
}

void
mmNoiseIcoSphere_Init(
    struct mmNoiseIcoSphere*                       p)
{
    mmVectorU32_Init(&p->vIdxs);
    mmVectorF32_Init(&p->vVtxs);
    
    p->pNoise3DImage = NULL;
    
    p->height = 0.045f;
    p->offset = 0.05f;
    p->detail = 1;
    
    p->radius = 1.0f;
    p->subdivision = 0;
    p->smooth = MM_TRUE;
}

void
mmNoiseIcoSphere_Destroy(
    struct mmNoiseIcoSphere*                       p)
{
    p->smooth = MM_TRUE;
    p->subdivision = 0;
    p->radius = 1.0f;
    
    p->detail = 1;
    p->offset = 0.05f;
    p->height = 0.045f;
    
    p->pNoise3DImage = NULL;
    
    mmVectorF32_Destroy(&p->vVtxs);
    mmVectorU32_Destroy(&p->vIdxs);
}

int
mmNoiseIcoSphere_Prepare(
    struct mmNoiseIcoSphere*                       p,
    float                                          radius,
    int                                            subdivision)
{
    p->radius = radius;
    p->subdivision = subdivision;
    mmNoiseIcoSphere_BuildVerticesSmooth(p);
    return 0;
}

void
mmNoiseIcoSphere_Discard(
    struct mmNoiseIcoSphere*                       p)
{
    mmNoiseIcoSphere_ClearArrays(p);
}

/*
 * build vertices of sphere with smooth shading using parametric equation
 *
 * right-hand +y up coordinate space.
 *
 *     x = +r * cos(u) * cos(v)
 *     y = +r * sin(u)
 *     z = -r * cos(u) * sin(v)
 *
 * where u: stacks( latitude) angle (-90 <= u <=  90)
 *       v: slices(longitude) angle (  0 <= v <= 360)
 */
void
mmNoiseIcoSphere_BuildVerticesSmooth(
    struct mmNoiseIcoSphere*                       p)
{
    static const float S_STEP = 186 / 2048.0f;     // horizontal texture step
    static const float T_STEP = 322 / 1024.0f;     // vertical texture step

    // compute 12 vertices of icosahedron
    // NOTE: v0 (top), v11(bottom), v1, v6(first vert on each row) cannot be
    // shared for smooth shading (they have different texcoords)
    float vPos[36] = { 0 };
    float vNor[36] = { 0 };
    
    float* vtx;
    size_t count;
    
    uint32_t* idx;
    
    int i;
    
    mmFloat64_t k;
    
    struct mmRbtreeF64U32 sIdxs;
    
    mmRbtreeF64U32_Init(&sIdxs);
    
    // clear memory of prev arrays
    mmVectorU32_Reset(&p->vIdxs);
    mmVectorF32_Reset(&p->vVtxs);
    
    count = 22;
    mmVectorF32_AllocatorMemory(&p->vVtxs, count * 8);
    vtx = (float*)p->vVtxs.arrays;
    p->vVtxs.size = count * 8;
    
    // suture coincide, one quad two triangle.
    count = 20;
    mmVectorU32_AllocatorMemory(&p->vIdxs, count * 3);
    idx = (uint32_t*)p->vIdxs.arrays;
    p->vIdxs.size = count * 3;
    
    mmNoiseIcoSphere_ComputeIcosahedronVertices(p, p->radius, vPos, vNor);

    // smooth icosahedron has 14 non-shared (0 to 13) and
    // 8 shared vertices (14 to 21) (total 22 vertices)
    //   00  01  02  03  04          //
    //   /\  /\  /\  /\  /\          //
    //  /  \/  \/  \/  \/  \         //
    // 10--14--15--16--17--11        //
    //  \  /\  /\  /\  /\  /\        //
    //   \/  \/  \/  \/  \/  \       //
    //   12--18--19--20--21--13      //
    //    \  /\  /\  /\  /\  /       //
    //     \/  \/  \/  \/  \/        //
    //     05  06  07  08  09        //
    // add 14 non-shared vertices first (index from 0 to 13)
    
    // (top   ) v0 v1 v2 v3 v4
    for (i = 0; i < 5; ++i)
    {
        (*vtx++) = vPos[0]; (*vtx++) = vPos[1]; (*vtx++) = vPos[2];
        (*vtx++) = vNor[0]; (*vtx++) = vNor[1]; (*vtx++) = vNor[2];
        (*vtx++) = S_STEP * ((i * 2) + 1); (*vtx++) = 0.0f;
    }

    // (bottom) v5 v6 v7 v8 v9
    for (i = 0; i < 5; ++i)
    {
        (*vtx++) = vPos[33]; (*vtx++) = vPos[34]; (*vtx++) = vPos[35];
        (*vtx++) = vNor[33]; (*vtx++) = vNor[34]; (*vtx++) = vNor[35];
        (*vtx++) = S_STEP * ((i + 1) * 2); (*vtx++) = T_STEP * 3;
    }
    
    // v10 (left)
    (*vtx++) = vPos[3]; (*vtx++) = vPos[4]; (*vtx++) = vPos[5];
    (*vtx++) = vNor[3]; (*vtx++) = vNor[4]; (*vtx++) = vNor[5];
    (*vtx++) = 0.0f; (*vtx++) = T_STEP;
    // v11 (right)
    (*vtx++) = vPos[3]; (*vtx++) = vPos[4]; (*vtx++) = vPos[5];
    (*vtx++) = vNor[3]; (*vtx++) = vNor[4]; (*vtx++) = vNor[5];
    (*vtx++) = S_STEP * 10; (*vtx++) = T_STEP;

    // v12 (left)
    (*vtx++) = vPos[18]; (*vtx++) = vPos[19]; (*vtx++) = vPos[20];
    (*vtx++) = vNor[18]; (*vtx++) = vNor[19]; (*vtx++) = vNor[20];
    (*vtx++) = S_STEP; (*vtx++) = T_STEP * 2;
    // v13 (right)
    (*vtx++) = vPos[18]; (*vtx++) = vPos[19]; (*vtx++) = vPos[20];
    (*vtx++) = vNor[18]; (*vtx++) = vNor[19]; (*vtx++) = vNor[20];
    (*vtx++) = S_STEP * 11; (*vtx++) = T_STEP * 2;
    
    // add 8 shared vertices to array (index from 14 to 21)
    // v14 (shared)
    (*vtx++) = vPos[6]; (*vtx++) = vPos[7]; (*vtx++) = vPos[8];
    (*vtx++) = vNor[6]; (*vtx++) = vNor[7]; (*vtx++) = vNor[8];
    (*vtx++) = S_STEP * 2; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 14);

    // v15 (shared)
    (*vtx++) = vPos[9]; (*vtx++) = vPos[10]; (*vtx++) = vPos[11];
    (*vtx++) = vNor[9]; (*vtx++) = vNor[10]; (*vtx++) = vNor[11];
    (*vtx++) = S_STEP * 4; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 15);

    // v16 (shared)
    (*vtx++) = vPos[12]; (*vtx++) = vPos[13]; (*vtx++) = vPos[14];
    (*vtx++) = vNor[12]; (*vtx++) = vNor[13]; (*vtx++) = vNor[14];
    (*vtx++) = S_STEP * 6; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 16);

    // v17 (shared)
    (*vtx++) = vPos[15]; (*vtx++) = vPos[16]; (*vtx++) = vPos[17];
    (*vtx++) = vNor[15]; (*vtx++) = vNor[16]; (*vtx++) = vNor[17];
    (*vtx++) = S_STEP * 8; (*vtx++) = T_STEP;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 17);
    
    // v18 (shared)
    (*vtx++) = vPos[21]; (*vtx++) = vPos[22]; (*vtx++) = vPos[23];
    (*vtx++) = vNor[21]; (*vtx++) = vNor[22]; (*vtx++) = vNor[23];
    (*vtx++) = S_STEP * 3; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 18);

    // v19 (shared)
    (*vtx++) = vPos[24]; (*vtx++) = vPos[25]; (*vtx++) = vPos[26];
    (*vtx++) = vNor[24]; (*vtx++) = vNor[25]; (*vtx++) = vNor[26];
    (*vtx++) = S_STEP * 5; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 19);

    // v20 (shared)
    (*vtx++) = vPos[27]; (*vtx++) = vPos[28]; (*vtx++) = vPos[29];
    (*vtx++) = vNor[27]; (*vtx++) = vNor[28]; (*vtx++) = vNor[29];
    (*vtx++) = S_STEP * 7; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 20);

    // v20 (shared)
    (*vtx++) = vPos[30]; (*vtx++) = vPos[31]; (*vtx++) = vPos[32];
    (*vtx++) = vNor[30]; (*vtx++) = vNor[31]; (*vtx++) = vNor[32];
    (*vtx++) = S_STEP * 9; (*vtx++) = T_STEP * 2;
    // shared index cache.
    memcpy(&k, vtx - 2, sizeof(mmFloat64_t));
    mmRbtreeF64U32_Set(&sIdxs, k, 21);

    // build index list for icosahedron (20 triangles)
    // 1st row (5 tris)
    (*idx++) =  0; (*idx++) = 10; (*idx++) = 14;
    (*idx++) =  1; (*idx++) = 14; (*idx++) = 15;
    (*idx++) =  2; (*idx++) = 15; (*idx++) = 16;
    (*idx++) =  3; (*idx++) = 16; (*idx++) = 17;
    (*idx++) =  4; (*idx++) = 17; (*idx++) = 11;
    // 2nd row (10 tris)
    (*idx++) = 10; (*idx++) = 12; (*idx++) = 14;
    (*idx++) = 12; (*idx++) = 18; (*idx++) = 14;
    (*idx++) = 14; (*idx++) = 18; (*idx++) = 15;
    (*idx++) = 18; (*idx++) = 19; (*idx++) = 15;
    (*idx++) = 15; (*idx++) = 19; (*idx++) = 16;
    (*idx++) = 19; (*idx++) = 20; (*idx++) = 16;
    (*idx++) = 16; (*idx++) = 20; (*idx++) = 17;
    (*idx++) = 20; (*idx++) = 21; (*idx++) = 17;
    (*idx++) = 17; (*idx++) = 21; (*idx++) = 11;
    (*idx++) = 21; (*idx++) = 13; (*idx++) = 11;
    // 3rd row (5 tris)
    (*idx++) =  5; (*idx++) = 18; (*idx++) = 12;
    (*idx++) =  6; (*idx++) = 19; (*idx++) = 18;
    (*idx++) =  7; (*idx++) = 20; (*idx++) = 19;
    (*idx++) =  8; (*idx++) = 21; (*idx++) = 20;
    (*idx++) =  9; (*idx++) = 13; (*idx++) = 21;

    // subdivide icosahedron
    mmNoiseIcoSphere_SubdivideVerticesSmooth(p, &sIdxs);
    
    mmRbtreeF64U32_Destroy(&sIdxs);
}

size_t
mmNoiseIcoSphere_GetIdxCount(
    const struct mmNoiseIcoSphere*                 p)
{
    return (p->vIdxs.size);
}

size_t
mmNoiseIcoSphere_GetVtxCount(
    const struct mmNoiseIcoSphere*                 p)
{
    return (p->vVtxs.size / 8);
}

size_t
mmNoiseIcoSphere_GetIdxBufferSize(
    const struct mmNoiseIcoSphere*                 p)
{
    return p->vIdxs.size * sizeof(uint32_t);
}

size_t
mmNoiseIcoSphere_GetVtxBufferSize(
    const struct mmNoiseIcoSphere*                 p)
{
    return p->vVtxs.size * sizeof(float);
}

const uint8_t*
mmNoiseIcoSphere_GetIdxBuffer(
    const struct mmNoiseIcoSphere*                 p)
{
    return (const uint8_t*)p->vIdxs.arrays;
}

const uint8_t*
mmNoiseIcoSphere_GetVtxBuffer(
    const struct mmNoiseIcoSphere*                 p)
{
    return (const uint8_t*)p->vVtxs.arrays;
}
