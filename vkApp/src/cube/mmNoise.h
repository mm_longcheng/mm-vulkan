/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/* https://github.com/caseman/noise */
/*
Copyright (c) 2008 Casey Duncan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __mmNoise_h__
#define __mmNoise_h__

#include "core/mmCore.h"

#include "random/mmXoshiro.h"

#include "core/mmPrefix.h"

struct mmNoise
{
    struct mmXoshiro256starstar random;
    unsigned char PERM[512];
};

void
mmNoise_Init(
    struct mmNoise*                                p);

void
mmNoise_Destroy(
    struct mmNoise*                                p);

int
mmNoise_Rand(
    struct mmNoise*                                p);

void
mmNoise_Srand(
    struct mmNoise*                                p,
    mmUInt64_t                                     seed);

/*
 * float x;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 * float repeatx = 1024; // arbitrary
 * int base = 0;
 */
float
mmNoise_PNoise1(
    struct mmNoise*                                p,
    float                                          x,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity,
    float                                          repeatx,
    int                                            base);

/*
 * float x, y;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 * float repeatx = 1024; // arbitrary
 * float repeaty = 1024; // arbitrary
 * int base = 0;
 */
float
mmNoise_PNoise2(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity,
    float                                          repeatx,
    float                                          repeaty,
    int                                            base);

/*
 * float x, y, z;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 * float repeatx = 1024; // arbitrary
 * float repeaty = 1024; // arbitrary
 * float repeatz = 1024; // arbitrary
 * int base = 0;
 */
float
mmNoise_PNoise3(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    float                                          z,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity,
    float                                            repeatx,
    float                                            repeaty,
    float                                            repeatz,
    int                                            base);

/*
 * float x, y;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 * float repeatx = FLT_MAX;
 * float repeaty = FLT_MAX;
 * float z = 0.0f;
 */
float
mmNoise_SNoise2Repeat(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity,
    float                                          repeatx,
    float                                          repeaty,
    float                                          z);

/*
 * float x, y;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 */
float
mmNoise_SNoise2(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity);

/*
 * float x, y, z;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 */
float
mmNoise_SNoise3(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    float                                          z,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity);

/*
 * float x, y, z, w;
 * int octaves = 1;
 * float persistence = 0.5f;
 * float lacunarity = 2.0f;
 */
float
mmNoise_SNoise4(
    struct mmNoise*                                p,
    float                                          x,
    float                                          y,
    float                                          z,
    float                                          w,
    int                                            octaves,
    float                                          persistence,
    float                                          lacunarity);

#include "core/mmSuffix.h"

#endif//__mmNoise_h__
