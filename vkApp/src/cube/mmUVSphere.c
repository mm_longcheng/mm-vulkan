/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUVSphere.h"

#include "math/mmMath.h"

void
mmUVSphere_ClearArrays(
    struct mmUVSphere*                             p)
{
    mmVectorU32_Reset(&p->vIdxs);
    mmVectorF32_Reset(&p->vVtxs);
}

void
mmUVSphere_Init(
    struct mmUVSphere*                             p)
{
    mmVectorU32_Init(&p->vIdxs);
    mmVectorF32_Init(&p->vVtxs);
    
    p->radius = 1.0f;
    p->slices = 36;
    p->stacks = 18;
    p->smooth = MM_TRUE;
}

void
mmUVSphere_Destroy(
    struct mmUVSphere*                             p)
{
    p->smooth = MM_TRUE;
    p->stacks = 18;
    p->slices = 36;
    p->radius = 1.0f;
    
    mmVectorF32_Destroy(&p->vVtxs);
    mmVectorU32_Destroy(&p->vIdxs);
}

int
mmUVSphere_Prepare(
    struct mmUVSphere*                             p,
    float                                          radius,
    int                                            slices,
    int                                            stacks)
{
    p->radius = radius;
    p->slices = slices;
    p->stacks = stacks;
    mmUVSphere_BuildVerticesSmooth(p);
    return 0;
}

void
mmUVSphere_Discard(
    struct mmUVSphere*                             p)
{
    mmUVSphere_ClearArrays(p);
}

/*
 * build vertices of sphere with smooth shading using parametric equation
 *
 * right-hand +y up coordinate space.
 *
 *     x = +r * cos(u) * cos(v)
 *     y = +r * sin(u)
 *     z = -r * cos(u) * sin(v)
 *
 * where u: stacks( latitude) angle (-90 <= u <=  90)
 *       v: slices(longitude) angle (  0 <= v <= 360)
 */
void
mmUVSphere_BuildVerticesSmooth(
    struct mmUVSphere*                             p)
{
    /*
     * generate interleaved vertices: V/N/T
     * stride must be 32 bytes
     */
    
    const float PI = (float)MM_PI;
    
    float radius = p->radius;
    int slices = p->slices;
    int stacks = p->stacks;

    int i, j;
    
    float k;                                        // xz plane radius.
    float x, y, z;                                  // vertex position
    float nx, ny, nz;                               // normal
    float s, t;                                     // texCoord
    float lengthInv = 1.0f / radius;

    float slicesStep = 2 * PI / slices;
    float stacksStep = PI / stacks;
    float v, u;
    
    float* vtx;
    size_t count;
    
    uint32_t* idx;
    uint32_t k1, k2;
    
    // clear memory of prev arrays
    mmUVSphere_ClearArrays(p);
    
    count = (stacks + 1) * (slices + 1);
    mmVectorF32_AllocatorMemory(&p->vVtxs, count * 8);
    vtx = (float*)p->vVtxs.arrays;
    p->vVtxs.size = count * 8;
    
    // suture coincide, one quad two triangle.
    count = (stacks - 1) * slices * 2;
    mmVectorU32_AllocatorMemory(&p->vIdxs, count * 3);
    idx = (uint32_t*)p->vIdxs.arrays;
    p->vIdxs.size = count * 3;

    for(i = 0; i <= stacks; ++i)
    {
        u = PI / 2 - i * stacksStep;               // starting from pi/2 to -pi/2
        k = +radius * cosf(u);                     // +r * cos(u)
        y = +radius * sinf(u);                     // +r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal,
        // but different tex coords
        for(j = 0; j <= slices; ++j)
        {
            v = j * slicesStep;                    // starting from 0 to 2pi

            // vertex position
            x = +k * cosf(v);                      // +r * cos(u) * cos(v)
            z = -k * sinf(v);                      // -r * cos(u) * sin(v)
            (*vtx++) = x;
            (*vtx++) = y;
            (*vtx++) = z;

            // normalized vertex normal
            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;
            (*vtx++) = nx;
            (*vtx++) = ny;
            (*vtx++) = nz;

            // vertex tex coord between [0, 1]
            s = (float)j / slices;
            t = (float)i / stacks;
            (*vtx++) = s;
            (*vtx++) = t;
        }
    }

    // indices
    //  k1--k1+1
    //  |  / |
    //  | /  |
    //  k2--k2+1
    for(i = 0; i < stacks; ++i)
    {
        k1 = i * (slices + 1);     // beginning of current stack
        k2 = k1 + slices + 1;      // beginning of next stack

        for(j = 0; j < slices; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding 1st and last stacks
            if(i != 0)
            {
                // k1---k2---k1+1
                (*idx++) = k1;
                (*idx++) = k2;
                (*idx++) = k1 + 1;
            }

            if(i != (stacks - 1))
            {
                // k1+1---k2---k2+1
                (*idx++) = k1 + 1;
                (*idx++) = k2;
                (*idx++) = k2 + 1;
            }
        }
    }
}

size_t
mmUVSphere_GetIdxCount(
    const struct mmUVSphere*                       p)
{
    return (p->vIdxs.size);
}

size_t
mmUVSphere_GetVtxCount(
    const struct mmUVSphere*                       p)
{
    return (p->vVtxs.size / 8);
}

size_t
mmUVSphere_GetIdxBufferSize(
    const struct mmUVSphere*                       p)
{
    return p->vIdxs.size * sizeof(uint32_t);
}

size_t
mmUVSphere_GetVtxBufferSize(
    const struct mmUVSphere*                       p)
{
    return p->vVtxs.size * sizeof(float);
}

const uint8_t*
mmUVSphere_GetIdxBuffer(
    const struct mmUVSphere*                       p)
{
    return (const uint8_t*)p->vIdxs.arrays;
}

const uint8_t*
mmUVSphere_GetVtxBuffer(
    const struct mmUVSphere*                       p)
{
    return (const uint8_t*)p->vVtxs.arrays;
}
