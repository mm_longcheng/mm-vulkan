/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNoise3DImage_h__
#define __mmNoise3DImage_h__

#include "core/mmCore.h"

#include "random/mmXoshiro.h"

#include "core/mmPrefix.h"

struct mmNoise;

struct mmNoise3DImage
{
    uint16_t* buffer;
    size_t length;
    
    float frequency;
    int width;
};

void
mmNoise3DImage_Init(
    struct mmNoise3DImage*                         p);

void
mmNoise3DImage_Destroy(
    struct mmNoise3DImage*                         p);

/*
 * float frequency = 8;
 * float width = 32;
 */
int
mmNoise3DImage_Prepare(
    struct mmNoise3DImage*                         p,
    struct mmNoise*                                pNoise,
    float                                          frequency,
    int                                            width);

void
mmNoise3DImage_Discard(
    struct mmNoise3DImage*                         p);

void
mmNoise3DImage_Texture(
    const struct mmNoise3DImage*                   p,
    const float                                    uvw[3],
    float                                          color[4]);

/* Simple perlin noise work-alike */
float
mmNoise3DImage_PNoise(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3]);

/* Multi-octave fractal brownian motion perlin noise */
float
mmNoise3DImage_FBMNoise(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3],
    int                                            octaves);

/* Multi-octave turbulent noise */
float
mmNoise3DImage_FBMTurbulence(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3],
    int                                            octaves);

#include "core/mmSuffix.h"

#endif//__mmNoise3DImage_h__
