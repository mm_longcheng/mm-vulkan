/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNoise3DImage.h"
#include "mmNoise.h"

#include "core/mmAlloc.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"

void
mmNoise3DImage_Init(
    struct mmNoise3DImage*                         p)
{
    p->buffer = NULL;
    p->length = 0;
    p->frequency = 8.0f;
    p->width = 32;
}

void
mmNoise3DImage_Destroy(
    struct mmNoise3DImage*                         p)
{
    assert(NULL == p->buffer && "assets is not destroy complete.");
    
    p->width = 32;
    p->frequency = 8.0f;
    p->length = 0;
    p->buffer = NULL;
}

int
mmNoise3DImage_Prepare(
    struct mmNoise3DImage*                         p,
    struct mmNoise*                                pNoise,
    float                                          frequency,
    int                                            width)
{
    uint16_t* texel;
    float scale;
    int width2;
    int x, y, z;
    int idx;
    float rnd;
    float v;
    
    assert(NULL == p->buffer && "assets is not destroy complete.");
    p->frequency = frequency;
    p->width = width;
    p->length = 2 * sizeof(uint16_t) * width * width * width;
    p->buffer = mmMalloc(p->length);
    
    texel = p->buffer;
    scale = (float)frequency / (float)width;
    width2 = width * width;

    for (z = 0; z < width; ++z)
    {
        for (y = 0; y < width; ++y)
        {
            for (x = 0; x < width; ++x)
            {
                idx = (x + (y * width) + (z * width2)) * 2;
                
                rnd = mmNoise_PNoise3(
                    pNoise,
                    x * scale, y * scale, z * scale,
                    1, 0.5f, 2.0f,
                    frequency, frequency, frequency, 0);
                v = (rnd + 1.0f) * 32767;
                texel[idx] = (int)(v);

                rnd = mmNoise_PNoise3(
                    pNoise,
                    x * scale, y * scale, z * scale,
                    1, 0.5f, 2.0f,
                    frequency, frequency, frequency, (int)frequency + 1);
                v = (rnd + 1.0f) * 32767;
                texel[idx + 1] = (int)(v);
            }
        }
    }

    return 0;
}

void
mmNoise3DImage_Discard(
    struct mmNoise3DImage*                         p)
{
    mmFree(p->buffer);
    p->frequency = 8;
    p->width = 32;
    p->length = 0;
    p->buffer = NULL;
}

void
mmNoise3DImage_Texture(
    const struct mmNoise3DImage*                   p,
    const float                                    uvw[3],
    float                                          color[4])
{
    uint16_t* texel;
    int width;
    int width2;
    int x, y, z;
    int idx;
    
    texel = p->buffer;
    width = p->width;
    width2 = width * width;
    x = (int)(mmMathMod(uvw[0], 1.0f) * width);
    y = (int)(mmMathMod(uvw[1], 1.0f) * width);
    z = (int)(mmMathMod(uvw[2], 1.0f) * width);
    idx = (x + (y * width) + (z * width2)) * 2;
    color[0] = texel[idx    ] / 65535.0f;
    color[1] = texel[idx + 1] / 65535.0f;
    color[2] = 0.0f;
    color[3] = 0.0f;
}

/* Simple perlin noise work-alike */
float
mmNoise3DImage_PNoise(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3])
{
    const float twopi = (float)(MM_PI * 2.0);
    
    float uvw[3];
    float hi[4];
    float lo[4];
    
    mmVec3Assign(uvw, position);
    mmNoise3DImage_Texture(p, uvw, hi);
    mmVec3Scale(hi, hi, 2.0f);
    mmVec3SubK(hi, hi, 1.0f);
    
    mmVec3Scale(uvw, uvw, 1.0f / 9.0f);
    mmNoise3DImage_Texture(p, uvw, lo);
    mmVec3Scale(lo, lo, 2.0f);
    mmVec3SubK(lo, lo, 1.0f);
    
    return hi[0] * cosf(twopi * lo[0]) + hi[1] * sinf(twopi * lo[0]);
}

/* Multi-octave fractal brownian motion perlin noise */
float
mmNoise3DImage_FBMNoise(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3],
    int                                            octaves)
{
    const float twopi = (float)(MM_PI * 2.0);
    
    float m = 1.0f;
    float uvw[3];
    float v[4];
    float hi[4];
    float lo[4];
    int x;
    
    mmVec3Assign(uvw, position);
    mmVec3AssignValue(hi, 0.0f);
    /* XXX Loops may not work correctly on all video cards */
    for (x = 0; x < octaves; x++)
    {
        mmNoise3DImage_Texture(p, uvw, v);
        mmVec3Scale(v, v, 2.0f);
        mmVec3SubK(v, v, 1.0f);
        mmVec3Scale(v, v, m);
        
        mmVec3Add(hi, hi, v);
        mmVec3MulK(uvw, uvw, 2.0f);
        m *= 0.5f;
    }
    
    mmVec3Assign(uvw, position);
    mmVec3Scale(uvw, uvw, 1.0f / 9.0f);
    mmNoise3DImage_Texture(p, uvw, lo);
    mmVec3Scale(lo, lo, 2.0f);
    mmVec3SubK(lo, lo, 1.0f);
    
    return hi[0] * cosf(twopi * lo[0]) + hi[1] * sinf(twopi * lo[0]);
}

/* Multi-octave turbulent noise */
float
mmNoise3DImage_FBMTurbulence(
    const struct mmNoise3DImage*                   p,
    const float                                    position[3],
    int                                            octaves)
{
    const float twopi = (float)(MM_PI * 2.0);
    
    float m = 1.0f;
    float uvw[3];
    float v[4];
    float hi[4];
    float lo[4];
    int x;
    
    mmVec3Assign(uvw, position);
    mmVec3AssignValue(hi, 0.0f);
    
    /* XXX Loops may not work correctly on all video cards */
    for (x = 0; x < octaves; x++)
    {
        mmNoise3DImage_Texture(p, uvw, v);
        mmVec3Scale(v, v, 2.0f);
        mmVec3SubK(v, v, 1.0f);
        mmVec3Abs(v, v);
        mmVec3Scale(v, v, m);
        
        mmVec3Add(hi, hi, v);
        mmVec3MulK(uvw, uvw, 2.0f);
        m *= 0.5f;
    }
    
    mmVec3Assign(uvw, position);
    mmVec3Scale(uvw, uvw, 1.0f / 9.0f);
    mmNoise3DImage_Texture(p, uvw, lo);
    
    return 2.0f * mmMathMix(hi[0], hi[1], cosf(twopi * lo[0]) * 0.5f + 0.5f) - 1.0f;
}
