/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIcoSphere_h__
#define __mmIcoSphere_h__

#include "core/mmCore.h"

#include "container/mmVectorF32.h"
#include "container/mmVectorU32.h"

#include "core/mmPrefix.h"

struct mmIcoSphere;
struct mmRbtreeF64U32;

void
mmIcoSphere_ClearArrays(
    struct mmIcoSphere*                            p);

/*
 * compute 12 vertices of icosahedron using spherical coordinates
 * The north pole is at (0, 0, r) and the south pole is at (0,0,-r).
 * 5 vertices are placed by rotating 72 deg at elevation 26.57 deg (=atan(1/2))
 * 5 vertices are placed by rotating 72 deg at elevation -26.57 deg
 *
 * right-hand +y up coordinate space.
 *
 */
void
mmIcoSphere_ComputeIcosahedronVertices(
    float                                          radius,
    float                                          vertices[36]);

/*
 * find middle point of 2 vertices
 * NOTE: new vertex must be resized, so the length is equal to the given length
 */
void
mmIcoSphere_ComputeHalfPosition(
    const float                                    v1[3],
    const float                                    v2[3],
    float                                          length,
    float                                          v[3]);

/*
 * find middle texcoords of 2 tex coords and return new coord (3rd param)
*/
void
mmIcoSphere_ComputeHalfTexCoord(
    const float                                    t1[2],
    const float                                    t2[2],
    float                                          t[2]);

/*
 * determine a point c is on the line segment a-b
*/
int
mmIcoSphere_IsOnLineSegment(
    const float                                    a[2],
    const float                                    b[2],
    const float                                    c[2]);

/*
 * This function used 20 non-shared line segments to determine if the given
 * texture coordinate is shared or no. If it is on the line segments, it is also
 * non-shared point
 *   00  01  02  03  04         //
 *   /\  /\  /\  /\  /\         //
 *  /  \/  \/  \/  \/  \        //
 * 05  06  07  08  09   \       //
 *   \   10  11  12  13  14     //
 *    \  /\  /\  /\  /\  /      //
 *     \/  \/  \/  \/  \/       //
 *      15  16  17  18  19      //
*/
int
mmIcoSphere_IsSharedTexCoord(
    const float                                    t[2]);

uint32_t
mmIcoSphere_AddSubVertexAttribs(
    struct mmIcoSphere*                            p,
    const float                                    v[3],
    const float                                    n[3],
    const float                                    t[2],
    uint32_t*                                      vid,
    struct mmRbtreeF64U32*                         sIdxs);

/*
 * divide a trianlge (v1-v2-v3) into 4 sub triangles by adding middle vertices
 * (newV1, newV2, newV3) and repeat N times
 * If subdivision=0, do nothing.
 *         v1           //
 *        / \           //
 * newV1 *---* newV3    //
 *      / \ / \         //
 *    v2---*---v3       //
 *        newV2         //
 */
void
mmIcoSphere_SubdivideVerticesSmooth(
    struct mmIcoSphere*                            p,
    struct mmRbtreeF64U32*                         sIdxs);

struct mmIcoSphere
{
    /* Vertex
     *     float position[3];
     *     float normal[3];
     *     float texcoord[2];
     */
    struct mmVectorU32 vIdxs;
    struct mmVectorF32 vVtxs;
    
    // radius for sphere
    // default 1.0f.
    float radius;
    // subdivision level.
    // default 0.
    int subdivision;
    // smooth flag.
    // default MM_TRUE.
    int smooth;
};

void
mmIcoSphere_Init(
    struct mmIcoSphere*                            p);

void
mmIcoSphere_Destroy(
    struct mmIcoSphere*                            p);

int
mmIcoSphere_Prepare(
    struct mmIcoSphere*                            p,
    float                                          radius,
    int                                            subdivision);

void
mmIcoSphere_Discard(
    struct mmIcoSphere*                            p);

void
mmIcoSphere_BuildVerticesSmooth(
    struct mmIcoSphere*                            p);

size_t
mmIcoSphere_GetIdxCount(
    const struct mmIcoSphere*                      p);

size_t
mmIcoSphere_GetVtxCount(
    const struct mmIcoSphere*                      p);

size_t
mmIcoSphere_GetIdxBufferSize(
    const struct mmIcoSphere*                      p);

size_t
mmIcoSphere_GetVtxBufferSize(
    const struct mmIcoSphere*                      p);

const uint8_t*
mmIcoSphere_GetIdxBuffer(
    const struct mmIcoSphere*                      p);

const uint8_t*
mmIcoSphere_GetVtxBuffer(
    const struct mmIcoSphere*                      p);

#include "core/mmSuffix.h"

#endif//__mmIcoSphere_h__
