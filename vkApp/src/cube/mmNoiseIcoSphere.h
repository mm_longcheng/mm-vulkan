/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNoiseIcoSphere_h__
#define __mmNoiseIcoSphere_h__

#include "core/mmCore.h"

#include "container/mmVectorF32.h"
#include "container/mmVectorU32.h"

#include "core/mmPrefix.h"

struct mmNoiseIcoSphere;
struct mmRbtreeF64U32;

float
mmNoiseIcoSphere_RandomRadius(
    const struct mmNoiseIcoSphere*                 p,
    const float                                    position[3],
    float                                          normal[3]);

void
mmNoiseIcoSphere_ClearArrays(
    struct mmNoiseIcoSphere*                       p);

void
mmNoiseIcoSphere_ComputeIcosahedronVertices(
    const struct mmNoiseIcoSphere*                 p,
    float                                          radius,
    float                                          v[36],
    float                                          n[36]);

void
mmNoiseIcoSphere_ComputeHalfVertex(
    const struct mmNoiseIcoSphere*                 p,
    const float                                    v1[3],
    const float                                    v2[3],
    float                                          length,
    float                                          v[3],
    float                                          n[3]);

/*
 * add a subdivided vertex attribs (vertex, normal, texCoord) to arrays, then
 * return its index value
 * If it is a shared vertex, remember its index, so it can be re-used
 */
uint32_t
mmNoiseIcoSphere_AddSubVertexAttribs(
    struct mmNoiseIcoSphere*                       p,
    const float                                    v[3],
    const float                                    n[3],
    const float                                    t[2],
    uint32_t*                                      vid,
    struct mmRbtreeF64U32*                         sIdxs);

/*
 * divide a trianlge (v1-v2-v3) into 4 sub triangles by adding middle vertices
 * (newV1, newV2, newV3) and repeat N times
 * If subdivision=0, do nothing.
 *         v1           //
 *        / \           //
 * newV1 *---* newV3    //
 *      / \ / \         //
 *    v2---*---v3       //
 *        newV2         //
 */
void
mmNoiseIcoSphere_SubdivideVerticesSmooth(
    struct mmNoiseIcoSphere*                       p,
    struct mmRbtreeF64U32*                         sIdxs);

struct mmNoiseIcoSphere
{
    /* Vertex
     *     float position[3];
     *     float normal[3];
     *     float texcoord[2];
     */
    struct mmVectorU32 vIdxs;
    struct mmVectorF32 vVtxs;
    
    struct mmNoise3DImage* pNoise3DImage;
    
    // height for sphere random.
    // default 0.045f.
    float height;
    // offset for computing surface derivative.
    // default 0.05f.
    float offset;
    // detail for sphere random.
    // default 1.
    int detail;
    
    // radius for sphere
    // default 1.0f.
    float radius;
    // subdivision level.
    // default 0.
    int subdivision;
    // smooth flag.
    // default MM_TRUE.
    int smooth;
};

void
mmNoiseIcoSphere_Init(
    struct mmNoiseIcoSphere*                       p);

void
mmNoiseIcoSphere_Destroy(
    struct mmNoiseIcoSphere*                       p);

int
mmNoiseIcoSphere_Prepare(
    struct mmNoiseIcoSphere*                       p,
    float                                          radius,
    int                                            subdivision);

void
mmNoiseIcoSphere_Discard(
    struct mmNoiseIcoSphere*                       p);

void
mmNoiseIcoSphere_BuildVerticesSmooth(
    struct mmNoiseIcoSphere*                       p);

size_t
mmNoiseIcoSphere_GetIdxCount(
    const struct mmNoiseIcoSphere*                 p);

size_t
mmNoiseIcoSphere_GetVtxCount(
    const struct mmNoiseIcoSphere*                 p);

size_t
mmNoiseIcoSphere_GetIdxBufferSize(
    const struct mmNoiseIcoSphere*                 p);

size_t
mmNoiseIcoSphere_GetVtxBufferSize(
    const struct mmNoiseIcoSphere*                 p);

const uint8_t*
mmNoiseIcoSphere_GetIdxBuffer(
    const struct mmNoiseIcoSphere*                 p);

const uint8_t*
mmNoiseIcoSphere_GetVtxBuffer(
    const struct mmNoiseIcoSphere*                 p);

#include "core/mmSuffix.h"

#endif//__mmNoiseIcoSphere_h__
