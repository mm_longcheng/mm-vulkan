/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKJSON.h"

#include "core/mmLimit.h"
#include "core/mmValueTransform.h"

#include "parse/mmParseJSON.h"
#include "parse/mmParseJSONHelper.h"

#include "vk/mmVKDescriptorLayout.h"

int 
mmParseJSON_DecodeVKDescriptorType(
    struct mmParseJSON*                            p, 
    int                                            i,
    VkDescriptorType*                              v,
    VkDescriptorType                               d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkDescriptorType),
        &mmValue_StringToVKDescriptorType);
}

int 
mmParseJSON_DecodeVKShaderStageFlags(
    struct mmParseJSON*                            p, 
    int                                            i,
    VkShaderStageFlags*                            v,
    VkShaderStageFlags                             d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkShaderStageFlags),
        &mmValue_StringToVKShaderStageFlags);
}

int 
mmParseJSON_DecodeVKVertexInputRate(
    struct mmParseJSON*                            p, 
    int                                            i,
    VkVertexInputRate*                             v,
    VkVertexInputRate                              d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkVertexInputRate),
        &mmValue_StringToVKVertexInputRate);
}

int 
mmParseJSON_DecodeVKFormat(
    struct mmParseJSON*                            p, 
    int                                            i,
    VkFormat*                                      v,
    VkFormat                                       d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkFormat),
        &mmValue_StringToVKFormat);
}

int
mmParseJSON_DecodeVKPrimitiveTopology(
    struct mmParseJSON*                            p,
    int                                            i,
    VkPrimitiveTopology*                           v,
    VkPrimitiveTopology                            d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkPrimitiveTopology),
        &mmValue_StringToVKPrimitiveTopology);
}

int
mmParseJSON_DecodeVKPolygonMode(
    struct mmParseJSON*                            p,
    int                                            i,
    VkPolygonMode*                                 v,
    VkPolygonMode                                  d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkPolygonMode),
        &mmValue_StringToVKPolygonMode);
}

int
mmParseJSON_DecodeVKCullModeFlags(
    struct mmParseJSON*                            p,
    int                                            i,
    VkCullModeFlags*                               v,
    VkCullModeFlags                                d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkCullModeFlags),
        &mmValue_StringToVKCullModeFlags);
}

int
mmParseJSON_DecodeVKFrontFace(
    struct mmParseJSON*                            p,
    int                                            i,
    VkFrontFace*                                   v,
    VkFrontFace                                    d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkFrontFace),
        &mmValue_StringToVKFrontFace);
}

int
mmParseJSON_DecodeVKBlendFactor(
    struct mmParseJSON*                            p,
    int                                            i,
    VkBlendFactor*                                 v,
    VkBlendFactor                                  d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkBlendFactor),
        &mmValue_StringToVKBlendFactor);
}

int
mmParseJSON_DecodeVKBlendOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkBlendOp*                                     v,
    VkBlendOp                                      d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkBlendOp),
        &mmValue_StringToVKBlendOp);
}

int
mmParseJSON_DecodeVKCompareOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkCompareOp*                                   v,
    VkCompareOp                                    d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkCompareOp),
        &mmValue_StringToVKCompareOp);
}

int
mmParseJSON_DecodeVKStencilOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkStencilOp*                                   v,
    VkStencilOp                                    d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkStencilOp),
        &mmValue_StringToVKStencilOp);
}

int
mmParseJSON_DecodeVKLogicOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkLogicOp*                                     v,
    VkLogicOp                                      d)
{
    return mmParseJSON_DecodeEnumerate(p, i, v, &d,
        sizeof(VkLogicOp),
        &mmValue_StringToVKLogicOp);
}

int 
mmParseJSON_DecodeVKDescriptorSetLayoutBinding(
    struct mmParseJSON*                            p, 
    int                                            i, 
    mmUInt32_t                                     h, 
    void*                                          o)
{
    VkDescriptorSetLayoutBinding* v = (VkDescriptorSetLayoutBinding*)(o);
    switch (h)
    {
    case 0xC0848ACB:
        // 0xC0848ACB binding
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->binding, 0);
        break;
    case 0xE0B9C096:
        // 0xE0B9C096 descriptorType
        i = mmParseJSON_DecodeVKDescriptorType(p, ++i, &v->descriptorType, VK_DESCRIPTOR_TYPE_SAMPLER);
        break;
    case 0x5C21BD07:
        // 0x5C21BD07 descriptorCount
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->descriptorCount, 0);
        break;
    case 0x710E3700:
        // 0x710E3700 stageFlags
        i = mmParseJSON_DecodeVKShaderStageFlags(p, ++i, &v->stageFlags, 0x00000000);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKPushConstantRange(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    VkPushConstantRange* v = (VkPushConstantRange*)(o);
    switch (h)
    {
    case 0x710E3700:
        // 0x710E3700 stageFlags
        i = mmParseJSON_DecodeVKShaderStageFlags(p, ++i, &v->stageFlags, 0x00000000);
        break;
    case 0x9DF2C725:
        // 0x9DF2C725 offset
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->offset, 0);
        break;
    case 0x4AD2D14C:
        // 0x4AD2D14C size
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->size, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKDescriptorPoolSize(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    VkDescriptorPoolSize* v = (VkDescriptorPoolSize*)(o);
    switch (h)
    {
    case 0x6A235628:
        // 0x6A235628 type
        i = mmParseJSON_DecodeVKDescriptorType(p, ++i, &v->type, 0x00000000);
        break;
    case 0x5C21BD07:
        // 0x5C21BD07 descriptorCount
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->descriptorCount, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKVertexInputBindingDescription(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    VkVertexInputBindingDescription* v = (VkVertexInputBindingDescription*)(o);
    switch (h)
    {
    case 0xC0848ACB:
        // 0xC0848ACB binding
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->binding, 0);
        break;
    case 0xE1FD4756:
        // 0xE1FD4756 stride
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->stride, 0);
        break;
    case 0x1ABB1770:
        // 0x1ABB1770 inputRate
        i = mmParseJSON_DecodeVKVertexInputRate(p, ++i, &v->inputRate, VK_VERTEX_INPUT_RATE_VERTEX);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKVertexInputAttributeDescription(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    VkVertexInputAttributeDescription* v = (VkVertexInputAttributeDescription*)(o);
    switch (h)
    {
    case 0xE50146A2:
        // 0xE50146A2 location
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->location, 0);
        break;
    case 0xC0848ACB:
        // 0xC0848ACB binding
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->binding, 0);
        break;
    case 0x4B1F4F58:
        // 0x4B1F4F58 format
        i = mmParseJSON_DecodeVKFormat(p, ++i, &v->format, VK_FORMAT_UNDEFINED);
        break;
    case 0x9DF2C725:
        // 0x9DF2C725 offset
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->offset, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKPipelineVertexInputInfo(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineVertexInputInfo* v = (struct mmVKPipelineVertexInputInfo*)(o);
    switch (h)
    {
    case 0x48FE8D72:
        // 0x48FE8D72 vibd
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->vibd, &mmParseJSON_DecodeVKVertexInputBindingDescription);
        break;
    case 0x9C25A119:
        // 0x9C25A119 viad
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->viad, &mmParseJSON_DecodeVKVertexInputAttributeDescription);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKPipelineShaderStageInfo(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineShaderStageInfo* v = (struct mmVKPipelineShaderStageInfo*)(o);
    switch (h)
    {
    case 0x8DFE2481:
        // 0x8DFE2481 stage
        i = mmParseJSON_DecodeVKShaderStageFlags(p, ++i, (VkShaderStageFlags*)&v->stage, 0x00000000);
        break;
    case 0x38E3A30F:
        // 0x38E3A30F module
        i = mmParseJSON_DecodeString(p, ++i, &v->module, "");
        break;
    case 0xC133C8D3:
        // 0xC133C8D3 name
        i = mmParseJSON_DecodeString(p, ++i, &v->name, "");
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKLayoutInfoArgs(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKLayoutInfoArgs* v = (struct mmVKLayoutInfoArgs*)(o);
    switch (h)
    {
    case 0x95FC78CD:
        // 0x95FC78CD dslb
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->dslb, &mmParseJSON_DecodeVKDescriptorSetLayoutBinding);
        break;
    case 0x7AFE5A72:
        // 0x7AFE5A72 dslc
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->dslc, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineInputAssemblyState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineInputAssemblyState* v = (struct mmVKPipelineInputAssemblyState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0x99D41D13:
        // 0x99D41D13 topology
        i = mmParseJSON_DecodeVKPrimitiveTopology(p, ++i, (VkPrimitiveTopology*)&v->topology, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
        break;
    case 0xEB50A02D:
        // 0xEB50A02D primitiveRestartEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->primitiveRestartEnable, VK_FALSE);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineTessellationState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineTessellationState* v = (struct mmVKPipelineTessellationState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0xF0A010EA:
        // 0xF0A010EA patchControlPoints
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->patchControlPoints, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineRasterizationState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineRasterizationState* v = (struct mmVKPipelineRasterizationState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0x8B91031E:
        // 0x8B91031E depthClampEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->depthClampEnable, VK_FALSE);
        break;
    case 0xB05DD318:
        // 0xB05DD318 rasterizerDiscardEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->rasterizerDiscardEnable, VK_FALSE);
        break;
    case 0x668B9B94:
        // 0x668B9B94 polygonMode
        i = mmParseJSON_DecodeVKPolygonMode(p, ++i, (VkPolygonMode*)&v->polygonMode, VK_POLYGON_MODE_FILL);
        break;
    case 0x511D2144:
        // 0x511D2144 cullMode
        i = mmParseJSON_DecodeVKCullModeFlags(p, ++i, (VkCullModeFlags*)&v->cullMode, VK_CULL_MODE_BACK_BIT);
        break;
    case 0xD2848358:
        // 0xD2848358 frontFace
        i = mmParseJSON_DecodeVKFrontFace(p, ++i, (VkFrontFace*)&v->frontFace, VK_FRONT_FACE_COUNTER_CLOCKWISE);
        break;
    case 0xE2EF6DD3:
        // 0xE2EF6DD3 depthBiasEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->depthBiasEnable, VK_FALSE);
        break;
    case 0xAEB895E9:
        // 0xAEB895E9 depthBiasConstantFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->depthBiasConstantFactor, 0.0f);
        break;
    case 0x69F82DFE:
        // 0x69F82DFE depthBiasClamp
        i = mmParseJSON_DecodeFloat(p, ++i, &v->depthBiasClamp, 0.0f);
        break;
    case 0xB1C4E3FC:
        // 0xB1C4E3FC depthBiasSlopeFactor
        i = mmParseJSON_DecodeFloat(p, ++i, &v->depthBiasSlopeFactor, 0.0f);
        break;
    case 0xF65F48FD:
        // 0xF65F48FD lineWidth
        i = mmParseJSON_DecodeFloat(p, ++i, &v->lineWidth, 1.0f);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineColorBlendAttachmentState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineColorBlendAttachmentState* v = (struct mmVKPipelineColorBlendAttachmentState*)(o);
    switch (h)
    {
    case 0x537AADF7:
        // 0x537AADF7 blendEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->blendEnable, VK_TRUE);
        break;
    case 0x72B89BCE:
        // 0x72B89BCE srcColorBlendFactor
        i = mmParseJSON_DecodeVKBlendFactor(p, ++i, (VkBlendFactor*)&v->srcColorBlendFactor, VK_BLEND_FACTOR_SRC_ALPHA);
        break;
    case 0xB703F733:
        // 0xB703F733 dstColorBlendFactor
        i = mmParseJSON_DecodeVKBlendFactor(p, ++i, (VkBlendFactor*)&v->dstColorBlendFactor, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);
        break;
    case 0x6C3044F9:
        // 0x6C3044F9 colorBlendOp
        i = mmParseJSON_DecodeVKBlendOp(p, ++i, (VkBlendOp*)&v->colorBlendOp, VK_BLEND_OP_ADD);
        break;
    case 0xADF4E05F:
        // 0xADF4E05F srcAlphaBlendFactor
        i = mmParseJSON_DecodeVKBlendFactor(p, ++i, (VkBlendFactor*)&v->srcAlphaBlendFactor, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);
        break;
    case 0xB6500B08:
        // 0xB6500B08 dstAlphaBlendFactor
        i = mmParseJSON_DecodeVKBlendFactor(p, ++i, (VkBlendFactor*)&v->dstAlphaBlendFactor, VK_BLEND_FACTOR_SRC_ALPHA);
        break;
    case 0xCCE275E2:
        // 0xCCE275E2 alphaBlendOp
        i = mmParseJSON_DecodeVKBlendOp(p, ++i, (VkBlendOp*)&v->alphaBlendOp, VK_BLEND_OP_ADD);
        break;
    case 0xA24DEC97:
        // 0xA24DEC97 colorWriteMask
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->colorWriteMask, 0x0000000F);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineColorBlendState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineColorBlendState* v = (struct mmVKPipelineColorBlendState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0xE1D4A2A3:
        // 0xE1D4A2A3 logicOpEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->logicOpEnable, VK_FALSE);
        break;
    case 0x6BFBFE7E:
        // 0x6BFBFE7E logicOp
        i = mmParseJSON_DecodeVKLogicOp(p, ++i, (VkLogicOp*)&v->logicOp, VK_LOGIC_OP_CLEAR);
        break;
    case 0xA5205070:
        // 0xA5205070 attachments
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->attachments, &mmParseJSON_DecodeVKPipelineColorBlendAttachmentState);
        break;
    case 0x98567B73:
        // 0x98567B73 blendConstants
        i = mmParseJSON_DecodeFloatArray(p, ++i, v->blendConstants, &MM_FLOAT_ZERO, 4, NULL);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKStencilOpState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKStencilOpState* v = (struct mmVKStencilOpState*)(o);
    switch (h)
    {
    case 0x87319388:
        // 0x87319388 failOp
        i = mmParseJSON_DecodeVKStencilOp(p, ++i, (VkStencilOp*)&v->failOp, VK_STENCIL_OP_KEEP);
        break;
    case 0xAD940FCC:
        // 0xAD940FCC passOp
        i = mmParseJSON_DecodeVKStencilOp(p, ++i, (VkStencilOp*)&v->passOp, VK_STENCIL_OP_KEEP);
        break;
    case 0xFCFDC5DC:
        // 0xFCFDC5DC depthFailOp
        i = mmParseJSON_DecodeVKStencilOp(p, ++i, (VkStencilOp*)&v->depthFailOp, VK_STENCIL_OP_KEEP);
        break;
    case 0x5BF3C98C:
        // 0x5BF3C98C compareOp
        i = mmParseJSON_DecodeVKCompareOp(p, ++i, (VkCompareOp*)&v->compareOp, VK_COMPARE_OP_ALWAYS);
        break;
    case 0x05508F74:
        // 0x05508F74 compareMask
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->compareMask, 0);
        break;
    case 0xBC7C3AEE:
        // 0xBC7C3AEE writeMask
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->writeMask, 0);
        break;
    case 0xB2EFFB5D:
        // 0xB2EFFB5D reference
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->reference, 0);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineDepthStencilState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineDepthStencilState* v = (struct mmVKPipelineDepthStencilState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0x1394262D:
        // 0x1394262D depthTestEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->depthTestEnable, VK_TRUE);
        break;
    case 0xFE17DCD7:
        // 0xFE17DCD7 depthWriteEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->depthWriteEnable, VK_TRUE);
        break;
    case 0x4EB1A7CA:
        // 0x4EB1A7CA depthCompareOp
        i = mmParseJSON_DecodeVKCompareOp(p, ++i, (VkCompareOp*)&v->depthCompareOp, VK_COMPARE_OP_LESS_OR_EQUAL);
        break;
    case 0x02ACE823:
        // 0x02ACE823 depthBoundsTestEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->depthBoundsTestEnable, VK_FALSE);
        break;
    case 0x3BF0D968:
        // 0x3BF0D968 stencilTestEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->stencilTestEnable, VK_FALSE);
        break;
    case 0x1202E853:
        // 0x1202E853 front
        i = mmParseJSON_DecodeObject(p, ++i, &v->front, &mmParseJSON_DecodeVKStencilOpState);
        break;
    case 0x5EC270E0:
        // 0x5EC270E0 back
        i = mmParseJSON_DecodeObject(p, ++i, &v->back, &mmParseJSON_DecodeVKStencilOpState);
        break;
    case 0x3869A716:
        // 0x3869A716 minDepthBounds
        i = mmParseJSON_DecodeFloat(p, ++i, &v->minDepthBounds, 0.0f);
        break;
    case 0xFBDAA466:
        // 0xFBDAA466 maxDepthBounds
        i = mmParseJSON_DecodeFloat(p, ++i, &v->maxDepthBounds, 0.0f);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int
mmParseJSON_DecodeVKPipelineMultisampleState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineMultisampleState* v = (struct mmVKPipelineMultisampleState*)(o);
    switch (h)
    {
    case 0xEE7D6E43:
        // 0xEE7D6E43 flags
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->flags, 0);
        break;
    case 0xDB8AB845:
        // 0xDB8AB845 rasterizationSamples
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->rasterizationSamples, 0x00000000);
        break;
    case 0x36603332:
        // 0x36603332 sampleShadingEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->sampleShadingEnable, VK_FALSE);
        break;
    case 0xDC78270E:
        // 0xDC78270E minSampleShading
        i = mmParseJSON_DecodeFloat(p, ++i, &v->minSampleShading, 0.0f);
        break;
    case 0xE2574CAE:
        // 0xE2574CAE sampleMask
        i = mmParseJSON_DecodeNumberVector(p, ++i, &v->sampleMask, &MM_UINT32_ZERO, &mmValue_AToUInt32);
        break;
    case 0xD4FC8D0A:
        // 0xD4FC8D0A alphaToCoverageEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->alphaToCoverageEnable, VK_FALSE);
        break;
    case 0xF8D5D0A6:
        // 0xF8D5D0A6 alphaToOneEnable
        i = mmParseJSON_DecodeSInt32(p, ++i, &v->alphaToOneEnable, VK_FALSE);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}

int 
mmParseJSON_DecodeVKPipelineArgs(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o)
{
    struct mmVKPipelineArgs* v = (struct mmVKPipelineArgs*)(o);
    switch (h)
    {
    case 0xE0EE144D:
        // 0xE0EE144D pvii
        i = mmParseJSON_DecodeObject(p, ++i, &v->pvii, &mmParseJSON_DecodeVKPipelineVertexInputInfo);
        break;
    case 0x4E577541:
        // 0x4E577541 dslp
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->dslp, &mmParseJSON_DecodeVKPushConstantRange);
        break;
    case 0x6D32AE9B:
        // 0x6D32AE9B pssi
        i = mmParseJSON_DecodeObjectArray(p, ++i, &v->pssi, &mmParseJSON_DecodeVKPipelineShaderStageInfo);
        break;
    case 0xF95BC474:
        // 0xF95BC474 layouts
        i = mmParseJSON_DecodeStringVector(p, ++i, &v->layouts, "");
        break;
    case 0x56F6CFD7:
        // 0x56F6CFD7 passName
        i = mmParseJSON_DecodeString(p, ++i, &v->passName, "");
        break;
    case 0xC8BBA3B1:
        // 0xC8BBA3B1 samples
        i = mmParseJSON_DecodeUInt32(p, ++i, &v->samples, 0);
        break;
    case 0xBED08B3D:
        // 0xBED08B3D inputAssembly
        i = mmParseJSON_DecodeObject(p, ++i, &v->inputAssembly, &mmParseJSON_DecodeVKPipelineInputAssemblyState);
        break;
    case 0x00A50D31:
        // 0x00A50D31 tessellation
        i = mmParseJSON_DecodeObject(p, ++i, &v->tessellation, &mmParseJSON_DecodeVKPipelineTessellationState);
        break;
    case 0xBF1A121D:
        // 0xBF1A121D rasterization
        i = mmParseJSON_DecodeObject(p, ++i, &v->rasterization, &mmParseJSON_DecodeVKPipelineRasterizationState);
        break;
    case 0x6FE47D7D:
        // 0x6FE47D7D colorBlend
        i = mmParseJSON_DecodeObject(p, ++i, &v->colorBlend, &mmParseJSON_DecodeVKPipelineColorBlendState);
        break;
    case 0x8AE29BDF:
        // 0x8AE29BDF depthStencil
        i = mmParseJSON_DecodeObject(p, ++i, &v->depthStencil, &mmParseJSON_DecodeVKPipelineDepthStencilState);
        break;
    case 0xC050B6B2:
        // 0xC050B6B2 multisample
        i = mmParseJSON_DecodeObject(p, ++i, &v->multisample, &mmParseJSON_DecodeVKPipelineMultisampleState);
        break;
    default:
        i = mmParseJSON_SkipToken(p, i + 1);
        break;
    }
    return i;
}
