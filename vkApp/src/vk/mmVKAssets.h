/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKAssets_h__
#define __mmVKAssets_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKPipelineCache.h"
#include "vk/mmVKSamplerPool.h"
#include "vk/mmVKImageLoader.h"
#include "vk/mmVKShaderLoader.h"
#include "vk/mmVKLayoutLoader.h"
#include "vk/mmVKPassLoader.h"
#include "vk/mmVKPipelineLoader.h"
#include "vk/mmVKPoolLoader.h"
#include "vk/mmVKSheetLoader.h"
#include "vk/mmVKTFFileIO.h"
#include "vk/mmVKTFModelLoader.h"

#include "core/mmPrefix.h"

struct mmPackageAssets;
struct mmFileContext;
struct mmVKUploader;
struct mmVKDevice;

struct mmVKAssetsDefault
{
    struct mmVKImageInfo* pImage2DTransparent;
    struct mmVKImageInfo* pImage2DWhite;
    struct mmVKImageInfo* pImageCubeMap;

    struct mmVKImageInfo* pImageLUTCharlie;
    struct mmVKImageInfo* pImageLUTGGX;
    struct mmVKImageInfo* pImageLUTSheenE;

    struct mmVKSamplerInfo* pSamplerNearestRepeat;
    struct mmVKSamplerInfo* pSamplerNearestEdge;
    struct mmVKSamplerInfo* pSamplerLinearRepeat;
    struct mmVKSamplerInfo* pSamplerLinearEdge;

    struct mmVKTFMaterial hMaterialDefault;
};

void
mmVKAssetsDefault_Init(
    struct mmVKAssetsDefault*                      p);

void
mmVKAssetsDefault_Destroy(
    struct mmVKAssetsDefault*                      p);

VkResult
mmVKAssetsDefault_PrepareImage(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader);

void
mmVKAssetsDefault_DiscardImage(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader);

VkResult
mmVKAssetsDefault_PrepareSampler(
    struct mmVKAssetsDefault*                      p,
    struct mmVKSamplerPool*                        pSamplerPool);

void
mmVKAssetsDefault_DiscardSampler(
    struct mmVKAssetsDefault*                      p,
    struct mmVKSamplerPool*                        pSamplerPool);

VkResult
mmVKAssetsDefault_Prepare(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader,
    struct mmVKSamplerPool*                        pSamplerPool,
    struct mmVKPoolLoader*                         pPoolLoader);

void
mmVKAssetsDefault_Discard(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader,
    struct mmVKSamplerPool*                        pSamplerPool);

struct mmVKAssets
{
    struct mmPackageAssets* pPackageAssets;
    struct mmFileContext* pFileContext;
    struct mmVKUploader* pUploader;
    struct mmVKDevice* pDevice;

    struct mmVKPipelineCache vPipelineCache;
    struct mmVKSamplerPool vSamplerPool;

    struct mmVKSheetLoader vSheetLoader;
    struct mmVKImageLoader vImageLoader;
    struct mmVKShaderLoader vShaderLoader;
    struct mmVKLayoutLoader vLayoutLoader;
    struct mmVKPoolLoader vPoolLoader;
    struct mmVKPassLoader vPassLoader;
    struct mmVKPipelineLoader vPipelineLoader;
    struct mmVKTFModelLoader vModelLoader;

    struct mmVKAssetsDefault vAssetsDefault;
    struct mmVKTFFileIO vFileIO;
};

void
mmVKAssets_Init(
    struct mmVKAssets*                             p);

void
mmVKAssets_Destroy(
    struct mmVKAssets*                             p);

void
mmVKAssets_SetPackageAssets(
    struct mmVKAssets*                             p,
    struct mmPackageAssets*                        pPackageAssets);

void
mmVKAssets_SetFileContext(
    struct mmVKAssets*                             p,
    struct mmFileContext*                          pFileContext);

void
mmVKAssets_SetUploader(
    struct mmVKAssets*                             p,
    struct mmVKUploader*                           pUploader);

void
mmVKAssets_SetDevice(
    struct mmVKAssets*                             p,
    struct mmVKDevice*                             pDevice);

void
mmVKAssets_SetPipelineCacheName(
    struct mmVKAssets*                             p,
    const char*                                    pPathName);

void
mmVKAssets_SetTranscodeFmt(
    struct mmVKAssets*                             p,
    ktx_transcode_fmt_e                            transcodefmt);

void
mmVKAssets_SetSamples(
    struct mmVKAssets*                             p,
    VkSampleCountFlagBits                          samples);

VkResult
mmVKAssets_Prepare(
    struct mmVKAssets*                             p);

void
mmVKAssets_Discard(
    struct mmVKAssets*                             p);

VkResult
mmVKAssets_PrepareDefaultPass(
    struct mmVKAssets*                             p,
    struct mmVKSwapchain*                          pSwapchain);

void
mmVKAssets_DiscardDefaultPass(
    struct mmVKAssets*                             p);

VkResult
mmVKAssets_PrepareDefaultPipeline(
    struct mmVKAssets*                             p);

void
mmVKAssets_DiscardDefaultPipeline(
    struct mmVKAssets*                             p);

/* Helper function */

void
mmVKAssets_LoadPipelineFromName(
    struct mmVKAssets*                             p,
    const char*                                    pPipelineName);

void
mmVKAssets_UnloadPipelineByName(
    struct mmVKAssets*                             p,
    const char*                                    pPipelineName);

#include "core/mmSuffix.h"

#endif//__mmVKAssets_h__
