/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKAssets.h"

#include "vk/mmVKImageLoader.h"

#include "core/mmLogger.h"

void
mmVKAssetsDefault_Init(
    struct mmVKAssetsDefault*                      p)
{
    p->pImage2DTransparent = NULL;
    p->pImage2DWhite = NULL;
    p->pImageCubeMap = NULL;

    p->pImageLUTCharlie = NULL;
    p->pImageLUTGGX = NULL;
    p->pImageLUTSheenE = NULL;

    p->pSamplerNearestRepeat = NULL;
    p->pSamplerNearestEdge = NULL;
    p->pSamplerLinearRepeat = NULL;
    p->pSamplerLinearEdge = NULL;

    mmVKTFMaterial_Init(&p->hMaterialDefault);
}

void
mmVKAssetsDefault_Destroy(
    struct mmVKAssetsDefault*                      p)
{
    mmVKTFMaterial_Destroy(&p->hMaterialDefault);

    p->pSamplerLinearEdge = NULL;
    p->pSamplerLinearRepeat = NULL;
    p->pSamplerNearestEdge = NULL;
    p->pSamplerNearestRepeat = NULL;

    p->pImageLUTSheenE = NULL;
    p->pImageLUTGGX = NULL;
    p->pImageLUTCharlie = NULL;

    p->pImageCubeMap = NULL;
    p->pImage2DWhite = NULL;
    p->pImage2DTransparent = NULL;
}

VkResult
mmVKAssetsDefault_PrepareImage(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pImageLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pImageLoader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pImage2DTransparent = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/transparent2x2.png");
        if (mmVKImageInfo_Invalid(p->pImage2DTransparent))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImage2DTransparent failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImage2DTransparent = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImage2DTransparent);

        p->pImage2DWhite = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/white2x2.png");
        if (mmVKImageInfo_Invalid(p->pImage2DWhite))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImage2DWhite failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImage2DWhite = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImage2DWhite);

        p->pImageCubeMap = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/cubemap2x2.ktx2");
        if (mmVKImageInfo_Invalid(p->pImageCubeMap))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImageCubeMap failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageCubeMap = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageCubeMap);

        p->pImageLUTCharlie = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/lut_charlie.png");
        if (mmVKImageInfo_Invalid(p->pImageLUTCharlie))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImageLUTCharlie failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageLUTCharlie = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageLUTCharlie);

        p->pImageLUTGGX = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/lut_ggx.png");
        if (mmVKImageInfo_Invalid(p->pImageLUTGGX))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImageLUTGGX failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageLUTGGX = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageLUTGGX);

        p->pImageLUTSheenE = mmVKImageLoader_LoadFromFile(
            pImageLoader,
            "images/lut_sheen_E.png");
        if (mmVKImageInfo_Invalid(p->pImageLUTSheenE))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageLoader_LoadFromFile pImageLUTSheenE failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pImageLUTSheenE = NULL;
            break;
        }
        mmVKImageInfo_Increase(p->pImageLUTSheenE);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

void
mmVKAssetsDefault_DiscardImage(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader)
{
    mmVKImageInfo_Decrease(p->pImageLUTSheenE);
    p->pImageLUTSheenE = NULL;

    mmVKImageInfo_Decrease(p->pImageLUTGGX);
    p->pImageLUTGGX = NULL;

    mmVKImageInfo_Decrease(p->pImageLUTCharlie);
    p->pImageLUTCharlie = NULL;

    mmVKImageInfo_Decrease(p->pImageCubeMap);
    p->pImageCubeMap = NULL;

    mmVKImageInfo_Decrease(p->pImage2DWhite);
    p->pImage2DWhite = NULL;

    mmVKImageInfo_Decrease(p->pImage2DTransparent);
    p->pImage2DTransparent = NULL;
}

VkResult
mmVKAssetsDefault_PrepareSampler(
    struct mmVKAssetsDefault*                      p,
    struct mmVKSamplerPool*                        pSamplerPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pSamplerPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSamplerPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pSamplerNearestRepeat = mmVKSamplerPool_Add(
            pSamplerPool, 
            &mmVKSamplerNearestRepeat);
        if (mmVKSamplerInfo_Invalid(p->pSamplerNearestRepeat))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerNearestRepeat failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerNearestRepeat = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerNearestRepeat);

        p->pSamplerNearestEdge = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerNearestEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerNearestEdge))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerNearestEdge failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerNearestEdge = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerNearestEdge);

        p->pSamplerLinearRepeat = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerLinearRepeat);
        if (mmVKSamplerInfo_Invalid(p->pSamplerLinearRepeat))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerLinearRepeat failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerLinearRepeat = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerLinearRepeat);

        p->pSamplerLinearEdge = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerLinearEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerLinearEdge))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerLinearEdge failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerLinearEdge = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerLinearEdge);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

void
mmVKAssetsDefault_DiscardSampler(
    struct mmVKAssetsDefault*                      p,
    struct mmVKSamplerPool*                        pSamplerPool)
{
    mmVKSamplerInfo_Decrease(p->pSamplerLinearEdge);
    p->pSamplerLinearEdge = NULL;

    mmVKSamplerInfo_Decrease(p->pSamplerLinearRepeat);
    p->pSamplerLinearRepeat = NULL;

    mmVKSamplerInfo_Decrease(p->pSamplerNearestEdge);
    p->pSamplerNearestEdge = NULL;

    mmVKSamplerInfo_Decrease(p->pSamplerNearestRepeat);
    p->pSamplerNearestRepeat = NULL;
}

VkResult
mmVKAssetsDefault_Prepare(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader,
    struct mmVKSamplerPool*                        pSamplerPool,
    struct mmVKPoolLoader*                         pPoolLoader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pUploader = pImageLoader->pUploader;

        err = mmVKAssetsDefault_PrepareImage(p, pImageLoader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKAssetsDefault_PrepareImage failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKAssetsDefault_PrepareSampler(p,   pSamplerPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKAssetsDefault_PrepareSampler failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMaterial_PrepareDefault(
            &p->hMaterialDefault,
            pUploader,
            pPoolLoader,
            mmVKAssetsNameDefault.pool,
            "mmVKTFMaterialDefault");
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_PrepareDefault failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKAssetsDefault_Discard(
    struct mmVKAssetsDefault*                      p,
    struct mmVKImageLoader*                        pImageLoader,
    struct mmVKSamplerPool*                        pSamplerPool)
{
    struct mmVKUploader* pUploader = NULL;

    pUploader = pImageLoader->pUploader;

    mmVKTFMaterial_DiscardDefault(&p->hMaterialDefault, pUploader);

    mmVKAssetsDefault_DiscardSampler(p, pSamplerPool);
    mmVKAssetsDefault_DiscardImage(p, pImageLoader);
}

void
mmVKAssets_Init(
    struct mmVKAssets*                             p)
{
    p->pPackageAssets = NULL;
    p->pFileContext = NULL;
    p->pUploader = NULL;

    mmVKPipelineCache_Init(&p->vPipelineCache);
    mmVKSamplerPool_Init(&p->vSamplerPool);

    mmVKSheetLoader_Init(&p->vSheetLoader);
    mmVKImageLoader_Init(&p->vImageLoader);
    mmVKShaderLoader_Init(&p->vShaderLoader);
    mmVKLayoutLoader_Init(&p->vLayoutLoader);
    mmVKPoolLoader_Init(&p->vPoolLoader);
    mmVKPassLoader_Init(&p->vPassLoader);
    mmVKPipelineLoader_Init(&p->vPipelineLoader);
    mmVKTFModelLoader_Init(&p->vModelLoader);

    mmVKAssetsDefault_Init(&p->vAssetsDefault);
    mmVKTFFileIO_Init(&p->vFileIO);

    mmVKPoolLoader_SetLayoutLoader(&p->vPoolLoader, &p->vLayoutLoader);

    mmVKPipelineLoader_SetPipelineCache(&p->vPipelineLoader, &p->vPipelineCache);
    mmVKPipelineLoader_SetShaderLoader(&p->vPipelineLoader, &p->vShaderLoader);
    mmVKPipelineLoader_SetLayoutLoader(&p->vPipelineLoader, &p->vLayoutLoader);
    mmVKPipelineLoader_SetPassLoader(&p->vPipelineLoader, &p->vPassLoader);

    mmVKTFModelLoader_SetAssets(&p->vModelLoader, p);
    mmVKTFModelLoader_SetAssetsName(&p->vModelLoader, &mmVKAssetsNameDefault);
}

void
mmVKAssets_Destroy(
    struct mmVKAssets*                             p)
{
    mmVKTFFileIO_Destroy(&p->vFileIO);
    mmVKAssetsDefault_Destroy(&p->vAssetsDefault);

    mmVKTFModelLoader_Destroy(&p->vModelLoader);
    mmVKPipelineLoader_Destroy(&p->vPipelineLoader);
    mmVKPassLoader_Destroy(&p->vPassLoader);
    mmVKPoolLoader_Destroy(&p->vPoolLoader);
    mmVKLayoutLoader_Destroy(&p->vLayoutLoader);
    mmVKShaderLoader_Destroy(&p->vShaderLoader);
    mmVKImageLoader_Destroy(&p->vImageLoader);
    mmVKSheetLoader_Destroy(&p->vSheetLoader);

    mmVKSamplerPool_Destroy(&p->vSamplerPool);
    mmVKPipelineCache_Destroy(&p->vPipelineCache);

    p->pUploader = NULL;
    p->pFileContext = NULL;
    p->pPackageAssets = NULL;
}

void
mmVKAssets_SetPackageAssets(
    struct mmVKAssets*                             p,
    struct mmPackageAssets*                        pPackageAssets)
{
    p->pPackageAssets = pPackageAssets;
}

void
mmVKAssets_SetFileContext(
    struct mmVKAssets*                             p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;

    mmVKShaderLoader_SetFileContext(&p->vShaderLoader, pFileContext);
    mmVKImageLoader_SetFileContext(&p->vImageLoader, pFileContext);
    mmVKSheetLoader_SetFileContext(&p->vSheetLoader, pFileContext);
    mmVKLayoutLoader_SetFileContext(&p->vLayoutLoader, pFileContext);
    mmVKPoolLoader_SetFileContext(&p->vPoolLoader, pFileContext);
    mmVKPassLoader_SetFileContext(&p->vPassLoader, pFileContext);
    mmVKPipelineLoader_SetFileContext(&p->vPipelineLoader, pFileContext);

    mmVKTFFileIO_SetFileContext(&p->vFileIO, pFileContext);
}

void
mmVKAssets_SetUploader(
    struct mmVKAssets*                             p,
    struct mmVKUploader*                           pUploader)
{
    p->pUploader = pUploader;

    mmVKSamplerPool_SetUploader(&p->vSamplerPool, pUploader);
    mmVKShaderLoader_SetUploader(&p->vShaderLoader, pUploader);
    mmVKImageLoader_SetUploader(&p->vImageLoader, pUploader);
}

void
mmVKAssets_SetDevice(
    struct mmVKAssets*                             p,
    struct mmVKDevice*                             pDevice)
{
    p->pDevice = pDevice;

    mmVKLayoutLoader_SetDevice(&p->vLayoutLoader, pDevice);
    mmVKPoolLoader_SetDevice(&p->vPoolLoader, pDevice);
    mmVKPassLoader_SetDevice(&p->vPassLoader, pDevice);
    mmVKPipelineLoader_SetDevice(&p->vPipelineLoader, pDevice);
}

void
mmVKAssets_SetPipelineCacheName(
    struct mmVKAssets*                             p,
    const char*                                    pPathName)
{
    mmVKPipelineCache_SetCacheName(&p->vPipelineCache, pPathName);
}

void
mmVKAssets_SetTranscodeFmt(
    struct mmVKAssets*                             p,
    ktx_transcode_fmt_e                            transcodefmt)
{
    mmVKImageLoader_SetTranscodeFmt(&p->vImageLoader, transcodefmt);
}

void
mmVKAssets_SetSamples(
    struct mmVKAssets*                             p,
    VkSampleCountFlagBits                          samples)
{
    mmVKPipelineLoader_SetSamples(&p->vPipelineLoader, samples);
}

VkResult
mmVKAssets_Prepare(
    struct mmVKAssets*                             p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        const char* pPathName = "";
        struct mmVKPipelineCacheCreateInfo hPipelineCacheInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        pPathName = mmString_CStr(&p->vPipelineCache.hCacheName);
        hPipelineCacheInfo.pPackageAssets = p->pPackageAssets;
        hPipelineCacheInfo.pFileContext = p->pFileContext;
        hPipelineCacheInfo.pPathName = pPathName;
        err = mmVKPipelineCache_Create(&p->vPipelineCache, p->pDevice, &hPipelineCacheInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineCache_Create failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKTFPrepareDefaultAssets(p, &mmVKAssetsNameDefault);

        err = mmVKAssetsDefault_Prepare(
            &p->vAssetsDefault, 
            &p->vImageLoader,
            &p->vSamplerPool,
            &p->vPoolLoader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKAssetsDefault_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKAssets_Discard(
    struct mmVKAssets*                             p)
{
    mmVKAssetsDefault_Discard(&p->vAssetsDefault, &p->vImageLoader, &p->vSamplerPool);

    mmVKTFDiscardDefaultAssets(p, &mmVKAssetsNameDefault);

    mmVKPipelineLoader_UnloadComplete(&p->vPipelineLoader);
    mmVKPassLoader_UnloadComplete(&p->vPassLoader);
    mmVKPoolLoader_UnloadComplete(&p->vPoolLoader);
    mmVKLayoutLoader_UnloadComplete(&p->vLayoutLoader);
    mmVKShaderLoader_UnloadComplete(&p->vShaderLoader);
    mmVKImageLoader_UnloadComplete(&p->vImageLoader);
    mmVKSheetLoader_UnloadComplete(&p->vSheetLoader);

    mmVKSamplerPool_Reset(&p->vSamplerPool);

    mmVKPipelineCache_Delete(&p->vPipelineCache, p->pDevice);
}

VkResult
mmVKAssets_PrepareDefaultPass(
    struct mmVKAssets*                             p,
    struct mmVKSwapchain*                          pSwapchain)
{
    mmVKPassLoaderPrepareDefaultPass(&p->vPassLoader, pSwapchain);
    return VK_SUCCESS;
}

void
mmVKAssets_DiscardDefaultPass(
    struct mmVKAssets*                             p)
{
    mmVKPassLoaderDiscardDefaultPass(&p->vPassLoader);
}

VkResult
mmVKAssets_PrepareDefaultPipeline(
    struct mmVKAssets*                             p)
{
    // It is port API. Do nothing here.
    return VK_SUCCESS;
}

void
mmVKAssets_DiscardDefaultPipeline(
    struct mmVKAssets*                             p)
{
    mmVKTFModelLoader_UnloadComplete(&p->vModelLoader);
    mmVKTFModelLoader_UnloadCompletePipelineInfo(&p->vModelLoader, &p->vPipelineLoader);
}

void
mmVKAssets_LoadPipelineFromName(
    struct mmVKAssets*                             p,
    const char*                                    pPipelineName)
{
    char hLayoutPath[128] = { 0 };
    char hPipelinePath[128] = { 0 };
    
    struct mmVKLayoutLoader* pLayoutLoader = &p->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &p->vPoolLoader;
    struct mmVKPipelineLoader* pPipelineLoader = &p->vPipelineLoader;

    sprintf(hLayoutPath, "pipeline/%s.layout.json", pPipelineName);
    sprintf(hPipelinePath, "pipeline/%s.pipeline.json", pPipelineName);
    
    mmVKLayoutLoader_LoadFromFile(pLayoutLoader, pPipelineName, hLayoutPath);
    mmVKPoolLoader_LoadFromLayoutName(pPoolLoader, pPipelineName, pPipelineName);
    mmVKPipelineLoader_LoadFromFile(pPipelineLoader, pPipelineName, hPipelinePath);
}

void
mmVKAssets_UnloadPipelineByName(
    struct mmVKAssets*                             p,
    const char*                                    pPipelineName)
{
    struct mmVKLayoutLoader* pLayoutLoader = &p->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &p->vPoolLoader;
    struct mmVKPipelineLoader* pPipelineLoader = &p->vPipelineLoader;
    
    mmVKPipelineLoader_UnloadByName(pPipelineLoader, pPipelineName);
    mmVKPoolLoader_UnloadByName(pPoolLoader, pPipelineName);
    mmVKLayoutLoader_UnloadByName(pLayoutLoader, pPipelineName);
}
