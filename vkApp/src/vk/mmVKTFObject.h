/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKTFObject_h__
#define __mmVKTFObject_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKNode.h"
#include "vk/mmVKTFModelLoader.h"

#include "core/mmPrefix.h"

struct mmVKScene;
struct mmVKPipeline;
struct mmVKAssets;

void
mmVKTFNodeJoints_Init(
    struct mmVectorValue*                          p);

void
mmVKTFNodeJoints_Destroy(
    struct mmVectorValue* p);

void
mmVKTFNodeWeights_Init(
    struct mmVectorValue*                          p);

void
mmVKTFNodeWeights_Destroy(
    struct mmVectorValue*                          p);

struct mmVKTFNode
{
    struct mmString name;
    struct mmVKNode node;

    struct mmVKTFSkin* skin;
    struct mmVKTFMesh* mesh;

    struct mmVKPushConstants pushConstant;

    struct mmVectorValue jointMatrices;
    struct mmVectorValue weights;

    struct mmVKBuffer bufferJoints;
    struct mmVKBuffer bufferWeights;
    struct mmVKDescriptorSet vDescriptorSet;
};

void
mmVKTFNode_Init(
    struct mmVKTFNode*                             p);

void
mmVKTFNode_Destroy(
    struct mmVKTFNode*                             p);

VkResult
mmVKTFNode_PrepareUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFNode_DiscardUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTFNode_UploadeUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTFNode_DescriptorSetProduce(
    struct mmVKTFNode*                             p,
    struct mmVKDescriptorPool*                     pDescriptorPool);

void
mmVKTFNode_DescriptorSetRecycle(
    struct mmVKTFNode*                             p,
    struct mmVKDescriptorPool*                     pDescriptorPool);

VkResult
mmVKTFNode_DescriptorSetUpdate(
    struct mmVKTFNode*                             p,
    VkDevice                                       device);

VkResult
mmVKTFNode_Prepare(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFNode*                             pGLTFNode,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKDescriptorPool*                     pDescriptorPool);

void
mmVKTFNode_Discard(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDescriptorPool*                     pDescriptorPool);

VkResult
mmVKTFNode_UpdateJoins(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmVectorValue*                          nodes,
    struct mmVectorValue*                          skins);

void
mmVKTFNodes_Init(
    struct mmVectorValue*                          p);

void
mmVKTFNodes_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFNodes_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKDescriptorPool*                     pDescriptorPool);

void
mmVKTFNodes_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDescriptorPool*                     pDescriptorPool);

struct mmVKTFScene
{
    struct mmString name;
    struct mmVectorVpt nodes;
};

void
mmVKTFScene_Init(
    struct mmVKTFScene*                            p);

void
mmVKTFScene_Destroy(
    struct mmVKTFScene*                            p);

VkResult
mmVKTFScene_Prepare(
    struct mmVKTFScene*                            p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFScene*                            pGLTFScene,
    struct mmVectorValue*                          nodes);

void
mmVKTFScene_Discard(
    struct mmVKTFScene*                            p);

void
mmVKTFScene_PrepareRootNode(
    struct mmVKTFScene*                            p,
    struct mmVKNode*                               node);

void
mmVKTFScene_DiscardRootNode(
    struct mmVKTFScene*                            p,
    struct mmVKNode*                               node);

void
mmVKTFScenes_Init(
    struct mmVectorValue*                          p);

void
mmVKTFScenes_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFScenes_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          nodes);

void
mmVKTFScenes_Discard(
    struct mmVectorValue*                          p);

struct mmVKTFAnimationSampler
{
    struct mmVKTFAccessor input;
    struct mmVKTFAccessor output;
    enum mmGLTFInterpolation_t interpolation;
};

void
mmVKTFAnimationSampler_Init(
    struct mmVKTFAnimationSampler*                 p);

void
mmVKTFAnimationSampler_Destroy(
    struct mmVKTFAnimationSampler*                 p);

VkResult
mmVKTFAnimationSampler_IsValid(
    const struct mmVKTFAnimationSampler*           p);

float
mmVKTFAnimationSampler_GetMaxTime(
    const struct mmVKTFAnimationSampler*           p);

VkResult
mmVKTFAnimationSampler_Prepare(
    struct mmVKTFAnimationSampler*                 p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimationSampler*                 pGLTFAnimationSampler);

void
mmVKTFAnimationSampler_Discard(
    struct mmVKTFAnimationSampler*                 p);

void
mmVKTFAnimationSamplers_Init(
    struct mmVectorValue*                          p);

void
mmVKTFAnimationSamplers_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFAnimationSamplers_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation);

void
mmVKTFAnimationSamplers_Discard(
    struct mmVectorValue*                          p);

struct mmVKTFAnimationChannel
{
    struct mmVKTFAnimationSampler* sampler;
    struct mmVKTFNode* node;
    enum mmGLTFAnimationPath_t path;
    size_t prevKey;
    float prevTime;
};

void
mmVKTFAnimationChannel_Init(
    struct mmVKTFAnimationChannel*                 p);

void
mmVKTFAnimationChannel_Destroy(
    struct mmVKTFAnimationChannel*                 p);

VkResult
mmVKTFAnimationChannel_Prepare(
    struct mmVKTFAnimationChannel*                 p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimationChannel*                 pGLTFAnimationChannel,
    struct mmVectorValue*                          nodes);

void
mmVKTFAnimationChannel_Discard(
    struct mmVKTFAnimationChannel*                 p);

void
mmVKTFAnimationChannel_SetTime(
    struct mmVKTFAnimationChannel*                 p,
    float                                          time,
    float                                          maxTime);

void
mmVKTFAnimationChannels_Init(
    struct mmVectorValue*                          p);

void
mmVKTFAnimationChannels_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFAnimationChannels_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation,
    struct mmVectorValue*                          nodes);

void
mmVKTFAnimationChannels_Discard(
    struct mmVectorValue*                          p);

struct mmVKTFAnimation
{
    struct mmString name;
    struct mmVectorValue samplers;
    struct mmVectorValue channels;

    int loop;
    int state;
    float currentTime;
    float maxTime;
    float speed;
};

void
mmVKTFAnimation_Init(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_Destroy(
    struct mmVKTFAnimation*                        p);

VkResult
mmVKTFAnimation_Prepare(
    struct mmVKTFAnimation*                        p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation,
    struct mmVectorValue*                          nodes);

void
mmVKTFAnimation_Discard(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_SetTime(
    struct mmVKTFAnimation*                        p,
    float                                          time);

void
mmVKTFAnimation_Update(
    struct mmVKTFAnimation*                        p,
    float                                          deltaTime);

void
mmVKTFAnimation_Play(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_Stop(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_Pause(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_Resume(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimation_Rewind(
    struct mmVKTFAnimation*                        p);

void
mmVKTFAnimations_Init(
    struct mmVectorValue*                          p);

void
mmVKTFAnimations_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFAnimations_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          nodes);

void
mmVKTFAnimations_Discard(
    struct mmVectorValue*                          p);

void
mmVKTFAnimations_MakeAnimationMap(
    const struct mmVectorValue*                    p,
    struct mmRbtreeStrVpt*                         m);

struct mmVKTFObject
{
    struct mmVKTFModelInfo* pModelInfo;
    struct mmVKPoolInfo* pPoolInfo;

    struct mmVKNode node;
    
    struct mmVectorValue nodes;
    struct mmVectorValue animations;
    struct mmVectorValue scenes;

    struct mmRbtreeStrVpt animationsMap;
};

void
mmVKTFObject_Init(
    struct mmVKTFObject*                           p);

void
mmVKTFObject_Destroy(
    struct mmVKTFObject*                           p);

void
mmVKTFObject_UpdateAnimations(
    struct mmVKTFObject*                           p,
    float                                          deltaTime);

VkResult
mmVKTFObject_UpdateJoins(
    struct mmVKTFObject*                           p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTFObject_Update(
    struct mmVKTFObject*                           p,
    struct mmVKUploader*                           pUploader,
    float                                          interval);

struct mmVKTFAnimation*
mmVKTFObject_GetAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name);

struct mmVKTFAnimation*
mmVKTFObject_PlayAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name);

struct mmVKTFAnimation*
mmVKTFObject_StopAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name);

void
mmVKTFObject_StopAllAnimation(
    struct mmVKTFObject*                           p);

void
mmVKTFObject_PrepareRootNode(
    struct mmVKTFObject*                           p);

void
mmVKTFObject_DiscardRootNode(
    struct mmVKTFObject*                           p);

VkResult
mmVKTFObject_Prepare(
    struct mmVKTFObject*                           p,
    struct mmVKAssets*                             pAssets,
    const char*                                    path);

void
mmVKTFObject_Discard(
    struct mmVKTFObject*                           p,
    struct mmVKAssets*                             pAssets);

void
mmVKTFPrimitive_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode,
    struct mmVKTFPrimitive*                        pVKTFPrimitive);

void
mmVKTFMesh_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode,
    struct mmVKTFMesh*                             pVKTFMesh);

void
mmVKTFNode_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode);

void
mmVKTFScene_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFScene*                            pVKTFScene);

void
mmVKTFObject_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFObject*                           pVKTFObject);

#include "core/mmSuffix.h"

#endif//__mmVKTFObject_h__
