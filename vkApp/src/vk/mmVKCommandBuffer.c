/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKCommandBuffer.h"

#include "vk/mmVKBuffer.h"
#include "vk/mmVKImage.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

VkResult
mmVKCommandBufferUpdateGPUOnlyBuffer(
    VkCommandBuffer                                cmdBuffer,
    VmaAllocator                                   vmaAllocator,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset)
{
    VkResult result = VK_ERROR_UNKNOWN;
    struct mmVKBuffer stage;

    do
    {
        VkBufferCreateInfo hSrcCreateInfo;
        VmaAllocationCreateInfo hVmaAllocInfo;
        VkBufferCopy hBufferCopy;
        void* pMappedBuffer = NULL;

        mmMemset(&hVmaAllocInfo, 0, sizeof(VmaAllocationCreateInfo));

        hSrcCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hSrcCreateInfo.pNext = NULL;
        hSrcCreateInfo.flags = 0;
        hSrcCreateInfo.size = length;
        hSrcCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        hSrcCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hSrcCreateInfo.queueFamilyIndexCount = 0;
        hSrcCreateInfo.pQueueFamilyIndices = NULL;

        hVmaAllocInfo.flags = 0;
        hVmaAllocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        result = vmaCreateBuffer(
            vmaAllocator,
            &hSrcCreateInfo,
            &hVmaAllocInfo,
            &stage.buffer,
            &stage.allocation,
            NULL);
        if (VK_SUCCESS != result)
        {
            break;
        }

        result = vmaMapMemory(
            vmaAllocator, 
            stage.allocation, 
            (void**)&pMappedBuffer);
        if (VK_SUCCESS != result)
        {
            break;
        }

        mmMemcpy(pMappedBuffer, buffer + offset, length);
        vmaUnmapMemory(vmaAllocator, stage.allocation);

        hBufferCopy.srcOffset = 0;
        hBufferCopy.dstOffset = vOffset;
        hBufferCopy.size = length;
        vkCmdCopyBuffer(cmdBuffer, stage.buffer, pBuffer->buffer, 1, &hBufferCopy);

    } while (0);

    if (VK_NULL_HANDLE != stage.buffer && NULL != stage.allocation)
    {
        vmaDestroyBuffer(vmaAllocator, stage.buffer, stage.allocation);
    }

    return result;
}

VkResult
mmVKCommandBufferUpdateCPU2GPUBuffer(
    VkCommandBuffer                                cmdBuffer,
    VmaAllocator                                   vmaAllocator,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset)
{
    VkResult result = VK_ERROR_UNKNOWN;
    void* pMappedBuffer = NULL;

    result = vmaMapMemory(
        vmaAllocator, 
        pBuffer->allocation, 
        (void**)&pMappedBuffer);
    if (VK_SUCCESS == result)
    {
        mmMemcpy(((char*)pMappedBuffer) + vOffset, buffer + offset, length);
        vmaUnmapMemory(vmaAllocator, pBuffer->allocation);
    }

    return result;
}

void
mmVKCommandBufferCopyBufferToImage(
    VkCommandBuffer                                cmdBuffer,
    const struct mmVKBuffer*                       pBuffer,
    struct mmVKImage*                              pImage,
    VkImageLayout                                  finalLayout)
{
    uint32_t regionCount = 1;
    VkBufferImageCopy copyRegions;
    VkImageSubresourceRange subresourceRange;

    copyRegions.bufferOffset = 0;
    copyRegions.bufferRowLength = 0;
    copyRegions.bufferImageHeight = 0;
    copyRegions.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    copyRegions.imageSubresource.mipLevel = 0;
    copyRegions.imageSubresource.baseArrayLayer = 0;
    copyRegions.imageSubresource.layerCount = 1;
    copyRegions.imageOffset.x = 0;
    copyRegions.imageOffset.y = 0;
    copyRegions.imageOffset.z = 0;
    copyRegions.imageExtent.width = pImage->width;
    copyRegions.imageExtent.height = pImage->height;
    copyRegions.imageExtent.depth = pImage->depth;

    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = 1;
    subresourceRange.baseArrayLayer = 0;
    subresourceRange.layerCount = 1;

    // Image barrier for optimal image (target)
    // Optimal image will be used as destination for the copy
    mmVKSetImageLayout(
        cmdBuffer,
        pImage->image,
        pImage->imageLayout,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        &subresourceRange);

    // copy buffer to image.
    vkCmdCopyBufferToImage(
        cmdBuffer,
        pBuffer->buffer,
        pImage->image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        regionCount,
        &copyRegions);

    // Change texture image layout to shader read after all mip levels have been copied
    mmVKSetImageLayout(
        cmdBuffer,
        pImage->image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        finalLayout,
        &subresourceRange);

    pImage->imageLayout = finalLayout;
}

VkResult
mmVKCommandBufferBeginCommandBuffer(
    VkCommandBuffer                                cmdBuffer)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkCommandBufferBeginInfo hCmdBeginInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == cmdBuffer)
        {

            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hCmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        hCmdBeginInfo.pNext = NULL;
        hCmdBeginInfo.flags = 0;
        hCmdBeginInfo.pInheritanceInfo = NULL;

        err = vkBeginCommandBuffer(cmdBuffer, &hCmdBeginInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkBeginCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKCommandBufferEndCommandBuffer(
    VkCommandBuffer                                cmdBuffer)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = vkEndCommandBuffer(cmdBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkEndCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKCommandBufferSetViewport(
    VkCommandBuffer                                cmdBuffer,
    float                                          hViewport[4],
    float                                          hMinDepth,
    float                                          hMaxDepth)
{
    VkResult err = VK_ERROR_UNKNOWN;

    if (VK_NULL_HANDLE == cmdBuffer)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d"
            " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
        err = VK_ERROR_INITIALIZATION_FAILED;
    }
    else
    {
        VkViewport viewport;

        viewport.x = hViewport[0];
        viewport.y = hViewport[1];
        viewport.width  = hViewport[2] - hViewport[0];
        viewport.height = hViewport[3] - hViewport[1];
        viewport.minDepth = hMinDepth;
        viewport.maxDepth = hMaxDepth;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        err = VK_SUCCESS;
    }

    return err;
}

VkResult
mmVKCommandBufferSetScissor(
    VkCommandBuffer                                cmdBuffer,
    uint32_t                                       hScissor[4])
{
    VkResult err = VK_ERROR_UNKNOWN;

    if (VK_NULL_HANDLE == cmdBuffer)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d"
            " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
        err = VK_ERROR_INITIALIZATION_FAILED;
    }
    else
    {
        VkRect2D scissor;
        scissor.offset.x = hScissor[0];
        scissor.offset.y = hScissor[1];
        scissor.extent.width  = hScissor[2] - hScissor[0];
        scissor.extent.height = hScissor[3] - hScissor[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        err = VK_SUCCESS;
    }

    return err;
}

void
mmVKCommandBuffer_SetWindowSize(
    struct mmVKCommandBuffer*                      p,
    uint32_t                                       hWindowSize[2])
{
    p->hWindowSize[0] = hWindowSize[0];
    p->hWindowSize[1] = hWindowSize[1];

    p->hViewport[0] = 0;
    p->hViewport[1] = 0;
    p->hViewport[2] = (float)hWindowSize[0];
    p->hViewport[3] = (float)hWindowSize[1];

    p->hScissor[0] = 0;
    p->hScissor[1] = 0;
    p->hScissor[2] = hWindowSize[0];
    p->hScissor[3] = hWindowSize[1];
}

void
mmVKCommandBuffer_SetCommandBuffer(
    struct mmVKCommandBuffer*                      p,
    VkCommandBuffer                                cmdBuffer)
{
    p->cmdBuffer = cmdBuffer;
}

void
mmVKCommandBuffer_SetViewport(
    struct mmVKCommandBuffer*                      p,
    float                                          hMinDepth,
    float                                          hMaxDepth)
{
    mmVKCommandBufferSetViewport(p->cmdBuffer, p->hViewport, hMinDepth, hMaxDepth);
}

void
mmVKCommandBuffer_SetScissor(
    struct mmVKCommandBuffer*                      p)
{
    mmVKCommandBufferSetScissor(p->cmdBuffer, p->hScissor);
}

void 
mmVKCommandBuffer_ScissorStorage(
    struct mmVKCommandBuffer*                      p, 
    uint32_t                                       hScissorCache[4], 
    uint32_t                                       hScissorValue[4])
{
    uint32_t hScissor[4];
    // Intersection
    hScissor[0] = hScissorValue[0] > p->hScissor[0] ? hScissorValue[0] : p->hScissor[0];
    hScissor[0] = hScissorValue[0] < p->hScissor[2] ? hScissorValue[0] : p->hScissor[2];
    
    hScissor[1] = hScissorValue[1] > p->hScissor[1] ? hScissorValue[1] : p->hScissor[1];
    hScissor[1] = hScissorValue[1] < p->hScissor[3] ? hScissorValue[1] : p->hScissor[3];
    
    hScissor[2] = hScissorValue[2] > p->hScissor[0] ? hScissorValue[2] : p->hScissor[0];
    hScissor[2] = hScissorValue[2] < p->hScissor[2] ? hScissorValue[2] : p->hScissor[2];
    
    hScissor[3] = hScissorValue[3] > p->hScissor[1] ? hScissorValue[3] : p->hScissor[1];
    hScissor[3] = hScissorValue[3] < p->hScissor[3] ? hScissorValue[3] : p->hScissor[3];

    mmMemcpy(hScissorCache, p->hScissor, sizeof(p->hScissor));
    mmMemcpy(p->hScissor, hScissor, sizeof(p->hScissor));
    mmVKCommandBufferSetScissor(p->cmdBuffer, p->hScissor);
}

void 
mmVKCommandBuffer_ScissorRestore(
    struct mmVKCommandBuffer*                      p, 
    uint32_t                                       hScissorCache[4])
{
    mmMemcpy(p->hScissor, hScissorCache, sizeof(p->hScissor));
    mmVKCommandBufferSetScissor(p->cmdBuffer, p->hScissor);
}

void
mmVKCommandBuffer_ScissorValueClamp(
    struct mmVKCommandBuffer*                      p,
    const float                                    hWorldRect[4],
    uint32_t                                       hScissorValue[4])
{
    hScissorValue[0] = (hWorldRect[0] < /*         */0.0f) ? /*            */0 : (uint32_t)/*  */hWorldRect[0];
    hScissorValue[0] = (hWorldRect[0] > p->hWindowSize[0]) ? p->hWindowSize[0] : (uint32_t)/*  */hWorldRect[0];
    
    hScissorValue[1] = (hWorldRect[1] < /*         */0.0f) ? /*            */0 : (uint32_t)/*  */hWorldRect[1];
    hScissorValue[1] = (hWorldRect[1] > p->hWindowSize[1]) ? p->hWindowSize[1] : (uint32_t)/*  */hWorldRect[1];
    
    hScissorValue[2] = (hWorldRect[2] < /*         */0.0f) ? /*            */0 : (uint32_t)ceilf(hWorldRect[2]);
    hScissorValue[2] = (hWorldRect[2] > p->hWindowSize[0]) ? p->hWindowSize[0] : (uint32_t)ceilf(hWorldRect[2]);
    
    hScissorValue[3] = (hWorldRect[3] < /*         */0.0f) ? /*            */0 : (uint32_t)ceilf(hWorldRect[3]);
    hScissorValue[3] = (hWorldRect[3] > p->hWindowSize[1]) ? p->hWindowSize[1] : (uint32_t)ceilf(hWorldRect[3]);
}
