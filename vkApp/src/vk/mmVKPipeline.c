/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPipeline.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKShaderLoader.h"
#include "vk/mmVKLayoutLoader.h"
#include "vk/mmVKPassLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static
VkResult
mmVKPipeline_IncreaseLayoutsReference(
    struct mmVectorVpt*                            dsli,
    struct mmVectorValue*                          dsls);

static
void
mmVKPipeline_DecreaseLayoutsReference(
    struct mmVectorVpt*                            dsli);

static
void
mmVKPipeline_MakeStencilOpState(
    VkStencilOpState*                              s,
    const struct mmVKStencilOpState*               state);

static
void
mmVKPipeline_MakeInputAssembly(
    VkPipelineInputAssemblyStateCreateInfo*        ia,
    const struct mmVKPipelineInputAssemblyState*   inputAssembly);

static
void
mmVKPipeline_MakeTessellation(
    VkPipelineTessellationStateCreateInfo*         ts,
    const struct mmVKPipelineTessellationState*    tessellation);

static
void
mmVKPipeline_MakeRasterization(
    VkPipelineRasterizationStateCreateInfo*        rs,
    const struct mmVKPipelineRasterizationState*   rasterization);

static
void
mmVKPipeline_MakeColorBlend(
    VkPipelineColorBlendStateCreateInfo*           cb,
    const struct mmVKPipelineColorBlendState*      colorBlend);

static
void
mmVKPipeline_MakeDepthStencil(
    VkPipelineDepthStencilStateCreateInfo*         ds,
    const struct mmVKPipelineDepthStencilState*    depthStencil);

static
void
mmVKPipeline_MakeMultisample(
    VkPipelineMultisampleStateCreateInfo*          ms,
    const struct mmVKPipelineMultisampleState*     multisample,
    VkSampleCountFlagBits                          samples);

void
mmVKPipeline_Init(
    struct mmVKPipeline*                           p)
{
    mmVectorVpt_Init(&p->dsli);
    p->pipelineLayout = VK_NULL_HANDLE;
    p->pipeline = VK_NULL_HANDLE;
}

void
mmVKPipeline_Destroy(
    struct mmVKPipeline*                           p)
{
    p->pipeline = VK_NULL_HANDLE;
    p->pipelineLayout = VK_NULL_HANDLE;
    mmVectorVpt_Destroy(&p->dsli);
}

VkResult
mmVKPipeline_Create(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCreateInfo*           info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmVectorValue dsls;

        p->pPassInfo = info->pPassInfo;
        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            p->pPassInfo = NULL;
            break;
        }
        mmVKPassInfo_Increase(p->pPassInfo);

        mmVectorVpt_CopyFromValue(&p->dsli, info->dsli);

        mmVectorValue_Init(&dsls);
        mmVectorValue_SetElement(&dsls, sizeof(VkDescriptorSetLayout));
        mmVKPipeline_IncreaseLayoutsReference(&p->dsli, &dsls);
        err = mmVKPipeline_PreparePipelineLayout(p, pDevice, info->dslp, &dsls);
        mmVectorValue_Destroy(&dsls);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipeline_PreparePipelineLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKPipeline_PreparePipeline(p, pDevice, info);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipeline_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKPipeline_Delete(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice)
{
    mmVKPipeline_DiscardPipeline(p, pDevice);
    mmVKPipeline_DiscardPipelineLayout(p, pDevice);

    mmVKPassInfo_Decrease(p->pPassInfo);
    p->pPassInfo = NULL;

    mmVKPipeline_DecreaseLayoutsReference(&p->dsli);
}

VkResult
mmVKPipeline_PreparePipelineLayout(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVectorValue*                    dslp,
    const struct mmVectorValue*                    dsls)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        const struct mmVectorValue* l = dsls;
        const struct mmVectorValue* c = dslp;

        VkPipelineLayoutCreateInfo hPipelineLayoutInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        hPipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        hPipelineLayoutInfo.pNext = NULL;
        hPipelineLayoutInfo.flags = 0;
        hPipelineLayoutInfo.setLayoutCount = (uint32_t)l->size;
        hPipelineLayoutInfo.pSetLayouts = (const VkDescriptorSetLayout*)l->arrays;
        hPipelineLayoutInfo.pushConstantRangeCount = (uint32_t)c->size;
        hPipelineLayoutInfo.pPushConstantRanges = (const VkPushConstantRange*)c->arrays;

        err = vkCreatePipelineLayout(
            device,
            &hPipelineLayoutInfo,
            pAllocator,
            &p->pipelineLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreatePipelineLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}
void 
mmVKPipeline_DiscardPipelineLayout(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (VK_NULL_HANDLE != p->pipelineLayout)
        {
            vkDestroyPipelineLayout(device, p->pipelineLayout, pAllocator);
            p->pipelineLayout = VK_NULL_HANDLE;
        }

    } while (0);
}

VkResult
mmVKPipeline_PreparePipeline(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCreateInfo*           info)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;
        
        const struct mmVKPipelineInputAssemblyState*   inputAssembly = info->inputAssembly;
        const struct mmVKPipelineTessellationState*    tessellation = info->tessellation;
        const struct mmVKPipelineRasterizationState*   rasterization = info->rasterization;
        const struct mmVKPipelineColorBlendState*      colorBlend = info->colorBlend;
        const struct mmVKPipelineDepthStencilState*    depthStencil = info->depthStencil;
        const struct mmVKPipelineMultisampleState*     multisample = info->multisample;

        const struct mmVectorValue* psmi = info->psmi;
        VkRenderPass renderPass = info->pPassInfo->vRenderPass.renderPass;
        VkPipelineCache pipelineCache = info->pipelineCache;

        const struct mmVectorValue* vibd = &info->pvii->vibd;
        const struct mmVectorValue* viad = &info->pvii->viad;
        
        VkGraphicsPipelineCreateInfo pipeline;

        VkPipelineVertexInputStateCreateInfo vi;
        VkPipelineInputAssemblyStateCreateInfo ia;
        VkPipelineTessellationStateCreateInfo ts;
        VkPipelineRasterizationStateCreateInfo rs;
        VkPipelineColorBlendStateCreateInfo cb;
        VkPipelineDepthStencilStateCreateInfo ds;
        VkPipelineViewportStateCreateInfo vp;
        VkPipelineMultisampleStateCreateInfo ms;

        VkDynamicState dynamicStateEnables[2];
        VkPipelineDynamicStateCreateInfo dynamicState;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        pAllocator = pDevice->pAllocator;
        
        mmMemset(dynamicStateEnables, 0, sizeof(dynamicStateEnables));

        dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicState.pNext = NULL;
        dynamicState.flags = 0;
        dynamicState.dynamicStateCount = 0;
        dynamicState.pDynamicStates = dynamicStateEnables;

        vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vi.pNext = NULL;
        vi.flags = 0;
        vi.vertexBindingDescriptionCount = (uint32_t)vibd->size;
        vi.pVertexBindingDescriptions = (const VkVertexInputBindingDescription*)vibd->arrays;
        vi.vertexAttributeDescriptionCount = (uint32_t)viad->size;
        vi.pVertexAttributeDescriptions = (const VkVertexInputAttributeDescription*)viad->arrays;
        
        mmVKPipeline_MakeInputAssembly(&ia, inputAssembly);
        mmVKPipeline_MakeTessellation(&ts, tessellation);
        mmVKPipeline_MakeRasterization(&rs, rasterization);
        mmVKPipeline_MakeColorBlend(&cb, colorBlend);
        mmVKPipeline_MakeDepthStencil(&ds, depthStencil);
        mmVKPipeline_MakeMultisample(&ms, multisample, info->samples);

        vp.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        vp.pNext = NULL;
        vp.flags = 0;
        vp.viewportCount = 1;
        vp.pViewports = NULL;
        vp.scissorCount = 1;
        vp.pScissors = NULL;
        
        dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
        dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;
        
        pipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipeline.pNext = NULL;
        pipeline.flags = 0;
        pipeline.stageCount = (uint32_t)psmi->size;
        pipeline.pStages = (const VkPipelineShaderStageCreateInfo*)psmi->arrays;
        pipeline.pVertexInputState = &vi;
        pipeline.pInputAssemblyState = &ia;
        pipeline.pTessellationState = &ts;
        pipeline.pViewportState = &vp;
        pipeline.pRasterizationState = &rs;
        pipeline.pMultisampleState = &ms;
        pipeline.pDepthStencilState = &ds;
        pipeline.pColorBlendState = &cb;
        pipeline.pDynamicState = &dynamicState;
        pipeline.layout = p->pipelineLayout;
        pipeline.renderPass = renderPass;
        pipeline.subpass = 0;
        pipeline.basePipelineHandle = VK_NULL_HANDLE;
        pipeline.basePipelineIndex = 0;

        err = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipeline, pAllocator, &p->pipeline);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateGraphicsPipelines failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
    
    return err;
}
void
mmVKPipeline_DiscardPipeline(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (VK_NULL_HANDLE != p->pipeline)
        {
            vkDestroyPipeline(device, p->pipeline, pAllocator);
            p->pipeline = VK_NULL_HANDLE;
        }
    } while (0);
}

static
VkResult
mmVKPipeline_IncreaseLayoutsReference(
    struct mmVectorVpt*                            dsli,
    struct mmVectorValue*                          dsls)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i;
        struct mmVKLayoutInfo* pLayoutInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmVectorValue_Reset(dsls);

        if (0 == dsli->size)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        else
        {
            err = VK_SUCCESS;
        }

        mmVectorValue_AllocatorMemory(dsls, dsli->size);

        for (i = 0; i < dsli->size; i++)
        {
            pLayoutInfo = (struct mmVKLayoutInfo*)mmVectorVpt_At(dsli, i);
            if (mmVKLayoutInfo_Invalid(pLayoutInfo))
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " pLayoutInfo is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_INITIALIZATION_FAILED;
                break;
            }
            mmVKLayoutInfo_Increase(pLayoutInfo);

            mmVectorValue_SetIndex(dsls, i, &pLayoutInfo->descriptorSetLayout);
        }
    } while (0);

    return err;
}

static
void
mmVKPipeline_DecreaseLayoutsReference(
    struct mmVectorVpt*                            dsli)
{
    size_t i;
    struct mmVKLayoutInfo* pLayoutInfo;

    for (i = 0; i < dsli->size; i++)
    {
        pLayoutInfo = (struct mmVKLayoutInfo*)mmVectorVpt_At(dsli, i);
        mmVKLayoutInfo_Decrease(pLayoutInfo);
    }

    mmVectorVpt_Reset(dsli);
}

static
void
mmVKPipeline_MakeStencilOpState(
    VkStencilOpState*                              s,
    const struct mmVKStencilOpState*               state)
{
    s->failOp = (VkStencilOp)state->failOp;
    s->passOp = (VkStencilOp)state->passOp;
    s->depthFailOp = (VkStencilOp)state->depthFailOp;
    s->compareOp = (VkCompareOp)state->compareOp;
    s->compareMask = state->compareMask;
    s->writeMask = state->writeMask;
    s->reference = state->reference;
}

static
void
mmVKPipeline_MakeInputAssembly(
    VkPipelineInputAssemblyStateCreateInfo*        ia,
    const struct mmVKPipelineInputAssemblyState*   inputAssembly)
{
    ia->sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    ia->pNext = NULL;

    if (NULL == inputAssembly)
    {
        ia->flags = 0;
        ia->topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        ia->primitiveRestartEnable = VK_FALSE;
    }
    else
    {
        ia->flags = (VkPipelineInputAssemblyStateCreateFlags)inputAssembly->flags;
        ia->topology = (VkPrimitiveTopology)inputAssembly->topology;
        ia->primitiveRestartEnable = (VkBool32)inputAssembly->primitiveRestartEnable;
    }
}

static
void
mmVKPipeline_MakeTessellation(
    VkPipelineTessellationStateCreateInfo*         ts,
    const struct mmVKPipelineTessellationState*    tessellation)
{
    ts->sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    ts->pNext = NULL;

    if (NULL == tessellation)
    {
        ts->flags = 0;
        ts->patchControlPoints = 0;
    }
    else
    {
        ts->flags = (VkPipelineTessellationStateCreateFlags)tessellation->flags;
        ts->patchControlPoints = (VkBool32)tessellation->patchControlPoints;
    }
}

static
void
mmVKPipeline_MakeRasterization(
    VkPipelineRasterizationStateCreateInfo*        rs,
    const struct mmVKPipelineRasterizationState*   rasterization)
{
    rs->sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rs->pNext = NULL;

    if (NULL == rasterization)
    {
        rs->flags = 0;
        rs->depthClampEnable = VK_FALSE;
        rs->rasterizerDiscardEnable = VK_FALSE;
        rs->polygonMode = VK_POLYGON_MODE_FILL;
        rs->cullMode = VK_CULL_MODE_BACK_BIT;
        rs->frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rs->depthBiasEnable = VK_FALSE;
        rs->depthBiasConstantFactor = 0.0f;
        rs->depthBiasClamp = 0.0f;
        rs->depthBiasSlopeFactor = 0.0f;
        rs->lineWidth = 1.0f;
    }
    else
    {
        rs->flags = (VkPipelineRasterizationStateCreateFlags)rasterization->flags;
        rs->depthClampEnable = (VkBool32)rasterization->depthClampEnable;
        rs->rasterizerDiscardEnable = (VkBool32)rasterization->rasterizerDiscardEnable;
        rs->polygonMode = (VkPolygonMode)rasterization->polygonMode;
        rs->cullMode = (VkCullModeFlags)rasterization->cullMode;
        rs->frontFace = (VkFrontFace)rasterization->frontFace;
        rs->depthBiasEnable = (VkBool32)rasterization->depthBiasEnable;
        rs->depthBiasConstantFactor = rasterization->depthBiasConstantFactor;
        rs->depthBiasClamp = rasterization->depthBiasClamp;
        rs->depthBiasSlopeFactor = rasterization->depthBiasSlopeFactor;
        rs->lineWidth = rasterization->lineWidth;
    }
}

static
void
mmVKPipeline_MakeColorBlend(
    VkPipelineColorBlendStateCreateInfo*           cb,
    const struct mmVKPipelineColorBlendState*      colorBlend)
{
    cb->sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    cb->pNext = NULL;

    if (NULL == colorBlend)
    {
        static const VkPipelineColorBlendAttachmentState att_state[1] = 
        {
            {
                VK_TRUE,
                VK_BLEND_FACTOR_SRC_ALPHA,
                VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
                VK_BLEND_OP_ADD,
                VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
                VK_BLEND_FACTOR_SRC_ALPHA,
                VK_BLEND_OP_ADD,
                0x0000000F,
            },
        };

        cb->flags = 0;
        cb->logicOpEnable = VK_FALSE;
        cb->logicOp = VK_LOGIC_OP_CLEAR;
        cb->attachmentCount = 1;
        cb->pAttachments = att_state;
        mmMemset(cb->blendConstants, 0, sizeof(cb->blendConstants));
    }
    else
    {
        cb->flags = (VkPipelineColorBlendStateCreateFlags)colorBlend->flags;
        cb->logicOpEnable = (VkBool32)colorBlend->logicOpEnable;
        cb->logicOp = (VkLogicOp)colorBlend->logicOp;
        cb->attachmentCount = (uint32_t)colorBlend->attachments.size;
        cb->pAttachments = (const VkPipelineColorBlendAttachmentState*)colorBlend->attachments.arrays;
        mmMemcpy(cb->blendConstants, colorBlend->blendConstants, sizeof(cb->blendConstants));
    }
}

static
void
mmVKPipeline_MakeDepthStencil(
    VkPipelineDepthStencilStateCreateInfo*         ds,
    const struct mmVKPipelineDepthStencilState*    depthStencil)
{
    ds->sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ds->pNext = NULL;

    if (NULL == depthStencil)
    {
        ds->flags = 0;
        ds->depthTestEnable = VK_TRUE;
        ds->depthWriteEnable = VK_TRUE;
        ds->depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
        ds->depthBoundsTestEnable = VK_FALSE;
        ds->stencilTestEnable = VK_FALSE;
        ds->front.failOp = VK_STENCIL_OP_KEEP;
        ds->front.passOp = VK_STENCIL_OP_KEEP;
        ds->front.depthFailOp = VK_STENCIL_OP_KEEP;
        ds->front.compareOp = VK_COMPARE_OP_ALWAYS;
        ds->front.compareMask = 0;
        ds->front.writeMask = 0;
        ds->front.reference = 0;
        ds->back.failOp = VK_STENCIL_OP_KEEP;
        ds->back.passOp = VK_STENCIL_OP_KEEP;
        ds->back.depthFailOp = VK_STENCIL_OP_KEEP;
        ds->back.compareOp = VK_COMPARE_OP_ALWAYS;
        ds->back.compareMask = 0;
        ds->back.writeMask = 0;
        ds->back.reference = 0;
        ds->minDepthBounds = 0.0f;
        ds->maxDepthBounds = 0.0f;
    }
    else
    {
        ds->flags = (VkPipelineDepthStencilStateCreateFlags)depthStencil->flags;
        ds->depthTestEnable = (VkBool32)depthStencil->depthTestEnable;
        ds->depthWriteEnable = (VkBool32)depthStencil->depthWriteEnable;
        ds->depthCompareOp = (VkCompareOp)depthStencil->depthCompareOp;
        ds->depthBoundsTestEnable = (VkBool32)depthStencil->depthBoundsTestEnable;
        ds->stencilTestEnable = (VkBool32)depthStencil->stencilTestEnable;
        mmVKPipeline_MakeStencilOpState(&ds->front, &depthStencil->front);
        mmVKPipeline_MakeStencilOpState(&ds->back, &depthStencil->back);
        ds->minDepthBounds = depthStencil->minDepthBounds;
        ds->maxDepthBounds = depthStencil->maxDepthBounds;
    }
}

static
void
mmVKPipeline_MakeMultisample(
    VkPipelineMultisampleStateCreateInfo*          ms,
    const struct mmVKPipelineMultisampleState*     multisample,
    VkSampleCountFlagBits                          samples)
{
    ms->sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    ms->pNext = NULL;

    if (NULL == multisample)
    {
        ms->flags = 0;
        ms->rasterizationSamples = samples;
        ms->sampleShadingEnable = VK_FALSE;
        ms->minSampleShading = 0.0f;
        ms->pSampleMask = NULL;
        ms->alphaToCoverageEnable = VK_FALSE;
        ms->alphaToOneEnable = VK_FALSE;
    }
    else
    {
        VkSampleCountFlagBits rs = (VkSampleCountFlagBits)multisample->rasterizationSamples;
        ms->flags = (VkPipelineMultisampleStateCreateFlags)multisample->flags;
        ms->rasterizationSamples = (0x00000000 == rs) ? samples : rs;
        ms->sampleShadingEnable = (VkBool32)multisample->sampleShadingEnable;
        ms->minSampleShading = multisample->minSampleShading;
        ms->pSampleMask = (0 == multisample->sampleMask.size) ? NULL : (const VkSampleMask*)(multisample->sampleMask.arrays);
        ms->alphaToCoverageEnable = (VkBool32)multisample->alphaToCoverageEnable;
        ms->alphaToOneEnable = (VkBool32)multisample->alphaToOneEnable;
    }
}
