/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPassLoader.h"

#include "vk/mmVKSwapchain.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKPassInfo_Init(
    struct mmVKPassInfo*                           p)
{
    mmString_Init(&p->name);
    mmVKRenderPass_Init(&p->vRenderPass);
    p->reference = 0;
}

void
mmVKPassInfo_Destroy(
    struct mmVKPassInfo*                           p)
{
    p->reference = 0;
    mmVKRenderPass_Destroy(&p->vRenderPass);
    mmString_Destroy(&p->name);
}

int
mmVKPassInfo_Invalid(
    const struct mmVKPassInfo*                     p)
{
    return
        (NULL == p) ||
        (VK_NULL_HANDLE == p->vRenderPass.renderPass);
}

void
mmVKPassInfo_Increase(
    struct mmVKPassInfo*                           p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKPassInfo_Decrease(
    struct mmVKPassInfo*                           p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKPassInfo_Prepare(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain)
{
    return mmVKRenderPass_Create(&p->vRenderPass, pDevice, pSwapchain);
}

VkResult
mmVKPassInfo_PrepareDepthFormat(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice,
    VkFormat                                       depthFormat)
{
    return mmVKRenderPass_CreateDepth(&p->vRenderPass, pDevice, depthFormat);
}

void
mmVKPassInfo_Discard(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice)
{
    mmVKRenderPass_Delete(&p->vRenderPass, pDevice);
}

void
mmVKPassLoader_Init(
    struct mmVKPassLoader*                         p)
{
    p->pFileContext = NULL;
    p->pDevice = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
}

void
mmVKPassLoader_Destroy(
    struct mmVKPassLoader*                         p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pDevice = NULL;
    p->pFileContext = NULL;
}

void
mmVKPassLoader_SetFileContext(
    struct mmVKPassLoader*                         p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmVKPassLoader_SetDevice(
    struct mmVKPassLoader*                         p,
    struct mmVKDevice*                             pDevice)
{
    p->pDevice = pDevice;
}

VkResult
mmVKPassLoader_PreparePassInfo(
    struct mmVKPassLoader*                         p,
    const struct mmVKSwapchain*                    pSwapchain,
    struct mmVKPassInfo*                           pPassInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pPassInfo->reference && "reference not zero.");

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKPassInfo_Prepare(pPassInfo, p->pDevice, pSwapchain);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPassInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        pPassInfo->reference = 0;
    } while (0);

    return err;
}

VkResult
mmVKPassLoader_PreparePassInfoDepthFormat(
    struct mmVKPassLoader*                         p,
    VkFormat                                       depthFormat,
    struct mmVKPassInfo*                           pPassInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pPassInfo->reference && "reference not zero.");

        err = mmVKPassInfo_PrepareDepthFormat(pPassInfo, p->pDevice, depthFormat);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPassInfo_PrepareDepthFormat failure.", __FUNCTION__, __LINE__);
            break;
        }

        pPassInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKPassLoader_DiscardPassInfo(
    struct mmVKPassLoader*                         p,
    struct mmVKPassInfo*                           pPassInfo)
{
    assert(0 == pPassInfo->reference && "reference not zero.");
    mmVKPassInfo_Discard(pPassInfo, p->pDevice);
}

void
mmVKPassLoader_UnloadByName(
    struct mmVKPassLoader*                         p,
    const char*                                    name)
{
    struct mmVKPassInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPassInfo*)it->v;
        mmVKPassLoader_DiscardPassInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPassInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKPassLoader_UnloadComplete(
    struct mmVKPassLoader*                         p)
{
    struct mmVKPassInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKPassInfo*)(it->v);
        mmVKPassLoader_DiscardPassInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPassInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKPassInfo*
mmVKPassLoader_LoadFromSwapchain(
    struct mmVKPassLoader*                         p,
    const char*                                    name,
    const struct mmVKSwapchain*                    pSwapchain)
{
    struct mmVKPassInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKPassInfo*)mmMalloc(sizeof(struct mmVKPassInfo));
        mmVKPassInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKPassLoader_PreparePassInfo(p, pSwapchain, s);
    }
    else
    {
        s = (struct mmVKPassInfo*)(it->v);
    }
    return s;
}

struct mmVKPassInfo*
mmVKPassLoader_LoadFromDepthFormat(
    struct mmVKPassLoader*                         p,
    const char*                                    name,
    VkFormat                                       depthFormat)
{
    struct mmVKPassInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKPassInfo*)mmMalloc(sizeof(struct mmVKPassInfo));
        mmVKPassInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKPassLoader_PreparePassInfoDepthFormat(p, depthFormat, s);
    }
    else
    {
        s = (struct mmVKPassInfo*)(it->v);
    }
    return s;
}

struct mmVKPassInfo*
mmVKPassLoader_GetFromName(
    struct mmVKPassLoader*                         p,
    const char*                                    name)
{
    struct mmVKPassInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPassInfo*)(it->v);
    }
    return s;
}

void
mmVKPassLoaderPrepareDefaultPass(
    struct mmVKPassLoader*                         p,
    struct mmVKSwapchain*                          pSwapchain)
{
    VkFormat hDepthFormat = pSwapchain->depthFormat;
    mmVKPassLoader_LoadFromSwapchain(p, mmVKRenderPassMasterName, pSwapchain);
    mmVKPassLoader_LoadFromDepthFormat(p, mmVKRenderPassDepthName, hDepthFormat);
}

void
mmVKPassLoaderDiscardDefaultPass(
    struct mmVKPassLoader*                         p)
{
    mmVKPassLoader_UnloadByName(p, mmVKRenderPassDepthName);
    mmVKPassLoader_UnloadByName(p, mmVKRenderPassMasterName);
}
