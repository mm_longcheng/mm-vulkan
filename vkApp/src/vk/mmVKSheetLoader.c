/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKSheetLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"
#include "core/mmFilePath.h"

#include "algorithm/mmMurmurHash.h"

static 
void 
mmVKSheetFrame_AnalysisVec4I(
    int                                            v[4], 
    int                                            idx, 
    const char**                                   row)
{
    mmValue_AToSInt(row[idx++], &v[0]);
    mmValue_AToSInt(row[idx++], &v[1]);
    mmValue_AToSInt(row[idx++], &v[2]);
    mmValue_AToSInt(row[idx++], &v[3]);
}

static 
void 
mmVKSheetFrame_AnalysisVec2I(
    int                                            v[2], 
    int                                            idx, 
    const char**                                   row)
{
    mmValue_AToSInt(row[idx++], &v[0]);
    mmValue_AToSInt(row[idx++], &v[1]);
}

static 
void 
mmVKSheetFrame_AnalysisVec2F(
    float                                          v[2], 
    int                                            idx, 
    const char**                                   row)
{
    mmValue_AToFloat(row[idx++], &v[0]);
    mmValue_AToFloat(row[idx++], &v[1]);
}

void 
mmVKSheetFrame_Init(
    struct mmVKSheetFrame*                         p)
{
    mmMemset(p, 0, sizeof(struct mmVKSheetFrame));
}

void 
mmVKSheetFrame_Destroy(
    struct mmVKSheetFrame*                         p)
{
    mmMemset(p, 0, sizeof(struct mmVKSheetFrame));
}

void 
mmVKSheetFrame_AnalysisRow(
    struct mmVKSheetFrame*                         p, 
    const char**                                   row)
{
    int idx = 0;

    mmValue_AToSInt(row[idx], &p->id);
    idx += 1;

    mmVKSheetFrame_AnalysisVec4I(p->frame, idx, row);
    idx += 4;

    mmVKSheetFrame_AnalysisVec4I(p->source, idx, row);
    idx += 4;

    mmVKSheetFrame_AnalysisVec2I(p->offset, idx, row);
    idx += 2;

    mmVKSheetFrame_AnalysisVec2I(p->original, idx, row);
    idx += 2;

    mmValue_AToSInt(row[idx], &p->r);
    idx += 1;

    mmValue_AToSInt(row[idx], &p->t);
    idx += 1;

    mmVKSheetFrame_AnalysisVec2F(p->anchor, idx, row);
    idx += 2;

    mmVKSheetFrame_AnalysisVec4I(p->borders, idx, row);
    idx += 4;
}

void
mmVKSheetFrame_SetSheetImage(
    struct mmVKSheetFrame*                         p,
    struct mmVKSheetImage*                         image)
{
    p->image = image;
}

int
mmVKSheetFrame_Invalid(
    const struct mmVKSheetFrame*                   p)
{
    return
        (NULL == p);
}

void
mmVKSheetFrame_Increase(
    struct mmVKSheetFrame*                         p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKSheetFrame_Decrease(
    struct mmVKSheetFrame*                         p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

void
mmVKSheetGetNameByPath(
    const char*                                    path,
    struct mmString*                               name)
{
    struct mmString hQualifiedName;
    struct mmString hBaseName;
    struct mmString hPathName;
    struct mmString hSuffixName;

    mmString_Init(&hQualifiedName);
    mmString_Init(&hBaseName);
    mmString_Init(&hPathName);
    mmString_Init(&hSuffixName);

    mmString_Assigns(&hQualifiedName, path);
    mmPathSplitFileName(&hQualifiedName, &hBaseName, &hPathName);
    mmPathSplitSuffixName(&hBaseName, name, &hSuffixName);

    mmString_Destroy(&hSuffixName);
    mmString_Destroy(&hPathName);
    mmString_Destroy(&hBaseName);
    mmString_Destroy(&hQualifiedName);
}


void 
mmVKSheetImage_Init(
    struct mmVKSheetImage*                         p)
{
    mmString_Init(&p->name);
    mmString_Init(&p->sheetpath);
    mmString_Init(&p->path);
    mmString_Init(&p->image);
    mmMemset(p->imageSize, 0, sizeof(size_t) * 2);
    mmMemset(p->sheetSize, 0, sizeof(size_t) * 2);
    mmRbtreeStringVpt_Init(&p->frames);
    mmVectorVpt_Init(&p->vec);
    p->reference = 0;
}

void 
mmVKSheetImage_Destroy(
    struct mmVKSheetImage*                         p)
{
    mmVKSheetImage_FrameClear(p);
    mmVKSheetImage_FrameIdReset(p);

    p->reference = 0;
    mmVectorVpt_Destroy(&p->vec);
    mmRbtreeStringVpt_Destroy(&p->frames);
    mmMemset(p->sheetSize, 0, sizeof(size_t) * 2);
    mmMemset(p->imageSize, 0, sizeof(size_t) * 2);
    mmString_Destroy(&p->image);
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->sheetpath);
    mmString_Destroy(&p->name);
}

void 
mmVKSheetImage_SetSheetPath(
    struct mmVKSheetImage*                         p,
    const char*                                    pSheetPath)
{
    mmString_Assigns(&p->sheetpath, (const char*)pSheetPath);
}

void 
mmVKSheetImage_GetImagePath(
    struct mmVKSheetImage*                         p,
    struct mmString*                               pImagePath)
{
    if (mmString_Empty(&p->path))
    {
        struct mmString hQualifiedName;
        struct mmString hBaseName;
        struct mmString hPathName;

        mmString_Init(&hQualifiedName);
        mmString_Init(&hBaseName);
        mmString_Init(&hPathName);

        mmString_Assign(&hQualifiedName, &p->sheetpath);
        mmPathSplitFileName(&hQualifiedName, &hBaseName, &hPathName);

        mmString_Assign(pImagePath, &hPathName);
        mmDirectoryHaveSuffix(pImagePath, mmString_CStr(pImagePath));
        mmString_Append(pImagePath, &p->image);

        mmString_Destroy(&hPathName);
        mmString_Destroy(&hBaseName);
        mmString_Destroy(&hQualifiedName);
    }
    else
    {
        mmString_Assign(pImagePath, &p->path);
        mmDirectoryHaveSuffix(pImagePath, mmString_CStr(pImagePath));
        mmString_Append(pImagePath, &p->image);
    }
}

void
mmVKSheetImage_GenerateIdentifyName(
    struct mmVKSheetImage*                         p)
{
    mmVKSheetGetNameByPath(mmString_CStr(&p->sheetpath), &p->name);
}

struct mmVKSheetFrame* 
mmVKSheetImage_AddFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn)
{
    struct mmVKSheetFrame* e = mmVKSheetImage_GetFrame(p, fn);
    if (NULL == e)
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmString hWeakKey;
        mmString_MakeWeaks(&hWeakKey, fn);
        e = (struct mmVKSheetFrame*)mmMalloc(sizeof(struct mmVKSheetFrame));
        mmVKSheetFrame_Init(e);
        it = mmRbtreeStringVpt_Set(&p->frames, &hWeakKey, e);
        e->fn = mmString_CStr(&it->k);
    }
    return e;
}
struct mmVKSheetFrame* 
mmVKSheetImage_GetFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn)
{
    struct mmVKSheetFrame* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, fn);
    it = mmRbtreeStringVpt_GetIterator(&p->frames, &hWeakKey);
    if (NULL != it)
    {
        e = (struct mmVKSheetFrame*)it->v;
    }
    return e;
}

void 
mmVKSheetImage_RmvFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn)
{
    struct mmVKSheetFrame* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, fn);
    it = mmRbtreeStringVpt_GetIterator(&p->frames, &hWeakKey);
    if (NULL != it)
    {
        e = (struct mmVKSheetFrame*)it->v;
        mmRbtreeStringVpt_Erase(&p->frames, it);
        mmVKSheetFrame_Destroy(e);
        mmFree(e);
    }
}

void 
mmVKSheetImage_FrameClear(
    struct mmVKSheetImage*                         p)
{
    struct mmVKSheetFrame* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->frames.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmVKSheetFrame*)(it->v);
        mmRbtreeStringVpt_Erase(&p->frames, it);
        mmVKSheetFrame_Destroy(e);
        mmFree(e);
    }
}

void 
mmVKSheetImage_AllocatorMemory(
    struct mmVKSheetImage*                         p,
    size_t                                         size)
{
    mmVectorVpt_AllocatorMemory(&p->vec, size);
}

void 
mmVKSheetImage_SetFrameById(
    struct mmVKSheetImage*                         p,
    int                                            id, 
    struct mmVKSheetFrame*                         e)
{
    mmVectorVpt_SetIndex(&p->vec, e->id, e);
}

struct mmVKSheetFrame* 
mmVKSheetImage_GetFrameById(
    struct mmVKSheetImage*                         p,
    int                                            id)
{
    return (struct mmVKSheetFrame*)mmVectorVpt_At(&p->vec, id);
}

void 
mmVKSheetImage_FrameIdReset(
    struct mmVKSheetImage*                         p)
{
    mmVectorVpt_Reset(&p->vec);
}

int
mmVKSheetImage_Invalid(
    const struct mmVKSheetImage*                   p)
{
    return
        (NULL == p);
}

void
mmVKSheetImage_Increase(
    struct mmVKSheetImage*                         p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKSheetImage_Decrease(
    struct mmVKSheetImage*                         p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

static 
void 
mmVKSheetImage_DecodeSize(
    size_t                                         size[2], 
    const mmUInt8_t*                               w,
    size_t                                         l)
{
    size_t e = mmCStringFindNextOf((const char*)w, 'x', 0, l);
    // k = w          , e
    // v = w + e + 1  , length - e
    mmUInt8_t* sw = (mmUInt8_t*)(w);
    mmUInt8_t* sh = (mmUInt8_t*)(w + e + 1);
    mmUInt8_t c = sw[e];
    sw[e] = 0;
    mmValue_AToSize((const char*)sw, &size[0]);
    mmValue_AToSize((const char*)sh, &size[1]);
    sw[e] = c;
}

static 
void 
mmVKSheetImage_Decode(
    mmUInt8_t*                                     w, 
    size_t                                         l, 
    mmUInt32_t                                     h, 
    void*                                          o)
{
    struct mmVKSheetImage* v = (struct mmVKSheetImage*)(o);
    switch (h)
    {
    case 0x27A2D463:
        // 0x27A2D463 path
        mmString_Assigns(&v->path, (const char*)w);
        break;
    case 0xE9451CCE:
        // 0xE9451CCE image
        mmString_Assigns(&v->image, (const char*)w);
        break;
    case 0xC73D4A47:
        // 0xC73D4A47 imageSize
        mmVKSheetImage_DecodeSize(v->imageSize, w, l);
        break;
    case 0x74D76271:
        // 0x74D76271 sheetSize
        mmVKSheetImage_DecodeSize(v->sheetSize, w, l);
        break;
    default:
        break;
    }
}

void 
mmVKSheetParse_Init(
    struct mmVKSheetParse*                         p)
{
    mmParseTSV_Init(&p->tsv);
    p->split = 0;
    p->bytes = NULL;
    p->image = NULL;
}

void 
mmVKSheetParse_Destroy(
    struct mmVKSheetParse*                         p)
{
    p->image = NULL;
    p->bytes = NULL;
    p->split = 0;
    mmParseTSV_Destroy(&p->tsv);
}

void 
mmVKSheetParse_Reset(
    struct mmVKSheetParse*                         p)
{
    mmParseTSV_Reset(&p->tsv);
    p->split = 0;
    p->bytes = NULL;
    p->image = NULL;
}

void 
mmVKSheetParse_SetByteBuffer(
    struct mmVKSheetParse*                         p,
    struct mmByteBuffer*                           bytes)
{
    p->bytes = bytes;
}

void
mmVKSheetParse_SetSheetImage(
    struct mmVKSheetParse*                         p,
    struct mmVKSheetImage*                         image)
{
    p->image = image;
}

void 
mmVKSheetParse_AnalysisKeyValuePairs(
    struct mmVKSheetParse*                         p,
    mmUInt8_t*                                     w)
{
    size_t length = strlen((const char*)w);
    size_t m = mmCStringFindNextNotOf((const char*)w, ':', 0, length);
    size_t e = mmCStringFindNextOf((const char*)w, '=', 0, length);

    if (e != mmStringNpos && e != length)
    {
        // k = w + m      , e - m
        // v = w + e + 1  , length - e
        mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)(w + m), (int)(e - m), 0);
        mmUInt8_t* v = w + e + 1;
        mmVKSheetImage_Decode(v, length - e, h, p->image);
    }
}

void 
mmVKSheetParse_AnalysisLine(
    struct mmVKSheetParse*                         p,
    mmUInt8_t*                                     w, 
    size_t                                         z, 
    size_t                                         x, 
    size_t                                         y, 
    size_t                                         i)
{
    switch (w[0])
    {
    case '#':
    {
        // Annotation
        // mmUInt8_t c = w[z];
        // w[z] = 0;
        // printf("%s\n", w);
        // w[z] = c;
    }
    break;

    case ':':
    {
        // key-value pairs.
        mmUInt8_t c = w[z];
        w[z] = 0;
        // printf("%s\n", w);
        mmVKSheetParse_AnalysisKeyValuePairs(p, w);
        w[z] = c;
    }
    break;

    case '\r':
    case '\n':
    case ' ':
    {
        // split line metadata and tsv.
        // printf("--- split line ---\n");
        size_t n = w - p->bytes->buffer;
        size_t idx = ((n < i) && ('\n' == w[1]) && ('\n' != w[0]));
        p->split = n + idx + 1;
    }
    break;

    default:
    {
        // nothing.
        // mmUInt8_t c = w[z];
        // w[z] = 0;
        // printf("%s\n", w);
        // w[z] = c;
    }
    break;
    }
}

void 
mmVKSheetParse_AnalysisMetadata(
    struct mmVKSheetParse*                         p)
{
    size_t i = 0;
    size_t b = 0;
    size_t e = 0;
    size_t x = 0;
    size_t y = 0;

    mmUInt8_t* w = NULL;
    mmUInt8_t* buff = p->bytes->buffer;
    size_t pptr = p->bytes->length;
    size_t gptr = p->bytes->offset;

    i = gptr;

    while (i < pptr)
    {
        switch (buff[i])
        {
        case '\r':
        case '\n':
        {
            w = (buff + gptr + b);
            i += ((i + 1 < pptr) && ('\n' == buff[i + 1]) && ('\n' != buff[i]));
            mmVKSheetParse_AnalysisLine(p, w, e - b, x, y, i);
            
            x = 0;
            y++;
            i++;
            b = i;
            e = i;
        }
        break;

        default:
        {
            i++;
            e++;
        }
        break;
        }

        if (0 != p->split)
        {
            break;
        }
    }

    // w = (buff + gptr + p->split);

    // Metadata must end for split line, we not need handle remaining buffer.
}

void 
mmVKSheetParse_AnalysisTSV(
    struct mmVKSheetParse*                         p)
{
    size_t hMaxColumnNumber = p->image->sheetSize[0];
    size_t hRowNumber = p->image->sheetSize[1];
    size_t hMaxRowNumber = hRowNumber + 1;
    mmUInt8_t* buffer = p->bytes->buffer + p->split;
    size_t length = p->bytes->length - p->split;
    size_t r = 0;
    static size_t s = 1;
    const char** rows = NULL;
    const char* fn = NULL;
    struct mmVKSheetFrame* frame = NULL;

    mmParseTSV_SetMaxNumber(&p->tsv, hMaxRowNumber, hMaxColumnNumber);
    mmParseTSV_AnalysisBuffer(&p->tsv, buffer, length);

    mmVKSheetImage_FrameClear(p->image);
    mmVKSheetImage_AllocatorMemory(p->image, hRowNumber);

    for (r = s;r < p->tsv.hRowNumber;++r)
    {
        // row data.
        rows = mmParseTSV_GetPhysicalRow(&p->tsv, r);

        // frame name at column 21.
        fn = rows[21];

        // add frame.
        frame = mmVKSheetImage_AddFrame(p->image, fn);

        // decode frame data.
        mmVKSheetFrame_AnalysisRow(frame, rows);

        // bind sheet.
        mmVKSheetFrame_SetSheetImage(frame, p->image);

        // make id weak index.
        mmVKSheetImage_SetFrameById(p->image, frame->id, frame);
    }
}

void 
mmVKSheetParse_Analysis(
    struct mmVKSheetParse*                         p)
{
    mmVKSheetParse_AnalysisMetadata(p);
    mmVKSheetParse_AnalysisTSV(p);
}

void 
mmVKSheetLoader_Init(
    struct mmVKSheetLoader*                        p)
{
    p->pFileContext = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    mmVKSheetParse_Init(&p->parse);
}

void 
mmVKSheetLoader_Destroy(
    struct mmVKSheetLoader*                        p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmVKSheetParse_Destroy(&p->parse);
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pFileContext = NULL;
}

void 
mmVKSheetLoader_SetFileContext(
    struct mmVKSheetLoader*                        p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

struct mmVKSheetImage*
mmVKSheetLoader_LoadFromFile(
    struct mmVKSheetLoader*                        p,
    const char*                                    path)
{
    struct mmVKSheetImage* s = NULL;

    do
    {
        struct mmString hWeakPath;
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmByteBuffer bytes;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakPath, path);
        it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakPath);
        if (NULL != it)
        {
            s = (struct mmVKSheetImage*)it->v;
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "mmVKSheetLoader Sheet file: %s not exists.", path);
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        s = (struct mmVKSheetImage*)mmMalloc(sizeof(struct mmVKSheetImage));
        mmVKSheetImage_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakPath, s);
        mmString_MakeWeak(&s->sheetpath, &it->k);
        mmVKSheetImage_GenerateIdentifyName(s);

        mmVKSheetParse_Reset(&p->parse);
        mmVKSheetParse_SetByteBuffer(&p->parse, &bytes);
        mmVKSheetParse_SetSheetImage(&p->parse, s);
        mmVKSheetParse_Analysis(&p->parse);
        mmVKSheetParse_Reset(&p->parse);

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);

    return s;
}

void
mmVKSheetLoader_UnloadByFile(
    struct mmVKSheetLoader*                        p,
    const char*                                    path)
{
    struct mmString hWeakPath;
    struct mmVKSheetImage* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmString_MakeWeaks(&hWeakPath, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakPath);
    if (NULL != it)
    {
        s = (struct mmVKSheetImage*)it->v;
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKSheetImage_Destroy(s);
        mmFree(s);
    }
}

void 
mmVKSheetLoader_UnloadComplete(
    struct mmVKSheetLoader*                        p)
{
    struct mmVKSheetImage* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKSheetImage*)(it->v);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKSheetImage_Destroy(s);
        mmFree(s);
    }
}
