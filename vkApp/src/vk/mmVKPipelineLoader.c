/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPipelineLoader.h"

#include "vk/mmVKPipelineCache.h"
#include "vk/mmVKShaderLoader.h"
#include "vk/mmVKLayoutLoader.h"
#include "vk/mmVKPassLoader.h"
#include "vk/mmVKDescriptorLayout.h"
#include "vk/mmVKJSON.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKPipelineInfo_Init(
    struct mmVKPipelineInfo*                       p)
{
    mmString_Init(&p->name);
    mmVKPipeline_Init(&p->vPipeline);
    p->reference = 0;
}

void
mmVKPipelineInfo_Destroy(
    struct mmVKPipelineInfo*                       p)
{
    p->reference = 0;
    mmVKPipeline_Destroy(&p->vPipeline);
    mmString_Destroy(&p->name);
}

int
mmVKPipelineInfo_Invalid(
    const struct mmVKPipelineInfo*                 p)
{
    return
        (NULL == p) ||
        (VK_NULL_HANDLE == p->vPipeline.pipeline);
}

void
mmVKPipelineInfo_Increase(
    struct mmVKPipelineInfo*                       p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKPipelineInfo_Decrease(
    struct mmVKPipelineInfo*                       p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKPipelineInfo_Prepare(
    struct mmVKPipelineInfo*                       p,
    struct mmVKDevice*                             pDevice,
    struct mmVKPipelineCreateInfo*                 pInfo)
{
    return mmVKPipeline_Create(&p->vPipeline, pDevice, pInfo);
}

void
mmVKPipelineInfo_Discard(
    struct mmVKPipelineInfo*                       p,
    struct mmVKDevice*                             pDevice)
{
    mmVKPipeline_Delete(&p->vPipeline, pDevice);
}

void
mmVKPipelineLoader_Init(
    struct mmVKPipelineLoader*                     p)
{
    p->pFileContext = NULL;
    p->pPipelineCache = NULL;
    p->pShaderLoader = NULL;
    p->pLayoutLoader = NULL;
    p->pPassLoader = NULL;
    p->pDevice = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    mmParseJSON_Init(&p->parse);
    p->samples = 0x00000000;
}

void
mmVKPipelineLoader_Destroy(
    struct mmVKPipelineLoader*                     p)
{
    p->samples = 0x00000000;
    mmParseJSON_Destroy(&p->parse);
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pDevice = NULL;
    p->pPassLoader = NULL;
    p->pLayoutLoader = NULL;
    p->pShaderLoader = NULL;
    p->pPipelineCache = NULL;
    p->pFileContext = NULL;
}

void
mmVKPipelineLoader_SetFileContext(
    struct mmVKPipelineLoader*                     p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmVKPipelineLoader_SetPipelineCache(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineCache*                      pPipelineCache)
{
    p->pPipelineCache = pPipelineCache;
}

void
mmVKPipelineLoader_SetShaderLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKShaderLoader*                       pShaderLoader)
{
    p->pShaderLoader = pShaderLoader;
}

void
mmVKPipelineLoader_SetLayoutLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKLayoutLoader*                       pLayoutLoader)
{
    p->pLayoutLoader = pLayoutLoader;
}

void
mmVKPipelineLoader_SetPassLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPassLoader*                         pPassLoader)
{
    p->pPassLoader = pPassLoader;
}

void
mmVKPipelineLoader_SetDevice(
    struct mmVKPipelineLoader*                     p,
    struct mmVKDevice*                             pDevice)
{
    p->pDevice = pDevice;
}


void
mmVKPipelineLoader_SetSamples(
    struct mmVKPipelineLoader*                     p,
    VkSampleCountFlagBits                          samples)
{
    p->samples = samples;
}

VkResult
mmVKPipelineLoader_PreparePipelineInfo(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineCreateInfo*                 pInfo,
    struct mmVKPipelineInfo*                       pPipelineInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKDevice* pDevice = p->pDevice;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pPipelineInfo->reference && "reference not zero.");

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKPipeline_Create(&pPipelineInfo->vPipeline, pDevice, pInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipeline_Create failure.", __FUNCTION__, __LINE__);
            break;
        }

        pPipelineInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKPipelineLoader_DiscardPipelineInfo(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineInfo*                       pPipelineInfo)
{
    assert(0 == pPipelineInfo->reference && "reference not zero.");
    mmVKPipeline_Delete(&pPipelineInfo->vPipeline, p->pDevice);
}

void
mmVKPipelineLoader_UnloadByName(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name)
{
    struct mmVKPipelineInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPipelineInfo*)it->v;
        mmVKPipelineLoader_DiscardPipelineInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPipelineInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKPipelineLoader_UnloadComplete(
    struct mmVKPipelineLoader*                     p)
{
    struct mmVKPipelineInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKPipelineInfo*)(it->v);
        mmVKPipelineLoader_DiscardPipelineInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPipelineInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKPipelineInfo*
mmVKPipelineLoader_LoadFromInfo(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name,
    struct mmVKPipelineCreateInfo*                 pInfo)
{
    struct mmVKPipelineInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKPipelineInfo*)mmMalloc(sizeof(struct mmVKPipelineInfo));
        mmVKPipelineInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKPipelineLoader_PreparePipelineInfo(p, pInfo, s);
    }
    else
    {
        s = (struct mmVKPipelineInfo*)(it->v);
    }
    return s;
}

struct mmVKPipelineInfo*
mmVKPipelineLoader_LoadFromFile(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name,
    const char*                                    path)
{
    struct mmVKPipelineInfo* s = NULL;

    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmString hWeakKey;

        int i;

        const uint8_t* data = NULL;
        size_t size = 0;

        struct mmByteBuffer bytes;

        struct mmVKPipelineArgs hArgs;
        struct mmVectorVpt dsli;

        struct mmVKPassInfo* pPassInfo = NULL;
        const char* pPassName = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakKey, name);
        it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
        if (NULL != it)
        {
            s = (struct mmVKPipelineInfo*)(it->v);
            break;
        }

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pShaderLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pShaderLoader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pLayoutLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutLoader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pPassLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassLoader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        mmVKPipelineArgs_Init(&hArgs);
        mmVectorVpt_Init(&dsli);

        mmParseJSON_Reset(&p->parse);
        mmParseJSON_SetJsonBuffer(&p->parse, (const char*)data, (size_t)size);
        mmParseJSON_Analysis(&p->parse);
        i = mmParseJSON_DecodeObject(&p->parse, 0, &hArgs, &mmParseJSON_DecodeVKPipelineArgs);
        if (i <= 0)
        {
            if (p->parse.code <= 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON AnalysisMetadata failure.", __FUNCTION__, __LINE__);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON root is invalid.", __FUNCTION__, __LINE__);
            }

            // do not break here.
        }
        else
        {
            VkResult err = VK_ERROR_UNKNOWN;
            pPassName = mmString_CStr(&hArgs.passName);
            pPassInfo = mmVKPassLoader_GetFromName(p->pPassLoader, pPassName);
            err = mmVKLayoutLoader_GetLayoutsByStrings(p->pLayoutLoader, &hArgs.layouts, &dsli);
            if (mmVKPassInfo_Invalid(pPassInfo) || VK_SUCCESS != err)
            {
                if (mmVKPassInfo_Invalid(pPassInfo))
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pPassInfo is invalid. name: %s", __FUNCTION__, __LINE__, pPassName);
                }

                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " layouts is invalid.", __FUNCTION__, __LINE__);
                }

                // do not break here.
            }
            else
            {
                struct mmVectorValue psmi;
                struct mmVKPipelineCreateInfo hPipelineInfo;
                VkPipelineCache pipelineCache;
                VkSampleCountFlagBits samples;

                s = (struct mmVKPipelineInfo*)mmMalloc(sizeof(struct mmVKPipelineInfo));
                mmVKPipelineInfo_Init(s);
                it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
                mmString_MakeWeak(&s->name, &it->k);

                mmVKPipelineShaderStageCreateInfos_Init(&psmi);

                mmVKShaderLoaderMakeStageInfo(p->pShaderLoader, &hArgs.pssi, &psmi);

                pipelineCache = (NULL == p->pPipelineCache) ? VK_NULL_HANDLE : p->pPipelineCache->pipelineCache;
                samples = (0x00000000 == hArgs.samples) ? p->samples : (VkSampleCountFlagBits)hArgs.samples;

                hPipelineInfo.pvii = &hArgs.pvii;
                hPipelineInfo.dslp = &hArgs.dslp;
                hPipelineInfo.psmi = &psmi;
                hPipelineInfo.dsli = &dsli;
                hPipelineInfo.inputAssembly = &hArgs.inputAssembly;
                hPipelineInfo.tessellation = &hArgs.tessellation;
                hPipelineInfo.rasterization = &hArgs.rasterization;
                hPipelineInfo.colorBlend = &hArgs.colorBlend;
                hPipelineInfo.depthStencil = &hArgs.depthStencil;
                hPipelineInfo.multisample = &hArgs.multisample;
                hPipelineInfo.pPassInfo = pPassInfo;
                hPipelineInfo.pipelineCache = pipelineCache;
                hPipelineInfo.samples = samples;

                mmVKPipelineLoader_PreparePipelineInfo(p, &hPipelineInfo, s);

                mmVKPipelineShaderStageCreateInfos_Destroy(&psmi);
            }
        }

        mmVectorVpt_Destroy(&dsli);
        mmVKPipelineArgs_Destroy(&hArgs);

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);

    return s;
}

struct mmVKPipelineInfo*
mmVKPipelineLoader_GetFromName(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name)
{
    struct mmVKPipelineInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPipelineInfo*)(it->v);
    }
    return s;
}
