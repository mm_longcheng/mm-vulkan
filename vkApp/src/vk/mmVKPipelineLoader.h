/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPipelineLoader_h__
#define __mmVKPipelineLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKPipeline.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmVKDevice;
struct mmVKPipelineCache;
struct mmVKShaderLoader;
struct mmVKLayoutLoader;
struct mmVKPassLoader;

struct mmVKPipelineInfo
{
    // name for pipeline info, read only. 
    // string is weak reference value.
    struct mmString name;

    // pipeline archive.
    struct mmVKPipeline vPipeline;

    // reference count. 
    size_t reference;
};

void
mmVKPipelineInfo_Init(
    struct mmVKPipelineInfo*                       p);

void
mmVKPipelineInfo_Destroy(
    struct mmVKPipelineInfo*                       p);

int
mmVKPipelineInfo_Invalid(
    const struct mmVKPipelineInfo*                 p);

void
mmVKPipelineInfo_Increase(
    struct mmVKPipelineInfo*                       p);

void
mmVKPipelineInfo_Decrease(
    struct mmVKPipelineInfo*                       p);

VkResult
mmVKPipelineInfo_Prepare(
    struct mmVKPipelineInfo*                       p,
    struct mmVKDevice*                             pDevice,
    struct mmVKPipelineCreateInfo*                 pInfo);

void
mmVKPipelineInfo_Discard(
    struct mmVKPipelineInfo*                       p,
    struct mmVKDevice*                             pDevice);

struct mmVKPipelineLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKPipelineCache* pPipelineCache;
    struct mmVKShaderLoader* pShaderLoader;
    struct mmVKLayoutLoader* pLayoutLoader;
    struct mmVKPassLoader* pPassLoader;
    struct mmVKDevice* pDevice;
    struct mmRbtreeStringVpt sets;
    struct mmParseJSON parse;

    // pipeline sample count flag bits. 
    // if config is 0x00000000 will use this default value.
    VkSampleCountFlagBits                          samples;
};

void
mmVKPipelineLoader_Init(
    struct mmVKPipelineLoader*                     p);

void
mmVKPipelineLoader_Destroy(
    struct mmVKPipelineLoader*                     p);

void
mmVKPipelineLoader_SetFileContext(
    struct mmVKPipelineLoader*                     p,
    struct mmFileContext*                          pFileContext);

void
mmVKPipelineLoader_SetPipelineCache(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineCache*                      pPipelineCache);

void
mmVKPipelineLoader_SetShaderLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKShaderLoader*                       pShaderLoader);

void
mmVKPipelineLoader_SetLayoutLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKLayoutLoader*                       pLayoutLoader);

void
mmVKPipelineLoader_SetPassLoader(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPassLoader*                         pPassLoader);

void
mmVKPipelineLoader_SetDevice(
    struct mmVKPipelineLoader*                     p,
    struct mmVKDevice*                             pDevice);

void
mmVKPipelineLoader_SetSamples(
    struct mmVKPipelineLoader*                     p,
    VkSampleCountFlagBits                          samples);

VkResult
mmVKPipelineLoader_PreparePipelineInfo(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineCreateInfo*                 pInfo,
    struct mmVKPipelineInfo*                       pPipelineInfo);

void
mmVKPipelineLoader_DiscardPipelineInfo(
    struct mmVKPipelineLoader*                     p,
    struct mmVKPipelineInfo*                       pPipelineInfo);

void
mmVKPipelineLoader_UnloadByName(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name);

void
mmVKPipelineLoader_UnloadComplete(
    struct mmVKPipelineLoader*                     p);

struct mmVKPipelineInfo*
mmVKPipelineLoader_LoadFromInfo(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name,
    struct mmVKPipelineCreateInfo*                 pInfo);

struct mmVKPipelineInfo*
mmVKPipelineLoader_LoadFromFile(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name,
    const char*                                    path);

struct mmVKPipelineInfo*
mmVKPipelineLoader_GetFromName(
    struct mmVKPipelineLoader*                     p,
    const char*                                    name);

#include "core/mmSuffix.h"

#endif//__mmVKPipelineLoader_h__
