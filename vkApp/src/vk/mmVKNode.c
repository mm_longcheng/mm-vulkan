/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKNode.h"

#include "core/mmAlloc.h"

#include "math/mmMatrix4x4.h"
#include "math/mmQuaternion.h"
#include "math/mmVector3.h"

static void mmVKNode_MarkCacheInvalid(struct mmVKNode* p, size_t o, size_t l, void* func)
{
    typedef void(*ValidFuncType)(struct mmVKNode* p);
    ValidFuncType Invalid = (ValidFuncType)func;
    void* pointer = (void*)((char*)p + o);

    struct mmVKNode* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    mmMemset(pointer, 0, l);

    next = p->children.l.next;
    while (next != &p->children.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmVKNode*)it->v;

        (*(Invalid))(child);
    }
}

void mmVKNode_Init(struct mmVKNode* p)
{
    mmListVpt_Init(&p->children);

    p->parent = NULL;

    mmVec3Make(p->scale, 1.0f, 1.0f, 1.0f);
    mmQuatMakeIdentity(p->quaternion);
    mmVec3Make(p->position, 0.0f, 0.0f, 0.0f);

    mmMat4x4MakeIdentity(p->worldTransform);

    p->validWorldTransform = MM_FALSE;
}
void mmVKNode_Destroy(struct mmVKNode* p)
{
    p->validWorldTransform = MM_FALSE;
    mmMat4x4MakeIdentity(p->worldTransform);

    mmVec3Make(p->position, 0.0f, 0.0f, 0.0f);
    mmQuatMakeIdentity(p->quaternion);
    mmVec3Make(p->scale, 1.0f, 1.0f, 1.0f);

    p->parent = NULL;

    mmListVpt_Destroy(&p->children);
}

void mmVKNode_GetWorldTransform(struct mmVKNode* p, float worldTransform[4][4])
{
    if (p->validWorldTransform)
    {
        mmMat4x4Assign(worldTransform, p->worldTransform);
    }
    else
    {
        if (NULL != p->parent)
        {
            float hParentWorldTransform[4][4];
            float hLocalWorldTransform[4][4];
            mmVKNode_GetLocalTransform(p, hLocalWorldTransform);
            mmVKNode_GetWorldTransform(p->parent, hParentWorldTransform);
            mmMat4x4Mul(worldTransform, hParentWorldTransform, hLocalWorldTransform);
        }
        else
        {
            mmVKNode_GetLocalTransform(p, worldTransform);
        }

        mmMat4x4Assign(p->worldTransform, worldTransform);
        p->validWorldTransform = MM_TRUE;
    }
}

void mmVKNode_GetLocalTransform(struct mmVKNode* p, float localTransform[4][4])
{
    mmMat4x4MakeTransform(localTransform, p->position, p->quaternion, p->scale);
}

void mmVKNode_AddChild(struct mmVKNode* p, struct mmVKNode* child)
{
    child->parent = p;
    mmVKNode_MarkCacheInvalidWhole(child);
    mmListVpt_AddTail(&p->children, child);
}
void mmVKNode_RmvChild(struct mmVKNode* p, struct mmVKNode* child)
{
    child->parent = NULL;
    mmVKNode_MarkCacheInvalidWhole(child);
    mmListVpt_Remove(&p->children, child);
}

void mmVKNode_SetScale(struct mmVKNode* p, float scale[3])
{
    mmVec3Assign(p->scale, scale);
    mmVKNode_MarkCacheInvalidWorldTransform(p);
}
void mmVKNode_SetQuaternion(struct mmVKNode* p, float quaternion[4])
{
    mmQuatAssign(p->quaternion, quaternion);
    mmVKNode_MarkCacheInvalidWorldTransform(p);
}
void mmVKNode_SetPosition(struct mmVKNode* p, float position[3])
{
    mmVec3Assign(p->position, position);
    mmVKNode_MarkCacheInvalidWorldTransform(p);
}

void mmVKNode_MarkCacheInvalidWhole(struct mmVKNode* p)
{
    enum
    {
        offset = mmOffsetof(struct mmVKNode, validWorldTransform),
        length = sizeof(int) * 1,
    };
    mmVKNode_MarkCacheInvalid(p, offset, length, &mmVKNode_MarkCacheInvalidWhole);
}
void mmVKNode_MarkCacheInvalidWorldTransform(struct mmVKNode* p)
{
    enum
    {
        offset = mmOffsetof(struct mmVKNode, validWorldTransform),
        length = mmMemberSizeof(struct mmVKNode, validWorldTransform),
    };
    mmVKNode_MarkCacheInvalid(p, offset, length, &mmVKNode_MarkCacheInvalidWorldTransform);
}
