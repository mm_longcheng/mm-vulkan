/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKShadow.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKScene.h"
#include "vk/mmVKTFObject.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

void
mmVKShadowCascade_Init(
    struct mmVKShadowCascade*                      p)
{
    mmVectorValue_Init(&p->hLights);
    mmVKFrame_Init(&p->hShadowMap);
    p->hFrameNumber = 0;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->pPassInfo = NULL;
    p->pSamplerInfo = NULL;

    mmVectorValue_SetElement(&p->hLights, sizeof(struct mmVKShadowCascadeLight));
}

void
mmVKShadowCascade_Destroy(
    struct mmVKShadowCascade*                      p)
{
    p->pSamplerInfo = NULL;
    p->pPassInfo = NULL;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hFrameNumber = 0;
    mmVKFrame_Destroy(&p->hShadowMap);
    mmVectorValue_Destroy(&p->hLights);
}

VkResult
mmVKShadowCascade_Prepare(
    struct mmVKShadowCascade*                      p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkRenderPass renderPass = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKPassLoader* pPassLoader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;

        size_t i, j;
        struct mmVKLightInfo* pLightInfo;
        struct mmVKShadowCascadeLight* pCascadeLight;
        struct mmVKShadowCascadeFrustum* pFrustum;
        struct mmVKFrameCreateInfo hFrameInfo;
        VkImageViewCreateInfo hImageViewInfo;
        VkFramebufferCreateInfo hFramebufferInfo;
        VkImageSubresourceRange subresourceRange;
        VkImageUsageFlags usage;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pPassLoader = &pAssets->vPassLoader;
        pSamplerPool = &pAssets->vSamplerPool;

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pAllocator = pUploader->pAllocator;

        p->hDepthFormat = mmVKPickDepthFormat(pUploader->physicalDevice, hDepthFormat);

        mmVectorValue_AllocatorMemory(&p->hLights, pLights->size);

        p->hFrameNumber = 0;
        for (i = 0; i < pLights->size; i++)
        {
            pLightInfo = (struct mmVKLightInfo*)mmVectorVpt_At(pLights, i);
            pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hLights, i);

            pCascadeLight->hSplitLambda = pLightInfo->splitLambda;
            pCascadeLight->hIdxMap = p->hFrameNumber;
            pCascadeLight->hCount = mmVKLightInfo_GetFrameNumber(pLightInfo);

            p->hFrameNumber += pLightInfo->count;
        }

        usage = 0;
        usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
        
        if (0 != p->hFrameNumber)
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
            hFrameInfo.extent[0] = p->hShadowMapSize[0];
            hFrameInfo.extent[1] = p->hShadowMapSize[1];
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = p->hFrameNumber;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
            hFrameInfo.extent[0] = 1;
            hFrameInfo.extent[1] = 1;
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = 1;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        subresourceRange.aspectMask = hFrameInfo.aspectMask;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = hFrameInfo.mipLevels;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = hFrameInfo.arrayLayers;
        err = mmVKUploader_SetImageLayout(
            pUploader,
            p->hShadowMap.image,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL,
            &subresourceRange);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_SetImageLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->pPassInfo = mmVKPassLoader_GetFromName(pPassLoader, mmVKRenderPassDepthName);
        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPassInfo = NULL;
            break;
        }
        mmVKPassInfo_Increase(p->pPassInfo);
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        // Image view for this cascade's layer (inside the depth map)
        // This view is used to render to that specific depth image layer
        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = p->hShadowMap.image;
        hImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        hImageViewInfo.format = p->hDepthFormat;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = hFrameInfo.aspectMask;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = 1;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = 1;

        hFramebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        hFramebufferInfo.pNext = NULL;
        hFramebufferInfo.flags = 0;
        hFramebufferInfo.renderPass = renderPass;
        hFramebufferInfo.attachmentCount = 1;
        hFramebufferInfo.pAttachments = NULL;
        hFramebufferInfo.width  = p->hShadowMapSize[0];
        hFramebufferInfo.height = p->hShadowMapSize[1];
        hFramebufferInfo.layers = 1;

        for (j = 0; j < p->hLights.size; j++)
        {
            pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hLights, j);

            // One image and framebuffer per cascade
            for (i = 0; i < pCascadeLight->hCount; i++)
            {
                pFrustum = &pCascadeLight->hFrustum[i];

                hImageViewInfo.subresourceRange.baseArrayLayer = (uint32_t)(pCascadeLight->hIdxMap + i);
                err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &pFrustum->imageView);
                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " vkCreateImageView failure.", __FUNCTION__, __LINE__);
                    break;
                }

                hFramebufferInfo.pAttachments = &pFrustum->imageView;
                err = vkCreateFramebuffer(device, &hFramebufferInfo, pAllocator, &pFrustum->framebuffer);
                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " vkCreateFramebuffer failure.", __FUNCTION__, __LINE__);
                    break;
                }
            }
        }

        p->pSamplerInfo = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerLinearEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerInfo failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

    } while (0);

    return err;
}

void
mmVKShadowCascade_Discard(
    struct mmVKShadowCascade*                      p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;

        size_t i, j;
        struct mmVKShadowCascadeLight* pCascadeLight;
        struct mmVKShadowCascadeFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        mmVKSamplerInfo_Decrease(p->pSamplerInfo);
        p->pSamplerInfo = NULL;

        for (j = 0; j < p->hLights.size; j++)
        {
            pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hLights, j);

            for (i = 0; i < pCascadeLight->hCount; i++)
            {
                pFrustum = &pCascadeLight->hFrustum[i];
                if (VK_NULL_HANDLE != pFrustum->framebuffer)
                {
                    vkDestroyFramebuffer(device, pFrustum->framebuffer, pAllocator);
                    pFrustum->framebuffer = VK_NULL_HANDLE;
                }

                if (VK_NULL_HANDLE != pFrustum->imageView)
                {
                    vkDestroyImageView(device, pFrustum->imageView, pAllocator);
                    pFrustum->imageView = VK_NULL_HANDLE;
                }
            }
        }

        mmVKPassInfo_Decrease(p->pPassInfo);
        p->pPassInfo = NULL;

        mmVKFrame_Delete(&p->hShadowMap, pUploader);

        p->hFrameNumber = 0;

        mmVectorValue_Reset(&p->hLights);

        p->hDepthFormat = VK_FORMAT_D32_SFLOAT;

    } while (0);
}

VkResult
mmVKShadowCascade_OnRecordCommand(
    struct mmVKShadowCascade*                      p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkRenderPass renderPass = VK_NULL_HANDLE;

        VkClearValue hClearValues[1];
        VkRenderPassBeginInfo hPassBeginInfo;
        VkViewport viewport;
        VkRect2D scissor;

        int idx, i;
        size_t j;
        struct mmVKShadowCascadeLight* pCascadeLight;
        struct mmVKShadowCascadeFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " VK_NULL_HANDLE is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        mmMemset(hClearValues[0].color.float32, 0, sizeof(float) * 4);
        hClearValues[0].depthStencil.depth = 1.0f;
        hClearValues[0].depthStencil.stencil = 0;

        hPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        hPassBeginInfo.pNext = NULL;
        hPassBeginInfo.renderPass = renderPass;
        hPassBeginInfo.framebuffer = VK_NULL_HANDLE;
        hPassBeginInfo.renderArea.offset.x = 0;
        hPassBeginInfo.renderArea.offset.y = 0;
        hPassBeginInfo.renderArea.extent.width = p->hShadowMapSize[0];
        hPassBeginInfo.renderArea.extent.height = p->hShadowMapSize[1];
        hPassBeginInfo.clearValueCount = 1;
        hPassBeginInfo.pClearValues = hClearValues;

        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width  = (float)p->hShadowMapSize[0];
        viewport.height = (float)p->hShadowMapSize[1];
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        scissor.offset.x = 0;
        scissor.offset.y = 0;
        scissor.extent.width  = (uint32_t)p->hShadowMapSize[0];
        scissor.extent.height = (uint32_t)p->hShadowMapSize[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        // One pass per cascade
        // The layer that this pass renders to is defined by the cascade's
        // image view (selected via the cascade's descriptor set)
        for (j = 0; j < p->hLights.size; j++)
        {
            pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hLights, j);

            for (i = 0; i < pCascadeLight->hCount; i++)
            {
                pFrustum = &pCascadeLight->hFrustum[i];

                idx = pCascadeLight->hIdxMap + i;

                mmVKScene_SetPipelineIndex(pScene, mmVKPipelineDepth, pCascadeLight->hIdxMat, idx);

                hPassBeginInfo.framebuffer = pFrustum->framebuffer;

                vkCmdBeginRenderPass(cmdBuffer, &hPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

                mmVKTFObject_OnRecordCommand(cmdBuffer, pScene, pObject);

                vkCmdEndRenderPass(cmdBuffer);
            }
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

/*
    Calculate frustum split depths and matrices for the shadow map cascades
    Based on https://johanmedestrom.wordpress.com/2016/03/18/opengl-cascaded-shadow-maps/
*/
void
mmVKShadowCascade_UpdateTransforms(
    struct mmVKShadowCascade*                      p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip)
{
    struct mmVKShadowCascadeFrustum* pFrustum;
    struct mmVKShadowCascadeLight* pCascadeLight = mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    uint32_t i, n;
    uint32_t hCascadeCount = pCascadeLight->hCount;

    float hCascadeSplitLambda = pCascadeLight->hSplitLambda;

    float cascadeSplits[4];

    float clipRange = farClip - nearClip;

    float minZ = nearClip;
    float maxZ = nearClip + clipRange;

    float range = maxZ - minZ;
    float ratio = maxZ / minZ;

    float lastSplitDist = 0.0f;

    float lightDir[3];

    mmMat4x4ConstRef ProjectionView = hProjectionView;

    // Calculate split depths based on view camera frustum
    // Based on method presented in https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch10.html
    for (n = 0; n < hCascadeCount; ++n)
    {
        float p = (n + 1) / (float)(hCascadeCount);
        float log = minZ * powf(ratio, p);
        float uniform = minZ + range * p;
        float d = hCascadeSplitLambda * (log - uniform) + uniform;
        cascadeSplits[n] = (d - nearClip) / clipRange;
    }

    mmVec3Assign(lightDir, pUBOLight->direction);
    mmVec3Normalize(lightDir, lightDir);

    // Calculate orthographic projection matrix for each cascade
    for (n = 0; n < hCascadeCount; n++)
    {
        mmMat4x4Ref Projection;
        mmMat4x4Ref View;

        float invCam[4][4];

        float point[3];
        float eye[3];

        float maxExtents[3];
        float minExtents[3];

        float radius = 0.0f;

        float splitDist = cascadeSplits[n];

        float frustumCorners[8][3] =
        {
            { -1.0f, +1.0f, -1.0f },
            { +1.0f, +1.0f, -1.0f },
            { +1.0f, -1.0f, -1.0f },
            { -1.0f, -1.0f, -1.0f },
            { -1.0f, +1.0f, +1.0f },
            { +1.0f, +1.0f, +1.0f },
            { +1.0f, -1.0f, +1.0f },
            { -1.0f, -1.0f, +1.0f },
        };

        float frustumCenter[3] = { 0.0f, 0.0f, 0.0f };

        pFrustum = &pCascadeLight->hFrustum[n];

        Projection = pFrustum->Projection;
        View = pFrustum->View;

        // Project frustum corners into world space
        mmMat4x4Inverse(invCam, ProjectionView);
        for (i = 0; i < 8; i++)
        {
            float corner[4];
            float invCorner[4];
            mmVec3Ref frustumCorner = frustumCorners[i];
            mmVec4MakeFromVec3(corner, frustumCorner, 1.0f);
            mmMat4x4MulVec4(invCorner, invCam, corner);
            mmVec3DivK(frustumCorner, invCorner, invCorner[3]);
        }

        for (i = 0; i < 4; i++)
        {
            float hDistance[3];
            float hSplitDistance[3];
            float hSplitDistanceLast[3];
            // hDistance = frustumCorners[i + 4] - frustumCorners[i]
            mmVec3Sub(hDistance, frustumCorners[i + 4], frustumCorners[i]);

            // frustumCorners[i + 4] = frustumCorners[i] + (hDistance * splitDist)
            mmVec3MulK(hSplitDistance, hDistance, splitDist);
            mmVec3Add(frustumCorners[i + 4], frustumCorners[i], hSplitDistance);

            // frustumCorners[i] = frustumCorners[i] + (hDistance * lastSplitDist)
            mmVec3MulK(hSplitDistanceLast, hDistance, lastSplitDist);
            mmVec3Add(frustumCorners[i], frustumCorners[i], hSplitDistanceLast);
        }

        // Get frustum center
        for (i = 0; i < 8; i++)
        {
            // frustumCenter += frustumCorners[i];
            mmVec3Add(frustumCenter, frustumCenter, frustumCorners[i]);
        }
        // frustumCenter /= 8.0f;
        mmVec3DivK(frustumCenter, frustumCenter, 8.0f);

        for (i = 0; i < 8; i++)
        {
            float distance;
            float hDistance[3];
            mmVec3Sub(hDistance, frustumCorners[i], frustumCenter);

            distance = mmVec3Length(hDistance);
            radius = mmMathMax(radius, distance);
        }
        radius = ceilf(radius * 16.0f) / 16.0f;

        // maxExtents = vec3(radius)
        mmVec3AssignValue(maxExtents, radius);
        // minExtents = -maxExtents;
        mmVec3Negate(minExtents, maxExtents);

        mmVec3MulK(point, lightDir, -minExtents[2]);
        mmVec3Sub(eye, frustumCenter, point);
        // right-hand coordinate look at.
        mmMat4x4LookAtRH(View, eye, frustumCenter, mmVec3PositiveUnitY);
        // right-hand coordinate clip-space of [ 0, 1] +y up.
        mmMat4x4OrthogonalRHZOYU(
            Projection,
            minExtents[0], maxExtents[0],
            minExtents[1], maxExtents[1],
            0.0f, maxExtents[2] - minExtents[2]);

        // Store split distance and matrix in cascade
        pFrustum->SplitDepth = (nearClip + splitDist * clipRange) * (-1.0f);
        mmMat4x4Mul(pFrustum->ProjectionView, Projection, View);

        lastSplitDist = cascadeSplits[n];
    }
}

void
mmVKShadowCascade_UpdateUBODataLights(
    struct mmVKShadowCascade*                      p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews)
{
    int idx, i;
    struct mmVKShadowCascadeLight* pCascadeLight;
    mmMat4x4Ref ProjectionView;
    
    pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    pUBOLight->count = pCascadeLight->hCount;
    pUBOLight->idxMap = pCascadeLight->hIdxMap;
    pUBOLight->idxMat = idxMat;

    pCascadeLight->hIdxMat = idxMat;

    for (i = 0; i < pUBOLight->count; i++)
    {
        struct mmVKShadowCascadeFrustum* pFrustum;
        pFrustum = &pCascadeLight->hFrustum[i];

        idx = pUBOLight->idxMat + pUBOLight->idxMap + i;
        ProjectionView = (mmMat4x4Ref)mmVectorValue_At(uboProjectionViews, idx);
        mmMat4x4Assign(ProjectionView, pFrustum->ProjectionView);
    }
}

void
mmVKShadowOmni_Init(
    struct mmVKShadowOmni*                         p)
{
    mmVectorValue_Init(&p->hLights);
    mmVKFrame_Init(&p->hShadowMap);
    p->hFrameNumber = 0;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->pPassInfo = NULL;
    p->pSamplerInfo = NULL;

    mmVectorValue_SetElement(&p->hLights, sizeof(struct mmVKShadowOmniLight));
}

void
mmVKShadowOmni_Destroy(
    struct mmVKShadowOmni*                         p)
{
    p->pSamplerInfo = NULL;
    p->pPassInfo = NULL;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hFrameNumber = 0;
    mmVKFrame_Destroy(&p->hShadowMap);
    mmVectorValue_Destroy(&p->hLights);
}

VkResult
mmVKShadowOmni_Prepare(
    struct mmVKShadowOmni*                         p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkRenderPass renderPass = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKPassLoader* pPassLoader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;

        size_t i, j;
        struct mmVKLightInfo* pLightInfo;
        struct mmVKShadowOmniLight* pOmniLight;
        struct mmVKShadowOmniFrustum* pFrustum;
        struct mmVKFrameCreateInfo hFrameInfo;
        VkImageViewCreateInfo hImageViewInfo;
        VkFramebufferCreateInfo hFramebufferInfo;
        VkImageSubresourceRange subresourceRange;
        VkImageUsageFlags usage;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pPassLoader = &pAssets->vPassLoader;
        pSamplerPool = &pAssets->vSamplerPool;

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pAllocator = pUploader->pAllocator;

        p->hDepthFormat = mmVKPickDepthFormat(pUploader->physicalDevice, hDepthFormat);

        mmVectorValue_AllocatorMemory(&p->hLights, pLights->size);

        p->hFrameNumber = 0;
        for (i = 0; i < pLights->size; i++)
        {
            pLightInfo = (struct mmVKLightInfo*)mmVectorVpt_At(pLights, i);
            pOmniLight = (struct mmVKShadowOmniLight*)mmVectorValue_At(&p->hLights, i);

            pOmniLight->hIdxMap = p->hFrameNumber;

            p->hFrameNumber += mmVKLightInfo_GetFrameNumber(pLightInfo);
        }

        usage = 0;
        usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
        
        if (0 != p->hFrameNumber)
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
            hFrameInfo.extent[0] = p->hShadowMapSize[0];
            hFrameInfo.extent[1] = p->hShadowMapSize[1];
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = p->hFrameNumber;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
            hFrameInfo.extent[0] = 1;
            hFrameInfo.extent[1] = 1;
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = 6;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        subresourceRange.aspectMask = hFrameInfo.aspectMask;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = hFrameInfo.mipLevels;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = hFrameInfo.arrayLayers;
        err = mmVKUploader_SetImageLayout(
            pUploader,
            p->hShadowMap.image,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL,
            &subresourceRange);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_SetImageLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->pPassInfo = mmVKPassLoader_GetFromName(pPassLoader, mmVKRenderPassDepthName);
        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPassInfo = NULL;
            break;
        }
        mmVKPassInfo_Increase(p->pPassInfo);
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        // Image view for this omni's layer (inside the depth map)
        // This view is used to render to that specific depth image layer
        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = p->hShadowMap.image;
        hImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        hImageViewInfo.format = p->hDepthFormat;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = hFrameInfo.aspectMask;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = 1;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = 1;

        hFramebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        hFramebufferInfo.pNext = NULL;
        hFramebufferInfo.flags = 0;
        hFramebufferInfo.renderPass = renderPass;
        hFramebufferInfo.attachmentCount = 1;
        hFramebufferInfo.pAttachments = NULL;
        hFramebufferInfo.width = p->hShadowMapSize[0];
        hFramebufferInfo.height = p->hShadowMapSize[1];
        hFramebufferInfo.layers = 1;

        for (j = 0; j < p->hLights.size; j++)
        {
            pOmniLight = (struct mmVKShadowOmniLight*)mmVectorValue_At(&p->hLights, j);

            // One image and framebuffer per Omni
            for (i = 0; i < 6; i++)
            {
                pFrustum = &pOmniLight->hFrustum[i];

                hImageViewInfo.subresourceRange.baseArrayLayer = (uint32_t)(pOmniLight->hIdxMap + i);
                err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &pFrustum->imageView);
                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " vkCreateImageView failure.", __FUNCTION__, __LINE__);
                    break;
                }

                hFramebufferInfo.pAttachments = &pFrustum->imageView;
                err = vkCreateFramebuffer(device, &hFramebufferInfo, pAllocator, &pFrustum->framebuffer);
                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " vkCreateFramebuffer failure.", __FUNCTION__, __LINE__);
                    break;
                }
            }
        }

        p->pSamplerInfo = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerLinearEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerInfo failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

    } while (0);

    return err;
}

void
mmVKShadowOmni_Discard(
    struct mmVKShadowOmni*                         p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;

        size_t i, j;
        struct mmVKShadowOmniLight* pOmniLight;
        struct mmVKShadowOmniFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        mmVKSamplerInfo_Decrease(p->pSamplerInfo);
        p->pSamplerInfo = NULL;

        for (j = 0; j < p->hLights.size; j++)
        {
            pOmniLight = (struct mmVKShadowOmniLight*)mmVectorValue_At(&p->hLights, j);

            for (i = 0; i < 6; i++)
            {
                pFrustum = &pOmniLight->hFrustum[i];
                if (VK_NULL_HANDLE != pFrustum->framebuffer)
                {
                    vkDestroyFramebuffer(device, pFrustum->framebuffer, pAllocator);
                    pFrustum->framebuffer = VK_NULL_HANDLE;
                }

                if (VK_NULL_HANDLE != pFrustum->imageView)
                {
                    vkDestroyImageView(device, pFrustum->imageView, pAllocator);
                    pFrustum->imageView = VK_NULL_HANDLE;
                }
            }
        }

        mmVKPassInfo_Decrease(p->pPassInfo);
        p->pPassInfo = NULL;

        mmVKFrame_Delete(&p->hShadowMap, pUploader);
        
        p->hFrameNumber = 0;

        mmVectorValue_Reset(&p->hLights);

        p->hDepthFormat = VK_FORMAT_D32_SFLOAT;

    } while (0);
}

VkResult
mmVKShadowOmni_OnRecordCommand(
    struct mmVKShadowOmni*                         p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkRenderPass renderPass = VK_NULL_HANDLE;

        VkClearValue hClearValues[1];
        VkRenderPassBeginInfo hPassBeginInfo;
        VkViewport viewport;
        VkRect2D scissor;

        int idx, i;
        size_t j;
        struct mmVKShadowOmniLight* pOmniLight;
        struct mmVKShadowOmniFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " VK_NULL_HANDLE is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        mmMemset(hClearValues[0].color.float32, 0, sizeof(float) * 4);
        hClearValues[0].depthStencil.depth = 1.0f;
        hClearValues[0].depthStencil.stencil = 0;

        hPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        hPassBeginInfo.pNext = NULL;
        hPassBeginInfo.renderPass = renderPass;
        hPassBeginInfo.framebuffer = VK_NULL_HANDLE;
        hPassBeginInfo.renderArea.offset.x = 0;
        hPassBeginInfo.renderArea.offset.y = 0;
        hPassBeginInfo.renderArea.extent.width = p->hShadowMapSize[0];
        hPassBeginInfo.renderArea.extent.height = p->hShadowMapSize[1];
        hPassBeginInfo.clearValueCount = 1;
        hPassBeginInfo.pClearValues = hClearValues;

        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float)p->hShadowMapSize[0];
        viewport.height = (float)p->hShadowMapSize[1];
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        scissor.offset.x = 0;
        scissor.offset.y = 0;
        scissor.extent.width = (uint32_t)p->hShadowMapSize[0];
        scissor.extent.height = (uint32_t)p->hShadowMapSize[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        for (j = 0; j < p->hLights.size; j++)
        {
            pOmniLight = (struct mmVKShadowOmniLight*)mmVectorValue_At(&p->hLights, j);

            for (i = 0; i < 6; i++)
            {
                pFrustum = &pOmniLight->hFrustum[i];

                idx = pOmniLight->hIdxMap + i;

                mmVKScene_SetPipelineIndex(pScene, mmVKPipelineDepth, pOmniLight->hIdxMat, idx);

                hPassBeginInfo.framebuffer = pFrustum->framebuffer;

                vkCmdBeginRenderPass(cmdBuffer, &hPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

                mmVKTFObject_OnRecordCommand(cmdBuffer, pScene, pObject);

                vkCmdEndRenderPass(cmdBuffer);
            }
        }
 
        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKShadowOmni_UpdateTransforms(
    struct mmVKShadowOmni*                         p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip)
{
    struct mmVKShadowOmniFrustum* pFrustum;
    struct mmVKShadowOmniLight* pOmniLight = mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    float zNear = nearClip;
    float zFar = farClip;

    float eye[3];
    float center[3];
    float dir[3];
    float up[3];

    uint32_t n;

    mmVec3Assign(eye, pUBOLight->position);

    static const float gOmniDirection[6][2][3] =
    {
        { { +1.0f, 0.0f, 0.0f }, { 0.0f, -1.0f, 0.0f }, },
        { { -1.0f, 0.0f, 0.0f }, { 0.0f, -1.0f, 0.0f }, },
        { { 0.0f, +1.0f, 0.0f }, { 0.0f, 0.0f, +1.0f }, },
        { { 0.0f, -1.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, },
        { { 0.0f, 0.0f, +1.0f }, { 0.0f, -1.0f, 0.0f }, },
        { { 0.0f, 0.0f, -1.0f }, { 0.0f, -1.0f, 0.0f }, },
    };

    for (n = 0; n < 6; n++)
    {
        mmMat4x4Ref Projection;
        mmMat4x4Ref View;

        mmVec3Assign(dir, gOmniDirection[n][0]);
        mmVec3Assign(up, gOmniDirection[n][1]);

        mmVec3Add(center, eye, dir);

        pFrustum = &pOmniLight->hFrustum[n];

        Projection = pFrustum->Projection;
        View = pFrustum->View;

        // right-hand coordinate look at.
        mmMat4x4LookAtRH(View, eye, center, up);
        // right-hand coordinate clip-space of [ 0, 1] +y up.
        mmMat4x4PerspectiveRHZOYU(Projection, (float)(MM_PI / 2.0), 1.0f, zNear, zFar);

        mmMat4x4Mul(pFrustum->ProjectionView, Projection, View);
    }
}

void
mmVKShadowOmni_UpdateUBODataLights(
    struct mmVKShadowOmni*                         p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews)
{
    int idx, i;
    struct mmVKShadowOmniLight* pOmniLight;
    mmMat4x4Ref ProjectionView;
    
    pOmniLight = (struct mmVKShadowOmniLight*)mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    pUBOLight->count = 6;
    pUBOLight->idxMap = pOmniLight->hIdxMap;
    pUBOLight->idxMat = idxMat;

    pOmniLight->hIdxMat = idxMat;

    for (i = 0; i < pUBOLight->count; i++)
    {
        struct mmVKShadowOmniFrustum* pFrustum;
        pFrustum = &pOmniLight->hFrustum[i];

        idx = pUBOLight->idxMat + pUBOLight->idxMap + i;
        ProjectionView = (mmMat4x4Ref)mmVectorValue_At(uboProjectionViews, idx);
        mmMat4x4Assign(ProjectionView, pFrustum->ProjectionView);
    }
}

void
mmVKShadowSpot_Init(
    struct mmVKShadowSpot*                         p)
{
    mmVectorValue_Init(&p->hLights);
    mmVKFrame_Init(&p->hShadowMap);
    p->hFrameNumber = 0;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->pPassInfo = NULL;
    p->pSamplerInfo = NULL;

    mmVectorValue_SetElement(&p->hLights, sizeof(struct mmVKShadowSpotLight));
}

void
mmVKShadowSpot_Destroy(
    struct mmVKShadowSpot*                         p)
{
    p->pSamplerInfo = NULL;
    p->pPassInfo = NULL;
    p->hDepthFormat = VK_FORMAT_D32_SFLOAT;
    p->hShadowMapSize[0] = 2048;
    p->hShadowMapSize[1] = 2048;
    p->hFrameNumber = 0;
    mmVKFrame_Destroy(&p->hShadowMap);
    mmVectorValue_Destroy(&p->hLights);
}

VkResult
mmVKShadowSpot_Prepare(
    struct mmVKShadowSpot*                         p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkRenderPass renderPass = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKPassLoader* pPassLoader = NULL;
        struct mmVKSamplerPool* pSamplerPool = NULL;

        size_t i, j;
        struct mmVKLightInfo* pLightInfo;
        struct mmVKShadowSpotLight* pSpotLight;
        struct mmVKShadowSpotFrustum* pFrustum;
        struct mmVKFrameCreateInfo hFrameInfo;
        VkImageViewCreateInfo hImageViewInfo;
        VkFramebufferCreateInfo hFramebufferInfo;
        VkImageSubresourceRange subresourceRange;
        VkImageUsageFlags usage;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pPassLoader = &pAssets->vPassLoader;
        pSamplerPool = &pAssets->vSamplerPool;

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pAllocator = pUploader->pAllocator;

        p->hDepthFormat = mmVKPickDepthFormat(pUploader->physicalDevice, hDepthFormat);

        mmVectorValue_AllocatorMemory(&p->hLights, pLights->size);

        p->hFrameNumber = 0;
        for (i = 0; i < pLights->size; i++)
        {
            pLightInfo = (struct mmVKLightInfo*)mmVectorVpt_At(pLights, i);
            pSpotLight = (struct mmVKShadowSpotLight*)mmVectorValue_At(&p->hLights, i);

            pSpotLight->hIdxMap = p->hFrameNumber;

            p->hFrameNumber += mmVKLightInfo_GetFrameNumber(pLightInfo);
        }

        usage = 0;
        usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
        
        if (0 != p->hFrameNumber)
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
            hFrameInfo.extent[0] = p->hShadowMapSize[0];
            hFrameInfo.extent[1] = p->hShadowMapSize[1];
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = p->hFrameNumber;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            hFrameInfo.format = p->hDepthFormat;
            hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
            hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
            hFrameInfo.extent[0] = 1;
            hFrameInfo.extent[1] = 1;
            hFrameInfo.extent[2] = 1;
            hFrameInfo.mipLevels = 1;
            hFrameInfo.arrayLayers = 1;
            hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            hFrameInfo.usage = usage;
            hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            hFrameInfo.vmaFlags = 0;
            hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

            err = mmVKFrame_Create(&p->hShadowMap, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKFrame_Create failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        subresourceRange.aspectMask = hFrameInfo.aspectMask;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = hFrameInfo.mipLevels;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = hFrameInfo.arrayLayers;
        err = mmVKUploader_SetImageLayout(
            pUploader,
            p->hShadowMap.image,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL,
            &subresourceRange);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_SetImageLayout failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->pPassInfo = mmVKPassLoader_GetFromName(pPassLoader, mmVKRenderPassDepthName);
        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPassInfo = NULL;
            break;
        }
        mmVKPassInfo_Increase(p->pPassInfo);
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        // Image view for this omni's layer (inside the depth map)
        // This view is used to render to that specific depth image layer
        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = p->hShadowMap.image;
        hImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        hImageViewInfo.format = p->hDepthFormat;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = hFrameInfo.aspectMask;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = 1;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = 1;

        hFramebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        hFramebufferInfo.pNext = NULL;
        hFramebufferInfo.flags = 0;
        hFramebufferInfo.renderPass = renderPass;
        hFramebufferInfo.attachmentCount = 1;
        hFramebufferInfo.pAttachments = NULL;
        hFramebufferInfo.width = p->hShadowMapSize[0];
        hFramebufferInfo.height = p->hShadowMapSize[1];
        hFramebufferInfo.layers = 1;

        for (j = 0; j < p->hLights.size; j++)
        {
            pSpotLight = (struct mmVKShadowSpotLight*)mmVectorValue_At(&p->hLights, j);

            pFrustum = &pSpotLight->hFrustum;

            hImageViewInfo.subresourceRange.baseArrayLayer = (uint32_t)(pSpotLight->hIdxMap);
            err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &pFrustum->imageView);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateImageView failure.", __FUNCTION__, __LINE__);
                break;
            }

            hFramebufferInfo.pAttachments = &pFrustum->imageView;
            err = vkCreateFramebuffer(device, &hFramebufferInfo, pAllocator, &pFrustum->framebuffer);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateFramebuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        p->pSamplerInfo = mmVKSamplerPool_Add(
            pSamplerPool,
            &mmVKSamplerLinearEdge);
        if (mmVKSamplerInfo_Invalid(p->pSamplerInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_Add pSamplerInfo failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pSamplerInfo = NULL;
            break;
        }
        mmVKSamplerInfo_Increase(p->pSamplerInfo);

    } while (0);

    return err;
}

void
mmVKShadowSpot_Discard(
    struct mmVKShadowSpot*                         p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmVKUploader* pUploader = NULL;

        size_t j;
        struct mmVKShadowSpotLight* pSpotLight;
        struct mmVKShadowSpotFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        mmVKSamplerInfo_Decrease(p->pSamplerInfo);
        p->pSamplerInfo = NULL;

        for (j = 0; j < p->hLights.size; j++)
        {
            pSpotLight = (struct mmVKShadowSpotLight*)mmVectorValue_At(&p->hLights, j);

            pFrustum = &pSpotLight->hFrustum;
            if (VK_NULL_HANDLE != pFrustum->framebuffer)
            {
                vkDestroyFramebuffer(device, pFrustum->framebuffer, pAllocator);
                pFrustum->framebuffer = VK_NULL_HANDLE;
            }

            if (VK_NULL_HANDLE != pFrustum->imageView)
            {
                vkDestroyImageView(device, pFrustum->imageView, pAllocator);
                pFrustum->imageView = VK_NULL_HANDLE;
            }
        }

        mmVKPassInfo_Decrease(p->pPassInfo);
        p->pPassInfo = NULL;

        mmVKFrame_Delete(&p->hShadowMap, pUploader);
        
        p->hFrameNumber = 0;

        mmVectorValue_Reset(&p->hLights);

        p->hDepthFormat = VK_FORMAT_D32_SFLOAT;

    } while (0);
}

VkResult
mmVKShadowSpot_OnRecordCommand(
    struct mmVKShadowSpot*                         p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkRenderPass renderPass = VK_NULL_HANDLE;

        VkClearValue hClearValues[1];
        VkRenderPassBeginInfo hPassBeginInfo;
        VkViewport viewport;
        VkRect2D scissor;

        int idx;
        size_t j;
        struct mmVKShadowSpotLight* pSpotLight;
        struct mmVKShadowSpotFrustum* pFrustum;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " VK_NULL_HANDLE is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        renderPass = p->pPassInfo->vRenderPass.renderPass;

        mmMemset(hClearValues[0].color.float32, 0, sizeof(float) * 4);
        hClearValues[0].depthStencil.depth = 1.0f;
        hClearValues[0].depthStencil.stencil = 0;

        hPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        hPassBeginInfo.pNext = NULL;
        hPassBeginInfo.renderPass = renderPass;
        hPassBeginInfo.framebuffer = VK_NULL_HANDLE;
        hPassBeginInfo.renderArea.offset.x = 0;
        hPassBeginInfo.renderArea.offset.y = 0;
        hPassBeginInfo.renderArea.extent.width = p->hShadowMapSize[0];
        hPassBeginInfo.renderArea.extent.height = p->hShadowMapSize[1];
        hPassBeginInfo.clearValueCount = 1;
        hPassBeginInfo.pClearValues = hClearValues;

        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float)p->hShadowMapSize[0];
        viewport.height = (float)p->hShadowMapSize[1];
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        scissor.offset.x = 0;
        scissor.offset.y = 0;
        scissor.extent.width = (uint32_t)p->hShadowMapSize[0];
        scissor.extent.height = (uint32_t)p->hShadowMapSize[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        for (j = 0; j < p->hLights.size; j++)
        {
            pSpotLight = (struct mmVKShadowSpotLight*)mmVectorValue_At(&p->hLights, j);

            pFrustum = &pSpotLight->hFrustum;

            idx = pSpotLight->hIdxMap;

            mmVKScene_SetPipelineIndex(pScene, mmVKPipelineDepth, pSpotLight->hIdxMat, idx);

            hPassBeginInfo.framebuffer = pFrustum->framebuffer;

            vkCmdBeginRenderPass(cmdBuffer, &hPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            mmVKTFObject_OnRecordCommand(cmdBuffer, pScene, pObject);

            vkCmdEndRenderPass(cmdBuffer);
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKShadowSpot_UpdateTransforms(
    struct mmVKShadowSpot*                         p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip)
{
    struct mmVKShadowSpotFrustum* pFrustum;
    struct mmVKShadowSpotLight* pSpotLight = mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    float zNear = nearClip;
    float zFar = farClip;

    float eye[3];
    float center[3];

    mmMat4x4Ref Projection;
    mmMat4x4Ref View;

    mmVec3Assign(eye, pUBOLight->position);

    mmVec3Add(center, eye, pUBOLight->direction);

    pFrustum = &pSpotLight->hFrustum;

    Projection = pFrustum->Projection;
    View = pFrustum->View;

    float fovy = mmMathClamp(pUBOLight->outerConeCos * 1.2f, 0.0f, (float)MM_PI_DIV_2);
    fovy = (float)MM_PI_DIV_2;

    // right-hand coordinate look at.
    mmMat4x4LookAtRH(View, eye, center, mmVec3PositiveUnitY);
    // right-hand coordinate clip-space of [ 0, 1] +y up.
    mmMat4x4PerspectiveRHZOYU(Projection, fovy, 1.0f, zNear, zFar);

    mmMat4x4Mul(pFrustum->ProjectionView, Projection, View);
}

void
mmVKShadowSpot_UpdateUBODataLights(
    struct mmVKShadowSpot*                         p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews)
{
    int idx;
    struct mmVKShadowSpotLight* pSpotLight;
    mmMat4x4Ref ProjectionView;
    
    pSpotLight = (struct mmVKShadowSpotLight*)mmVectorValue_At(&p->hLights, pUBOLight->shadow);

    pUBOLight->count = 1;
    pUBOLight->idxMap = pSpotLight->hIdxMap;
    pUBOLight->idxMat = idxMat;

    pSpotLight->hIdxMat = idxMat;

    struct mmVKShadowSpotFrustum* pFrustum;
    pFrustum = &pSpotLight->hFrustum;

    idx = pUBOLight->idxMat + pUBOLight->idxMap;
    ProjectionView = (mmMat4x4Ref)mmVectorValue_At(uboProjectionViews, idx);
    mmMat4x4Assign(ProjectionView, pFrustum->ProjectionView);
}

void
mmVKShadow_Init(
    struct mmVKShadow*                             p)
{
    mmVKShadowCascade_Init(&p->hCascade);
    mmVKShadowOmni_Init(&p->hOmni);
    mmVKShadowSpot_Init(&p->hSpot);

    mmVectorValue_Init(&p->uboSplits);
    mmVectorValue_Init(&p->uboProjectionViews);
    mmVKBuffer_Init(&p->bufferUBOSplits);
    mmVKBuffer_Init(&p->bufferUBOProjectionViews);
    p->hFrameNumber = 0;

    mmVectorValue_SetElement(&p->uboSplits, sizeof(float));
    mmVectorValue_SetElement(&p->uboProjectionViews, sizeof(mmMat4x4));
}

void
mmVKShadow_Destroy(
    struct mmVKShadow*                             p)
{
    p->hFrameNumber = 0;
    mmVKBuffer_Destroy(&p->bufferUBOProjectionViews);
    mmVKBuffer_Destroy(&p->bufferUBOSplits);
    mmVectorValue_Destroy(&p->uboProjectionViews);
    mmVectorValue_Destroy(&p->uboSplits);

    mmVKShadowSpot_Destroy(&p->hSpot);
    mmVKShadowOmni_Destroy(&p->hOmni);
    mmVKShadowCascade_Destroy(&p->hCascade);
}

VkResult
mmVKShadow_Prepare(
    struct mmVKShadow*                             p,
    struct mmVKAssets*                             pAssets,
    const struct mmVectorVpt                       lights[3],
    VkFormat                                       hDepthFormat)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t hCount;
        VkBufferCreateInfo hBufferInfo;

        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadowCascade_Prepare(&p->hCascade, pAssets, hDepthFormat, &lights[0]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowCascade_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadowOmni_Prepare(&p->hOmni, pAssets, hDepthFormat, &lights[1]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowOmni_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadowSpot_Prepare(&p->hSpot, pAssets, hDepthFormat, &lights[2]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowSpot_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        hCount = p->hCascade.hFrameNumber;
        hCount = (0 != hCount) ? hCount : 1;
        mmVectorValue_AllocatorMemory(&p->uboSplits, hCount);
        
        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = hCount * p->uboSplits.element;
        hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;
        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferUBOSplits);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        p->hFrameNumber = 0;
        p->hFrameNumber += p->hCascade.hFrameNumber;
        p->hFrameNumber += p->hOmni.hFrameNumber;
        p->hFrameNumber += p->hSpot.hFrameNumber;
        
        hCount = p->hFrameNumber;
        hCount = (0 != hCount) ? hCount : 1;
        mmVectorValue_AllocatorMemory(&p->uboProjectionViews, hCount);

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = hCount * p->uboProjectionViews.element;
        hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;
        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferUBOProjectionViews);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKShadow_Discard(
    struct mmVKShadow*                             p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKUploader_DeleteBuffer(pUploader, &p->bufferUBOProjectionViews);
        mmVKUploader_DeleteBuffer(pUploader, &p->bufferUBOSplits);

        mmVKShadowSpot_Discard(&p->hSpot, pAssets);
        mmVKShadowOmni_Discard(&p->hOmni, pAssets);
        mmVKShadowCascade_Discard(&p->hCascade, pAssets);
    } while (0);
}

void
mmVKShadow_UpdateUBODataTransforms(
    struct mmVKShadow*                             p)
{
    size_t i, j, idx;
    struct mmVKShadowCascadeLight* pCascadeLight;
    struct mmVKShadowCascadeFrustum* pFrustum;

    for (j = 0; j < p->hCascade.hLights.size; j++)
    {
        pCascadeLight = (struct mmVKShadowCascadeLight*)mmVectorValue_At(&p->hCascade.hLights, j);

        for (i = 0; i < pCascadeLight->hCount; i++)
        {
            pFrustum = &pCascadeLight->hFrustum[i];

            idx = pCascadeLight->hIdxMap + i;

            mmVectorValue_SetIndex(&p->uboSplits, idx, &pFrustum->SplitDepth);
        }
    }
}

VkResult
mmVKShadow_UploadUBOBuffer(
    struct mmVKShadow*                             p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (0 != p->uboSplits.size)
        {
            err = mmVKUploader_UpdateCPU2GPUBuffer(
                pUploader,
                (uint8_t*)p->uboSplits.arrays,
                (size_t)0,
                (size_t)(p->uboSplits.size * p->uboSplits.element),
                &p->bufferUBOSplits,
                (size_t)0);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }
        
        if (0 != p->uboProjectionViews.size)
        {
            err = mmVKUploader_UpdateCPU2GPUBuffer(
                pUploader,
                (uint8_t*)p->uboProjectionViews.arrays,
                (size_t)0,
                (size_t)(p->uboProjectionViews.size * p->uboProjectionViews.element),
                &p->bufferUBOProjectionViews,
                (size_t)0);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }

    } while (0);

    return err;
}

VkResult
mmVKShadow_OnRecordCommand(
    struct mmVKShadow*                             p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmVKShadowCascade_OnRecordCommand(&p->hCascade, cmdBuffer, pObject, pScene);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowCascade_OnRecordCommand failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadowOmni_OnRecordCommand(&p->hOmni, cmdBuffer, pObject, pScene);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowOmni_OnRecordCommand failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadowSpot_OnRecordCommand(&p->hSpot, cmdBuffer, pObject, pScene);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadowSpot_OnRecordCommand failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKShadow_UpdateTransforms(
    struct mmVKShadow*                             p,
    struct mmVectorValue*                          uboSceneLights,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip)
{
    size_t i;
    struct mmVKUBOLight* pUBOLight;
    
    for (i = 0; i < uboSceneLights->size; i++)
    {
        pUBOLight = (struct mmVKUBOLight*)mmVectorValue_At(uboSceneLights, i);

        if (-1 == pUBOLight->shadow)
        {
            continue;
        }

        switch (pUBOLight->type)
        {
        case mmGLTFLightTypeDirectional:
            mmVKShadowCascade_UpdateTransforms(
                &p->hCascade,
                pUBOLight,
                hProjectionView,
                nearClip,
                farClip);
            break;

        case mmGLTFLightTypePoint:
            mmVKShadowOmni_UpdateTransforms(
                &p->hOmni,
                pUBOLight,
                hProjectionView,
                nearClip,
                farClip);
            break;

        case mmGLTFLightTypeSpot:
            mmVKShadowSpot_UpdateTransforms(
                &p->hSpot,
                pUBOLight,
                hProjectionView,
                nearClip,
                farClip);
            break;

        default:
            break;
        }
    }
}

void
mmVKShadow_UpdateUBODataLights(
    struct mmVKShadow*                             p,
    struct mmVectorValue*                          uboSceneLights,
    struct mmVectorValue*                          uboProjectionViews)
{
    int idxMat = 0;
    size_t i;
    struct mmVKUBOLight* pUBOLight;

    for (i = 0; i < uboSceneLights->size; i++)
    {
        pUBOLight = (struct mmVKUBOLight*)mmVectorValue_At(uboSceneLights, i);

        if (-1 == pUBOLight->shadow)
        {
            continue;
        }

        switch (pUBOLight->type)
        {
        case mmGLTFLightTypeDirectional:
            mmVKShadowCascade_UpdateUBODataLights(
                &p->hCascade,
                pUBOLight,
                idxMat,
                uboProjectionViews);
            break;

        case mmGLTFLightTypePoint:
            mmVKShadowOmni_UpdateUBODataLights(
                &p->hOmni,
                pUBOLight,
                idxMat,
                uboProjectionViews);
            break;

        case mmGLTFLightTypeSpot:
            mmVKShadowSpot_UpdateUBODataLights(
                &p->hSpot,
                pUBOLight,
                idxMat,
                uboProjectionViews);
            break;

        default:
            break;
        }
        
        idxMat += pUBOLight->count;
    }
}
