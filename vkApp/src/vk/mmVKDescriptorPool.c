/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDescriptorPool.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKLayoutLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "container/mmVectorVpt.h"
#include "container/mmRbtreeU32.h"

void
mmVKDescriptorChunk_Init(
    struct mmVKDescriptorChunk*                    p)
{
    mmRbtsetU64_Init(&p->idle);
    p->descriptorPool = VK_NULL_HANDLE;
    p->number = 0;
    p->hIndexProduce = 0;
    p->hIndexRecycle = 0;
    p->pool = NULL;
}

void
mmVKDescriptorChunk_Destroy(
    struct mmVKDescriptorChunk*                    p)
{
    p->pool = NULL;
    p->hIndexRecycle = 0;
    p->hIndexProduce = 0;
    p->number = 0;
    p->descriptorPool = VK_NULL_HANDLE;
    mmRbtsetU64_Destroy(&p->idle);
}

void
mmVKDescriptorChunk_SetPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorPool*                     pool)
{
    p->pool = pool;
}

void
mmVKDescriptorChunk_ResetIndex(
    struct mmVKDescriptorChunk*                    p)
{
    p->hIndexProduce = 0;
    p->hIndexRecycle = 0;
}

int
mmVKDescriptorChunk_ProduceComplete(
    const struct mmVKDescriptorChunk*              p)
{
    assert(NULL != p->pool && "pool is invalid.");
    return p->hIndexProduce == p->pool->maxSets;
}

int
mmVKDescriptorChunk_RecycleComplete(
    const struct mmVKDescriptorChunk*              p)
{
    assert(NULL != p->pool && "pool is invalid.");
    return p->hIndexRecycle == p->pool->maxSets;
}

VkResult
mmVKDescriptorChunk_PrepareDescriptorPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDevice*                             pDevice,
    struct mmVectorValue*                          pSizes,
    uint32_t                                       hMaxSets)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != p->pool && "pool is invalid.");

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkDescriptorPoolCreateInfo hPoolInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(NULL != pSizes && "pSizes is invalid.");

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKLayoutInfo_Invalid(p->pool->pLayoutInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        assert(0 == p->idle.size && "assets is not destroy complete.");

        p->number = 0;
        mmRbtsetU64_Clear(&p->idle);

        hPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        hPoolInfo.pNext = NULL;
        hPoolInfo.flags = 0;
        hPoolInfo.maxSets = hMaxSets;
        hPoolInfo.poolSizeCount = (uint32_t)pSizes->size;
        hPoolInfo.pPoolSizes = (const VkDescriptorPoolSize*)pSizes->arrays;

        err = vkCreateDescriptorPool(device, &hPoolInfo, pAllocator, &p->descriptorPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateDescriptorPool failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKDescriptorChunk_DiscardDescriptorPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        if (VK_NULL_HANDLE == p->descriptorPool)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        vkDestroyDescriptorPool(device, p->descriptorPool, pAllocator);
    } while (0);
}

VkResult
mmVKDescriptorChunk_DescriptorSetProduce(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorSet*                      pSet)
{
    VkResult err = VK_ERROR_UNKNOWN;

    assert(NULL != pSet && "pSet is a invalid.");
    assert(NULL != p->pool && "pool is invalid.");

    pSet->descriptorSet = VK_NULL_HANDLE;
    pSet->chunk = 0;

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;

        VkDescriptorSetAllocateInfo hDescriptorSetAllocateInfo;

        struct mmVKDevice*     pDevice = p->pool->pDevice;
        struct mmVKLayoutInfo* pLayoutInfo = p->pool->pLayoutInfo;
        uint32_t maxSets = p->pool->maxSets;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == p->descriptorPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " descriptorPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKLayoutInfo_Invalid(pLayoutInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 < p->idle.size)
        {
            struct mmRbNode* n = NULL;
            struct mmRbtsetU64Iterator* it = NULL;
            // min or max.
            n = mmRb_First(&p->idle.rbt);
            assert(NULL != n);
            it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
            pSet->descriptorSet = (VkDescriptorSet)it->k;
            pSet->chunk = (mmUInt64_t)(mmUIntptr_t)(p);
            p->hIndexProduce++;
            mmRbtsetU64_Erase(&p->idle, it);

            err = VK_SUCCESS;
            break;
        }

        if (maxSets <= p->number)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " descriptorPool remaining is empty.", __FUNCTION__, __LINE__);
            err = VK_ERROR_TOO_MANY_OBJECTS;
            break;
        }

        descriptorSetLayout = pLayoutInfo->descriptorSetLayout;

        hDescriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        hDescriptorSetAllocateInfo.pNext = NULL;
        hDescriptorSetAllocateInfo.descriptorPool = p->descriptorPool;
        hDescriptorSetAllocateInfo.descriptorSetCount = 1;
        hDescriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

        err = vkAllocateDescriptorSets(device, &hDescriptorSetAllocateInfo, &pSet->descriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkAllocateDescriptorSets failure.", __FUNCTION__, __LINE__);
            break;
        }
        pSet->chunk = (mmUInt64_t)(mmUIntptr_t)(p);
        p->hIndexProduce++;

        // record Allocate number.
        p->number += 1;

    } while (0);

    return err;
}

void
mmVKDescriptorChunk_DescriptorSetRecycle(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorSet*                      pSet)
{
    assert(NULL != pSet && "pSet is invalid.");
    assert(0 != pSet->chunk && "pSet->chunk is invalid.");
    assert((mmUInt64_t)(mmUIntptr_t)(p) == pSet->chunk && "pSet chunk not same.");
    mmRbtsetU64_Add(&p->idle, (uint64_t)pSet->descriptorSet);
    p->hIndexRecycle++;
}

void
mmVKDescriptorPool_Init(
    struct mmVKDescriptorPool*                     p)
{
    mmVKDescriptorPoolSizes_Init(&p->sizes);

    mmRbtreeU64Vpt_Init(&p->rbtree);
    mmRbtsetVpt_Init(&p->used);
    mmRbtsetVpt_Init(&p->idle);
    p->current = NULL;
    p->hRateTrimBorder = 0.75f;
    p->hRateTrimNumber = 0.25f;

    p->pLayoutInfo = NULL;
    p->maxSets = 0;
    p->pDevice = NULL;
}

void
mmVKDescriptorPool_Destroy(
    struct mmVKDescriptorPool*                     p)
{
    assert(0 == p->rbtree.size && "assets is not destroy complete.");

    p->pDevice = NULL;
    p->maxSets = 0;
    p->pLayoutInfo = NULL;

    p->hRateTrimNumber = 0.25f;
    p->hRateTrimBorder = 0.75f;
    p->current = NULL;
    mmRbtsetVpt_Destroy(&p->idle);
    mmRbtsetVpt_Destroy(&p->used);
    mmRbtreeU64Vpt_Destroy(&p->rbtree);

    mmVKDescriptorPoolSizes_Destroy(&p->sizes);
}

void
mmVKDescriptorPool_UpdatePoolSize(
    struct mmVKDescriptorPool*                     p,
    const struct mmVectorValue*                    dslb,
    uint32_t                                       dslc)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;

    struct mmRbtreeU32U32 types;
    VkDescriptorType type;
    uint32_t count;

    const VkDescriptorSetLayoutBinding* binding = NULL;
    VkDescriptorPoolSize* poolSize = NULL;

    size_t i = 0;

    p->maxSets = dslc;

    mmRbtreeU32U32_Init(&types);

    mmVectorValue_Reset(&p->sizes);

    for (i = 0; i < dslb->size; i++)
    {
        binding = (const VkDescriptorSetLayoutBinding*)mmVectorValue_At(dslb, i);

        type = binding->descriptorType;
        count = binding->descriptorCount * p->maxSets;

        it = mmRbtreeU32U32_Add(&types, type);
        mmRbtreeU32U32_Set(&types, type, it->v + count);
    }

    i = 0;
    mmVectorValue_AllocatorMemory(&p->sizes, types.size);

    n = mmRb_First(&types.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
        n = mmRb_Next(n);

        poolSize = (VkDescriptorPoolSize*)mmVectorValue_At(&p->sizes, i++);

        poolSize->type = it->k;
        poolSize->descriptorCount = it->v;
    }

    mmRbtreeU32U32_Destroy(&types);
}

VkResult
mmVKDescriptorPool_Create(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDevice*                             pDevice,
    struct mmVKLayoutInfo*                         info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        const struct mmVectorValue* dslb = NULL;
        uint32_t                    dslc = info->dslc;

        struct mmLogger* gLogger = mmLogger_Instance();

        p->pDevice = pDevice;

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 == dslc)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " dslc is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pLayoutInfo = info;
        if (mmVKLayoutInfo_Invalid(p->pLayoutInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutInfo is invalid.", __FUNCTION__, __LINE__);
            p->pLayoutInfo = NULL;
            break;
        }
        mmVKLayoutInfo_Increase(p->pLayoutInfo);

        dslb = &p->pLayoutInfo->dslb;
        mmVKDescriptorPool_UpdatePoolSize(p, dslb, dslc);
        if (0 == p->maxSets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_UpdatePoolSize failure.", __FUNCTION__, __LINE__);

            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        err = VK_SUCCESS;

    } while (0);

    return err;
}

void
mmVKDescriptorPool_Delete(
    struct mmVKDescriptorPool*                     p)
{
    do
    {
        struct mmVKDevice* pDevice = p->pDevice;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKDescriptorPool_DestroyChunk(p);

        mmVKLayoutInfo_Decrease(p->pLayoutInfo);
        p->pLayoutInfo = NULL;

    } while (0);
}

size_t
mmVKDescriptorPool_Capacity(
    const struct mmVKDescriptorPool*               p)
{
    return p->maxSets * p->rbtree.size;
}

struct mmVKDescriptorChunk*
mmVKDescriptorPool_AddChunk(
    struct mmVKDescriptorPool*                     p)
{
    struct mmVKDescriptorChunk* s = NULL;
    s = (struct mmVKDescriptorChunk*)mmMalloc(sizeof(struct mmVKDescriptorChunk));
    mmVKDescriptorChunk_Init(s);
    mmVKDescriptorChunk_SetPool(s, p);
    mmRbtreeU64Vpt_Set(&p->rbtree, (mmUInt64_t)(mmUIntptr_t)(s), (void*)s);
    mmVKDescriptorChunk_PrepareDescriptorPool(s, p->pDevice, &p->sizes, p->maxSets);
    return s;
}

struct mmVKDescriptorChunk*
mmVKDescriptorPool_GetChunk(
    const struct mmVKDescriptorPool*               p,
    mmUInt64_t                                     page)
{
    struct mmVKDescriptorChunk* s = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    it = mmRbtreeU64Vpt_GetIterator(&p->rbtree, page);
    if (NULL != it)
    {
        s = (struct mmVKDescriptorChunk*)it->v;
    }
    return s;
}

void
mmVKDescriptorPool_RmvChunk(
    struct mmVKDescriptorPool*                     p,
    mmUInt64_t                                     page)
{
    struct mmVKDescriptorChunk* s = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    it = mmRbtreeU64Vpt_GetIterator(&p->rbtree, page);
    if (NULL != it)
    {
        s = (struct mmVKDescriptorChunk*)it->v;
        mmVKDescriptorChunk_DiscardDescriptorPool(s, p->pDevice);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmVKDescriptorChunk_Destroy(s);
        mmFree(s);
    }
}

void
mmVKDescriptorPool_DestroyChunk(
    struct mmVKDescriptorPool*                     p)
{
    struct mmVKDescriptorChunk* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;

    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKDescriptorChunk*)(it->v);

        assert(s->hIndexProduce == s->hIndexRecycle && "assets is not destroy complete.");

        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, s);

        // rmv chunk to idle set.
        mmRbtsetVpt_Rmv(&p->idle, s);

        mmVKDescriptorChunk_DiscardDescriptorPool(s, p->pDevice);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmVKDescriptorChunk_Destroy(s);
        mmFree(s);
    }
}

VkResult
mmVKDescriptorPool_DescriptorSetProduce(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDescriptorSet*                      pSet)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmVKDescriptorChunk* e = NULL;

    assert(NULL != pSet && "pSet is a invalid.");

    e = p->current;
    if (NULL == e)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            e = (struct mmVKDescriptorChunk*)it->k;
            // rmv chunk to idle set.
            mmRbtsetVpt_Rmv(&p->idle, e);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        if (NULL == e)
        {
            // add new chunk.
            e = mmVKDescriptorPool_AddChunk(p);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        // cache the current chunk.
        p->current = e;
    }

    err = mmVKDescriptorChunk_DescriptorSetProduce(e, pSet);
    if (mmVKDescriptorChunk_ProduceComplete(e))
    {
        p->current = NULL;
    }

    return err;
}

void
mmVKDescriptorPool_DescriptorSetRecycle(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDescriptorSet*                      pSet)
{
    struct mmVKDescriptorChunk* e = NULL;

    assert(NULL != pSet && "pSet is a invalid.");

    e = mmVKDescriptorPool_GetChunk(p, pSet->chunk);

    assert(NULL != e && "NULL != e is a invalid.");

    mmVKDescriptorChunk_DescriptorSetRecycle(e, pSet);
    if (mmVKDescriptorChunk_RecycleComplete(e))
    {
        float idle_sz = 0;
        float memb_sz = 0;
        int trim_number = 0;

        // reset the index for reused.
        mmVKDescriptorChunk_ResetIndex(e);
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // add chunk to idle set.
        mmRbtsetVpt_Add(&p->idle, e);
        // 
        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->rbtree.size;
        trim_number = (int)(memb_sz * p->hRateTrimNumber);

        if (idle_sz / memb_sz > p->hRateTrimBorder)
        {
            int i = 0;
            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            // max
            n = mmRb_Last(&p->idle.rbt);
            while (NULL != n && i < trim_number)
            {
                it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                e = (struct mmVKDescriptorChunk*)it->k;
                n = mmRb_Prev(n);
                mmRbtsetVpt_Erase(&p->idle, it);
                mmVKDescriptorPool_RmvChunk(p, (mmUInt64_t)e);
                i++;
            }
        }
    }
}
