/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKCamera_h__
#define __mmVKCamera_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmVKCamera
{
    float projection[4][4];
    float view[4][4];

    // near clip.
    float n;
    // far  clip.
    float f;
};

void
mmVKCamera_Init(
    struct mmVKCamera*                             p);

void
mmVKCamera_Destroy(
    struct mmVKCamera*                             p);

void
mmVKCamera_OrthogonalVK(
    struct mmVKCamera* p,
    float l, float r,
    float b, float t,
    float n, float f);

void
mmVKCamera_PerspectiveVK(
    struct mmVKCamera* p,
    float fovy,
    float aspect,
    float n,
    float f);

#include "core/mmSuffix.h"

#endif//__mmVKCamera_h__
