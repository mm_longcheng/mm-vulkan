/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKImageLoader_h__
#define __mmVKImageLoader_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "ktx.h"

#include "vk/mmVKUploader.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmVKUploader;

extern
ktx_error_code_e
mmVKImageUploader_UploadFromFileContext(
    const char*                                    path,
    ktx_transcode_fmt_e                            transcodefmt,
    struct mmFileContext*                          pFileContext,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage);

struct mmVKImageInfo
{
    // name for image info, read only. 
    // string is weak reference value.
    struct mmString name;

    // image archive.
    struct mmVKImage vImage;

    // default image view for image.
    VkImageView imageView;

    // reference count. 
    size_t reference;
};

void
mmVKImageInfo_Init(
    struct mmVKImageInfo*                          p);

void
mmVKImageInfo_Destroy(
    struct mmVKImageInfo*                          p);

int
mmVKImageInfo_Invalid(
    const struct mmVKImageInfo*                    p);

void
mmVKImageInfo_Increase(
    struct mmVKImageInfo*                          p);

void
mmVKImageInfo_Decrease(
    struct mmVKImageInfo*                          p);

VkResult
mmVKImageInfo_Prepare(
    struct mmVKImageInfo*                          p,
    VkImageCreateInfo*                             pInfo,
    VmaAllocationCreateFlags                       flags,
    VmaMemoryUsage                                 usage,
    VkImageViewType                                viewType,
    VkImageAspectFlags                             aspectMask,
    struct mmVKUploader*                           pUploader);

void
mmVKImageInfo_Discard(
    struct mmVKImageInfo*                          p,
    struct mmVKUploader*                           pUploader);

struct mmVKImageLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKUploader* pUploader;
    struct mmRbtreeStringVpt sets;
    ktx_transcode_fmt_e transcodefmt;
};

void 
mmVKImageLoader_Init(
    struct mmVKImageLoader*                        p);

void 
mmVKImageLoader_Destroy(
    struct mmVKImageLoader*                        p);

void 
mmVKImageLoader_SetFileContext(
    struct mmVKImageLoader*                        p,
    struct mmFileContext*                          pFileContext);

void 
mmVKImageLoader_SetUploader(
    struct mmVKImageLoader*                        p,
    struct mmVKUploader*                           pUploader);

void 
mmVKImageLoader_SetTranscodeFmt(
    struct mmVKImageLoader*                        p,
    ktx_transcode_fmt_e                            transcodefmt);

VkResult
mmVKImageLoader_PrepareImageInfo(
    struct mmVKImageLoader*                        p,
    const char*                                    path,
    struct mmVKImageInfo*                          pImageInfo);

void
mmVKImageLoader_DiscardImageInfo(
    struct mmVKImageLoader*                        p,
    struct mmVKImageInfo*                          pImageInfo);

void 
mmVKImageLoader_UnloadByFile(
    struct mmVKImageLoader*                        p,
    const char*                                    path);

void
mmVKImageLoader_UnloadComplete(
    struct mmVKImageLoader*                        p);

struct mmVKImageInfo* 
mmVKImageLoader_LoadFromFile(
    struct mmVKImageLoader*                        p,
    const char*                                    path);

struct mmVKImageInfo*
mmVKImageLoader_LoadFromFilePriority(
    struct mmVKImageLoader*                        p,
    const char*                                    path,
    const char*                                    suffix);

#include "core/mmSuffix.h"

#endif//__mmVKImageLoader_h__
