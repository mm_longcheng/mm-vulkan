/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPoolLoader.h"

#include "vk/mmVKLayoutLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKPoolInfo_Init(
    struct mmVKPoolInfo*                           p)
{
    mmString_Init(&p->name);
    mmVKDescriptorPool_Init(&p->vDescriptorPool);
    p->reference = 0;
}

void
mmVKPoolInfo_Destroy(
    struct mmVKPoolInfo*                           p)
{
    p->reference = 0;
    mmVKDescriptorPool_Destroy(&p->vDescriptorPool);
    mmString_Destroy(&p->name);
}

int
mmVKPoolInfo_Invalid(
    const struct mmVKPoolInfo*                     p)
{
    return
        (NULL == p) ||
        (0 == p->vDescriptorPool.maxSets);
}

void
mmVKPoolInfo_Increase(
    struct mmVKPoolInfo*                           p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKPoolInfo_Decrease(
    struct mmVKPoolInfo*                           p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKPoolInfo_Prepare(
    struct mmVKPoolInfo*                           p,
    struct mmVKDevice*                             pDevice,
    struct mmVKLayoutInfo*                         pInfo)
{
    return mmVKDescriptorPool_Create(&p->vDescriptorPool, pDevice, pInfo);
}

void
mmVKPoolInfo_Discard(
    struct mmVKPoolInfo*                           p,
    struct mmVKDevice*                             pDevice)
{
    mmVKDescriptorPool_Delete(&p->vDescriptorPool);
}

void
mmVKPoolLoader_Init(
    struct mmVKPoolLoader*                         p)
{
    p->pFileContext = NULL;
    p->pDevice = NULL;
    p->pLayoutLoader = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
}

void
mmVKPoolLoader_Destroy(
    struct mmVKPoolLoader*                         p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pLayoutLoader = NULL;
    p->pDevice = NULL;
    p->pFileContext = NULL;
}

void
mmVKPoolLoader_SetFileContext(
    struct mmVKPoolLoader*                         p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmVKPoolLoader_SetDevice(
    struct mmVKPoolLoader*                         p,
    struct mmVKDevice*                             pDevice)
{
    p->pDevice = pDevice;
}

void
mmVKPoolLoader_SetLayoutLoader(
    struct mmVKPoolLoader*                         p,
    struct mmVKLayoutLoader*                       pLayoutLoader)
{
    p->pLayoutLoader = pLayoutLoader;
}

VkResult
mmVKPoolLoader_PreparePoolInfo(
    struct mmVKPoolLoader*                         p,
    struct mmVKLayoutInfo*                         pInfo,
    struct mmVKPoolInfo*                           pPoolInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pPoolInfo->reference && "reference not zero.");

        if (NULL == pInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKPoolInfo_Prepare(pPoolInfo, p->pDevice, pInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPoolInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        pPoolInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKPoolLoader_DiscardPoolInfo(
    struct mmVKPoolLoader*                         p,
    struct mmVKPoolInfo*                           pPoolInfo)
{
    assert(0 == pPoolInfo->reference && "reference not zero.");
    mmVKPoolInfo_Discard(pPoolInfo, p->pDevice);
}

void
mmVKPoolLoader_UnloadByName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name)
{
    struct mmVKPoolInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPoolInfo*)it->v;
        mmVKPoolLoader_DiscardPoolInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPoolInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKPoolLoader_UnloadComplete(
    struct mmVKPoolLoader*                         p)
{
    struct mmVKPoolInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKPoolInfo*)(it->v);
        mmVKPoolLoader_DiscardPoolInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKPoolInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKPoolInfo*
mmVKPoolLoader_LoadFromInfo(
    struct mmVKPoolLoader*                         p,
    const char*                                    name,
    struct mmVKLayoutInfo*                         pInfo)
{
    struct mmVKPoolInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKPoolInfo*)mmMalloc(sizeof(struct mmVKPoolInfo));
        mmVKPoolInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKPoolLoader_PreparePoolInfo(p, pInfo, s);
    }
    else
    {
        s = (struct mmVKPoolInfo*)(it->v);
    }
    return s;
}

struct mmVKPoolInfo*
mmVKPoolLoader_LoadFromLayoutName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name,
    const char*                                    pLayoutName)
{
    struct mmVKPoolInfo* s = NULL;

    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmString hWeakKey;

        struct mmVKLayoutInfo* pLayoutInfo = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakKey, name);
        it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
        if (NULL != it)
        {
            s = (struct mmVKPoolInfo*)(it->v);
            break;
        }

        if (NULL == p->pLayoutLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutLoader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pLayoutInfo = mmVKLayoutLoader_GetFromName(p->pLayoutLoader, pLayoutName);

        if (NULL == pLayoutInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pLayoutInfo is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        s = (struct mmVKPoolInfo*)mmMalloc(sizeof(struct mmVKPoolInfo));
        mmVKPoolInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKPoolLoader_PreparePoolInfo(p, pLayoutInfo, s);
    } while (0);

    return s;
}

struct mmVKPoolInfo*
mmVKPoolLoader_GetFromName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name)
{
    struct mmVKPoolInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKPoolInfo*)(it->v);
    }
    return s;
}
