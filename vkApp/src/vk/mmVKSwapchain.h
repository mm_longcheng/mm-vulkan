/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKSwapchain_h__
#define __mmVKSwapchain_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKInstance.h"
#include "vk/mmVKSurface.h"
#include "vk/mmVKDevice.h"
#include "vk/mmVKFrame.h"
#include "vk/mmVKUploader.h"

#include "core/mmPrefix.h"

VkSurfaceFormatKHR
mmVKPickSurfaceFormat(
    const VkSurfaceFormatKHR*                      surfaceFormats,
    uint32_t                                       count,
    VkFormat                                       format);

struct mmVKSwapchainFrame
{
    VkImage image;
    VkImageView imageView;
};

struct mmVKSwapchainCreateInfo
{
    struct mmVKInstance* pInstance;
    struct mmVKDevice* pDevice;
    VkPhysicalDevice physicalDevice;
    VkSurfaceKHR surface;
    VkFormat depthFormat;
    VkFormat surfaceFormat;
    VkPresentModeKHR presentMode;
    VkSampleCountFlagBits samples;
    uint32_t windowSize[2];
};

struct mmVKSwapchain
{
    VkSurfaceFormatKHR surfaceFormat;
    VkSwapchainKHR swapchain;
    VkPresentModeKHR presentMode;
    VkFormat depthFormat;
    VkSampleCountFlagBits samples;
    struct mmVKFrame color;
    struct mmVKFrame depth;
    struct mmVKSwapchainFrame* frames;
    uint32_t frameCount;
    uint32_t windowSize[2];
};

void
mmVKSwapchain_Init(
    struct mmVKSwapchain*                          p);

void
mmVKSwapchain_Destroy(
    struct mmVKSwapchain*                          p);

void
mmVKSwapchain_Reset(
    struct mmVKSwapchain*                          p);

VkResult
mmVKSwapchain_EvaluationSuitable(
    struct mmVKSwapchain*                          p,
    struct mmVKInstance*                           pInstance,
    VkPhysicalDevice                               physicalDevice,
    VkSurfaceKHR                                   surface,
    VkFormat                                       surfaceFormat);

VkResult
mmVKSwapchain_PrepareSwapchain(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

void
mmVKSwapchain_DiscardSwapchain(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSwapchain_RebuildSwapchain(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSwapchain_PrepareFrames(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice);

void
mmVKSwapchain_DiscardFrames(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKSwapchain_PrepareColor(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

void
mmVKSwapchain_DiscardColor(
    struct mmVKSwapchain*                          p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSwapchain_PrepareDepthStencil(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

void
mmVKSwapchain_DiscardDepthStencil(
    struct mmVKSwapchain*                          p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSwapchain_Create(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

void
mmVKSwapchain_Delete(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSwapchain_Rebuild(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader);

#include "core/mmSuffix.h"

#endif//__mmVKSwapchain_h__
