/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKUniformType_h__
#define __mmVKUniformType_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// Uniform block object layout need aligned the data type.
//
// https://www.khronos.org/registry/OpenGL/specs/gl/GLSLangSpec.4.60.pdf
// std140 std430

// bool int float double
typedef int      MM_ALIGNED(4) mmUniformBool;
typedef int      MM_ALIGNED(4) mmUniformInt;
typedef uint32_t MM_ALIGNED(4) mmUniformUInt;
typedef float    MM_ALIGNED(4) mmUniformFloat;
typedef double   MM_ALIGNED(8) mmUniformDouble;

// vec2 vec3 vec4
typedef float MM_ALIGNED( 8) mmUniformVec2[2];
typedef float MM_ALIGNED(16) mmUniformVec3[3];
typedef float MM_ALIGNED(16) mmUniformVec4[4];

// mat2 mat3 mat4
typedef mmUniformVec2 MM_ALIGNED( 8) mmUniformMat2[2];
typedef mmUniformVec3 MM_ALIGNED(16) mmUniformMat3[3];
typedef mmUniformVec4 MM_ALIGNED(16) mmUniformMat4[4];

// bvec2 bvec3 bvec4
typedef int MM_ALIGNED( 8) mmUniformBVec2[2];
typedef int MM_ALIGNED(16) mmUniformBVec3[3];
typedef int MM_ALIGNED(16) mmUniformBVec4[4];

// ivec2 ivec3 ivec4
typedef int32_t MM_ALIGNED( 8) mmUniformIVec2[2];
typedef int32_t MM_ALIGNED(16) mmUniformIVec3[3];
typedef int32_t MM_ALIGNED(16) mmUniformIVec4[4];

// uvec2 uvec3 uvec4
typedef uint32_t MM_ALIGNED( 8) mmUniformUVec2[2];
typedef uint32_t MM_ALIGNED(16) mmUniformUVec3[3];
typedef uint32_t MM_ALIGNED(16) mmUniformUVec4[4];

void mmUniformVec2Assign(mmUniformVec2 v, const float d[2]);
void mmUniformVec3Assign(mmUniformVec3 v, const float d[3]);
void mmUniformVec4Assign(mmUniformVec4 v, const float d[4]);

void mmUniformMat2Assign(mmUniformMat2 v, const float d[2][2]);
void mmUniformMat3Assign(mmUniformMat3 v, const float d[3][3]);
void mmUniformMat4Assign(mmUniformMat4 v, const float d[4][4]);

void mmUniformBVec2Assign(mmUniformBVec2 v, const int d[2]);
void mmUniformBVec3Assign(mmUniformBVec3 v, const int d[3]);
void mmUniformBVec4Assign(mmUniformBVec4 v, const int d[4]);

void mmUniformIVec2Assign(mmUniformIVec2 v, const int32_t d[2]);
void mmUniformIVec3Assign(mmUniformIVec3 v, const int32_t d[3]);
void mmUniformIVec4Assign(mmUniformIVec4 v, const int32_t d[4]);

void mmUniformUVec2Assign(mmUniformUVec2 v, const uint32_t d[2]);
void mmUniformUVec3Assign(mmUniformUVec3 v, const uint32_t d[3]);
void mmUniformUVec4Assign(mmUniformUVec4 v, const uint32_t d[4]);

#include "core/mmSuffix.h"

#endif//__mmVKUniformType_h__
