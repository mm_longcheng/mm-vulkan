/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKUploader_h__
#define __mmVKUploader_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKBuffer.h"
#include "vk/mmVKImage.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKUploaderCreateInfo
{
    VmaAllocator vmaAllocator;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkQueue graphicsQueue;
    VkQueue transferQueue;
    VkCommandPool graphicsCmdPool;
    VkCommandPool transferCmdPool;
};

struct mmVKUploader
{
    VmaAllocator vmaAllocator;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkQueue graphicsQueue;
    VkQueue transferQueue;
    VkCommandPool graphicsCmdPool;
    VkCommandPool transferCmdPool;
    VkCommandBuffer graphicsCmdBuffer;
    VkCommandBuffer transferCmdBuffer;
    const VkAllocationCallbacks* pAllocator;

    VkFence copyFence;
    VkSubmitInfo graphicsSubmitInfo;
    VkSubmitInfo transferSubmitInfo;
};

void
mmVKUploader_Init(
    struct mmVKUploader*                           p);

void
mmVKUploader_Destroy(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_Create(
    struct mmVKUploader*                           p,
    const struct mmVKUploaderCreateInfo*           info,
    const VkAllocationCallbacks*                   pAllocator);

void
mmVKUploader_Delete(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_GraphicsCommandBufferBegin(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_TransferCommandBufferBegin(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_GraphicsCommandBufferEnd(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_TransferCommandBufferEnd(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_GraphicsQueueSubmitWait(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_TransferQueueSubmitWait(
    struct mmVKUploader*                           p);

VkResult
mmVKUploader_CreateGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    const VkBufferCreateInfo*                      buffInfo,
    VmaAllocationCreateFlags                       flags,
    struct mmVKBuffer*                             pBuffer);

VkResult
mmVKUploader_UpdateGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset);

VkResult
mmVKUploader_UploadGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    VkBufferUsageFlags                             usage,
    VmaAllocationCreateFlags                       flags,
    const uint8_t*                                 buffer,
    size_t                                         length,
    struct mmVKBuffer*                             pBuffer);

VkResult
mmVKUploader_CreateCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    const VkBufferCreateInfo*                      buffInfo,
    VmaAllocationCreateFlags                       flags,
    struct mmVKBuffer*                             pBuffer);

VkResult
mmVKUploader_UpdateCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset);

VkResult
mmVKUploader_UploadCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    VkBufferUsageFlags                             usage,
    VmaAllocationCreateFlags                       flags,
    const uint8_t*                                 buffer,
    size_t                                         length,
    struct mmVKBuffer*                             pBuffer);

void
mmVKUploader_DeleteBuffer(
    struct mmVKUploader*                           p,
    struct mmVKBuffer*                             pBuffer);

VkResult
mmVKUploader_CreateImage(
    struct mmVKUploader*                           p,
    VkImageCreateInfo*                             pImageInfo,
    VmaAllocationCreateFlags                       flags,
    VmaMemoryUsage                                 usage,
    VkImageViewType                                viewType,
    struct mmVKImage*                              pImage);

void
mmVKUploader_DeleteImage(
    struct mmVKUploader*                           p,
    struct mmVKImage*                              pImage);

VkResult
mmVKUploader_PrepareImageView(
    struct mmVKUploader*                           pUploader,
    const struct mmVKImage*                        vImage,
    VkImageViewType                                viewType,
    VkImageAspectFlags                             aspectMask,
    VkImageView*                                   pImageView);

void
mmVKUploader_DiscardImageView(
    struct mmVKUploader*                           pUploader,
    VkImageView                                    imageView);

VkResult
mmVKUploader_CopyBufferToImage(
    struct mmVKUploader*                           pUploader,
    const struct mmVKBuffer*                       pBuffer,
    struct mmVKImage*                              pImage,
    VkImageLayout                                  finalLayout);

VkResult
mmVKUploader_SetVKImageLayout(
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage,
    VkImageAspectFlags                             aspectMask,
    VkImageLayout                                  finalLayout);

VkResult
mmVKUploader_SetImageLayout(
    struct mmVKUploader*                           pUploader,
    VkImage                                        image,
    VkImageLayout                                  oldLayout,
    VkImageLayout                                  newLayout,
    VkImageSubresourceRange*                       pSubresourceRange);

#include "core/mmSuffix.h"

#endif//__mmVKUploader_h__
