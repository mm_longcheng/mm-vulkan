/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDescriptorLayout.h"

#include "core/mmAlloc.h"

#include "algorithm/mmMurmurHash.h"

const char*
mmVKShaderStageFlags_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_SHADER_STAGE_VERTEX_BIT:
        return "VK_SHADER_STAGE_VERTEX_BIT";
    case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
        return "VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT";
    case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
        return "VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT";
    case VK_SHADER_STAGE_GEOMETRY_BIT:
        return "VK_SHADER_STAGE_GEOMETRY_BIT";
    case VK_SHADER_STAGE_FRAGMENT_BIT:
        return "VK_SHADER_STAGE_FRAGMENT_BIT";
    case VK_SHADER_STAGE_COMPUTE_BIT:
        return "VK_SHADER_STAGE_COMPUTE_BIT";
    case VK_SHADER_STAGE_ALL_GRAPHICS:
        return "VK_SHADER_STAGE_ALL_GRAPHICS";
    case VK_SHADER_STAGE_ALL:
        return "VK_SHADER_STAGE_ALL";
#if (VK_HEADER_VERSION >= 158)
    case VK_SHADER_STAGE_RAYGEN_BIT_KHR:
        return "VK_SHADER_STAGE_RAYGEN_BIT_KHR";
    case VK_SHADER_STAGE_ANY_HIT_BIT_KHR:
        return "VK_SHADER_STAGE_ANY_HIT_BIT_KHR";
    case VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR:
        return "VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR";
    case VK_SHADER_STAGE_MISS_BIT_KHR:
        return "VK_SHADER_STAGE_MISS_BIT_KHR";
    case VK_SHADER_STAGE_INTERSECTION_BIT_KHR:
        return "VK_SHADER_STAGE_INTERSECTION_BIT_KHR";
    case VK_SHADER_STAGE_CALLABLE_BIT_KHR:
        return "VK_SHADER_STAGE_CALLABLE_BIT_KHR";
#endif// VK_HEADER_VERSION
    case VK_SHADER_STAGE_TASK_BIT_NV:
        return "VK_SHADER_STAGE_TASK_BIT_NV";
    case VK_SHADER_STAGE_MESH_BIT_NV:
        return "VK_SHADER_STAGE_MESH_BIT_NV";
    default:
        return "VK_SHADER_STAGE_VERTEX_BIT";
    }
}

int
mmVKShaderStageFlags_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x0E1BECC7:
        // 0x0E1BECC7 VK_SHADER_STAGE_VERTEX_BIT
        return VK_SHADER_STAGE_VERTEX_BIT;
    case 0x2FAE7B59:
        // 0x2FAE7B59 VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT
        return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case 0x8962B790:
        // 0x8962B790 VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT
        return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    case 0x918CE1AF:
        // 0x918CE1AF VK_SHADER_STAGE_GEOMETRY_BIT
        return VK_SHADER_STAGE_GEOMETRY_BIT;
    case 0xE49729A7:
        // 0xE49729A7 VK_SHADER_STAGE_FRAGMENT_BIT
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    case 0x77BCDCF9:
        // 0x77BCDCF9 VK_SHADER_STAGE_COMPUTE_BIT
        return VK_SHADER_STAGE_COMPUTE_BIT;
    case 0x1DBD411A:
        // 0x1DBD411A VK_SHADER_STAGE_ALL_GRAPHICS
        return VK_SHADER_STAGE_ALL_GRAPHICS;
    case 0x5A7F58E6:
        // 0x5A7F58E6 VK_SHADER_STAGE_ALL
        return VK_SHADER_STAGE_ALL;
#if (VK_HEADER_VERSION >= 158)
    case 0x3A97174F:
        // 0x3A97174F VK_SHADER_STAGE_RAYGEN_BIT_KHR
        return VK_SHADER_STAGE_RAYGEN_BIT_KHR;
    case 0x96159F8E:
        // 0x96159F8E VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        return VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
    case 0x27DC3F42:
        // 0x27DC3F42 VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR
        return VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    case 0x41C94616:
        // 0x41C94616 VK_SHADER_STAGE_MISS_BIT_KHR
        return VK_SHADER_STAGE_MISS_BIT_KHR;
    case 0xE8B9EB1A:
        // 0xE8B9EB1A VK_SHADER_STAGE_INTERSECTION_BIT_KHR
        return VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
    case 0x5B81641F:
        // 0x5B81641F VK_SHADER_STAGE_CALLABLE_BIT_KHR
        return VK_SHADER_STAGE_CALLABLE_BIT_KHR;
#endif// VK_HEADER_VERSION
    case 0x0022E0CA:
        // 0x0022E0CA VK_SHADER_STAGE_TASK_BIT_NV
        return VK_SHADER_STAGE_TASK_BIT_NV;
    case 0x6678B746:
        // 0x6678B746 VK_SHADER_STAGE_MESH_BIT_NV
        return VK_SHADER_STAGE_MESH_BIT_NV;
    case 0xE9C28F7C:
        // 0xE9C28F7C VK_SHADER_STAGE_RAYGEN_BIT_NV
        return VK_SHADER_STAGE_RAYGEN_BIT_NV;
    case 0x0F7AE91F:
        // 0x0F7AE91F VK_SHADER_STAGE_ANY_HIT_BIT_NV
        return VK_SHADER_STAGE_ANY_HIT_BIT_NV;
    case 0x647010E0:
        // 0x647010E0 VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV
        return VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;
    case 0x00B1D188:
        // 0x00B1D188 VK_SHADER_STAGE_MISS_BIT_NV
        return VK_SHADER_STAGE_MISS_BIT_NV;
    case 0x396CBB5D:
        // 0x396CBB5D VK_SHADER_STAGE_INTERSECTION_BIT_NV
        return VK_SHADER_STAGE_INTERSECTION_BIT_NV;
    case 0x3D7BC645:
        // 0x3D7BC645 VK_SHADER_STAGE_CALLABLE_BIT_NV
        return VK_SHADER_STAGE_CALLABLE_BIT_NV;
    default:
        return VK_SHADER_STAGE_VERTEX_BIT;
    }
}

const char*
mmVKDescriptorType_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_DESCRIPTOR_TYPE_SAMPLER:
        return "VK_DESCRIPTOR_TYPE_SAMPLER";
    case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
        return "VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER";
    case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
        return "VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE";
    case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE:
        return "VK_DESCRIPTOR_TYPE_STORAGE_IMAGE";
    case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
        return "VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER";
    case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
        return "VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER";
    case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
        return "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER";
    case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
        return "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER";
    case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
        return "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC";
    case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
        return "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC";
    case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
        return "VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT";
#if (VK_HEADER_VERSION >= 121)
    case VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT:
        return "VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT";
#endif// VK_HEADER_VERSION
#if (VK_HEADER_VERSION >= 158)
    case VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR:
        return "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR";
#endif// VK_HEADER_VERSION
#if (VK_HEADER_VERSION >= 170)
    case VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV:
        return "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV";
    case VK_DESCRIPTOR_TYPE_MUTABLE_VALVE:
        return "VK_DESCRIPTOR_TYPE_MUTABLE_VALVE";
#endif// VK_HEADER_VERSION
    default:
        return "VK_DESCRIPTOR_TYPE_SAMPLER";
    }
}

int
mmVKDescriptorType_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x7FFD7A9C:
        // 0x7FFD7A9C VK_DESCRIPTOR_TYPE_SAMPLER
        return VK_DESCRIPTOR_TYPE_SAMPLER;
    case 0x9B4BC0CC:
        // 0x9B4BC0CC VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
        return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    case 0x934E2BC8:
        // 0x934E2BC8 VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE
        return VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    case 0xBA8E1F21:
        // 0xBA8E1F21 VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
        return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    case 0xA65C4B1C:
        // 0xA65C4B1C VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER
        return VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
    case 0xDE6594E5:
        // 0xDE6594E5 VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER
        return VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
    case 0x142F650F:
        // 0x142F650F VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
        return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    case 0x52643A61:
        // 0x52643A61 VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
        return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    case 0xEEBC48E1:
        // 0xEEBC48E1 VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
        return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    case 0xD32FD304:
        // 0xD32FD304 VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC
        return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
    case 0xA4614A76:
        // 0xA4614A76 VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT
        return VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
#if (VK_HEADER_VERSION >= 121)
    case 0xEB8F83D8:
        // 0xEB8F83D8 VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT
        return VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT;
#endif// VK_HEADER_VERSION
#if (VK_HEADER_VERSION >= 158)
    case 0xECCB92FF:
        // 0xECCB92FF VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR
        return VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
#endif// VK_HEADER_VERSION
#if (VK_HEADER_VERSION >= 170)
    case 0x7C6DFEAF:
        // 0x7C6DFEAF VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV
        return VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
    case 0x327671B4:
        // 0x327671B4 VK_DESCRIPTOR_TYPE_MUTABLE_VALVE
        return VK_DESCRIPTOR_TYPE_MUTABLE_VALVE;
#endif// VK_HEADER_VERSION
    default:
        return VK_DESCRIPTOR_TYPE_SAMPLER;
    }
}

const char*
mmVKVertexInputRate_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_VERTEX_INPUT_RATE_VERTEX:
        return "VK_VERTEX_INPUT_RATE_VERTEX";
    case VK_VERTEX_INPUT_RATE_INSTANCE:
        return "VK_VERTEX_INPUT_RATE_INSTANCE";
    default:
        return "VK_VERTEX_INPUT_RATE_VERTEX";
    }
}

int
mmVKVertexInputRate_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xA4E90AA1:
        // 0xA4E90AA1 VK_VERTEX_INPUT_RATE_VERTEX
        return VK_VERTEX_INPUT_RATE_VERTEX;
    case 0x8FC65241:
        // 0x8FC65241 VK_VERTEX_INPUT_RATE_INSTANCE
        return VK_VERTEX_INPUT_RATE_INSTANCE;
    default:
        return VK_VERTEX_INPUT_RATE_VERTEX;
    }
}

const char*
mmVKFormat_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_FORMAT_R8_SNORM:
        return "VK_FORMAT_R8_SNORM";
    case VK_FORMAT_R8_SINT:
        return "VK_FORMAT_R8_SINT";
    case VK_FORMAT_R8_UNORM:
        return "VK_FORMAT_R8_UNORM";
    case VK_FORMAT_R8_UINT:
        return "VK_FORMAT_R8_UINT";
    case VK_FORMAT_R16_SNORM:
        return "VK_FORMAT_R16_SNORM";
    case VK_FORMAT_R16_SINT:
        return "VK_FORMAT_R16_SINT";
    case VK_FORMAT_R16_UNORM:
        return "VK_FORMAT_R16_UNORM";
    case VK_FORMAT_R16_UINT:
        return "VK_FORMAT_R16_UINT";
    case VK_FORMAT_R32_SINT:
        return "VK_FORMAT_R32_SINT";
    case VK_FORMAT_R32_UINT:
        return "VK_FORMAT_R32_UINT";
    case VK_FORMAT_R32_SFLOAT:
        return "VK_FORMAT_R32_SFLOAT";
    case VK_FORMAT_R64_SFLOAT:
        return "VK_FORMAT_R64_SFLOAT";
    case VK_FORMAT_R8G8_SNORM:
        return "VK_FORMAT_R8G8_SNORM";
    case VK_FORMAT_R8G8_SINT:
        return "VK_FORMAT_R8G8_SINT";
    case VK_FORMAT_R8G8_UNORM:
        return "VK_FORMAT_R8G8_UNORM";
    case VK_FORMAT_R8G8_UINT:
        return "VK_FORMAT_R8G8_UINT";
    case VK_FORMAT_R16G16_SNORM:
        return "VK_FORMAT_R16G16_SNORM";
    case VK_FORMAT_R16G16_SINT:
        return "VK_FORMAT_R16G16_SINT";
    case VK_FORMAT_R16G16_UNORM:
        return "VK_FORMAT_R16G16_UNORM";
    case VK_FORMAT_R16G16_UINT:
        return "VK_FORMAT_R16G16_UINT";
    case VK_FORMAT_R32G32_SINT:
        return "VK_FORMAT_R32G32_SINT";
    case VK_FORMAT_R32G32_UINT:
        return "VK_FORMAT_R32G32_UINT";
    case VK_FORMAT_R32G32_SFLOAT:
        return "VK_FORMAT_R32G32_SFLOAT";
    case VK_FORMAT_R64G64_SFLOAT:
        return "VK_FORMAT_R64G64_SFLOAT";
    case VK_FORMAT_R8G8B8_SNORM:
        return "VK_FORMAT_R8G8B8_SNORM";
    case VK_FORMAT_R8G8B8_SINT:
        return "VK_FORMAT_R8G8B8_SINT";
    case VK_FORMAT_R8G8B8_UNORM:
        return "VK_FORMAT_R8G8B8_UNORM";
    case VK_FORMAT_R8G8B8_UINT:
        return "VK_FORMAT_R8G8B8_UINT";
    case VK_FORMAT_R16G16B16_SNORM:
        return "VK_FORMAT_R16G16B16_SNORM";
    case VK_FORMAT_R16G16B16_SINT:
        return "VK_FORMAT_R16G16B16_SINT";
    case VK_FORMAT_R16G16B16_UNORM:
        return "VK_FORMAT_R16G16B16_UNORM";
    case VK_FORMAT_R16G16B16_UINT:
        return "VK_FORMAT_R16G16B16_UINT";
    case VK_FORMAT_R32G32B32_SINT:
        return "VK_FORMAT_R32G32B32_SINT";
    case VK_FORMAT_R32G32B32_UINT:
        return "VK_FORMAT_R32G32B32_UINT";
    case VK_FORMAT_R32G32B32_SFLOAT:
        return "VK_FORMAT_R32G32B32_SFLOAT";
    case VK_FORMAT_R64G64B64_SFLOAT:
        return "VK_FORMAT_R64G64B64_SFLOAT";
    case VK_FORMAT_R8G8B8A8_SNORM:
        return "VK_FORMAT_R8G8B8A8_SNORM";
    case VK_FORMAT_R8G8B8A8_SINT:
        return "VK_FORMAT_R8G8B8A8_SINT";
    case VK_FORMAT_R8G8B8A8_UNORM:
        return "VK_FORMAT_R8G8B8A8_UNORM";
    case VK_FORMAT_R8G8B8A8_UINT:
        return "VK_FORMAT_R8G8B8A8_UINT";
    case VK_FORMAT_R16G16B16A16_SNORM:
        return "VK_FORMAT_R16G16B16A16_SNORM";
    case VK_FORMAT_R16G16B16A16_SINT:
        return "VK_FORMAT_R16G16B16A16_SINT";
    case VK_FORMAT_R16G16B16A16_UNORM:
        return "VK_FORMAT_R16G16B16A16_UNORM";
    case VK_FORMAT_R16G16B16A16_UINT:
        return "VK_FORMAT_R16G16B16A16_UINT";
    case VK_FORMAT_R32G32B32A32_SINT:
        return "VK_FORMAT_R32G32B32A32_SINT";
    case VK_FORMAT_R32G32B32A32_UINT:
        return "VK_FORMAT_R32G32B32A32_UINT";
    case VK_FORMAT_R32G32B32A32_SFLOAT:
        return "VK_FORMAT_R32G32B32A32_SFLOAT";
    case VK_FORMAT_R64G64B64A64_SFLOAT:
        return "VK_FORMAT_R64G64B64A64_SFLOAT";
    default:
        return "VK_FORMAT_UNDEFINED";
    }
}

int
mmVKFormat_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xA62C4A8E:
        // 0xA62C4A8E VK_FORMAT_R8_SNORM
        return VK_FORMAT_R8_SNORM;
    case 0x23369478:
        // 0x23369478 VK_FORMAT_R8_SINT
        return VK_FORMAT_R8_SINT;
    case 0xDA457BF5:
        // 0xDA457BF5 VK_FORMAT_R8_UNORM
        return VK_FORMAT_R8_UNORM;
    case 0xB6BD13BA:
        // 0xB6BD13BA VK_FORMAT_R8_UINT
        return VK_FORMAT_R8_UINT;
    case 0x4D71F594:
        // 0x4D71F594 VK_FORMAT_R16_SNORM
        return VK_FORMAT_R16_SNORM;
    case 0xFD42B975:
        // 0xFD42B975 VK_FORMAT_R16_SINT
        return VK_FORMAT_R16_SINT;
    case 0x57396C64:
        // 0x57396C64 VK_FORMAT_R16_UNORM
        return VK_FORMAT_R16_UNORM;
    case 0x6F10970E:
        // 0x6F10970E VK_FORMAT_R16_UINT
        return VK_FORMAT_R16_UINT;
    case 0x48C61BBE:
        // 0x48C61BBE VK_FORMAT_R32_SINT
        return VK_FORMAT_R32_SINT;
    case 0xC5F731DA:
        // 0xC5F731DA VK_FORMAT_R32_UINT
        return VK_FORMAT_R32_UINT;
    case 0xDB67BC4F:
        // 0xDB67BC4F VK_FORMAT_R32_SFLOAT
        return VK_FORMAT_R32_SFLOAT;
    case 0x594B9325:
        // 0x594B9325 VK_FORMAT_R64_SFLOAT
        return VK_FORMAT_R64_SFLOAT;
    case 0xEB05F326:
        // 0xEB05F326 VK_FORMAT_R8G8_SNORM
        return VK_FORMAT_R8G8_SNORM;
    case 0x7815357F:
        // 0x7815357F VK_FORMAT_R8G8_SINT
        return VK_FORMAT_R8G8_SINT;
    case 0x55F08FF3:
        // 0x55F08FF3 VK_FORMAT_R8G8_UNORM
        return VK_FORMAT_R8G8_UNORM;
    case 0x049739C6:
        // 0x049739C6 VK_FORMAT_R8G8_UINT
        return VK_FORMAT_R8G8_UINT;
    case 0x61036A82:
        // 0x61036A82 VK_FORMAT_R16G16_SNORM
        return VK_FORMAT_R16G16_SNORM;
    case 0x6AD7B544:
        // 0x6AD7B544 VK_FORMAT_R16G16_SINT
        return VK_FORMAT_R16G16_SINT;
    case 0x4C3CCC09:
        // 0x4C3CCC09 VK_FORMAT_R16G16_UNORM
        return VK_FORMAT_R16G16_UNORM;
    case 0x40865ECC:
        // 0x40865ECC VK_FORMAT_R16G16_UINT
        return VK_FORMAT_R16G16_UINT;
    case 0x68EBD0DF:
        // 0x68EBD0DF VK_FORMAT_R32G32_SINT
        return VK_FORMAT_R32G32_SINT;
    case 0x85BD5861:
        // 0x85BD5861 VK_FORMAT_R32G32_UINT
        return VK_FORMAT_R32G32_UINT;
    case 0xF9469B8F:
        // 0xF9469B8F VK_FORMAT_R32G32_SFLOAT
        return VK_FORMAT_R32G32_SFLOAT;
    case 0x3C889570:
        // 0x3C889570 VK_FORMAT_R64G64_SFLOAT
        return VK_FORMAT_R64G64_SFLOAT;
    case 0xE84A2F9C:
        // 0xE84A2F9C VK_FORMAT_R8G8B8_SNORM
        return VK_FORMAT_R8G8B8_SNORM;
    case 0x7DE9DED7:
        // 0x7DE9DED7 VK_FORMAT_R8G8B8_SINT
        return VK_FORMAT_R8G8B8_SINT;
    case 0xAA3DE71A:
        // 0xAA3DE71A VK_FORMAT_R8G8B8_UNORM
        return VK_FORMAT_R8G8B8_UNORM;
    case 0x12B45CAE:
        // 0x12B45CAE VK_FORMAT_R8G8B8_UINT
        return VK_FORMAT_R8G8B8_UINT;
    case 0xF9EEE037:
        // 0xF9EEE037 VK_FORMAT_R16G16B16_SNORM
        return VK_FORMAT_R16G16B16_SNORM;
    case 0xBC4E3625:
        // 0xBC4E3625 VK_FORMAT_R16G16B16_SINT
        return VK_FORMAT_R16G16B16_SINT;
    case 0x69C38912:
        // 0x69C38912 VK_FORMAT_R16G16B16_UNORM
        return VK_FORMAT_R16G16B16_UNORM;
    case 0x7E82141B:
        // 0x7E82141B VK_FORMAT_R16G16B16_UINT
        return VK_FORMAT_R16G16B16_UINT;
    case 0x5A86F2BC:
        // 0x5A86F2BC VK_FORMAT_R32G32B32_SINT
        return VK_FORMAT_R32G32B32_SINT;
    case 0x62BE6830:
        // 0x62BE6830 VK_FORMAT_R32G32B32_UINT
        return VK_FORMAT_R32G32B32_UINT;
    case 0x31DF9590:
        // 0x31DF9590 VK_FORMAT_R32G32B32_SFLOAT
        return VK_FORMAT_R32G32B32_SFLOAT;
    case 0x17C02060:
        // 0x17C02060 VK_FORMAT_R64G64B64_SFLOAT
        return VK_FORMAT_R64G64B64_SFLOAT;
    case 0xA20D597F:
        // 0xA20D597F VK_FORMAT_R8G8B8A8_SNORM
        return VK_FORMAT_R8G8B8A8_SNORM;
    case 0x7ED2D959:
        // 0x7ED2D959 VK_FORMAT_R8G8B8A8_SINT
        return VK_FORMAT_R8G8B8A8_SINT;
    case 0x929305F7:
        // 0x929305F7 VK_FORMAT_R8G8B8A8_UNORM
        return VK_FORMAT_R8G8B8A8_UNORM;
    case 0xA7A36010:
        // 0xA7A36010 VK_FORMAT_R8G8B8A8_UINT
        return VK_FORMAT_R8G8B8A8_UINT;
    case 0x24A5309A:
        // 0x24A5309A VK_FORMAT_R16G16B16A16_SNORM
        return VK_FORMAT_R16G16B16A16_SNORM;
    case 0xB6A42721:
        // 0xB6A42721 VK_FORMAT_R16G16B16A16_SINT
        return VK_FORMAT_R16G16B16A16_SINT;
    case 0x084664F1:
        // 0x084664F1 VK_FORMAT_R16G16B16A16_UNORM
        return VK_FORMAT_R16G16B16A16_UNORM;
    case 0xFD2E8F74:
        // 0xFD2E8F74 VK_FORMAT_R16G16B16A16_UINT
        return VK_FORMAT_R16G16B16A16_UINT;
    case 0x5505E2B7:
        // 0x5505E2B7 VK_FORMAT_R32G32B32A32_SINT
        return VK_FORMAT_R32G32B32A32_SINT;
    case 0x58CE86A5:
        // 0x58CE86A5 VK_FORMAT_R32G32B32A32_UINT
        return VK_FORMAT_R32G32B32A32_UINT;
    case 0xB5969C57:
        // 0xB5969C57 VK_FORMAT_R32G32B32A32_SFLOAT
        return VK_FORMAT_R32G32B32A32_SFLOAT;
    case 0xB188AD6D:
        // 0xB188AD6D VK_FORMAT_R64G64B64A64_SFLOAT
        return VK_FORMAT_R64G64B64A64_SFLOAT;
    default:
        return VK_FORMAT_UNDEFINED;
    }
}

const char*
mmVKPrimitiveTopology_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_PRIMITIVE_TOPOLOGY_POINT_LIST:
        return "VK_PRIMITIVE_TOPOLOGY_POINT_LIST";
    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST:
        return "VK_PRIMITIVE_TOPOLOGY_LINE_LIST";
    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
        return "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP";
    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST";
    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP";
    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN";
    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY:
        return "VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY";
    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY:
        return "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY";
    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY";
    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY";
    case VK_PRIMITIVE_TOPOLOGY_PATCH_LIST:
        return "VK_PRIMITIVE_TOPOLOGY_PATCH_LIST";
    default:
        return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST";
    }
}

int
mmVKPrimitiveTopology_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x63190AAE:
        // 0x63190AAE VK_PRIMITIVE_TOPOLOGY_POINT_LIST
        return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    case 0x6A239F7C:
        // 0x6A239F7C VK_PRIMITIVE_TOPOLOGY_LINE_LIST
        return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    case 0x2BACB7B4:
        // 0x2BACB7B4 VK_PRIMITIVE_TOPOLOGY_LINE_STRIP
        return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
    case 0x761B8078:
        // 0x761B8078 VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    case 0xD3D64CC9:
        // 0xD3D64CC9 VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    case 0xA90783FD:
        // 0xA90783FD VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
    case 0xE6F08D1D:
        // 0xE6F08D1D VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY
        return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
    case 0x56339B94:
        // 0x56339B94 VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY
        return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
    case 0x34561A7A:
        // 0x34561A7A VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
    case 0xD7B44D56:
        // 0xD7B44D56 VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
    case 0xC81AA1E2:
        // 0xC81AA1E2 VK_PRIMITIVE_TOPOLOGY_PATCH_LIST
        return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
    default:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }
}

const char*
mmVKPolygonMode_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_POLYGON_MODE_FILL:
        return "VK_POLYGON_MODE_FILL";
    case VK_POLYGON_MODE_LINE:
        return "VK_POLYGON_MODE_LINE";
    case VK_POLYGON_MODE_POINT:
        return "VK_POLYGON_MODE_POINT";
    case VK_POLYGON_MODE_FILL_RECTANGLE_NV:
        return "VK_POLYGON_MODE_FILL_RECTANGLE_NV";
    default:
        return "VK_POLYGON_MODE_FILL";
    }
}

int
mmVKPolygonMode_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x5BA66F79:
        // 0x5BA66F79 VK_POLYGON_MODE_FILL
        return VK_POLYGON_MODE_FILL;
    case 0x29CFE017:
        // 0x29CFE017 VK_POLYGON_MODE_LINE
        return VK_POLYGON_MODE_LINE;
    case 0x05F2A900:
        // 0x05F2A900 VK_POLYGON_MODE_POINT
        return VK_POLYGON_MODE_POINT;
    case 0x75CDDE61:
        // 0x75CDDE61 VK_POLYGON_MODE_FILL_RECTANGLE_NV
        return VK_POLYGON_MODE_FILL_RECTANGLE_NV;
    default:
        return VK_POLYGON_MODE_FILL;
    }
}

const char*
mmVKCullModeFlags_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_CULL_MODE_NONE:
        return "VK_CULL_MODE_NONE";
    case VK_CULL_MODE_FRONT_BIT:
        return "VK_CULL_MODE_FRONT_BIT";
    case VK_CULL_MODE_BACK_BIT:
        return "VK_CULL_MODE_BACK_BIT";
    case VK_CULL_MODE_FRONT_AND_BACK:
        return "VK_CULL_MODE_FRONT_AND_BACK";
    default:
        return "VK_CULL_MODE_NONE";
    }
}

int
mmVKCullModeFlags_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x06A749E7:
        // 0x06A749E7 VK_CULL_MODE_NONE
        return VK_CULL_MODE_NONE;
    case 0xEF7219A2:
        // 0xEF7219A2 VK_CULL_MODE_FRONT_BIT
        return VK_CULL_MODE_FRONT_BIT;
    case 0xE10C8F23:
        // 0xE10C8F23 VK_CULL_MODE_BACK_BIT
        return VK_CULL_MODE_BACK_BIT;
    case 0x55EE473C:
        // 0x55EE473C VK_CULL_MODE_FRONT_AND_BACK
        return VK_CULL_MODE_FRONT_AND_BACK;
    default:
        return VK_CULL_MODE_NONE;
    }
}

const char*
mmVKFrontFace_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_FRONT_FACE_COUNTER_CLOCKWISE:
        return "VK_FRONT_FACE_COUNTER_CLOCKWISE";
    case VK_FRONT_FACE_CLOCKWISE:
        return "VK_FRONT_FACE_CLOCKWISE";
    default:
        return "VK_FRONT_FACE_COUNTER_CLOCKWISE";
    }
}

int
mmVKFrontFace_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xCB5D9EDF:
        // 0xCB5D9EDF VK_FRONT_FACE_COUNTER_CLOCKWISE
        return VK_FRONT_FACE_COUNTER_CLOCKWISE;
    case 0x373664A9:
        // 0x373664A9 VK_FRONT_FACE_CLOCKWISE
        return VK_FRONT_FACE_CLOCKWISE;
    default:
        return VK_FRONT_FACE_COUNTER_CLOCKWISE;
    }
}

const char*
mmVKBlendFactor_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_BLEND_FACTOR_ZERO:
        return "VK_BLEND_FACTOR_ZERO";
    case VK_BLEND_FACTOR_ONE:
        return "VK_BLEND_FACTOR_ONE";
    case VK_BLEND_FACTOR_SRC_COLOR:
        return "VK_BLEND_FACTOR_SRC_COLOR";
    case VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR:
        return "VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR";
    case VK_BLEND_FACTOR_DST_COLOR:
        return "VK_BLEND_FACTOR_DST_COLOR";
    case VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR:
        return "VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR";
    case VK_BLEND_FACTOR_SRC_ALPHA:
        return "VK_BLEND_FACTOR_SRC_ALPHA";
    case VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA:
        return "VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA";
    case VK_BLEND_FACTOR_DST_ALPHA:
        return "VK_BLEND_FACTOR_DST_ALPHA";
    case VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA:
        return "VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA";
    case VK_BLEND_FACTOR_CONSTANT_COLOR:
        return "VK_BLEND_FACTOR_CONSTANT_COLOR";
    case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR:
        return "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR";
    case VK_BLEND_FACTOR_CONSTANT_ALPHA:
        return "VK_BLEND_FACTOR_CONSTANT_ALPHA";
    case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA:
        return "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA";
    case VK_BLEND_FACTOR_SRC_ALPHA_SATURATE:
        return "VK_BLEND_FACTOR_SRC_ALPHA_SATURATE";
    case VK_BLEND_FACTOR_SRC1_COLOR:
        return "VK_BLEND_FACTOR_SRC1_COLOR";
    case VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR:
        return "VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR";
    case VK_BLEND_FACTOR_SRC1_ALPHA:
        return "VK_BLEND_FACTOR_SRC1_ALPHA";
    case VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA:
        return "VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA";
    default:
        return "VK_BLEND_FACTOR_ZERO";
    }
}

int
mmVKBlendFactor_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xAEA45517:
        // 0xAEA45517 VK_BLEND_FACTOR_ZERO
        return VK_BLEND_FACTOR_ZERO;
    case 0x21C60132:
        // 0x21C60132 VK_BLEND_FACTOR_ONE
        return VK_BLEND_FACTOR_ONE;
    case 0x0AB4B642:
        // 0x0AB4B642 VK_BLEND_FACTOR_SRC_COLOR
        return VK_BLEND_FACTOR_SRC_COLOR;
    case 0x624093FC:
        // 0x624093FC VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR
        return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
    case 0xD23EC9F0:
        // 0xD23EC9F0 VK_BLEND_FACTOR_DST_COLOR
        return VK_BLEND_FACTOR_DST_COLOR;
    case 0xB6618113:
        // 0xB6618113 VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR
        return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
    case 0x13A7937F:
        // 0x13A7937F VK_BLEND_FACTOR_SRC_ALPHA
        return VK_BLEND_FACTOR_SRC_ALPHA;
    case 0x3038F88E:
        // 0x3038F88E VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
        return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    case 0xF379C010:
        // 0xF379C010 VK_BLEND_FACTOR_DST_ALPHA
        return VK_BLEND_FACTOR_DST_ALPHA;
    case 0xF43ABB3C:
        // 0xF43ABB3C VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA
        return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
    case 0x2400F996:
        // 0x2400F996 VK_BLEND_FACTOR_CONSTANT_COLOR
        return VK_BLEND_FACTOR_CONSTANT_COLOR;
    case 0xC4055C46:
        // 0xC4055C46 VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR
        return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
    case 0xE30C5B29:
        // 0xE30C5B29 VK_BLEND_FACTOR_CONSTANT_ALPHA
        return VK_BLEND_FACTOR_CONSTANT_ALPHA;
    case 0xE0FB1C5E:
        // 0xE0FB1C5E VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA
        return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
    case 0x564E571C:
        // 0x564E571C VK_BLEND_FACTOR_SRC_ALPHA_SATURATE
        return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
    case 0xAC4F9E89:
        // 0xAC4F9E89 VK_BLEND_FACTOR_SRC1_COLOR
        return VK_BLEND_FACTOR_SRC1_COLOR;
    case 0x7196303E:
        // 0x7196303E VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR
        return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
    case 0xA8555218:
        // 0xA8555218 VK_BLEND_FACTOR_SRC1_ALPHA
        return VK_BLEND_FACTOR_SRC1_ALPHA;
    case 0xC0E88798:
        // 0xC0E88798 VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA
        return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
    default:
        return VK_BLEND_FACTOR_ZERO;
    }
}

const char*
mmVKBlendOp_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_BLEND_OP_ADD:
        return "VK_BLEND_OP_ADD";
    case VK_BLEND_OP_SUBTRACT:
        return "VK_BLEND_OP_SUBTRACT";
    case VK_BLEND_OP_REVERSE_SUBTRACT:
        return "VK_BLEND_OP_REVERSE_SUBTRACT";
    case VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA:
        return "VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA";
    case VK_BLEND_OP_MIN:
        return "VK_BLEND_OP_MIN";
    case VK_BLEND_OP_MAX:
        return "VK_BLEND_OP_MAX";
    case VK_BLEND_OP_ZERO_EXT:
        return "VK_BLEND_OP_ZERO_EXT";
    case VK_BLEND_OP_SRC_EXT:
        return "VK_BLEND_OP_SRC_EXT";
    case VK_BLEND_OP_DST_EXT:
        return "VK_BLEND_OP_DST_EXT";
    case VK_BLEND_OP_SRC_OVER_EXT:
        return "VK_BLEND_OP_SRC_OVER_EXT";
    case VK_BLEND_OP_DST_OVER_EXT:
        return "VK_BLEND_OP_DST_OVER_EXT";
    case VK_BLEND_OP_SRC_IN_EXT:
        return "VK_BLEND_OP_SRC_IN_EXT";
    case VK_BLEND_OP_DST_IN_EXT:
        return "VK_BLEND_OP_DST_IN_EXT";
    case VK_BLEND_OP_SRC_OUT_EXT:
        return "VK_BLEND_OP_SRC_OUT_EXT";
    case VK_BLEND_OP_DST_OUT_EXT:
        return "VK_BLEND_OP_DST_OUT_EXT";
    case VK_BLEND_OP_SRC_ATOP_EXT:
        return "VK_BLEND_OP_SRC_ATOP_EXT";
    case VK_BLEND_OP_DST_ATOP_EXT:
        return "VK_BLEND_OP_DST_ATOP_EXT";
    case VK_BLEND_OP_XOR_EXT:
        return "VK_BLEND_OP_XOR_EXT";
    case VK_BLEND_OP_MULTIPLY_EXT:
        return "VK_BLEND_OP_MULTIPLY_EXT";
    case VK_BLEND_OP_SCREEN_EXT:
        return "VK_BLEND_OP_SCREEN_EXT";
    case VK_BLEND_OP_OVERLAY_EXT:
        return "VK_BLEND_OP_OVERLAY_EXT";
    case VK_BLEND_OP_DARKEN_EXT:
        return "VK_BLEND_OP_DARKEN_EXT";
    case VK_BLEND_OP_LIGHTEN_EXT:
        return "VK_BLEND_OP_LIGHTEN_EXT";
    case VK_BLEND_OP_COLORDODGE_EXT:
        return "VK_BLEND_OP_COLORDODGE_EXT";
    case VK_BLEND_OP_COLORBURN_EXT:
        return "VK_BLEND_OP_COLORBURN_EXT";
    case VK_BLEND_OP_HARDLIGHT_EXT:
        return "VK_BLEND_OP_HARDLIGHT_EXT";
    case VK_BLEND_OP_SOFTLIGHT_EXT:
        return "VK_BLEND_OP_SOFTLIGHT_EXT";
    case VK_BLEND_OP_DIFFERENCE_EXT:
        return "VK_BLEND_OP_DIFFERENCE_EXT";
    case VK_BLEND_OP_EXCLUSION_EXT:
        return "VK_BLEND_OP_EXCLUSION_EXT";
    case VK_BLEND_OP_INVERT_EXT:
        return "VK_BLEND_OP_INVERT_EXT";
    case VK_BLEND_OP_INVERT_RGB_EXT:
        return "VK_BLEND_OP_INVERT_RGB_EXT";
    case VK_BLEND_OP_LINEARDODGE_EXT:
        return "VK_BLEND_OP_LINEARDODGE_EXT";
    case VK_BLEND_OP_LINEARBURN_EXT:
        return "VK_BLEND_OP_LINEARBURN_EXT";
    case VK_BLEND_OP_VIVIDLIGHT_EXT:
        return "VK_BLEND_OP_VIVIDLIGHT_EXT";
    case VK_BLEND_OP_LINEARLIGHT_EXT:
        return "VK_BLEND_OP_LINEARLIGHT_EXT";
    case VK_BLEND_OP_PINLIGHT_EXT:
        return "VK_BLEND_OP_PINLIGHT_EXT";
    case VK_BLEND_OP_HARDMIX_EXT:
        return "VK_BLEND_OP_HARDMIX_EXT";
    case VK_BLEND_OP_HSL_HUE_EXT:
        return "VK_BLEND_OP_HSL_HUE_EXT";
    case VK_BLEND_OP_HSL_SATURATION_EXT:
        return "VK_BLEND_OP_HSL_SATURATION_EXT";
    case VK_BLEND_OP_HSL_COLOR_EXT:
        return "VK_BLEND_OP_HSL_COLOR_EXT";
    case VK_BLEND_OP_HSL_LUMINOSITY_EXT:
        return "VK_BLEND_OP_HSL_LUMINOSITY_EXT";
    case VK_BLEND_OP_PLUS_EXT:
        return "VK_BLEND_OP_PLUS_EXT";
    case VK_BLEND_OP_PLUS_CLAMPED_EXT:
        return "VK_BLEND_OP_PLUS_CLAMPED_EXT";
    case VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT:
        return "VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT";
    case VK_BLEND_OP_PLUS_DARKER_EXT:
        return "VK_BLEND_OP_PLUS_DARKER_EXT";
    case VK_BLEND_OP_MINUS_EXT:
        return "VK_BLEND_OP_MINUS_EXT";
    case VK_BLEND_OP_MINUS_CLAMPED_EXT:
        return "VK_BLEND_OP_MINUS_CLAMPED_EXT";
    case VK_BLEND_OP_CONTRAST_EXT:
        return "VK_BLEND_OP_CONTRAST_EXT";
    case VK_BLEND_OP_INVERT_OVG_EXT:
        return "VK_BLEND_OP_INVERT_OVG_EXT";
    case VK_BLEND_OP_RED_EXT:
        return "VK_BLEND_OP_RED_EXT";
    case VK_BLEND_OP_GREEN_EXT:
        return "VK_BLEND_OP_GREEN_EXT";
    case VK_BLEND_OP_BLUE_EXT:
        return "VK_BLEND_OP_BLUE_EXT";
    default:
        return "VK_BLEND_OP_ADD";
    }
}

int
mmVKBlendOp_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x1334EBF3:
        // 0x1334EBF3 VK_BLEND_OP_ADD
        return VK_BLEND_OP_ADD;
    case 0xCE87DEEE:
        // 0xCE87DEEE VK_BLEND_OP_SUBTRACT
        return VK_BLEND_OP_SUBTRACT;
    case 0x48382192:
        // 0x48382192 VK_BLEND_OP_REVERSE_SUBTRACT
        return VK_BLEND_OP_REVERSE_SUBTRACT;
    case 0x78B246BC:
        // 0x78B246BC VK_BLEND_OP_MIN
        return VK_BLEND_OP_MIN;
    case 0x4A46FD37:
        // 0x4A46FD37 VK_BLEND_OP_MAX
        return VK_BLEND_OP_MAX;
    case 0xE34D34D9:
        // 0xE34D34D9 VK_BLEND_OP_ZERO_EXT
        return VK_BLEND_OP_ZERO_EXT;
    case 0x07875150:
        // 0x07875150 VK_BLEND_OP_SRC_EXT
        return VK_BLEND_OP_SRC_EXT;
    case 0xA0917D34:
        // 0xA0917D34 VK_BLEND_OP_DST_EXT
        return VK_BLEND_OP_DST_EXT;
    case 0x69D40F1F:
        // 0x69D40F1F VK_BLEND_OP_SRC_OVER_EXT
        return VK_BLEND_OP_SRC_OVER_EXT;
    case 0x48CA76A7:
        // 0x48CA76A7 VK_BLEND_OP_DST_OVER_EXT
        return VK_BLEND_OP_DST_OVER_EXT;
    case 0x72993232:
        // 0x72993232 VK_BLEND_OP_SRC_IN_EXT
        return VK_BLEND_OP_SRC_IN_EXT;
    case 0x1E05B1A6:
        // 0x1E05B1A6 VK_BLEND_OP_DST_IN_EXT
        return VK_BLEND_OP_DST_IN_EXT;
    case 0xD1115520:
        // 0xD1115520 VK_BLEND_OP_SRC_OUT_EXT
        return VK_BLEND_OP_SRC_OUT_EXT;
    case 0xAC29F15A:
        // 0xAC29F15A VK_BLEND_OP_DST_OUT_EXT
        return VK_BLEND_OP_DST_OUT_EXT;
    case 0xB7086880:
        // 0xB7086880 VK_BLEND_OP_SRC_ATOP_EXT
        return VK_BLEND_OP_SRC_ATOP_EXT;
    case 0x0EFE83E2:
        // 0x0EFE83E2 VK_BLEND_OP_DST_ATOP_EXT
        return VK_BLEND_OP_DST_ATOP_EXT;
    case 0x38740460:
        // 0x38740460 VK_BLEND_OP_XOR_EXT
        return VK_BLEND_OP_XOR_EXT;
    case 0x75A87282:
        // 0x75A87282 VK_BLEND_OP_MULTIPLY_EXT
        return VK_BLEND_OP_MULTIPLY_EXT;
    case 0x2BF1B378:
        // 0x2BF1B378 VK_BLEND_OP_SCREEN_EXT
        return VK_BLEND_OP_SCREEN_EXT;
    case 0x06ADF1D0:
        // 0x06ADF1D0 VK_BLEND_OP_OVERLAY_EXT
        return VK_BLEND_OP_OVERLAY_EXT;
    case 0xE1E4A32C:
        // 0xE1E4A32C VK_BLEND_OP_DARKEN_EXT
        return VK_BLEND_OP_DARKEN_EXT;
    case 0x9DCC8651:
        // 0x9DCC8651 VK_BLEND_OP_LIGHTEN_EXT
        return VK_BLEND_OP_LIGHTEN_EXT;
    case 0xCC69DC24:
        // 0xCC69DC24 VK_BLEND_OP_COLORDODGE_EXT
        return VK_BLEND_OP_COLORDODGE_EXT;
    case 0x6F5E1472:
        // 0x6F5E1472 VK_BLEND_OP_COLORBURN_EXT
        return VK_BLEND_OP_COLORBURN_EXT;
    case 0xA452783A:
        // 0xA452783A VK_BLEND_OP_HARDLIGHT_EXT
        return VK_BLEND_OP_HARDLIGHT_EXT;
    case 0xDA65E011:
        // 0xDA65E011 VK_BLEND_OP_SOFTLIGHT_EXT
        return VK_BLEND_OP_SOFTLIGHT_EXT;
    case 0xF86C06F7:
        // 0xF86C06F7 VK_BLEND_OP_DIFFERENCE_EXT
        return VK_BLEND_OP_DIFFERENCE_EXT;
    case 0xBB3B6796:
        // 0xBB3B6796 VK_BLEND_OP_EXCLUSION_EXT
        return VK_BLEND_OP_EXCLUSION_EXT;
    case 0x050148AE:
        // 0x050148AE VK_BLEND_OP_INVERT_EXT
        return VK_BLEND_OP_INVERT_EXT;
    case 0x0AF79AD4:
        // 0x0AF79AD4 VK_BLEND_OP_INVERT_RGB_EXT
        return VK_BLEND_OP_INVERT_RGB_EXT;
    case 0x06EB4639:
        // 0x06EB4639 VK_BLEND_OP_LINEARDODGE_EXT
        return VK_BLEND_OP_LINEARDODGE_EXT;
    case 0x1672471E:
        // 0x1672471E VK_BLEND_OP_LINEARBURN_EXT
        return VK_BLEND_OP_LINEARBURN_EXT;
    case 0x920F3B57:
        // 0x920F3B57 VK_BLEND_OP_VIVIDLIGHT_EXT
        return VK_BLEND_OP_VIVIDLIGHT_EXT;
    case 0x61F98F28:
        // 0x61F98F28 VK_BLEND_OP_LINEARLIGHT_EXT
        return VK_BLEND_OP_LINEARLIGHT_EXT;
    case 0x99908B5A:
        // 0x99908B5A VK_BLEND_OP_PINLIGHT_EXT
        return VK_BLEND_OP_PINLIGHT_EXT;
    case 0x2413624B:
        // 0x2413624B VK_BLEND_OP_HARDMIX_EXT
        return VK_BLEND_OP_HARDMIX_EXT;
    case 0x30D8BBDE:
        // 0x30D8BBDE VK_BLEND_OP_HSL_HUE_EXT
        return VK_BLEND_OP_HSL_HUE_EXT;
    case 0x5312289C:
        // 0x5312289C VK_BLEND_OP_HSL_SATURATION_EXT
        return VK_BLEND_OP_HSL_SATURATION_EXT;
    case 0x64D85BBF:
        // 0x64D85BBF VK_BLEND_OP_HSL_COLOR_EXT
        return VK_BLEND_OP_HSL_COLOR_EXT;
    case 0x1D6FB007:
        // 0x1D6FB007 VK_BLEND_OP_HSL_LUMINOSITY_EXT
        return VK_BLEND_OP_HSL_LUMINOSITY_EXT;
    case 0xB5B1D756:
        // 0xB5B1D756 VK_BLEND_OP_PLUS_EXT
        return VK_BLEND_OP_PLUS_EXT;
    case 0xCAC24632:
        // 0xCAC24632 VK_BLEND_OP_PLUS_CLAMPED_EXT
        return VK_BLEND_OP_PLUS_CLAMPED_EXT;
    case 0xA5EB3559:
        // 0xA5EB3559 VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT
        return VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT;
    case 0x44B65AA9:
        // 0x44B65AA9 VK_BLEND_OP_PLUS_DARKER_EXT
        return VK_BLEND_OP_PLUS_DARKER_EXT;
    case 0x93CC799B:
        // 0x93CC799B VK_BLEND_OP_MINUS_EXT
        return VK_BLEND_OP_MINUS_EXT;
    case 0x31C4F425:
        // 0x31C4F425 VK_BLEND_OP_MINUS_CLAMPED_EXT
        return VK_BLEND_OP_MINUS_CLAMPED_EXT;
    case 0x78EC9002:
        // 0x78EC9002 VK_BLEND_OP_CONTRAST_EXT
        return VK_BLEND_OP_CONTRAST_EXT;
    case 0x6E16FEA3:
        // 0x6E16FEA3 VK_BLEND_OP_INVERT_OVG_EXT
        return VK_BLEND_OP_INVERT_OVG_EXT;
    case 0x35F238D0:
        // 0x35F238D0 VK_BLEND_OP_RED_EXT
        return VK_BLEND_OP_RED_EXT;
    case 0xFAE75128:
        // 0xFAE75128 VK_BLEND_OP_GREEN_EXT
        return VK_BLEND_OP_GREEN_EXT;
    case 0x5ACB6E6D:
        // 0x5ACB6E6D VK_BLEND_OP_BLUE_EXT
        return VK_BLEND_OP_BLUE_EXT;
    default:
        return VK_BLEND_OP_ADD;
    }
}

const char*
mmVKCompareOp_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_COMPARE_OP_NEVER:
        return "VK_COMPARE_OP_NEVER";
    case VK_COMPARE_OP_LESS:
        return "VK_COMPARE_OP_LESS";
    case VK_COMPARE_OP_EQUAL:
        return "VK_COMPARE_OP_EQUAL";
    case VK_COMPARE_OP_LESS_OR_EQUAL:
        return "VK_COMPARE_OP_LESS_OR_EQUAL";
    case VK_COMPARE_OP_GREATER:
        return "VK_COMPARE_OP_GREATER";
    case VK_COMPARE_OP_NOT_EQUAL:
        return "VK_COMPARE_OP_NOT_EQUAL";
    case VK_COMPARE_OP_GREATER_OR_EQUAL:
        return "VK_COMPARE_OP_GREATER_OR_EQUAL";
    case VK_COMPARE_OP_ALWAYS:
        return "VK_COMPARE_OP_ALWAYS";
    default:
        return "VK_COMPARE_OP_NEVER";
    }
}

int
mmVKCompareOp_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x2D08DDF9:
        // 0x2D08DDF9 VK_COMPARE_OP_NEVER
        return VK_COMPARE_OP_NEVER;
    case 0x6EF8F2DC:
        // 0x6EF8F2DC VK_COMPARE_OP_LESS
        return VK_COMPARE_OP_LESS;
    case 0xD9656C7A:
        // 0xD9656C7A VK_COMPARE_OP_EQUAL
        return VK_COMPARE_OP_EQUAL;
    case 0x119EEC47:
        // 0x119EEC47 VK_COMPARE_OP_LESS_OR_EQUAL
        return VK_COMPARE_OP_LESS_OR_EQUAL;
    case 0x7208DEFA:
        // 0x7208DEFA VK_COMPARE_OP_GREATER
        return VK_COMPARE_OP_GREATER;
    case 0x9B9C32E0:
        // 0x9B9C32E0 VK_COMPARE_OP_NOT_EQUAL
        return VK_COMPARE_OP_NOT_EQUAL;
    case 0xC33F0A75:
        // 0xC33F0A75 VK_COMPARE_OP_GREATER_OR_EQUAL
        return VK_COMPARE_OP_GREATER_OR_EQUAL;
    case 0x6473EB4F:
        // 0x6473EB4F VK_COMPARE_OP_ALWAYS
        return VK_COMPARE_OP_ALWAYS;
    default:
        return VK_COMPARE_OP_NEVER;
    }
}

const char*
mmVKStencilOp_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_STENCIL_OP_KEEP:
        return "VK_STENCIL_OP_KEEP";
    case VK_STENCIL_OP_ZERO:
        return "VK_STENCIL_OP_ZERO";
    case VK_STENCIL_OP_REPLACE:
        return "VK_STENCIL_OP_REPLACE";
    case VK_STENCIL_OP_INCREMENT_AND_CLAMP:
        return "VK_STENCIL_OP_INCREMENT_AND_CLAMP";
    case VK_STENCIL_OP_DECREMENT_AND_CLAMP:
        return "VK_STENCIL_OP_DECREMENT_AND_CLAMP";
    case VK_STENCIL_OP_INVERT:
        return "VK_STENCIL_OP_INVERT";
    case VK_STENCIL_OP_INCREMENT_AND_WRAP:
        return "VK_STENCIL_OP_INCREMENT_AND_WRAP";
    case VK_STENCIL_OP_DECREMENT_AND_WRAP:
        return "VK_STENCIL_OP_DECREMENT_AND_WRAP";
    default:
        return "VK_STENCIL_OP_KEEP";
    }
}

int
mmVKStencilOp_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0x5964CD0A:
        // 0x5964CD0A VK_STENCIL_OP_KEEP
        return VK_STENCIL_OP_KEEP;
    case 0x4990C3BD:
        // 0x4990C3BD VK_STENCIL_OP_ZERO
        return VK_STENCIL_OP_ZERO;
    case 0x1AE28B54:
        // 0x1AE28B54 VK_STENCIL_OP_REPLACE
        return VK_STENCIL_OP_REPLACE;
    case 0x959CF136:
        // 0x959CF136 VK_STENCIL_OP_INCREMENT_AND_CLAMP
        return VK_STENCIL_OP_INCREMENT_AND_CLAMP;
    case 0xF5C5EFBD:
        // 0xF5C5EFBD VK_STENCIL_OP_DECREMENT_AND_CLAMP
        return VK_STENCIL_OP_DECREMENT_AND_CLAMP;
    case 0xAEB11BAD:
        // 0xAEB11BAD VK_STENCIL_OP_INVERT
        return VK_STENCIL_OP_INVERT;
    case 0xAE8BFF25:
        // 0xAE8BFF25 VK_STENCIL_OP_INCREMENT_AND_WRAP
        return VK_STENCIL_OP_INCREMENT_AND_WRAP;
    case 0x2487EB3E:
        // 0x2487EB3E VK_STENCIL_OP_DECREMENT_AND_WRAP
        return VK_STENCIL_OP_DECREMENT_AND_WRAP;
    default:
        return VK_STENCIL_OP_KEEP;
    }
}

const char*
mmVKLogicOp_EncodeEnumerate(
    int                                            v)
{
    switch (v)
    {
    case VK_LOGIC_OP_CLEAR:
        return "VK_LOGIC_OP_CLEAR";
    case VK_LOGIC_OP_AND:
        return "VK_LOGIC_OP_AND";
    case VK_LOGIC_OP_AND_REVERSE:
        return "VK_LOGIC_OP_AND_REVERSE";
    case VK_LOGIC_OP_COPY:
        return "VK_LOGIC_OP_COPY";
    case VK_LOGIC_OP_AND_INVERTED:
        return "VK_LOGIC_OP_AND_INVERTED";
    case VK_LOGIC_OP_NO_OP:
        return "VK_LOGIC_OP_NO_OP";
    case VK_LOGIC_OP_XOR:
        return "VK_LOGIC_OP_XOR";
    case VK_LOGIC_OP_OR:
        return "VK_LOGIC_OP_OR";
    case VK_LOGIC_OP_NOR:
        return "VK_LOGIC_OP_NOR";
    case VK_LOGIC_OP_EQUIVALENT:
        return "VK_LOGIC_OP_EQUIVALENT";
    case VK_LOGIC_OP_INVERT:
        return "VK_LOGIC_OP_INVERT";
    case VK_LOGIC_OP_OR_REVERSE:
        return "VK_LOGIC_OP_OR_REVERSE";
    case VK_LOGIC_OP_COPY_INVERTED:
        return "VK_LOGIC_OP_COPY_INVERTED";
    case VK_LOGIC_OP_OR_INVERTED:
        return "VK_LOGIC_OP_OR_INVERTED";
    case VK_LOGIC_OP_NAND:
        return "VK_LOGIC_OP_NAND";
    case VK_LOGIC_OP_SET:
        return "VK_LOGIC_OP_SET";
    default:
        return "VK_LOGIC_OP_CLEAR";
    }
}

int
mmVKLogicOp_DecodeEnumerate(
    const char*                                    w)
{
    size_t len = strlen(w);
    mmUInt32_t h = (mmUInt32_t)mmMurmurHash2((const void*)w, (int)len, 0);
    switch (h)
    {
    case 0xAB721CAD:
        // 0xAB721CAD VK_LOGIC_OP_CLEAR
        return VK_LOGIC_OP_CLEAR;
    case 0xB7868F4C:
        // 0xB7868F4C VK_LOGIC_OP_AND
        return VK_LOGIC_OP_AND;
    case 0xD4AB7494:
        // 0xD4AB7494 VK_LOGIC_OP_AND_REVERSE
        return VK_LOGIC_OP_AND_REVERSE;
    case 0xAAD2AE35:
        // 0xAAD2AE35 VK_LOGIC_OP_COPY
        return VK_LOGIC_OP_COPY;
    case 0xA3EE5227:
        // 0xA3EE5227 VK_LOGIC_OP_AND_INVERTED
        return VK_LOGIC_OP_AND_INVERTED;
    case 0x3AEE7330:
        // 0x3AEE7330 VK_LOGIC_OP_NO_OP
        return VK_LOGIC_OP_NO_OP;
    case 0x3B00A490:
        // 0x3B00A490 VK_LOGIC_OP_XOR
        return VK_LOGIC_OP_XOR;
    case 0x69E14D6B:
        // 0x69E14D6B VK_LOGIC_OP_OR
        return VK_LOGIC_OP_OR;
    case 0x98D9CD5A:
        // 0x98D9CD5A VK_LOGIC_OP_NOR
        return VK_LOGIC_OP_NOR;
    case 0x17ECF405:
        // 0x17ECF405 VK_LOGIC_OP_EQUIVALENT
        return VK_LOGIC_OP_EQUIVALENT;
    case 0xA4D88F32:
        // 0xA4D88F32 VK_LOGIC_OP_INVERT
        return VK_LOGIC_OP_INVERT;
    case 0x233D24C4:
        // 0x233D24C4 VK_LOGIC_OP_OR_REVERSE
        return VK_LOGIC_OP_OR_REVERSE;
    case 0x2BB68145:
        // 0x2BB68145 VK_LOGIC_OP_COPY_INVERTED
        return VK_LOGIC_OP_COPY_INVERTED;
    case 0x5E996C98:
        // 0x5E996C98 VK_LOGIC_OP_OR_INVERTED
        return VK_LOGIC_OP_OR_INVERTED;
    case 0x002687B4:
        // 0x002687B4 VK_LOGIC_OP_NAND
        return VK_LOGIC_OP_NAND;
    case 0x4AFFA669:
        // 0x4AFFA669 VK_LOGIC_OP_SET
        return VK_LOGIC_OP_SET;
    default:
        return VK_LOGIC_OP_CLEAR;
    }
}

void
mmValue_StringToVKDescriptorType(
    const char*                                    str,
    VkDescriptorType*                              val)
{
    (*val) = mmVKDescriptorType_DecodeEnumerate(str);
}

void
mmValue_StringToVKShaderStageFlags(
    const char*                                    str,
    VkShaderStageFlags*                            val)
{
    (*val) = mmVKShaderStageFlags_DecodeEnumerate(str);
}

void
mmValue_StringToVKVertexInputRate(
    const char*                                    str,
    VkShaderStageFlags*                            val)
{
    (*val) = mmVKVertexInputRate_DecodeEnumerate(str);
}

void
mmValue_StringToVKFormat(
    const char*                                    str,
    VkShaderStageFlags*                            val)
{
    (*val) = mmVKFormat_DecodeEnumerate(str);
}

void
mmValue_StringToVKPrimitiveTopology(
    const char*                                    str,
    VkPrimitiveTopology*                           val)
{
    (*val) = mmVKPrimitiveTopology_DecodeEnumerate(str);
}

void
mmValue_StringToVKPolygonMode(
    const char*                                    str,
    VkPolygonMode*                                 val)
{
    (*val) = mmVKPolygonMode_DecodeEnumerate(str);
}

void
mmValue_StringToVKCullModeFlags(
    const char*                                    str,
    VkCullModeFlags*                               val)
{
    (*val) = mmVKCullModeFlags_DecodeEnumerate(str);
}

void
mmValue_StringToVKFrontFace(
    const char*                                    str,
    VkFrontFace*                                   val)
{
    (*val) = mmVKFrontFace_DecodeEnumerate(str);
}

void
mmValue_StringToVKBlendFactor(
    const char*                                    str,
    VkBlendFactor*                                 val)
{
    (*val) = mmVKBlendFactor_DecodeEnumerate(str);
}

void
mmValue_StringToVKBlendOp(
    const char*                                    str,
    VkBlendOp*                                     val)
{
    (*val) = mmVKBlendOp_DecodeEnumerate(str);
}

void
mmValue_StringToVKCompareOp(
    const char*                                    str,
    VkCompareOp*                                   val)
{
    (*val) = mmVKCompareOp_DecodeEnumerate(str);
}

void
mmValue_StringToVKStencilOp(
    const char*                                    str,
    VkStencilOp*                                   val)
{
    (*val) = mmVKStencilOp_DecodeEnumerate(str);
}

void
mmValue_StringToVKLogicOp(
    const char*                                    str,
    VkLogicOp*                                     val)
{
    (*val) = mmVKLogicOp_DecodeEnumerate(str);
}

void
mmVKDescriptorSetLayoutBindings_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkDescriptorSetLayoutBinding));
}

void
mmVKDescriptorSetLayoutBindings_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKDescriptorSetLayoutBindings_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q)
{
    mmVectorValue_CopyFromValue(p, q);
}

void
mmVKPushConstantRanges_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkPushConstantRange));
}

void
mmVKPushConstantRanges_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKPushConstantRanges_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q)
{
    mmVectorValue_CopyFromValue(p, q);
}

void
mmVKDescriptorPoolSizes_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkDescriptorPoolSize));
}

void
mmVKDescriptorPoolSizes_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKVertexInputBindingDescriptions_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkVertexInputBindingDescription));
}

void
mmVKVertexInputBindingDescriptions_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKVertexInputAttributeDescriptions_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkVertexInputAttributeDescription));
}

void
mmVKVertexInputAttributeDescriptions_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKPipelineVertexInputInfo_Init(
    struct mmVKPipelineVertexInputInfo*            p)
{
    mmVKVertexInputBindingDescriptions_Init(&p->vibd);
    mmVKVertexInputAttributeDescriptions_Init(&p->viad);
}

void
mmVKPipelineVertexInputInfo_Destroy(
    struct mmVKPipelineVertexInputInfo*            p)
{
    mmVKVertexInputAttributeDescriptions_Destroy(&p->viad);
    mmVKVertexInputBindingDescriptions_Destroy(&p->vibd);
}

void
mmVKPipelineVertexInputInfo_CopyFrom(
    struct mmVKPipelineVertexInputInfo*            p,
    const struct mmVKPipelineVertexInputInfo*      q)
{
    mmVectorValue_CopyFromValue(&p->vibd, &q->vibd);
    mmVectorValue_CopyFromValue(&p->viad, &q->viad);
}

void
mmVKPipelineShaderStageInfo_Init(
    struct mmVKPipelineShaderStageInfo*            p)
{
    p->stage = VK_SHADER_STAGE_VERTEX_BIT;
    mmString_Init(&p->module);
    mmString_Init(&p->name);
}

void
mmVKPipelineShaderStageInfo_Destroy(
    struct mmVKPipelineShaderStageInfo*            p)
{
    mmString_Destroy(&p->name);
    mmString_Destroy(&p->module);
    p->stage = VK_SHADER_STAGE_VERTEX_BIT;
}

void
mmVKPipelineShaderStageInfo_CopyFrom(
    struct mmVKPipelineShaderStageInfo*            p,
    const struct mmVKPipelineShaderStageInfo*      q)
{
    p->stage = q->stage;
    mmString_Assign(&p->module, &q->module);
    mmString_Assign(&p->name, &q->name);
}

void
mmVKPipelineShaderStageInfos_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKPipelineShaderStageInfo_Init;
    hValueAllocator.Recycle = &mmVKPipelineShaderStageInfo_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKPipelineShaderStageInfo));
}

void
mmVKPipelineShaderStageInfos_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKPipelineShaderStageInfos_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q)
{
    mmVectorValue_CopyFromType(p, q, &mmVKPipelineShaderStageInfo_CopyFrom);
}

void
mmVKPipelineShaderStageCreateInfos_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(VkPipelineShaderStageCreateInfo));
}

void
mmVKPipelineShaderStageCreateInfos_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKLayoutInfoArgs_Init(
    struct mmVKLayoutInfoArgs*                     p)
{
    mmVKDescriptorSetLayoutBindings_Init(&p->dslb);
    p->dslc = 0;
}

void
mmVKLayoutInfoArgs_Destroy(
    struct mmVKLayoutInfoArgs*                     p)
{
    p->dslc = 0;
    mmVKDescriptorSetLayoutBindings_Destroy(&p->dslb);
}

int
mmVKLayoutInfoArgs_Invalid(
    const struct mmVKLayoutInfoArgs*               p)
{
    return
        (NULL == p) ||
        (0 == p->dslb.size) ||
        (0 == p->dslc);
}

void
mmVKLayoutInfoArgs_CopyFrom(
    struct mmVKLayoutInfoArgs*                     p,
    const struct mmVKLayoutInfoArgs*               q)
{
    mmVKDescriptorSetLayoutBindings_CopyFrom(&p->dslb, &q->dslb);
    p->dslc = q->dslc;
}

void
mmVKLayoutNames_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmString_Init;
    hValueAllocator.Recycle = &mmString_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmString));
}

void
mmVKLayoutNames_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKPipelineInputAssemblyState_Init(
    struct mmVKPipelineInputAssemblyState*         p)
{
    mmVKPipelineInputAssemblyState_Reset(p);
}

void
mmVKPipelineInputAssemblyState_Destroy(
    struct mmVKPipelineInputAssemblyState*         p)
{
    mmVKPipelineInputAssemblyState_Reset(p);
}

void
mmVKPipelineInputAssemblyState_Reset(
    struct mmVKPipelineInputAssemblyState*          p)
{
    p->flags = 0;
    p->topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    p->primitiveRestartEnable = VK_FALSE;
}

void
mmVKPipelineTessellationState_Init(
    struct mmVKPipelineTessellationState*          p)
{
    mmVKPipelineTessellationState_Reset(p);
}

void
mmVKPipelineTessellationState_Destroy(
    struct mmVKPipelineTessellationState*          p)
{
    mmVKPipelineTessellationState_Reset(p);
}

void
mmVKPipelineTessellationState_Reset(
    struct mmVKPipelineTessellationState*          p)
{
    p->flags = 0;
    p->patchControlPoints = 0;
}

void
mmVKPipelineRasterizationState_Init(
    struct mmVKPipelineRasterizationState*         p)
{
    mmVKPipelineRasterizationState_Reset(p);
}

void
mmVKPipelineRasterizationState_Destroy(
    struct mmVKPipelineRasterizationState*         p)
{
    mmVKPipelineRasterizationState_Reset(p);
}

void
mmVKPipelineRasterizationState_Reset(
    struct mmVKPipelineRasterizationState*         p)
{
    p->flags = 0;
    p->depthClampEnable = VK_FALSE;
    p->rasterizerDiscardEnable = VK_FALSE;
    p->polygonMode = VK_POLYGON_MODE_FILL;
    p->cullMode = VK_CULL_MODE_BACK_BIT;
    p->frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    p->depthBiasEnable = VK_FALSE;
    p->depthBiasConstantFactor = 0.0f;
    p->depthBiasClamp = 0.0f;
    p->depthBiasSlopeFactor = 0.0f;
    p->lineWidth = 1.0f;
}

void
mmVKPipelineColorBlendAttachmentState_Reset(
    struct mmVKPipelineColorBlendAttachmentState*     p)
{
    p->blendEnable = VK_TRUE;
    p->srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    p->dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    p->colorBlendOp = VK_BLEND_OP_ADD;
    p->srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    p->dstAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    p->alphaBlendOp = VK_BLEND_OP_ADD;
    p->colorWriteMask = 0x0000000F;
}

void
mmVKPipelineColorBlendAttachmentStates_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKPipelineColorBlendAttachmentState_Reset;
    hValueAllocator.Recycle = &mmVKPipelineColorBlendAttachmentState_Reset;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKPipelineColorBlendAttachmentState));
}

void
mmVKPipelineColorBlendAttachmentStates_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKPipelineColorBlendState_Init(
    struct mmVKPipelineColorBlendState*            p)
{
    p->flags = 0;
    p->logicOpEnable = VK_FALSE;
    p->logicOp = VK_LOGIC_OP_CLEAR;
    mmVKPipelineColorBlendAttachmentStates_Init(&p->attachments);
    mmMemset(p->blendConstants, 0, sizeof(p->blendConstants));
}

void
mmVKPipelineColorBlendState_Destroy(
    struct mmVKPipelineColorBlendState*            p)
{
    p->flags = 0;
    p->logicOpEnable = VK_FALSE;
    p->logicOp = VK_LOGIC_OP_CLEAR;
    mmVKPipelineColorBlendAttachmentStates_Destroy(&p->attachments);
    mmMemset(p->blendConstants, 0, sizeof(p->blendConstants));
}

void
mmVKPipelineColorBlendState_MakeDefaultAttachments(
    struct mmVKPipelineColorBlendState*            p)
{
    struct mmVKPipelineColorBlendAttachmentState* attachment = NULL;
    mmVectorValue_AllocatorMemory(&p->attachments, 1);
    attachment = (struct mmVKPipelineColorBlendAttachmentState*)mmVectorValue_At(&p->attachments, 0);
    attachment->blendEnable = VK_TRUE;
    attachment->srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    attachment->dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    attachment->colorBlendOp = VK_BLEND_OP_ADD;
    attachment->srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    attachment->dstAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    attachment->alphaBlendOp = VK_BLEND_OP_ADD;
    attachment->colorWriteMask = 0x0000000F;
}

void
mmVKStencilOpState_Reset(
    struct mmVKStencilOpState*                     p)
{
    p->failOp = VK_STENCIL_OP_KEEP;
    p->passOp = VK_STENCIL_OP_KEEP;
    p->depthFailOp = VK_STENCIL_OP_KEEP;
    p->compareOp = VK_COMPARE_OP_ALWAYS;
    p->compareMask = 0;
    p->writeMask = 0;
    p->reference = 0;
}

void
mmVKPipelineDepthStencilState_Init(
    struct mmVKPipelineDepthStencilState*          p)
{
    mmVKPipelineDepthStencilState_Reset(p);
}

void
mmVKPipelineDepthStencilState_Destroy(
    struct mmVKPipelineDepthStencilState*          p)
{
    mmVKPipelineDepthStencilState_Reset(p);
}

void
mmVKPipelineDepthStencilState_Reset(
    struct mmVKPipelineDepthStencilState*          p)
{
    p->flags = 0;
    p->depthTestEnable = VK_TRUE;
    p->depthWriteEnable = VK_TRUE;
    p->depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    p->depthBoundsTestEnable = VK_FALSE;
    p->stencilTestEnable = VK_FALSE;
    mmVKStencilOpState_Reset(&p->front);
    mmVKStencilOpState_Reset(&p->back);
    p->minDepthBounds = 0.0f;
    p->maxDepthBounds = 0.0f;
}

void
mmVKPipelineMultisampleState_Init(
    struct mmVKPipelineMultisampleState*           p)
{
    p->flags = 0;
    p->rasterizationSamples = 0x00000000;
    p->sampleShadingEnable = VK_FALSE;
    p->minSampleShading = 0.0f;
    mmVectorValue_Init(&p->sampleMask);
    p->alphaToCoverageEnable = VK_FALSE;
    p->alphaToOneEnable = VK_FALSE;

    mmVectorValue_SetElement(&p->sampleMask, sizeof(VkSampleMask));
}

void
mmVKPipelineMultisampleState_Destroy(
    struct mmVKPipelineMultisampleState*           p)
{
    p->flags = 0;
    p->rasterizationSamples = 0x00000000;
    p->sampleShadingEnable = VK_FALSE;
    p->minSampleShading = 0.0f;
    mmVectorValue_Destroy(&p->sampleMask);
    p->alphaToCoverageEnable = VK_FALSE;
    p->alphaToOneEnable = VK_FALSE;
}

void
mmVKPipelineArgs_Init(
    struct mmVKPipelineArgs*                       p)
{
    mmVKPipelineVertexInputInfo_Init(&p->pvii);
    mmVKPushConstantRanges_Init(&p->dslp);
    mmVKPipelineShaderStageInfos_Init(&p->pssi);
    mmVKLayoutNames_Init(&p->layouts);
    mmString_Init(&p->passName);
    p->samples = 0x00000000;

    mmVKPipelineInputAssemblyState_Init(&p->inputAssembly);
    mmVKPipelineTessellationState_Init(&p->tessellation);
    mmVKPipelineRasterizationState_Init(&p->rasterization);
    mmVKPipelineColorBlendState_Init(&p->colorBlend);
    mmVKPipelineDepthStencilState_Init(&p->depthStencil);
    mmVKPipelineMultisampleState_Init(&p->multisample);
}

void
mmVKPipelineArgs_Destroy(
    struct mmVKPipelineArgs*                       p)
{
    mmVKPipelineMultisampleState_Destroy(&p->multisample);
    mmVKPipelineDepthStencilState_Destroy(&p->depthStencil);
    mmVKPipelineColorBlendState_Destroy(&p->colorBlend);
    mmVKPipelineRasterizationState_Destroy(&p->rasterization);
    mmVKPipelineTessellationState_Destroy(&p->tessellation);
    mmVKPipelineInputAssemblyState_Destroy(&p->inputAssembly);

    p->samples = 0x00000000;
    mmString_Destroy(&p->passName);
    mmVKLayoutNames_Destroy(&p->layouts);
    mmVKPipelineShaderStageInfos_Destroy(&p->pssi);
    mmVKPushConstantRanges_Destroy(&p->dslp);
    mmVKPipelineVertexInputInfo_Destroy(&p->pvii);
}
