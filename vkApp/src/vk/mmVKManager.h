/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKManager_h__
#define __mmVKManager_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKSurface.h"
#include "vk/mmVKSwapchain.h"
#include "vk/mmVKRenderer.h"
#include "vk/mmVKAssets.h"
#include "vk/mmVKPipelineTarget.h"

#include "core/mmPrefix.h"

struct mmContextMaster;
struct mmSurfaceMaster;

struct mmEventMetrics;

struct mmVKCommandBuffer;

struct mmVKManager
{
    struct mmVKRenderer vRenderer;
    struct mmVKSurface vSurface;
    struct mmVKSwapchain vSwapchain;
    struct mmVKAssets vAssets;
    struct mmVKPipelineTarget vPipelineTarget;

    struct mmContextMaster* pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster;
};

void
mmVKManager_Init(
    struct mmVKManager*                            p);

void
mmVKManager_Destroy(
    struct mmVKManager*                            p);

void
mmVKManager_SetContextMaster(
    struct mmVKManager*                            p,
    struct mmContextMaster*                        pContextMaster);

void
mmVKManager_SetSurfaceMaster(
    struct mmVKManager* p,
    struct mmSurfaceMaster*                        pSurfaceMaster);

void
mmVKManager_Prepare(
    struct mmVKManager*                            p);

void
mmVKManager_Discard(
    struct mmVKManager*                            p);

void
mmVKManager_OnSurfacePrepare(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content);

void
mmVKManager_OnSurfaceDiscard(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content);

void
mmVKManager_OnSurfaceChanged(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content);

void
mmVKManager_DeviceWaitIdle(
    struct mmVKManager*                            p);

void
mmVKManager_AcquireNextFrame(
    struct mmVKManager*                            p,
    struct mmVKCommandBuffer*                      pCommandBuffer,
    uint32_t                                       hWindowRect[4]);

VkResult
mmVKManager_BeginCommandBuffer(
    struct mmVKManager*                            p);

VkResult
mmVKManager_BeginRenderPass(
    struct mmVKManager*                            p,
    uint32_t                                       hWindowRect[4],
    float                                          hClearColor[4]);

VkResult
mmVKManager_EndRenderPass(
    struct mmVKManager*                            p);

VkResult
mmVKManager_EndCommandBuffer(
    struct mmVKManager*                            p);

VkResult
mmVKManager_QueueSubmit(
    struct mmVKManager*                            p);

VkResult
mmVKManager_QueuePresent(
    struct mmVKManager*                            p);

#include "core/mmSuffix.h"

#endif//__mmVKManager_h__
