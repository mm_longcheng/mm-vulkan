/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTFModel.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKImageLoader.h"
#include "vk/mmVKTFPrimitiveHelper.h"
#include "vk/mmVKScene.h"
#include "vk/mmVKAssets.h"
#include "vk/mmVKImageUploader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmVector2.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

#include "dish/mmFileContext.h"

#include "parse/mmParseJSONHelper.h"

VkSamplerAddressMode
mmVKTFGetSamplerAddressMode(
    int32_t                                        wrapMode)
{
    switch (wrapMode)
    {
    case mmSamplerAddressModeRepeat:
        return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    case mmSamplerAddressModeClampToEdge:
        return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    case mmSamplerAddressModeMirroredRepeat:
        return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
    default:
        return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    }
}

VkFilter
mmVKTFGetFilterMode(
    int32_t                                        filterMode)
{
    switch (filterMode)
    {
    case mmSamplerFilterModeNearest:
        return VK_FILTER_NEAREST;
    case mmSamplerFilterModeLinear:
        return VK_FILTER_LINEAR;
    case mmSamplerFilterModeNearestMipmapNearest:
        return VK_FILTER_NEAREST;
    case mmSamplerFilterModeLinearMipmapNearest:
        return VK_FILTER_NEAREST;
    case mmSamplerFilterModeNearestMipmapLinear:
        return VK_FILTER_LINEAR;
    case mmSamplerFilterModeLinearMipmapLinear:
        return VK_FILTER_LINEAR;
    default:
        return VK_FILTER_NEAREST;
    }
}

VkSamplerMipmapMode
mmVKTFGetSamplerMipmapMode(
    VkFilter                                       magFilter,
    VkFilter                                       minFilter)
{
    if (VK_FILTER_NEAREST == magFilter && 
        VK_FILTER_NEAREST == minFilter)
    {
        return VK_SAMPLER_MIPMAP_MODE_NEAREST;
    }
    else
    {
        return VK_SAMPLER_MIPMAP_MODE_LINEAR;
    }
}

VkFormat
mmVKTFGetAccessorAttributeFormat(
    const struct mmGLTFAccessor*                   pGLTFAccessor)
{
    switch (pGLTFAccessor->type)
    {
    case mmGLTFTypeScalar:
        switch (pGLTFAccessor->componentType)
        {
        case mmGLTFComponentTypeByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8_SNORM : VK_FORMAT_R8_SINT;
            break;
        case mmGLTFComponentTypeUnsignedByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8_UNORM : VK_FORMAT_R8_UINT;
            break;
        case mmGLTFComponentTypeShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16_SNORM : VK_FORMAT_R16_SINT;
            break;
        case mmGLTFComponentTypeUnsignedShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16_UNORM : VK_FORMAT_R16_UINT;
            break;
        case mmGLTFComponentTypeInt:
            return VK_FORMAT_R32_SINT;
            break;
        case mmGLTFComponentTypeUnsignedInt:
            return VK_FORMAT_R32_UINT;
            break;
        case mmGLTFComponentTypeFloat:
            return VK_FORMAT_R32_SFLOAT;
            break;
        case mmGLTFComponentTypeDouble:
            return VK_FORMAT_R64_SFLOAT;
            break;
        case mmGLTFComponentTypeInvalid:
        default:
            return VK_FORMAT_UNDEFINED;
            break;
        }
        break;
    case mmGLTFTypeVec2:
    case mmGLTFTypeMat2:
        switch (pGLTFAccessor->componentType)
        {
        case mmGLTFComponentTypeByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8_SNORM : VK_FORMAT_R8G8_SINT;
            break;
        case mmGLTFComponentTypeUnsignedByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8_UNORM : VK_FORMAT_R8G8_UINT;
            break;
        case mmGLTFComponentTypeShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16_SNORM : VK_FORMAT_R16G16_SINT;
            break;
        case mmGLTFComponentTypeUnsignedShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16_UNORM : VK_FORMAT_R16G16_UINT;
            break;
        case mmGLTFComponentTypeInt:
            return VK_FORMAT_R32G32_SINT;
            break;
        case mmGLTFComponentTypeUnsignedInt:
            return VK_FORMAT_R32G32_UINT;
            break;
        case mmGLTFComponentTypeFloat:
            return VK_FORMAT_R32G32_SFLOAT;
            break;
        case mmGLTFComponentTypeDouble:
            return VK_FORMAT_R64G64_SFLOAT;
            break;
        case mmGLTFComponentTypeInvalid:
        default:
            return VK_FORMAT_UNDEFINED;
            break;
        }
        break;
    case mmGLTFTypeVec3:
    case mmGLTFTypeMat3:
        switch (pGLTFAccessor->componentType)
        {
        case mmGLTFComponentTypeByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8B8_SNORM : VK_FORMAT_R8G8B8_SINT;
            break;
        case mmGLTFComponentTypeUnsignedByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8B8_UNORM : VK_FORMAT_R8G8B8_UINT;
            break;
        case mmGLTFComponentTypeShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16B16_SNORM : VK_FORMAT_R16G16B16_SINT;
            break;
        case mmGLTFComponentTypeUnsignedShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16B16_UNORM : VK_FORMAT_R16G16B16_UINT;
            break;
        case mmGLTFComponentTypeInt:
            return VK_FORMAT_R32G32B32_SINT;
            break;
        case mmGLTFComponentTypeUnsignedInt:
            return VK_FORMAT_R32G32B32_UINT;
            break;
        case mmGLTFComponentTypeFloat:
            return VK_FORMAT_R32G32B32_SFLOAT;
            break;
        case mmGLTFComponentTypeDouble:
            return VK_FORMAT_R64G64B64_SFLOAT;
            break;
        case mmGLTFComponentTypeInvalid:
        default:
            return VK_FORMAT_UNDEFINED;
            break;
        }
        break;
    case mmGLTFTypeVec4:
    case mmGLTFTypeMat4:
        switch (pGLTFAccessor->componentType)
        {
        case mmGLTFComponentTypeByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8B8A8_SNORM : VK_FORMAT_R8G8B8A8_SINT;
            break;
        case mmGLTFComponentTypeUnsignedByte:
            return pGLTFAccessor->normalized ? VK_FORMAT_R8G8B8A8_UNORM : VK_FORMAT_R8G8B8A8_UINT;
            break;
        case mmGLTFComponentTypeShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16B16A16_SNORM : VK_FORMAT_R16G16B16A16_SINT;
            break;
        case mmGLTFComponentTypeUnsignedShort:
            return pGLTFAccessor->normalized ? VK_FORMAT_R16G16B16A16_UNORM : VK_FORMAT_R16G16B16A16_UINT;
            break;
        case mmGLTFComponentTypeInt:
            return VK_FORMAT_R32G32B32A32_SINT;
            break;
        case mmGLTFComponentTypeUnsignedInt:
            return VK_FORMAT_R32G32B32A32_UINT;
            break;
        case mmGLTFComponentTypeFloat:
            return VK_FORMAT_R32G32B32A32_SFLOAT;
            break;
        case mmGLTFComponentTypeDouble:
            return VK_FORMAT_R64G64B64A64_SFLOAT;
            break;
        case mmGLTFComponentTypeInvalid:
        default:
            return VK_FORMAT_UNDEFINED;
            break;
        }
        break;
    case mmGLTFTypeInvalid:
    default:
        return VK_FORMAT_UNDEFINED;
        break;
    }
}

uint32_t
mmVKTFGetAccessorAttributeSize(
    const struct mmGLTFAccessor*                   pGLTFAccessor)
{
    return (uint32_t)mmGLTFCalculationSize(pGLTFAccessor->type, pGLTFAccessor->componentType);
}

uint32_t
mmVKTFGetFormatAttributeSize(
    VkFormat                                       format)
{
    switch (format)
    {
    case VK_FORMAT_R8_SNORM:
    case VK_FORMAT_R8_SINT:
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8_UINT:
        return 1;
    case VK_FORMAT_R16_SNORM:
    case VK_FORMAT_R16_SINT:
    case VK_FORMAT_R16_UNORM:
    case VK_FORMAT_R16_UINT:
        return 2;
    case VK_FORMAT_R32_SINT:
    case VK_FORMAT_R32_UINT:
    case VK_FORMAT_R32_SFLOAT:
        return 4;
    case VK_FORMAT_R64_SFLOAT:
        return 8;

    case VK_FORMAT_R8G8_SNORM:
    case VK_FORMAT_R8G8_SINT:
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R8G8_UINT:
        return 2;
    case VK_FORMAT_R16G16_SNORM:
    case VK_FORMAT_R16G16_SINT:
    case VK_FORMAT_R16G16_UNORM:
    case VK_FORMAT_R16G16_UINT:
        return 4;
    case VK_FORMAT_R32G32_SINT:
    case VK_FORMAT_R32G32_UINT:
    case VK_FORMAT_R32G32_SFLOAT:
        return 8;
    case VK_FORMAT_R64G64_SFLOAT:
        return 16;

    case VK_FORMAT_R8G8B8_SNORM:
    case VK_FORMAT_R8G8B8_SINT:
    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_R8G8B8_UINT:
        return 3;
    case VK_FORMAT_R16G16B16_SNORM:
    case VK_FORMAT_R16G16B16_SINT:
    case VK_FORMAT_R16G16B16_UNORM:
    case VK_FORMAT_R16G16B16_UINT:
        return 6;
    case VK_FORMAT_R32G32B32_SINT:
    case VK_FORMAT_R32G32B32_UINT:
    case VK_FORMAT_R32G32B32_SFLOAT:
        return 12;
    case VK_FORMAT_R64G64B64_SFLOAT:
        return 24;

    case VK_FORMAT_R8G8B8A8_SNORM:
    case VK_FORMAT_R8G8B8A8_SINT:
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_UINT:
        return 4;
    case VK_FORMAT_R16G16B16A16_SNORM:
    case VK_FORMAT_R16G16B16A16_SINT:
    case VK_FORMAT_R16G16B16A16_UNORM:
    case VK_FORMAT_R16G16B16A16_UINT:
        return 8;
    case VK_FORMAT_R32G32B32A32_SINT:
    case VK_FORMAT_R32G32B32A32_UINT:
    case VK_FORMAT_R32G32B32A32_SFLOAT:
        return 16;
    case VK_FORMAT_R64G64B64A64_SFLOAT:
        return 32;

    default:
        return 1;
    }
}

VkPrimitiveTopology
mmVKTFGetPrimitiveTopology(
    int                                            mode)
{
    switch (mode)
    {
    case mmGLTFPrimitiveTypePoints:
        return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    case mmGLTFPrimitiveTypeLines:
        return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    case mmGLTFPrimitiveTypeLineLoop:
        return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    case mmGLTFPrimitiveTypeLineStrip:
        return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
    case mmGLTFPrimitiveTypeTriangles:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    case mmGLTFPrimitiveTypeTriangleStrip:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    case mmGLTFPrimitiveTypeTriangleFan:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
    default:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }
}

void
mmVKTFMakeSamplerCreateInfo(
    VkSamplerCreateInfo*                           info,
    const struct mmGLTFSampler*                    pGLTFSampler)
{
    info->magFilter = mmVKTFGetFilterMode(pGLTFSampler->magFilter);
    info->minFilter = mmVKTFGetFilterMode(pGLTFSampler->minFilter);
    info->mipmapMode = mmVKTFGetSamplerMipmapMode(info->magFilter, info->minFilter);
    info->addressModeU = mmVKTFGetSamplerAddressMode(pGLTFSampler->wrapS);
    info->addressModeV = mmVKTFGetSamplerAddressMode(pGLTFSampler->wrapT);
    info->addressModeW = mmVKTFGetSamplerAddressMode(pGLTFSampler->wrapT);
}

VkIndexType
mmVKTFGetIndexType(
    enum mmGLTFComponent_t                         componentType)
{
    switch (componentType)
    {
    case mmGLTFComponentTypeUnsignedByte:
    case mmGLTFComponentTypeByte:
        // VK_EXT_index_type_uint8 
        return VK_INDEX_TYPE_UINT8_EXT;
    case mmGLTFComponentTypeUnsignedShort:
    case mmGLTFComponentTypeShort:
        return VK_INDEX_TYPE_UINT16;
    case mmGLTFComponentTypeInt:
    case mmGLTFComponentTypeUnsignedInt:
        return VK_INDEX_TYPE_UINT32;
    case mmGLTFComponentTypeFloat:
    case mmGLTFComponentTypeDouble:
    default:
        return VK_INDEX_TYPE_UINT16;
    }
}

const struct mmVKTFAssetsName mmVKAssetsNameDefault = 
{
    "mmVKTFMaterial",
    "mmVKTFMaterial",
    "spiv/gltf",
    "depth",
    "whole",
    "mmVKRenderPassMaster",
};

void
mmVKTFAccessor_Init(
    struct mmVKTFAccessor*                         p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAccessor));
}

void
mmVKTFAccessor_Destroy(
    struct mmVKTFAccessor*                         p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAccessor));
}

const mmUInt8_t*
mmVKTFAccessor_GetData(
    const struct mmVKTFAccessor*                   p)
{
    assert(NULL != p->bytes.buffer && 0 != p->bytes.length && "bytesI is invalid");
    return p->bytes.buffer + p->bytes.offset;
}

size_t
mmVKTFAccessor_GetCount(
    const struct mmVKTFAccessor*                   p)
{
    if (NULL != p->accessor)
    {
        return p->accessor->count;
    }
    else
    {
        return 0;
    }
}

VkResult
mmVKTFAccessor_IsValid(
    const struct mmVKTFAccessor*                   p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        if (NULL == p->bufferView)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->accessor)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 == p->accessor->count)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->bytes.buffer || 0 == p->bytes.length)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = VK_SUCCESS;

    } while (0);

    return err;
}

VkResult
mmVKTFAccessor_Prepare(
    struct mmVKTFAccessor*                         p,
    struct mmGLTFRoot*                             pGLTFRoot,
    int                                            index)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        const struct mmVectorValue* accessors = &pGLTFRoot->accessors;

        mmByteBuffer_Reset(&p->bytes);

        if (index < 0)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->accessor = (struct mmGLTFAccessor*)mmVectorValue_At(accessors, index);
        if (NULL == p->accessor)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->bufferView = mmGLTFRoot_AccessorBytes(pGLTFRoot, p->accessor, &p->bytes);
        if (NULL == p->bufferView)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->bytes.buffer || 0 == p->bytes.length)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->bytes.offset += p->accessor->byteOffset;

        err = VK_SUCCESS;

    } while (0);

    return err;
}

void
mmVKTFAccessor_Discard(
    struct mmVKTFAccessor*                         p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAccessor));
}

void
mmVKTFImage_Init(
    struct mmVKTFImage*                            p)
{
    mmString_Init(&p->name);
    mmVKImage_Init(&p->vImage);
    p->imageView = VK_NULL_HANDLE;
}

void
mmVKTFImage_Destroy(
    struct mmVKTFImage*                            p)
{
    p->imageView = VK_NULL_HANDLE;
    mmVKImage_Destroy(&p->vImage);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFImage_Prepare(
    struct mmVKTFImage*                            p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFImage*                            pGLTFImage,
    ktx_transcode_fmt_e                            transcodefmt)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmString hMimeType;
    struct mmString hFileName;

    mmString_Init(&hMimeType);
    mmString_Init(&hFileName);

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        VkImageViewCreateInfo hImageViewInfo;

        struct mmGLTFRoot* pGLTFRoot = &pModel->root;
        struct mmParseJSON* parse = &pModel->parse;
        struct mmGLTFUriData* pGLTFUriData = NULL;
        struct mmGLTFBuffer* pGLTFBuffer = NULL;
        struct mmGLTFBufferView* pGLTFBufferView = NULL;
        struct mmVectorValue* buffers = &pGLTFRoot->buffers;
        struct mmVectorValue* bufferViews = &pGLTFRoot->bufferViews;

        const uint8_t* bytes = NULL;
        size_t size = 0;

        ktx_error_code_e ktxErr;

        const char* mimeType = "";
        const char* fileName = "";

        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pGLTFImage)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pGLTFImage is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmParseJSON_GetRangeString(parse, &pGLTFImage->name, &p->name);

        if (0 <= pGLTFImage->bufferView)
        {
            pGLTFBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, pGLTFImage->bufferView);
            if (pGLTFBufferView->buffer < 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " buffer is out of range.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, pGLTFBufferView->buffer);

            if (NULL == pGLTFBuffer)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " pGLTFBuffer is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            pGLTFUriData = &pGLTFBuffer->data;

            bytes = (const uint8_t*)(pGLTFUriData->bytes.buffer + pGLTFBufferView->byteOffset);
            size = (size_t)pGLTFBufferView->byteLength;
        }
        else
        {
            pGLTFUriData = &pGLTFImage->data;

            bytes = (const uint8_t*)(pGLTFUriData->bytes.buffer);
            size = (size_t)pGLTFUriData->bytes.length;
        }

        if (NULL == bytes || 0 == size)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes and size is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }

        mmParseJSON_GetRangeString(parse, &pGLTFImage->mimeType, &hMimeType);

        if (mmString_Empty(&hMimeType))
        {
            mmParseJSON_GetRangeString(parse, &pGLTFImage->uri, &hFileName);

            if (mmString_Empty(&hFileName))
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " hFileName is empty.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            fileName = mmString_CStr(&hFileName);

            ktxErr = mmVKImageUploader_UploadFromNamedMemory(
                fileName,
                bytes,
                size,
                0,
                MM_TRUE,
                transcodefmt,
                pUploader,
                &p->vImage);

            if (KTX_SUCCESS != ktxErr)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKImageUploader_UploadFromNamedMemory failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            err = VK_SUCCESS;
        }
        else
        {
            mimeType = mmString_CStr(&hMimeType);

            ktxErr = mmVKImageUploader_UploadFromMimeTypeMemory(
                mimeType,
                bytes,
                size,
                0,
                MM_TRUE,
                transcodefmt,
                pUploader,
                &p->vImage);

            if (KTX_SUCCESS != ktxErr)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKImageUploader_UploadFromMimeTypeMemory failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            err = VK_SUCCESS;
        }

        pAllocator = pUploader->pAllocator;

        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = p->vImage.image;
        hImageViewInfo.viewType = p->vImage.viewType;
        hImageViewInfo.format = p->vImage.format;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = p->vImage.levelCount;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = p->vImage.layerCount;

        err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &p->imageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateImageView failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    mmString_Destroy(&hFileName);
    mmString_Destroy(&hMimeType);

    return err;
}

void
mmVKTFImage_Discard(
    struct mmVKTFImage*                            p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        if (VK_NULL_HANDLE != p->imageView)
        {
            vkDestroyImageView(device, p->imageView, pAllocator);
            p->imageView = VK_NULL_HANDLE;
        }

        mmVKUploader_DeleteImage(pUploader, &p->vImage);
        mmString_Reset(&p->name);
    } while (0);
}

void
mmVKTFImages_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFImage_Init;
    hValueAllocator.Recycle = &mmVKTFImage_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFImage));
}

void
mmVKTFImages_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFImages_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    ktx_transcode_fmt_e                            transcodefmt)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmGLTFImage* pGLTFImage = NULL;
        struct mmVKTFImage* pVKTFImage = NULL;

        struct mmVectorValue* images0 = p;
        struct mmVectorValue* images1 = &pModel->root.images;

        size_t i = 0;

        VkDevice device = VK_NULL_HANDLE;

        assert(p->element == sizeof(struct mmVKTFImage) && "invalid type");

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 == images1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(images0, images1->size);

        for (i = 0; i < images1->size; ++i)
        {
            pGLTFImage = (struct mmGLTFImage*)mmVectorValue_At(images1, i);
            pVKTFImage = (struct mmVKTFImage*)mmVectorValue_At(images0, i);

            err = mmVKTFImage_Prepare(pVKTFImage, pUploader, pModel, pGLTFImage, transcodefmt);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFImage_PrepareImage failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }
        }

    } while (0);

    return err;
}

void
mmVKTFImages_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        struct mmVectorValue* images0 = p;
        size_t i = 0;

        struct mmVKTFImage* pVKTFImage = NULL;

        assert(p->element == sizeof(struct mmVKTFImage) && "invalid type");

        for (i = 0; i < images0->size; ++i)
        {
            pVKTFImage = (struct mmVKTFImage*)mmVectorValue_At(images0, i);

            mmVKTFImage_Discard(pVKTFImage, pUploader);
        }

        mmVectorValue_Reset(images0);

    } while (0);
}

void
mmVKTFSampler_Init(
    struct mmVKTFSampler*                          p)
{
    mmString_Init(&p->name);
    p->sampler = VK_NULL_HANDLE;
}

void
mmVKTFSampler_Destroy(
    struct mmVKTFSampler*                          p)
{
    p->sampler = VK_NULL_HANDLE;
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFSampler_Prepare(
    struct mmVKTFSampler*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFSampler*                          pGLTFSampler)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmParseJSON* parse = &pModel->parse;

        const VkAllocationCallbacks* pAllocator = NULL;

        VkDevice device = VK_NULL_HANDLE;

        VkSamplerCreateInfo hSamplerInfo;

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pUploader->pAllocator;

        hSamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        hSamplerInfo.pNext = NULL;
        hSamplerInfo.flags = 0;
        hSamplerInfo.magFilter = VK_FILTER_NEAREST;
        hSamplerInfo.minFilter = VK_FILTER_NEAREST;
        hSamplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        hSamplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.mipLodBias = 0.0f;
        hSamplerInfo.anisotropyEnable = VK_FALSE;
        hSamplerInfo.maxAnisotropy = 1;
        hSamplerInfo.compareEnable = VK_FALSE;
        hSamplerInfo.compareOp = VK_COMPARE_OP_NEVER;
        hSamplerInfo.minLod = 0.0f;
        hSamplerInfo.maxLod = 0.0f;
        hSamplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
        hSamplerInfo.unnormalizedCoordinates = VK_FALSE;

        mmParseJSON_GetRangeString(parse, &pGLTFSampler->name, &p->name);

        mmVKTFMakeSamplerCreateInfo(&hSamplerInfo, pGLTFSampler);

        /* create sampler */
        err = vkCreateSampler(device, &hSamplerInfo, pAllocator, &p->sampler);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipeline_PrepareSemaphore failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKTFSampler_Discard(
    struct mmVKTFSampler*                          p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        const VkAllocationCallbacks* pAllocator = NULL;

        VkDevice device = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        if (VK_NULL_HANDLE != p->sampler)
        {
            vkDestroySampler(device, p->sampler, pAllocator);
            p->sampler = VK_NULL_HANDLE;
        }

        mmString_Reset(&p->name);

    } while (0);
}

void
mmVKTFSamplers_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFSampler_Init;
    hValueAllocator.Recycle = &mmVKTFSampler_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFSampler));
}

void
mmVKTFSamplers_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFSamplers_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmGLTFSampler* pGLTFSampler = NULL;
        struct mmVKTFSampler* pVKTFSampler = NULL;

        struct mmVectorValue* samplers0 = p;
        struct mmVectorValue* samplers1 = &pModel->root.samplers;
        size_t i = 0;

        VkDevice device = VK_NULL_HANDLE;

        assert(p->element == sizeof(struct mmVKTFSampler) && "invalid type");

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 == samplers1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(samplers0, samplers1->size);

        for (i = 0; i < samplers1->size; ++i)
        {
            pVKTFSampler = (struct mmVKTFSampler*)mmVectorValue_At(samplers0, i);
            pGLTFSampler = (struct mmGLTFSampler*)mmVectorValue_At(samplers1, i);

            err = mmVKTFSampler_Prepare(pVKTFSampler, pUploader, pModel, pGLTFSampler);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFSampler_PrepareSampler failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

    } while (0);

    return err;
}

void
mmVKTFSamplers_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmVectorValue* samplers0 = p;
        size_t i = 0;

        VkDevice device = VK_NULL_HANDLE;

        struct mmVKTFSampler* pVKTFSampler = NULL;

        assert(p->element == sizeof(struct mmVKTFSampler) && "invalid type");

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        for (i = 0; i < samplers0->size; ++i)
        {
            pVKTFSampler = (struct mmVKTFSampler*)mmVectorValue_At(samplers0, i);
            mmVKTFSampler_Discard(pVKTFSampler, pUploader);
        }

        mmVectorValue_Reset(samplers0);

    } while (0);
}

void
mmVKUBOTextureInfo_Reset(
    struct mmVKUBOTextureInfo*                     p)
{
    p->index = -1;
    p->texCoord = 0;
}

void
mmVKUBOTextureInfo_CopyFrom(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFTextureInfo*                q)
{
    p->index = q->index;
    p->texCoord = q->texCoord;
}

void
mmVKUBOTextureInfo_CopyFromNormal(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFNormalTextureInfo*          q)
{
    p->index = q->index;
    p->texCoord = q->texCoord;
}

void
mmVKUBOTextureInfo_CopyFromOcclusion(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFOcclusionTextureInfo*       q)
{
    p->index = q->index;
    p->texCoord = q->texCoord;
}

static const VkPushConstantRange dsl_pushs0[] =
{
    {
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(struct mmVKPushConstantsDepth),
    },
};

static const VkPushConstantRange dsl_pushs1[] =
{
    {
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(struct mmVKPushConstantsWhole),
    },
};

const struct mmVectorValue mmVKPushDepth_DSLP = mmVectorValue_Make(dsl_pushs0, VkPushConstantRange);
const struct mmVectorValue mmVKPushWhole_DSLP = mmVectorValue_Make(dsl_pushs1, VkPushConstantRange);

static const VkDescriptorSetLayoutBinding dslb_bindings1[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 2,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 3,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 4,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        .binding = 5,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
};

const struct mmVectorValue mmVKMaterial_DSLB1 = mmVectorValue_Make(dslb_bindings1, VkDescriptorSetLayoutBinding);

static const VkDescriptorSetLayoutBinding dslb_bindings2[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = NULL,
    },
};

const struct mmVectorValue mmVKJoints_DSLB2 = mmVectorValue_Make(dslb_bindings2, VkDescriptorSetLayoutBinding);

const char* mmVKJointsPoolName = "mmVKJoints";

void
mmVKUBOMaterial_CopyFrom(
    struct mmVKUBOMaterial*                        p,
    const struct mmGLTFMaterial*                   q)
{
    mmVKUBOTextureInfo_CopyFrom(&p->baseColorTexture, &q->pbrMetallicRoughness.baseColorTexture);
    mmVKUBOTextureInfo_CopyFrom(&p->metallicRoughnessTexture, &q->pbrMetallicRoughness.metallicRoughnessTexture);
    mmVKUBOTextureInfo_CopyFromNormal(&p->normalTexture, &q->normalTexture);
    mmVKUBOTextureInfo_CopyFromOcclusion(&p->occlusionTexture, &q->occlusionTexture);
    mmVKUBOTextureInfo_CopyFrom(&p->emissiveTexture, &q->emissiveTexture);
    mmUniformVec4Assign(p->baseColorFactor, q->pbrMetallicRoughness.baseColorFactor);
    mmUniformVec3Assign(p->emissiveFactor, q->emissiveFactor);
    p->metallicFactor = q->pbrMetallicRoughness.metallicFactor;
    p->roughnessFactor = q->pbrMetallicRoughness.roughnessFactor;
    p->scale = q->normalTexture.scale;
    p->strength = q->occlusionTexture.strength;
    p->alphaCutoff = q->alphaCutoff;
    p->alphaMode = q->alphaMode;
    p->doubleSided = q->doubleSided;
}

void
mmVKUBOMaterial_MakeDefault(
    struct mmVKUBOMaterial*                        p)
{
    mmVKUBOTextureInfo_Reset(&p->baseColorTexture);
    mmVKUBOTextureInfo_Reset(&p->metallicRoughnessTexture);
    mmVKUBOTextureInfo_Reset(&p->normalTexture);
    mmVKUBOTextureInfo_Reset(&p->occlusionTexture);
    mmVKUBOTextureInfo_Reset(&p->emissiveTexture);
    mmUniformVec4Assign(p->baseColorFactor, mmVec4Unit);
    mmUniformVec3Assign(p->emissiveFactor, mmVec3Zero);
    p->metallicFactor = 1.0f;
    p->roughnessFactor = 1.0f;
    p->scale = 1.0f;
    p->strength = 1.0f;
    p->alphaCutoff = 0.5f;
    p->alphaMode = mmGLTFAlphaModeOpaque;
    p->doubleSided = MM_FALSE;
}

void
mmVKTFMaterial_Init(
    struct mmVKTFMaterial*                         p)
{
    mmString_Init(&p->name);
    mmMemset(&p->vUBOMaterial, 0, sizeof(struct mmVKUBOMaterial));
    mmVKBuffer_Init(&p->vBufferUBOMaterial);
    p->vIndex[mmVKTFTextureBaseColor] = -1;
    p->vIndex[mmVKTFTextureMetallicRoughness] = -1;
    p->vIndex[mmVKTFTextureNormal] = -1;
    p->vIndex[mmVKTFTextureOcclusion] = -1;
    p->vIndex[mmVKTFTextureEmissive] = -1;
    mmVKDescriptorSet_Init(&p->vDescriptorSet);
    p->pPoolInfo = NULL;
}

void
mmVKTFMaterial_Destroy(
    struct mmVKTFMaterial*                         p)
{
    p->pPoolInfo = NULL;
    mmVKDescriptorSet_Destroy(&p->vDescriptorSet);
    p->vIndex[mmVKTFTextureBaseColor] = -1;
    p->vIndex[mmVKTFTextureMetallicRoughness] = -1;
    p->vIndex[mmVKTFTextureNormal] = -1;
    p->vIndex[mmVKTFTextureOcclusion] = -1;
    p->vIndex[mmVKTFTextureEmissive] = -1;
    mmVKBuffer_Destroy(&p->vBufferUBOMaterial);
    mmMemset(&p->vUBOMaterial, 0, sizeof(struct mmVKUBOMaterial));
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFMaterial_PrepareUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkBufferCreateInfo hBufferInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = sizeof(struct mmVKUBOMaterial);
        hBufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateGPUOnlyBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->vBufferUBOMaterial);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKTFMaterial_DiscardUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteBuffer(pUploader, &p->vBufferUBOMaterial);
}

VkResult
mmVKTFMaterial_UploadeUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            pUploader,
            (uint8_t*)&p->vUBOMaterial,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOMaterial),
            &p->vBufferUBOMaterial,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKTFMaterial_DescriptorSetProduce(
    struct mmVKTFMaterial*                         p,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDescriptorPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDescriptorPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(pDescriptorPool, &p->vDescriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while (0);

    return err;
}

void
mmVKTFMaterial_DescriptorSetRecycle(
    struct mmVKTFMaterial*                         p,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    mmVKDescriptorPool_DescriptorSetRecycle(pDescriptorPool, &p->vDescriptorSet);
}

VkResult
mmVKTFMaterial_DescriptorSetUpdate(
    struct mmVKTFMaterial*                         p,
    VkDevice                                       device,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKAssetsDefault*                      pAssetsDefault)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDescriptorBufferInfo hDescriptorBufferInfo[1];

        VkDescriptorImageInfo hDescriptorImageInfo[mmVKTFTextureMax];
        VkWriteDescriptorSet writes[1 + mmVKTFTextureMax];
        VkWriteDescriptorSet* write = NULL;
        
        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        uint32_t n = 0;
        uint32_t i = 0;

        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        descriptorSet = p->vDescriptorSet.descriptorSet;

        for (i = 0; i < mmVKTFTextureMax; i++)
        {
            if (-1 != p->vIndex[i])
            {
                err = mmVKTFModel_MakeDescriptorImageInfo(
                    pVKTFModel,
                    pAssetsDefault,
                    &hDescriptorImageInfo[i],
                    p->vIndex[i]);
                if (VK_SUCCESS != err)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " mmVKMakeDescriptorImageInfo failure.", __FUNCTION__, __LINE__);
                    break;
                }

                write = &writes[1 + n];

                write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                write->pNext = NULL;
                write->dstSet = descriptorSet;
                write->dstBinding = 1 + n;
                write->dstArrayElement = 0;
                write->descriptorCount = 1;
                write->descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                write->pImageInfo = &hDescriptorImageInfo[i];
                write->pBufferInfo = NULL;
                write->pTexelBufferView = NULL;

                n += 1;
            }
            else
            {
                err = VK_SUCCESS;
            }
        }

        if (VK_SUCCESS != err)
        {
            // Make Texture write failure.
            mmLogger_LogE(gLogger, "%s %d"
                " Make Texture write failure.", __FUNCTION__, __LINE__);
            break;
        }

        // descriptorSet1
        hDescriptorBufferInfo[0].buffer = p->vBufferUBOMaterial.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmVKUBOMaterial);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 1 + n, writes, 0, NULL);
        
        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmVKTFMaterial_Prepare(
    struct mmVKTFMaterial*                         p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFMaterial*                         pGLTFMaterial,
    struct mmVKTFModel*                            pVKTFModel,
    const char*                                    pool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        char l[2] = { 0 };
        char hPoolName[64] = { 0 };
        struct mmVKDescriptorPool* pDescriptorPool = NULL;

        struct mmVKUploader* pUploader = NULL;
        struct mmVKAssetsDefault* pAssetsDefault = NULL;
        struct mmVKPoolLoader* pPoolLoader = NULL;

        struct mmParseJSON* parse = &pModel->parse;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(55 > strlen(pool) && "pool name out of range.");

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssetsDefault = &pAssets->vAssetsDefault;
        pPoolLoader = &pAssets->vPoolLoader;

        mmParseJSON_GetRangeString(parse, &pGLTFMaterial->name, &p->name);
        mmVKUBOMaterial_CopyFrom(&p->vUBOMaterial, pGLTFMaterial);

        p->vIndex[mmVKTFTextureBaseColor] = p->vUBOMaterial.baseColorTexture.index;
        p->vIndex[mmVKTFTextureMetallicRoughness] = p->vUBOMaterial.metallicRoughnessTexture.index;
        p->vIndex[mmVKTFTextureNormal] = p->vUBOMaterial.normalTexture.index;
        p->vIndex[mmVKTFTextureOcclusion] = p->vUBOMaterial.occlusionTexture.index;
        p->vIndex[mmVKTFTextureEmissive] = p->vUBOMaterial.emissiveTexture.index;

        mmVKTFMaterial_MakeLayoutHashId(p, l);
        sprintf(hPoolName, "%s-%s", pool, l);

        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, hPoolName);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);
        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;

        err = mmVKTFMaterial_PrepareUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_PrepareUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKTFMaterial_DescriptorSetProduce(p, pDescriptorPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKTFMaterial_UploadeUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_UploadeUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKTFMaterial_DescriptorSetUpdate(
            p,
            pUploader->device,
            pVKTFModel,
            pAssetsDefault);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_DescriptorSetUpdate failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFMaterial_Discard(
    struct mmVKTFMaterial*                         p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        struct mmVKDescriptorPool* pDescriptorPool = NULL;

        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pPoolInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;
        mmVKPoolInfo_Decrease(p->pPoolInfo);
        p->pPoolInfo = NULL;

        mmVKTFMaterial_DescriptorSetRecycle(p, pDescriptorPool);
        mmVKTFMaterial_DiscardUBOBuffer(p, pUploader);

        p->vIndex[mmVKTFTextureBaseColor] = -1;
        p->vIndex[mmVKTFTextureMetallicRoughness] = -1;
        p->vIndex[mmVKTFTextureNormal] = -1;
        p->vIndex[mmVKTFTextureOcclusion] = -1;
        p->vIndex[mmVKTFTextureEmissive] = -1;

        mmMemset(&p->vUBOMaterial, 0, sizeof(struct mmVKUBOMaterial));
        mmString_Reset(&p->name);
    } while (0);
}

VkResult
mmVKTFMaterial_PrepareDefault(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader,
    struct mmVKPoolLoader*                         pPoolLoader,
    const char*                                    pool,
    const char*                                    name)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        char l[2] = { 0 };
        char hPoolName[64] = { 0 };
        struct mmVKDescriptorPool* pDescriptorPool = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(55 > strlen(pool) && "pool name out of range.");

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmString_Assigns(&p->name, name);
        mmVKUBOMaterial_MakeDefault(&p->vUBOMaterial);

        p->vIndex[mmVKTFTextureBaseColor] = p->vUBOMaterial.baseColorTexture.index;
        p->vIndex[mmVKTFTextureMetallicRoughness] = p->vUBOMaterial.metallicRoughnessTexture.index;
        p->vIndex[mmVKTFTextureNormal] = p->vUBOMaterial.normalTexture.index;
        p->vIndex[mmVKTFTextureOcclusion] = p->vUBOMaterial.occlusionTexture.index;
        p->vIndex[mmVKTFTextureEmissive] = p->vUBOMaterial.emissiveTexture.index;

        mmVKTFMaterial_MakeLayoutHashId(p, l);
        sprintf(hPoolName, "%s-%s", pool, l);

        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, hPoolName);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);
        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;

        err = mmVKTFMaterial_PrepareUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_PrepareUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMaterial_DescriptorSetProduce(p, pDescriptorPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMaterial_UploadeUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_UploadeUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMaterial_DescriptorSetUpdate(
            p,
            pUploader->device,
            NULL,
            NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterial_DescriptorSetUpdate failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFMaterial_DiscardDefault(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        struct mmVKDescriptorPool* pDescriptorPool = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pPoolInfo)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;
        mmVKPoolInfo_Decrease(p->pPoolInfo);
        p->pPoolInfo = NULL;

        mmVKTFMaterial_DescriptorSetRecycle(p, pDescriptorPool);
        mmVKTFMaterial_DiscardUBOBuffer(p, pUploader);

        p->vIndex[mmVKTFTextureBaseColor] = -1;
        p->vIndex[mmVKTFTextureMetallicRoughness] = -1;
        p->vIndex[mmVKTFTextureNormal] = -1;
        p->vIndex[mmVKTFTextureOcclusion] = -1;
        p->vIndex[mmVKTFTextureEmissive] = -1;

        mmMemset(&p->vUBOMaterial, 0, sizeof(struct mmVKUBOMaterial));
        mmString_Reset(&p->name);
    } while (0);
}

void
mmVKTFMaterial_MakeLayoutHashId(
    const struct mmVKTFMaterial*                   p,
    char                                           l[2])
{
    int n = 0;

    n += (-1 != p->vIndex[mmVKTFTextureBaseColor]);
    n += (-1 != p->vIndex[mmVKTFTextureMetallicRoughness]);
    n += (-1 != p->vIndex[mmVKTFTextureNormal]);
    n += (-1 != p->vIndex[mmVKTFTextureOcclusion]);
    n += (-1 != p->vIndex[mmVKTFTextureEmissive]);

    // layout          hash: n
    sprintf(l, "%d", n);
}

void
mmVKTFMaterials_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFMaterial_Init;
    hValueAllocator.Recycle = &mmVKTFMaterial_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFMaterial));
}

void
mmVKTFMaterials_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFMaterials_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmVKTFModel*                            pVKTFModel,
    const char*                                    pool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFMaterial* pVKTFMaterial = NULL;
        struct mmGLTFMaterial* pGLTFMaterial = NULL;

        struct mmVectorValue* materials0 = p;
        struct mmVectorValue* materials1 = &pModel->root.materials;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFMaterial) && "invalid type");

        if (0 == materials1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(p, materials1->size);

        for (i = 0; i < materials1->size; ++i)
        {
            pVKTFMaterial = (struct mmVKTFMaterial*)mmVectorValue_At(materials0, i);
            pGLTFMaterial = (struct mmGLTFMaterial*)mmVectorValue_At(materials1, i);

            err = mmVKTFMaterial_Prepare(
                pVKTFMaterial,
                pAssets,
                pModel,
                pGLTFMaterial,
                pVKTFModel,
                pool);

            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFMaterial_Prepare failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKTFMaterials_Discard(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets)
{
    size_t i = 0;
    struct mmVKTFMaterial* pVKTFMaterial = NULL;

    struct mmVectorValue* materials0 = p;

    assert(p->element == sizeof(struct mmVKTFMaterial) && "invalid type");

    for (i = 0; i < materials0->size; ++i)
    {
        pVKTFMaterial = (struct mmVKTFMaterial*)mmVectorValue_At(materials0, i);

        mmVKTFMaterial_Discard(pVKTFMaterial, pAssets);
    }

    mmVectorValue_Reset(p);
}

int
mmVKTFAttribute_GetTypeHashId(
    const struct mmVKTFAttribute*                  p)
{
    // Primitive Component Type
    //     NONE            0
    //     UNSIGNED_BYTE   1
    //     UNSIGNED_SHORT  2
    //     FLOAT           3
    //
    // POSITION   "VEC3" 5126 (FLOAT)
    //
    // NORMAL     "VEC3" 5126 (FLOAT)
    //
    // TANGENT    "VEC4" 5126 (FLOAT)
    //
    // TEXCOORD_0 "VEC2" 5126 (FLOAT)                    
    //                   5121 (UNSIGNED_BYTE)normalized  
    //                   5123 (UNSIGNED_SHORT)normalized 
    //
    // COLOR_0    "VEC3" 5126 (FLOAT)                    
    //            "VEC4" 5121 (UNSIGNED_BYTE)normalized  
    //                   5123 (UNSIGNED_SHORT)normalized 
    //
    // JOINTS_0   "VEC4" 5121 (UNSIGNED_BYTE)            
    //                   5123 (UNSIGNED_SHORT)           
    //
    // WEIGHTS_0  "VEC4" 5126 (FLOAT)                    
    //                   5121 (UNSIGNED_BYTE)normalized  
    //                   5123 (UNSIGNED_SHORT)normalized 

    if (VK_FORMAT_UNDEFINED != p->vFormat)
    {
        switch (p->vComponentType)
        {
        case mmGLTFComponentTypeUnsignedByte:
            return 1;
        case mmGLTFComponentTypeUnsignedShort:
            return 2;
        case mmGLTFComponentTypeFloat:
            return 3;
        default:
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

void
mmVKTFAttribute_UpdateByAccessor(
    struct mmVKTFAttribute*                        p,
    struct mmGLTFAccessor*                         pGLTFAccessor,
    struct mmGLTFAttribute*                        pGLTFAttribute,
    size_t                                         numBytes)
{
    // Note: 
    // gltf VkVertexInputBindingDescription stride is not always equal 
    // the element attribute size. Sometimes it is directly determined 
    // by bufferView.byteStride.
    p->vFormat = mmVKTFGetAccessorAttributeFormat(pGLTFAccessor);
    p->vSize = mmVKTFGetAccessorAttributeSize(pGLTFAccessor);
    p->vAttribute = pGLTFAttribute->type;
    p->vComponentType = pGLTFAccessor->componentType;
    p->vType = pGLTFAccessor->type;
    p->vByteStride = (uint32_t)(numBytes / pGLTFAccessor->count);
}

uint32_t
mmVKTFPrimitiveMaskId(
    uint32_t                                       hPipeline)
{
    static const mmUInt32_t gMaskId[mmVKPipelineMax] = 
    {
        mmVKTFAttributeDepthMask,
        mmVKTFAttributeWholeMask,
    };
    assert(hPipeline < MM_ARRAY_SIZE(gMaskId) && "hPipeline is out of range.");
    return gMaskId[hPipeline];
}

uint32_t
mmVKTFPrimitivePushConstantsSize(
    uint32_t                                       hPipeline)
{
    static const uint32_t gPushConstantsSize[mmVKPipelineMax] =
    {
        (uint32_t)sizeof(struct mmVKPushConstantsDepth),
        (uint32_t)sizeof(struct mmVKPushConstantsWhole),
    };
    assert(hPipeline < MM_ARRAY_SIZE(gPushConstantsSize) && "hPipeline is out of range.");
    return gPushConstantsSize[hPipeline];
}

void
mmVKTFPrimitive_Init(
    struct mmVKTFPrimitive*                        p)
{
    mmMemset(&p->vBufferIdx, 0, sizeof(struct mmVKBuffer));
    mmMemset(p->vAttribute, 0, sizeof(p->vAttribute));

    p->mode = mmGLTFPrimitiveTypeTriangles;

    p->hIdxType = VK_INDEX_TYPE_UINT16;

    p->hIdxCount = 0;
    p->hVtxCount = 0;

    p->material = NULL;

    p->pPipelineInfo[mmVKPipelineDepth] = NULL;
    p->pPipelineInfo[mmVKPipelineWhole] = NULL;
}

void
mmVKTFPrimitive_Destroy(
    struct mmVKTFPrimitive*                        p)
{
    p->pPipelineInfo[mmVKPipelineWhole] = NULL;
    p->pPipelineInfo[mmVKPipelineDepth] = NULL;

    p->material = NULL;

    p->hTypeId = 0;

    p->hVtxCount = 0;
    p->hIdxCount = 0;

    p->hIdxType = VK_INDEX_TYPE_UINT16;

    p->mode = mmGLTFPrimitiveTypeTriangles;

    mmMemset(p->vAttribute, 0, sizeof(p->vAttribute));
    mmMemset(&p->vBufferIdx, 0, sizeof(struct mmVKBuffer));
}

void
mmVKTFPrimitive_MakeTypeId(
    const struct mmVKTFPrimitive*                  p,
    uint32_t*                                      pTypeId)
{
    int i;
    uint32_t hId = 0;

    const struct mmVKTFAttribute* pAttribute;
    const struct mmVKTFAttribute* attr = p->vAttribute;

    for (i = 0; i < mmVKTFAttributeMax; i++)
    {
        pAttribute = &attr[i];
        hId |= ((VK_FORMAT_UNDEFINED != pAttribute->vFormat) << i);
    }

    (*pTypeId) = hId;
}

void
mmVKTFPrimitive_MakeHashId(
    const struct mmVKTFPrimitive*                  p,
    int                                            a[4],
    int                                            m[5],
    int                                            t[13],
    int                                            u[2])
{
    // Primitive Attribute flag
    // 
    // a0 | 0 NotNORMAL     NotTANGENT    | 1 HasNORMAL     NotTANGENT    | 2 HasNORMAL     HasTANGENT    |
    // a1 | 0 NotTEXCOORD0  NotTEXCOORD1  | 1 HasTEXCOORD0  NotTEXCOORD1  | 2 HasTEXCOORD0  HasTEXCOORD1  |
    // a2 | 0 NotCOLOR0VEC3 NotCOLOR0VEC4 | 1 HasCOLOR0VEC3 NotCOLOR0VEC4 | 2 HasCOLOR0VEC3 HasCOLOR0VEC4 |
    // a3 | 0 NotJOINTS0    NotJOINTS1    | 1 HasJOINTS0    NotJOINTS1    | 2 HasJOINTS0    HasJOINTS1    |
    //
    // Material Texture flag
    // 
    // m0 has TextureBaseColor
    // m1 has TextureMetallicRoughness
    // m2 has TextureNormal
    // m3 has TextureOcclusion
    // m4 has TextureEmissive
    //
    // Attribute Type flag
    // 
    // t0 type AttributeTEXCOORD0
    // t1 type AttributeTEXCOORD1
    // t2 type AttributeTEXCOORD2
    // t3 type AttributeTEXCOORD3
    // t4 type AttributeTEXCOORD4
    // t5 type AttributeTEXCOORD5
    // t6 type AttributeTEXCOORD6
    // t7 type AttributeTEXCOORD7
    // t8 type AttributeCOLOR0
    // t9 type AttributeJOINTS0
    // tA type AttributeJOINTS1
    // tB type AttributeWEIGHTS0
    // tC type AttributeWEIGHTS1
    //
    // u0 = Primitive mode
    // u1 = Material double sided

    int i;
    struct mmVKTFMaterial* material = p->material;
    const struct mmVKTFAttribute* attr = p->vAttribute;

    assert(NULL != material && "material is invalid.");

    a[0] =
        (VK_FORMAT_UNDEFINED != attr[mmVKTFAttributeNORMAL].vFormat) +
        (VK_FORMAT_UNDEFINED != attr[mmVKTFAttributeTANGENT].vFormat);

    a[1] = 0;
    for (i = 0; i < 8; i++)
    {
        a[1] += (VK_FORMAT_UNDEFINED != attr[mmVKTFAttributeTEXCOORD0 + i].vFormat);
    }

    a[2] =
        (mmGLTFTypeVec3 == attr[mmVKTFAttributeCOLOR0].vType) +
        (mmGLTFTypeVec4 == attr[mmVKTFAttributeCOLOR0].vType) * 2;

    a[3] =
        (VK_FORMAT_UNDEFINED != attr[mmVKTFAttributeJOINTS0].vFormat) +
        (VK_FORMAT_UNDEFINED != attr[mmVKTFAttributeJOINTS1].vFormat);

    m[0] = (-1 != material->vIndex[mmVKTFTextureBaseColor]);
    m[1] = (-1 != material->vIndex[mmVKTFTextureMetallicRoughness]);
    m[2] = (-1 != material->vIndex[mmVKTFTextureNormal]);
    m[3] = (-1 != material->vIndex[mmVKTFTextureOcclusion]);
    m[4] = (-1 != material->vIndex[mmVKTFTextureEmissive]);

    for (i = 0; i < 13; i++)
    {
        t[i] = mmVKTFAttribute_GetTypeHashId(&p->vAttribute[mmVKTFAttributeTEXCOORD0 + i]);
    }

    u[0] = (int)mmVKTFGetPrimitiveTopology(p->mode);
    u[1] = (int)material->vUBOMaterial.doubleSided;
}

void
mmVKTFPrimitive_MakeHashIdDepth(
    const struct mmVKTFPrimitive*                  p,
    char                                           v[8],
    char                                           f[16],
    char                                           x[32])
{
    // vertex   shader hash: a3a1
    // fragment shader hash: a1-m0
    // pipeline        hash: a3a2a1a0-m4m3m2m1m0-tCtBtAt9t8t7t6t5t4t3t2t1t0-u0d0

    int a[4];
    int m[5];
    int t[13];
    int u[2];

    mmVKTFPrimitive_MakeHashId(p, a, m, t, u);

    sprintf(v, "%d%d", a[3], a[1]);
    sprintf(f, "%d-%d", a[1], m[0]);
    sprintf(x, "%d%d%d%d-%d%d%d%d%d-%d%d%d%d%d%d%d%d%d%d%d%d%d-%d%d",
        a[3], a[2], a[1], a[0],
        m[4], m[3], m[2], m[1], m[0],
        t[12], t[11], t[10], t[9], t[8], t[7], t[6], t[5], t[4], t[3], t[2], t[1], t[0],
        u[0], u[1]);
}

void
mmVKTFPrimitive_MakeHashIdWhole(
    const struct mmVKTFPrimitive*                  p,
    char                                           v[8],
    char                                           f[16],
    char                                           x[32])
{
    // vertex   shader hash: a3a2a1a0
    // fragment shader hash: a2a1a0-m4m3m2m1m0
    // pipeline        hash: a3a2a1a0-m4m3m2m1m0-tCtBtAt9t8t7t6t5t4t3t2t1t0-u0d0

    int a[4];
    int m[5];
    int t[13];
    int u[2];

    mmVKTFPrimitive_MakeHashId(p, a, m, t, u);

    sprintf(v, "%d%d%d%d", a[3], a[2], a[1], a[0]);
    sprintf(f, "%d%d%d-%d%d%d%d%d", a[2], a[1], a[0], m[4], m[3], m[2], m[1], m[0]);
    sprintf(x, "%d%d%d%d-%d%d%d%d%d-%d%d%d%d%d%d%d%d%d%d%d%d%d-%d%d", 
        a[3], a[2], a[1], a[0], 
        m[4], m[3], m[2], m[1], m[0], 
        t[12], t[11], t[10], t[9], t[8], t[7], t[6], t[5], t[4], t[3], t[2], t[1], t[0],
        u[0], u[0]);
}

void
mmVKTFPrimitive_MakeBuffers(
    const struct mmVKTFPrimitive*                  p,
    uint32_t                                       hMaskId,
    VkBuffer                                       buffers[mmVKTFAttributeMax],
    uint32_t*                                      pBindingCount)
{
    int n;
    int i;

    const struct mmVKTFAttribute* pAttribute;

    n = 0;

    for (i = 0; i < mmVKTFAttributeMax; i++)
    {
        pAttribute = &p->vAttribute[i];
        if ((0 != (hMaskId & (1 << i))) && (VK_FORMAT_UNDEFINED != pAttribute->vFormat))
        {
            buffers[n++] = pAttribute->vBuffer.buffer;
        }
    }

    (*pBindingCount) = n;
}

void
mmVKTFPrimitive_MakePVII(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKPipelineVertexInputInfo*            pvii,
    uint32_t                                       hMaskId)
{
    int n;
    int i;

    struct mmVectorValue* vibd = &pvii->vibd;
    struct mmVectorValue* viad = &pvii->viad;

    const struct mmVKTFAttribute* pAttribute;

    VkVertexInputBindingDescription* vibdi = NULL;
    VkVertexInputAttributeDescription* viadi = NULL;

    n = 0;

    for (i = 0; i < mmVKTFAttributeMax; i++)
    {
        pAttribute = &p->vAttribute[i];
        if (0 != (hMaskId & (1 << i)))
        {
            n += (VK_FORMAT_UNDEFINED != pAttribute->vFormat);
        }
    }

    mmVectorValue_AllocatorMemory(vibd, n);
    mmVectorValue_AllocatorMemory(viad, n);

    n = 0;

    for (i = 0; i < mmVKTFAttributeMax; i++)
    {
        pAttribute = &p->vAttribute[i];
        if ((0 != (hMaskId & (1 << i))) && (VK_FORMAT_UNDEFINED != pAttribute->vFormat))
        {
            vibdi = (VkVertexInputBindingDescription*)mmVectorValue_At(vibd, n);
            viadi = (VkVertexInputAttributeDescription*)mmVectorValue_At(viad, n);

            vibdi->binding = n;
            vibdi->stride = pAttribute->vByteStride;
            vibdi->inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

            viadi->location = n;
            viadi->binding = n;
            viadi->format = pAttribute->vFormat;
            viadi->offset = 0;

            n++;
        }
    }
}

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipeline(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    struct mmVKTFPrimitivePipelineInfo*            pInfo)
{
    struct mmVKPipelineInfo* pPipelineInfo = NULL;

    do
    {
        struct mmVKPipelineVertexInputInfo pvii;

        struct mmVKPipelineInputAssemblyState   inputAssembly;
        struct mmVKPipelineTessellationState    tessellation;
        struct mmVKPipelineRasterizationState   rasterization;
        struct mmVKPipelineColorBlendState      colorBlend;
        struct mmVKPipelineDepthStencilState    depthStencil;
        struct mmVKPipelineMultisampleState     multisample;

        struct mmVKPipelineCreateInfo hPipelineInfo;

        struct mmVKPipelineCache* pPipelineCache = &pAssets->vPipelineCache;
        struct mmVKLayoutLoader* pLayoutLoader = &pAssets->vLayoutLoader;
        struct mmVKPassLoader* pPassLoader = &pAssets->vPassLoader;
        struct mmVKShaderLoader* pShaderLoader = &pAssets->vShaderLoader;
        struct mmVKPipelineLoader* pPipelineLoader = &pAssets->vPipelineLoader;

        const char* spvs[2] = { 0 };
        struct mmVKLayoutInfo* layouts[3] = { 0 };
        struct mmVectorVpt dsli = mmVectorVpt_Make(layouts);
        struct mmVKPassInfo* pPassInfo = NULL;
        VkPipelineShaderStageCreateInfo stages[2];
        const struct mmVectorValue psmi = mmVectorValue_Make(stages, VkPipelineShaderStageCreateInfo);
        int doubleSided = p->material->vUBOMaterial.doubleSided;

        pPipelineInfo = mmVKPipelineLoader_GetFromName(pPipelineLoader, pInfo->xName);
        if (!mmVKPipelineInfo_Invalid(pPipelineInfo))
        {
            // pPipelineInfo by xName is already exists.
            break;
        }

        mmVKPipelineVertexInputInfo_Init(&pvii);

        mmVKPipelineInputAssemblyState_Init(&inputAssembly);
        mmVKPipelineTessellationState_Init(&tessellation);
        mmVKPipelineRasterizationState_Init(&rasterization);
        mmVKPipelineColorBlendState_Init(&colorBlend);
        mmVKPipelineDepthStencilState_Init(&depthStencil);
        mmVKPipelineMultisampleState_Init(&multisample);

        mmVKTFPrimitive_MakePVII(p, &pvii, pInfo->hMaskId);

        layouts[0] = mmVKLayoutLoader_GetFromName(pLayoutLoader, pInfo->scene);
        layouts[1] = mmVKLayoutLoader_GetFromName(pLayoutLoader, pInfo->lName);
        layouts[2] = mmVKLayoutLoader_GetFromName(pLayoutLoader, pInfo->joints);
        dsli.size = (0 != (p->hTypeId & mmVKTFAttributeSkinsMask)) ? 3 : 2;

        spvs[0] = pInfo->vName;
        spvs[1] = pInfo->fName;
        mmVKShaderLoaderMakeClassicalStageInfo(pShaderLoader, spvs, stages);

        pPassInfo = mmVKPassLoader_GetFromName(pPassLoader, pInfo->pass);

        inputAssembly.topology = (mmUInt32_t)mmVKTFGetPrimitiveTopology(p->mode);
        rasterization.cullMode = doubleSided ? VK_CULL_MODE_NONE : VK_CULL_MODE_BACK_BIT;
        mmVKPipelineColorBlendState_MakeDefaultAttachments(&colorBlend);
        multisample.rasterizationSamples = pInfo->samples;

        hPipelineInfo.pvii = &pvii;
        hPipelineInfo.dslp = pInfo->dslp;
        hPipelineInfo.psmi = &psmi;
        hPipelineInfo.dsli = &dsli;
        hPipelineInfo.inputAssembly = &inputAssembly;
        hPipelineInfo.tessellation = &tessellation;
        hPipelineInfo.rasterization = &rasterization;
        hPipelineInfo.colorBlend = &colorBlend;
        hPipelineInfo.depthStencil = &depthStencil;
        hPipelineInfo.multisample = &multisample;
        hPipelineInfo.pPassInfo = pPassInfo;
        hPipelineInfo.pipelineCache = pPipelineCache->pipelineCache;
        hPipelineInfo.samples = pAssets->vPipelineLoader.samples;

        pPipelineInfo = mmVKPipelineLoader_LoadFromInfo(pPipelineLoader, pInfo->xName, &hPipelineInfo);

        if (mmVKPipelineInfo_Invalid(pPipelineInfo))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "    Pipeline   lName: %s", pInfo->lName);
            mmLogger_LogE(gLogger, "    Pipeline   vName: %s", pInfo->vName);
            mmLogger_LogE(gLogger, "    Pipeline   fName: %s", pInfo->fName);
            mmLogger_LogE(gLogger, "    Pipeline   xName: %s", pInfo->xName);
            mmLogger_LogE(gLogger, "    Pipeline   scene: %s", pInfo->scene);
            mmLogger_LogE(gLogger, "    Pipeline  joints: %s", pInfo->joints);
            mmLogger_LogE(gLogger, "    Pipeline    pass: %s", pInfo->pass);
            mmLogger_LogE(gLogger, "    Pipeline hMaskId: %u", pInfo->hMaskId);
            mmLogger_LogE(gLogger, "    Pipeline samples: %u", pInfo->samples);
        }
        else
        {
            // Cache model pipeline info reference.
            mmVKTFModelLoader_AddPipelineInfo(&pAssets->vModelLoader, pPipelineInfo);
        }

        mmVKPipelineMultisampleState_Destroy(&multisample);
        mmVKPipelineDepthStencilState_Destroy(&depthStencil);
        mmVKPipelineColorBlendState_Destroy(&colorBlend);
        mmVKPipelineRasterizationState_Destroy(&rasterization);
        mmVKPipelineTessellationState_Destroy(&tessellation);
        mmVKPipelineInputAssemblyState_Destroy(&inputAssembly);

        mmVKPipelineVertexInputInfo_Destroy(&pvii);
    } while (0);

    return pPipelineInfo;
}

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipelineDepth(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    char l[2] = { 0 };
    char v[8] = { 0 };
    char f[16] = { 0 };
    char x[32] = { 0 };

    struct mmVKTFPrimitivePipelineInfo hInfo;

    mmVKTFPrimitive_MakeHashIdDepth(p, v, f, x);
    mmVKTFMaterial_MakeLayoutHashId(p->material, l);

    sprintf(hInfo.lName, "%s-%s", pAssetsName->layout, l);
    sprintf(hInfo.vName, "%s/%s-%s.vert.spv", pAssetsName->spiv, pAssetsName->depth, v);
    sprintf(hInfo.fName, "%s/%s-%s.frag.spv", pAssetsName->spiv, pAssetsName->depth, f);
    sprintf(hInfo.xName, "%s-%s", pAssetsName->depth, x);

    hInfo.scene = mmVKSceneDepthPoolName;
    hInfo.joints = mmVKJointsPoolName;
    hInfo.pass = mmVKRenderPassDepthName;
    hInfo.hMaskId = mmVKTFAttributeDepthMask;
    hInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    hInfo.dslp = &mmVKPushDepth_DSLP;

    return mmVKTFPrimitive_MakePipeline(p, pAssets, &hInfo);
}

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipelineWhole(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    char l[2] = { 0 };
    char v[8] = { 0 };
    char f[16] = { 0 };
    char x[32] = { 0 };

    struct mmVKTFPrimitivePipelineInfo hInfo;

    mmVKTFPrimitive_MakeHashIdWhole(p, v, f, x);
    mmVKTFMaterial_MakeLayoutHashId(p->material, l);

    sprintf(hInfo.lName, "%s-%s", pAssetsName->layout, l);
    sprintf(hInfo.vName, "%s/%s-%s.vert.spv", pAssetsName->spiv, pAssetsName->whole, v);
    sprintf(hInfo.fName, "%s/%s-%s.frag.spv", pAssetsName->spiv, pAssetsName->whole, f);
    sprintf(hInfo.xName, "%s-%s", pAssetsName->whole, x);

    hInfo.scene = mmVKSceneWholePoolName;
    hInfo.joints = mmVKJointsPoolName;
    hInfo.pass = pAssetsName->pass;
    hInfo.hMaskId = mmVKTFAttributeWholeMask;
    hInfo.samples = 0x00000000;
    hInfo.dslp = &mmVKPushWhole_DSLP;

    return mmVKTFPrimitive_MakePipeline(p, pAssets, &hInfo);
}

VkResult
mmVKTFPrimitive_UploadAttribute(
    struct mmVKTFPrimitive*                        pVKTFPrimitive,
    struct mmVKUploader*                           uploader,
    struct mmGLTFAccessor*                         pGLTFAccessor,
    struct mmGLTFAttribute*                        pGLTFAttribute,
    size_t                                         numBytes,
    const uint8_t*                                 ptrBytes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmVKTFAttribute* pAttribute = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();

    switch (pGLTFAttribute->type)
    {
    case mmGLTFAttributeTypePosition:
    {
        const int idx = mmVKTFAttributePOSITION;
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        pVKTFPrimitive->hVtxCount = (uint32_t)pGLTFAccessor->count;

        // POSITION "VEC3"  5126 (FLOAT)
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer POSITION failure.", __FUNCTION__, __LINE__);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeNormal:
    {
        const int idx = mmVKTFAttributeNORMAL;
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        // NORMAL   "VEC3"  5126 (FLOAT)
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer NORMAL failure.", __FUNCTION__, __LINE__);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeTangent:
    {
        const int idx = mmVKTFAttributeTANGENT;
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        // TANGENT  "VEC4"  5126 (FLOAT)
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer TANGENT failure.", __FUNCTION__, __LINE__);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeTexcoord:
    {
        const int idx = mmVKTFAttributeTEXCOORD0 + pGLTFAttribute->index;
        assert(8 > pGLTFAttribute->index && "mmVKTFAttributeTEXCOORD < 8 invalid.");
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        // TEXCOORD_0   "VEC2"  5126 (FLOAT)
        //                      5121 (UNSIGNED_BYTE)normalized
        //                      5123 (UNSIGNED_SHORT)normalized
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer TEXCOORD_%d failure.", __FUNCTION__, __LINE__, pGLTFAttribute->index);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeColor:
    {
        const int idx = mmVKTFAttributeCOLOR0 + pGLTFAttribute->index;
        assert(1 > pGLTFAttribute->index && "mmVKTFAttributeCOLOR < 1 invalid.");
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        // COLOR_0  "VEC3"  5126 (FLOAT)
        //          "VEC4"  5121 (UNSIGNED_BYTE)normalized
        //                  5123 (UNSIGNED_SHORT)normalized
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer COLOR_%d failure.", __FUNCTION__, __LINE__, pGLTFAttribute->index);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeJoints:
    {
        const int idx = mmVKTFAttributeJOINTS0 + pGLTFAttribute->index;
        assert(2 > pGLTFAttribute->index && "mmVKTFAttributeJOINTS < 2 invalid.");
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute,
            pGLTFAccessor,
            pGLTFAttribute,
            numBytes);

        // JOINTS_0 "VEC4"  5121 (UNSIGNED_BYTE)
        //                  5123 (UNSIGNED_SHORT)
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer JOINTS_%d failure.", __FUNCTION__, __LINE__, pGLTFAttribute->index);
            break;
        }
    }
    break;

    case mmGLTFAttributeTypeWeights:
    {
        const int idx = mmVKTFAttributeWEIGHTS0 + pGLTFAttribute->index;
        assert(2 > pGLTFAttribute->index && "mmVKTFAttributeWEIGHTS < 2 invalid.");
        pAttribute = &pVKTFPrimitive->vAttribute[idx];

        mmVKTFAttribute_UpdateByAccessor(
            pAttribute, 
            pGLTFAccessor, 
            pGLTFAttribute, 
            numBytes);

        // WEIGHTS_0    "VEC4"  5126 (FLOAT)
        //                      5121 (UNSIGNED_BYTE)normalized
        //                      5123 (UNSIGNED_SHORT)normalized
        err = mmVKUploader_UploadGPUOnlyBuffer(
            uploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            (uint8_t*)(ptrBytes),
            numBytes,
            &pAttribute->vBuffer);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer Weights_%d failure.", __FUNCTION__, __LINE__, pGLTFAttribute->index);
            break;
        }
    }
    break;
    {
        mmLogger_LogE(gLogger, "%s %d"
            " pGLTFAttribute->type is invalid.", __FUNCTION__, __LINE__);
        err = VK_ERROR_FORMAT_NOT_SUPPORTED;
        break;
    }
    default:
        break;
    }

    return err;
}

VkResult
mmVKTFPrimitive_PreparePipeline(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        p->pPipelineInfo[mmVKPipelineDepth] = mmVKTFPrimitive_MakePipelineDepth(p, pAssets, pAssetsName);
        if (mmVKPipelineInfo_Invalid(p->pPipelineInfo[mmVKPipelineDepth]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPipelineInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPipelineInfo[mmVKPipelineDepth] = NULL;
            break;
        }
        mmVKPipelineInfo_Increase(p->pPipelineInfo[mmVKPipelineDepth]);

        p->pPipelineInfo[mmVKPipelineWhole] = mmVKTFPrimitive_MakePipelineWhole(p, pAssets, pAssetsName);
        if (mmVKPipelineInfo_Invalid(p->pPipelineInfo[mmVKPipelineWhole]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPipelineInfoWhole is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPipelineInfo[mmVKPipelineWhole] = NULL;
            break;
        }
        mmVKPipelineInfo_Increase(p->pPipelineInfo[mmVKPipelineWhole]);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFPrimitive_DiscardPipeline(
    struct mmVKTFPrimitive*                        p)
{
    mmVKPipelineInfo_Decrease(p->pPipelineInfo[mmVKPipelineWhole]);
    p->pPipelineInfo[mmVKPipelineWhole] = NULL;

    mmVKPipelineInfo_Decrease(p->pPipelineInfo[mmVKPipelineDepth]);
    p->pPipelineInfo[mmVKPipelineDepth] = NULL;
}

VkResult
mmVKTFPrimitive_Prepare(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFPrimitive*                        pGLTFPrimitive,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int material;

        struct mmVKAssetsDefault* pAssetsDefault = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAssetsDefault = &pAssets->vAssetsDefault;

        err = mmVKTFPrimitive_PrepareAttributes(p, pAssets->pUploader, pModel, pGLTFPrimitive);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFPrimitive_PrepareAttributes failure.", __FUNCTION__, __LINE__);
            break;
        }

        material = pGLTFPrimitive->material;
        if (-1 != material)
        {
            p->material = (struct mmVKTFMaterial*)mmVectorValue_At(materials, material);
        }
        else
        {
            p->material = &pAssetsDefault->hMaterialDefault;
        }
        p->mode = pGLTFPrimitive->mode;

        err = mmVKTFPrimitive_PreparePipeline(p, pAssets, pAssetsName);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFPrimitive_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKTFPrimitive_Discard(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets)
{
    mmVKTFPrimitive_DiscardPipeline(p);

    p->mode = -1;
    p->material = NULL;

    mmVKTFPrimitive_DiscardAttributes(p, pAssets->pUploader);
}

void
mmVKTFPrimitives_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFPrimitive_Init;
    hValueAllocator.Recycle = &mmVKTFPrimitive_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFPrimitive));
}

void
mmVKTFPrimitives_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKTFMesh_Init(
    struct mmVKTFMesh*                             p)
{
    mmString_Init(&p->name);
    mmVKTFPrimitives_Init(&p->primitives);
}

void
mmVKTFMesh_Destroy(
    struct mmVKTFMesh*                             p)
{
    mmVKTFPrimitives_Destroy(&p->primitives);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFMesh_Prepare(
    struct mmVKTFMesh*                             p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFMesh*                             pGLTFMesh,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmParseJSON* parse = &pModel->parse;

        size_t i = 0;
        struct mmVKTFPrimitive* pVKTFPrimitive = NULL;
        struct mmGLTFPrimitive* pGLTFPrimitive = NULL;

        struct mmVectorValue* primitives0 = &p->primitives;
        struct mmVectorValue* primitives1 = &pGLTFMesh->primitives;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmParseJSON_GetRangeString(parse, &pGLTFMesh->name, &p->name);

        mmVectorValue_AllocatorMemory(primitives0, primitives1->size);

        if (0 == primitives1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        for (i = 0; i < primitives1->size; ++i)
        {
            pVKTFPrimitive = (struct mmVKTFPrimitive*)mmVectorValue_At(primitives0, i);
            pGLTFPrimitive = (struct mmGLTFPrimitive*)mmVectorValue_At(primitives1, i);

            err = mmVKTFPrimitive_Prepare(
                pVKTFPrimitive,
                pAssets,
                pModel,
                pGLTFPrimitive,
                materials,
                pAssetsName);

            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFPrimitive_PreparePrimitive failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKTFMesh_Discard(
    struct mmVKTFMesh*                             p,
    struct mmVKAssets*                             pAssets)
{
    size_t i = 0;
    struct mmVKTFPrimitive* pVKTFPrimitive = NULL;

    struct mmVectorValue* primitives0 = &p->primitives;

    for (i = 0; i < primitives0->size; ++i)
    {
        pVKTFPrimitive = (struct mmVKTFPrimitive*)mmVectorValue_At(primitives0, i);

        mmVKTFPrimitive_Discard(pVKTFPrimitive, pAssets);
    }

    mmVectorValue_Reset(primitives0);

    mmString_Reset(&p->name);
}

void
mmVKTFMeshs_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFMesh_Init;
    hValueAllocator.Recycle = &mmVKTFMesh_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFMesh));
}

void
mmVKTFMeshs_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFMeshs_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFMesh* pVKTFMesh = NULL;
        struct mmGLTFMesh* pGLTFMesh = NULL;

        struct mmGLTFRoot* root = &pModel->root;
        struct mmVectorValue* meshes0 = p;
        struct mmVectorValue* meshes1 = &root->meshes;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFMesh) && "invalid type");

        if (0 == meshes1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(meshes0, meshes1->size);

        for (i = 0; i < meshes1->size; ++i)
        {
            pVKTFMesh = (struct mmVKTFMesh*)mmVectorValue_At(meshes0, i);
            pGLTFMesh = (struct mmGLTFMesh*)mmVectorValue_At(meshes1, i);
            err = mmVKTFMesh_Prepare(
                pVKTFMesh,
                pAssets,
                pModel,
                pGLTFMesh,
                materials,
                pAssetsName);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFMesh_PrepareMesh failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void 
mmVKTFMeshs_Discard(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets)
{
    size_t i = 0;
    struct mmVKTFMesh* pVKTFMesh = NULL;

    struct mmVectorValue* meshes0 = p;

    assert(p->element == sizeof(struct mmVKTFMesh) && "invalid type");

    for (i = 0; i < meshes0->size; ++i)
    {
        pVKTFMesh = (struct mmVKTFMesh*)mmVectorValue_At(meshes0, i);
        mmVKTFMesh_Discard(pVKTFMesh, pAssets);
    }

    mmVectorValue_Reset(meshes0);
}

void
mmVKTFSkin_Init(
    struct mmVKTFSkin*                             p)
{
    mmString_Init(&p->name);
    mmVectorU32_Init(&p->joints);
    p->skeleton = -1;
    mmVKTFAccessor_Init(&p->inverseBindMatrices);
}

void
mmVKTFSkin_Destroy(
    struct mmVKTFSkin*                             p)
{
    mmVKTFAccessor_Destroy(&p->inverseBindMatrices);
    p->skeleton = -1;
    mmVectorU32_Destroy(&p->joints);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFSkin_Prepare(
    struct mmVKTFSkin*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFSkin*                             pGLTFSkin)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmGLTFRoot* pGLTFRoot = &pModel->root;

        struct mmParseJSON* parse = &pModel->parse;

        size_t i = 0;
        int* v = NULL;
        int n = 0;
        struct mmVectorValue* joints = &pGLTFSkin->joints;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmParseJSON_GetRangeString(parse, &pGLTFSkin->name, &p->name);

        err = mmVKTFAccessor_Prepare(
            &p->inverseBindMatrices,
            pGLTFRoot,
            pGLTFSkin->inverseBindMatrices);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAccessor_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->skeleton = pGLTFSkin->skeleton;

        if (0 == joints->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorU32_Reset(&p->joints);
        mmVectorU32_AllocatorMemory(&p->joints, joints->size);
        for (i = 0; i < joints->size; ++i)
        {
            v = (int*)mmVectorValue_At(joints, i);
            n = (*v);
            mmVectorU32_SetIndex(&p->joints, i, (mmUInt32_t)n);
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFSkin_Discard(
    struct mmVKTFSkin*                             p,
    struct mmVKUploader*                           pUploader)
{
    mmVectorU32_Reset(&p->joints);

    p->skeleton = -1;

    mmVKTFAccessor_Discard(&p->inverseBindMatrices);

    mmString_Reset(&p->name);
}

void
mmVKTFSkins_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFSkin_Init;
    hValueAllocator.Recycle = &mmVKTFSkin_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFSkin));
}

void
mmVKTFSkins_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFSkins_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFSkin* pVKTFSkin = NULL;
        struct mmGLTFSkin* pGLTFSkin = NULL;

        struct mmGLTFRoot* root = &pModel->root;
        struct mmVectorValue* skins0 = p;
        struct mmVectorValue* skins1 = &root->skins;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFSkin) && "invalid type");

        if (0 == skins1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(skins0, skins1->size);

        for (i = 0; i < skins1->size; ++i)
        {
            pVKTFSkin = (struct mmVKTFSkin*)mmVectorValue_At(skins0, i);
            pGLTFSkin = (struct mmGLTFSkin*)mmVectorValue_At(skins1, i);
            err = mmVKTFSkin_Prepare(pVKTFSkin, pUploader, pModel, pGLTFSkin);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFSkin_Prepare failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKTFSkins_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader)
{
    size_t i = 0;
    struct mmVKTFSkin* pVKTFSkin = NULL;

    struct mmVectorValue* skins0 = p;

    assert(p->element == sizeof(struct mmVKTFSkin) && "invalid type");

    for (i = 0; i < skins0->size; ++i)
    {
        pVKTFSkin = (struct mmVKTFSkin*)mmVectorValue_At(skins0, i);
        mmVKTFSkin_Discard(pVKTFSkin, pUploader);
    }

    mmVectorValue_Reset(skins0);
}

void
mmVKTFModel_Init(
    struct mmVKTFModel*                            p)
{
    mmGLTFModel_Init(&p->model);

    mmVKTFImages_Init(&p->images);
    mmVKTFSamplers_Init(&p->samplers);
    mmVKTFMaterials_Init(&p->materials);
    mmVKTFMeshs_Init(&p->meshes);
    mmVKTFSkins_Init(&p->skins);

    p->code = VK_ERROR_UNKNOWN;
}

void
mmVKTFModel_Destroy(
    struct mmVKTFModel*                            p)
{
    p->code = VK_ERROR_UNKNOWN;

    mmVKTFSkins_Destroy(&p->skins);
    mmVKTFMeshs_Destroy(&p->meshes);
    mmVKTFMaterials_Destroy(&p->materials);
    mmVKTFSamplers_Destroy(&p->samplers);
    mmVKTFImages_Destroy(&p->images);

    mmGLTFModel_Destroy(&p->model);
}

int
mmVKTFModel_Invalid(
    const struct mmVKTFModel*                      p)
{
    return
        (NULL == p) ||
        (VK_SUCCESS != p->code);
}

VkResult
mmVKTFModel_Prepare(
    struct mmVKTFModel*                            p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName,
    const char*                                    path)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int code;

        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        pAssetsName = (NULL == pAssetsName) ? &mmVKAssetsNameDefault : pAssetsName;

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        code = mmGLTFModel_PrepareUriDatas(
            &p->model, 
            &pAssets->vFileIO.hSuper, 
            path);
        if (mmGLTFResultSuccess != code)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGLTFModel_PrepareUriDatas failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKTFImages_Prepare(
            &p->images, 
            pUploader, 
            &p->model, 
            pAssets->vImageLoader.transcodefmt);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFImages_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFSamplers_Prepare(
            &p->samplers, 
            pUploader, 
            &p->model);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFSamplers_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMaterials_Prepare(
            &p->materials,
            pAssets,
            &p->model,
            p,
            pAssetsName->pool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMaterials_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFMeshs_Prepare(
            &p->meshes, 
            pAssets, 
            &p->model, 
            &p->materials,
            pAssetsName);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFMeshs_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFSkins_Prepare(&p->skins, pUploader, &p->model);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFSkins_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    p->code = err;

    return err;
}

void
mmVKTFModel_Discard(
    struct mmVKTFModel*                            p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        p->code = VK_ERROR_UNKNOWN;

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKTFSkins_Discard(&p->skins, pUploader);
        mmVKTFMeshs_Discard(&p->meshes, pAssets);
        mmVKTFMaterials_Discard(&p->materials, pAssets);
        mmVKTFSamplers_Discard(&p->samplers, pUploader);
        mmVKTFImages_Discard(&p->images, pUploader);

        mmGLTFModel_DiscardUriDatas(&p->model, &pAssets->vFileIO.hSuper);
        mmGLTFModel_Reset(&p->model);
    } while (0);
}

VkResult
mmVKTFModel_MakeDescriptorImageInfo(
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKAssetsDefault*                      pAssetsDefault,
    VkDescriptorImageInfo*                         pDescriptorImageInfo,
    int                                            index)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    if (-1 == index)
    {
        pDescriptorImageInfo->sampler = pAssetsDefault->pSamplerNearestRepeat->sampler;
        pDescriptorImageInfo->imageView = pAssetsDefault->pImage2DTransparent->imageView;
        pDescriptorImageInfo->imageLayout = pAssetsDefault->pImage2DTransparent->vImage.imageLayout;
        err = VK_SUCCESS;
    }
    else
    {
        int source;
        struct mmGLTFTexture* pGLTFTexture = NULL;
        struct mmGLTFModel* pModel = &pVKTFModel->model;
        struct mmGLTFRoot* pGLTFRoot = &pModel->root;
        struct mmVectorValue* textures = &pGLTFRoot->textures;
        
        pGLTFTexture = (struct mmGLTFTexture*)mmVectorValue_At(textures, index);

        if (-1 == pGLTFTexture->sampler)
        {
            pDescriptorImageInfo->sampler = pAssetsDefault->pSamplerLinearRepeat->sampler;
        }
        else
        {
            struct mmVKTFSampler* pVKTFSampler = NULL;
            struct mmVectorValue* pVKTFSamplers = &pVKTFModel->samplers;
            pVKTFSampler = (struct mmVKTFSampler*)mmVectorValue_At(pVKTFSamplers, pGLTFTexture->sampler);
            pDescriptorImageInfo->sampler = pVKTFSampler->sampler;
        }

        source = mmGLTFTexture_GetSource(pGLTFTexture);

        if (-1 == source)
        {
            pDescriptorImageInfo->imageView = pAssetsDefault->pImage2DTransparent->imageView;
            pDescriptorImageInfo->imageLayout = pAssetsDefault->pImage2DTransparent->vImage.imageLayout;
            err = VK_SUCCESS;
        }
        else
        {
            struct mmVKTFImage* pVKTFImage = NULL;
            struct mmVectorValue* pVKTFImages = &pVKTFModel->images;
            pVKTFImage = (struct mmVKTFImage*)mmVectorValue_At(pVKTFImages, source);
            pDescriptorImageInfo->imageView = pVKTFImage->imageView;
            pDescriptorImageInfo->imageLayout = pVKTFImage->vImage.imageLayout;

            if (VK_NULL_HANDLE == pVKTFImage->vImage.image)
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "%s %d"
                    " p->vImage is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_INITIALIZATION_FAILED;
            }
            else
            {
                err = VK_SUCCESS;
            }
        }
    }

    return err;
}


void
mmVKTFPrepareDefaultAssets(
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    int i;
    char hLayoutName[64] = { 0 };
    struct mmVectorValue hMaterial_DSLB1 = mmVKMaterial_DSLB1;
    struct mmVKLayoutInfo* pLayout = NULL;
    struct mmVKLayoutCreateInfo hLayoutInfo;

    struct mmVKLayoutLoader* pLayoutLoader = &pAssets->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &pAssets->vPoolLoader;

    hLayoutInfo.dslb = &mmVKSceneDepth_DSLB0;
    hLayoutInfo.dslc = 64;
    pLayout = mmVKLayoutLoader_LoadFromInfo(pLayoutLoader, mmVKSceneDepthPoolName, &hLayoutInfo);
    mmVKPoolLoader_LoadFromInfo(pPoolLoader, mmVKSceneDepthPoolName, pLayout);

    hLayoutInfo.dslb = &mmVKSceneWhole_DSLB0;
    hLayoutInfo.dslc = 64;
    pLayout = mmVKLayoutLoader_LoadFromInfo(pLayoutLoader, mmVKSceneWholePoolName, &hLayoutInfo);
    mmVKPoolLoader_LoadFromInfo(pPoolLoader, mmVKSceneWholePoolName, pLayout);

    hLayoutInfo.dslb = &mmVKJoints_DSLB2;
    hLayoutInfo.dslc = 64;
    pLayout = mmVKLayoutLoader_LoadFromInfo(pLayoutLoader, mmVKJointsPoolName, &hLayoutInfo);
    mmVKPoolLoader_LoadFromInfo(pPoolLoader, mmVKJointsPoolName, pLayout);

    hLayoutInfo.dslb = &hMaterial_DSLB1;
    hLayoutInfo.dslc = 64;
    for (i = 0; i < 6; i++)
    {
        mmMemset(hLayoutName, 0, sizeof(hLayoutName));
        sprintf(hLayoutName, "%s-%d", pAssetsName->layout, i);
        hMaterial_DSLB1.size = 1 + i;
        pLayout = mmVKLayoutLoader_LoadFromInfo(pLayoutLoader, hLayoutName, &hLayoutInfo);
        mmVKPoolLoader_LoadFromInfo(pPoolLoader, hLayoutName, pLayout);
    }
}

void
mmVKTFDiscardDefaultAssets(
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    int i;
    char hLayoutName[64] = { 0 };

    struct mmVKLayoutLoader* pLayoutLoader = &pAssets->vLayoutLoader;
    struct mmVKPoolLoader* pPoolLoader = &pAssets->vPoolLoader;

    for (i = 0; i < 6; i++)
    {
        mmMemset(hLayoutName, 0, sizeof(hLayoutName));
        sprintf(hLayoutName, "%s-%d", pAssetsName->layout, i);
        mmVKPoolLoader_UnloadByName(pPoolLoader, hLayoutName);
        mmVKLayoutLoader_UnloadByName(pLayoutLoader, hLayoutName);
    }

    mmVKPoolLoader_UnloadByName(pPoolLoader, mmVKJointsPoolName);
    mmVKLayoutLoader_UnloadByName(pLayoutLoader, mmVKJointsPoolName);

    mmVKPoolLoader_UnloadByName(pPoolLoader, mmVKSceneDepthPoolName);
    mmVKLayoutLoader_UnloadByName(pLayoutLoader, mmVKSceneDepthPoolName);

    mmVKPoolLoader_UnloadByName(pPoolLoader, mmVKSceneWholePoolName);
    mmVKLayoutLoader_UnloadByName(pLayoutLoader, mmVKSceneWholePoolName);
}
