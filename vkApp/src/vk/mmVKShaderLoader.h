/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKShaderLoader_h__
#define __mmVKShaderLoader_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "dish/mmFileContext.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmVKUploader;

struct mmVectorVpt;
struct mmVectorValue;

VkResult
mmVKUploader_PrepareShaderModule(
    struct mmVKUploader*                           pUploader,
    const uint32_t*                                code,
    size_t                                         size,
    VkShaderModule*                                pShader);

void
mmVKUploader_DiscardShaderModule(
    struct mmVKUploader*                           pUploader,
    VkShaderModule                                 shader);

struct mmVKShaderModuleInfo
{
    // name for ShaderModule info, read only. 
    // string is weak reference value.
    struct mmString name;

    // shader module.
    VkShaderModule shaderModule;

    // reference count. 
    size_t reference;
};

void
mmVKShaderModuleInfo_Init(
    struct mmVKShaderModuleInfo*                   p);

void
mmVKShaderModuleInfo_Destroy(
    struct mmVKShaderModuleInfo*                   p);

int
mmVKShaderModuleInfo_Invalid(
    const struct mmVKShaderModuleInfo*             p);

void
mmVKShaderModuleInfo_Increase(
    struct mmVKShaderModuleInfo*                   p);

void
mmVKShaderModuleInfo_Decrease(
    struct mmVKShaderModuleInfo*                   p);

struct mmVKShaderLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKUploader* pUploader;
    struct mmRbtreeStringVpt sets;
};

void
mmVKShaderLoader_Init(
    struct mmVKShaderLoader*                       p);

void
mmVKShaderLoader_Destroy(
    struct mmVKShaderLoader*                       p);

void
mmVKShaderLoader_SetFileContext(
    struct mmVKShaderLoader*                       p,
    struct mmFileContext*                          pFileContext);

void
mmVKShaderLoader_SetUploader(
    struct mmVKShaderLoader*                       p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKShaderLoader_PrepareShaderModuleInfo(
    struct mmVKShaderLoader*                       p,
    const char*                                    path,
    struct mmVKShaderModuleInfo*                   pShaderModuleInfo);

void
mmVKShaderLoader_DiscardShaderModuleInfo(
    struct mmVKShaderLoader*                       p,
    struct mmVKShaderModuleInfo*                   pShaderModuleInfo);

struct mmVKShaderModuleInfo*
mmVKShaderLoader_LoadFromFile(
    struct mmVKShaderLoader*                       p,
    const char*                                    path);

void
mmVKShaderLoader_UnloadByFile(
    struct mmVKShaderLoader*                       p,
    const char*                                    path);

void
mmVKShaderLoader_UnloadComplete(
    struct mmVKShaderLoader*                       p);

void
mmVKShaderLoaderMakeClassicalStageInfo(
    struct mmVKShaderLoader*                       p,
    const char*                                    spvs[2],
    VkPipelineShaderStageCreateInfo                stages[2]);

void
mmVKShaderLoaderMakeStageInfo(
    struct mmVKShaderLoader*                       p,
    struct mmVectorValue*                          pssi,
    struct mmVectorValue*                          psmi);

#include "core/mmSuffix.h"

#endif//__mmVKShaderLoader_h__
