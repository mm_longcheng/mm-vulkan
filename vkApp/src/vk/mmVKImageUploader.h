/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKImageUploader_h__
#define __mmVKImageUploader_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKPlatform.h"

#include "ktx.h"

#include "core/mmPrefix.h"

struct mmVKImage;
struct mmVKUploader;

extern
int
mmVKImageR8G8B8A8WriteToPNGFile(
    const char*                                    path,
    const unsigned char*                           data,
    int                                            w,
    int                                            h);

extern
int
mmVKImageGetIsFormatSupported(
    VkPhysicalDevice                               physicalDevice,
    VkFormat                                       format);

extern
ktx_error_code_e
mmVKImageUploader_UploadFromNamedFile(
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage);

extern
ktx_error_code_e
mmVKImageUploader_UploadFromNamedMemory(
    const char*                                    filename,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage);

extern
ktx_error_code_e
mmVKImageUploader_UploadFromMimeTypeMemory(
    const char*                                    mimeType,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage);

extern
ktx_error_code_e
mmVKImageUploader_UploadFromR8G8B8A8Memory(
    const uint8_t*                                 data,
    size_t                                         size,
    uint32_t                                       width,
    uint32_t                                       height,
    int                                            flags,
    int                                            generateMipmaps,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage);

extern
void
mmVKImageUploader_Prepare(void);

extern
void
mmVKImageUploader_Discard(void);

#include "core/mmSuffix.h"

#endif//__mmVKImageUploader_h__
