/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKDescriptorLayout_h__
#define __mmVKDescriptorLayout_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

const char*
mmVKShaderStageFlags_EncodeEnumerate(
    int                                            v);

int
mmVKShaderStageFlags_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKDescriptorType_EncodeEnumerate(
    int                                            v);

int
mmVKDescriptorType_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKVertexInputRate_EncodeEnumerate(
    int                                            v);

int
mmVKVertexInputRate_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKFormat_EncodeEnumerate(
    int                                            v);

int
mmVKFormat_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKPrimitiveTopology_EncodeEnumerate(
    int                                            v);

int
mmVKPrimitiveTopology_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKPolygonMode_EncodeEnumerate(
    int                                            v);

int
mmVKPolygonMode_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKCullModeFlags_EncodeEnumerate(
    int                                            v);

int
mmVKCullModeFlags_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKFrontFace_EncodeEnumerate(
    int                                            v);

int
mmVKFrontFace_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKBlendFactor_EncodeEnumerate(
    int                                            v);

int
mmVKBlendFactor_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKBlendOp_EncodeEnumerate(
    int                                            v);

int
mmVKBlendOp_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKCompareOp_EncodeEnumerate(
    int                                            v);

int
mmVKCompareOp_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKStencilOp_EncodeEnumerate(
    int                                            v);

int
mmVKStencilOp_DecodeEnumerate(
    const char*                                    w);

const char*
mmVKLogicOp_EncodeEnumerate(
    int                                            v);

int
mmVKLogicOp_DecodeEnumerate(
    const char*                                    w);

/* Value api */
void
mmValue_StringToVKDescriptorType(
    const char*                                    str,
    VkDescriptorType*                              val);

void
mmValue_StringToVKShaderStageFlags(
    const char*                                    str,
    VkShaderStageFlags*                            val);

void
mmValue_StringToVKVertexInputRate(
    const char*                                    str,
    VkShaderStageFlags*                            val);

void
mmValue_StringToVKFormat(
    const char*                                    str,
    VkShaderStageFlags*                            val);

void
mmValue_StringToVKPrimitiveTopology(
    const char*                                    str,
    VkPrimitiveTopology*                           val);

void
mmValue_StringToVKPolygonMode(
    const char*                                    str,
    VkPolygonMode*                                 val);

void
mmValue_StringToVKCullModeFlags(
    const char*                                    str,
    VkCullModeFlags*                               val);

void
mmValue_StringToVKFrontFace(
    const char*                                    str,
    VkFrontFace*                                   val);

void
mmValue_StringToVKBlendFactor(
    const char*                                    str,
    VkBlendFactor*                                 val);

void
mmValue_StringToVKBlendOp(
    const char*                                    str,
    VkBlendOp*                                     val);

void
mmValue_StringToVKCompareOp(
    const char*                                    str,
    VkCompareOp*                                   val);

void
mmValue_StringToVKStencilOp(
    const char*                                    str,
    VkStencilOp*                                   val);

void
mmValue_StringToVKLogicOp(
    const char*                                    str,
    VkLogicOp*                                     val);

void
mmVKDescriptorSetLayoutBindings_Init(
    struct mmVectorValue*                          p);

void
mmVKDescriptorSetLayoutBindings_Destroy(
    struct mmVectorValue*                          p);

void
mmVKDescriptorSetLayoutBindings_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q);

void
mmVKPushConstantRanges_Init(
    struct mmVectorValue*                          p);

void
mmVKPushConstantRanges_Destroy(
    struct mmVectorValue*                          p);

void
mmVKPushConstantRanges_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q);

void
mmVKDescriptorPoolSizes_Init(
    struct mmVectorValue*                          p);

void
mmVKDescriptorPoolSizes_Destroy(
    struct mmVectorValue*                          p);

void
mmVKVertexInputBindingDescriptions_Init(
    struct mmVectorValue*                          p);

void
mmVKVertexInputBindingDescriptions_Destroy(
    struct mmVectorValue*                          p);

void
mmVKVertexInputAttributeDescriptions_Init(
    struct mmVectorValue*                          p);

void
mmVKVertexInputAttributeDescriptions_Destroy(
    struct mmVectorValue*                          p);

struct mmVKPipelineVertexInputInfo
{
    struct mmVectorValue vibd;
    struct mmVectorValue viad;
};

void
mmVKPipelineVertexInputInfo_Init(
    struct mmVKPipelineVertexInputInfo*            p);

void
mmVKPipelineVertexInputInfo_Destroy(
    struct mmVKPipelineVertexInputInfo*            p);

void
mmVKPipelineVertexInputInfo_CopyFrom(
    struct mmVKPipelineVertexInputInfo*            p,
    const struct mmVKPipelineVertexInputInfo*      q);

struct mmVKPipelineShaderStageInfo
{
    VkShaderStageFlagBits stage;
    struct mmString module;
    struct mmString name;
};

void
mmVKPipelineShaderStageInfo_Init(
    struct mmVKPipelineShaderStageInfo*            p);

void
mmVKPipelineShaderStageInfo_Destroy(
    struct mmVKPipelineShaderStageInfo*            p);

void
mmVKPipelineShaderStageInfo_CopyFrom(
    struct mmVKPipelineShaderStageInfo*            p,
    const struct mmVKPipelineShaderStageInfo*      q);

void
mmVKPipelineShaderStageInfos_Init(
    struct mmVectorValue*                          p);

void
mmVKPipelineShaderStageInfos_Destroy(
    struct mmVectorValue*                          p);

void
mmVKPipelineShaderStageInfos_CopyFrom(
    struct mmVectorValue*                          p,
    const struct mmVectorValue*                    q);

void
mmVKPipelineShaderStageCreateInfos_Init(
    struct mmVectorValue*                          p);

void
mmVKPipelineShaderStageCreateInfos_Destroy(
    struct mmVectorValue*                          p);

struct mmVKLayoutInfoArgs
{
    // vector<VkDescriptorSetLayoutBinding>
    struct mmVectorValue dslb;

    // descriptor set layout count.
    uint32_t dslc;
};

void
mmVKLayoutInfoArgs_Init(
    struct mmVKLayoutInfoArgs*                     p);

void
mmVKLayoutInfoArgs_Destroy(
    struct mmVKLayoutInfoArgs*                     p);

int
mmVKLayoutInfoArgs_Invalid(
    const struct mmVKLayoutInfoArgs*               p);

void
mmVKLayoutInfoArgs_CopyFrom(
    struct mmVKLayoutInfoArgs*                     p,
    const struct mmVKLayoutInfoArgs*               q);

void
mmVKLayoutNames_Init(
    struct mmVectorValue*                          p);

void
mmVKLayoutNames_Destroy(
    struct mmVectorValue*                          p);

struct mmVKPipelineInputAssemblyState
{
    // VkPipelineInputAssemblyStateCreateFlags
    int                       flags;
    // VkPrimitiveTopology
    mmUInt32_t                topology;
    // VkBool32
    int                       primitiveRestartEnable;
};

void
mmVKPipelineInputAssemblyState_Init(
    struct mmVKPipelineInputAssemblyState*         p);

void
mmVKPipelineInputAssemblyState_Destroy(
    struct mmVKPipelineInputAssemblyState*         p);

void
mmVKPipelineInputAssemblyState_Reset(
    struct mmVKPipelineInputAssemblyState*         p);

struct mmVKPipelineTessellationState
{
    // VkPipelineTessellationStateCreateFlags
    int                       flags;
    // uint32_t
    uint32_t                  patchControlPoints;
};

void
mmVKPipelineTessellationState_Init(
    struct mmVKPipelineTessellationState*          p);

void
mmVKPipelineTessellationState_Destroy(
    struct mmVKPipelineTessellationState*          p);

void
mmVKPipelineTessellationState_Reset(
    struct mmVKPipelineTessellationState*          p);

struct mmVKPipelineRasterizationState
{
    // VkPipelineRasterizationStateCreateFlags
    int                       flags;
    // VkBool32
    int                       depthClampEnable;
    // VkBool32
    int                       rasterizerDiscardEnable;
    // VkPolygonMode
    mmUInt32_t                polygonMode;
    // VkCullModeFlags
    mmUInt32_t                cullMode;
    // VkFrontFace
    mmUInt32_t                frontFace;
    // VkBool32
    int                       depthBiasEnable;
    // float
    float                     depthBiasConstantFactor;
    // float
    float                     depthBiasClamp;
    // float
    float                     depthBiasSlopeFactor;
    // float
    float                     lineWidth;
};

void
mmVKPipelineRasterizationState_Init(
    struct mmVKPipelineRasterizationState*         p);

void
mmVKPipelineRasterizationState_Destroy(
    struct mmVKPipelineRasterizationState*         p);

void
mmVKPipelineRasterizationState_Reset(
    struct mmVKPipelineRasterizationState*         p);

struct mmVKPipelineColorBlendAttachmentState
{
    // VkBool32
    int                       blendEnable;
    // VkBlendFactor
    mmUInt32_t                srcColorBlendFactor;
    // VkBlendFactor
    mmUInt32_t                dstColorBlendFactor;
    // VkBlendOp
    mmUInt32_t                colorBlendOp;
    // VkBlendFactor
    mmUInt32_t                srcAlphaBlendFactor;
    // VkBlendFactor
    mmUInt32_t                dstAlphaBlendFactor;
    // VkBlendOp
    mmUInt32_t                alphaBlendOp;
    // VkColorComponentFlags
    mmUInt32_t                colorWriteMask;
};

void
mmVKPipelineColorBlendAttachmentState_Reset(
    struct mmVKPipelineColorBlendAttachmentState*  p);

void
mmVKPipelineColorBlendAttachmentStates_Init(
    struct mmVectorValue*                          p);

void
mmVKPipelineColorBlendAttachmentStates_Destroy(
    struct mmVectorValue*                          p);

struct mmVKPipelineColorBlendState
{
    // VkPipelineColorBlendStateCreateFlags
    int                       flags;
    // VkBool32
    int                       logicOpEnable;
    // VkLogicOp
    mmUInt32_t                logicOp;
    // const VkPipelineColorBlendAttachmentState*
    struct mmVectorValue      attachments;
    // float
    float                     blendConstants[4];
};

void
mmVKPipelineColorBlendState_Init(
    struct mmVKPipelineColorBlendState*            p);

void
mmVKPipelineColorBlendState_Destroy(
    struct mmVKPipelineColorBlendState*            p);

void
mmVKPipelineColorBlendState_MakeDefaultAttachments(
    struct mmVKPipelineColorBlendState*            p);

struct mmVKStencilOpState
{
    // VkStencilOp
    mmUInt32_t                failOp;
    // VkStencilOp
    mmUInt32_t                passOp;
    // VkStencilOp
    mmUInt32_t                depthFailOp;
    // VkCompareOp
    mmUInt32_t                compareOp;
    // uint32_t
    uint32_t                  compareMask;
    // uint32_t
    uint32_t                  writeMask;
    // uint32_t
    uint32_t                  reference;
};

void
mmVKStencilOpState_Reset(
    struct mmVKStencilOpState*                     p);

struct mmVKPipelineDepthStencilState
{
    // VkPipelineDepthStencilStateCreateFlags
    int                       flags;
    // VkBool32
    int                       depthTestEnable;
    // VkBool32
    int                       depthWriteEnable;
    // VkCompareOp
    mmUInt32_t                depthCompareOp;
    // VkBool32
    int                       depthBoundsTestEnable;
    // VkBool32
    int                       stencilTestEnable;
    // VkStencilOpState
    struct mmVKStencilOpState front;
    // VkStencilOpState
    struct mmVKStencilOpState back;
    // float
    float                     minDepthBounds;
    // float
    float                     maxDepthBounds;
};

void
mmVKPipelineDepthStencilState_Init(
    struct mmVKPipelineDepthStencilState*          p);

void
mmVKPipelineDepthStencilState_Destroy(
    struct mmVKPipelineDepthStencilState*          p);

void
mmVKPipelineDepthStencilState_Reset(
    struct mmVKPipelineDepthStencilState*          p);

struct mmVKPipelineMultisampleState
{
    // VkPipelineMultisampleStateCreateFlags
    int                       flags;
    // VkSampleCountFlagBits
    // pipeline sample count flag bits. 
    // 0x00000000 will used default value.
    mmUInt32_t                rasterizationSamples;
    // VkBool32
    int                       sampleShadingEnable;
    // float
    float                     minSampleShading;
    // const VkSampleMask*
    struct mmVectorValue      sampleMask;
    // VkBool32
    int                       alphaToCoverageEnable;
    // VkBool32
    int                       alphaToOneEnable;
};

void
mmVKPipelineMultisampleState_Init(
    struct mmVKPipelineMultisampleState*           p);

void
mmVKPipelineMultisampleState_Destroy(
    struct mmVKPipelineMultisampleState*           p);

struct mmVKPipelineArgs
{
    // pipeline vertex input info.
    struct mmVKPipelineVertexInputInfo    pvii;

    // descriptor set layout pushs.
    struct mmVectorValue                  dslp;

    // pipeline shader stage info.
    struct mmVectorValue                  pssi;

    // descriptor set layout sets name.
    struct mmVectorValue                  layouts;

    // render pass name.
    struct mmString                       passName;

    // pipeline sample count flag bits. 
    // 0x00000000 will used default value.
    mmUInt32_t                            samples;

    // Pipeline InputAssembly.
    struct mmVKPipelineInputAssemblyState inputAssembly;
    // Pipeline Tessellation.
    struct mmVKPipelineTessellationState  tessellation;
    // Pipeline Rasterization.
    struct mmVKPipelineRasterizationState rasterization;
    // Pipeline ColorBlend.
    struct mmVKPipelineColorBlendState    colorBlend;
    // Pipeline DepthStencil.
    struct mmVKPipelineDepthStencilState  depthStencil;
    // Pipeline Multisample.
    struct mmVKPipelineMultisampleState   multisample;
};

void
mmVKPipelineArgs_Init(
    struct mmVKPipelineArgs*                       p);

void
mmVKPipelineArgs_Destroy(
    struct mmVKPipelineArgs*                       p);

#include "core/mmSuffix.h"

#endif//__mmVKDescriptorLayout_h__
