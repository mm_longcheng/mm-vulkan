/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDrawable.h"

#include "vk/mmVKAssets.h"
#include "vk/mmVKUploader.h"

#include "core/mmLogger.h"

void
mmVKDrawable_Init(
    struct mmVKDrawable*                           p)
{
    mmString_Init(&p->hPipelineName);
    mmString_Init(&p->hPoolName);
    
    mmVKBuffer_Init(&p->vIdxBuffer);
    mmVKBuffer_Init(&p->vVtxBuffer);
    
    mmByteBuffer_Init(&p->vConstantBytes);

    mmVKDescriptorSet_Init(&p->vDescriptorSet);

    p->pPipelineInfo = NULL;
    p->pPoolInfo = NULL;

    p->hIdxCount = 0;
    p->hVtxCount = 0;

    p->hIdxType = VK_INDEX_TYPE_UINT16;
}

void
mmVKDrawable_Destroy(
    struct mmVKDrawable*                           p)
{
    p->hIdxType = VK_INDEX_TYPE_UINT16;

    p->hVtxCount = 0;
    p->hIdxCount = 0;

    p->pPoolInfo = NULL;
    p->pPipelineInfo = NULL;

    mmVKDescriptorSet_Destroy(&p->vDescriptorSet);

    mmByteBuffer_Destroy(&p->vConstantBytes);

    mmVKBuffer_Destroy(&p->vVtxBuffer);
    mmVKBuffer_Destroy(&p->vIdxBuffer);
    
    mmString_Destroy(&p->hPoolName);
    mmString_Destroy(&p->hPipelineName);
}

VkResult
mmVKDrawable_PrepareBuffer(
    struct mmVKDrawable*                           p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDrawableCreateInfo*                 info)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        p->hIdxType = info->hIdxType;

        p->hIdxCount = info->hIdxCount;
        p->hVtxCount = info->hVtxCount;
        
        p->vConstantBytes.buffer = (mmUInt8_t*)info->pCstBuffer;
        p->vConstantBytes.offset = 0;
        p->vConstantBytes.length = info->hCstBufferSize;

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            0,
            info->pIdxBuffer,
            info->hIdxBufferSize,
            &p->vIdxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UploadGPUOnlyBuffer(
            pUploader,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            0,
            info->pVtxBuffer,
            info->hVtxBufferSize,
            &p->vVtxBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UploadGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);
    
    return err;
}

void
mmVKDrawable_DiscardBuffer(
    struct mmVKDrawable*                           p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteBuffer(pUploader, &p->vVtxBuffer);
    mmVKUploader_DeleteBuffer(pUploader, &p->vIdxBuffer);
}

VkResult
mmVKDrawable_PreparePipeline(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineLoader* pPipelineLoader = NULL;
        const char* pPipelineName = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pPipelineLoader = &pAssets->vPipelineLoader;
        pPipelineName = mmString_CStr(&p->hPipelineName);
        p->pPipelineInfo = mmVKPipelineLoader_GetFromName(pPipelineLoader, pPipelineName);
        if (mmVKPipelineInfo_Invalid(p->pPipelineInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPipelineInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPipelineInfo = NULL;
            break;
        }
        mmVKPipelineInfo_Increase(p->pPipelineInfo);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKDrawable_DiscardPipeline(
    struct mmVKDrawable*                           p)
{
    mmVKPipelineInfo_Decrease(p->pPipelineInfo);
    p->pPipelineInfo = NULL;
}

VkResult
mmVKDrawable_PreparePool(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPoolLoader* pPoolLoader = NULL;
        const char* pPoolName = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pPoolLoader = &pAssets->vPoolLoader;
        pPoolName = mmString_CStr(&p->hPoolName);
        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, pPoolName);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKDrawable_DiscardPool(
    struct mmVKDrawable*                           p)
{
    mmVKPoolInfo_Decrease(p->pPoolInfo);
    p->pPoolInfo = NULL;
}

VkResult
mmVKDrawable_DescriptorSetProduce(
    struct mmVKDrawable*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(
            &p->pPoolInfo->vDescriptorPool,
            &p->vDescriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKDrawable_DescriptorSetRecycle(
    struct mmVKDrawable*                           p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pViewContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKDescriptorPool_DescriptorSetRecycle(
            &p->pPoolInfo->vDescriptorPool,
            &p->vDescriptorSet);
    } while (0);
}

VkResult
mmVKDrawable_Prepare(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets,
    struct mmVKDrawableCreateInfo*                 info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmString_Assigns(&p->hPipelineName, info->pPipelineName);
        mmString_Assigns(&p->hPoolName, info->pPoolName);

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        err = mmVKDrawable_PrepareBuffer(p, pAssets->pUploader, info);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDrawable_PrepareBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKDrawable_PreparePipeline(p, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDrawable_PreparePipeline failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKDrawable_PreparePool(p, pAssets);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKDrawable_DescriptorSetProduce(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDrawable_PreparePool failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKDrawable_Discard(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets)
{
    if (NULL == pAssets)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d"
            " pAssets is invalid.", __FUNCTION__, __LINE__);
    }
    else
    {
        mmVKDrawable_DescriptorSetRecycle(p);
        mmVKDrawable_DiscardPool(p);
        mmVKDrawable_DiscardPipeline(p);
        mmVKDrawable_DiscardBuffer(p, pAssets->pUploader);
    }
}

void
mmVKDrawable_OnRecord(
    struct mmVKDrawable*                           p,
    VkCommandBuffer                                cmdBuffer)
{
    struct mmVKDescriptorSet* pDescriptorSet = &p->vDescriptorSet;
    struct mmVKPipelineInfo* pPipelineInfo = p->pPipelineInfo;
    struct mmVKPipeline* pPipeline = NULL;
    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkDeviceSize offset = 0;

    assert(NULL != pPipelineInfo && "pPipelineInfo is a invalid.");

    pPipeline = &pPipelineInfo->vPipeline;
    pipelineLayout = pPipeline->pipelineLayout;
    pipeline = pPipeline->pipeline;
    descriptorSet = pDescriptorSet->descriptorSet;

    assert(VK_NULL_HANDLE != pipelineLayout && "pipelineLayout is a invalid.");
    assert(VK_NULL_HANDLE != pipeline && "pipeline is a invalid.");
    assert(VK_NULL_HANDLE != descriptorSet && "descriptorSet is a invalid.");

    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
    vkCmdBindIndexBuffer(cmdBuffer, p->vIdxBuffer.buffer, 0, p->hIdxType);
    vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &p->vVtxBuffer.buffer, &offset);

    vkCmdBindDescriptorSets(
        cmdBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        NULL);

    vkCmdPushConstants(
        cmdBuffer,
        pipelineLayout,
        VK_SHADER_STAGE_VERTEX_BIT,
        0,
        (uint32_t)p->vConstantBytes.length,
        (const void*)p->vConstantBytes.buffer);

    vkCmdDrawIndexed(cmdBuffer, p->hIdxCount, 1, 0, 0, 0);
}

