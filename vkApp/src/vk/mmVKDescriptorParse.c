/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDescriptorParse.h"

#include "core/mmValueTransform.h"

#include "parse/mmCJsonHelper.h"

void
mmVKDescriptorSetLayoutBinding_JEncode(
    const VkDescriptorSetLayoutBinding*            p,
    cJSON*                                         j)
{
    mmCJson_EncodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_EncodeEnumerate(j, "descriptorType", &p->descriptorType, VK_DESCRIPTOR_TYPE_SAMPLER, 1, &mmVKDescriptorType_EncodeEnumerate);
    mmCJson_EncodeUInt32(j, "descriptorCount", &p->descriptorCount, 0, 1);
    mmCJson_EncodeEnumerate(j, "stageFlags", &p->stageFlags, 0x00000000, 1, &mmVKShaderStageFlags_EncodeEnumerate);
}

void
mmVKDescriptorSetLayoutBinding_JDecode(
    VkDescriptorSetLayoutBinding*                  p,
    const cJSON*                                   j)
{
    mmCJson_DecodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_DecodeEnumerate(j, "descriptorType", &p->descriptorType, VK_DESCRIPTOR_TYPE_SAMPLER, 1, &mmVKDescriptorType_DecodeEnumerate);
    mmCJson_DecodeUInt32(j, "descriptorCount", &p->descriptorCount, 0, 1);
    mmCJson_DecodeEnumerate(j, "stageFlags", &p->stageFlags, 0x00000000, 1, &mmVKShaderStageFlags_DecodeEnumerate);
}

void
mmVKPushConstantRange_JEncode(
    const VkPushConstantRange*                     p,
    cJSON*                                         j)
{
    mmCJson_EncodeEnumerate(j, "stageFlags", &p->stageFlags, 0x00000000, 1, &mmVKShaderStageFlags_EncodeEnumerate);
    mmCJson_EncodeUInt32(j, "offset", &p->offset, 0, 1);
    mmCJson_EncodeUInt32(j, "size", &p->size, 0, 1);
}

void
mmVKPushConstantRange_JDecode(
    VkPushConstantRange*                           p,
    const cJSON*                                   j)
{
    mmCJson_DecodeEnumerate(j, "stageFlags", &p->stageFlags, 0x00000000, 1, &mmVKShaderStageFlags_DecodeEnumerate);
    mmCJson_DecodeUInt32(j, "offset", &p->offset, 0, 1);
    mmCJson_DecodeUInt32(j, "size", &p->size, 0, 1);
}

void
mmVKDescriptorPoolSize_JEncode(
    const VkDescriptorPoolSize*                    p,
    cJSON*                                         j)
{
    mmCJson_EncodeEnumerate(j, "type", &p->type, 0x00000000, 1, &mmVKDescriptorType_EncodeEnumerate);
    mmCJson_EncodeUInt32(j, "descriptorCount", &p->descriptorCount, 0, 1);
}

void
mmVKDescriptorPoolSize_JDecode(
    VkDescriptorPoolSize*                          p,
    const cJSON*                                   j)
{
    mmCJson_DecodeEnumerate(j, "type", &p->type, 0x00000000, 1, &mmVKDescriptorType_DecodeEnumerate);
    mmCJson_DecodeUInt32(j, "descriptorCount", &p->descriptorCount, 0, 1);
}

void
mmVKVertexInputBindingDescription_JEncode(
    const VkVertexInputBindingDescription*         p,
    cJSON*                                         j)
{
    mmCJson_EncodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_EncodeUInt32(j, "stride", &p->stride, 0, 1);
    mmCJson_EncodeEnumerate(j, "inputRate", &p->inputRate, VK_VERTEX_INPUT_RATE_VERTEX, 1, &mmVKVertexInputRate_EncodeEnumerate);
}

void
mmVKVertexInputBindingDescription_JDecode(
    VkVertexInputBindingDescription*               p,
    const cJSON*                                   j)
{
    mmCJson_DecodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_DecodeUInt32(j, "stride", &p->stride, 0, 1);
    mmCJson_DecodeEnumerate(j, "inputRate", &p->inputRate, VK_VERTEX_INPUT_RATE_VERTEX, 1, &mmVKVertexInputRate_DecodeEnumerate);
}

void
mmVKVertexInputAttributeDescription_JEncode(
    const VkVertexInputAttributeDescription*         p,
    cJSON*                                         j)
{
    mmCJson_EncodeUInt32(j, "location", &p->location, 0, 1);
    mmCJson_EncodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_EncodeEnumerate(j, "format", &p->format, VK_FORMAT_UNDEFINED, 1, &mmVKFormat_EncodeEnumerate);
    mmCJson_EncodeUInt32(j, "offset", &p->offset, 0, 1);
}

void
mmVKVertexInputAttributeDescription_JDecode(
    VkVertexInputAttributeDescription*               p,
    const cJSON*                                   j)
{
    mmCJson_DecodeUInt32(j, "location", &p->location, 0, 1);
    mmCJson_DecodeUInt32(j, "binding", &p->binding, 0, 1);
    mmCJson_DecodeEnumerate(j, "format", &p->format, VK_FORMAT_UNDEFINED, 1, &mmVKFormat_DecodeEnumerate);
    mmCJson_DecodeUInt32(j, "offset", &p->offset, 0, 1);
}

void
mmVKPipelineVertexInputInfo_JEncode(
    const struct mmVKPipelineVertexInputInfo*      p,
    cJSON*                                         j)
{
    mmCJson_EncodeArray(j, "vibd", &p->vibd, &mmVKVertexInputBindingDescription_JEncode);
    mmCJson_EncodeArray(j, "viad", &p->viad, &mmVKVertexInputAttributeDescription_JEncode);
}

void
mmVKPipelineVertexInputInfo_JDecode(
    struct mmVKPipelineVertexInputInfo*            p,
    const cJSON*                                   j)
{
    mmCJson_DecodeArray(j, "vibd", &p->vibd, &mmVKVertexInputBindingDescription_JDecode);
    mmCJson_DecodeArray(j, "viad", &p->viad, &mmVKVertexInputAttributeDescription_JDecode);
}

void
mmVKPipelineShaderStageInfo_JEncode(
    const struct mmVKPipelineShaderStageInfo*      p,
    cJSON*                                         j)
{
    mmCJson_EncodeEnumerate(j, "stage", &p->stage, 0x00000000, 1, &mmVKShaderStageFlags_EncodeEnumerate);
    mmCJson_EncodeString(j, "module", &p->module, "", 1);
    mmCJson_EncodeString(j, "name", &p->name, "", 1);
}

void
mmVKPipelineShaderStageInfo_JDecode(
    struct mmVKPipelineShaderStageInfo*            p,
    const cJSON*                                   j)
{
    mmCJson_DecodeEnumerate(j, "stage", &p->stage, 0x00000000, 1, &mmVKShaderStageFlags_DecodeEnumerate);
    mmCJson_DecodeString(j, "module", &p->module, "", 1);
    mmCJson_DecodeString(j, "name", &p->name, "", 1);
}

void
mmVKLayoutInfoArgs_JEncode(
    const struct mmVKLayoutInfoArgs*               p,
    cJSON*                                         j)
{
    mmCJson_EncodeArray(j, "dslb", &p->dslb, &mmVKDescriptorSetLayoutBinding_JEncode);
    mmCJson_EncodeUInt32(j, "dslc", &p->dslc, 0, 1);
}

void
mmVKLayoutInfoArgs_JDecode(
    struct mmVKLayoutInfoArgs*                     p,
    const cJSON*                                   j)
{
    mmCJson_DecodeArray(j, "dslb", &p->dslb, &mmVKDescriptorSetLayoutBinding_JDecode);
    mmCJson_DecodeUInt32(j, "dslc", &p->dslc, 0, 1);
}

void
mmVKPipelineArgs_JEncode(
    const struct mmVKPipelineArgs*                 p,
    cJSON*                                         j)
{
    mmCJson_EncodeObject(j, "pvii", &p->pvii, &mmVKPipelineVertexInputInfo_JEncode);
    mmCJson_EncodeArray(j, "dslp", &p->dslp, &mmVKPushConstantRange_JEncode);
    mmCJson_EncodeArray(j, "pssi", &p->pssi, &mmVKPipelineShaderStageInfo_JEncode);
    mmCJson_EncodeStringVector(j, "layouts", &p->layouts);
    mmCJson_EncodeString(j, "passName", &p->passName, "", 1);
    mmCJson_EncodeUInt32(j, "samples", &p->samples, 0x00000000, 1);
}

void
mmVKPipelineArgs_JDecode(
    struct mmVKPipelineArgs*                       p,
    const cJSON*                                   j)
{
    mmCJson_DecodeObject(j, "pvii", &p->pvii, &mmVKPipelineVertexInputInfo_JDecode);
    mmCJson_DecodeArray(j, "dslp", &p->dslp, &mmVKPushConstantRange_JDecode);
    mmCJson_DecodeArray(j, "pssi", &p->pssi, &mmVKPipelineShaderStageInfo_JDecode);
    mmCJson_DecodeStringVector(j, "layouts", &p->layouts);
    mmCJson_DecodeString(j, "passName", &p->passName, "", 1);
    mmCJson_DecodeUInt32(j, "samples", &p->samples, 0x00000000, 1);
}
