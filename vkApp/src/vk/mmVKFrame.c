/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKFrame.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

VkFormat
mmVKPickDepthFormat(
    VkPhysicalDevice                               physicalDevice,
    VkFormat                                       expectFormat)
{
    /*
        Suitable depth format
    */
    static const VkFormat hDepthFormats[5] =
    {
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D32_SFLOAT,
        VK_FORMAT_D24_UNORM_S8_UINT,
        VK_FORMAT_D16_UNORM_S8_UINT,
        VK_FORMAT_D16_UNORM,
    };
    
    VkFormat hDepthFormat = VK_FORMAT_UNDEFINED;
    VkFormat hFormat;
    VkFormatProperties hFormatProperties;
    
    hFormat = expectFormat;
    vkGetPhysicalDeviceFormatProperties(physicalDevice, hFormat, &hFormatProperties);
    if (hFormatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
    {
        hDepthFormat = hFormat;
    }
    else
    {
        size_t i = 0;
        for (i = 0;i < MM_ARRAY_SIZE(hDepthFormats);++i)
        {
            hFormat = hDepthFormats[i];
            
            vkGetPhysicalDeviceFormatProperties(physicalDevice, hFormat, &hFormatProperties);
            if (hFormatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
            {
                hDepthFormat = hFormat;
                break;
            }
        }
    }
    
    return hDepthFormat;
}

void
mmVKFrame_Init(
    struct mmVKFrame*                              p)
{
    mmMemset(p, 0, sizeof(struct mmVKFrame));
}

void
mmVKFrame_Destroy(
    struct mmVKFrame*                              p)
{
    mmMemset(p, 0, sizeof(struct mmVKFrame));
}

void
mmVKFrame_Reset(
    struct mmVKFrame*                              p)
{
    mmMemset(p, 0, sizeof(struct mmVKFrame));
}

VkResult
mmVKFrame_Create(
    struct mmVKFrame*                              p,
    const struct mmVKFrameCreateInfo*              info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;
        VmaAllocator vmaAllocator = VK_NULL_HANDLE;

        VkImageCreateInfo hImageInfo;
        VkImageViewCreateInfo hImageViewInfo;
        VmaAllocationCreateInfo hVmaAllocInfo;
        VkImageCreateFlags flags;

        int hCubeMap;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        vmaAllocator = pUploader->vmaAllocator;
        if (VK_NULL_HANDLE == vmaAllocator)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaAllocator is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        if (0 == info->extent[0] || 0 == info->extent[1] || 0 == info->extent[2])
        {
            mmLogger_LogE(gLogger, "%s %d"
                " info->extent is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        pAllocator = pUploader->pAllocator;
                
        mmMemset(&hVmaAllocInfo, 0, sizeof(VmaAllocationCreateInfo));
        
        hCubeMap = (VK_IMAGE_VIEW_TYPE_CUBE_ARRAY == info->viewType || VK_IMAGE_VIEW_TYPE_CUBE == info->viewType);
        flags = (hCubeMap) ? VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : 0;

        hImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        hImageInfo.pNext = NULL;
        hImageInfo.flags = flags;
        hImageInfo.imageType = info->imageType;
        hImageInfo.format = info->format;
        hImageInfo.extent.width = info->extent[0];
        hImageInfo.extent.height = info->extent[1];
        hImageInfo.extent.depth = info->extent[2];
        hImageInfo.mipLevels = info->mipLevels;
        hImageInfo.arrayLayers = info->arrayLayers;
        hImageInfo.samples = info->samples;
        hImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        hImageInfo.usage = info->usage;
        hImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hImageInfo.queueFamilyIndexCount = 0;
        hImageInfo.pQueueFamilyIndices = NULL;
        hImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        
        hVmaAllocInfo.flags = info->vmaFlags;
        hVmaAllocInfo.usage = info->vmaUsage;
        err = vmaCreateImage(
            vmaAllocator,
            &hImageInfo,
            &hVmaAllocInfo,
            &p->image,
            &p->allocation,
            NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaCreateImage failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = p->image;
        hImageViewInfo.viewType = info->viewType;
        hImageViewInfo.format = info->format;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = info->aspectMask;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = 1;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = info->arrayLayers;
        
        err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &p->imageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateImageView failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
    
    return err;
}

void
mmVKFrame_Delete(
    struct mmVKFrame*                              p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;
        VmaAllocator vmaAllocator = VK_NULL_HANDLE;

        if (NULL == pUploader)
        {
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        vmaAllocator = pUploader->vmaAllocator;
        if (VK_NULL_HANDLE == vmaAllocator)
        {
            break;
        }

        pAllocator = pUploader->pAllocator;

        if (VK_NULL_HANDLE != p->imageView)
        {
            vkDestroyImageView(device, p->imageView, pAllocator);
            p->imageView = VK_NULL_HANDLE;
        }

        if (VK_NULL_HANDLE != p->allocation)
        {
            vmaDestroyImage(vmaAllocator, p->image, p->allocation);
            p->allocation = VK_NULL_HANDLE;
            p->image = VK_NULL_HANDLE;
        }

        // image just reset NULL.
        p->image = VK_NULL_HANDLE;
    } while (0);
}

