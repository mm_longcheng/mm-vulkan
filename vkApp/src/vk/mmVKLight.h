/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKLight_h__
#define __mmVKLight_h__

#include "core/mmCore.h"

#include "vk/mmVKUniformType.h"

#include "core/mmPrefix.h"

struct mmVKUBOShadowLightInfo
{
    mmUniformInt count;
    mmUniformInt index;
    mmUniformInt debug;
    mmUniformInt mode;
    mmUniformFloat ambient;
    mmUniformFloat biasMax;
    mmUniformFloat biasMin;
};

struct mmVKUBOLight
{
    mmUniformVec3 direction;
    mmUniformVec3 position;
    mmUniformVec3 color;
    mmUniformFloat range;
    mmUniformFloat intensity;
    mmUniformFloat innerConeCos;
    mmUniformFloat outerConeCos;
    mmUniformInt type;
    mmUniformInt shadow;

    mmUniformInt count;
    mmUniformInt mode;
    mmUniformFloat ambient;
    mmUniformFloat biasMax;
    mmUniformFloat biasMin;

    mmUniformInt debug;
    mmUniformInt idxMat;
    mmUniformInt idxMap;
};

struct mmVKLightInfo
{
    // light direction.
    float direction[3];

    // light position.
    float position[3];

    // light color.
    float color[3];

    // light range.
    float range;

    // light intensity.
    float intensity;

    // light inner cone cos.
    float innerConeCos;

    // light outer cone cos.
    float outerConeCos;

    // light type.
    int type;
    
    // whether cast shadow.
    int castShadow;

    // shadow mapping cascade count.
    // default is 4. range[0, 4].
    int count;

    // Percentage Closer Filter(PCF) [1, 5, 9]
    int mode;

    // for debug color.
    int debug;

    // Shadow ambient, default 0.0f
    float ambient;

    // Shadow BiasMax, default 0.005f
    float biasMax;

    // Shadow BiasMin, default 0.001f
    float biasMin;

    // cascade split lambda. default is 0.95f.
    float splitLambda;
};

int
mmVKLightInfo_GetFrameNumber(
    struct mmVKLightInfo*                          p);

#include "core/mmSuffix.h"

#endif//__mmVKLight_h__
