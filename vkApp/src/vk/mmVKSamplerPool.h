/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKSamplerPool_h__
#define __mmVKSamplerPool_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmVKUploader;

struct mmVKSamplerCreateInfo
{
    VkFilter                magFilter;
    VkFilter                minFilter;
    VkSamplerMipmapMode     mipmapMode;
    VkSamplerAddressMode    addressModeU;
    VkSamplerAddressMode    addressModeV;
    VkSamplerAddressMode    addressModeW;
    VkBorderColor           borderColor;
};

extern const struct mmVKSamplerCreateInfo mmVKSamplerNearestRepeat;
extern const struct mmVKSamplerCreateInfo mmVKSamplerNearestEdge;
extern const struct mmVKSamplerCreateInfo mmVKSamplerLinearRepeat;
extern const struct mmVKSamplerCreateInfo mmVKSamplerLinearEdge;

uint32_t
mmVKSamplerCreateInfo_Hash(
    const struct mmVKSamplerCreateInfo*            p);

int
mmVKSamplerCreateInfo_Equals(
    const struct mmVKSamplerCreateInfo*            p,
    const struct mmVKSamplerCreateInfo*            q);

VkResult
mmVKUploader_PrepareSampler(
    struct mmVKUploader*                           pUploader,
    const struct mmVKSamplerCreateInfo*            info,
    VkSampler*                                     pSampler);

void
mmVKUploader_DiscardSampler(
    struct mmVKUploader*                           pUploader,
    VkSampler                                      sampler);

struct mmVKSamplerInfo
{
    // info
    struct mmVKSamplerCreateInfo info;

    // hash value.
    uint32_t hash;

    // sampler.
    VkSampler sampler;

    // reference count.  
    size_t reference;
};

void
mmVKSamplerInfo_Init(
    struct mmVKSamplerInfo*                        p);

void
mmVKSamplerInfo_Destroy(
    struct mmVKSamplerInfo*                        p);

int
mmVKSamplerInfo_Invalid(
    const struct mmVKSamplerInfo*                  p);

void
mmVKSamplerInfo_Increase(
    struct mmVKSamplerInfo*                        p);

void
mmVKSamplerInfo_Decrease(
    struct mmVKSamplerInfo*                        p);

struct mmVKSamplerPool
{
    struct mmRbtreeU32Vpt samplers;
    struct mmVKUploader* pUploader;
};

void
mmVKSamplerPool_Init(
    struct mmVKSamplerPool*                        p);

void
mmVKSamplerPool_Destroy(
    struct mmVKSamplerPool*                        p);

void
mmVKSamplerPool_SetUploader(
    struct mmVKSamplerPool*                        p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKSamplerPool_PrepareSamplerInfo(
    struct mmVKSamplerPool*                        p,
    const struct mmVKSamplerCreateInfo*            info,
    struct mmVKSamplerInfo*                        pSamplerInfo);

void
mmVKSamplerPool_DiscardSamplerInfo(
    struct mmVKSamplerPool*                        p,
    struct mmVKSamplerInfo*                        pSamplerInfo);

struct mmVKSamplerInfo*
mmVKSamplerPool_Add(
    struct mmVKSamplerPool*                        p,
    const struct mmVKSamplerCreateInfo*            info);

void
mmVKSamplerPool_Remove(
    struct mmVKSamplerPool*                        p,
    uint32_t                                       hash);

void
mmVKSamplerPool_Reset(
    struct mmVKSamplerPool*                        p);

#include "core/mmSuffix.h"

#endif//__mmVKSamplerPool_h__
