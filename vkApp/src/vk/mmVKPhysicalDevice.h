/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPhysicalDevice_h__
#define __mmVKPhysicalDevice_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKInstance.h"

#include "ktx.h"

#include "core/mmPrefix.h"

VkDeviceSize
mmVKPhysicalDeviceGetHeapSize(
    VkPhysicalDeviceMemoryProperties*              memoryProperties);

ktx_transcode_fmt_e
mmVKPhysicalDeviceGetTranscodefmt(
    VkPhysicalDeviceFeatures*                      features);

struct mmVKPhysicalDevice
{
    VkPhysicalDevice physicalDevice;
    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceMemoryProperties memoryProperties;
    VkPhysicalDeviceFeatures features;
    ktx_transcode_fmt_e transcodefmt;
    uint32_t index;
};

void
mmVKPhysicalDevice_Reset(
    struct mmVKPhysicalDevice*                     p);

VkResult
mmVKPhysicalDevice_Create(
    struct mmVKPhysicalDevice*                     p,
    VkInstance                                     instance,
    uint32_t                                       index);

void
mmVKPhysicalDevice_Delete(
    struct mmVKPhysicalDevice*                     p);

VkSampleCountFlagBits
mmVKPhysicalDevice_SupportSamples(
    struct mmVKPhysicalDevice*                     p,
    VkSampleCountFlags                             samples);

VkSampleCountFlagBits
mmVKPhysicalDevice_SuitableSamples(
    struct mmVKPhysicalDevice*                     p,
    VkSampleCountFlags                             samples,
    double                                         hDisplayDensity);

#include "core/mmSuffix.h"

#endif//__mmVKPhysicalDevice_h__
