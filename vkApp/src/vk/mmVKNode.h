/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKNode_h__
#define __mmVKNode_h__

#include "core/mmCore.h"

#include "container/mmListVpt.h"

#include "core/mmPrefix.h"

struct mmVKNode
{
    // children
    struct mmListVpt children;

    // parent node
    struct mmVKNode* parent;

    // [x, y, z]
    float scale[3];

    // [x, y, z, w]
    float quaternion[4];

    // [x, y, z]
    float position[3];

    // cache world value.
    float worldTransform[4][4];

    // valid world cache.
    mmUInt8_t validWorldTransform;
};
void mmVKNode_Init(struct mmVKNode* p);
void mmVKNode_Destroy(struct mmVKNode* p);

void mmVKNode_GetWorldTransform(struct mmVKNode* p, float worldTransform[4][4]);
void mmVKNode_GetLocalTransform(struct mmVKNode* p, float localTransform[4][4]);

void mmVKNode_AddChild(struct mmVKNode* p, struct mmVKNode* child);
void mmVKNode_RmvChild(struct mmVKNode* p, struct mmVKNode* child);

void mmVKNode_SetScale(struct mmVKNode* p, float scale[3]);
void mmVKNode_SetQuaternion(struct mmVKNode* p, float quaternion[4]);
void mmVKNode_SetPosition(struct mmVKNode* p, float position[3]);

void mmVKNode_MarkCacheInvalidWhole(struct mmVKNode* p);
void mmVKNode_MarkCacheInvalidWorldTransform(struct mmVKNode* p);

#include "core/mmSuffix.h"

#endif//__mmVKNode_h__
