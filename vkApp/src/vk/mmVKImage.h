/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKImage_h__
#define __mmVKImage_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

void
mmVKSetImageLayout(
    VkCommandBuffer                                cmdBuffer,
    VkImage                                        image,
    VkImageLayout                                  oldLayout,
    VkImageLayout                                  newLayout,
    VkImageSubresourceRange*                       subresourceRange);

struct mmVKImage
{
    VmaAllocation allocation;
    VkImage image;
    VkFormat format;
    VkImageLayout imageLayout;
    VkImageViewType viewType;
    uint32_t width;
    uint32_t height;
    uint32_t depth;
    uint32_t levelCount;
    uint32_t layerCount;
};

void
mmVKImage_Init(
    struct mmVKImage*                              p);

void
mmVKImage_Destroy(
    struct mmVKImage*                              p);

void
mmVKImage_Reset(
    struct mmVKImage*                              p);

#include "core/mmSuffix.h"

#endif//__mmVKImage_h__
