/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKKTXUploader_h__
#define __mmVKKTXUploader_h__

#include "core/mmCore.h"

#include "vk/mmVKUploader.h"

#include "vma/VmaUsage.h"

#include "ktx.h"

#include "core/mmPrefix.h"

void
mmVKKTXUploader_GenerateMipmaps(
    struct mmVKUploader*                           vdi,
    struct mmVKImage*                              pImage,
    VkFilter                                       filter,
    VkImageLayout                                  initialLayout);

ktx_error_code_e
mmVKKTXUploader_VkUploadEx(
    ktxTexture*                                    This,
    struct mmVKUploader*                           vdi,
    struct mmVKImage*                              pImage,
    VkImageTiling                                  tiling,
    VkImageUsageFlags                              usageFlags,
    VkImageLayout                                  finalLayout);

#include "core/mmSuffix.h"

#endif//__mmVKKTXUploader_h__
