/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKLayoutLoader_h__
#define __mmVKLayoutLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorU64.h"
#include "container/mmRbtreeString.h"
#include "container/mmVectorValue.h"

#include "vk/mmVKPlatform.h"

#include "parse/mmParseJSON.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmVKDevice;

struct mmVKLayoutCreateInfo
{
    // vector<VkDescriptorSetLayoutBinding>
    const struct mmVectorValue* dslb;

    // descriptor set layout count
    uint32_t dslc;
};

struct mmVKLayoutInfo
{
    // name for layout info, read only. 
    // string is weak reference value.
    struct mmString name;

    // vector<VkDescriptorSetLayoutBinding>
    struct mmVectorValue dslb;

    // descriptor set layout count.
    uint32_t dslc;

    // descriptor set layout.
    VkDescriptorSetLayout descriptorSetLayout;

    // reference count. 
    size_t reference;
};

void
mmVKLayoutInfo_Init(
    struct mmVKLayoutInfo*                         p);

void
mmVKLayoutInfo_Destroy(
    struct mmVKLayoutInfo*                         p);

int
mmVKLayoutInfo_Invalid(
    const struct mmVKLayoutInfo*                   p);

void
mmVKLayoutInfo_Increase(
    struct mmVKLayoutInfo*                         p);

void
mmVKLayoutInfo_Decrease(
    struct mmVKLayoutInfo*                         p);

VkResult
mmVKLayoutInfo_Prepare(
    struct mmVKLayoutInfo*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKLayoutCreateInfo*             info);

void
mmVKLayoutInfo_Discard(
    struct mmVKLayoutInfo*                         p,
    struct mmVKDevice*                             pDevice);

struct mmVKLayoutLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKDevice* pDevice;
    struct mmRbtreeStringVpt sets;
    struct mmParseJSON parse;
};

void
mmVKLayoutLoader_Init(
    struct mmVKLayoutLoader*                       p);

void
mmVKLayoutLoader_Destroy(
    struct mmVKLayoutLoader*                       p);

void
mmVKLayoutLoader_SetFileContext(
    struct mmVKLayoutLoader*                       p,
    struct mmFileContext*                          pFileContext);

void
mmVKLayoutLoader_SetDevice(
    struct mmVKLayoutLoader*                       p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKLayoutLoader_PrepareLayoutInfo(
    struct mmVKLayoutLoader*                       p,
    const struct mmVKLayoutCreateInfo*             info,
    struct mmVKLayoutInfo*                         pLayoutInfo);

void
mmVKLayoutLoader_DiscardLayoutInfo(
    struct mmVKLayoutLoader*                       p,
    struct mmVKLayoutInfo*                         pLayoutInfo);

void
mmVKLayoutLoader_UnloadByName(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name);

void
mmVKLayoutLoader_UnloadComplete(
    struct mmVKLayoutLoader*                       p);

struct mmVKLayoutInfo*
mmVKLayoutLoader_LoadFromInfo(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name,
    const struct mmVKLayoutCreateInfo*             info);

struct mmVKLayoutInfo*
mmVKLayoutLoader_LoadFromFile(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name,
    const char*                                    path);

struct mmVKLayoutInfo*
mmVKLayoutLoader_GetFromName(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name);

VkResult
mmVKLayoutLoader_GetLayoutsByStrings(
    struct mmVKLayoutLoader*                       p,
    const struct mmVectorValue*                    strings,
    struct mmVectorVpt*                            dsli);

VkResult
mmVKLayoutLoader_GetLayoutsByNames(
    struct mmVKLayoutLoader*                       p,
    const struct mmVectorVpt*                      names,
    struct mmVectorVpt*                            dsli);

#include "core/mmSuffix.h"

#endif//__mmVKLayoutLoader_h__
