/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKExtension.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

void
mmVKExtension_Init(
    struct mmVKExtension*                          p)
{
    p->hArraySize = 0;
    p->pSupportArray = NULL;
    p->pRequireArray = NULL;
    p->pEnabledArray = NULL;
    p->hRequireCount = 0;
    p->hEnabledCount = 0;
}

void
mmVKExtension_Destroy(
    struct mmVKExtension*                          p)
{
    assert(0 == p->hArraySize && "must dealloc array before destroy.");

    p->hArraySize = 0;
    p->pSupportArray = NULL;
    p->pRequireArray = NULL;
    p->pEnabledArray = NULL;
    p->hRequireCount = 0;
    p->hEnabledCount = 0;
}

void
mmVKExtension_AllocArraySize(
    struct mmVKExtension*                          p,
    size_t                                         hArraySize)
{
    size_t sz = sizeof(struct mmVKExtensionSupport) * hArraySize;
    p->hArraySize = hArraySize;
    p->pSupportArray = (struct mmVKExtensionSupport*)mmMalloc(sz);
    p->pRequireArray = (char**)mmMalloc(sizeof(const char*) * p->hArraySize);
    p->pEnabledArray = (char**)mmMalloc(sizeof(const char*) * p->hArraySize);
    mmMemset(p->pSupportArray, 0, sizeof(struct mmVKExtensionSupport) * p->hArraySize);
    mmMemset((void*)p->pRequireArray, 0, sizeof(const char*) * p->hArraySize);
    mmMemset((void*)p->pEnabledArray, 0, sizeof(const char*) * p->hArraySize);
    p->hRequireCount = 0;
    p->hEnabledCount = 0;
}

void
mmVKExtension_DeallocArray(
    struct mmVKExtension*                          p)
{
    mmFree(p->pSupportArray);
    mmFree(p->pRequireArray);
    mmFree(p->pEnabledArray);

    p->hArraySize = 0;
    p->pSupportArray = NULL;
    p->pRequireArray = NULL;
    p->pEnabledArray = NULL;
    p->hRequireCount = 0;
    p->hEnabledCount = 0;
}

void
mmVKExtension_SetRequireFeature(
    struct mmVKExtension*                          p,
    uint32_t                                       idx,
    uint8_t                                        flag,
    const char*                                    pName)
{
    assert(idx < p->hArraySize && "idx < p->hArraySize");
    p->pSupportArray[idx].hFlag = flag;
    p->pRequireArray[idx] = (char*)pName;
    p->hRequireCount = idx + 1;
}

int
mmVKExtension_GetIsSuitable(
    struct mmVKExtension*                          p)
{
    uint32_t i = 0;
    for (i = 0; i < p->hRequireCount; ++i)
    {
        if (NULL != p->pRequireArray[i] &&
            1 == p->pSupportArray[i].hFlag &&
            0 == p->pSupportArray[i].hFind)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "Failed to find the %s extension.", p->pRequireArray[i]);
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            return VK_FALSE;
        }
    }
    return VK_TRUE;
}

int
mmVKExtension_GetIsSupportName(
    struct mmVKExtension*                          p,
    const char*                                    name)
{
    uint32_t i = 0;
    for (i = 0; i < p->hRequireCount; ++i)
    {
        if (NULL != p->pRequireArray[i] &&
            0 == strcmp(p->pRequireArray[i], name))
        {
            return p->pSupportArray[i].hFind;
        }
    }
    return VK_FALSE;
}

int
mmVKExtension_GetIsSupport(
    struct mmVKExtension*                          p,
    uint32_t                                       idx)
{
    assert(idx < p->hRequireCount && "idx < p->hRequireCount");
    return p->pSupportArray[idx].hFind;
}

void
mmVKExtension_EnabledInstanceLayer(
    struct mmVKExtension*                          p)
{
    VkLayerProperties* pArray = NULL;

    do
    {
        VkResult err = VK_ERROR_UNKNOWN;
        uint32_t i = 0;
        uint32_t j = 0;
        uint32_t hCount = 0;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (0 == p->hRequireCount)
        {
            break;
        }

        err = vkEnumerateInstanceLayerProperties(&hCount, NULL);
        if (VK_SUCCESS != err || 0 == hCount)
        {
            break;
        }

        mmMemset((void*)p->pEnabledArray, 0, sizeof(const char*) * p->hArraySize);
        p->hEnabledCount = 0;

        pArray = mmMalloc(sizeof(VkLayerProperties) * hCount);
        if (NULL == pArray)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = vkEnumerateInstanceLayerProperties(&hCount, pArray);
        if (VK_SUCCESS != err)
        {
            break;
        }

        for (i = 0; i < p->hRequireCount; i++)
        {
            p->pSupportArray[i].hFind = 0;
            for (j = 0; j < hCount; j++)
            {
                if (0 == strcmp(p->pRequireArray[i], pArray[j].layerName))
                {
                    p->pEnabledArray[p->hEnabledCount] = p->pRequireArray[i];
                    p->hEnabledCount++;
                    p->pSupportArray[i].hFind = 1;
                    break;
                }
            }
        }

    } while (0);

    mmFree(pArray);
}

void
mmVKExtension_EnabledInstanceExtension(
    struct mmVKExtension*                          p)
{
    VkExtensionProperties* pArray = NULL;

    do
    {
        VkResult err = VK_ERROR_UNKNOWN;
        uint32_t i = 0;
        uint32_t j = 0;
        uint32_t hCount = 0;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (0 == p->hRequireCount)
        {
            break;
        }

        err = vkEnumerateInstanceExtensionProperties(NULL, &hCount, NULL);
        if (VK_SUCCESS != err || 0 == hCount)
        {
            break;
        }

        mmMemset((void*)p->pEnabledArray, 0, sizeof(const char*) * p->hArraySize);
        p->hEnabledCount = 0;

        pArray = mmMalloc(sizeof(VkExtensionProperties) * hCount);
        if (NULL == pArray)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = vkEnumerateInstanceExtensionProperties(NULL, &hCount, pArray);
        if (VK_SUCCESS != err)
        {
            break;
        }

        for (i = 0; i < p->hRequireCount; i++)
        {
            p->pSupportArray[i].hFind = 0;
            for (j = 0; j < hCount; j++)
            {
                if (0 == strcmp(p->pRequireArray[i], pArray[j].extensionName))
                {
                    p->pEnabledArray[p->hEnabledCount] = p->pRequireArray[i];
                    p->hEnabledCount++;
                    p->pSupportArray[i].hFind = 1;
                    break;
                }
            }
        }

    } while (0);

    mmFree(pArray);
}

void
mmVKExtension_EnabledDeviceExtension(
    struct mmVKExtension*                          p,
    VkPhysicalDevice                               pPhysicalDevice)
{
    VkExtensionProperties* pArray = NULL;

    do
    {
        VkResult err = VK_ERROR_UNKNOWN;
        uint32_t i = 0;
        uint32_t j = 0;
        uint32_t hCount = 0;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pPhysicalDevice)
        {
            break;
        }

        if (0 == p->hRequireCount)
        {
            break;
        }

        err = vkEnumerateDeviceExtensionProperties(pPhysicalDevice, NULL, &hCount, NULL);
        if (VK_SUCCESS != err || 0 == hCount)
        {
            break;
        }

        mmMemset((void*)p->pEnabledArray, 0, sizeof(const char*) * p->hArraySize);
        p->hEnabledCount = 0;

        pArray = mmMalloc(sizeof(VkExtensionProperties) * hCount);
        if (NULL == pArray)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = vkEnumerateDeviceExtensionProperties(pPhysicalDevice, NULL, &hCount, pArray);
        if (VK_SUCCESS != err)
        {
            break;
        }

        for (i = 0; i < p->hRequireCount; i++)
        {
            p->pSupportArray[i].hFind = 0;
            for (j = 0; j < hCount; j++)
            {
                if (0 == strcmp(p->pRequireArray[i], pArray[j].extensionName))
                {
                    p->pEnabledArray[p->hEnabledCount] = p->pRequireArray[i];
                    p->hEnabledCount++;
                    p->pSupportArray[i].hFind = 1;
                    break;
                }
            }
        }

    } while (0);

    mmFree(pArray);
}
