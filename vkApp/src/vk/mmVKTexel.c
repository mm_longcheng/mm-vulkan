/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTexel.h"

#include "vk/mmVKUploader.h"

#include "core/mmLogger.h"

void
mmVKTexel_Init(
    struct mmVKTexel*                              p)
{
    mmVKBuffer_Init(&p->vBuffer);
    mmVKImage_Init(&p->vImage);
    p->imageView = VK_NULL_HANDLE;
}

void
mmVKTexel_Destroy(
    struct mmVKTexel*                              p)
{
    p->imageView = VK_NULL_HANDLE;
    mmVKImage_Destroy(&p->vImage);
    mmVKBuffer_Destroy(&p->vBuffer);
}

int
mmVKTexel_Invalid(
    const struct mmVKTexel*                        p)
{
    return
        (NULL == p) ||
        (VK_NULL_HANDLE == p->vBuffer.buffer) ||
        (VK_NULL_HANDLE == p->vImage.image) ||
        (VK_NULL_HANDLE == p->imageView);
}

VkResult
mmVKTexel_Prepare(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader,
    const uint8_t*                                 buffer,
    size_t                                         size,
    struct mmVKTexelCreateInfo*                    info)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkBufferCreateInfo hBufferInfo;
        VkImageCreateInfo hImageInfo;
        
        VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageLayout finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = size;
        hBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->vBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        hImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        hImageInfo.pNext = NULL;
        hImageInfo.flags = 0;
        hImageInfo.imageType = info->imageType;
        hImageInfo.format = info->format;
        hImageInfo.extent.width = info->extent[0];
        hImageInfo.extent.height = info->extent[1];
        hImageInfo.extent.depth = info->extent[2];
        hImageInfo.mipLevels = info->mipLevels;
        hImageInfo.arrayLayers = info->arrayLayers;
        hImageInfo.samples = info->samples;
        hImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        hImageInfo.usage = info->usage;
        hImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hImageInfo.queueFamilyIndexCount = 0;
        hImageInfo.pQueueFamilyIndices = NULL;
        hImageInfo.initialLayout = initialLayout;
        
        err = mmVKUploader_CreateImage(
            pUploader,
            &hImageInfo,
            info->vmaFlags,
            info->vmaUsage,
            info->viewType,
            &p->vImage);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateImage failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKUploader_PrepareImageView(
            pUploader,
            &p->vImage,
            info->viewType,
            info->aspectMask,
            &p->imageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_PrepareImageView failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (const uint8_t*)buffer,
            0,
            size,
            &p->vBuffer,
            0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
        }
        
        err = mmVKUploader_CopyBufferToImage(
            pUploader,
            &p->vBuffer,
            &p->vImage,
            finalLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_CopyBufferToImage failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
    
    return err;
}

void
mmVKTexel_Discard(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader)
{
    if (NULL == pUploader)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d"
            " pUploader is invalid.", __FUNCTION__, __LINE__);
    }
    else
    {
        mmVKUploader_DiscardImageView(pUploader, p->imageView);
        mmVKUploader_DeleteImage(pUploader, &p->vImage);
        mmVKUploader_DeleteBuffer(pUploader, &p->vBuffer);
    }
}

VkResult
mmVKTexel_UpdateBuffer(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader,
    const uint8_t*                                 buffer,
    size_t                                         size)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (const uint8_t*)buffer,
            0,
            size,
            &p->vBuffer,
            0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
        }
        
        err = mmVKUploader_CopyBufferToImage(
            pUploader,
            &p->vBuffer,
            &p->vImage,
            p->vImage.imageLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_CopyBufferToImage failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);
    
    return err;
}
