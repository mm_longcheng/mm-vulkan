/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKJSON_h__
#define __mmVKJSON_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "parse/mmParseJSONHelper.h"

#include "core/mmPrefix.h"

struct mmParseJSON;

int
mmParseJSON_DecodeVKDescriptorType(
    struct mmParseJSON*                            p,
    int                                            i,
    VkDescriptorType*                              v,
    VkDescriptorType                               d);

int
mmParseJSON_DecodeVKShaderStageFlags(
    struct mmParseJSON*                            p,
    int                                            i,
    VkShaderStageFlags*                            v,
    VkShaderStageFlags                             d);

int
mmParseJSON_DecodeVKVertexInputRate(
    struct mmParseJSON*                            p,
    int                                            i,
    VkVertexInputRate*                             v,
    VkVertexInputRate                              d);

int
mmParseJSON_DecodeVKFormat(
    struct mmParseJSON*                            p,
    int                                            i,
    VkFormat*                                      v,
    VkFormat                                       d);

int
mmParseJSON_DecodeVKPrimitiveTopology(
    struct mmParseJSON*                            p,
    int                                            i,
    VkPrimitiveTopology*                           v,
    VkPrimitiveTopology                            d);

int
mmParseJSON_DecodeVKPolygonMode(
    struct mmParseJSON*                            p,
    int                                            i,
    VkPolygonMode*                                 v,
    VkPolygonMode                                  d);

int
mmParseJSON_DecodeVKCullModeFlags(
    struct mmParseJSON*                            p,
    int                                            i,
    VkCullModeFlags*                               v,
    VkCullModeFlags                                d);

int
mmParseJSON_DecodeVKFrontFace(
    struct mmParseJSON*                            p,
    int                                            i,
    VkFrontFace*                                   v,
    VkFrontFace                                    d);

int
mmParseJSON_DecodeVKBlendFactor(
    struct mmParseJSON*                            p,
    int                                            i,
    VkBlendFactor*                                 v,
    VkBlendFactor                                  d);

int
mmParseJSON_DecodeVKBlendOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkBlendOp*                                     v,
    VkBlendOp                                      d);

int
mmParseJSON_DecodeVKCompareOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkCompareOp*                                   v,
    VkCompareOp                                    d);

int
mmParseJSON_DecodeVKStencilOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkStencilOp*                                   v,
    VkStencilOp                                    d);

int
mmParseJSON_DecodeVKLogicOp(
    struct mmParseJSON*                            p,
    int                                            i,
    VkLogicOp*                                     v,
    VkLogicOp                                      d);

int
mmParseJSON_DecodeVKDescriptorSetLayoutBinding(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPushConstantRange(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKDescriptorPoolSize(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKVertexInputBindingDescription(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKVertexInputAttributeDescription(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineVertexInputInfo(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineShaderStageInfo(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKLayoutInfoArgs(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineInputAssemblyState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineTessellationState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineRasterizationState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineColorBlendAttachmentState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineColorBlendState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKStencilOpState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineDepthStencilState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineMultisampleState(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

int
mmParseJSON_DecodeVKPipelineArgs(
    struct mmParseJSON*                            p,
    int                                            i,
    mmUInt32_t                                     h,
    void*                                          o);

#include "core/mmSuffix.h"

#endif//__mmVKJSON_h__
