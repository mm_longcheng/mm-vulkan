/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKSamplerPool.h"

#include "vk/mmVKUploader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

const struct mmVKSamplerCreateInfo mmVKSamplerNearestRepeat =
{
    VK_FILTER_NEAREST,
    VK_FILTER_NEAREST,
    VK_SAMPLER_MIPMAP_MODE_NEAREST,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
};

const struct mmVKSamplerCreateInfo mmVKSamplerNearestEdge =
{
    VK_FILTER_NEAREST,
    VK_FILTER_NEAREST,
    VK_SAMPLER_MIPMAP_MODE_NEAREST,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
};

const struct mmVKSamplerCreateInfo mmVKSamplerLinearRepeat =
{
    VK_FILTER_LINEAR,
    VK_FILTER_LINEAR,
    VK_SAMPLER_MIPMAP_MODE_LINEAR,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_SAMPLER_ADDRESS_MODE_REPEAT,
    VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
};

const struct mmVKSamplerCreateInfo mmVKSamplerLinearEdge =
{
    VK_FILTER_LINEAR,
    VK_FILTER_LINEAR,
    VK_SAMPLER_MIPMAP_MODE_LINEAR,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
};

uint32_t
mmVKSamplerCreateInfo_Hash(
    const struct mmVKSamplerCreateInfo*            p)
{
    uint32_t hash = 0;

    hash |= (p->magFilter         );
    hash |= (p->minFilter    <<  1);
    hash |= (p->mipmapMode   <<  2);
    hash |= (p->addressModeU <<  3);
    hash |= (p->addressModeV <<  6);
    hash |= (p->addressModeW <<  9);
    hash |= (p->borderColor  << 12);

    return hash;
}

int
mmVKSamplerCreateInfo_Equals(
    const struct mmVKSamplerCreateInfo*            p,
    const struct mmVKSamplerCreateInfo*            q)
{
    return
        p->magFilter    == q->magFilter    &&
        p->minFilter    == q->minFilter    &&
        p->mipmapMode   == q->mipmapMode   &&
        p->addressModeU == q->addressModeU &&
        p->addressModeV == q->addressModeV &&
        p->addressModeW == q->addressModeW &&
        p->borderColor  == q->borderColor;
}

VkResult
mmVKUploader_PrepareSampler(
    struct mmVKUploader*                           pUploader,
    const struct mmVKSamplerCreateInfo*            info,
    VkSampler*                                     pSampler)
{
    VkResult err = VK_ERROR_UNKNOWN;

    (*pSampler) = VK_NULL_HANDLE;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkSamplerCreateInfo hSamplerInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pUploader->pAllocator;

        hSamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        hSamplerInfo.pNext = NULL;
        hSamplerInfo.flags = 0;
        hSamplerInfo.magFilter = info->magFilter;
        hSamplerInfo.minFilter = info->minFilter;
        hSamplerInfo.mipmapMode = info->mipmapMode;
        hSamplerInfo.addressModeU = info->addressModeU;
        hSamplerInfo.addressModeV = info->addressModeV;
        hSamplerInfo.addressModeW = info->addressModeW;
        hSamplerInfo.mipLodBias = 0.0f;
        hSamplerInfo.anisotropyEnable = VK_FALSE;
        hSamplerInfo.maxAnisotropy = 1;
        hSamplerInfo.compareEnable = VK_FALSE;
        hSamplerInfo.compareOp = VK_COMPARE_OP_NEVER;
        hSamplerInfo.minLod = 0.0f;
        hSamplerInfo.maxLod = 0.0f;
        hSamplerInfo.borderColor = info->borderColor;
        hSamplerInfo.unnormalizedCoordinates = VK_FALSE;

        err = vkCreateSampler(device, &hSamplerInfo, pAllocator, pSampler);

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateSampler failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKUploader_DiscardSampler(
    struct mmVKUploader*                           pUploader,
    VkSampler                                      sampler)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (VK_NULL_HANDLE == sampler)
        {
            break;
        }

        pAllocator = pUploader->pAllocator;

        vkDestroySampler(device, sampler, pAllocator);
    } while (0);
}

void
mmVKSamplerInfo_Init(
    struct mmVKSamplerInfo*                        p)
{
    mmMemset(&p->info, 0, sizeof(struct mmVKSamplerCreateInfo));
    p->hash = 0;
    p->sampler = VK_NULL_HANDLE;
    p->reference = 0;
}

void
mmVKSamplerInfo_Destroy(
    struct mmVKSamplerInfo*                        p)
{
    p->reference = 0;
    p->sampler = VK_NULL_HANDLE;
    p->hash = 0;
    mmMemset(&p->info, 0, sizeof(struct mmVKSamplerCreateInfo));
}

int
mmVKSamplerInfo_Invalid(
    const struct mmVKSamplerInfo*                  p)
{
    return 
        (NULL == p) || 
        (VK_NULL_HANDLE == p->sampler);
}

void
mmVKSamplerInfo_Increase(
    struct mmVKSamplerInfo*                        p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKSamplerInfo_Decrease(
    struct mmVKSamplerInfo*                        p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

void
mmVKSamplerPool_Init(
    struct mmVKSamplerPool*                        p)
{
    mmRbtreeU32Vpt_Init(&p->samplers);
    p->pUploader = NULL;
}

void
mmVKSamplerPool_Destroy(
    struct mmVKSamplerPool*                        p)
{
    assert(0 == p->samplers.size && "assets is not destroy complete.");

    p->pUploader = NULL;
    mmRbtreeU32Vpt_Destroy(&p->samplers);
}

void
mmVKSamplerPool_SetUploader(
    struct mmVKSamplerPool*                        p,
    struct mmVKUploader*                           pUploader)
{
    p->pUploader = pUploader;
}

VkResult
mmVKSamplerPool_PrepareSamplerInfo(
    struct mmVKSamplerPool*                        p,
    const struct mmVKSamplerCreateInfo*            info,
    struct mmVKSamplerInfo*                        pSamplerInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        assert(0 == pSamplerInfo->reference && "reference not zero.");

        err = mmVKUploader_PrepareSampler(p->pUploader, info, &pSamplerInfo->sampler);
        if (VK_SUCCESS != err)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSamplerPool_PrepareSampler failure.", __FUNCTION__, __LINE__);

            pSamplerInfo->sampler = VK_NULL_HANDLE;
            break;
        }

        pSamplerInfo->info = (*info);
        pSamplerInfo->hash = mmVKSamplerCreateInfo_Hash(info);
        pSamplerInfo->reference = 0;

    } while (0);

    return err;
}

void
mmVKSamplerPool_DiscardSamplerInfo(
    struct mmVKSamplerPool*                        p,
    struct mmVKSamplerInfo*                        pSamplerInfo)
{
    assert(0 == pSamplerInfo->reference && "reference not zero.");

    mmVKUploader_DiscardSampler(p->pUploader, pSamplerInfo->sampler);
}

struct mmVKSamplerInfo*
    mmVKSamplerPool_Add(
    struct mmVKSamplerPool*                        p,
    const struct mmVKSamplerCreateInfo*            info)
{
    struct mmVKSamplerInfo* s = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    uint32_t hash = mmVKSamplerCreateInfo_Hash(info);
    it = mmRbtreeU32Vpt_GetIterator(&p->samplers, hash);
    if (NULL != it)
    {
        s = (struct mmVKSamplerInfo*)it->v;
    }
    else
    {
        s = (struct mmVKSamplerInfo*)mmMalloc(sizeof(struct mmVKSamplerInfo));
        mmVKSamplerInfo_Init(s);
        mmRbtreeU32Vpt_Set(&p->samplers, hash, (void*)s);

        mmVKSamplerPool_PrepareSamplerInfo(p, info, s);
    }

    return s;
}

void
mmVKSamplerPool_Remove(
    struct mmVKSamplerPool*                        p,
    uint32_t                                       hash)
{
    struct mmVKSamplerInfo* s = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    it = mmRbtreeU32Vpt_GetIterator(&p->samplers, hash);
    if (NULL != it)
    {
        s = (struct mmVKSamplerInfo*)it->v;
        mmVKSamplerPool_DiscardSamplerInfo(p, s);
        mmRbtreeU32Vpt_Erase(&p->samplers, it);
        mmVKSamplerInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKSamplerPool_Reset(
    struct mmVKSamplerPool*                        p)
{
    struct mmVKSamplerInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    n = mmRb_First(&p->samplers.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKSamplerInfo*)(it->v);
        mmVKSamplerPool_DiscardSamplerInfo(p, s);
        mmRbtreeU32Vpt_Erase(&p->samplers, it);
        mmVKSamplerInfo_Destroy(s);
        mmFree(s);
    }
}
