/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPhysicalDevice.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

VkDeviceSize
mmVKPhysicalDeviceGetHeapSize(
    VkPhysicalDeviceMemoryProperties*              memoryProperties)
{
    VkDeviceSize hHeapSize = 0;
    uint32_t i = 0;
    for (i = 0; i < memoryProperties->memoryHeapCount; ++i)
    {
        hHeapSize += memoryProperties->memoryHeaps[i].size;
    }
    return hHeapSize;
}

ktx_transcode_fmt_e
mmVKPhysicalDeviceGetTranscodefmt(
    VkPhysicalDeviceFeatures*                      features)
{
    if (features->textureCompressionASTC_LDR)
    {
        return KTX_TTF_ASTC_4x4_RGBA;
    }
    else if (features->textureCompressionETC2)
    {
        return KTX_TTF_ETC2_RGBA;
    }
    else if (features->textureCompressionBC)
    {
        return KTX_TTF_BC3_RGBA;
    }
    else
    {
        return KTX_TTF_RGBA32;
    }
}


void
mmVKPhysicalDevice_Reset(
    struct mmVKPhysicalDevice*                     p)
{
    mmMemset(p, 0, sizeof(struct mmVKPhysicalDevice));
}

VkResult
mmVKPhysicalDevice_Create(
    struct mmVKPhysicalDevice*                     p,
    VkInstance                                     instance,
    uint32_t                                       index)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    VkDeviceSize* pHeapSizeArray = NULL;
    VkPhysicalDevice* pPhysicalDeviceArray = NULL;
    
    do
    {
        uint32_t hPhysicalDeviceCount = 0;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (VK_NULL_HANDLE == instance)
        {
            mmLogger_LogE(gLogger, "pInstance is invalid.");
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        err = vkEnumeratePhysicalDevices(instance, &hPhysicalDeviceCount, NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkEnumeratePhysicalDevices failure.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }

        if (0 == hPhysicalDeviceCount)
        {
            mmLogger_LogE(gLogger, "vkEnumeratePhysicalDevices reported zero accessible devices.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            err = VK_ERROR_DEVICE_LOST;
            break;
        }
        
        if (1 == hPhysicalDeviceCount)
        {
            // No choice.
            err = vkEnumeratePhysicalDevices(instance, &hPhysicalDeviceCount, &p->physicalDevice);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkEnumeratePhysicalDevices failure.", __FUNCTION__, __LINE__);
                break;
            }
            p->index = 0;
        }
        else if (-1 != index && index < hPhysicalDeviceCount)
        {
            // Selected a special graphics physical device.
            pPhysicalDeviceArray = (VkPhysicalDevice*)mmMalloc(sizeof(VkPhysicalDevice) * hPhysicalDeviceCount);
            if (NULL == pPhysicalDeviceArray)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " Not enough Memory.", __FUNCTION__, __LINE__);
                err = VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            }
            
            err = vkEnumeratePhysicalDevices(instance, &hPhysicalDeviceCount, pPhysicalDeviceArray);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkEnumeratePhysicalDevices failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_DEVICE_LOST;
                break;
            }
            p->physicalDevice = pPhysicalDeviceArray[index];
            p->index = index;
        }
        else
        {
            static const uint32_t gPhysicalDeviceTypepPriority[5] =
            {
                VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
                VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
                VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU,
                VK_PHYSICAL_DEVICE_TYPE_CPU,
                VK_PHYSICAL_DEVICE_TYPE_OTHER,
            };
            
            uint32_t i = 0;
            
            uint32_t pDeviceTypeCounter[VK_PHYSICAL_DEVICE_TYPE_CPU + 1] = { 0 };
            uint64_t hMaxHeapSize = 0;
            uint32_t hDeviceIndex = 0;
            uint32_t hPhysicalDeviceType = VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
            VkPhysicalDeviceProperties hPhysicalDeviceProperties;
            VkPhysicalDeviceMemoryProperties hPhysicalDeviceMemoryProperties;
            VkPhysicalDeviceType hSearchForDeviceType = VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
            
            pPhysicalDeviceArray = (VkPhysicalDevice*)mmMalloc(sizeof(VkPhysicalDevice) * hPhysicalDeviceCount);
            if (NULL == pPhysicalDeviceArray)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " Not enough Memory.", __FUNCTION__, __LINE__);
                err = VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            }
            
            err = vkEnumeratePhysicalDevices(instance, &hPhysicalDeviceCount, pPhysicalDeviceArray);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkEnumeratePhysicalDevices failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_DEVICE_LOST;
                break;
            }
            
            for (i = 0; i < hPhysicalDeviceCount; i++)
            {
                vkGetPhysicalDeviceProperties(pPhysicalDeviceArray[i], &hPhysicalDeviceProperties);
                assert(hPhysicalDeviceProperties.deviceType <= VK_PHYSICAL_DEVICE_TYPE_CPU);
                pDeviceTypeCounter[hPhysicalDeviceProperties.deviceType]++;
            }
            
            pHeapSizeArray = (VkDeviceSize*)mmMalloc(sizeof(VkDeviceSize) * hPhysicalDeviceCount);
            if (NULL == pHeapSizeArray)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " Not enough Memory.", __FUNCTION__, __LINE__);
                err = VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            }
            
            for (i = 0; i < hPhysicalDeviceCount; i++)
            {
                mmMemset(&hPhysicalDeviceMemoryProperties, 0, sizeof(VkPhysicalDeviceMemoryProperties));
                vkGetPhysicalDeviceMemoryProperties(pPhysicalDeviceArray[i], &hPhysicalDeviceMemoryProperties);
                pHeapSizeArray[i] = mmVKPhysicalDeviceGetHeapSize(&hPhysicalDeviceMemoryProperties);
            }
            
            for (i = 0; i < MM_ARRAY_SIZE(gPhysicalDeviceTypepPriority); i++)
            {
                hPhysicalDeviceType = gPhysicalDeviceTypepPriority[i];
                if (0 < pDeviceTypeCounter[hPhysicalDeviceType])
                {
                    hSearchForDeviceType = hPhysicalDeviceType;
                    break;
                }
            }

            // Use graphics card storage to determine the priority.
            for (i = 0; i < hPhysicalDeviceCount; i++)
            {
                vkGetPhysicalDeviceProperties(pPhysicalDeviceArray[i], &hPhysicalDeviceProperties);
                if (hPhysicalDeviceProperties.deviceType == hSearchForDeviceType)
                {
                    if (hMaxHeapSize < pHeapSizeArray[i])
                    {
                        hMaxHeapSize = pHeapSizeArray[i];
                        hDeviceIndex = i;
                    }
                }
            }
            
            p->physicalDevice = pPhysicalDeviceArray[hDeviceIndex];
            p->index = hDeviceIndex;
        }
        
        // Query physical device properties.
        vkGetPhysicalDeviceProperties(p->physicalDevice, &p->properties);

        // Get Memory information and properties
        vkGetPhysicalDeviceMemoryProperties(p->physicalDevice, &p->memoryProperties);

        // Query fine-grained feature support for this device.
        //  If app has specific feature requirements it should check supported
        //  features based on this query
        vkGetPhysicalDeviceFeatures(p->physicalDevice, &p->features);
        
        // ktx transcodefmt.
        p->transcodefmt = mmVKPhysicalDeviceGetTranscodefmt(&p->features);
        
        err = VK_SUCCESS;
    } while(0);
    
    mmFree(pPhysicalDeviceArray);
    mmFree(pHeapSizeArray);
    
    return err;
}

void
mmVKPhysicalDevice_Delete(
    struct mmVKPhysicalDevice*                     p)
{
    p->physicalDevice = VK_NULL_HANDLE;
}

VkSampleCountFlagBits
mmVKPhysicalDevice_SupportSamples(
    struct mmVKPhysicalDevice*                     p,
    VkSampleCountFlags                             samples)
{
    VkSampleCountFlags hSampleCount = samples;
    VkPhysicalDeviceLimits* limits = &p->properties.limits;
    VkSampleCountFlags c = limits->framebufferColorSampleCounts;
    VkSampleCountFlags d = limits->framebufferDepthSampleCounts;
    do
    {
        if (c & hSampleCount && d & hSampleCount)
        {
            break;
        }

        //VK_SAMPLE_COUNT_1_BIT  = 0x00000001,
        //VK_SAMPLE_COUNT_2_BIT  = 0x00000002,
        //VK_SAMPLE_COUNT_4_BIT  = 0x00000004,
        //VK_SAMPLE_COUNT_8_BIT  = 0x00000008,
        //VK_SAMPLE_COUNT_16_BIT = 0x00000010,
        //VK_SAMPLE_COUNT_32_BIT = 0x00000020,
        //VK_SAMPLE_COUNT_64_BIT = 0x00000040,

        hSampleCount >>= 1;
    } while (VK_SAMPLE_COUNT_1_BIT != hSampleCount);

    return hSampleCount;
}

VkSampleCountFlagBits
mmVKPhysicalDevice_SuitableSamples(
    struct mmVKPhysicalDevice*                     p,
    VkSampleCountFlags                             samples,
    double                                         hDisplayDensity)
{
    if (1.0f < hDisplayDensity)
    {
        return VK_SAMPLE_COUNT_1_BIT;
    }
    else
    {
        return mmVKPhysicalDevice_SupportSamples(p, samples);
    }
}