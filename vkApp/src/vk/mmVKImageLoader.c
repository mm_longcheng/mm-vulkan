/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKImageLoader.h"

#include "vk/mmVKKTXUploader.h"
#include "vk/mmVKImageUploader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFilePath.h"

#include "dish/mmFileContext.h"

ktx_error_code_e
mmVKImageUploader_UploadFromFileContext(
    const char*                                    path,
    ktx_transcode_fmt_e                            transcodefmt,
    struct mmFileContext*                          pFileContext,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;

    do
    {
        const uint8_t* data = NULL;
        size_t size = 0;

        struct mmByteBuffer bytes;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

        if (!mmFileContext_IsFileExists(pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            ktxresult = KTX_FILE_DATA_ERROR;
            break;
        }

        mmFileContext_AcquireFileByteBuffer(pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        ktxresult = mmVKImageUploader_UploadFromNamedMemory(
            path,
            data,
            size,
            0,
            MM_FALSE,
            transcodefmt,
            pUploader,
            pImage);

        mmFileContext_ReleaseFileByteBuffer(pFileContext, &bytes);

        if (KTX_SUCCESS != ktxresult)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageUploader_UploadFromNamedMemory failure.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

    } while (0);

    return ktxresult;
}

void
mmVKImageInfo_Init(
    struct mmVKImageInfo*                          p)
{
    mmString_Init(&p->name);
    mmVKImage_Init(&p->vImage);
    p->imageView = VK_NULL_HANDLE;
    p->reference = 0;
}

void
mmVKImageInfo_Destroy(
    struct mmVKImageInfo*                          p)
{
    p->reference = 0;
    p->imageView = VK_NULL_HANDLE;
    mmVKImage_Destroy(&p->vImage);
    mmString_Destroy(&p->name);
}

int
mmVKImageInfo_Invalid(
    const struct mmVKImageInfo*                    p)
{
    return 
        (NULL           == p              ) || 
        (VK_NULL_HANDLE == p->vImage.image) || 
        (VK_NULL_HANDLE == p->imageView   );
}

void
mmVKImageInfo_Increase(
    struct mmVKImageInfo*                          p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKImageInfo_Decrease(
    struct mmVKImageInfo*                          p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKImageInfo_Prepare(
    struct mmVKImageInfo*                          p,
    VkImageCreateInfo*                             pInfo,
    VmaAllocationCreateFlags                       flags,
    VmaMemoryUsage                                 usage,
    VkImageViewType                                viewType,
    VkImageAspectFlags                             aspectMask,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        err = mmVKUploader_CreateImage(
            pUploader,
            pInfo,
            flags,
            usage,
            viewType,
            &p->vImage);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_CreateImage failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_PrepareImageView(
            pUploader, 
            &p->vImage,
            viewType,
            aspectMask, 
            &p->imageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " mmVKUploader_PrepareImageView failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKImageInfo_Discard(
    struct mmVKImageInfo*                          p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteImage(pUploader, &p->vImage);
    mmVKUploader_DiscardImageView(pUploader, p->imageView);
}

void 
mmVKImageLoader_Init(
    struct mmVKImageLoader*                        p)
{
    p->pFileContext = NULL;
    p->pUploader = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    p->transcodefmt = KTX_TTF_RGBA32;
}

void 
mmVKImageLoader_Destroy(
    struct mmVKImageLoader*                        p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    p->transcodefmt = KTX_TTF_RGBA32;
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pUploader = NULL;
    p->pFileContext = NULL;
}

void 
mmVKImageLoader_SetFileContext(
    struct mmVKImageLoader*                        p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void 
mmVKImageLoader_SetUploader(
    struct mmVKImageLoader*                        p,
    struct mmVKUploader*                           pUploader)
{
    p->pUploader = pUploader;
}

void 
mmVKImageLoader_SetTranscodeFmt(
    struct mmVKImageLoader*                        p,
    ktx_transcode_fmt_e                            transcodefmt)
{
    p->transcodefmt = transcodefmt;
}

VkResult
mmVKImageLoader_PrepareImageInfo(
    struct mmVKImageLoader*                        p,
    const char*                                    path,
    struct mmVKImageInfo*                          pImageInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pImageInfo->reference && "reference not zero.");

        ktxresult = mmVKImageUploader_UploadFromFileContext(
            path,
            p->transcodefmt,
            p->pFileContext,
            p->pUploader,
            &pImageInfo->vImage);
        if (KTX_SUCCESS != ktxresult)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKImageUploader_UploadFromFileContext failure.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKUploader_PrepareImageView(
            p->pUploader,
            &pImageInfo->vImage,
            pImageInfo->vImage.viewType,
            VK_IMAGE_ASPECT_COLOR_BIT,
            &pImageInfo->imageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_PrepareImageView failure.", __FUNCTION__, __LINE__);
            break;
        }

        pImageInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKImageLoader_DiscardImageInfo(
    struct mmVKImageLoader*                        p,
    struct mmVKImageInfo*                          pImageInfo)
{
    assert(0 == pImageInfo->reference && "reference not zero.");
    mmVKUploader_DiscardImageView(p->pUploader, pImageInfo->imageView);
    mmVKUploader_DeleteImage(p->pUploader, &pImageInfo->vImage);
}

void
mmVKImageLoader_UnloadByFile(
    struct mmVKImageLoader*                        p,
    const char*                                    path)
{
    struct mmVKImageInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKImageInfo*)it->v;
        mmVKImageLoader_DiscardImageInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKImageInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKImageLoader_UnloadComplete(
    struct mmVKImageLoader*                        p)
{
    struct mmVKImageInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKImageInfo*)(it->v);
        mmVKImageLoader_DiscardImageInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKImageInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKImageInfo*
mmVKImageLoader_LoadFromFile(
    struct mmVKImageLoader*                        p,
    const char*                                    path)
{
    struct mmVKImageInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKImageInfo*)mmMalloc(sizeof(struct mmVKImageInfo));
        mmVKImageInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKImageLoader_PrepareImageInfo(p, path, s);
    }
    else
    {
        s = (struct mmVKImageInfo*)(it->v);
    }
    return s;
}

struct mmVKImageInfo*
mmVKImageLoader_LoadFromFilePriority(
    struct mmVKImageLoader*                        p,
    const char*                                    path,
    const char*                                    suffix)
{
    struct mmVKImageInfo* s = NULL;
    const char* pPriority = NULL;
    struct mmString hPriorityPath;
    mmString_Init(&hPriorityPath);
    mmPathReplaceSuffix(path, suffix, &hPriorityPath);
    pPriority = mmString_CStr(&hPriorityPath);
    if (mmFileContext_IsFileExists(p->pFileContext, pPriority))
    {
        s = mmVKImageLoader_LoadFromFile(p, pPriority);
    }
    else
    {
        s = mmVKImageLoader_LoadFromFile(p, path);
    }
    mmString_Destroy(&hPriorityPath);
    return s;
}
