/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKRenderPass_h__
#define __mmVKRenderPass_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmVKDevice;
struct mmVKSwapchain;

extern const char* mmVKRenderPassMasterName;
extern const char* mmVKRenderPassDepthName;

struct mmVKRenderPass
{
    // handle.
    VkRenderPass renderPass;
};

void
mmVKRenderPass_Init(
    struct mmVKRenderPass*                         p);

void
mmVKRenderPass_Destroy(
    struct mmVKRenderPass*                         p);

VkResult
mmVKRenderPass_CreateDepth(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    VkFormat                                       depthFormat);

VkResult
mmVKRenderPass_CreateBasicSampled(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain);

VkResult
mmVKRenderPass_CreateMultiSampled(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain);

VkResult
mmVKRenderPass_Create(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain);

void
mmVKRenderPass_Delete(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice);

#include "core/mmSuffix.h"

#endif//__mmVKRenderPass_h__
