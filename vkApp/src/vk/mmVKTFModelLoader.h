/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKTFModelLoader_h__
#define __mmVKTFModelLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"
#include "container/mmRbtsetVpt.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKTFModel.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmVKAssets;
struct mmVKPipelineInfo;
struct mmVKPipelineLoader;

struct mmVKTFModelInfo
{
    // name for model info, read only. 
    // string is weak reference value.
    struct mmString name;

    // RenderPass archive.
    struct mmVKTFModel vModel;

    // reference count. 
    size_t reference;
};

void
mmVKTFModelInfo_Init(
    struct mmVKTFModelInfo*                        p);

void
mmVKTFModelInfo_Destroy(
    struct mmVKTFModelInfo*                        p);

int
mmVKTFModelInfo_Invalid(
    const struct mmVKTFModelInfo*                  p);

void
mmVKTFModelInfo_Increase(
    struct mmVKTFModelInfo*                        p);

void
mmVKTFModelInfo_Decrease(
    struct mmVKTFModelInfo*                        p);

VkResult
mmVKTFModelInfo_Prepare(
    struct mmVKTFModelInfo*                        p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName,
    const char*                                    path);

void
mmVKTFModelInfo_Discard(
    struct mmVKTFModelInfo*                        p,
    struct mmVKAssets*                             pAssets);

struct mmVKTFModelLoader
{
    struct mmVKAssets* pAssets;
    const struct mmVKTFAssetsName* pAssetsName;
    struct mmRbtreeStringVpt sets;
    struct mmRbtsetVpt vPipelineInfos;
};

void
mmVKTFModelLoader_Init(
    struct mmVKTFModelLoader*                      p);

void
mmVKTFModelLoader_Destroy(
    struct mmVKTFModelLoader*                      p);

void
mmVKTFModelLoader_SetAssets(
    struct mmVKTFModelLoader*                      p,
    struct mmVKAssets*                             pAssets);

void
mmVKTFModelLoader_SetAssetsName(
    struct mmVKTFModelLoader*                      p,
    const struct mmVKTFAssetsName*                 pAssetsName);

VkResult
mmVKTFModelLoader_PrepareModelInfo(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path,
    struct mmVKTFModelInfo*                        pModelInfo);

void
mmVKTFModelLoader_DiscardModelInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKTFModelInfo*                        pModelInfo);

void
mmVKTFModelLoader_UnloadByPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path);

void
mmVKTFModelLoader_UnloadComplete(
    struct mmVKTFModelLoader*                      p);

struct mmVKTFModelInfo*
mmVKTFModelLoader_LoadFromPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path);

struct mmVKTFModelInfo*
mmVKTFModelLoader_GetFromPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path);

void
mmVKTFModelLoader_AddPipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineInfo*                       pPipelineInfo);

void
mmVKTFModelLoader_RmvPipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineInfo*                       pPipelineInfo);

void
mmVKTFModelLoader_UnloadCompletePipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineLoader*                     pPipelineLoader);

#include "core/mmSuffix.h"

#endif//__mmVKTFModelLoader_h__
