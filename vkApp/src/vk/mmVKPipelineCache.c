/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPipelineCache.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFilePath.h"

#include "dish/mmFileContext.h"

#include "nwsi/mmPackageAssets.h"

void
mmVKPipelineCache_Init(
    struct mmVKPipelineCache*                      p)
{
    mmString_Init(&p->hCacheName);
    mmString_Init(&p->hCachePath);
    p->pipelineCache = VK_NULL_HANDLE;
}

void
mmVKPipelineCache_Destroy(
    struct mmVKPipelineCache*                      p)
{
    p->pipelineCache = VK_NULL_HANDLE;
    mmString_Destroy(&p->hCachePath);
    mmString_Destroy(&p->hCacheName);
}

void
mmVKPipelineCache_SetCacheName(
    struct mmVKPipelineCache*                      p,
    const char*                                    pPathName)
{
    mmString_Assigns(&p->hCacheName, pPathName);
}

void
mmVKPipelineCache_Save(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice,
    const char*                                    path)
{
    void* data = NULL;

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        size_t size = 0;
        FILE* f = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (VK_NULL_HANDLE == p->pipelineCache)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pipelineCache is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        vkGetPipelineCacheData(device, p->pipelineCache, &size, NULL);
        if (0 == size)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " size is empty.", __FUNCTION__, __LINE__);
            break;
        }

        data = (void*)mmMalloc(size);
        if (NULL == data)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmMalloc failure.", __FUNCTION__, __LINE__);
            break;
        }

        vkGetPipelineCacheData(device, p->pipelineCache, &size, data);

        f = fopen(path, "wb");
        if (NULL == f)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " fopen path: %s failure.", __FUNCTION__, __LINE__, path);
            break;
        }

        fwrite(data, sizeof(char), size, f);
        fclose(f);
    } while (0);

    mmFree(data);
    data = NULL;
}

VkResult
mmVKPipelineCache_Create(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCacheCreateInfo*      info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmByteBuffer bytes;
        const void* buffer = NULL;
        size_t length = 0;

        VkPipelineCacheCreateInfo hPipelineCacheInfo;

        struct mmPackageAssets*  pPackageAssets = info->pPackageAssets;
        struct mmFileContext*      pFileContext = info->pFileContext;
        const char*                   pPathName = info->pPathName;
        const char*                  pCachePath = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        mmString_Assigns(&p->hCacheName, pPathName);
        mmPackageAssets_MakeShaderCachePath(pPackageAssets, pPathName, &p->hCachePath);
        pCachePath = mmString_CStr(&p->hCachePath);

        mmByteBuffer_Reset(&bytes);

        if (!mmFileContext_IsFileExists(pFileContext, pCachePath))
        {
            mmLogger_LogI(gLogger, "%s %d"
                " VKPipelineCache is not exists now.", __FUNCTION__, __LINE__);

            // bytes is not exist.
            buffer = NULL;
            length = 0;

            hPipelineCacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
            hPipelineCacheInfo.pNext = NULL;
            hPipelineCacheInfo.flags = 0;
            hPipelineCacheInfo.initialDataSize = length;
            hPipelineCacheInfo.pInitialData = buffer;

            err = vkCreatePipelineCache(device, &hPipelineCacheInfo, pAllocator, &p->pipelineCache);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d"
                " VKPipelineCache is exists.", __FUNCTION__, __LINE__);

            // Acquire Bytes
            mmFileContext_AcquireFileByteBuffer(pFileContext, pCachePath, &bytes);

            buffer = (const void*)(bytes.buffer + bytes.offset);
            length = (size_t)bytes.length;

            hPipelineCacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
            hPipelineCacheInfo.pNext = NULL;
            hPipelineCacheInfo.flags = 0;
            hPipelineCacheInfo.initialDataSize = length;
            hPipelineCacheInfo.pInitialData = buffer;

            err = vkCreatePipelineCache(device, &hPipelineCacheInfo, pAllocator, &p->pipelineCache);

            // Release Bytes
            mmFileContext_ReleaseFileByteBuffer(pFileContext, &bytes);
        }

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreatePipelineCache failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKPipelineCache_Delete(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        mmVKPipelineCache_Save(p, pDevice, mmString_CStr(&p->hCachePath));

        if (VK_NULL_HANDLE != p->pipelineCache)
        {
            vkDestroyPipelineCache(device, p->pipelineCache, pAllocator);
            p->pipelineCache = VK_NULL_HANDLE;
        }

    } while (0);
}

