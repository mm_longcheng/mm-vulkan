/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKUniformType.h"

#include "core/mmAlloc.h"

void mmUniformVec2Assign(mmUniformVec2 v, const float d[2])
{
    v[0] = d[0];
    v[1] = d[1];
}
void mmUniformVec3Assign(mmUniformVec3 v, const float d[3])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
}
void mmUniformVec4Assign(mmUniformVec4 v, const float d[4])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
    v[3] = d[3];
}

void mmUniformMat2Assign(mmUniformMat2 v, const float d[2][2])
{
    v[0][0] = d[0][0]; v[0][1] = d[0][1];
    v[1][0] = d[1][0]; v[1][1] = d[1][1];
}
void mmUniformMat3Assign(mmUniformMat3 v, const float d[3][3])
{
    v[0][0] = d[0][0]; v[0][1] = d[0][1]; v[0][2] = d[0][2];
    v[1][0] = d[1][0]; v[1][1] = d[1][1]; v[1][2] = d[1][2];
    v[2][0] = d[2][0]; v[2][1] = d[2][1]; v[2][2] = d[2][2];
}
void mmUniformMat4Assign(mmUniformMat4 v, const float d[4][4])
{
    // layout is same.
    mmMemcpy(v, d, sizeof(float) * 16);
}

void mmUniformBVec2Assign(mmUniformBVec2 v, const int d[2])
{
    v[0] = d[0];
    v[1] = d[1];
}
void mmUniformBVec3Assign(mmUniformBVec3 v, const int d[3])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
}
void mmUniformBVec4Assign(mmUniformBVec4 v, const int d[4])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
    v[3] = d[3];
}

void mmUniformIVec2Assign(mmUniformIVec2 v, const int32_t d[2])
{
    v[0] = d[0];
    v[1] = d[1];
}
void mmUniformIVec3Assign(mmUniformIVec3 v, const int32_t d[3])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
}
void mmUniformIVec4Assign(mmUniformIVec4 v, const int32_t d[4])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
    v[3] = d[3];
}

void mmUniformUVec2Assign(mmUniformUVec2 v, const uint32_t d[2])
{
    v[0] = d[0];
    v[1] = d[1];
}
void mmUniformUVec3Assign(mmUniformUVec3 v, const uint32_t d[3])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
}
void mmUniformUVec4Assign(mmUniformUVec4 v, const uint32_t d[4])
{
    v[0] = d[0];
    v[1] = d[1];
    v[2] = d[2];
    v[3] = d[3];
}

