/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTFFileIO.h"

#include "dish/mmFileContext.h"

static
int
mmVKTFFileIO_IsFileExists(
    struct mmGLTFFileIO*                           pSuper,
    const char*                                    path_name)
{
    struct mmVKTFFileIO* p = (struct mmVKTFFileIO*)(pSuper);
    assert(NULL != p->pFileContext && "p->pFileContext is a null.");
    return mmFileContext_IsFileExists(p->pFileContext, path_name);
}

static
void
mmVKTFFileIO_AcquireFileByteBuffer(
    struct mmGLTFFileIO*                           pSuper,
    const char*                                    path_name,
    struct mmByteBuffer*                           byte_buffer)
{
    struct mmVKTFFileIO* p = (struct mmVKTFFileIO*)(pSuper);
    assert(NULL != p->pFileContext && "p->pFileContext is a null.");
    mmFileContext_AcquireFileByteBuffer(p->pFileContext, path_name, byte_buffer);
}

static
void
mmVKTFFileIO_ReleaseFileByteBuffer(
    struct mmGLTFFileIO*                           pSuper,
    struct mmByteBuffer*                           byte_buffer)
{
    struct mmVKTFFileIO* p = (struct mmVKTFFileIO*)(pSuper);
    assert(NULL != p->pFileContext && "p->pFileContext is a null.");
    mmFileContext_ReleaseFileByteBuffer(p->pFileContext, byte_buffer);
}

void
mmVKTFFileIO_Init(
    struct mmVKTFFileIO*                           p)
{
    mmGLTFFileIO_Init(&p->hSuper);
    p->pFileContext = NULL;

    p->hSuper.IsFileExists = &mmVKTFFileIO_IsFileExists;
    p->hSuper.AcquireFileByteBuffer = &mmVKTFFileIO_AcquireFileByteBuffer;
    p->hSuper.ReleaseFileByteBuffer = &mmVKTFFileIO_ReleaseFileByteBuffer;
}
void
mmVKTFFileIO_Destroy(
    struct mmVKTFFileIO*                           p)
{
    p->pFileContext = NULL;
    mmGLTFFileIO_Destroy(&p->hSuper);
}

void
mmVKTFFileIO_SetFileContext(
    struct mmVKTFFileIO*                           p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

