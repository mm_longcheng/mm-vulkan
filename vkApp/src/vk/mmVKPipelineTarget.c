/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKPipelineTarget.h"

#include "vk/mmVKPassLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKPipelineTarget_Init(
    struct mmVKPipelineTarget*                     p)
{
    mmMemset(p, 0, sizeof(struct mmVKPipelineTarget));
}

void
mmVKPipelineTarget_Destroy(
    struct mmVKPipelineTarget*                     p)
{
    mmMemset(p, 0, sizeof(struct mmVKPipelineTarget));
}

VkResult
mmVKPipelineTarget_PrepareFrames(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       frameCount)
{
    VkResult err = VK_ERROR_UNKNOWN;

    size_t sz = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    p->frameCount = frameCount;

    sz = sizeof(struct mmVKPipelineFrame) * frameCount;
    p->frames = (struct mmVKPipelineFrame*)mmMalloc(sz);
    if (NULL == p->frames)
    {
        mmLogger_LogE(gLogger, "%s %d"
            " Not enough Memory.", __FUNCTION__, __LINE__);
        err = VK_ERROR_OUT_OF_HOST_MEMORY;
    }
    else
    {
        err = VK_SUCCESS;
    }

    return err;
}

void
mmVKPipelineTarget_DiscardFrames(
    struct mmVKPipelineTarget*                     p)
{
    if (NULL != p->frames)
    {
        mmFree(p->frames);
        p->frames = NULL;

        p->frameCount = 0;
    }
}

VkResult
mmVKPipelineTarget_PrepareFrameCommands(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    VkCommandPool                                  commandPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i;
        VkCommandBufferAllocateInfo hCmdInfo;
        struct mmVKPipelineFrame* pFrame = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == commandPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " commandPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hCmdInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        hCmdInfo.pNext = NULL;
        hCmdInfo.commandPool = commandPool;
        hCmdInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        hCmdInfo.commandBufferCount = 1;

        for (i = 0; i < p->frameCount; i++)
        {
            pFrame = &p->frames[i];
            err = vkAllocateCommandBuffers(device, &hCmdInfo, &pFrame->cmdBuffer);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkAllocateCommandBuffers failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

    } while (0);

    return err;
}

void
mmVKPipelineTarget_DiscardFrameCommands(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    VkCommandPool                                  commandPool)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i = 0;
        struct mmVKPipelineFrame* pFrame = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        if (VK_NULL_HANDLE == commandPool)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (NULL == p->frames)
        {
            break;
        }

        for (i = 0; i < p->frameCount; i++)
        {
            pFrame = &p->frames[i];
            if (VK_NULL_HANDLE != pFrame->cmdBuffer)
            {
                vkFreeCommandBuffers(device, commandPool, 1, &pFrame->cmdBuffer);
                pFrame->cmdBuffer = VK_NULL_HANDLE;
            }
        }
    } while (0);
}

VkResult
mmVKPipelineTarget_PrepareFramebuffers(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    struct mmVKSwapchain*                          pSwapchain,
    VkRenderPass                                   renderPass)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i;
        int hMultiSampled = 0;
        VkImageView attachments[3];
        VkFramebufferCreateInfo hFramebufferInfo;
        struct mmVKPipelineFrame* pFrame = NULL;
        struct mmVKSwapchainFrame* pSwapchainFrame = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == renderPass)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " renderPass is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hMultiSampled = (VK_SAMPLE_COUNT_1_BIT != pSwapchain->samples);

        hFramebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        hFramebufferInfo.pNext = NULL;
        hFramebufferInfo.flags = 0;
        hFramebufferInfo.renderPass = renderPass;
        hFramebufferInfo.attachmentCount = (2 + hMultiSampled);
        hFramebufferInfo.pAttachments = &attachments[1 - hMultiSampled];
        hFramebufferInfo.width = pSwapchain->windowSize[0];
        hFramebufferInfo.height = pSwapchain->windowSize[1];
        hFramebufferInfo.layers = 1;

        attachments[0] = pSwapchain->color.imageView;
        attachments[2] = pSwapchain->depth.imageView;

        for (i = 0; i < p->frameCount; i++)
        {
            pFrame = &p->frames[i];
            pSwapchainFrame = &pSwapchain->frames[i];
            attachments[1] = pSwapchainFrame->imageView;
            err = vkCreateFramebuffer(device, &hFramebufferInfo, pAllocator, &pFrame->framebuffer);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateFramebuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKPipelineTarget_DiscardFramebuffers(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i = 0;
        struct mmVKPipelineFrame* pFrame = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (NULL == p->frames)
        {
            break;
        }

        for (i = 0; i < p->frameCount; i++)
        {
            pFrame = &p->frames[i];
            if (VK_NULL_HANDLE != pFrame->framebuffer)
            {
                vkDestroyFramebuffer(device, pFrame->framebuffer, pAllocator);
                pFrame->framebuffer = VK_NULL_HANDLE;
            }
        }
    } while (0);
}

VkResult
mmVKPipelineTarget_PrepareSemaphores(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkSemaphoreCreateInfo hSemaphoreInfo;
        VkFenceCreateInfo hFenceInfo;
        uint32_t i = 0;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        // Create semaphores to synchronize acquiring presentable buffers before
        // rendering and waiting for drawing to be complete before presenting
        hSemaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        hSemaphoreInfo.pNext = NULL;
        hSemaphoreInfo.flags = 0;

        // Create fences that we can use to throttle if we get too far
        // ahead of the image presents
        hFenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        hFenceInfo.pNext = NULL;
        hFenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for (i = 0; i < 2; i++)
        {
            err = vkCreateFence(device, &hFenceInfo, pAllocator, &p->fences[i]);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateFence failure.", __FUNCTION__, __LINE__);
                break;
            }

            err = vkCreateSemaphore(device, &hSemaphoreInfo, pAllocator, &p->acquiredSemaphores[i]);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateSemaphore failure.", __FUNCTION__, __LINE__);
                break;
            }

            err = vkCreateSemaphore(device, &hSemaphoreInfo, pAllocator, &p->completeSemaphores[i]);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateSemaphore failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKPipelineTarget_DiscardSemaphores(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i = 0;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        for (i = 0; i < 2; i++)
        {
            if (VK_NULL_HANDLE != p->fences[i])
            {
                vkDestroyFence(device, p->fences[i], pAllocator);
                p->fences[i] = VK_NULL_HANDLE;
            }

            if (VK_NULL_HANDLE != p->acquiredSemaphores[i])
            {
                vkDestroySemaphore(device, p->acquiredSemaphores[i], pAllocator);
                p->acquiredSemaphores[i] = VK_NULL_HANDLE;
            }

            if (VK_NULL_HANDLE != p->completeSemaphores[i])
            {
                vkDestroySemaphore(device, p->completeSemaphores[i], pAllocator);
                p->completeSemaphores[i] = VK_NULL_HANDLE;
            }
        }
    } while (0);
}

VkResult
mmVKPipelineTarget_Create(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineTargetCreateInfo*     info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKSwapchain*    pSwapchain = info->pSwapchain;
        struct mmVKPassInfo*      pPassInfo = info->pPassInfo;
        VkCommandPool           commandPool = info->commandPool;

        VkRenderPass renderPass = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        p->pSwapchain = pSwapchain;
        p->pDevice = pDevice;
        p->commandPool = commandPool;

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (VK_NULL_HANDLE == commandPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " commandPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->pPassInfo = pPassInfo;
        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPassInfo = NULL;
            break;
        }
        mmVKPassInfo_Increase(p->pPassInfo);

        err = mmVKPipelineTarget_PrepareFrames(p, pSwapchain->frameCount);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineTarget_PrepareFrames failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKPipelineTarget_PrepareFrameCommands(p, pDevice, commandPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineTarget_PrepareFrameCommands failure.", __FUNCTION__, __LINE__);
            break;
        }

        renderPass = p->pPassInfo->vRenderPass.renderPass;
        err = mmVKPipelineTarget_PrepareFramebuffers(p, pDevice, pSwapchain, renderPass);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineTarget_PrepareFramebuffers failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKPipelineTarget_PrepareSemaphores(p, pDevice);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKPipelineTarget_PrepareSemaphores failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKPipelineTarget_Delete(
    struct mmVKPipelineTarget*                     p)
{
    do
    {
        struct mmVKDevice* pDevice = p->pDevice;
        VkCommandPool      commandPool = p->commandPool;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (VK_NULL_HANDLE == commandPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " commandPool is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKPipelineTarget_DiscardSemaphores(p, pDevice);
        mmVKPipelineTarget_DiscardFramebuffers(p, pDevice);
        mmVKPipelineTarget_DiscardFrameCommands(p, pDevice, commandPool);
        mmVKPipelineTarget_DiscardFrames(p);

        mmVKPassInfo_Decrease(p->pPassInfo);
        p->pPassInfo = NULL;
    } while (0);
}

VkResult
mmVKPipelineTarget_PrepareSurfaceFrame(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkRenderPass renderPass = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        renderPass = p->pPassInfo->vRenderPass.renderPass;
        err = mmVKPipelineTarget_PrepareFramebuffers(p, p->pDevice, p->pSwapchain, renderPass);
    } while (0);

    return err;
}

void
mmVKPipelineTarget_DiscardSurfaceFrame(
    struct mmVKPipelineTarget*                     p)
{
    mmVKPipelineTarget_DiscardFramebuffers(p, p->pDevice);
}

VkResult
mmVKPipelineTarget_AcquireNextImage(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkSwapchainKHR swapchain = VK_NULL_HANDLE;

        struct mmVKDevice*    pDevice = p->pDevice;
        struct mmVKSwapchain* pSwapchain = p->pSwapchain;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        swapchain = pSwapchain->swapchain;
        if (VK_NULL_HANDLE == swapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " swapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        // Ensure no more than FRAME_LAG renderings are outstanding
        vkWaitForFences(device, 1, &p->fences[p->frameIndex], VK_TRUE, UINT64_MAX);
        vkResetFences(device, 1, &p->fences[p->frameIndex]);

        // Get the index of the next available swapchain image:
        err = pDevice->vkAcquireNextImageKHR(
            device,
            swapchain,
            UINT64_MAX,
            p->acquiredSemaphores[p->frameIndex],
            VK_NULL_HANDLE,
            &p->currentBuffer);
        if (VK_SUCCESS != err && VK_SUBOPTIMAL_KHR != err)
        {
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_QueueSubmit(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        VkPipelineStageFlags hPipelineStageFlags;
        VkSubmitInfo hSubmitInfo;

        struct mmVKDevice*    pDevice = p->pDevice;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        // Wait for the image acquired semaphore to be signaled to ensure
        // that the image won't be rendered to until the presentation
        // engine has fully released ownership to the application, and it is
        // okay to render to the image.
        hPipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

        hSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        hSubmitInfo.pNext = NULL;
        hSubmitInfo.waitSemaphoreCount = 1;
        hSubmitInfo.pWaitSemaphores = &p->acquiredSemaphores[p->frameIndex];
        hSubmitInfo.pWaitDstStageMask = &hPipelineStageFlags;
        hSubmitInfo.commandBufferCount = 1;
        hSubmitInfo.pCommandBuffers = &p->frames[p->currentBuffer].cmdBuffer;
        hSubmitInfo.signalSemaphoreCount = 1;
        hSubmitInfo.pSignalSemaphores = &p->completeSemaphores[p->frameIndex];
        err = vkQueueSubmit(pDevice->graphicsQueue, 1, &hSubmitInfo, p->fences[p->frameIndex]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkQueueSubmit failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_QueuePresent(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkSwapchainKHR swapchain = VK_NULL_HANDLE;

        VkPresentInfoKHR present;

        struct mmVKDevice*    pDevice = p->pDevice;
        struct mmVKSwapchain* pSwapchain = p->pSwapchain;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        swapchain = pSwapchain->swapchain;
        if (VK_NULL_HANDLE == swapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " swapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        // If we are using separate queues we have to wait for image ownership,
        // otherwise wait for draw complete
        present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        present.pNext = NULL;
        present.waitSemaphoreCount = 1;
        present.pWaitSemaphores = &p->completeSemaphores[p->frameIndex];
        present.swapchainCount = 1;
        present.pSwapchains = &swapchain;
        present.pImageIndices = &p->currentBuffer;
        present.pResults = NULL;

        err = pDevice->vkQueuePresentKHR(pDevice->presentQueue, &present);
        p->frameIndex += 1;
        p->frameIndex &= 1;
        if (VK_SUCCESS != err && VK_SUBOPTIMAL_KHR != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkQueuePresentKHR failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKPipelineTarget_WaitIdle(
    struct mmVKPipelineTarget*                     p)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        struct mmVKDevice*    pDevice = p->pDevice;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        vkWaitForFences(device, 2, p->fences, VK_TRUE, UINT64_MAX);
    } while (0);
}

VkResult
mmVKPipelineTarget_CurrentCommandBuffer(
    struct mmVKPipelineTarget*                     p,
    VkCommandBuffer*                               cmdBuffer)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        (*cmdBuffer) = VK_NULL_HANDLE;

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        (*cmdBuffer) = frame->cmdBuffer;

        if (VK_NULL_HANDLE == frame->cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_BeginCommandBuffer(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        VkCommandBufferBeginInfo hCmdBeginInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hCmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        hCmdBeginInfo.pNext = NULL;
        hCmdBeginInfo.flags = 0;
        hCmdBeginInfo.pInheritanceInfo = NULL;

        err = vkBeginCommandBuffer(cmdBuffer, &hCmdBeginInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkBeginCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_EndCommandBuffer(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = vkEndCommandBuffer(cmdBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkEndCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_BeginRenderPass(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       windowRect[4],
    float                                          clearColor[4])
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int hMultiSampled = 0;
        VkClearValue hClearValues[3];

        VkRenderPassBeginInfo hPassBeginInfo;

        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        VkRenderPass renderPass = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        renderPass = p->pPassInfo->vRenderPass.renderPass;
        if (VK_NULL_HANDLE == renderPass)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " renderPass is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hMultiSampled = (VK_SAMPLE_COUNT_1_BIT != p->pSwapchain->samples);

        mmMemcpy(hClearValues[0].color.float32, clearColor, sizeof(float) * 4);
        mmMemcpy(hClearValues[1].color.float32, clearColor, sizeof(float) * 4);
        hClearValues[2].depthStencil.depth = 1.0f;
        hClearValues[2].depthStencil.stencil = 0;

        hPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        hPassBeginInfo.pNext = NULL;
        hPassBeginInfo.renderPass = renderPass;
        hPassBeginInfo.framebuffer = frame->framebuffer;
        hPassBeginInfo.renderArea.offset.x = windowRect[0];
        hPassBeginInfo.renderArea.offset.y = windowRect[1];
        hPassBeginInfo.renderArea.extent.width  = windowRect[2];
        hPassBeginInfo.renderArea.extent.height = windowRect[3];
        hPassBeginInfo.clearValueCount = (2 + hMultiSampled);
        hPassBeginInfo.pClearValues = &hClearValues[1 - hMultiSampled];

        vkCmdBeginRenderPass(cmdBuffer, &hPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_EndRenderPass(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        // Note that ending the renderpass changes the image's layout from
        // COLOR_ATTACHMENT_OPTIMAL to PRESENT_SRC_KHR
        vkCmdEndRenderPass(cmdBuffer);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_SetViewport(
    struct mmVKPipelineTarget*                     p,
    float                                          hViewport[4],
    float                                          hMinDepth,
    float                                          hMaxDepth)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        VkViewport viewport;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        viewport.x = hViewport[0];
        viewport.y = hViewport[1];
        viewport.width  = hViewport[2] - hViewport[0];
        viewport.height = hViewport[3] - hViewport[1];
        viewport.minDepth = hMinDepth;
        viewport.maxDepth = hMaxDepth;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_SetScissor(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       hScissor[4])
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        VkRect2D scissor;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        scissor.offset.x = (int32_t)hScissor[0];
        scissor.offset.y = (int32_t)hScissor[1];
        scissor.extent.width  = (uint32_t)hScissor[2] - hScissor[0];
        scissor.extent.height = (uint32_t)hScissor[3] - hScissor[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        err = VK_SUCCESS;

    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_BeginFrame(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       windowSize[2],
    float                                          clearColor[4])
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkCommandBufferBeginInfo hCmdBeginInfo;
        VkClearValue hClearValues[2];
        VkRenderPassBeginInfo hPassBeginInfo;
        VkViewport viewport;
        VkRect2D scissor;

        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        VkRenderPass renderPass = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPassInfo_Invalid(p->pPassInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPassInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        renderPass = p->pPassInfo->vRenderPass.renderPass;
        if (VK_NULL_HANDLE == renderPass)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " renderPass is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hCmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        hCmdBeginInfo.pNext = NULL;
        hCmdBeginInfo.flags = 0;
        hCmdBeginInfo.pInheritanceInfo = NULL;

        hClearValues[0].color.float32[0] = clearColor[0];
        hClearValues[0].color.float32[1] = clearColor[1];
        hClearValues[0].color.float32[2] = clearColor[2];
        hClearValues[0].color.float32[3] = clearColor[3];
        hClearValues[1].depthStencil.depth = 1.0f;
        hClearValues[1].depthStencil.stencil = 0;

        hPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        hPassBeginInfo.pNext = NULL;
        hPassBeginInfo.renderPass = renderPass;
        hPassBeginInfo.framebuffer = frame->framebuffer;
        hPassBeginInfo.renderArea.offset.x = 0;
        hPassBeginInfo.renderArea.offset.y = 0;
        hPassBeginInfo.renderArea.extent.width  = windowSize[0];
        hPassBeginInfo.renderArea.extent.height = windowSize[1];
        hPassBeginInfo.clearValueCount = 2;
        hPassBeginInfo.pClearValues = hClearValues;

        err = vkBeginCommandBuffer(cmdBuffer, &hCmdBeginInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkBeginCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        vkCmdBeginRenderPass(cmdBuffer, &hPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

        viewport.x = 0;
        viewport.y = 0;
        viewport.width  = (float)windowSize[0];
        viewport.height = (float)windowSize[1];
        viewport.minDepth = (float)0.0f;
        viewport.maxDepth = (float)1.0f;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        scissor.offset.x = 0;
        scissor.offset.y = 0;
        scissor.extent.width  = (uint32_t)windowSize[0];
        scissor.extent.height = (uint32_t)windowSize[1];
        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);
    } while (0);

    return err;
}

VkResult
mmVKPipelineTarget_EndFrame(
    struct mmVKPipelineTarget*                     p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKPipelineFrame* frame = NULL;
        VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " frames is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (p->currentBuffer >= p->frameCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " currentBuffer index is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        frame = &p->frames[p->currentBuffer];
        cmdBuffer = frame->cmdBuffer;

        if (VK_NULL_HANDLE == cmdBuffer)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " cmdBuffer is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        // Note that ending the renderpass changes the image's layout from
        // COLOR_ATTACHMENT_OPTIMAL to PRESENT_SRC_KHR
        vkCmdEndRenderPass(cmdBuffer);

        err = vkEndCommandBuffer(cmdBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkEndCommandBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

