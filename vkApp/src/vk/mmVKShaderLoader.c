/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKShaderLoader.h"

#include "vk/mmVKUploader.h"
#include "vk/mmVKDescriptorLayout.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmBit.h"

#include "container/mmVectorVpt.h"

VkResult
mmVKUploader_PrepareShaderModule(
    struct mmVKUploader*                           pUploader,
    const uint32_t*                                code,
    size_t                                         size,
    VkShaderModule*                                pShader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkShaderModuleCreateInfo hShaderModuleInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (size != mmSizeAlignment(size, 4))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " size must alignment 4.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        pAllocator = pUploader->pAllocator;

        hShaderModuleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        hShaderModuleInfo.pNext = NULL;
        hShaderModuleInfo.flags = 0;
        hShaderModuleInfo.codeSize = size;
        hShaderModuleInfo.pCode = code;

        err = vkCreateShaderModule(device, &hShaderModuleInfo, pAllocator, pShader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateShaderModule failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKUploader_DiscardShaderModule(
    struct mmVKUploader*                           pUploader,
    VkShaderModule                                 shader)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (VK_NULL_HANDLE == shader)
        {
            break;
        }

        pAllocator = pUploader->pAllocator;

        vkDestroyShaderModule(device, shader, pAllocator);
    } while (0);
}

void
mmVKShaderModuleInfo_Init(
    struct mmVKShaderModuleInfo*                   p)
{
    mmString_Init(&p->name);
    p->shaderModule = VK_NULL_HANDLE;
    p->reference = 0;
}

void
mmVKShaderModuleInfo_Destroy(
    struct mmVKShaderModuleInfo*                   p)
{
    p->reference = 0;
    p->shaderModule = VK_NULL_HANDLE;
    mmString_Destroy(&p->name);
}

int
mmVKShaderModuleInfo_Invalid(
    const struct mmVKShaderModuleInfo*             p)
{
    return
        (NULL == p) ||
        (VK_NULL_HANDLE == p->shaderModule);
}

void
mmVKShaderModuleInfo_Increase(
    struct mmVKShaderModuleInfo*                   p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKShaderModuleInfo_Decrease(
    struct mmVKShaderModuleInfo*                   p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

void
mmVKShaderLoader_Init(
    struct mmVKShaderLoader*                       p)
{
    p->pFileContext = NULL;
    p->pUploader = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
}

void
mmVKShaderLoader_Destroy(
    struct mmVKShaderLoader*                       p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pUploader = NULL;
    p->pFileContext = NULL;
}

void
mmVKShaderLoader_SetFileContext(
    struct mmVKShaderLoader*                       p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmVKShaderLoader_SetUploader(
    struct mmVKShaderLoader*                       p,
    struct mmVKUploader*                           pUploader)
{
    p->pUploader = pUploader;
}

VkResult
mmVKShaderLoader_PrepareShaderModuleInfo(
    struct mmVKShaderLoader*                       p,
    const char*                                    path,
    struct mmVKShaderModuleInfo*                   pShaderModuleInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        const uint8_t* data = NULL;
        size_t size = 0;

        struct mmByteBuffer bytes;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pShaderModuleInfo->reference && "pShaderModuleInfo->reference not zero.");

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        err = mmVKUploader_PrepareShaderModule(
            p->pUploader,
            (const uint32_t*)data,
            size,
            &pShaderModuleInfo->shaderModule);

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);

        if (VK_SUCCESS != err)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_PrepareShaderModule failure.", __FUNCTION__, __LINE__);

            pShaderModuleInfo->shaderModule = VK_NULL_HANDLE;
            break;
        }

        pShaderModuleInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKShaderLoader_DiscardShaderModuleInfo(
    struct mmVKShaderLoader*                       p,
    struct mmVKShaderModuleInfo*                   pShaderModuleInfo)
{
    assert(0 == pShaderModuleInfo->reference && "reference not zero.");

    mmVKUploader_DiscardShaderModule(p->pUploader, pShaderModuleInfo->shaderModule);
}

struct mmVKShaderModuleInfo*
mmVKShaderLoader_LoadFromFile(
    struct mmVKShaderLoader*                       p,
    const char*                                    path)
{
    struct mmVKShaderModuleInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKShaderModuleInfo*)mmMalloc(sizeof(struct mmVKShaderModuleInfo));
        mmVKShaderModuleInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKShaderLoader_PrepareShaderModuleInfo(p, path, s);
    }
    else
    {
        s = (struct mmVKShaderModuleInfo*)(it->v);
    }

    return s;
}

void
mmVKShaderLoader_UnloadByFile(
    struct mmVKShaderLoader*                       p,
    const char*                                    path)
{
    struct mmVKShaderModuleInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKShaderModuleInfo*)it->v;
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKShaderLoader_DiscardShaderModuleInfo(p, s);
        mmFree(s);
    }
}

void
mmVKShaderLoader_UnloadComplete(
    struct mmVKShaderLoader*                       p)
{
    struct mmVKShaderModuleInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKShaderModuleInfo*)it->v;
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKShaderLoader_DiscardShaderModuleInfo(p, s);
        mmFree(s);
    }
}

void
mmVKShaderLoaderMakeClassicalStageInfo(
    struct mmVKShaderLoader*                       p,
    const char*                                    spvs[2],
    VkPipelineShaderStageCreateInfo                stages[2])
{
    struct mmVKShaderModuleInfo* smia[2];

    smia[0] = mmVKShaderLoader_LoadFromFile(p, spvs[0]);
    smia[1] = mmVKShaderLoader_LoadFromFile(p, spvs[1]);

    stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stages[0].pNext = NULL;
    stages[0].flags = 0;
    stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    stages[0].module = smia[0]->shaderModule;
    stages[0].pName = "main";
    stages[0].pSpecializationInfo = NULL;

    stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stages[1].pNext = NULL;
    stages[1].flags = 0;
    stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    stages[1].module = smia[1]->shaderModule;
    stages[1].pName = "main";
    stages[1].pSpecializationInfo = NULL;
}

void
mmVKShaderLoaderMakeStageInfo(
    struct mmVKShaderLoader*                       p,
    struct mmVectorValue*                          pssi,
    struct mmVectorValue*                          psmi)
{
    VkPipelineShaderStageCreateInfo* v = NULL;
    struct mmVKPipelineShaderStageInfo* s = NULL;
    struct mmVKShaderModuleInfo* smi = NULL;
    size_t i = 0;
    size_t size = pssi->size;
    mmVectorValue_AllocatorMemory(psmi, size);
    for (i = 0; i < size; ++i)
    {
        v = (VkPipelineShaderStageCreateInfo*)mmVectorValue_At(psmi, i);
        s = (struct mmVKPipelineShaderStageInfo*)mmVectorValue_At(pssi, i);
        smi = mmVKShaderLoader_LoadFromFile(p, mmString_CStr(&s->module));
        v->sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        v->pNext = NULL;
        v->flags = 0;
        v->stage = s->stage;
        v->module = smi->shaderModule;
        v->pName = mmString_CStr(&s->name);
        v->pSpecializationInfo = NULL;
    }
}
