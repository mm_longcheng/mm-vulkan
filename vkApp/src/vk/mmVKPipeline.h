/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPipeline_h__
#define __mmVKPipeline_h__

#include "core/mmCore.h"

#include "container/mmVectorU64.h"
#include "container/mmVectorVpt.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDescriptorLayout.h"

#include "core/mmPrefix.h"


struct mmVKDevice;
struct mmVKShaderLoader;
struct mmVKLayoutInfo;
struct mmVKPassInfo;

struct mmVKPipelineCreateInfo
{
    // pipeline vertex input info.
    const struct mmVKPipelineVertexInputInfo*      pvii;

    // vector<VkPushConstantRange>
    // descriptor set layout pushs.
    const struct mmVectorValue*                    dslp;

    // vector<VkPipelineShaderStageCreateInfo> 
    // pipeline shader module info.
    const struct mmVectorValue*                    psmi;

    // vector<mmVKLayoutInfo*> 
    // descriptor set layout info. 
    const struct mmVectorVpt*                      dsli;

    // Pipeline InputAssembly.
    const struct mmVKPipelineInputAssemblyState*   inputAssembly;

    // Pipeline Tessellation.
    const struct mmVKPipelineTessellationState*    tessellation;

    // Pipeline Rasterization.
    const struct mmVKPipelineRasterizationState*   rasterization;

    // Pipeline ColorBlend.
    const struct mmVKPipelineColorBlendState*      colorBlend;

    // Pipeline DepthStencil.
    const struct mmVKPipelineDepthStencilState*    depthStencil;

    // Pipeline Multisample.
    const struct mmVKPipelineMultisampleState*     multisample;

    // pipeline render pass.
    struct mmVKPassInfo*                           pPassInfo;

    // pipeline cache.
    VkPipelineCache                                pipelineCache;

    // pipeline sample count flag bits.
    VkSampleCountFlagBits                          samples;
};

struct mmVKPipeline
{
    // vector<mmVKLayoutInfo*> 
    // descriptor set layout info. 
    struct mmVectorVpt dsli;

    // pipeline render pass. 
    // reference value.
    struct mmVKPassInfo* pPassInfo;

    // pipeline layout.
    VkPipelineLayout pipelineLayout;

    // pipeline.
    VkPipeline pipeline;
};

void
mmVKPipeline_Init(
    struct mmVKPipeline*                           p);

void
mmVKPipeline_Destroy(
    struct mmVKPipeline*                           p);

VkResult
mmVKPipeline_Create(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCreateInfo*           info);

void
mmVKPipeline_Delete(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKPipeline_PreparePipelineLayout(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVectorValue*                    dslp,
    const struct mmVectorValue*                    dsls);

void
mmVKPipeline_DiscardPipelineLayout(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKPipeline_PreparePipeline(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCreateInfo*           info);

void
mmVKPipeline_DiscardPipeline(
    struct mmVKPipeline*                           p,
    struct mmVKDevice*                             pDevice);

#include "core/mmSuffix.h"

#endif//__mmVKPipeline_h__
