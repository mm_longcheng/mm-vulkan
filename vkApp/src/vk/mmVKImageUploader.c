/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKImageUploader.h"

#include "vk/mmVKKTXUploader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStringUtils.h"
#include "core/mmFilePath.h"

#include "algorithm/mmMurmurHash.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4828)
#endif

#define FREEIMAGE_COLORORDER FREEIMAGE_COLORORDER_RGB

#include "FreeImage.h"

#ifdef _MSC_VER
#pragma warning(pop)
#endif

// freeimage 3.9.1~3.11.0 interoperability fix
#ifndef FREEIMAGE_COLORORDER
// we have freeimage 3.9.1, define these symbols in such way as 3.9.1 really work 
// (do not use 3.11.0 definition, as color order was changed between these two versions on Apple systems)
#define FREEIMAGE_COLORORDER_BGR    0
#define FREEIMAGE_COLORORDER_RGB    1
#if defined(FREEIMAGE_BIGENDIAN)
#define FREEIMAGE_COLORORDER FREEIMAGE_COLORORDER_RGB
#else
#define FREEIMAGE_COLORORDER FREEIMAGE_COLORORDER_BGR
#endif
#endif

int
mmVKImageR8G8B8A8WriteToPNGFile(
    const char*                                    path,
    const unsigned char*                           data,
    int                                            w,
    int                                            h)
{
    BOOL bSuccess = FALSE;
    FIBITMAP* bitmap = NULL;
    do
    {
        BYTE* bits = NULL;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        bitmap = FreeImage_Allocate(w, h, 32, 8, 8, 8);
        if (NULL == bitmap)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " FreeImage_Allocate failed.", __FUNCTION__, __LINE__);
            break;
        }
        bits = FreeImage_GetBits(bitmap);
        mmMemcpy(bits, data, w * h * 4);
        bSuccess = FreeImage_FlipVertical(bitmap);
        if (TRUE != bSuccess)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " FreeImage_FlipVertical failed.", __FUNCTION__, __LINE__);
            break;
        }
        bSuccess = FreeImage_Save(FIF_PNG, bitmap, path, PNG_DEFAULT);
        if (TRUE != bSuccess)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " FreeImage_Save failed. path: %s", __FUNCTION__, __LINE__, path);
            break;
        }
    } while (0);

    if (NULL != bitmap)
    {
        FreeImage_Unload(bitmap);
    }
    return bSuccess;
}

int
mmVKImageGetIsFormatSupported(
    VkPhysicalDevice                               physicalDevice,
    VkFormat                                       format)
{
    VkImageType             imageType   = VK_IMAGE_TYPE_2D;
    VkImageTiling           tiling      = VK_IMAGE_TILING_OPTIMAL;
    VkImageUsageFlags       usageFlags  = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    VkImageCreateFlags      createFlags = 0;
    VkImageFormatProperties imageFormatProperties;

    VkResult vResult = vkGetPhysicalDeviceImageFormatProperties(
        physicalDevice,
        format,
        imageType,
        tiling,
        usageFlags,
        createFlags,
        &imageFormatProperties);

    return (VK_SUCCESS == vResult);
}

typedef struct FIBitmapConverter
{
    VkFormat format;
    FIBITMAP*(*convert)(FIBITMAP*);
} FIBitmapConverter;

typedef struct FIBitmapSupported
{
    VkFormat format;
    FIBITMAP* dib32;
} FIBitmapSupported;

static
FIBITMAP*
FreeImage_ConvertDefault(
    FIBITMAP*                                      dib)
{
    return dib;
}

static
void
mmVKImagePickSupportedFormatConverter(
    VkPhysicalDevice                               physicalDevice,
    FIBitmapSupported*                             bitmap,
    FIBITMAP*                                      dib,
    const FIBitmapConverter*                       converts,
    int                                            n)
{
    const FIBitmapConverter* convert = NULL;
    int i = 0;

    bitmap->format = VK_FORMAT_UNDEFINED;
    bitmap->dib32 = NULL;

    for (i = 0; i < n; ++i)
    {
        convert = &converts[i];

        if (mmVKImageGetIsFormatSupported(physicalDevice, convert->format))
        {
            bitmap->dib32 = (*(convert->convert))(dib);
            bitmap->format = convert->format;
            break;
        }
    }

    if (NULL == bitmap->dib32)
    {
        bitmap->dib32 = FreeImage_ConvertTo32Bits(dib);
        bitmap->format = VK_FORMAT_R8G8B8A8_UNORM;
    }
}

static
void
mmVKImagePickSupportedFormatFallback(
    VkPhysicalDevice                               physicalDevice,
    FIBitmapSupported*                             bitmap,
    FIBITMAP*                                      dib,
    VkFormat                                       format)
{
    if (mmVKImageGetIsFormatSupported(physicalDevice, format))
    {
        bitmap->dib32 = dib;
        bitmap->format = format;
    }
    else
    {
        bitmap->dib32 = FreeImage_ConvertTo32Bits(dib);
        bitmap->format = VK_FORMAT_R8G8B8A8_UNORM;
    }
}

static
void
mmVKImagePickSupportedFormatDefault(
    VkPhysicalDevice                               physicalDevice,
    FIBitmapSupported*                             bitmap,
    FIBITMAP*                                      dib)
{
    bitmap->dib32 = FreeImage_ConvertTo32Bits(dib);
    bitmap->format = VK_FORMAT_R8G8B8A8_UNORM;
}

static
void
mmVKImagePickSupportedFormatR8G8B8A8(
    FIBitmapSupported*                             bitmap,
    FIBITMAP*                                      dib)
{
    bitmap->dib32 = dib;
    bitmap->format = VK_FORMAT_R8G8B8A8_UNORM;
}

static
void
mmVKImagePickSupportedFormatUndefined(
    FIBitmapSupported*                             bitmap)
{
    bitmap->dib32 = NULL;
    bitmap->format = VK_FORMAT_UNDEFINED;
}

static
void
mmVKImageBitmapConvertToSupportedFormat(
    VkPhysicalDevice                               physicalDevice,
    FIBITMAP*                                      dib,
    FIBitmapSupported*                             bitmap)
{
    FREE_IMAGE_TYPE hImageType = FreeImage_GetImageType(dib);

    switch (hImageType)
    {
    case FIT_UNKNOWN:
        mmVKImagePickSupportedFormatUndefined(bitmap);
        break;
    case FIT_BITMAP:
    {
        unsigned hBPP = FreeImage_GetBPP(dib);

        switch (hBPP)
        {
        case 1:
            // PixelFormat.Format1bppIndexed FIT_BITMAP component need normalized.
            // mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R8_UNORM);
            // bbp not image format, may have used palette, we need converter to default.
            mmVKImagePickSupportedFormatDefault(physicalDevice, bitmap, dib);
            break;
        case 4:
            // PixelFormat.Format4bppIndexed FIT_BITMAP component need normalized.
            // mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R8_UNORM);
            // bbp not image format, may have used palette, we need converter to default.
            mmVKImagePickSupportedFormatDefault(physicalDevice, bitmap, dib);
            break;
        case 8:
            // PixelFormat.Format8bppIndexed FIT_BITMAP component need normalized.
            //mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R8_UNORM);
            // bbp not image format, may have used palette, we need converter to default.
            mmVKImagePickSupportedFormatDefault(physicalDevice, bitmap, dib);
            break;
        case 16:
        {
            // PixelFormat.Format16bppRgb565
            static const FIBitmapConverter gConverts[] =
            {
                { VK_FORMAT_R5G6B5_UNORM_PACK16, &FreeImage_ConvertTo16Bits565, },
            };

            mmVKImagePickSupportedFormatConverter(
                physicalDevice,
                bitmap,
                dib,
                gConverts,
                (int)MM_ARRAY_SIZE(gConverts));

            break;
        }
        case 24:
            // PixelFormat.Format24bppRgb
            mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R8G8B8_UNORM);
            break;
        case 32:
            // PixelFormat.Format32bppArgb
            mmVKImagePickSupportedFormatR8G8B8A8(bitmap, dib);
            break;
        default:
            mmVKImagePickSupportedFormatUndefined(bitmap);
            break;
        }

        break;
    }
    case FIT_UINT16:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R16_UINT);
        break;
    case FIT_INT16:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R16_SINT);
        break;
    case FIT_UINT32:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R32_UINT);
        break;
    case FIT_INT32:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R32_SINT);
        break;
    case FIT_FLOAT:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R32_SFLOAT);
        break;
    case FIT_DOUBLE:
    {
        static const FIBitmapConverter gConverts[] =
        {
            {          VK_FORMAT_R64_SFLOAT, &FreeImage_ConvertDefault,  },
            {          VK_FORMAT_R32_SFLOAT, &FreeImage_ConvertToFloat,  },
        };

        mmVKImagePickSupportedFormatConverter(
            physicalDevice,
            bitmap,
            dib,
            gConverts,
            (int)MM_ARRAY_SIZE(gConverts));

        break;
    }
    case FIT_COMPLEX:
    {
        static const FIBitmapConverter gConverts[] =
        {
            {       VK_FORMAT_R32G32_SFLOAT, &FreeImage_ConvertDefault,  },
            {    VK_FORMAT_R32G32B32_SFLOAT, &FreeImage_ConvertToRGBF,   },
            { VK_FORMAT_R32G32B32A32_SFLOAT, &FreeImage_ConvertToRGBAF,  },
            {     VK_FORMAT_R16G16B16_UNORM, &FreeImage_ConvertToRGB16,  },
            {  VK_FORMAT_R16G16B16A16_UNORM, &FreeImage_ConvertToRGBA16, },
        };

        mmVKImagePickSupportedFormatConverter(
            physicalDevice,
            bitmap,
            dib,
            gConverts,
            (int)MM_ARRAY_SIZE(gConverts));

        break;
    }
    case FIT_RGB16:
    {
        static const FIBitmapConverter gConverts[] =
        {
            {     VK_FORMAT_R16G16B16_UNORM, &FreeImage_ConvertDefault,  },
            {  VK_FORMAT_R16G16B16A16_UNORM, &FreeImage_ConvertToRGBA16, },
        };

        mmVKImagePickSupportedFormatConverter(
            physicalDevice,
            bitmap,
            dib,
            gConverts,
            (int)MM_ARRAY_SIZE(gConverts));

        break;
    }
    case FIT_RGBA16:
        mmVKImagePickSupportedFormatFallback(physicalDevice, bitmap, dib, VK_FORMAT_R16G16B16A16_UNORM);
        break;
    case FIT_RGBF:
    {
        static const FIBitmapConverter gConverts[] =
        {
            {    VK_FORMAT_R32G32B32_SFLOAT, &FreeImage_ConvertDefault,  },
            { VK_FORMAT_R32G32B32A32_SFLOAT, &FreeImage_ConvertToRGBAF,  },
            {     VK_FORMAT_R16G16B16_UNORM, &FreeImage_ConvertToRGB16,  },
            {  VK_FORMAT_R16G16B16A16_UNORM, &FreeImage_ConvertToRGBA16, },
        };

        mmVKImagePickSupportedFormatConverter(
            physicalDevice,
            bitmap,
            dib,
            gConverts,
            (int)MM_ARRAY_SIZE(gConverts));

        break;
    }
    case FIT_RGBAF:
    {
        static const FIBitmapConverter gConverts[] =
        {
            { VK_FORMAT_R32G32B32A32_SFLOAT, &FreeImage_ConvertDefault,  },
            {  VK_FORMAT_R16G16B16A16_UNORM, &FreeImage_ConvertToRGBA16, },
        };

        mmVKImagePickSupportedFormatConverter(
            physicalDevice,
            bitmap,
            dib,
            gConverts,
            (int)MM_ARRAY_SIZE(gConverts));

        break;
    }
    default:
        mmVKImagePickSupportedFormatUndefined(bitmap);
        break;
    }
}

typedef struct FreeImageBitmap
{
    ktxTexture2* texture2;
    FIBITMAP* dib;
} FreeImageBitmap;

static
ktx_error_code_e
FreeImageBitMap_CreateFromNamedFile(
    VkPhysicalDevice                               physicalDevice,
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    FreeImageBitmap*                               bitmap)
{
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    FIBITMAP* dib = NULL;
    FIBITMAP* dib32 = NULL;
    BYTE* bits = NULL;
    unsigned int pitch = 0;
    unsigned int width = 0;
    unsigned int height = 0;
    unsigned int dataSize = 0;
    BOOL hFlipResult = FALSE;

    FIBitmapSupported bitmapSupported;

    ktxTextureCreateInfo createInfo;
    ktx_error_code_e result;
    ktxTexture2* prototype;

    //check the file signature and deduce its format
    fif = FreeImage_GetFileType(filename, 0);
    //if still unknown, try to guess the file format from the file extension
    if (fif == FIF_UNKNOWN)
    {
        fif = FreeImage_GetFIFFromFilename(filename);
    }

    //if still unkown, return failure
    if (fif == FIF_UNKNOWN)
    {
        return KTX_UNKNOWN_FILE_FORMAT;
    }

    //check that the plugin has reading capabilities and load the file
    if (!FreeImage_FIFSupportsReading(fif))
    {
        return KTX_UNSUPPORTED_TEXTURE_TYPE;
    }

    // load from file.
    dib = FreeImage_Load(fif, filename, flags);

    //if the image failed to load, return failure
    if (NULL == dib)
    {
        return KTX_FILE_READ_ERROR;
    }

    mmVKImageBitmapConvertToSupportedFormat(
        physicalDevice,
        dib,
        &bitmapSupported);

    dib32 = bitmapSupported.dib32;
    if (dib != dib32)
    {
        FreeImage_Unload(dib);
        dib = NULL;
    }

    if (NULL == dib32)
    {
        return KTX_TRANSCODE_FAILED;
    }

    // upper left maps to s0t0.
    hFlipResult = FreeImage_FlipVertical(dib32);
    if (FALSE == hFlipResult)
    {
        FreeImage_Unload(dib32);
        return KTX_TRANSCODE_FAILED;
    }

    bits = FreeImage_GetBits(dib32);
    pitch = FreeImage_GetPitch(dib32);
    width = FreeImage_GetWidth(dib32);
    height = FreeImage_GetHeight(dib32);
    dataSize = height * pitch;

    createInfo.glInternalformat = 0;
    createInfo.vkFormat = (ktx_uint32_t)bitmapSupported.format;
    createInfo.baseWidth = (ktx_uint32_t)width;
    createInfo.baseHeight = (ktx_uint32_t)height;
    createInfo.baseDepth = (ktx_uint32_t)1;
    createInfo.generateMipmaps = generateMipmaps;
    createInfo.isArray = KTX_FALSE;
    createInfo.numDimensions = 2;
    createInfo.numFaces = 1;
    createInfo.numLayers = 1;
    createInfo.numLevels = 1;
    createInfo.pDfd = NULL;

    result = ktxTexture2_Create(
        &createInfo,
        KTX_TEXTURE_CREATE_NO_STORAGE,
        &prototype);

    if (KTX_SUCCESS != result)
    {
        FreeImage_Unload(dib32);
        return result;
    }

    prototype->pData = (ktx_uint8_t*)bits;
    prototype->dataSize = dataSize;

    bitmap->dib = dib32;
    bitmap->texture2 = prototype;
    return KTX_SUCCESS;
}

static
ktx_error_code_e
FreeImageBitMap_CreateFromMemory(
    VkPhysicalDevice                               physicalDevice,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    FreeImageBitmap*                               bitmap)
{
    FIMEMORY* stream = NULL;
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    FIBITMAP* dib = NULL;
    FIBITMAP* dib32 = NULL;
    BYTE* bits = NULL;
    unsigned int pitch = 0;
    unsigned int width = 0;
    unsigned int height = 0;
    unsigned int dataSize = 0;
    BOOL hFlipResult = FALSE;

    FIBitmapSupported bitmapSupported;

    ktxTextureCreateInfo createInfo;
    ktx_error_code_e result;
    ktxTexture2* prototype;

    stream = FreeImage_OpenMemory((BYTE*)data, (DWORD)size);
    if (NULL == stream)
    {
        return KTX_OUT_OF_MEMORY;
    }

    fif = FreeImage_GetFileTypeFromMemory(stream, (int)size);

    if (fif == FIF_UNKNOWN)
    {
        FreeImage_CloseMemory(stream);
        return KTX_UNKNOWN_FILE_FORMAT;
    }

    //check that the plugin has reading capabilities and load the file
    if (!FreeImage_FIFSupportsReading(fif))
    {
        FreeImage_CloseMemory(stream);
        return KTX_UNSUPPORTED_TEXTURE_TYPE;
    }

    // load from memory.
    dib = FreeImage_LoadFromMemory(fif, stream, flags);
    FreeImage_CloseMemory(stream);
    stream = NULL;

    //if the image failed to load, return failure
    if (NULL == dib)
    {
        return KTX_FILE_READ_ERROR;
    }

    mmVKImageBitmapConvertToSupportedFormat(
        physicalDevice,
        dib,
        &bitmapSupported);

    dib32 = bitmapSupported.dib32;
    if (dib != dib32)
    {
        FreeImage_Unload(dib);
        dib = NULL;
    }

    if (NULL == dib32)
    {
        return KTX_TRANSCODE_FAILED;
    }

    // upper left maps to s0t0.
    hFlipResult = FreeImage_FlipVertical(dib32);
    if (FALSE == hFlipResult)
    {
        FreeImage_Unload(dib32);
        return KTX_TRANSCODE_FAILED;
    }

    bits = FreeImage_GetBits(dib32);
    pitch = FreeImage_GetPitch(dib32);
    width = FreeImage_GetWidth(dib32);
    height = FreeImage_GetHeight(dib32);
    dataSize = height * pitch;

    createInfo.glInternalformat = 0;
    createInfo.vkFormat = (ktx_uint32_t)bitmapSupported.format;
    createInfo.baseWidth = (ktx_uint32_t)width;
    createInfo.baseHeight = (ktx_uint32_t)height;
    createInfo.baseDepth = (ktx_uint32_t)1;
    createInfo.generateMipmaps = generateMipmaps;
    createInfo.isArray = KTX_FALSE;
    createInfo.numDimensions = 2;
    createInfo.numFaces = 1;
    createInfo.numLayers = 1;
    createInfo.numLevels = 1;
    createInfo.pDfd = NULL;

    result = ktxTexture2_Create(
        &createInfo,
        KTX_TEXTURE_CREATE_NO_STORAGE,
        &prototype);

    if (KTX_SUCCESS != result)
    {
        FreeImage_Unload(dib32);
        return result;
    }

    prototype->pData = (ktx_uint8_t*)bits;
    prototype->dataSize = dataSize;

    bitmap->dib = dib32;
    bitmap->texture2 = prototype;
    return KTX_SUCCESS;
}

static
void
FreeImageBitMap_Destroy(
    FreeImageBitmap*                               p)
{
    if (NULL != p->texture2)
    {
        p->texture2->pData = NULL;
        p->texture2->dataSize = 0;
        ktxTexture_Destroy((ktxTexture*)p->texture2);
        p->texture2 = NULL;
    }
    if (NULL != p->dib)
    {
        FreeImage_Unload(p->dib);
        p->dib = NULL;
    }
}

static
ktx_error_code_e
mmVKKTXUploader_UploadKTX1FromNamedFile(
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture1* kTexture1 = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        ktxresult = ktxTexture1_CreateFromNamedFile(
            filename,
            KTX_TEXTURE_CREATE_NO_FLAGS,
            &kTexture1);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture1,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

    } while (0);

    if (NULL != kTexture1)
    {
        ktxTexture_Destroy((ktxTexture*)kTexture1);
    }

    return ktxresult;
}

static
ktx_error_code_e
mmVKKTXUploader_UploadKTX2FromNamedFile(
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture2* kTexture2 = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        ktxresult = ktxTexture2_CreateFromNamedFile(
            filename,
            KTX_TEXTURE_CREATE_NO_FLAGS,
            &kTexture2);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

        if (ktxTexture2_NeedsTranscoding(kTexture2))
        {
            ktxresult = ktxTexture2_TranscodeBasis(kTexture2, tf, 0);
            if (KTX_SUCCESS != ktxresult)
            {
                const char* pErrorString = ktxErrorString(ktxresult);
                mmLogger_LogE(gLogger, "Transcode of ktxTexture from %s failed: %s.",
                    filename, pErrorString);
                break;
            }
        }

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture2,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

    } while (0);

    if (NULL != kTexture2)
    {
        ktxTexture_Destroy((ktxTexture*)kTexture2);
    }

    return ktxresult;
}

static
ktx_error_code_e
mmVKKTXUploader_UploadRGBAFromNamedFile(
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture2* kTexture2 = NULL;
    FreeImageBitmap bitmap = { NULL, NULL };

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

        ktxresult = FreeImageBitMap_CreateFromNamedFile(
            pUploader->physicalDevice,
            filename,
            0,
            generateMipmaps,
            &bitmap);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

        kTexture2 = bitmap.texture2;

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture2,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                filename, pErrorString);
            break;
        }

    } while (0);

    FreeImageBitMap_Destroy(&bitmap);

    return ktxresult;
}

static
ktx_error_code_e
mmVKKTXUploader_UploadKTX1FromNamedMemory(
    const char*                                    name,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture1* kTexture1 = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        ktxresult = ktxTexture1_CreateFromMemory(
            (const ktx_uint8_t*)data,
            (ktx_size_t)size,
            KTX_TEXTURE_CREATE_NO_FLAGS,
            &kTexture1);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture1,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

    } while (0);

    if (NULL != kTexture1)
    {
        ktxTexture_Destroy((ktxTexture*)kTexture1);
    }

    return ktxresult;
}

static
ktx_error_code_e
mmVKKTXUploader_UploadKTX2FromNamedMemory(
    const char*                                    name,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture2* kTexture2 = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        ktxresult = ktxTexture2_CreateFromMemory(
            (const ktx_uint8_t*)data,
            (ktx_size_t)size,
            KTX_TEXTURE_CREATE_NO_FLAGS,
            &kTexture2);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

        if (ktxTexture2_NeedsTranscoding(kTexture2))
        {
            ktxresult = ktxTexture2_TranscodeBasis(kTexture2, tf, 0);
            if (KTX_SUCCESS != ktxresult)
            {
                const char* pErrorString = ktxErrorString(ktxresult);
                mmLogger_LogE(gLogger, "Transcode of ktxTexture from %s failed: %s.",
                    name, pErrorString);
                break;
            }
        }

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture2,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

    } while (0);

    if (NULL != kTexture2)
    {
        ktxTexture_Destroy((ktxTexture*)kTexture2);
    }

    return ktxresult;
}

static
ktx_error_code_e
mmVKKTXUploader_UploadRGBAFromNamedMemory(
    const char*                                    name,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture2* kTexture2 = NULL;
    FreeImageBitmap bitmap = { NULL, NULL };

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            ktxresult = KTX_INVALID_VALUE;
            break;
        }

        ktxresult = FreeImageBitMap_CreateFromMemory(
            pUploader->physicalDevice,
            data,
            size,
            0,
            generateMipmaps,
            &bitmap);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

        kTexture2 = bitmap.texture2;

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture2,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                name, pErrorString);
            break;
        }

    } while (0);

    FreeImageBitMap_Destroy(&bitmap);

    return ktxresult;
}

ktx_error_code_e
mmVKImageUploader_UploadFromNamedFile(
    const char*                                    filename,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    const char* extension = mmPathFileExtension(filename);

    mmUInt32_t h;
    char lower[64] = { 0 };
    size_t len = strlen(extension);
    len = mmCStringTolower(extension, len, lower, 63);
    h = (mmUInt32_t)mmMurmurHash2((const void*)lower, (int)len, 0);

    switch (h)
    {
    case 0xA0897147:
        // 0xA0897147 ktx
        ktxresult = mmVKKTXUploader_UploadKTX1FromNamedFile(
            filename, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    case 0x029A8926:
        // 0x029A8926 ktx2
        ktxresult = mmVKKTXUploader_UploadKTX2FromNamedFile(
            filename, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    default:
        ktxresult = mmVKKTXUploader_UploadRGBAFromNamedFile(
            filename, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    }

    return ktxresult;
}

ktx_error_code_e
mmVKImageUploader_UploadFromNamedMemory(
    const char*                                    filename,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    const char* extension = mmPathFileExtension(filename);

    mmUInt32_t h;
    char lower[64] = { 0 };
    size_t len = strlen(extension);
    len = mmCStringTolower(extension, len, lower, 63);
    h = (mmUInt32_t)mmMurmurHash2((const void*)lower, (int)len, 0);

    switch (h)
    {
    case 0xA0897147:
        // 0xA0897147 ktx
        ktxresult = mmVKKTXUploader_UploadKTX1FromNamedMemory(
            filename, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    case 0x029A8926:
        // 0x029A8926 ktx2
        ktxresult = mmVKKTXUploader_UploadKTX2FromNamedMemory(
            filename, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    default:
        ktxresult = mmVKKTXUploader_UploadRGBAFromNamedMemory(
            filename, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    }

    return ktxresult;
}

extern
ktx_error_code_e
mmVKImageUploader_UploadFromMimeTypeMemory(
    const char*                                    mimeType,
    const uint8_t*                                 data,
    size_t                                         size,
    int                                            flags,
    int                                            generateMipmaps,
    ktx_transcode_fmt_e                            tf,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;

    mmUInt32_t h;
    char lower[64] = { 0 };
    size_t len = strlen(mimeType);
    len = mmCStringTolower(mimeType, len, lower, 63);
    h = (mmUInt32_t)mmMurmurHash2((const void*)lower, (int)len, 0);

    switch (h)
    {
    case 0x46F02A48:
        // 0x46F02A48 image/ktx
        ktxresult = mmVKKTXUploader_UploadKTX1FromNamedMemory(
            mimeType, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    case 0xAA77D79F:
        // 0xAA77D79F image/ktx2
        ktxresult = mmVKKTXUploader_UploadKTX2FromNamedMemory(
            mimeType, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    default:
        ktxresult = mmVKKTXUploader_UploadRGBAFromNamedMemory(
            mimeType, data, size, flags, generateMipmaps, tf, pUploader, pImage);
        break;
    }

    return ktxresult;
}

ktx_error_code_e
mmVKImageUploader_UploadFromR8G8B8A8Memory(
    const uint8_t*                                 data,
    size_t                                         size,
    uint32_t                                       width,
    uint32_t                                       height,
    int                                            flags,
    int                                            generateMipmaps,
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage)
{
    ktx_error_code_e ktxresult = KTX_INVALID_OPERATION;
    ktxTexture2* kTexture2 = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        ktxTextureCreateInfo createInfo;

        createInfo.glInternalformat = 0;
        createInfo.vkFormat = (ktx_uint32_t)VK_FORMAT_R8G8B8A8_UNORM;
        createInfo.baseWidth = (ktx_uint32_t)width;
        createInfo.baseHeight = (ktx_uint32_t)height;
        createInfo.baseDepth = (ktx_uint32_t)1;
        createInfo.generateMipmaps = generateMipmaps;
        createInfo.isArray = KTX_FALSE;
        createInfo.numDimensions = 2;
        createInfo.numFaces = 1;
        createInfo.numLayers = 1;
        createInfo.numLevels = 1;
        createInfo.pDfd = NULL;

        ktxresult = ktxTexture2_Create(
            &createInfo,
            KTX_TEXTURE_CREATE_NO_STORAGE,
            &kTexture2);

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            char hFormatName[64] = { 0 };
            sprintf(hFormatName, "%u x %u", width, height);
            mmLogger_LogE(gLogger, "Creation of ktxTexture from %s failed: %s.",
                hFormatName, pErrorString);
            break;
        }

        kTexture2->pData = (ktx_uint8_t*)data;
        kTexture2->dataSize = size;

        ktxresult = mmVKKTXUploader_VkUploadEx(
            (ktxTexture*)kTexture2,
            pUploader,
            pImage,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        kTexture2->pData = (ktx_uint8_t*)NULL;
        kTexture2->dataSize = 0;

        if (KTX_SUCCESS != ktxresult)
        {
            const char* pErrorString = ktxErrorString(ktxresult);
            char hFormatName[64] = { 0 };
            sprintf(hFormatName, "%u x %u", width, height);
            mmLogger_LogE(gLogger, "Upload of ktxTexture from %s failed: %s.",
                hFormatName, pErrorString);
            break;
        }
    } while (0);

    if (NULL != kTexture2)
    {
        ktxTexture_Destroy((ktxTexture*)kTexture2);
    }

    return ktxresult;
}

void
mmVKImageUploader_Prepare(void)
{
    FreeImage_Initialise(FALSE);
}

void
mmVKImageUploader_Discard(void)
{
    FreeImage_DeInitialise();
}

