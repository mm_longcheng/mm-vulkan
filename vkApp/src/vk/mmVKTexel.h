/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKTexel_h__
#define __mmVKTexel_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKBuffer.h"
#include "vk/mmVKImage.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKUploader;

struct mmVKTexelCreateInfo
{
    VkFormat format;
    VkImageType imageType;
    VkImageViewType viewType;
    uint32_t extent[3];
    uint32_t mipLevels;
    uint32_t arrayLayers;
    VkSampleCountFlagBits samples;
    VkImageUsageFlags usage;
    VkImageAspectFlagBits aspectMask;
    VmaAllocationCreateFlags vmaFlags;
    VmaMemoryUsage vmaUsage;
};

struct mmVKTexel
{
    struct mmVKBuffer vBuffer;
    struct mmVKImage vImage;
    VkImageView imageView;
};

void
mmVKTexel_Init(
    struct mmVKTexel*                              p);

void
mmVKTexel_Destroy(
    struct mmVKTexel*                              p);

int
mmVKTexel_Invalid(
    const struct mmVKTexel*                        p);

VkResult
mmVKTexel_Prepare(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader,
    const uint8_t*                                 buffer,
    size_t                                         size,
    struct mmVKTexelCreateInfo*                    info);

void
mmVKTexel_Discard(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTexel_UpdateBuffer(
    struct mmVKTexel*                              p,
    struct mmVKUploader*                           pUploader,
    const uint8_t*                                 buffer,
    size_t                                         size);

#include "core/mmSuffix.h"

#endif//__mmVKTexel_h__
