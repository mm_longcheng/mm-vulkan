/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKMacro_h__
#define __mmVKMacro_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

#define mmVKDefineInstanceProcAddr(pEntryPoint) PFN_vk##pEntryPoint vk##pEntryPoint
#define mmVKInstanceProcAddrAssignNull(pEntryPoint) p->vk##pEntryPoint = NULL
#define mmVKGetInstanceProcAddr(p, pInstance, pEntryPoint)                                     \
{                                                                                                  \
    p->vk##pEntryPoint = (PFN_vk##pEntryPoint)vkGetInstanceProcAddr(pInstance, "vk" #pEntryPoint); \
    assert(NULL != p->vk##pEntryPoint);                                                            \
}


#define mmVKDefineDeviceProcAddr(pEntryPoint) PFN_vk##pEntryPoint vk##pEntryPoint
#define mmVKDeviceProcAddrAssignNull(pEntryPoint) p->vk##pEntryPoint = NULL
#define mmVKGetDeviceProcAddr(p, d, pDevice, pEntryPoint)                                       \
{                                                                                                   \
    d->vk##pEntryPoint = (PFN_vk##pEntryPoint)p->vkGetDeviceProcAddr(pDevice, "vk" #pEntryPoint);   \
    assert(NULL != d->vk##pEntryPoint);                                                             \
}

// Macro to check and display Vulkan return results.
// Use when the only possible errors are caused by invalid usage by this loader.
#if defined(_DEBUG)
#define mmVKCheckResult(f)                                                  \
{                                                                           \
    VkResult res = (f);                                                     \
    if (res != VK_SUCCESS)                                                  \
    {                                                                       \
        fprintf(stderr, "Fatal error in : "                                 \
                "VkResult is \"%d\" in %s at line %d\n",                    \
                res, __FILE__, __LINE__);                                   \
        assert(res == VK_SUCCESS);                                          \
    }                                                                       \
}
#else
#define mmVKCheckResult(f) ((void)f)
#endif

#include "core/mmSuffix.h"

#endif//__mmVKMacro_h__
