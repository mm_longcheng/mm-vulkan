/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPipelineCache_h__
#define __mmVKPipelineCache_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDevice.h"

#include "core/mmPrefix.h"

struct mmFileContext;
struct mmPackageAssets;

struct mmVKPipelineCacheCreateInfo
{
    struct mmPackageAssets*                        pPackageAssets;
    struct mmFileContext*                          pFileContext;
    const char*                                    pPathName;
};

struct mmVKPipelineCache
{
    // cache name.
    struct mmString hCacheName;

    // cache path.
    struct mmString hCachePath;

    // handle.
    VkPipelineCache pipelineCache;
};

void
mmVKPipelineCache_Init(
    struct mmVKPipelineCache*                      p);

void
mmVKPipelineCache_Destroy(
    struct mmVKPipelineCache*                      p);

void
mmVKPipelineCache_SetCacheName(
    struct mmVKPipelineCache*                      p,
    const char*                                    pPathName);

void
mmVKPipelineCache_Save(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice,
    const char*                                    path);

VkResult
mmVKPipelineCache_Create(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineCacheCreateInfo*      info);

void
mmVKPipelineCache_Delete(
    struct mmVKPipelineCache*                      p,
    struct mmVKDevice*                             pDevice);

#include "core/mmSuffix.h"

#endif//__mmVKPipelineCache_h__
