/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKUploader.h"

#include "vk/mmVKMacro.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKUploader_Init(
    struct mmVKUploader*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKUploader));
}

void
mmVKUploader_Destroy(
    struct mmVKUploader*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKUploader));
}

VkResult
mmVKUploader_Create(
    struct mmVKUploader*                           p,
    const struct mmVKUploaderCreateInfo*           info,
    const VkAllocationCallbacks*                   pAllocator)
{
    VkResult result;

    do
    {
        VkCommandBufferAllocateInfo hCmdBufInfo;
        VkFenceCreateInfo hFenceInfo;

        p->vmaAllocator = info->vmaAllocator;
        p->physicalDevice = info->physicalDevice;
        p->device = info->device;
        p->graphicsQueue = info->graphicsQueue;
        p->transferQueue = info->transferQueue;
        p->graphicsCmdPool = info->graphicsCmdPool;
        p->transferCmdPool = info->transferCmdPool;
        p->pAllocator = pAllocator;

        // Use a separate command buffer for texture loading. Needed for
        // submitting image barriers and converting tilings.
        hCmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        hCmdBufInfo.pNext = NULL;
        hCmdBufInfo.commandPool = p->graphicsCmdPool;
        hCmdBufInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        hCmdBufInfo.commandBufferCount = 1;
        result = vkAllocateCommandBuffers(p->device, &hCmdBufInfo, &p->graphicsCmdBuffer);
        if (VK_SUCCESS != result)
        {
            break;
        }

        hCmdBufInfo.commandPool = p->transferCmdPool;
        result = vkAllocateCommandBuffers(p->device, &hCmdBufInfo, &p->transferCmdBuffer);
        if (VK_SUCCESS != result)
        {
            break;
        }

        hFenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        hFenceInfo.pNext = NULL;
        hFenceInfo.flags = 0;
        result = vkCreateFence(p->device, &hFenceInfo, pAllocator, &p->copyFence);
        if (VK_SUCCESS != result)
        {
            break;
        }

        p->graphicsSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        p->graphicsSubmitInfo.pNext = NULL;
        p->graphicsSubmitInfo.waitSemaphoreCount = 0;
        p->graphicsSubmitInfo.pWaitSemaphores = NULL;
        p->graphicsSubmitInfo.pWaitDstStageMask = NULL;
        p->graphicsSubmitInfo.commandBufferCount = 1;
        p->graphicsSubmitInfo.pCommandBuffers = &p->graphicsCmdBuffer;
        p->graphicsSubmitInfo.signalSemaphoreCount = 0;
        p->graphicsSubmitInfo.pSignalSemaphores = NULL;

        p->transferSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        p->transferSubmitInfo.pNext = NULL;
        p->transferSubmitInfo.waitSemaphoreCount = 0;
        p->transferSubmitInfo.pWaitSemaphores = NULL;
        p->transferSubmitInfo.pWaitDstStageMask = NULL;
        p->transferSubmitInfo.commandBufferCount = 1;
        p->transferSubmitInfo.pCommandBuffers = &p->transferCmdBuffer;
        p->transferSubmitInfo.signalSemaphoreCount = 0;
        p->transferSubmitInfo.pSignalSemaphores = NULL;

    } while (0);

    return result;
}

void
mmVKUploader_Delete(
    struct mmVKUploader*                           p)
{
    if (VK_NULL_HANDLE != p->copyFence)
    {
        vkDestroyFence(p->device, p->copyFence, p->pAllocator);
        p->copyFence = VK_NULL_HANDLE;
    }
    if (VK_NULL_HANDLE != p->transferCmdBuffer)
    {
        vkFreeCommandBuffers(p->device, p->transferCmdPool, 1, &p->transferCmdBuffer);
        p->transferCmdBuffer = VK_NULL_HANDLE;
    }
    if (VK_NULL_HANDLE != p->graphicsCmdBuffer)
    {
        vkFreeCommandBuffers(p->device, p->graphicsCmdPool, 1, &p->graphicsCmdBuffer);
        p->graphicsCmdBuffer = VK_NULL_HANDLE;
    }
}

VkResult
mmVKUploader_GraphicsCommandBufferBegin(
    struct mmVKUploader*                           p)
{
    VkCommandBufferBeginInfo hCmdBeginInfo;
    hCmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    hCmdBeginInfo.pNext = NULL;
    hCmdBeginInfo.flags = 0;
    hCmdBeginInfo.pInheritanceInfo = NULL;
    return vkBeginCommandBuffer(p->graphicsCmdBuffer, &hCmdBeginInfo);
}

VkResult
mmVKUploader_TransferCommandBufferBegin(
    struct mmVKUploader*                           p)
{
    VkCommandBufferBeginInfo hCmdBeginInfo;
    hCmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    hCmdBeginInfo.pNext = NULL;
    hCmdBeginInfo.flags = 0;
    hCmdBeginInfo.pInheritanceInfo = NULL;
    return vkBeginCommandBuffer(p->transferCmdBuffer, &hCmdBeginInfo);
}

VkResult
mmVKUploader_GraphicsCommandBufferEnd(
    struct mmVKUploader*                           p)
{
    return vkEndCommandBuffer(p->graphicsCmdBuffer);
}

VkResult
mmVKUploader_TransferCommandBufferEnd(
    struct mmVKUploader*                           p)
{
    return vkEndCommandBuffer(p->transferCmdBuffer);
}

VkResult
mmVKUploader_GraphicsQueueSubmitWait(
    struct mmVKUploader*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    do
    {
        err = vkResetFences(p->device, 1, &p->copyFence);
        if (VK_SUCCESS != err)
        {
            break;
        }

        err = vkQueueSubmit(p->graphicsQueue, 1, &p->graphicsSubmitInfo, p->copyFence);
        if (VK_SUCCESS != err)
        {
            break;
        }

        err = vkWaitForFences(p->device, 1, &p->copyFence, VK_TRUE, 100000000000);
        if (VK_SUCCESS != err)
        {
            break;
        }
    } while (0);
    return err;
}

VkResult
mmVKUploader_TransferQueueSubmitWait(
    struct mmVKUploader*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    do
    {
        err = vkResetFences(p->device, 1, &p->copyFence);
        if (VK_SUCCESS != err)
        {
            break;
        }

        err = vkQueueSubmit(p->transferQueue, 1, &p->transferSubmitInfo, p->copyFence);
        if (VK_SUCCESS != err)
        {
            break;
        }

        err = vkWaitForFences(p->device, 1, &p->copyFence, VK_TRUE, 100000000000);
        if (VK_SUCCESS != err)
        {
            break;
        }
    } while (0);
    return err;
}

VkResult
mmVKUploader_CreateGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    const VkBufferCreateInfo*                      buffInfo,
    VmaAllocationCreateFlags                       flags,
    struct mmVKBuffer*                             pBuffer)
{
    VkResult result = VK_ERROR_UNKNOWN;

    VkBufferCreateInfo hDstCreateInfo;
    VmaAllocationCreateInfo hVmaAllocInfo;

    assert(VK_NULL_HANDLE == pBuffer->buffer && VK_NULL_HANDLE == pBuffer->allocation && "Buffer not empty.");

    mmMemset(&hVmaAllocInfo, 0, sizeof(VmaAllocationCreateInfo));

    hDstCreateInfo = *buffInfo;
    hDstCreateInfo.usage = buffInfo->usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

    hVmaAllocInfo.flags = flags;
    hVmaAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    result = vmaCreateBuffer(
        p->vmaAllocator,
        &hDstCreateInfo,
        &hVmaAllocInfo,
        &pBuffer->buffer,
        &pBuffer->allocation,
        NULL);

    return result;
}

VkResult
mmVKUploader_UpdateGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset)
{
    VkResult result = VK_ERROR_UNKNOWN;
    struct mmVKBuffer stage;

    do
    {
        VkBufferCreateInfo hSrcCreateInfo;
        VmaAllocationCreateInfo hVmaAllocInfo;
        VkBufferCopy hBufferCopy;
        void* pMappedBuffer = NULL;

        mmMemset(&hVmaAllocInfo, 0, sizeof(VmaAllocationCreateInfo));

        result = mmVKUploader_TransferCommandBufferBegin(p);
        if (VK_SUCCESS != result)
        {
            break;
        }

        hSrcCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hSrcCreateInfo.pNext = NULL;
        hSrcCreateInfo.flags = 0;
        hSrcCreateInfo.size = length;
        hSrcCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        hSrcCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hSrcCreateInfo.queueFamilyIndexCount = 0;
        hSrcCreateInfo.pQueueFamilyIndices = NULL;

        hVmaAllocInfo.flags = 0;
        hVmaAllocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        result = vmaCreateBuffer(
            p->vmaAllocator,
            &hSrcCreateInfo,
            &hVmaAllocInfo,
            &stage.buffer,
            &stage.allocation,
            NULL);
        if (VK_SUCCESS != result)
        {
            break;
        }

        result = vmaMapMemory(
            p->vmaAllocator, 
            stage.allocation, 
            (void**)&pMappedBuffer);
        if (VK_SUCCESS != result)
        {
            break;
        }

        mmMemcpy(pMappedBuffer, buffer + offset, length);
        vmaUnmapMemory(p->vmaAllocator, stage.allocation);

        hBufferCopy.srcOffset = 0;
        hBufferCopy.dstOffset = vOffset;
        hBufferCopy.size = length;
        vkCmdCopyBuffer(p->transferCmdBuffer, stage.buffer, pBuffer->buffer, 1, &hBufferCopy);

        result = mmVKUploader_TransferCommandBufferEnd(p);
        if (VK_SUCCESS != result)
        {
            break;
        }

        result = mmVKUploader_TransferQueueSubmitWait(p);

    } while (0);

    if (VK_NULL_HANDLE != stage.buffer && NULL != stage.allocation)
    {
        vmaDestroyBuffer(p->vmaAllocator, stage.buffer, stage.allocation);
    }

    return result;
}

VkResult
mmVKUploader_UploadGPUOnlyBuffer(
    struct mmVKUploader*                           p,
    VkBufferUsageFlags                             usage,
    VmaAllocationCreateFlags                       flags,
    const uint8_t*                                 buffer,
    size_t                                         length,
    struct mmVKBuffer*                             pBuffer)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        VkBufferCreateInfo hBufferInfo;

        // UBOAnimation
        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = (VkDeviceSize)length;
        hBufferInfo.usage = usage;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateGPUOnlyBuffer(
            p,
            &hBufferInfo,
            flags,
            pBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateGPUOnlyBuffer(
            p,
            (uint8_t*)(buffer),
            0,
            length,
            pBuffer,
            0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdataGPUOnlyBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKUploader_CreateCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    const VkBufferCreateInfo*                      buffInfo,
    VmaAllocationCreateFlags                       flags,
    struct mmVKBuffer*                             pBuffer)
{
    VkResult result = VK_ERROR_UNKNOWN;
    VmaAllocationCreateInfo hVmaAllocInfo;

    assert(VK_NULL_HANDLE == pBuffer->buffer && VK_NULL_HANDLE == pBuffer->allocation && "Buffer not empty.");

    mmMemset(&hVmaAllocInfo, 0, sizeof(VmaAllocationCreateInfo));
    
    hVmaAllocInfo.flags = flags;
    hVmaAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
    result = vmaCreateBuffer(
        p->vmaAllocator,
        buffInfo,
        &hVmaAllocInfo,
        &pBuffer->buffer,
        &pBuffer->allocation,
        NULL);

    return result;
}

VkResult
mmVKUploader_UpdateCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset)
{
    VkResult err = VK_ERROR_UNKNOWN;
    do
    {
        void* pMappedBuffer = NULL;

        if (VK_NULL_HANDLE == p->vmaAllocator)
        {
            // vmaMapMemory internal assert vmaAllocator.
            break;
        }

        if (VK_NULL_HANDLE == pBuffer->allocation)
        {
            // vmaMapMemory internal assert allocation.
            break;
        }

        err = vmaMapMemory(
            p->vmaAllocator,
            pBuffer->allocation,
            (void**)&pMappedBuffer);
        if (VK_SUCCESS != err)
        {
            break;
        }

        mmMemcpy(((char*)pMappedBuffer) + vOffset, buffer + offset, length);
        vmaUnmapMemory(p->vmaAllocator, pBuffer->allocation);
    } while (0);

    return err;
}

VkResult
mmVKUploader_UploadCPU2GPUBuffer(
    struct mmVKUploader*                           p,
    VkBufferUsageFlags                             usage,
    VmaAllocationCreateFlags                       flags,
    const uint8_t*                                 buffer,
    size_t                                         length,
    struct mmVKBuffer*                             pBuffer)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        VkBufferCreateInfo hBufferInfo;

        // UBOAnimation
        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = (VkDeviceSize)length;
        hBufferInfo.usage = usage;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            p,
            &hBufferInfo,
            flags,
            pBuffer);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            p,
            (uint8_t*)(buffer),
            0,
            length,
            pBuffer,
            0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdataCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKUploader_DeleteBuffer(
    struct mmVKUploader*                           p,
    struct mmVKBuffer*                             pBuffer)
{
    if (VK_NULL_HANDLE != pBuffer->buffer && VK_NULL_HANDLE != pBuffer->allocation)
    {
        vmaDestroyBuffer(p->vmaAllocator, pBuffer->buffer, pBuffer->allocation);
        pBuffer->buffer = VK_NULL_HANDLE;
        pBuffer->allocation = VK_NULL_HANDLE;
    }
}

VkResult
mmVKUploader_CreateImage(
    struct mmVKUploader*                           p,
    VkImageCreateInfo*                             pImageInfo,
    VmaAllocationCreateFlags                       flags,
    VmaMemoryUsage                                 usage,
    VkImageViewType                                viewType,
    struct mmVKImage*                              pImage)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VmaAllocationCreateInfo hVmaAllocInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->vmaAllocator)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaAllocator is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        mmMemset(&hVmaAllocInfo, 0, sizeof(struct VmaAllocationCreateInfo));
        hVmaAllocInfo.flags = flags;
        hVmaAllocInfo.usage = usage;

        err = vmaCreateImage(
            p->vmaAllocator,
            pImageInfo,
            &hVmaAllocInfo,
            &pImage->image,
            &pImage->allocation,
            NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaCreateImage failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        pImage->format = pImageInfo->format;
        pImage->imageLayout = pImageInfo->initialLayout;
        pImage->width = pImageInfo->extent.width;
        pImage->height = pImageInfo->extent.height;
        pImage->depth = pImageInfo->extent.depth;
        pImage->levelCount = pImageInfo->mipLevels;
        pImage->layerCount = pImageInfo->arrayLayers;
        // This is default view type.
        pImage->viewType = viewType;
    } while(0);
    
    return err;
}

void
mmVKUploader_DeleteImage(
    struct mmVKUploader*                           p,
    struct mmVKImage*                              pImage)
{
    if (VK_NULL_HANDLE != pImage->image && VK_NULL_HANDLE != pImage->allocation)
    {
        vmaDestroyImage(p->vmaAllocator, pImage->image, pImage->allocation);
        mmVKImage_Reset(pImage);
    }
}

VkResult
mmVKUploader_PrepareImageView(
    struct mmVKUploader*                           pUploader,
    const struct mmVKImage*                        vImage,
    VkImageViewType                                viewType,
    VkImageAspectFlags                             aspectMask,
    VkImageView*                                   pImageView)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        VkImageViewCreateInfo hImageViewInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == vImage)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vImage is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pUploader->pAllocator;

        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = vImage->image;
        hImageViewInfo.viewType = viewType;
        hImageViewInfo.format = vImage->format;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = aspectMask;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = vImage->levelCount;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = vImage->layerCount;

        /* create image view */
        err = vkCreateImageView(device, &hImageViewInfo, pAllocator, pImageView);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateImageView failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKUploader_DiscardImageView(
    struct mmVKUploader*                           pUploader,
    VkImageView                                    imageView)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (VK_NULL_HANDLE == imageView)
        {
            break;
        }

        pAllocator = pUploader->pAllocator;

        vkDestroyImageView(device, imageView, pAllocator);
    } while (0);
}

VkResult
mmVKUploader_CopyBufferToImage(
    struct mmVKUploader*                           pUploader,
    const struct mmVKBuffer*                       pBuffer,
    struct mmVKImage*                              pImage,
    VkImageLayout                                  finalLayout)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        uint32_t regionCount = 1;
        VkBufferImageCopy copyRegions;
        VkImageSubresourceRange subresourceRange;

        struct mmLogger* gLogger = mmLogger_Instance();

        copyRegions.bufferOffset = 0;
        copyRegions.bufferRowLength = 0;
        copyRegions.bufferImageHeight = 0;
        copyRegions.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        copyRegions.imageSubresource.mipLevel = 0;
        copyRegions.imageSubresource.baseArrayLayer = 0;
        copyRegions.imageSubresource.layerCount = 1;
        copyRegions.imageOffset.x = 0;
        copyRegions.imageOffset.y = 0;
        copyRegions.imageOffset.z = 0;
        copyRegions.imageExtent.width = pImage->width;
        copyRegions.imageExtent.height = pImage->height;
        copyRegions.imageExtent.depth = pImage->depth;

        err = mmVKUploader_TransferCommandBufferBegin(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferBegin failure.", __FUNCTION__, __LINE__);
            break;
        }

        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = 1;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = 1;

        // Image barrier for optimal image (target)
        // Optimal image will be used as destination for the copy
        mmVKSetImageLayout(
            pUploader->transferCmdBuffer,
            pImage->image,
            pImage->imageLayout,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &subresourceRange);

        // copy buffer to image.
        vkCmdCopyBufferToImage(
            pUploader->transferCmdBuffer,
            pBuffer->buffer,
            pImage->image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            regionCount,
            &copyRegions);

        // Change texture image layout to shader read after all mip levels have been copied
        mmVKSetImageLayout(
            pUploader->transferCmdBuffer,
            pImage->image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            finalLayout,
            &subresourceRange);

        pImage->imageLayout = finalLayout;

        err = mmVKUploader_TransferCommandBufferEnd(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferEnd failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_TransferQueueSubmitWait(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_QueueSubmitWait failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKUploader_SetVKImageLayout(
    struct mmVKUploader*                           pUploader,
    struct mmVKImage*                              pImage,
    VkImageAspectFlags                             aspectMask,
    VkImageLayout                                  finalLayout)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkImageSubresourceRange subresourceRange;

        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmVKUploader_TransferCommandBufferBegin(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferBegin failure.", __FUNCTION__, __LINE__);
            break;
        }

        subresourceRange.aspectMask = aspectMask;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = pImage->levelCount;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = pImage->layerCount;

        mmVKSetImageLayout(
            pUploader->transferCmdBuffer,
            pImage->image,
            pImage->imageLayout,
            finalLayout,
            &subresourceRange);

        pImage->imageLayout = finalLayout;

        err = mmVKUploader_TransferCommandBufferEnd(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferEnd failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_TransferQueueSubmitWait(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_QueueSubmitWait failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKUploader_SetImageLayout(
    struct mmVKUploader*                           pUploader,
    VkImage                                        image,
    VkImageLayout                                  oldLayout,
    VkImageLayout                                  newLayout,
    VkImageSubresourceRange*                       pSubresourceRange)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmVKUploader_TransferCommandBufferBegin(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferBegin failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKSetImageLayout(
            pUploader->transferCmdBuffer,
            image,
            oldLayout,
            newLayout,
            pSubresourceRange);

        err = mmVKUploader_TransferCommandBufferEnd(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CommandBufferEnd failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_TransferQueueSubmitWait(pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_QueueSubmitWait failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}
