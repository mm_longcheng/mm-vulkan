/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKDescriptorParse_h__
#define __mmVKDescriptorParse_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDescriptorLayout.h"

#include "cJSON.h"

#include "core/mmPrefix.h"

void
mmVKDescriptorSetLayoutBinding_JEncode(
    const VkDescriptorSetLayoutBinding*            p,
    cJSON*                                         j);

void
mmVKDescriptorSetLayoutBinding_JDecode(
    VkDescriptorSetLayoutBinding*                  p,
    const cJSON*                                   j);

void
mmVKPushConstantRange_JEncode(
    const VkPushConstantRange*                     p,
    cJSON*                                         j);

void
mmVKPushConstantRange_JDecode(
    VkPushConstantRange*                           p,
    const cJSON*                                   j);

void
mmVKDescriptorPoolSize_JEncode(
    const VkDescriptorPoolSize*                    p,
    cJSON*                                         j);

void
mmVKDescriptorPoolSize_JDecode(
    VkDescriptorPoolSize*                          p,
    const cJSON*                                   j);

void
mmVKPipelineVertexInputInfo_JEncode(
    const struct mmVKPipelineVertexInputInfo*      p,
    cJSON*                                         j);

void
mmVKPipelineVertexInputInfo_JDecode(
    struct mmVKPipelineVertexInputInfo*            p,
    const cJSON*                                   j);

void
mmVKPipelineShaderStageInfo_JEncode(
    const struct mmVKPipelineShaderStageInfo*      p,
    cJSON*                                         j);

void
mmVKPipelineShaderStageInfo_JDecode(
    struct mmVKPipelineShaderStageInfo*            p,
    const cJSON*                                   j);

void
mmVKLayoutInfoArgs_JEncode(
    const struct mmVKLayoutInfoArgs*               p,
    cJSON*                                         j);

void
mmVKLayoutInfoArgs_JDecode(
    struct mmVKLayoutInfoArgs*                     p,
    const cJSON*                                   j);

void
mmVKPipelineArgs_JEncode(
    const struct mmVKPipelineArgs*                 p,
    cJSON*                                         j);

void
mmVKPipelineArgs_JDecode(
    struct mmVKPipelineArgs*                       p,
    const cJSON*                                   j);

#include "core/mmSuffix.h"

#endif//__mmVKDescriptorParse_h__
