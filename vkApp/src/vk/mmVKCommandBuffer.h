/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKCommandBuffer_h__
#define __mmVKCommandBuffer_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKBuffer;
struct mmVKImage;

VkResult
mmVKCommandBufferUpdateGPUOnlyBuffer(
    VkCommandBuffer                                cmdBuffer,
    VmaAllocator                                   vmaAllocator,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset);

VkResult
mmVKCommandBufferUpdateCPU2GPUBuffer(
    VkCommandBuffer                                cmdBuffer,
    VmaAllocator                                   vmaAllocator,
    const uint8_t*                                 buffer,
    size_t                                         offset,
    size_t                                         length,
    const struct mmVKBuffer*                       pBuffer,
    size_t                                         vOffset);

void
mmVKCommandBufferCopyBufferToImage(
    VkCommandBuffer                                cmdBuffer,
    const struct mmVKBuffer*                       pBuffer,
    struct mmVKImage*                              pImage,
    VkImageLayout                                  finalLayout);

VkResult
mmVKCommandBufferBeginCommandBuffer(
    VkCommandBuffer                                cmdBuffer);

VkResult
mmVKCommandBufferEndCommandBuffer(
    VkCommandBuffer                                cmdBuffer);

// hViewport [minx, miny, maxx, maxy]
VkResult
mmVKCommandBufferSetViewport(
    VkCommandBuffer                                cmdBuffer,
    float                                          hViewport[4],
    float                                          hMinDepth,
    float                                          hMaxDepth);

// hScissor  [minx, miny, maxx, maxy]
VkResult
mmVKCommandBufferSetScissor(
    VkCommandBuffer                                cmdBuffer,
    uint32_t                                       hScissor[4]);

struct mmVKCommandBuffer
{
    // cmd buffer.
    VkCommandBuffer cmdBuffer;

    // window size.
    uint32_t hWindowSize[2];

    // Viewport status
    // [minx, miny, maxx, maxy]
    float hViewport[4];

    // Scissor  status
    // [minx, miny, maxx, maxy]
    uint32_t hScissor[4];
};

void
mmVKCommandBuffer_SetWindowSize(
    struct mmVKCommandBuffer*                      p,
    uint32_t                                       hWindowSize[2]);

void
mmVKCommandBuffer_SetCommandBuffer(
    struct mmVKCommandBuffer*                      p,
    VkCommandBuffer                                cmdBuffer);

void
mmVKCommandBuffer_SetViewport(
    struct mmVKCommandBuffer*                      p,
    float                                          hMinDepth,
    float                                          hMaxDepth);

void
mmVKCommandBuffer_SetScissor(
    struct mmVKCommandBuffer*                      p);

void
mmVKCommandBuffer_ScissorStorage(
    struct mmVKCommandBuffer*                      p,
    uint32_t                                       hScissorCache[4],
    uint32_t                                       hScissorValue[4]);

void
mmVKCommandBuffer_ScissorRestore(
    struct mmVKCommandBuffer*                      p,
    uint32_t                                       hScissorCache[4]);

void
mmVKCommandBuffer_ScissorValueClamp(
    struct mmVKCommandBuffer*                      p,
    const float                                    hWorldRect[4],
    uint32_t                                       hScissorValue[4]);

#include "core/mmSuffix.h"

#endif//__mmVKCommandBuffer_h__
