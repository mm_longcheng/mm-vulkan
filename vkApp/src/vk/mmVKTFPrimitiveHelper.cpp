/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTFPrimitiveHelper.h"

#include "vk/mmVKUploader.h"
#include "vk/mmVKTFModel.h"

#include "parse/mmParseGLTF.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmStreambuf.h"

#ifdef _MSVC_LANG
#pragma warning(push, 4)
#pragma warning(disable: 4804)
#pragma warning(disable: 4018)
#endif

#include "draco/compression/decode.h"
#include "draco/core/decoder_buffer.h"

#ifdef _MSVC_LANG
#pragma warning(pop)
#endif

static
void
mmParseDraco_DecodeIndex(
    draco::Mesh*                                   mesh,
    size_t                                         componentSize,
    std::vector<uint8_t>&                          o)
{
    if (componentSize == 4)
    {
        assert(sizeof(mesh->face(draco::FaceIndex(0))[0]) == componentSize);
        mmMemcpy(o.data(),
                 &mesh->face(draco::FaceIndex(0))[0],
                 o.size());
    }
    else
    {
        size_t faceStride = componentSize * 3;
        for (draco::FaceIndex f(0); f < mesh->num_faces(); ++f)
        {
            const draco::Mesh::Face& face = mesh->face(f);
            if (componentSize == 2)
            {
                uint16_t indices[3] =
                {
                    (uint16_t)face[0].value(),
                    (uint16_t)face[1].value(),
                    (uint16_t)face[2].value(),
                };
                mmMemcpy(o.data() + f.value() * faceStride,
                         &indices[0],
                         faceStride);
            }
            else
            {
                uint8_t indices[3] =
                {
                    (uint8_t)face[0].value(),
                    (uint8_t)face[1].value(),
                    (uint8_t)face[2].value(),
                };
                mmMemcpy(o.data() + f.value() * faceStride,
                         &indices[0],
                         faceStride);
            }
        }
    }
}

template <typename T>
static
int
mmParseDraco_DecodeTypeAttribute(
    draco::Mesh*                                   mesh,
    const draco::PointAttribute*                   attr,
    std::vector<uint8_t>&                          o)
{
    size_t byteOffset = 0;
    T values[4] = {0, 0, 0, 0};
    for (draco::PointIndex i(0); i < mesh->num_points(); ++i)
    {
        const draco::AttributeValueIndex val_index = attr->mapped_index(i);
        int8_t num = attr->num_components();
        if (!attr->ConvertValue<T>(val_index, num, values))
        {
            return MM_FALSE;
        }

        mmMemcpy(o.data() + byteOffset,
                 &values[0],
                 sizeof(T) * num);
        
        byteOffset += sizeof(T) * num;
    }

    return MM_TRUE;
}

static
int
mmParseDraco_DecodeAttribute(
    uint32_t                                       componentType,
    draco::Mesh*                                   mesh,
    const draco::PointAttribute*                   attr,
    std::vector<uint8_t>&                          o)
{
    switch (componentType)
    {
    case mmGLTFComponentTypeUnsignedByte:
        return mmParseDraco_DecodeTypeAttribute<uint8_t>(mesh, attr, o);
    case mmGLTFComponentTypeByte:
        return mmParseDraco_DecodeTypeAttribute<int8_t>(mesh, attr, o);
    case mmGLTFComponentTypeUnsignedShort:
        return mmParseDraco_DecodeTypeAttribute<uint16_t>(mesh, attr, o);
    case mmGLTFComponentTypeShort:
        return mmParseDraco_DecodeTypeAttribute<int16_t>(mesh, attr, o);
    case mmGLTFComponentTypeInt:
        return mmParseDraco_DecodeTypeAttribute<int32_t>(mesh, attr, o);
    case mmGLTFComponentTypeUnsignedInt:
        return mmParseDraco_DecodeTypeAttribute<uint32_t>(mesh, attr, o);
    case mmGLTFComponentTypeFloat:
        return mmParseDraco_DecodeTypeAttribute<float>(mesh, attr, o);
    case mmGLTFComponentTypeDouble:
        return mmParseDraco_DecodeTypeAttribute<double>(mesh, attr, o);
    default:
        return MM_FALSE;
    }
}

VkResult
mmVKTFPrimitive_PrepareAttributes(
    struct mmVKTFPrimitive*                        p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFPrimitive*                        pGLTFPrimitive)
{
    VkResult err = VK_ERROR_UNKNOWN;

    struct mmStreambuf streambuf;

    mmStreambuf_Init(&streambuf);

    do
    {        
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmGLTFRoot* root = &pModel->root;

        VkDevice device = VK_NULL_HANDLE;

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pGLTFPrimitive)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pGLTFPrimitive is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (NULL == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (pGLTFPrimitive->has_draco)
        {
            // has draco
            struct mmGLTFDracoMeshCompression* pDracoMesh = &pGLTFPrimitive->ext_draco;
            struct mmVectorValue* pDracoAttributes = &pDracoMesh->attributes;
            struct mmVectorValue* pPrimitiveAttributes = &pGLTFPrimitive->attributes;

            struct mmGLTFAccessor* pGLTFAccessor = NULL;
            struct mmGLTFAttribute* pGLTFAttribute = NULL;
            struct mmGLTFBufferView* pGLTFBufferView = NULL;
            struct mmGLTFBuffer* pGLTFBuffer = NULL;
            struct mmGLTFUriData* pGLTFUriData = NULL;
            struct mmByteBuffer* bytes = NULL;

            const char* pViewData = NULL;
            size_t hViewSize = 0;

            size_t i = 0;

            if (-1 == pDracoMesh->bufferView)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " pDracoMesh->bufferView is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }
            pGLTFBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(&root->bufferViews, pDracoMesh->bufferView);

            if (-1 == pGLTFBufferView->buffer)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " pGLTFBufferView->buffer is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }
            pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(&root->buffers, pGLTFBufferView->buffer);
            pGLTFUriData = &pGLTFBuffer->data;
            bytes = &pGLTFUriData->bytes;
            if (NULL == bytes->buffer || 0 == bytes->length)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " bytes is invalid.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }

            pViewData = (const char*)(
                bytes->buffer + bytes->offset + 
                pGLTFBufferView->byteOffset);

            hViewSize = pGLTFBufferView->byteLength;

            // decode draco
            typedef std::unique_ptr<draco::Mesh> MeshPtr;
            typedef draco::StatusOr<MeshPtr> StatusOr;
            draco::DecoderBuffer decoderBuffer;
            decoderBuffer.Init(pViewData, hViewSize);
            draco::Decoder decoder;
            StatusOr decodeResult = decoder.DecodeMeshFromBuffer(&decoderBuffer);
            if (!decodeResult.ok())
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " DecodeMeshFromBuffer failure.", __FUNCTION__, __LINE__);
                err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                break;
            }
            const MeshPtr& mesh = decodeResult.value();

            // create new bufferView for indices
            if (pGLTFPrimitive->indices >= 0)
            {
                pGLTFAccessor = (struct mmGLTFAccessor*)mmVectorValue_At(&root->accessors, pGLTFPrimitive->indices);

                enum mmGLTFComponent_t componentType = pGLTFAccessor->componentType;
                int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);

                std::vector<uint8_t> data;
                size_t byteLength = mesh->num_faces() * 3 * componentSize;
                // Buffer decodedIndexBuffer;
                data.resize(byteLength);

                mmParseDraco_DecodeIndex(mesh.get(), componentSize, data);

                VkIndexType hIdxType = mmVKTFGetIndexType(componentType);

                pViewData = (const char*)data.data();
                hViewSize = byteLength;

                if (VK_INDEX_TYPE_UINT8_EXT == hIdxType)
                {
                    // VK_EXT_index_type_uint8 many devices do not support.
                    const char* ptrElems = NULL;
                    uint32_t hIdxCount = mesh->num_faces() * 3;
                    hViewSize = sizeof(uint16_t) * hIdxCount;
                    mmStreambuf_Reset(&streambuf);
                    mmStreambuf_AlignedMemory(&streambuf, hViewSize);
                    ptrElems = (const char*)(streambuf.buff + streambuf.pptr);
                    mmGLTFIndex8ToIndex16((uint8_t*)pViewData, (uint16_t*)ptrElems, hIdxCount);
                    pViewData = ptrElems;

                    err = mmVKUploader_UploadGPUOnlyBuffer(
                        pUploader,
                        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                        0,
                        (uint8_t*)pViewData,
                        hViewSize,
                        &p->vBufferIdx);

                    p->hIdxCount = hIdxCount;
                    p->hIdxType = VK_INDEX_TYPE_UINT16;
                }
                else
                {
                    // buffer index.
                    err = mmVKUploader_UploadGPUOnlyBuffer(
                        pUploader,
                        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                        0,
                        (uint8_t*)pViewData,
                        hViewSize,
                        &p->vBufferIdx);

                    p->hIdxCount = mesh->num_faces() * 3;
                    p->hIdxType = hIdxType;
                }
            }
            else
            {
                p->hIdxCount = 0;
                p->hIdxType = VK_INDEX_TYPE_UINT16;
            }

            for (i = 0; i < pDracoAttributes->size; ++i)
            {
                pGLTFAttribute = (struct mmGLTFAttribute*)mmVectorValue_At(pDracoAttributes, i);
                int hDracoAttributeIndex = pGLTFAttribute->accessor;
                const draco::PointAttribute* attr = mesh->GetAttributeByUniqueId(hDracoAttributeIndex);
                if (NULL == attr)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " PointAttribute is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }

                int accessor = mmGLTFAttributes_GetAccessorSister(pDracoAttributes, pPrimitiveAttributes, i);

                if (-1 == accessor)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " AccessorSister is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFAccessor = (struct mmGLTFAccessor*)mmVectorValue_At(&root->accessors, accessor);

                enum mmGLTFComponent_t componentType = pGLTFAccessor->componentType;
                int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);

                // Buffer decodedAttributeBuffer;
                std::vector<uint8_t> data;
                size_t byteLength = mesh->num_points() * attr->num_components() * componentSize;
                data.resize(byteLength);

                if (!mmParseDraco_DecodeAttribute(
                    componentType,
                    mesh.get(),
                    attr,
                    data))
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " mmParseDraco_DecodeAttribute failure.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }

                err = mmVKTFPrimitive_UploadAttribute(
                    p,
                    pUploader,
                    pGLTFAccessor,
                    pGLTFAttribute,
                    byteLength,
                    (uint8_t*)data.data());
            }
        }
        else
        {
            // not draco
            struct mmVectorValue* pPrimitiveAttributes = &pGLTFPrimitive->attributes;

            const struct mmVectorValue* bufferViews = &root->bufferViews;
            const struct mmVectorValue* buffers = &root->buffers;

            struct mmGLTFAccessor* pGLTFAccessor = NULL;
            struct mmGLTFAttribute* pGLTFAttribute = NULL;
            struct mmGLTFBufferView* pGLTFBufferView = NULL;
            struct mmGLTFBuffer* pGLTFBuffer = NULL;
            struct mmGLTFUriData* pGLTFUriData = NULL;
            struct mmByteBuffer* bytes = NULL;

            const char* pViewData = NULL;
            size_t hViewSize = 0;
            int hByteStride;

            size_t i = 0;

            // create new bufferView for indices
            if (pGLTFPrimitive->indices >= 0)
            {
                pGLTFAccessor = (struct mmGLTFAccessor*)mmVectorValue_At(&root->accessors, pGLTFPrimitive->indices);

                if (-1 == pGLTFAccessor->bufferView)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pGLTFAccessor->bufferView is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, pGLTFAccessor->bufferView);

                if (-1 == pGLTFBufferView->buffer)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pGLTFBufferView->buffer is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, pGLTFBufferView->buffer);

                pGLTFUriData = &pGLTFBuffer->data;
                bytes = &pGLTFUriData->bytes;

                if (NULL == bytes->buffer || 0 == bytes->length)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " bytes is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }

                pViewData = (const char*)(
                    bytes->buffer + bytes->offset + 
                    pGLTFBufferView->byteOffset + pGLTFAccessor->byteOffset);

                hViewSize = pGLTFBufferView->byteLength;

                enum mmGLTFComponent_t componentType = pGLTFAccessor->componentType;
                VkIndexType hIdxType = mmVKTFGetIndexType(componentType);

                if (VK_INDEX_TYPE_UINT8_EXT == hIdxType)
                {
                    // VK_EXT_index_type_uint8 many devices do not support.
                    const char* ptrElems = NULL;
                    uint32_t hIdxCount = (uint32_t)(pGLTFAccessor->count);
                    hViewSize = sizeof(uint16_t) * hIdxCount;
                    mmStreambuf_Reset(&streambuf);
                    mmStreambuf_AlignedMemory(&streambuf, hViewSize);
                    ptrElems = (const char*)(streambuf.buff + streambuf.pptr);
                    mmGLTFIndex8ToIndex16((uint8_t*)pViewData, (uint16_t*)ptrElems, hIdxCount);
                    pViewData = ptrElems;

                    err = mmVKUploader_UploadGPUOnlyBuffer(
                        pUploader,
                        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                        0,
                        (uint8_t*)pViewData,
                        hViewSize,
                        &p->vBufferIdx);

                    p->hIdxCount = hIdxCount;
                    p->hIdxType = VK_INDEX_TYPE_UINT16;
                }
                else
                {
                    // hViewSize = pGLTFBufferView->byteLength;
                    // Need to manually calculate the size, BufferView.byteLength Unreliable.
                    hByteStride = mmGLTFAccessor_ByteStride(pGLTFAccessor, pGLTFBufferView);
                    hViewSize = hByteStride * pGLTFAccessor->count;

                    // buffer index.
                    err = mmVKUploader_UploadGPUOnlyBuffer(
                        pUploader,
                        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                        0,
                        (uint8_t*)pViewData,
                        hViewSize,
                        &p->vBufferIdx);

                    p->hIdxCount = (uint32_t)(pGLTFAccessor->count);
                    p->hIdxType = hIdxType;
                }
            }
            else
            {
                p->hIdxCount = 0;
                p->hIdxType = VK_INDEX_TYPE_UINT16;
            }

            for (i = 0; i < pPrimitiveAttributes->size; ++i)
            {
                pGLTFAttribute = (struct mmGLTFAttribute*)mmVectorValue_At(pPrimitiveAttributes, i);

                if (-1 == pGLTFAttribute->accessor)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pGLTFAttribute->accessor is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFAccessor = (struct mmGLTFAccessor*)mmVectorValue_At(&root->accessors, pGLTFAttribute->accessor);

                if (-1 == pGLTFAccessor->bufferView)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pGLTFAccessor->bufferView is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFBufferView = (struct mmGLTFBufferView*)mmVectorValue_At(bufferViews, pGLTFAccessor->bufferView);

                if (-1 == pGLTFBufferView->buffer)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " pGLTFBufferView->buffer is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }
                pGLTFBuffer = (struct mmGLTFBuffer*)mmVectorValue_At(buffers, pGLTFBufferView->buffer);

                pGLTFUriData = &pGLTFBuffer->data;
                bytes = &pGLTFUriData->bytes;

                if (NULL == bytes->buffer || 0 == bytes->length)
                {
                    mmLogger_LogE(gLogger, "%s %d"
                        " bytes is invalid.", __FUNCTION__, __LINE__);
                    err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    break;
                }

                pViewData = (const char *)(
                    bytes->buffer + bytes->offset + 
                    pGLTFBufferView->byteOffset + pGLTFAccessor->byteOffset);

                // hViewSize = pGLTFBufferView->byteLength;
                // Need to manually calculate the size, BufferView.byteLength Unreliable.
                hByteStride = mmGLTFAccessor_ByteStride(pGLTFAccessor, pGLTFBufferView);
                hViewSize = hByteStride * pGLTFAccessor->count;

                if (pGLTFAccessor->isSparse)
                {
                    struct mmByteBuffer bytes;
                    bytes.buffer = (mmUInt8_t*)pViewData;
                    bytes.offset = 0;
                    bytes.length = hViewSize;

                    enum mmGLTFComponent_t componentType = pGLTFAccessor->componentType;
                    int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);
                    int32_t componentNum = mmGLTFGetNumComponentsInType(pGLTFAccessor->type);

                    size_t numElems = pGLTFAccessor->count * componentNum;
                    size_t numBytes = componentSize * numElems;
                    mmStreambuf_Reset(&streambuf);
                    mmStreambuf_AlignedMemory(&streambuf, numBytes);
                    uint8_t* ptrElems = (uint8_t*)(streambuf.buff + streambuf.pptr);
                    size_t sz = mmGLTFRoot_AccessorUnpackBytes(root, pGLTFAccessor, &bytes, ptrElems, numElems);
                    if (0 == sz)
                    {
                        mmLogger_LogE(gLogger, "%s %d"
                            " mmGLTFRoot_AccessorUnpackBytes failure.", __FUNCTION__, __LINE__);
                        err = VK_ERROR_FORMAT_NOT_SUPPORTED;
                    }
                    else
                    {
                        err = mmVKTFPrimitive_UploadAttribute(
                            p,
                            pUploader,
                            pGLTFAccessor,
                            pGLTFAttribute,
                            numBytes,
                            (uint8_t*)ptrElems);
                    }
                }
                else
                {
                    err = mmVKTFPrimitive_UploadAttribute(
                        p,
                        pUploader,
                        pGLTFAccessor,
                        pGLTFAttribute,
                        hViewSize,
                        (uint8_t*)pViewData);
                }
            }
        }

        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFPrimitive_UploadAttribute failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKTFPrimitive_MakeTypeId(p, &p->hTypeId);

    } while (0);

    mmStreambuf_Destroy(&streambuf);

    return err;
}

void
mmVKTFPrimitive_DiscardAttributes(
    struct mmVKTFPrimitive*                        p,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        int i;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKUploader_DeleteBuffer(pUploader, &p->vBufferIdx);

        for (i = 0; i < mmVKTFAttributeMax; i++)
        {
            mmVKUploader_DeleteBuffer(pUploader, &p->vAttribute[i].vBuffer);
        }

    } while (0);
}
