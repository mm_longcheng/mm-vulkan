/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPassLoader_h__
#define __mmVKPassLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKRenderPass.h"

#include "core/mmPrefix.h"

struct mmVectorValue;
struct mmFileContext;

struct mmVKSwapchain;

struct mmVKPassInfo
{
    // name for pass info, read only. 
    // string is weak reference value.
    struct mmString name;

    // RenderPass archive.
    struct mmVKRenderPass vRenderPass;

    // reference count. 
    size_t reference;
};

void
mmVKPassInfo_Init(
    struct mmVKPassInfo*                           p);

void
mmVKPassInfo_Destroy(
    struct mmVKPassInfo*                           p);

int
mmVKPassInfo_Invalid(
    const struct mmVKPassInfo*                     p);

void
mmVKPassInfo_Increase(
    struct mmVKPassInfo*                           p);

void
mmVKPassInfo_Decrease(
    struct mmVKPassInfo*                           p);

VkResult
mmVKPassInfo_Prepare(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain);

VkResult
mmVKPassInfo_PrepareDepthFormat(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice,
    VkFormat                                       depthFormat);

void
mmVKPassInfo_Discard(
    struct mmVKPassInfo*                           p,
    struct mmVKDevice*                             pDevice);

struct mmVKPassLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKDevice* pDevice;
    struct mmRbtreeStringVpt sets;
};

void
mmVKPassLoader_Init(
    struct mmVKPassLoader*                         p);

void
mmVKPassLoader_Destroy(
    struct mmVKPassLoader*                         p);

void
mmVKPassLoader_SetFileContext(
    struct mmVKPassLoader*                         p,
    struct mmFileContext*                          pFileContext);

void
mmVKPassLoader_SetDevice(
    struct mmVKPassLoader*                         p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKPassLoader_PreparePassInfo(
    struct mmVKPassLoader*                         p,
    const struct mmVKSwapchain*                    pSwapchain,
    struct mmVKPassInfo*                           pPassInfo);

VkResult
mmVKPassLoader_PreparePassInfoDepthFormat(
    struct mmVKPassLoader*                         p,
    VkFormat                                       depthFormat,
    struct mmVKPassInfo*                           pPassInfo);

void
mmVKPassLoader_DiscardPassInfo(
    struct mmVKPassLoader*                         p,
    struct mmVKPassInfo*                           pPassInfo);

void
mmVKPassLoader_UnloadByName(
    struct mmVKPassLoader*                         p,
    const char*                                    name);

void
mmVKPassLoader_UnloadComplete(
    struct mmVKPassLoader*                         p);

struct mmVKPassInfo*
mmVKPassLoader_LoadFromSwapchain(
    struct mmVKPassLoader*                         p,
    const char*                                    name,
    const struct mmVKSwapchain*                    pSwapchain);

struct mmVKPassInfo*
mmVKPassLoader_LoadFromDepthFormat(
    struct mmVKPassLoader*                         p,
    const char*                                    name,
    VkFormat                                       depthFormat);

struct mmVKPassInfo*
mmVKPassLoader_GetFromName(
    struct mmVKPassLoader*                         p,
    const char*                                    name);

void
mmVKPassLoaderPrepareDefaultPass(
    struct mmVKPassLoader*                         p,
    struct mmVKSwapchain*                          pSwapchain);

void
mmVKPassLoaderDiscardDefaultPass(
    struct mmVKPassLoader*                         p);

#include "core/mmSuffix.h"

#endif//__mmVKPassLoader_h__
