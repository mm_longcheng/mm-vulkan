/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKBuffer_h__
#define __mmVKBuffer_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKBuffer
{
    VkBuffer buffer;
    VmaAllocation allocation;
};

void
mmVKBuffer_Init(
    struct mmVKBuffer*                             p);

void
mmVKBuffer_Destroy(
    struct mmVKBuffer*                             p);

void
mmVKBuffer_Reset(
    struct mmVKBuffer*                             p);

#include "core/mmSuffix.h"

#endif//__mmVKBuffer_h__
