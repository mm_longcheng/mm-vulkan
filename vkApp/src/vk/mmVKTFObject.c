/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTFObject.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKPipeline.h"
#include "vk/mmVKScene.h"
#include "vk/mmVKPipelineLoader.h"
#include "vk/mmVKAssets.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"
#include "math/mmMatrix4x4.h"

#include "parse/mmParseJSONHelper.h"

void
mmVKTFNodeJoints_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(mmMat4x4));
}

void
mmVKTFNodeJoints_Destroy(
    struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKTFNodeWeights_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(float));
}

void
mmVKTFNodeWeights_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKTFNode_Init(
    struct mmVKTFNode*                             p)
{
    mmString_Init(&p->name);
    mmVKNode_Init(&p->node);

    p->skin = NULL;
    p->mesh = NULL;

    mmMemset(&p->pushConstant, 0, sizeof(struct mmVKPushConstants));
    
    mmVKTFNodeJoints_Init(&p->jointMatrices);
    mmVKTFNodeWeights_Init(&p->weights);

    mmVKBuffer_Init(&p->bufferJoints);
    mmVKBuffer_Init(&p->bufferWeights);
    mmVKDescriptorSet_Init(&p->vDescriptorSet);
}

void
mmVKTFNode_Destroy(
    struct mmVKTFNode*                             p)
{
    mmVKDescriptorSet_Destroy(&p->vDescriptorSet);
    mmVKBuffer_Destroy(&p->bufferWeights);
    mmVKBuffer_Destroy(&p->bufferJoints);

    mmVKTFNodeWeights_Destroy(&p->weights);
    mmVKTFNodeJoints_Destroy(&p->jointMatrices);

    mmMemset(&p->pushConstant, 0, sizeof(struct mmVKPushConstants));

    p->mesh = NULL;
    p->skin = NULL;

    mmVKNode_Destroy(&p->node);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFNode_PrepareUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkBufferCreateInfo hBufferInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 != p->jointMatrices.size)
        {
            hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            hBufferInfo.pNext = NULL;
            hBufferInfo.flags = 0;
            hBufferInfo.size = p->jointMatrices.size * p->jointMatrices.element;
            hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
            hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            hBufferInfo.queueFamilyIndexCount = 0;
            hBufferInfo.pQueueFamilyIndices = NULL;
            err = mmVKUploader_CreateCPU2GPUBuffer(
                pUploader,
                &hBufferInfo,
                0,
                &p->bufferJoints);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }

        if (0 != p->weights.size)
        {
            hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            hBufferInfo.pNext = NULL;
            hBufferInfo.flags = 0;
            hBufferInfo.size = p->weights.size * p->weights.element;
            hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
            hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            hBufferInfo.queueFamilyIndexCount = 0;
            hBufferInfo.pQueueFamilyIndices = NULL;
            err = mmVKUploader_CreateCPU2GPUBuffer(
                pUploader,
                &hBufferInfo,
                0,
                &p->bufferWeights);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }

    } while (0);

    return err;
}

void
mmVKTFNode_DiscardUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferWeights);
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferJoints);

    mmVectorValue_Reset(&p->jointMatrices);
}

VkResult
mmVKTFNode_UploadeUBOBuffer(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (0 != p->jointMatrices.size)
        {
            err = mmVKUploader_UpdateCPU2GPUBuffer(
                pUploader,
                (uint8_t*)p->jointMatrices.arrays,
                (size_t)0,
                (size_t)(p->jointMatrices.size * p->jointMatrices.element),
                &p->bufferJoints,
                (size_t)0);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }

        if (0 != p->weights.size)
        {
            err = mmVKUploader_UpdateCPU2GPUBuffer(
                pUploader,
                (uint8_t*)p->weights.arrays,
                (size_t)0,
                (size_t)(p->weights.size * p->weights.element),
                &p->bufferWeights,
                (size_t)0);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        else
        {
            err = VK_SUCCESS;
        }
    } while (0);

    return err;
}

VkResult
mmVKTFNode_DescriptorSetProduce(
    struct mmVKTFNode*                             p,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->skin)
        {
            err = VK_SUCCESS;
            break;
        }

        if (NULL == pDescriptorPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDescriptorPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(pDescriptorPool, &p->vDescriptorSet);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKTFNode_DescriptorSetRecycle(
    struct mmVKTFNode*                             p,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->skin)
        {
            break;
        }

        if (NULL == pDescriptorPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDescriptorPool is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKDescriptorPool_DescriptorSetRecycle(pDescriptorPool, &p->vDescriptorSet);
    } while (0);
}

VkResult
mmVKTFNode_DescriptorSetUpdate(
    struct mmVKTFNode*                             p,
    VkDevice                                       device)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDescriptorBufferInfo hDescriptorBufferInfo[1];

        VkWriteDescriptorSet writes[1];

        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == p->skin)
        {
            err = VK_SUCCESS;
            break;
        }

        descriptorSet = p->vDescriptorSet.descriptorSet;

        // descriptorSet2
        hDescriptorBufferInfo[0].buffer = p->bufferJoints.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(mmMat4x4Identity) * p->jointMatrices.size;

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 1, writes, 0, NULL);
        
        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmVKTFNode_Prepare(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFNode*                             pGLTFNode,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmGLTFModel* pModel = &pVKTFModel->model;
        struct mmVectorValue* skins = &pVKTFModel->skins;
        struct mmVectorValue* meshes = &pVKTFModel->meshes;
        
        struct mmParseJSON* parse = &pModel->parse;
        struct mmVKNode* node = &p->node;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmParseJSON_GetRangeString(parse, &pGLTFNode->name, &p->name);

        if (16 == pGLTFNode->sizeMatrix)
        {
            mmMat4x4Decomposition(pGLTFNode->matrix, node->scale, node->quaternion, node->position);
        }
        else
        {
            mmVKNode_SetScale(node, pGLTFNode->scale);
            mmVKNode_SetQuaternion(node, pGLTFNode->rotation);
            mmVKNode_SetPosition(node, pGLTFNode->translation);
        }

        if (-1 != pGLTFNode->skin)
        {
            size_t count;
            p->skin = (struct mmVKTFSkin*)mmVectorValue_At(skins, pGLTFNode->skin);

            count = mmVKTFAccessor_GetCount(&p->skin->inverseBindMatrices);
            mmVectorValue_AllocatorMemory(&p->jointMatrices, count);
        }
        else
        {
            p->skin = NULL;
            mmVectorValue_Reset(&p->jointMatrices);
        }

        if (-1 != pGLTFNode->mesh)
        {
            struct mmVectorValue* meshes0 = &pModel->root.meshes;
            struct mmGLTFMesh* mesh0 = (struct mmGLTFMesh*)mmVectorValue_At(meshes0, pGLTFNode->mesh);

            p->mesh = (struct mmVKTFMesh*)mmVectorValue_At(meshes, pGLTFNode->mesh);
            mmVectorValue_CopyFromValue(&p->weights, &mesh0->weights);
        }
        else
        {
            p->mesh = NULL;
            mmVectorValue_CopyFromValue(&p->weights, &pGLTFNode->weights);
        }

        err = mmVKTFNode_PrepareUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKNode_PrepareUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKTFNode_DescriptorSetProduce(p, pDescriptorPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFNode_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKTFNode_DescriptorSetUpdate(p, pUploader->device);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFNode_DescriptorSetUpdate failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKTFNode_Discard(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    mmVKTFNode_DescriptorSetRecycle(p, pDescriptorPool);
    mmVKTFNode_DiscardUBOBuffer(p, pUploader);

    p->mesh = NULL;
    p->skin = NULL;

    mmString_Reset(&p->name);
}

VkResult
mmVKTFNode_UpdateJoins(
    struct mmVKTFNode*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmVectorValue*                          nodes,
    struct mmVectorValue*                          skins)
{
    VkResult err = VK_ERROR_UNKNOWN;

    if (NULL != p->skin)
    {
        float worldTransform[4][4];
        float inverseTransform[4][4];

        float sjm[4][4];
        size_t i;

        int n = 0;
        struct mmVKTFNode* pVKTFNode = NULL;
        struct mmVectorU32* joints = &p->skin->joints;
        size_t numJoints = joints->size;

        const mmMat4x4* ibm = (const mmMat4x4*)mmVKTFAccessor_GetData(&p->skin->inverseBindMatrices);
        mmMat4x4* jointMatrices = (mmMat4x4*)(p->jointMatrices.arrays);

        mmVKNode_GetWorldTransform(&p->node, worldTransform);
        mmMat4x4Inverse(inverseTransform, worldTransform);

        for (i = 0; i < numJoints; i++)
        {
            n = (int)mmVectorU32_At(joints, i);
            pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes, n);

            mmVKNode_GetWorldTransform(&pVKTFNode->node, sjm);
            mmMat4x4Mul(jointMatrices[i], sjm, ibm[i]);
            mmMat4x4Mul(jointMatrices[i], inverseTransform, jointMatrices[i]);
        }

        err = mmVKTFNode_UploadeUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKNode_UploadeUBOBuffer failure.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        err = VK_SUCCESS;
    }

    return err;
}

void
mmVKTFNodes_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFNode_Init;
    hValueAllocator.Recycle = &mmVKTFNode_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFNode));
}

void
mmVKTFNodes_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFNodes_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int* id = 0;
        size_t i = 0;
        size_t j = 0;
        struct mmVKTFNode* pVKTFNode = NULL;
        struct mmGLTFNode* pGLTFNode = NULL;
        struct mmVKTFNode* child = NULL;

        struct mmGLTFModel* pModel = &pVKTFModel->model;

        struct mmGLTFRoot* root = &pModel->root;
        struct mmVectorValue* nodes0 = p;
        struct mmVectorValue* nodes1 = &root->nodes;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFNode) && "invalid type");

        if (0 == nodes1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(nodes0, nodes1->size);

        for (i = 0; i < nodes1->size; ++i)
        {
            pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes0, i);
            pGLTFNode = (struct mmGLTFNode*)mmVectorValue_At(nodes1, i);
            err = mmVKTFNode_Prepare(
                pVKTFNode,
                pUploader,
                pGLTFNode,
                pVKTFModel,
                pDescriptorPool);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFNode_PrepareNode failure.", __FUNCTION__, __LINE__);
                break;
            }

            for (j = 0; j < pGLTFNode->children.size; ++j)
            {
                id = (int*)mmVectorValue_At(&pGLTFNode->children, j);
                child = (struct mmVKTFNode*)mmVectorValue_At(nodes0, (*id));
                mmVKNode_AddChild(&pVKTFNode->node, &child->node);
            }
        }
    } while (0);

    return err;
}

void
mmVKTFNodes_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDescriptorPool*                     pDescriptorPool)
{
    size_t i = 0;
    struct mmVKTFNode* pVKTFNode = NULL;

    struct mmVectorValue* nodes0 = p;

    assert(p->element == sizeof(struct mmVKTFNode) && "invalid type");

    for (i = 0; i < nodes0->size; ++i)
    {
        pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes0, i);
        mmVKTFNode_Discard(pVKTFNode, pUploader, pDescriptorPool);
    }

    mmVectorValue_Reset(nodes0);
}

void
mmVKTFScene_Init(
    struct mmVKTFScene*                            p)
{
    mmString_Init(&p->name);
    mmVectorVpt_Init(&p->nodes);
}

void
mmVKTFScene_Destroy(
    struct mmVKTFScene*                            p)
{
    mmVectorVpt_Destroy(&p->nodes);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFScene_Prepare(
    struct mmVKTFScene*                            p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFScene*                            pGLTFScene,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmParseJSON* parse = &pModel->parse;

        size_t i = 0;
        int* v = NULL;
        int n = 0;
        struct mmVKTFNode* pVKTFNode = NULL;
        struct mmVectorValue* nodes1 = &pGLTFScene->nodes;

        assert(nodes->element == sizeof(struct mmVKTFNode) && "invalid type");

        mmParseJSON_GetRangeString(parse, &pGLTFScene->name, &p->name);

        if (0 == nodes1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorVpt_Reset(&p->nodes);
        mmVectorVpt_AllocatorMemory(&p->nodes, nodes1->size);
        for (i = 0; i < nodes1->size; ++i)
        {
            v = (int*)mmVectorValue_At(nodes1, i);
            n = (*v);
            pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes, n);

            mmVectorVpt_SetIndex(&p->nodes, i, (void*)pVKTFNode);
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFScene_Discard(
    struct mmVKTFScene*                            p)
{
    mmVectorVpt_Reset(&p->nodes);
    mmString_Reset(&p->name);
}

void
mmVKTFScene_PrepareRootNode(
    struct mmVKTFScene*                            p,
    struct mmVKNode*                               node)
{
    size_t i = 0;
    struct mmVKTFNode* pVKTFNode = NULL;
    struct mmVectorVpt* nodes0 = &p->nodes;
    for (i = 0; i < nodes0->size; ++i)
    {
        pVKTFNode = (struct mmVKTFNode*)mmVectorVpt_At(nodes0, i);
        mmVKNode_AddChild(node, &pVKTFNode->node);
    }
}

void
mmVKTFScene_DiscardRootNode(
    struct mmVKTFScene*                            p,
    struct mmVKNode*                               node)
{
    size_t i = 0;
    struct mmVKTFNode* pVKTFNode = NULL;
    struct mmVectorVpt* nodes0 = &p->nodes;
    for (i = 0; i < nodes0->size; ++i)
    {
        pVKTFNode = (struct mmVKTFNode*)mmVectorVpt_At(nodes0, i);
        mmVKNode_RmvChild(node, &pVKTFNode->node);
    }
}

void
mmVKTFScenes_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFScene_Init;
    hValueAllocator.Recycle = &mmVKTFScene_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFScene));
}

void
mmVKTFScenes_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFScenes_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFScene* pVKTFScene = NULL;
        struct mmGLTFScene* pGLTFScene = NULL;

        struct mmGLTFRoot* root = &pModel->root;
        struct mmVectorValue* scenes0 = p;
        struct mmVectorValue* scenes1 = &root->scenes;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFScene) && "invalid type");

        if (0 == scenes1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(scenes0, scenes1->size);

        for (i = 0; i < scenes1->size; ++i)
        {
            pVKTFScene = (struct mmVKTFScene*)mmVectorValue_At(scenes0, i);
            pGLTFScene = (struct mmGLTFScene*)mmVectorValue_At(scenes1, i);
            err = mmVKTFScene_Prepare(pVKTFScene, pModel, pGLTFScene, nodes);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFScene_Prepare failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKTFScenes_Discard(
    struct mmVectorValue*                          p)
{
    size_t i = 0;
    struct mmVKTFScene* pVKTFScene = NULL;

    struct mmVectorValue* scenes0 = p;

    assert(p->element == sizeof(struct mmVKTFScene) && "invalid type");

    for (i = 0; i < scenes0->size; ++i)
    {
        pVKTFScene = (struct mmVKTFScene*)mmVectorValue_At(scenes0, i);
        mmVKTFScene_Discard(pVKTFScene);
    }

    mmVectorValue_Reset(scenes0);
}

void
mmVKTFAnimationSampler_Init(
    struct mmVKTFAnimationSampler*                 p)
{
    mmVKTFAccessor_Init(&p->input);
    mmVKTFAccessor_Init(&p->output);
    p->interpolation = mmGLTFInterpolationTypeLinear;
}

void
mmVKTFAnimationSampler_Destroy(
    struct mmVKTFAnimationSampler*                 p)
{
    p->interpolation = mmGLTFInterpolationTypeLinear;
    mmVKTFAccessor_Destroy(&p->output);
    mmVKTFAccessor_Destroy(&p->input);
}

VkResult
mmVKTFAnimationSampler_IsValid(
    const struct mmVKTFAnimationSampler*           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    do
    {
        err = mmVKTFAccessor_IsValid(&p->input);
        if (VK_SUCCESS != err)
        {
            break;
        }
        err = mmVKTFAccessor_IsValid(&p->output);
        if (VK_SUCCESS != err)
        {
            break;
        }
    } while (0);
    return err;
}

float
mmVKTFAnimationSampler_GetMaxTime(
    const struct mmVKTFAnimationSampler*           p)
{
    float hMaxTime = 0.0f;

    do
    {
        size_t count;
        size_t last;
        const float* iBuffer;
        if (VK_SUCCESS != mmVKTFAnimationSampler_IsValid(p))
        {
            break;
        }
        count = p->input.accessor->count;
        if (0 == count)
        {
            break;
        }
        iBuffer = (const float*)mmVKTFAccessor_GetData(&p->input);
        last = count - 1;
        hMaxTime = iBuffer[last];
    } while (0);

    return hMaxTime;
}

VkResult
mmVKTFAnimationSampler_Prepare(
    struct mmVKTFAnimationSampler*                 p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimationSampler*                 pGLTFAnimationSampler)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmGLTFRoot* pGLTFRoot = &pModel->root;

        struct mmLogger* gLogger = mmLogger_Instance();

        p->interpolation = pGLTFAnimationSampler->interpolation;

        err = mmVKTFAccessor_Prepare(
            &p->input,
            pGLTFRoot,
            pGLTFAnimationSampler->input);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAccessor_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFAccessor_Prepare(
            &p->output,
            pGLTFRoot,
            pGLTFAnimationSampler->output);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAccessor_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKTFAnimationSampler_Discard(
    struct mmVKTFAnimationSampler*                 p)
{
    p->interpolation = mmGLTFInterpolationTypeLinear;
    mmVKTFAccessor_Discard(&p->output);
    mmVKTFAccessor_Discard(&p->input);
}

void
mmVKTFAnimationSamplers_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFAnimationSampler_Init;
    hValueAllocator.Recycle = &mmVKTFAnimationSampler_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFAnimationSampler));
}

void
mmVKTFAnimationSamplers_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFAnimationSamplers_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFAnimationSampler* pVKTFAnimationSampler = NULL;
        struct mmGLTFAnimationSampler* pGLTFAnimationSampler = NULL;

        struct mmVectorValue* samplers0 = p;
        struct mmVectorValue* samplers1 = &pGLTFAnimation->samplers;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFAnimationSampler) && "invalid type");

        if (0 == samplers1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(p, samplers1->size);

        for (i = 0; i < samplers1->size; ++i)
        {
            pVKTFAnimationSampler = (struct mmVKTFAnimationSampler*)mmVectorValue_At(samplers0, i);
            pGLTFAnimationSampler = (struct mmGLTFAnimationSampler*)mmVectorValue_At(samplers1, i);

            err = mmVKTFAnimationSampler_Prepare(pVKTFAnimationSampler, pModel, pGLTFAnimationSampler);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFAnimationSampler_Prepare failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

void
mmVKTFAnimationSamplers_Discard(
    struct mmVectorValue*                          p)
{
    size_t i = 0;
    struct mmVKTFAnimationSampler* pVKTFAnimationSampler = NULL;

    struct mmVectorValue* samplers0 = p;

    assert(p->element == sizeof(struct mmVKTFAnimationSampler) && "invalid type");

    for (i = 0; i < samplers0->size; ++i)
    {
        pVKTFAnimationSampler = (struct mmVKTFAnimationSampler*)mmVectorValue_At(samplers0, i);

        mmVKTFAnimationSampler_Discard(pVKTFAnimationSampler);
    }

    mmVectorValue_Reset(p);
}

void
mmVKTFAnimationChannel_Init(
    struct mmVKTFAnimationChannel*                 p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAnimationChannel));
}

void
mmVKTFAnimationChannel_Destroy(
    struct mmVKTFAnimationChannel*                 p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAnimationChannel));
}

VkResult
mmVKTFAnimationChannel_Prepare(
    struct mmVKTFAnimationChannel*                 p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimationChannel*                 pGLTFAnimationChannel,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        int node = pGLTFAnimationChannel->target.node;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (-1 == node)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " target.node is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        p->node = (struct mmVKTFNode*)mmVectorValue_At(nodes, node);
        p->path = pGLTFAnimationChannel->target.path;
        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFAnimationChannel_Discard(
    struct mmVKTFAnimationChannel*                 p)
{
    mmMemset(p, 0, sizeof(struct mmVKTFAnimationChannel));
}

void
mmVKTFAnimationChannel_SetTime(
    struct mmVKTFAnimationChannel*                 p,
    float                                          time,
    float                                          maxTime)
{
    if (NULL != p->sampler && NULL != p->node)
    {
        struct mmVKTFAnimationSampler* sampler = p->sampler;
        struct mmVKTFNode* node = p->node;

        struct mmGLTFAccessor* accessorI = sampler->input.accessor;
        struct mmGLTFAccessor* accessorO = sampler->output.accessor;

        // mmVKTFAccessor_GetData will assert buffer is valid.
        const float* iBuffer = (const float*)mmVKTFAccessor_GetData(&sampler->input);
        const uint8_t* oBuffer = (const uint8_t*)mmVKTFAccessor_GetData(&sampler->output);

        enum mmGLTFInterpolation_t interpolation = sampler->interpolation;
        enum mmGLTFAnimationPath_t path = p->path;
        enum mmGLTFAccessor_t type = accessorO->type;
        enum mmGLTFComponent_t componentType = accessorO->componentType;
        mmBool_t normalized = accessorO->normalized;

        int32_t numComponents = mmGLTFGetNumComponentsInType(type);;
        int32_t componentSize = mmGLTFGetComponentSizeInBytes(componentType);
        int32_t elementSize = numComponents * componentSize;

        float t0, t1, td, tc, t;

        // we already check the buffer and accessor.
        assert(!(NULL == accessorI || NULL == accessorO) && "accessor is invalid");
        assert(0 <= accessorI->count && "Accessor->count is invalid");

        if (1 >= accessorI->count)
        {
            // key frame == 1
            p->prevKey = 0;
            p->prevTime = iBuffer[0];

            switch (path)
            {
            case mmGLTFAnimationPathTypeTranslation:
            {
                // "translation" "VEC3"   float                       XYZ translation vector
                float vi[3];
                mmGLTFElementReadVectorFloat(
                    oBuffer,
                    numComponents,
                    componentSize,
                    componentType,
                    normalized,
                    vi);

                mmVKNode_SetPosition(&node->node, vi);
                //printf("translation (%f, %f, %f)\n", vi[0], vi[1], vi[2]);
            }
            break;
            case mmGLTFAnimationPathTypeRotation:
            {
                // "rotation"    "VEC4"   float                       XYZW rotation quaternion
                //                        signed byte normalized
                //                        unsigned byte normalized
                //                        signed short normalized
                //                        unsigned short normalized
                float vi[4];
                mmGLTFElementReadVectorFloat(
                    oBuffer,
                    numComponents,
                    componentSize,
                    componentType,
                    normalized,
                    vi);

                mmVKNode_SetQuaternion(&node->node, vi);
                // printf("rotation (%f, %f, %f, %f)\n", vi[0], vi[1], vi[2], vi[3]);
            }
            break;
            case mmGLTFAnimationPathTypeScale:
            {
                // "scale"       "VEC3"   float                       XYZ scale vector
                float vi[3];
                mmGLTFElementReadVectorFloat(
                    oBuffer,
                    numComponents,
                    componentSize,
                    componentType,
                    normalized,
                    vi);

                mmVKNode_SetScale(&node->node, vi);
                //printf("scale (%f, %f, %f)\n", v[0], v[1], v[2]);
            }
            break;
            case mmGLTFAnimationPathTypeWeights:
            {
                // "weights"     "SCALAR" float                       Weights of morph targets
                //                        signed byte normalized
                //                        unsigned byte normalized
                //                        signed short normalized
                //                        unsigned short normalized
                float* vi = (float*)(node->weights.arrays);
                int32_t size = (int32_t)(node->weights.size);
                mmGLTFElementReadVectorFloat(
                    oBuffer,
                    numComponents * size,
                    componentSize,
                    componentType,
                    normalized,
                    vi);

                //printf("weights (%f)\n", v[0]);
            }
            break;
            default:
                break;
            }
        }
        else
        {
            // key frame >= 2
            size_t prevKey = p->prevKey;
            size_t nextKey = 0;
            size_t i;
            size_t count = accessorI->count;

            t = fmodf(time, maxTime);
            t = mmMathClamp(t, iBuffer[0], iBuffer[count - 1]);

            if (p->prevTime > t)
            {
                prevKey = 0;
            }

            p->prevTime = t;

            // Find next keyframe: min{ t of input | t > prevKey }
            for (i = prevKey; i < count; ++i)
            {
                if (t <= iBuffer[i])
                {
                    nextKey = mmClamp(i, 1, count - 1);
                    break;
                }
            }
            prevKey = mmClamp(nextKey - 1, 0, nextKey);
            p->prevKey = prevKey;

            t0 = iBuffer[prevKey];
            t1 = iBuffer[nextKey];
            td = t1 - t0;
            tc = mmMathClamp(time, iBuffer[0], iBuffer[count - 1]);
            t = (0 == td) ? 0.0f : ((tc - t0) / td);

            switch (path)
            {
            case mmGLTFAnimationPathTypeTranslation:
            {
                // "translation" "VEC3"   float                       XYZ translation vector
                float vi[3];

                switch (interpolation)
                {
                case mmGLTFInterpolationTypeLinear:
                {
                    float v0[3];
                    float v1[3];

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v0);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * nextKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v1);

                    mmVec3Lerp(vi, v0, v1, t);
                }
                break;
                case mmGLTFInterpolationTypeStep:
                {
                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vi);
                }
                break;
                case mmGLTFInterpolationTypeCubicSpline:
                {
                    float vk[3];// V curr v(k  ) key frame value
                    float ak[3];// A curr a(k  )
                    float bk[3];// B curr b(k  )
                    float vn[3];// N next v(k+1) key frame value

                    float t0[3];
                    float t1[3];

                    size_t idx0 = (prevKey * 3    );// A curr a(k  )
                    size_t idx1 = (prevKey * 3 + 1);// V curr v(k  ) key frame value
                    size_t idx2 = (prevKey * 3 + 2);// B curr b(k  )
                    size_t idx3 = (nextKey * 3 + 1);// N next v(k+1) key frame value

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx0,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        ak);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx1,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx2,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        bk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx3,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vn);

                    // Cubic Spline Interpolation.
                    mmVec3Scale(t0, bk, td);
                    mmVec3Scale(t1, ak, td);
                    mmVec3Hermite(vi, t, vk, t0, t1, vn);
                }
                break;
                default:
                break;
                }

                mmVKNode_SetPosition(&node->node, vi);
                //printf("translation (%f, %f, %f)\n", vi[0], vi[1], vi[2]);
            }
            break;
            case mmGLTFAnimationPathTypeRotation:
            {
                // "rotation"    "VEC4"   float                       XYZW rotation quaternion
                //                        signed byte normalized
                //                        unsigned byte normalized
                //                        signed short normalized
                //                        unsigned short normalized
                float vi[4];

                switch (interpolation)
                {
                case mmGLTFInterpolationTypeLinear:
                {
                    float v0[4];
                    float v1[4];

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v0);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * nextKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v1);

                    mmQuatSlerp(vi, v0, v1, t);
                    mmQuatNormalize(vi, vi);
                }
                break;
                case mmGLTFInterpolationTypeStep:
                {
                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vi);
                }
                break;
                case mmGLTFInterpolationTypeCubicSpline:
                {
                    float vk[4];// V curr v(k  ) key frame value
                    float ak[4];// A curr a(k  )
                    float bk[4];// B curr b(k  )
                    float vn[4];// N next v(k+1) key frame value

                    float t0[4];
                    float t1[4];

                    size_t idx0 = (prevKey * 3    );// A curr a(k  )
                    size_t idx1 = (prevKey * 3 + 1);// V curr v(k  ) key frame value
                    size_t idx2 = (prevKey * 3 + 2);// B curr b(k  )
                    size_t idx3 = (nextKey * 3 + 1);// N next v(k+1) key frame value

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx0,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        ak);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx1,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx2,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        bk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx3,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vn);

                    // Cubic Spline Interpolation.
                    mmVec4Scale(t0, bk, td);
                    mmVec4Scale(t1, ak, td);
                    mmVec4Hermite(vi, t, vk, t0, t1, vn);
                    mmQuatNormalize(vi, vi);

                    // rotation Cubic Spline Interpolation.
                    // https://github.com/KhronosGroup/glTF/issues/2008
                    //
                    // mmQuatSlerp(vi, vk, vn, t);
                }
                break;
                default:
                break;
                }

                mmVKNode_SetQuaternion(&node->node, vi);
                // printf("rotation (%f, %f, %f, %f)\n", vi[0], vi[1], vi[2], vi[3]);
            }
            break;
            case mmGLTFAnimationPathTypeScale:
            {
                // "scale"       "VEC3"   float                       XYZ scale vector
                float vi[3];

                switch (interpolation)
                {
                case mmGLTFInterpolationTypeLinear:
                {
                    float v0[3];
                    float v1[3];

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v0);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * nextKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        v1);

                    mmVec3Lerp(vi, v0, v1, t);
                }
                break;
                case mmGLTFInterpolationTypeStep:
                {
                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * prevKey,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vi);
                }
                break;
                case mmGLTFInterpolationTypeCubicSpline:
                {
                    float vk[3];// V curr v(k  ) key frame value
                    float ak[3];// A curr a(k  )
                    float bk[3];// B curr b(k  )
                    float vn[3];// N next v(k+1) key frame value

                    float t0[3];
                    float t1[3];

                    size_t idx0 = (prevKey * 3    );// A curr a(k  )
                    size_t idx1 = (prevKey * 3 + 1);// V curr v(k  ) key frame value
                    size_t idx2 = (prevKey * 3 + 2);// B curr b(k  )
                    size_t idx3 = (nextKey * 3 + 1);// N next v(k+1) key frame value

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx0,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        ak);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx1,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx2,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        bk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * idx3,
                        numComponents,
                        componentSize,
                        componentType,
                        normalized,
                        vn);

                    // Cubic Spline Interpolation.
                    mmVec3Scale(t0, bk, td);
                    mmVec3Scale(t1, ak, td);
                    mmVec3Hermite(vi, t, vk, t0, t1, vn);
                }
                break;
                default:
                break;
                }

                mmVKNode_SetScale(&node->node, vi);
                //printf("scale (%f, %f, %f)\n", v[0], v[1], v[2]);
            }
            break;
            case mmGLTFAnimationPathTypeWeights:
            {
                // "weights"     "SCALAR" float                       Weights of morph targets
                //                        signed byte normalized
                //                        unsigned byte normalized
                //                        signed short normalized
                //                        unsigned short normalized
                float* vi = (float*)(node->weights.arrays);
                int32_t size = (int32_t)(node->weights.size);

                switch (interpolation)
                {
                case mmGLTFInterpolationTypeLinear:
                {
                    float* v0 = (float*)mmMalloc(size * sizeof(float));
                    float* v1 = (float*)mmMalloc(size * sizeof(float));

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * prevKey,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        v0);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * nextKey,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        v1);

                    for (i = 0; i < size; i++)
                    {
                        vi[i] = mmMathLerp(v0[i], v1[i], t);
                    }

                    mmFree(v1);
                    mmFree(v0);
                }
                break;
                case mmGLTFInterpolationTypeStep:
                {
                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * prevKey,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        vi);
                }
                break;
                case mmGLTFInterpolationTypeCubicSpline:
                {
                    float* vk = (float*)mmMalloc(size * sizeof(float));
                    float* ak = (float*)mmMalloc(size * sizeof(float));
                    float* bk = (float*)mmMalloc(size * sizeof(float));
                    float* vn = (float*)mmMalloc(size * sizeof(float));

                    float t0;
                    float t1;

                    size_t idx0 = (prevKey * 3    );// A curr a(k  )
                    size_t idx1 = (prevKey * 3 + 1);// V curr v(k  ) key frame value
                    size_t idx2 = (prevKey * 3 + 2);// B curr b(k  )
                    size_t idx3 = (nextKey * 3 + 1);// N next v(k+1) key frame value

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * idx0,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        ak);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * idx1,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        vk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * idx2,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        bk);

                    mmGLTFElementReadVectorFloat(
                        oBuffer + elementSize * size * idx3,
                        numComponents * size,
                        componentSize,
                        componentType,
                        normalized,
                        vn);

                    // Cubic Spline Interpolation.
                    for (i = 0; i < size; i++)
                    {
                        t0 = bk[i] * td;
                        t1 = ak[i] * td;
                        vi[i] = mmMathHermite(t, vk[i], t0, t1, vn[i]);
                    }

                    mmFree(vn);
                    mmFree(bk);
                    mmFree(ak);
                    mmFree(vk);
                }
                break;
                default:
                break;
                }

                //printf("weights (%f)\n", v[0]);
            }
            break;
            default:
            break;
            }
        }
    }
}

void
mmVKTFAnimationChannels_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFAnimationChannel_Init;
    hValueAllocator.Recycle = &mmVKTFAnimationChannel_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFAnimationChannel));
}

void
mmVKTFAnimationChannels_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFAnimationChannels_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFAnimationChannel* pVKTFAnimationChannel = NULL;
        struct mmGLTFAnimationChannel* pGLTFAnimationChannel = NULL;

        struct mmVectorValue* channels0 = p;
        struct mmVectorValue* channels1 = &pGLTFAnimation->channels;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFAnimationChannel) && "invalid type");

        if (0 == channels1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(p, channels1->size);

        for (i = 0; i < channels1->size; ++i)
        {
            pVKTFAnimationChannel = (struct mmVKTFAnimationChannel*)mmVectorValue_At(channels0, i);
            pGLTFAnimationChannel = (struct mmGLTFAnimationChannel*)mmVectorValue_At(channels1, i);

            err = mmVKTFAnimationChannel_Prepare(
                pVKTFAnimationChannel,
                pModel,
                pGLTFAnimationChannel,
                nodes);

            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFAnimationChannel_Prepare failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFAnimationChannels_Discard(
    struct mmVectorValue*                          p)
{
    size_t i = 0;
    struct mmVKTFAnimationChannel* pVKTFAnimationChannel = NULL;

    struct mmVectorValue* channels0 = p;

    assert(p->element == sizeof(struct mmVKTFAnimationChannel) && "invalid type");

    for (i = 0; i < channels0->size; ++i)
    {
        pVKTFAnimationChannel = (struct mmVKTFAnimationChannel*)mmVectorValue_At(channels0, i);

        mmVKTFAnimationChannel_Discard(pVKTFAnimationChannel);
    }

    mmVectorValue_Reset(p);
}

void
mmVKTFAnimation_Init(
    struct mmVKTFAnimation*                        p)
{
    mmString_Init(&p->name);
    mmVKTFAnimationSamplers_Init(&p->samplers);
    mmVKTFAnimationChannels_Init(&p->channels);
    p->loop = 1;
    p->state = 0;
    p->currentTime = 0.0f;
    p->maxTime = 0.0f;
    p->speed = 1.0f;
}

void
mmVKTFAnimation_Destroy(
    struct mmVKTFAnimation*                        p)
{
    p->speed = 1.0f;
    p->maxTime = 0.0f;
    p->currentTime = 0.0f;
    p->state = 0;
    p->loop = 1;
    mmVKTFAnimationChannels_Destroy(&p->channels);
    mmVKTFAnimationSamplers_Destroy(&p->samplers);
    mmString_Destroy(&p->name);
}

VkResult
mmVKTFAnimation_Prepare(
    struct mmVKTFAnimation*                        p,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFAnimation*                        pGLTFAnimation,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmGLTFAnimationChannel* pGLTFAnimationChannel = NULL;
        struct mmVKTFAnimationChannel* pVKTFAnimationChannel = NULL;
        struct mmVKTFAnimationSampler* pVKTFAnimationSampler = NULL;
        struct mmVKTFNode* pVKTFNode = NULL;

        struct mmVectorValue* channels0 = &p->channels;
        struct mmVectorValue* channels1 = &pGLTFAnimation->channels;
        struct mmVectorValue* samplers0 = &p->samplers;

        int sampler = -1;
        int node = -1;

        float hMaxTime = 0.0f;

        struct mmParseJSON* parse = &pModel->parse;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmParseJSON_GetRangeString(parse, &pGLTFAnimation->name, &p->name);

        err = mmVKTFAnimationChannels_Prepare(&p->channels, pModel, pGLTFAnimation, nodes);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAnimationChannels_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFAnimationSamplers_Prepare(&p->samplers, pModel, pGLTFAnimation);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAnimationSamplers_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        for (i = 0; i < channels0->size; ++i)
        {
            pVKTFAnimationChannel = (struct mmVKTFAnimationChannel*)mmVectorValue_At(channels0, i);
            pGLTFAnimationChannel = (struct mmGLTFAnimationChannel*)mmVectorValue_At(channels1, i);

            sampler = pGLTFAnimationChannel->sampler;
            node = pGLTFAnimationChannel->target.node;

            if (sampler < 0 || node < 0)
            {
                continue;
            }

            pVKTFAnimationSampler = (struct mmVKTFAnimationSampler*)mmVectorValue_At(samplers0, sampler);
            pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes, node);

            hMaxTime = mmVKTFAnimationSampler_GetMaxTime(pVKTFAnimationSampler);
            p->maxTime = mmMathMax(hMaxTime, p->maxTime);

            pVKTFAnimationChannel->sampler = pVKTFAnimationSampler;
            pVKTFAnimationChannel->node = pVKTFNode;
        }

        err = VK_SUCCESS;
    } while (0);

    return err;
}

void
mmVKTFAnimation_Discard(
    struct mmVKTFAnimation*                        p)
{
    mmVKTFAnimationChannels_Discard(&p->channels);
    mmVKTFAnimationSamplers_Discard(&p->samplers);

    mmString_Reset(&p->name);
}

void
mmVKTFAnimation_SetTime(
    struct mmVKTFAnimation*                        p,
    float                                          time)
{
    size_t i = 0;

    struct mmVKTFAnimationChannel* pVKTFAnimationChannel = NULL;

    struct mmVectorValue* channels0 = &p->channels;

    for (i = 0; i < channels0->size; ++i)
    {
        pVKTFAnimationChannel = (struct mmVKTFAnimationChannel*)mmVectorValue_At(channels0, i);

        mmVKTFAnimationChannel_SetTime(
            pVKTFAnimationChannel,
            time,
            p->maxTime);
    }
}

void
mmVKTFAnimation_Update(
    struct mmVKTFAnimation*                        p,
    float                                          deltaTime)
{
    if (1 == p->state)
    {
        p->currentTime += (deltaTime * p->speed);

        if ((p->currentTime >= p->maxTime) && (0 == p->loop))
        {
            p->state = 0;
            p->currentTime = p->maxTime;
        }
        else
        {
            p->currentTime = fmodf(p->currentTime, p->maxTime);
        }

        mmVKTFAnimation_SetTime(p, p->currentTime);
    }
}

void
mmVKTFAnimation_Play(
    struct mmVKTFAnimation*                        p)
{
    p->state = 1;
    p->currentTime = 0.0f;
    mmVKTFAnimation_SetTime(p, p->currentTime);
}

void
mmVKTFAnimation_Stop(
    struct mmVKTFAnimation*                        p)
{
    p->state = 0;
}

void
mmVKTFAnimation_Pause(
    struct mmVKTFAnimation*                        p)
{
    p->state = 0;
}

void
mmVKTFAnimation_Resume(
    struct mmVKTFAnimation*                        p)
{
    p->state = 1;
}

void
mmVKTFAnimation_Rewind(
    struct mmVKTFAnimation*                        p)
{
    p->currentTime = 0.0f;
    mmVKTFAnimation_SetTime(p, p->currentTime);
}

void
mmVKTFAnimations_Init(
    struct mmVectorValue*                          p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &mmVKTFAnimation_Init;
    hValueAllocator.Recycle = &mmVKTFAnimation_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct mmVKTFAnimation));
}

void
mmVKTFAnimations_Destroy(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Destroy(p);
}

VkResult
mmVKTFAnimations_Prepare(
    struct mmVectorValue*                          p,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          nodes)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFAnimation* pVKTFAnimation = NULL;
        struct mmGLTFAnimation* pGLTFAnimation = NULL;

        struct mmGLTFRoot* root = &pModel->root;
        struct mmVectorValue* animations0 = p;
        struct mmVectorValue* animations1 = &root->animations;

        struct mmLogger* gLogger = mmLogger_Instance();

        assert(p->element == sizeof(struct mmVKTFAnimation) && "invalid type");

        if (0 == animations1->size)
        {
            err = VK_SUCCESS;
            break;
        }

        mmVectorValue_AllocatorMemory(animations0, animations1->size);

        for (i = 0; i < animations1->size; ++i)
        {
            pVKTFAnimation = (struct mmVKTFAnimation*)mmVectorValue_At(animations0, i);
            pGLTFAnimation = (struct mmGLTFAnimation*)mmVectorValue_At(animations1, i);
            err = mmVKTFAnimation_Prepare(pVKTFAnimation, pModel, pGLTFAnimation, nodes);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFAnimation_PrepareAnimation failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

    } while (0);

    return err;
}

void
mmVKTFAnimations_Discard(
    struct mmVectorValue*                          p)
{
    size_t i = 0;
    struct mmVKTFAnimation* pVKTFAnimation = NULL;

    struct mmVectorValue* animations0 = p;

    assert(p->element == sizeof(struct mmVKTFAnimation) && "invalid type");

    for (i = 0; i < animations0->size; ++i)
    {
        pVKTFAnimation = (struct mmVKTFAnimation*)mmVectorValue_At(animations0, i);
        mmVKTFAnimation_Discard(pVKTFAnimation);
    }

    mmVectorValue_Reset(animations0);
}

void
mmVKTFAnimations_MakeAnimationMap(
    const struct mmVectorValue*                    p,
    struct mmRbtreeStrVpt*                         m)
{
    size_t i = 0;
    const struct mmVKTFAnimation* pVKTFAnimation = NULL;

    const struct mmVectorValue* animations0 = p;

    assert(p->element == sizeof(struct mmVKTFAnimation) && "invalid type");

    mmRbtreeStrVpt_Clear(m);

    for (i = 0; i < animations0->size; ++i)
    {
        pVKTFAnimation = (const struct mmVKTFAnimation*)mmVectorValue_At(animations0, i);

        mmRbtreeStrVpt_Set(m, mmString_CStr(&pVKTFAnimation->name), (void*)pVKTFAnimation);
    }
}

void
mmVKTFObject_Init(
    struct mmVKTFObject*                           p)
{
    p->pModelInfo = NULL;
    p->pPoolInfo = NULL;

    mmVKNode_Init(&p->node);
    
    mmVKTFNodes_Init(&p->nodes);
    mmVKTFAnimations_Init(&p->animations);
    mmVKTFScenes_Init(&p->scenes);

    mmRbtreeStrVpt_Init(&p->animationsMap);
}

void
mmVKTFObject_Destroy(
    struct mmVKTFObject*                           p)
{
    mmRbtreeStrVpt_Destroy(&p->animationsMap);

    mmVKTFScenes_Destroy(&p->scenes);
    mmVKTFAnimations_Destroy(&p->animations);
    mmVKTFNodes_Destroy(&p->nodes);
    
    mmVKNode_Destroy(&p->node);

    p->pPoolInfo = NULL;
    p->pModelInfo = NULL;
}

void
mmVKTFObject_UpdateAnimations(
    struct mmVKTFObject*                           p,
    float                                          deltaTime)
{
    size_t i = 0;
    struct mmVKTFAnimation* pVKTFAnimation = NULL;

    struct mmVectorValue* animations1 = &p->animations;

    for (i = 0; i < animations1->size; ++i)
    {
        pVKTFAnimation = (struct mmVKTFAnimation*)mmVectorValue_At(animations1, i);
        mmVKTFAnimation_Update(pVKTFAnimation, deltaTime);
    }
}

VkResult
mmVKTFObject_UpdateJoins(
    struct mmVKTFObject*                           p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i = 0;
        struct mmVKTFNode* pVKTFNode = NULL;

        struct mmVectorValue* nodes1 = &p->nodes;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKTFModelInfo_Invalid(p->pModelInfo))
        {
            // pModelInfo is invalid. not need update joins.
            break;
        }

        for (i = 0; i < nodes1->size; ++i)
        {
            pVKTFNode = (struct mmVKTFNode*)mmVectorValue_At(nodes1, i);
            err = mmVKTFNode_UpdateJoins(pVKTFNode, pUploader, &p->nodes, &p->pModelInfo->vModel.skins);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " mmVKTFNode_UpdateJoins failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    return err;
}

VkResult
mmVKTFObject_Update(
    struct mmVKTFObject*                           p,
    struct mmVKUploader*                           pUploader,
    float                                          interval)
{
    mmVKTFObject_UpdateAnimations(p, interval);
    return mmVKTFObject_UpdateJoins(p, pUploader);
}

struct mmVKTFAnimation*
mmVKTFObject_GetAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name)
{
    struct mmVKTFAnimation* pVKTFAnimation = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->animationsMap, name);
    if (NULL != it)
    {
        pVKTFAnimation = (struct mmVKTFAnimation*)it->v;
    }
    return pVKTFAnimation;
}

struct mmVKTFAnimation*
mmVKTFObject_PlayAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name)
{
    struct mmVKTFAnimation* pVKTFAnimation = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->animationsMap, name);
    if (NULL != it)
    {
        pVKTFAnimation = (struct mmVKTFAnimation*)it->v;
        mmVKTFAnimation_Play(pVKTFAnimation);
    }
    return pVKTFAnimation;
}

struct mmVKTFAnimation*
mmVKTFObject_StopAnimation(
    struct mmVKTFObject*                           p,
    const char*                                    name)
{
    struct mmVKTFAnimation* pVKTFAnimation = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    it = mmRbtreeStrVpt_GetIterator(&p->animationsMap, name);
    if (NULL != it)
    {
        pVKTFAnimation = (struct mmVKTFAnimation*)it->v;
        mmVKTFAnimation_Stop(pVKTFAnimation);
    }
    return pVKTFAnimation;
}

void
mmVKTFObject_StopAllAnimation(
    struct mmVKTFObject*                           p)
{
    struct mmVKTFAnimation* pVKTFAnimation = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    n = mmRb_First(&p->animationsMap.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStrVptIterator, n);
        n = mmRb_Next(n);
        pVKTFAnimation = (struct mmVKTFAnimation*)it->v;
        mmVKTFAnimation_Stop(pVKTFAnimation);
    }
}

void
mmVKTFObject_PrepareRootNode(
    struct mmVKTFObject*                           p)
{
    do
    {
        struct mmGLTFModel* pModel = NULL;
        struct mmGLTFRoot* pGLTFRoot = NULL;
        struct mmVKTFScene* pVKTFScene = NULL;
        struct mmVectorValue* scenes1 = NULL;

        if (mmVKTFModelInfo_Invalid(p->pModelInfo))
        {
            // pModelInfo is invalid. not need update joins.
            break;
        }

        pModel = &p->pModelInfo->vModel.model;
        pGLTFRoot = &pModel->root;
        scenes1 = &p->scenes;

        if (0 > pGLTFRoot->scene)
        {
            // scene index is invalid.
            break;
        }

        if (pGLTFRoot->scene >= scenes1->size)
        {
            // scene index is out of range.
            break;
        }

        pVKTFScene = (struct mmVKTFScene*)mmVectorValue_At(scenes1, pGLTFRoot->scene);
        mmVKTFScene_PrepareRootNode(pVKTFScene, &p->node);
    } while (0);
}

void
mmVKTFObject_DiscardRootNode(
    struct mmVKTFObject*                           p)
{
    do
    {
        struct mmGLTFModel* pModel = NULL;
        struct mmGLTFRoot* pGLTFRoot = NULL;
        struct mmVKTFScene* pVKTFScene = NULL;
        struct mmVectorValue* scenes1 = NULL;

        if (mmVKTFModelInfo_Invalid(p->pModelInfo))
        {
            // pModelInfo is invalid. not need update joins.
            break;
        }

        pModel = &p->pModelInfo->vModel.model;
        pGLTFRoot = &pModel->root;
        scenes1 = &p->scenes;

        if (0 > pGLTFRoot->scene)
        {
            // scene index is invalid.
            break;
        }

        if (pGLTFRoot->scene >= scenes1->size)
        {
            // scene index is out of range.
            break;
        }

        pVKTFScene = (struct mmVKTFScene*)mmVectorValue_At(scenes1, pGLTFRoot->scene);
        mmVKTFScene_DiscardRootNode(pVKTFScene, &p->node);
    } while (0);
}

VkResult
mmVKTFObject_Prepare(
    struct mmVKTFObject*                           p,
    struct mmVKAssets*                             pAssets,
    const char*                                    path)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKUploader* pUploader = NULL;
        struct mmVKDescriptorPool* pDescriptorPool = NULL;
        struct mmVKPoolLoader* pPoolLoader = NULL;
        struct mmVKTFModelLoader* pModelLoader = NULL;
        struct mmVKTFModel* pVKTFModel = NULL;
        const char* pool = "";

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pool = mmVKJointsPoolName;
        pPoolLoader = &pAssets->vPoolLoader;
        p->pPoolInfo = mmVKPoolLoader_GetFromName(pPoolLoader, pool);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo);
        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;

        pModelLoader = &pAssets->vModelLoader;
        p->pModelInfo = mmVKTFModelLoader_GetFromPath(pModelLoader, path);
        if (mmVKTFModelInfo_Invalid(p->pModelInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pModelInfo is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pModelInfo = NULL;
            break;
        }
        mmVKTFModelInfo_Increase(p->pModelInfo);
        pVKTFModel = &p->pModelInfo->vModel;

        err = mmVKTFNodes_Prepare(&p->nodes, pUploader, pVKTFModel, pDescriptorPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFNodes_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFAnimations_Prepare(&p->animations, &pVKTFModel->model, &p->nodes);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFAnimations_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKTFScenes_Prepare(&p->scenes, &pVKTFModel->model, &p->nodes);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFScenes_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKTFAnimations_MakeAnimationMap(&p->animations, &p->animationsMap);
        mmVKTFObject_PrepareRootNode(p);

    } while (0);

    return err;
}

void
mmVKTFObject_Discard(
    struct mmVKTFObject*                           p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        struct mmVKUploader* pUploader = NULL;
        struct mmVKDescriptorPool* pDescriptorPool = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (mmVKPoolInfo_Invalid(p->pPoolInfo))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        pDescriptorPool = &p->pPoolInfo->vDescriptorPool;

        mmVKTFObject_DiscardRootNode(p);

        mmVKTFScenes_Discard(&p->scenes);
        mmVKTFAnimations_Discard(&p->animations);
        mmVKTFNodes_Discard(&p->nodes, pUploader, pDescriptorPool);

        mmVKTFModelInfo_Decrease(p->pModelInfo);
        p->pModelInfo = NULL;

        mmVKPoolInfo_Decrease(p->pPoolInfo);
        p->pPoolInfo = NULL;
    } while (0);
}

void
mmVKTFPrimitive_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode,
    struct mmVKTFPrimitive*                        pVKTFPrimitive)
{
    uint32_t hPipeline = pScene->hIndex.Pipeline;
    struct mmVKTFMaterial* pVKTFMaterial = pVKTFPrimitive->material;
    struct mmVKPipelineInfo* pPipelineInfo = pVKTFPrimitive->pPipelineInfo[hPipeline];
    struct mmVKPipeline* pPipeline = &pPipelineInfo->vPipeline;
    VkPipelineLayout pipelineLayout = pPipeline->pipelineLayout;
    VkPipeline pipeline = pPipeline->pipeline;

    VkDescriptorSet descriptorSets[3] =
    {
        pScene->vDescriptorSet[hPipeline].descriptorSet,
        pVKTFMaterial->vDescriptorSet.descriptorSet,
        pVKTFNode->vDescriptorSet.descriptorSet,
    };
    uint32_t descriptorSetCount = (0 != (pVKTFPrimitive->hTypeId & mmVKTFAttributeSkinsMask)) ? 3 : 2;

    uint32_t hBindingCount = 0;
    VkBuffer buffers[mmVKTFAttributeMax] = { 0 };
    VkDeviceSize offsets[mmVKTFAttributeMax] = { 0 };
    uint32_t hMaskId = mmVKTFPrimitiveMaskId(hPipeline);
    uint32_t hPushConstantsSize = mmVKTFPrimitivePushConstantsSize(hPipeline);
    mmVKTFPrimitive_MakeBuffers(pVKTFPrimitive, hMaskId, buffers, &hBindingCount);
    pVKTFNode->pushConstant.idxMat = pScene->hIndex.IdxMat;
    pVKTFNode->pushConstant.idxMap = pScene->hIndex.IdxMap;

    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
    vkCmdBindVertexBuffers(cmdBuffer, 0, hBindingCount, buffers, offsets);

    vkCmdBindDescriptorSets(
        cmdBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        descriptorSetCount,
        descriptorSets,
        0,
        NULL);

    vkCmdPushConstants(
        cmdBuffer,
        pipelineLayout,
        VK_SHADER_STAGE_VERTEX_BIT,
        0,
        hPushConstantsSize,
        &pVKTFNode->pushConstant);

    if (0 != pVKTFPrimitive->hIdxCount)
    {
        vkCmdBindIndexBuffer(cmdBuffer, pVKTFPrimitive->vBufferIdx.buffer, 0, pVKTFPrimitive->hIdxType);
        vkCmdDrawIndexed(cmdBuffer, pVKTFPrimitive->hIdxCount, 1, 0, 0, 0);
    }
    else
    {
        vkCmdDraw(cmdBuffer, pVKTFPrimitive->hVtxCount, 1, 0, 0);
    }
}

void
mmVKTFMesh_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode,
    struct mmVKTFMesh*                             pVKTFMesh)
{
    size_t i = 0;
    struct mmVKTFPrimitive* pVKTFPrimitive = NULL;

    struct mmVectorValue* primitives1 = &pVKTFMesh->primitives;

    for (i = 0; i < primitives1->size; ++i)
    {
        pVKTFPrimitive = (struct mmVKTFPrimitive*)mmVectorValue_At(primitives1, i);
        mmVKTFPrimitive_OnRecordCommand(
            cmdBuffer, 
            pScene,
            pVKTFNode, 
            pVKTFPrimitive);
    }
}

void
mmVKTFNode_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFNode*                             pVKTFNode)
{
    struct mmVKTFNode* pVKTFNodeChild = NULL;
    struct mmVKNode* child = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    struct mmListVpt* children = &pVKTFNode->node.children;
    struct mmVKTFMesh* pVKTFMesh = pVKTFNode->mesh;
    
    if (NULL != pVKTFMesh)
    {
        mmMat4x4Ref Normal = pVKTFNode->pushConstant.Normal;
        mmMat4x4Ref Model = pVKTFNode->pushConstant.Model;

        mmVKNode_GetWorldTransform(&pVKTFNode->node, Model);

        mmMat4x4Inverse(Normal, Model);
        mmMat4x4Transpose(Normal, Normal);

        mmVKTFMesh_OnRecordCommand(
            cmdBuffer,
            pScene,
            pVKTFNode,
            pVKTFMesh);
    }

    next = children->l.next;
    while (next != &children->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        child = (struct mmVKNode*)it->v;

        pVKTFNodeChild = mmContainerOf(child, struct mmVKTFNode, node);
        mmVKTFNode_OnRecordCommand(
            cmdBuffer, 
            pScene,
            pVKTFNodeChild);
    }
}

void
mmVKTFScene_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFScene*                            pVKTFScene)
{
    size_t i = 0;
    struct mmVKTFNode* pVKTFNode = NULL;
    struct mmVectorVpt* nodes0 = &pVKTFScene->nodes;
    for (i = 0; i < nodes0->size; ++i)
    {
        pVKTFNode = (struct mmVKTFNode*)mmVectorVpt_At(nodes0, i);

        mmVKTFNode_OnRecordCommand(
            cmdBuffer, 
            pScene,
            pVKTFNode);
    }
}

void
mmVKTFObject_OnRecordCommand(
    VkCommandBuffer                                cmdBuffer,
    struct mmVKScene*                              pScene,
    struct mmVKTFObject*                           pVKTFObject)
{
    do
    {
        struct mmGLTFModel* pModel = NULL;
        struct mmGLTFRoot* pGLTFRoot = NULL;
        struct mmVKTFScene* pVKTFScene = NULL;
        struct mmVectorValue* scenes1 = NULL;

        if (mmVKTFModelInfo_Invalid(pVKTFObject->pModelInfo))
        {
            // pModelInfo is invalid. not need update joins.
            break;
        }

        pModel = &pVKTFObject->pModelInfo->vModel.model;
        pGLTFRoot = &pModel->root;
        scenes1 = &pVKTFObject->scenes;

        if (0 > pGLTFRoot->scene)
        {
            // scene index is invalid.
            break;
        }

        if (pGLTFRoot->scene >= scenes1->size)
        {
            // scene index is out of range.
            break;
        }

        pVKTFScene = (struct mmVKTFScene*)mmVectorValue_At(scenes1, pGLTFRoot->scene);
        mmVKTFScene_OnRecordCommand(
            cmdBuffer,
            pScene,
            pVKTFScene);

    } while (0);
}
