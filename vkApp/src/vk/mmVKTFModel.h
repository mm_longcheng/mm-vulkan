/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKTFModel_h__
#define __mmVKTFModel_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorU32.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUploader.h"
#include "vk/mmVKUniformType.h"
#include "vk/mmVKDescriptorSet.h"

#include "parse/mmParseGLTF.h"

#include "ktx.h"

#include "core/mmPrefix.h"

struct mmFileContext;

struct mmVKDevice;
struct mmVKPipeline;
struct mmVKDescriptorPool;
struct mmVKUploader;
struct mmVKPoolLoader;
struct mmVKAssets;
struct mmVKAssetsDefault;
struct mmVKScene;
struct mmVKUBOScenePushConstant;
struct mmVKPipelineVertexInputInfo;

VkSamplerAddressMode
mmVKTFGetSamplerAddressMode(
    int32_t                                        wrapMode);

VkFilter
mmVKTFGetFilterMode(
    int32_t                                        filterMode);

VkSamplerMipmapMode
mmVKTFGetSamplerMipmapMode(
    VkFilter                                       magFilter,
    VkFilter                                       minFilter);

VkFormat
mmVKTFGetAccessorAttributeFormat(
    const struct mmGLTFAccessor*                   pGLTFAccessor);

uint32_t
mmVKTFGetAccessorAttributeSize(
    const struct mmGLTFAccessor*                   pGLTFAccessor);

uint32_t
mmVKTFGetFormatAttributeSize(
    VkFormat                                       format);

VkPrimitiveTopology
mmVKTFGetPrimitiveTopology(
    int                                            mode);

void
mmVKTFMakeSamplerCreateInfo(
    VkSamplerCreateInfo*                           info,
    const struct mmGLTFSampler*                    pGLTFSampler);

VkIndexType
mmVKTFGetIndexType(
    enum mmGLTFComponent_t                         componentType);

struct mmVKTFAssetsName
{
    const char* layout;
    const char* pool;
    const char* spiv;
    const char* depth;
    const char* whole;
    const char* pass;
};

extern const struct mmVKTFAssetsName mmVKAssetsNameDefault;

struct mmVKTFAccessor
{
    struct mmByteBuffer bytes;
    struct mmGLTFAccessor* accessor;
    struct mmGLTFBufferView* bufferView;
};

void
mmVKTFAccessor_Init(
    struct mmVKTFAccessor*                         p);

void
mmVKTFAccessor_Destroy(
    struct mmVKTFAccessor*                         p);

const mmUInt8_t*
mmVKTFAccessor_GetData(
    const struct mmVKTFAccessor*                   p);

size_t
mmVKTFAccessor_GetCount(
    const struct mmVKTFAccessor*                   p);

VkResult
mmVKTFAccessor_IsValid(
    const struct mmVKTFAccessor*                   p);

VkResult
mmVKTFAccessor_Prepare(
    struct mmVKTFAccessor*                         p,
    struct mmGLTFRoot*                             pGLTFRoot,
    int                                            index);

void
mmVKTFAccessor_Discard(
    struct mmVKTFAccessor*                         p);

struct mmVKTFImage
{
    struct mmString name;
    struct mmVKImage vImage;
    VkImageView imageView;
};

void
mmVKTFImage_Init(
    struct mmVKTFImage*                            p);

void
mmVKTFImage_Destroy(
    struct mmVKTFImage*                            p);

VkResult
mmVKTFImage_Prepare(
    struct mmVKTFImage*                            p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFImage*                            pGLTFImage,
    ktx_transcode_fmt_e                            transcodefmt);

void
mmVKTFImage_Discard(
    struct mmVKTFImage*                            p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFImages_Init(
    struct mmVectorValue*                          p);

void
mmVKTFImages_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFImages_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    ktx_transcode_fmt_e                            transcodefmt);

void
mmVKTFImages_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader);

struct mmVKTFSampler
{
    struct mmString name;
    VkSampler sampler;
};

void
mmVKTFSampler_Init(
    struct mmVKTFSampler*                          p);

void
mmVKTFSampler_Destroy(
    struct mmVKTFSampler*                          p);

VkResult
mmVKTFSampler_Prepare(
    struct mmVKTFSampler*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFSampler*                          pGLTFSampler);

void
mmVKTFSampler_Discard(
    struct mmVKTFSampler*                          p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFSamplers_Init(
    struct mmVectorValue*                          p);

void
mmVKTFSamplers_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFSamplers_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel);

void
mmVKTFSamplers_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader);

// We not support texture transform.
struct mmVKUBOTextureInfo
{
    mmUniformInt index;
    mmUniformInt texCoord;
};

// std140 aligned 16.
typedef struct mmVKUBOTextureInfo MM_ALIGNED(16) mmUniformUBOTextureInfo;

void
mmVKUBOTextureInfo_Reset(
    struct mmVKUBOTextureInfo*                     p);

void
mmVKUBOTextureInfo_CopyFrom(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFTextureInfo*                q);

void
mmVKUBOTextureInfo_CopyFromNormal(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFNormalTextureInfo*          q);

void
mmVKUBOTextureInfo_CopyFromOcclusion(
    struct mmVKUBOTextureInfo*                     p,
    const struct mmGLTFOcclusionTextureInfo*       q);

struct mmVKTFModel;

enum mmVKPipelinePhase_t
{
    mmVKPipelineDepth = 0,
    mmVKPipelineWhole = 1,

    mmVKPipelineMax,
};

struct mmVKPipelineIndex
{
    int Pipeline;
    int IdxMat;
    int IdxMap;
};

struct mmVKPushConstantsDepth
{
    mmUniformMat4 Normal;
    mmUniformMat4 Model;

    mmUniformInt idxMat;
    mmUniformInt idxMap;
};

struct mmVKPushConstantsWhole
{
    mmUniformMat4 Normal;
    mmUniformMat4 Model;
};

struct mmVKPushConstants
{
    mmUniformMat4 Normal;
    mmUniformMat4 Model;

    mmUniformInt idxMat;
    mmUniformInt idxMap;
};

extern const struct mmVectorValue mmVKPushDepth_DSLP;
extern const struct mmVectorValue mmVKPushWhole_DSLP;

extern const struct mmVectorValue mmVKMaterial_DSLB1;

extern const struct mmVectorValue mmVKJoints_DSLB2;

extern const char* mmVKJointsPoolName;

enum mmVKTFTextureIndex
{
    mmVKTFTextureBaseColor         = 0,
    mmVKTFTextureMetallicRoughness = 1,
    mmVKTFTextureNormal            = 2,
    mmVKTFTextureOcclusion         = 3,
    mmVKTFTextureEmissive          = 4,

    mmVKTFTextureMax,
};

struct mmVKUBOMaterial
{
    mmUniformUBOTextureInfo baseColorTexture;
    mmUniformUBOTextureInfo metallicRoughnessTexture;
    mmUniformUBOTextureInfo normalTexture;
    mmUniformUBOTextureInfo occlusionTexture;
    mmUniformUBOTextureInfo emissiveTexture;
    mmUniformVec4 baseColorFactor;
    mmUniformVec3 emissiveFactor;
    mmUniformFloat metallicFactor;
    mmUniformFloat roughnessFactor;
    mmUniformFloat scale;
    mmUniformFloat strength;
    mmUniformFloat alphaCutoff;
    mmUniformInt alphaMode;
    mmUniformInt doubleSided;
};

void
mmVKUBOMaterial_CopyFrom(
    struct mmVKUBOMaterial*                        p,
    const struct mmGLTFMaterial*                   q);

void
mmVKUBOMaterial_MakeDefault(
    struct mmVKUBOMaterial*                        p);

struct mmVKTFMaterial
{
    struct mmString name;
    struct mmVKUBOMaterial vUBOMaterial;
    struct mmVKBuffer vBufferUBOMaterial;
    int vIndex[mmVKTFTextureMax];
    struct mmVKDescriptorSet vDescriptorSet;
    struct mmVKPoolInfo* pPoolInfo;
};

void
mmVKTFMaterial_Init(
    struct mmVKTFMaterial*                         p);

void
mmVKTFMaterial_Destroy(
    struct mmVKTFMaterial*                         p);

VkResult
mmVKTFMaterial_PrepareUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFMaterial_DiscardUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTFMaterial_UploadeUBOBuffer(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKTFMaterial_DescriptorSetProduce(
    struct mmVKTFMaterial*                         p,
    struct mmVKDescriptorPool*                     pDescriptorPool);

void
mmVKTFMaterial_DescriptorSetRecycle(
    struct mmVKTFMaterial*                         p,
    struct mmVKDescriptorPool*                     pDescriptorPool);

VkResult
mmVKTFMaterial_DescriptorSetUpdate(
    struct mmVKTFMaterial*                         p,
    VkDevice                                       device,
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKAssetsDefault*                      pAssetsDefault);

VkResult
mmVKTFMaterial_Prepare(
    struct mmVKTFMaterial*                         p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFMaterial*                         pGLTFMaterial,
    struct mmVKTFModel*                            pVKTFModel,
    const char*                                    pool);

void
mmVKTFMaterial_Discard(
    struct mmVKTFMaterial*                         p,
    struct mmVKAssets*                             pAssets);

VkResult
mmVKTFMaterial_PrepareDefault(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader,
    struct mmVKPoolLoader*                         pPoolLoader,
    const char*                                    pool,
    const char*                                    name);

void
mmVKTFMaterial_DiscardDefault(
    struct mmVKTFMaterial*                         p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFMaterial_MakeLayoutHashId(
    const struct mmVKTFMaterial*                   p,
    char                                           l[2]);

void
mmVKTFMaterials_Init(
    struct mmVectorValue*                          p);

void
mmVKTFMaterials_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFMaterials_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmVKTFModel*                            pVKTFModel,
    const char*                                    pool);

void
mmVKTFMaterials_Discard(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets);

enum mmVKTFAttributeIndex
{
    mmVKTFAttributePOSITION  =  0,
    mmVKTFAttributeNORMAL    =  1,
    mmVKTFAttributeTANGENT   =  2,
    mmVKTFAttributeTEXCOORD0 =  3,
    mmVKTFAttributeTEXCOORD1 =  4,
    mmVKTFAttributeTEXCOORD2 =  5,
    mmVKTFAttributeTEXCOORD3 =  6,
    mmVKTFAttributeTEXCOORD4 =  7,
    mmVKTFAttributeTEXCOORD5 =  8,
    mmVKTFAttributeTEXCOORD6 =  9,
    mmVKTFAttributeTEXCOORD7 = 10,
    mmVKTFAttributeCOLOR0    = 11,
    mmVKTFAttributeJOINTS0   = 12,
    mmVKTFAttributeJOINTS1   = 13,
    mmVKTFAttributeWEIGHTS0  = 14,
    mmVKTFAttributeWEIGHTS1  = 15,

    mmVKTFAttributeMax,
};

enum mmVKTFAttributeMask
{
    mmVKTFAttributeDepthMask = 0x0000F7F9,
    mmVKTFAttributeWholeMask = 0x0000FFFF,
    mmVKTFAttributeSkinsMask = 0x0000F000,
};

struct mmVKTFAttribute
{
    struct mmVKBuffer      vBuffer;
    enum mmGLTFAttribute_t vAttribute;
    enum mmGLTFComponent_t vComponentType;
    enum mmGLTFAccessor_t  vType;
    VkFormat               vFormat;
    uint32_t               vSize;
    uint32_t               vByteStride;
};

int
mmVKTFAttribute_GetTypeHashId(
    const struct mmVKTFAttribute*                  p);

void
mmVKTFAttribute_UpdateByAccessor(
    struct mmVKTFAttribute*                        p,
    struct mmGLTFAccessor*                         pGLTFAccessor,
    struct mmGLTFAttribute*                        pGLTFAttribute,
    size_t                                         numBytes);

uint32_t
mmVKTFPrimitiveMaskId(
    uint32_t                                       hPipeline);

uint32_t
mmVKTFPrimitivePushConstantsSize(
    uint32_t                                       hPipeline);

struct mmVKTFPrimitivePipelineInfo
{
    char                                           lName[64];
    char                                           vName[64];
    char                                           fName[64];
    char                                           xName[64];
    const char*                                    scene;
    const char*                                    joints;
    const char*                                    pass;
    uint32_t                                       hMaskId;
    uint32_t                                       samples;
    const struct mmVectorValue*                    dslp;
};

struct mmVKTFPrimitive
{
    struct mmVKBuffer vBufferIdx;
    struct mmVKTFAttribute vAttribute[mmVKTFAttributeMax];

    int mode;

    VkIndexType hIdxType;

    uint32_t hIdxCount;
    uint32_t hVtxCount;

    uint32_t hTypeId;

    struct mmVKTFMaterial* material;

    struct mmVKPipelineInfo* pPipelineInfo[mmVKPipelineMax];
};

void
mmVKTFPrimitive_Init(
    struct mmVKTFPrimitive*                        p);

void
mmVKTFPrimitive_Destroy(
    struct mmVKTFPrimitive*                        p);

void
mmVKTFPrimitive_MakeTypeId(
    const struct mmVKTFPrimitive*                  p,
    uint32_t*                                      pTypeId);

void
mmVKTFPrimitive_MakeHashId(
    const struct mmVKTFPrimitive*                  p,
    int                                            a[4],
    int                                            m[5],
    int                                            t[13],
    int                                            u[2]);

void
mmVKTFPrimitive_MakeHashIdDepth(
    const struct mmVKTFPrimitive*                  p,
    char                                           v[8],
    char                                           f[16],
    char                                           x[32]);

void
mmVKTFPrimitive_MakeHashIdWhole(
    const struct mmVKTFPrimitive*                  p,
    char                                           v[8],
    char                                           f[16],
    char                                           x[32]);

void
mmVKTFPrimitive_MakeBuffers(
    const struct mmVKTFPrimitive*                  p,
    uint32_t                                       hMaskId,
    VkBuffer                                       buffers[mmVKTFAttributeMax],
    uint32_t*                                      pBindingCount);

void
mmVKTFPrimitive_MakePVII(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKPipelineVertexInputInfo*            pvii,
    uint32_t                                       hMaskId);

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipeline(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    struct mmVKTFPrimitivePipelineInfo*            pInfo);

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipelineDepth(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName);

struct mmVKPipelineInfo*
mmVKTFPrimitive_MakePipelineWhole(
    const struct mmVKTFPrimitive*                  p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName);

VkResult
mmVKTFPrimitive_UploadAttribute(
    struct mmVKTFPrimitive*                        pVKTFPrimitive,
    struct mmVKUploader*                           uploader,
    struct mmGLTFAccessor*                         pGLTFAccessor,
    struct mmGLTFAttribute*                        pGLTFAttribute,
    size_t                                         numBytes,
    const uint8_t*                                 ptrBytes);

VkResult
mmVKTFPrimitive_PreparePipeline(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName);

void
mmVKTFPrimitive_DiscardPipeline(
    struct mmVKTFPrimitive*                        p);

VkResult
mmVKTFPrimitive_Prepare(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFPrimitive*                        pGLTFPrimitive,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName);

void
mmVKTFPrimitive_Discard(
    struct mmVKTFPrimitive*                        p,
    struct mmVKAssets*                             pAssets);

void
mmVKTFPrimitives_Init(
    struct mmVectorValue*                          p);

void
mmVKTFPrimitives_Destroy(
    struct mmVectorValue*                          p);

struct mmVKTFMesh
{
    struct mmString name;
    struct mmVectorValue primitives;
};

void
mmVKTFMesh_Init(
    struct mmVKTFMesh*                             p);

void
mmVKTFMesh_Destroy(
    struct mmVKTFMesh*                             p);

VkResult
mmVKTFMesh_Prepare(
    struct mmVKTFMesh*                             p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFMesh*                             pGLTFMesh,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName);

void
mmVKTFMesh_Discard(
    struct mmVKTFMesh*                             p,
    struct mmVKAssets*                             pAssets);

void
mmVKTFMeshs_Init(
    struct mmVectorValue*                          p);

void
mmVKTFMeshs_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFMeshs_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets,
    struct mmGLTFModel*                            pModel,
    struct mmVectorValue*                          materials,
    const struct mmVKTFAssetsName*                 pAssetsName);

void
mmVKTFMeshs_Discard(
    struct mmVectorValue*                          p,
    struct mmVKAssets*                             pAssets);

struct mmVKTFSkin
{
    struct mmString name;
    struct mmVectorU32 joints;
    int skeleton;
    struct mmVKTFAccessor inverseBindMatrices;
};

void
mmVKTFSkin_Init(
    struct mmVKTFSkin*                             p);

void
mmVKTFSkin_Destroy(
    struct mmVKTFSkin*                             p);

VkResult
mmVKTFSkin_Prepare(
    struct mmVKTFSkin*                             p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel,
    struct mmGLTFSkin*                             pGLTFSkin);

void
mmVKTFSkin_Discard(
    struct mmVKTFSkin*                             p,
    struct mmVKUploader*                           pUploader);

void
mmVKTFSkins_Init(
    struct mmVectorValue*                          p);

void
mmVKTFSkins_Destroy(
    struct mmVectorValue*                          p);

VkResult
mmVKTFSkins_Prepare(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader,
    struct mmGLTFModel*                            pModel);

void
mmVKTFSkins_Discard(
    struct mmVectorValue*                          p,
    struct mmVKUploader*                           pUploader);

struct mmVKTFModel
{
    struct mmGLTFModel model;

    struct mmVectorValue images;
    struct mmVectorValue samplers;
    struct mmVectorValue materials;
    struct mmVectorValue meshes;
    struct mmVectorValue skins;

    VkResult code;
};

void
mmVKTFModel_Init(
    struct mmVKTFModel*                            p);

void
mmVKTFModel_Destroy(
    struct mmVKTFModel*                            p);

int
mmVKTFModel_Invalid(
    const struct mmVKTFModel*                      p);

VkResult
mmVKTFModel_Prepare(
    struct mmVKTFModel*                            p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName,
    const char*                                    path);

void
mmVKTFModel_Discard(
    struct mmVKTFModel*                            p,
    struct mmVKAssets*                             pAssets);

VkResult
mmVKTFModel_MakeDescriptorImageInfo(
    struct mmVKTFModel*                            pVKTFModel,
    struct mmVKAssetsDefault*                      pAssetsDefault,
    VkDescriptorImageInfo*                         pDescriptorImageInfo,
    int                                            index);

void
mmVKTFPrepareDefaultAssets(
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName);

void
mmVKTFDiscardDefaultAssets(
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName);

#include "core/mmSuffix.h"

#endif//__mmVKTFModel_h__
