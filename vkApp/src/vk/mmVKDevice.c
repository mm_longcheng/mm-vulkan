/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDevice.h"

#include "vk/mmVKExtension.h"
#include "vk/mmVKMacro.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

void
mmVKDevice_Reset(
    struct mmVKDevice*                             p)
{
    mmMemset(p, 0, sizeof(struct mmVKDevice));
}

void
mmVKDevice_SetAllocator(
    struct mmVKDevice*                             p,
    const VkAllocationCallbacks*                   pAllocator)
{
    p->pAllocator = pAllocator;
}

VkResult
mmVKDevice_EvaluationSuitable(
    struct mmVKDevice*                             p,
    struct mmVKInstance*                           pInstance,
    VkPhysicalDevice                               physicalDevice,
    VkSurfaceKHR                                   surface)
{
    VkResult err = VK_ERROR_UNKNOWN;
    VkQueueFamilyProperties* pPropertyArray = NULL;
    VkBool32* pSupportArray = NULL;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        size_t sz = 0;
        
        uint32_t i = 0;
        /* Call with NULL data to get count */
        uint32_t hPropertyCount = 0;
        
        VkQueueFamilyProperties* pProperties = NULL;
        
        uint32_t hGraphicsIndex = UINT32_MAX;
        uint32_t hComputeIndex = UINT32_MAX;
        uint32_t hTransferIndex = UINT32_MAX;
        uint32_t hPresentIndex = UINT32_MAX;

        if (NULL == pInstance)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pVKInstance is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &hPropertyCount, NULL);
        if (0 == hPropertyCount)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " hPropertyCount is 0.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INCOMPATIBLE_DRIVER;
            break;
        }

        sz = sizeof(VkQueueFamilyProperties) * hPropertyCount;
        pPropertyArray = (VkQueueFamilyProperties*)mmMalloc(sz);
        if (NULL == pPropertyArray)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }
        
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &hPropertyCount, pPropertyArray);
        
        // Iterate over each queue to learn whether it supports presenting:
        pSupportArray = (VkBool32*)mmMalloc(sizeof(VkBool32) * hPropertyCount);
        if (NULL == pSupportArray)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }
        
        for (i = 0; i < hPropertyCount; i++)
        {
            err = pInstance->vkGetPhysicalDeviceSurfaceSupportKHR(
                physicalDevice,
                i,
                surface,
                &pSupportArray[i]);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkGetPhysicalDeviceSurfaceSupportKHR failure.",
                    __FUNCTION__, __LINE__);
                break;
            }
        }
        
        if (VK_SUCCESS != err)
        {
            break;
        }
        
        for (i = 0; i < hPropertyCount; i++)
        {
            pProperties = &pPropertyArray[i];
            
            if ((UINT32_MAX == hGraphicsIndex) &&
                (0 < pProperties->queueCount) &&
                (0 != (pProperties->queueFlags & VK_QUEUE_GRAPHICS_BIT)))
            {
                hGraphicsIndex = i;
            }
            
            if ((UINT32_MAX == hComputeIndex) &&
                (0 < pProperties->queueCount) &&
                (0 != (pProperties->queueFlags & VK_QUEUE_COMPUTE_BIT )))
            {
                hComputeIndex = i;
            }
            if ((0 < pProperties->queueCount) &&
                (0 == (pProperties->queueFlags & VK_QUEUE_GRAPHICS_BIT)) &&
                (0 != (pProperties->queueFlags & VK_QUEUE_COMPUTE_BIT)) &&
                (i != hGraphicsIndex))
            {
                hComputeIndex = i;
            }

            if ((UINT32_MAX == hTransferIndex) &&
                (0 < pProperties->queueCount) &&
                (0 != (pProperties->queueFlags & VK_QUEUE_TRANSFER_BIT)))
            {
                hTransferIndex = i;
            }
            if ((0 < pProperties->queueCount) &&
                (0 == (pProperties->queueFlags & VK_QUEUE_GRAPHICS_BIT)) &&
                (0 == (pProperties->queueFlags & VK_QUEUE_COMPUTE_BIT )) &&
                (0 != (pProperties->queueFlags & VK_QUEUE_TRANSFER_BIT)) &&
                (i != hGraphicsIndex))
            {
                hTransferIndex = i;
            }
            
            if ((UINT32_MAX == hPresentIndex) &&
                (0 < pProperties->queueCount) &&
                (VK_TRUE == pSupportArray[i]))
            {
                hPresentIndex = i;
            }
        }
        
        if ((hPresentIndex != hGraphicsIndex) &&
            (VK_FALSE == pSupportArray[hGraphicsIndex]) &&
            (VK_TRUE == pSupportArray[hPresentIndex]))
        {
            hGraphicsIndex = hPresentIndex;
        }
        
        if ((UINT32_MAX == hTransferIndex) &&
            (UINT32_MAX != hGraphicsIndex))
        {
            hTransferIndex = hGraphicsIndex;
        }
        
        p->graphicsIndex = hGraphicsIndex;
        p->computeIndex = hComputeIndex;
        p->transferIndex = hTransferIndex;
        p->presentIndex = hPresentIndex;
        
        err = VK_SUCCESS;
    } while(0);
    
    mmFree(pSupportArray);
    mmFree(pPropertyArray);
    
    return err;
}

VkResult
mmVKDevice_Create(
    struct mmVKDevice*                             p,
    const struct mmVKDeviceCreateInfo*             info,
    const VkAllocationCallbacks*                   pAllocator)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    struct mmVKExtension hDeviceExtensionArray;

    mmVKDevice_SetAllocator(p, pAllocator);
    
    mmVKExtension_Init(&hDeviceExtensionArray);
    mmVKExtension_AllocArraySize(&hDeviceExtensionArray, 2);
    
    do
    {
        struct mmVKInstance* pInstance = NULL;
        struct mmVKPhysicalDevice* pPhysicalDevice = NULL;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkSurfaceKHR surface = VK_NULL_HANDLE;
        VkDevice device = VK_NULL_HANDLE;
        
        int hQueueNumber = 0;
        
        float hQueuePriorities[1] = { 0.0f };
        VkDeviceQueueCreateInfo hQueueInfo[4];
        VkDeviceCreateInfo hDeviceInfo;
        VkPhysicalDeviceFeatures hFeatures = { 0 };
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        pInstance = info->pInstance;
        if (NULL == pInstance)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pVKInstance is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        pPhysicalDevice = info->pPhysicalDevice;
        if (NULL == pPhysicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pVKPhysicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        physicalDevice = pPhysicalDevice->physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        surface = info->surface;
        if (VK_NULL_HANDLE == surface)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " surface is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        err = mmVKDevice_EvaluationSuitable(
            p,
            info->pInstance,
            physicalDevice,
            surface);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDevice_EvaluationSuitable failure.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        mmVKExtension_SetRequireFeature(&hDeviceExtensionArray, 0, 1, VK_KHR_SWAPCHAIN_EXTENSION_NAME);
        mmVKExtension_SetRequireFeature(&hDeviceExtensionArray, 1, 0, "VK_KHR_portability_subset");
        mmVKExtension_EnabledDeviceExtension(&hDeviceExtensionArray, physicalDevice);
        
        if (!mmVKExtension_GetIsSupport(&hDeviceExtensionArray, 0))
        {
            const char* n = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
            mmLogger_LogE(gLogger, "vkEnumerateDeviceExtensionProperties failed to find the %s extension.", n);
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            err = VK_ERROR_EXTENSION_NOT_PRESENT;
            break;
        }
        if (!mmVKExtension_GetIsSuitable(&hDeviceExtensionArray))
        {
            mmLogger_LogE(gLogger, "DeviceExtension extension is not complete suitable.");
            err = VK_ERROR_EXTENSION_NOT_PRESENT;
            break;
        }
        
        if (UINT32_MAX != p->graphicsIndex)
        {
            hQueueInfo[hQueueNumber].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            hQueueInfo[hQueueNumber].pNext = NULL;
            hQueueInfo[hQueueNumber].queueFamilyIndex = p->graphicsIndex;
            hQueueInfo[hQueueNumber].queueCount = 1;
            hQueueInfo[hQueueNumber].pQueuePriorities = hQueuePriorities;
            hQueueInfo[hQueueNumber].flags = 0;
            hQueueNumber++;
        }
        
        if (UINT32_MAX != p->computeIndex &&
            p->graphicsIndex != p->computeIndex)
        {
            hQueueInfo[hQueueNumber].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            hQueueInfo[hQueueNumber].pNext = NULL;
            hQueueInfo[hQueueNumber].queueFamilyIndex = p->computeIndex;
            hQueueInfo[hQueueNumber].queueCount = 1;
            hQueueInfo[hQueueNumber].pQueuePriorities = hQueuePriorities;
            hQueueInfo[hQueueNumber].flags = 0;
            hQueueNumber++;
        }
        
        if (UINT32_MAX != p->transferIndex &&
            p->graphicsIndex != p->transferIndex)
        {
            hQueueInfo[hQueueNumber].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            hQueueInfo[hQueueNumber].pNext = NULL;
            hQueueInfo[hQueueNumber].queueFamilyIndex = p->transferIndex;
            hQueueInfo[hQueueNumber].queueCount = 1;
            hQueueInfo[hQueueNumber].pQueuePriorities = hQueuePriorities;
            hQueueInfo[hQueueNumber].flags = 0;
            hQueueNumber++;
        }

        if (UINT32_MAX != p->presentIndex &&
            p->graphicsIndex != p->presentIndex)
        {
            hQueueInfo[hQueueNumber].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            hQueueInfo[hQueueNumber].pNext = NULL;
            hQueueInfo[hQueueNumber].queueFamilyIndex = p->presentIndex;
            hQueueInfo[hQueueNumber].queueCount = 1;
            hQueueInfo[hQueueNumber].pQueuePriorities = hQueuePriorities;
            hQueueInfo[hQueueNumber].flags = 0;
            hQueueNumber++;
        }

        hFeatures.fillModeNonSolid = (pPhysicalDevice->features.fillModeNonSolid) ? VK_TRUE : VK_FALSE;
        hFeatures.imageCubeArray = (pPhysicalDevice->features.imageCubeArray) ? VK_TRUE : VK_FALSE;

        hDeviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        hDeviceInfo.pNext = NULL;
        hDeviceInfo.flags = 0;
        hDeviceInfo.queueCreateInfoCount = hQueueNumber;
        hDeviceInfo.pQueueCreateInfos = hQueueInfo;
        hDeviceInfo.enabledLayerCount = 0;
        hDeviceInfo.ppEnabledLayerNames = NULL;
        hDeviceInfo.enabledExtensionCount = hDeviceExtensionArray.hEnabledCount;
        hDeviceInfo.ppEnabledExtensionNames = (const char *const *)hDeviceExtensionArray.pEnabledArray;
        hDeviceInfo.pEnabledFeatures = &hFeatures;
        
        err = vkCreateDevice(physicalDevice, &hDeviceInfo, pAllocator, &p->device);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkCreateDevice failure.");
            break;
        }
        
        mmLogger_LogV(gLogger, "vkCreateDevice Success.");
        
        device = p->device;
        
        mmVKGetDeviceProcAddr(pInstance, p, device, CreateSwapchainKHR);
        mmVKGetDeviceProcAddr(pInstance, p, device, DestroySwapchainKHR);
        mmVKGetDeviceProcAddr(pInstance, p, device, GetSwapchainImagesKHR);
        mmVKGetDeviceProcAddr(pInstance, p, device, AcquireNextImageKHR);
        mmVKGetDeviceProcAddr(pInstance, p, device, QueuePresentKHR);
        
        if (UINT32_MAX != p->graphicsIndex)
        {
            vkGetDeviceQueue(device, p->graphicsIndex, 0, &p->graphicsQueue);
        }
        
        if (UINT32_MAX != p->computeIndex)
        {
            vkGetDeviceQueue(device, p->computeIndex, 0, &p->computeQueue);
        }
        
        if (UINT32_MAX != p->transferIndex)
        {
            vkGetDeviceQueue(device, p->transferIndex, 0, &p->transferQueue);
        }
        
        if (UINT32_MAX != p->presentIndex)
        {
            vkGetDeviceQueue(device, p->presentIndex, 0, &p->presentQueue);
        }
        
        err = VK_SUCCESS;
    } while(0);
    
    mmVKExtension_DeallocArray(&hDeviceExtensionArray);
    mmVKExtension_Destroy(&hDeviceExtensionArray);
    
    return err;
}

void
mmVKDevice_Delete(
    struct mmVKDevice*                             p)
{
    if (VK_NULL_HANDLE != p->device)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        vkDeviceWaitIdle(p->device);

        vkDestroyDevice(p->device, p->pAllocator);
        p->device = VK_NULL_HANDLE;
        
        p->graphicsQueue = VK_NULL_HANDLE;
        p->computeQueue = VK_NULL_HANDLE;
        p->transferQueue = VK_NULL_HANDLE;
        p->presentQueue = VK_NULL_HANDLE;
        
        mmLogger_LogV(gLogger, "vkDestroyDevice Success.");
    }
}

void
mmVKDevice_WaitIdle(
    struct mmVKDevice*                             p)
{
    VkDevice device = p->device;
    if (VK_NULL_HANDLE != device)
    {
        vkDeviceWaitIdle(device);
    }
}

size_t
mmVKIndexTypeSize(
    VkIndexType                                    type)
{
    switch (type)
    {
    case VK_INDEX_TYPE_UINT16:
        return sizeof(mmUInt16_t);
    case VK_INDEX_TYPE_UINT32:
        return sizeof(mmUInt32_t);
    case VK_INDEX_TYPE_NONE_KHR:
        return 0;
    case VK_INDEX_TYPE_UINT8_EXT:
        return sizeof(mmUInt8_t);
    default:
        return 0;
    }
}
