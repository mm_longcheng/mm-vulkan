/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKScene.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKDescriptorPool.h"
#include "vk/mmVKAssets.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

static const VkDescriptorSetLayoutBinding dslb_bindings0_Whole[] =
{
    {
        // UBOSceneWhole
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // UBOSceneFragment
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // UBOLights
        .binding = 2,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // shadowMapCascade
        .binding = 3,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // shadowMapOmni
        .binding = 4,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // shadowMapSpot
        .binding = 5,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // UBOShadowCascadeSplits
        .binding = 6,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
    {
        // UBOShadowProjectionViews
        .binding = 7,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = NULL,
    },
};

static const VkDescriptorSetLayoutBinding dslb_bindings0_Depth[] =
{
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = NULL,
    },
};

const struct mmVectorValue mmVKSceneDepth_DSLB0 = mmVectorValue_Make(dslb_bindings0_Depth, VkDescriptorSetLayoutBinding);
const struct mmVectorValue mmVKSceneWhole_DSLB0 = mmVectorValue_Make(dslb_bindings0_Whole, VkDescriptorSetLayoutBinding);

const char* mmVKSceneWholePoolName = "mmVKSceneWhole";
const char* mmVKSceneDepthPoolName = "mmVKSceneDepth";

void
mmVKTFSceneLights_Init(
    struct mmVectorValue*                          p)
{
    mmVectorValue_Init(p);
    mmVectorValue_SetElement(p, sizeof(struct mmVKUBOLight));
}

void
mmVKTFSceneLights_Destroy(
    struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

void
mmVKScene_Init(
    struct mmVKScene*                              p)
{
    mmMemset(&p->uboSceneDepthVert, 0, sizeof(struct mmVKUBOSceneDepthVert));
    mmMemset(&p->uboSceneWholeVert, 0, sizeof(struct mmVKUBOSceneWholeVert));
    mmMemset(&p->uboSceneFragment, 0, sizeof(struct mmVKUBOSceneFragment));
    mmVKTFSceneLights_Init(&p->uboSceneLights);

    mmVKBuffer_Init(&p->bufferSceneDepthVert);
    mmVKBuffer_Init(&p->bufferSceneWholeVert);
    mmVKBuffer_Init(&p->bufferSceneFragment);
    mmVKBuffer_Init(&p->bufferSceneLights);
    mmVKBuffer_Init(&p->bufferSceneShadow);
    
    p->hIndex.Pipeline = mmVKPipelineWhole;
    p->hIndex.IdxMat = 0;
    p->hIndex.IdxMap = 0;
    mmVKShadow_Init(&p->hShadow);
    mmVKCamera_Init(&p->hCamera);

    mmVKDescriptorSet_Init(&p->vDescriptorSet[mmVKPipelineDepth]);
    mmVKDescriptorSet_Init(&p->vDescriptorSet[mmVKPipelineWhole]);

    p->pPoolInfo[mmVKPipelineDepth] = NULL;
    p->pPoolInfo[mmVKPipelineWhole] = NULL;
}

void
mmVKScene_Destroy(
    struct mmVKScene*                              p)
{
    p->pPoolInfo[mmVKPipelineDepth] = NULL;
    p->pPoolInfo[mmVKPipelineWhole] = NULL;

    mmVKDescriptorSet_Destroy(&p->vDescriptorSet[mmVKPipelineDepth]);
    mmVKDescriptorSet_Destroy(&p->vDescriptorSet[mmVKPipelineWhole]);

    mmVKCamera_Destroy(&p->hCamera);
    mmVKShadow_Destroy(&p->hShadow);
    p->hIndex.IdxMap = 0;
    p->hIndex.IdxMat = 0;
    p->hIndex.Pipeline = mmVKPipelineWhole;

    mmVKBuffer_Destroy(&p->bufferSceneShadow);
    mmVKBuffer_Destroy(&p->bufferSceneLights);
    mmVKBuffer_Destroy(&p->bufferSceneFragment);
    mmVKBuffer_Destroy(&p->bufferSceneWholeVert);
    mmVKBuffer_Destroy(&p->bufferSceneDepthVert);

    mmVKTFSceneLights_Destroy(&p->uboSceneLights);
    mmMemset(&p->uboSceneFragment, 0, sizeof(struct mmVKUBOSceneFragment));
    mmMemset(&p->uboSceneWholeVert, 0, sizeof(struct mmVKUBOSceneWholeVert));
    mmMemset(&p->uboSceneDepthVert, 0, sizeof(struct mmVKUBOSceneDepthVert));
}

void
mmVKScene_MakeData(
    struct mmVKScene*                              p,
    uint32_t                                       hWindowSize[2])
{
    float ambient[3] = { 0.0f, 0.0f, 0.0f };
    float aspect = (float)hWindowSize[0] / (float)hWindowSize[1];
    float angle = (float)(45 * MM_PI / 180.0);
    float eye[3] = { 1.8f, 2.0f, 5.0f };
    float origin[3] = { 0, 0, 0 };
    float up[3] = { 0.0f, 1.0f, 0.0f };

    float lightDirection0[3] = { -30.0f, -40.0f, 0.0f };
    float lightDirection1[3] = { -6.0f, -8.0f, 0.0f };
    float lightPosition0[3] = { 6.0f, 8.0f, 0.0f };
    float lightPosition1[3] = { 6.0f, 8.0f, 0.0f };
    float lightColor0[3] = { 1.0f, 1.0f, 1.0f };
    float lightColor1[3] = { 1.0f, 0.0f, 0.0f };
    float lightColor2[3] = { 0.0f, 1.0f, 0.0f };
    float lightColor3[3] = { 0.0f, 0.0f, 1.0f };

    struct mmVKUBOLight* lights;

    mmMat4x4Ref ProjectionView = p->uboSceneWholeVert.ProjectionView;
    mmMat4x4Ref Projection = p->uboSceneWholeVert.Projection;
    mmMat4x4Ref View = p->uboSceneWholeVert.View;

    mmMat4x4PerspectiveVK(Projection, (float)angle, aspect, 0.01f, 1000.0f);
    mmMat4x4LookAtRH(View, eye, origin, up);

    mmMat4x4Mul(ProjectionView, Projection, View);

    mmMat4x4Inverse(p->uboSceneFragment.inverseView, View);
    mmUniformVec3Assign(p->uboSceneFragment.camera, eye);
    mmUniformVec3Assign(p->uboSceneFragment.ambient, ambient);
    p->uboSceneFragment.exposure = 1.0f;
    p->uboSceneFragment.lightCount = 4;

    mmVectorValue_AllocatorMemory(&p->uboSceneLights, 4);
    lights = (struct mmVKUBOLight*)p->uboSceneLights.arrays;

    mmUniformVec3Assign(lights[0].direction, lightDirection0);
    mmUniformVec3Assign(lights[0].position, mmVec3Zero);
    mmUniformVec3Assign(lights[0].color, lightColor0);
    lights[0].range = 0.0f;
    lights[0].intensity = 0.4f;
    lights[0].innerConeCos = 0.0f;
    lights[0].outerConeCos = 0.0f;
    lights[0].type = mmGLTFLightTypeDirectional;
    lights[0].shadow = 0;
    lights[0].shadow = -1;

    mmUniformVec3Assign(lights[1].direction, mmVec3Zero);
    mmUniformVec3Assign(lights[1].position, lightPosition0);
    mmUniformVec3Assign(lights[1].color, lightColor1);
    lights[1].range = 0.0f;
    lights[1].intensity = 2.5f;
    lights[1].innerConeCos = 0.0f;
    lights[1].outerConeCos = 0.0f;
    lights[1].type = mmGLTFLightTypePoint;
    lights[1].shadow = 0;
    lights[1].shadow = -1;

    mmUniformVec3Assign(lights[2].direction, mmVec3Zero);
    mmUniformVec3Assign(lights[2].position, lightPosition1);
    mmUniformVec3Assign(lights[2].color, lightColor2);
    lights[2].range = 0.0f;
    lights[2].intensity = 2.5f;
    lights[2].innerConeCos = 0.0f;
    lights[2].outerConeCos = 0.0f;
    lights[2].type = mmGLTFLightTypePoint;
    lights[2].shadow = -1;

    mmUniformVec3Assign(lights[3].direction, lightDirection1);
    mmUniformVec3Assign(lights[3].position, lightPosition1);
    mmUniformVec3Assign(lights[3].color, lightColor3);
    lights[3].range = 0.0f;
    lights[3].intensity = 4.5f;
    lights[3].innerConeCos = 0.0f;
    lights[3].outerConeCos = (float)MM_PI_DIV_4;
    lights[3].type = mmGLTFLightTypeSpot;
    lights[3].shadow = 0;
    lights[3].shadow = -1;
}

void
mmVKScene_SetPipelineIndex(
    struct mmVKScene*                              p,
    int                                            hPipeline,
    int                                            hIdxMat,
    int                                            hIdxMap)
{
    p->hIndex.Pipeline = hPipeline;
    p->hIndex.IdxMat = hIdxMat;
    p->hIndex.IdxMap = hIdxMap;
}

VkResult
mmVKScene_PrepareUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkBufferCreateInfo hBufferInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = sizeof(struct mmVKUBOSceneDepthVert);
        hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferSceneDepthVert);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = sizeof(struct mmVKUBOSceneWholeVert);
        hBufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferSceneWholeVert);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = sizeof(struct mmVKUBOSceneFragment);
        hBufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferSceneFragment);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        hBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        hBufferInfo.pNext = NULL;
        hBufferInfo.flags = 0;
        hBufferInfo.size = p->uboSceneLights.size * p->uboSceneLights.element;
        hBufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        hBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hBufferInfo.queueFamilyIndexCount = 0;
        hBufferInfo.pQueueFamilyIndices = NULL;

        err = mmVKUploader_CreateCPU2GPUBuffer(
            pUploader,
            &hBufferInfo,
            0,
            &p->bufferSceneLights);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_CreateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKScene_DiscardUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferSceneShadow);
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferSceneLights);
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferSceneFragment);
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferSceneWholeVert);
    mmVKUploader_DeleteBuffer(pUploader, &p->bufferSceneDepthVert);
}

VkResult
mmVKScene_UpdateUBOBufferSceneDepth(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneDepthVert,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneDepthVert),
            &p->bufferSceneDepthVert,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKScene_UpdateUBOBufferSceneWhole(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneWholeVert,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneWholeVert),
            &p->bufferSceneWholeVert,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKScene_UpdateUBOBufferSceneFragment(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneFragment,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneFragment),
            &p->bufferSceneFragment,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKScene_UpdateUBOBufferSceneLights(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)p->uboSceneLights.arrays,
            (size_t)0,
            (size_t)(p->uboSceneLights.size * p->uboSceneLights.element),
            &p->bufferSceneLights,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKScene_UpdateUBOBufferShadow(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    return mmVKShadow_UploadUBOBuffer(&p->hShadow, pUploader);
}

VkResult
mmVKScene_UpdateUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneDepthVert,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneDepthVert),
            &p->bufferSceneDepthVert,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneWholeVert,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneWholeVert),
            &p->bufferSceneWholeVert,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)&p->uboSceneFragment,
            (size_t)0,
            (size_t)sizeof(struct mmVKUBOSceneFragment),
            &p->bufferSceneFragment,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKUploader_UpdateCPU2GPUBuffer(
            pUploader,
            (uint8_t*)p->uboSceneLights.arrays,
            (size_t)0,
            (size_t)(p->uboSceneLights.size * p->uboSceneLights.element),
            &p->bufferSceneLights,
            (size_t)0);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_UpdateCPU2GPUBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKShadow_UploadUBOBuffer(&p->hShadow, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadow_UploadUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKScene_DescriptorSetProduce(
    struct mmVKScene*                              p)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKPoolInfo_Invalid(p->pPoolInfo[mmVKPipelineDepth]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo[mmVKPipelineDepth] is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (mmVKPoolInfo_Invalid(p->pPoolInfo[mmVKPipelineDepth]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo[mmVKPipelineDepth] is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(
            &p->pPoolInfo[mmVKPipelineDepth]->vDescriptorPool, 
            &p->vDescriptorSet[mmVKPipelineDepth]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKDescriptorPool_DescriptorSetProduce(
            &p->pPoolInfo[mmVKPipelineWhole]->vDescriptorPool, 
            &p->vDescriptorSet[mmVKPipelineWhole]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKDescriptorPool_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while (0);

    return err;
}

void
mmVKScene_DescriptorSetRecycle(
    struct mmVKScene*                              p)
{
    mmVKDescriptorPool_DescriptorSetRecycle(
        &p->pPoolInfo[mmVKPipelineWhole]->vDescriptorPool, 
        &p->vDescriptorSet[mmVKPipelineWhole]);

    mmVKDescriptorPool_DescriptorSetRecycle(
        &p->pPoolInfo[mmVKPipelineDepth]->vDescriptorPool, 
        &p->vDescriptorSet[mmVKPipelineDepth]);
}

VkResult
mmVKScene_DescriptorSetUpdateCommon(
    struct mmVKScene*                              p,
    VkDevice                                       device)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDescriptorBufferInfo hDescriptorBufferInfo[3];

        VkWriteDescriptorSet writes[3];
        
        VkDeviceSize range;

        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet[mmVKPipelineDepth]))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet[mmVKPipelineWhole]))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*
         * mmVKPipelineDepth
         */
        descriptorSet = p->vDescriptorSet[mmVKPipelineDepth].descriptorSet;

        hDescriptorBufferInfo[0].buffer = p->bufferSceneDepthVert.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmVKUBOSceneDepthVert);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 1, writes, 0, NULL);

        /*
         * mmVKPipelineWhole
         */
        descriptorSet = p->vDescriptorSet[mmVKPipelineWhole].descriptorSet;

        hDescriptorBufferInfo[0].buffer = p->bufferSceneWholeVert.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = sizeof(struct mmVKUBOSceneWholeVert);

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        hDescriptorBufferInfo[1].buffer = p->bufferSceneFragment.buffer;
        hDescriptorBufferInfo[1].offset = 0;
        hDescriptorBufferInfo[1].range = sizeof(struct mmVKUBOSceneFragment);
        
        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 1;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writes[1].pImageInfo = NULL;
        writes[1].pBufferInfo = &hDescriptorBufferInfo[1];
        writes[1].pTexelBufferView = NULL;

        range = p->uboSceneLights.size * p->uboSceneLights.element;
        hDescriptorBufferInfo[2].buffer = p->bufferSceneLights.buffer;
        hDescriptorBufferInfo[2].offset = 0;
        hDescriptorBufferInfo[2].range = range;
        
        writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[2].pNext = NULL;
        writes[2].dstSet = descriptorSet;
        writes[2].dstBinding = 2;
        writes[2].dstArrayElement = 0;
        writes[2].descriptorCount = 1;
        writes[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[2].pImageInfo = NULL;
        writes[2].pBufferInfo = &hDescriptorBufferInfo[2];
        writes[2].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 3, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmVKScene_DescriptorSetUpdateShadow(
    struct mmVKScene*                              p,
    VkDevice                                       device)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkWriteDescriptorSet writes[6];

        VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

        VkDescriptorImageInfo hDescriptorImageInfo[4];
        VkDescriptorBufferInfo hDescriptorBufferInfo[2];
        
        VkDeviceSize range;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet[mmVKPipelineDepth]))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (mmVKDescriptorSet_Invalid(&p->vDescriptorSet[mmVKPipelineWhole]))
        {
            // descriptorSet is not ready, not need update descriptor set.
            err = VK_SUCCESS;
            break;
        }

        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        /*
         * mmVKPipelineWhole
         */
        descriptorSet = p->vDescriptorSet[mmVKPipelineWhole].descriptorSet;

        // shadowMap
        hDescriptorImageInfo[0].sampler = p->hShadow.hCascade.pSamplerInfo->sampler;
        hDescriptorImageInfo[0].imageView = p->hShadow.hCascade.hShadowMap.imageView;
        hDescriptorImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 3;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[0].pImageInfo = &hDescriptorImageInfo[0];
        writes[0].pBufferInfo = NULL;
        writes[0].pTexelBufferView = NULL;

        hDescriptorImageInfo[1].sampler = p->hShadow.hOmni.pSamplerInfo->sampler;
        hDescriptorImageInfo[1].imageView = p->hShadow.hOmni.hShadowMap.imageView;
        hDescriptorImageInfo[1].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[1].pNext = NULL;
        writes[1].dstSet = descriptorSet;
        writes[1].dstBinding = 4;
        writes[1].dstArrayElement = 0;
        writes[1].descriptorCount = 1;
        writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[1].pImageInfo = &hDescriptorImageInfo[1];
        writes[1].pBufferInfo = NULL;
        writes[1].pTexelBufferView = NULL;

        hDescriptorImageInfo[2].sampler = p->hShadow.hSpot.pSamplerInfo->sampler;
        hDescriptorImageInfo[2].imageView = p->hShadow.hSpot.hShadowMap.imageView;
        hDescriptorImageInfo[2].imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[2].pNext = NULL;
        writes[2].dstSet = descriptorSet;
        writes[2].dstBinding = 5;
        writes[2].dstArrayElement = 0;
        writes[2].descriptorCount = 1;
        writes[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writes[2].pImageInfo = &hDescriptorImageInfo[2];
        writes[2].pBufferInfo = NULL;
        writes[2].pTexelBufferView = NULL;

        // Cascade
        range = p->hShadow.uboSplits.size * p->hShadow.uboSplits.element;
        hDescriptorBufferInfo[0].buffer = p->hShadow.bufferUBOSplits.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = range;

        writes[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[3].pNext = NULL;
        writes[3].dstSet = descriptorSet;
        writes[3].dstBinding = 6;
        writes[3].dstArrayElement = 0;
        writes[3].descriptorCount = 1;
        writes[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[3].pImageInfo = NULL;
        writes[3].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[3].pTexelBufferView = NULL;

        range = p->hShadow.uboProjectionViews.size * p->hShadow.uboProjectionViews.element;
        hDescriptorBufferInfo[1].buffer = p->hShadow.bufferUBOProjectionViews.buffer;
        hDescriptorBufferInfo[1].offset = 0;
        hDescriptorBufferInfo[1].range = range;

        writes[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[4].pNext = NULL;
        writes[4].dstSet = descriptorSet;
        writes[4].dstBinding = 7;
        writes[4].dstArrayElement = 0;
        writes[4].descriptorCount = 1;
        writes[4].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[4].pImageInfo = NULL;
        writes[4].pBufferInfo = &hDescriptorBufferInfo[1];
        writes[4].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 5, writes, 0, NULL);

        /*
         * mmVKPipelineDepth
         */
        descriptorSet = p->vDescriptorSet[mmVKPipelineDepth].descriptorSet;

        range = p->hShadow.uboProjectionViews.size * p->hShadow.uboProjectionViews.element;
        hDescriptorBufferInfo[0].buffer = p->hShadow.bufferUBOProjectionViews.buffer;
        hDescriptorBufferInfo[0].offset = 0;
        hDescriptorBufferInfo[0].range = range;

        writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writes[0].pNext = NULL;
        writes[0].dstSet = descriptorSet;
        writes[0].dstBinding = 0;
        writes[0].dstArrayElement = 0;
        writes[0].descriptorCount = 1;
        writes[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writes[0].pImageInfo = NULL;
        writes[0].pBufferInfo = &hDescriptorBufferInfo[0];
        writes[0].pTexelBufferView = NULL;

        vkUpdateDescriptorSets(device, 1, writes, 0, NULL);

        err = VK_SUCCESS;
    } while (0);

    return err;
}

VkResult
mmVKScene_DescriptorSetUpdate(
    struct mmVKScene*                              p,
    VkDevice                                       device)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmVKScene_DescriptorSetUpdateCommon(p, device);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_DescriptorSetUpdateCommon failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKScene_DescriptorSetUpdateShadow(p, device);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_DescriptorSetUpdateShadow failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

VkResult
mmVKScene_Prepare(
    struct mmVKScene*                              p,
    struct mmVKAssets*                             pAssets,
    const struct mmVectorValue*                    pLightInfos,
    VkFormat                                       hDepthFormat)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        struct mmVKUploader* pUploader = NULL;
        struct mmVKPoolLoader* pPoolLoader = NULL;
        const char* pool = "";
        struct mmVectorVpt lights[3];

        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pool = mmVKSceneDepthPoolName;
        pPoolLoader = &pAssets->vPoolLoader;
        p->pPoolInfo[mmVKPipelineDepth] = mmVKPoolLoader_GetFromName(pPoolLoader, pool);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo[mmVKPipelineDepth]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo[mmVKPipelineDepth] is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo[mmVKPipelineDepth] = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo[mmVKPipelineDepth]);

        pool = mmVKSceneWholePoolName;
        pPoolLoader = &pAssets->vPoolLoader;
        p->pPoolInfo[mmVKPipelineWhole] = mmVKPoolLoader_GetFromName(pPoolLoader, pool);
        if (mmVKPoolInfo_Invalid(p->pPoolInfo[mmVKPipelineWhole]))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pPoolInfo[mmVKPipelineWhole] is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            p->pPoolInfo[mmVKPipelineWhole] = NULL;
            break;
        }
        mmVKPoolInfo_Increase(p->pPoolInfo[mmVKPipelineWhole]);
        
        mmVectorVpt_Init(&lights[0]);
        mmVectorVpt_Init(&lights[1]);
        mmVectorVpt_Init(&lights[2]);
        mmVKScene_UpdateLights(p, pLightInfos, lights);
        err = mmVKShadow_Prepare(&p->hShadow, pAssets, lights, hDepthFormat);
        mmVectorVpt_Destroy(&lights[2]);
        mmVectorVpt_Destroy(&lights[1]);
        mmVectorVpt_Destroy(&lights[0]);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKShadow_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKScene_PrepareUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_PrepareUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKScene_DescriptorSetProduce(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_DescriptorSetProduce failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKScene_UpdateUBOBuffer(p, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_UpdateUBOBuffer failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = mmVKScene_DescriptorSetUpdate(p, pUploader->device);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKScene_DescriptorSetUpdate failure.", __FUNCTION__, __LINE__);
            break;
        }
        
    } while(0);
    
    return err;
}

void
mmVKScene_Discard(
    struct mmVKScene*                              p,
    struct mmVKAssets*                             pAssets)
{
    do
    {
        struct mmVKUploader* pUploader = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pAssets)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pAssets is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pUploader = pAssets->pUploader;
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        mmVKScene_DescriptorSetRecycle(p);

        mmVKScene_DiscardUBOBuffer(p, pUploader);

        mmVKShadow_Discard(&p->hShadow, pAssets);

        mmVKPoolInfo_Decrease(p->pPoolInfo[mmVKPipelineWhole]);
        p->pPoolInfo[mmVKPipelineWhole] = NULL;

        mmVKPoolInfo_Decrease(p->pPoolInfo[mmVKPipelineDepth]);
        p->pPoolInfo[mmVKPipelineDepth] = NULL;
    } while (0);
}

void
mmVKScene_UpdateUBODataLights(
    struct mmVKScene*                              p)
{
    mmVKShadow_UpdateUBODataLights(
        &p->hShadow,
        &p->uboSceneLights,
        &p->hShadow.uboProjectionViews);
}

void
mmVKScene_UpdateUBODataTransforms(
    struct mmVKScene*                              p)
{
    mmVKShadow_UpdateUBODataTransforms(&p->hShadow);
}

void
mmVKScene_UpdateTransforms(
    struct mmVKScene*                              p)
{
    mmVKShadow_UpdateTransforms(
        &p->hShadow,
        &p->uboSceneLights,
        p->uboSceneWholeVert.ProjectionView,
        p->hCamera.n,
        p->hCamera.f);
}

void
mmVKScene_UpdateLights(
    struct mmVKScene*                              p,
    const struct mmVectorValue*                    pLightInfos,
    struct mmVectorVpt                             lights[3])
{
    int idxMat = 0;
    int hFrameNumber;
    size_t i;
    size_t hCount;
    size_t size = pLightInfos->size;
    struct mmVKUBOLight* pLight = NULL;
    struct mmVKLightInfo* pInfo = NULL;
    
    int b[3] = { 0 };
    int n[3] = { 0 };
    int shadow = 0;
    int index;

    hCount = size;
    hCount = (0 != hCount) ? hCount : 1;
    mmVectorValue_AllocatorMemory(&p->uboSceneLights, hCount);
    
    for (i = 0; i < size; ++i)
    {
        pLight = (struct mmVKUBOLight*)mmVectorValue_At(&p->uboSceneLights, i);
        pInfo = (struct mmVKLightInfo*)mmVectorValue_At(pLightInfos, i);
        
        index = pInfo->type - 1;
        shadow = b[index];
        hFrameNumber = mmVKLightInfo_GetFrameNumber(pInfo);
        
        mmUniformVec4Assign(pLight->direction, pInfo->direction);
        mmUniformVec4Assign(pLight->position, pInfo->position);
        mmUniformVec4Assign(pLight->color, pInfo->color);
        pLight->range = pInfo->range;
        pLight->intensity = pInfo->intensity;
        pLight->innerConeCos = pInfo->innerConeCos;
        pLight->outerConeCos = pInfo->outerConeCos;
        pLight->type = pInfo->type;
        
        pLight->count = hFrameNumber;
        pLight->mode = pInfo->mode;
        pLight->ambient = pInfo->ambient;
        pLight->biasMax = pInfo->biasMax;
        pLight->biasMin = pInfo->biasMin;
        
        pLight->debug = pInfo->debug;
        pLight->idxMat = idxMat;
        
        if (pInfo->castShadow)
        {
            pLight->shadow = shadow;
            pLight->idxMap = n[index];
            
            mmVectorVpt_AlignedMemory(&lights[index], shadow + 1);
            mmVectorVpt_SetIndex(&lights[index], shadow, pInfo);
            
            idxMat += hFrameNumber;
            b[index]++;
            n[index] += hFrameNumber;
        }
        else
        {
            pLight->shadow = -1;
            pLight->idxMap = 0;
        }
    }
}

