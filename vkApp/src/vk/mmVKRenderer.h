/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKRenderer_h__
#define __mmVKRenderer_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKInstance.h"
#include "vk/mmVKPhysicalDevice.h"

#include "vk/mmVKSurface.h"
#include "vk/mmVKDevice.h"
#include "vk/mmVKSwapchain.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKRendererCreateInfo
{
    const char* appName;
    VkBool32 validation;
    uint32_t vApiVersion;
    uint32_t vAppVersion;
    uint32_t index;
    void* viewSurface;
};

struct mmVKRenderer
{
    struct mmVKInstance vInstance;
    struct mmVKPhysicalDevice vPhysicalDevice;
    struct mmVKDevice vDevice;
    struct mmVKUploader vUploader;
    VkCommandPool graphicsCmdPool;
    VkCommandPool transferCmdPool;
    VmaAllocator vmaAllocator;
    uint32_t vApiVersion;
    uint32_t vAppVersion;
    const VkAllocationCallbacks* pAllocator;
};

void
mmVKRenderer_Init(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_Destroy(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_Reset(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_SetAllocator(
    struct mmVKRenderer*                           p,
    const VkAllocationCallbacks*                   pAllocator);

VkResult
mmVKRenderer_PrepareInstance(
    struct mmVKRenderer*                           p,
    struct mmVKInstanceCreateInfo*                 info);

VkResult
mmVKRenderer_PreparePhysicalDevice(
    struct mmVKRenderer*                           p,
    uint32_t                                       index);

VkResult
mmVKRenderer_PrepareDevice(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface);

VkResult
mmVKRenderer_PrepareVmaAllocator(
    struct mmVKRenderer*                           p);

VkResult
mmVKRenderer_PrepareCommandPool(
    struct mmVKRenderer*                           p);

VkResult
mmVKRenderer_PrepareUploader(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardInstance(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardPhysicalDevice(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardDevice(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardVmaAllocator(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardCommandPool(
    struct mmVKRenderer*                           p);

void
mmVKRenderer_DiscardUploader(
    struct mmVKRenderer*                           p);

VkResult
mmVKRenderer_Create(
    struct mmVKRenderer*                           p,
    const struct mmVKRendererCreateInfo*           info,
    struct mmVKSurface*                            pSurface,
    const VkAllocationCallbacks*                   pAllocator);

void
mmVKRenderer_Delete(
    struct mmVKRenderer*                           p,
    struct mmVKSurface*                            pSurface);

VkResult
mmVKRenderer_PrepareSurface(
    struct mmVKRenderer*                           p,
    void*                                          viewSurface,
    struct mmVKSurface*                            pSurface);

void
mmVKRenderer_DiscardSurface(
    struct mmVKRenderer*                           p,
    struct mmVKSurface*                            pSurface);

VkResult
mmVKRenderer_PrepareSwapchain(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface,
    uint32_t                                       windowSize[2],
    VkSampleCountFlagBits                          samples,
    struct mmVKSwapchain*                          pSwapchain);

void
mmVKRenderer_DiscardSwapchain(
    struct mmVKRenderer*                           p,
    struct mmVKSwapchain*                          pSwapchain);

VkResult
mmVKRenderer_RebuildSwapchain(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface,
    uint32_t                                       windowSize[2],
    struct mmVKSwapchain*                          pSwapchain);

void
mmVKRenderer_DeviceWaitIdle(
    struct mmVKRenderer*                           p);

#include "core/mmSuffix.h"

#endif//__mmVKRenderer_h__
