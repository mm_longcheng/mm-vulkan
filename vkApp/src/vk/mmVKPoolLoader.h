/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPoolLoader_h__
#define __mmVKPoolLoader_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDescriptorPool.h"

#include "core/mmPrefix.h"

struct mmVectorValue;
struct mmFileContext;
struct mmVKLayoutLoader;

struct mmVKPoolInfo
{
    // name for pool info, read only. 
    // string is weak reference value.
    struct mmString name;

    // RenderPass archive.
    struct mmVKDescriptorPool vDescriptorPool;

    // reference count. 
    size_t reference;
};

void
mmVKPoolInfo_Init(
    struct mmVKPoolInfo*                           p);

void
mmVKPoolInfo_Destroy(
    struct mmVKPoolInfo*                           p);

int
mmVKPoolInfo_Invalid(
    const struct mmVKPoolInfo*                     p);

void
mmVKPoolInfo_Increase(
    struct mmVKPoolInfo*                           p);

void
mmVKPoolInfo_Decrease(
    struct mmVKPoolInfo*                           p);

VkResult
mmVKPoolInfo_Prepare(
    struct mmVKPoolInfo*                           p,
    struct mmVKDevice*                             pDevice,
    struct mmVKLayoutInfo*                         pInfo);

void
mmVKPoolInfo_Discard(
    struct mmVKPoolInfo*                           p,
    struct mmVKDevice*                             pDevice);

struct mmVKPoolLoader
{
    struct mmFileContext* pFileContext;
    struct mmVKDevice* pDevice;
    struct mmVKLayoutLoader* pLayoutLoader;
    struct mmRbtreeStringVpt sets;
};

void
mmVKPoolLoader_Init(
    struct mmVKPoolLoader*                         p);

void
mmVKPoolLoader_Destroy(
    struct mmVKPoolLoader*                         p);

void
mmVKPoolLoader_SetFileContext(
    struct mmVKPoolLoader*                         p,
    struct mmFileContext*                          pFileContext);

void
mmVKPoolLoader_SetDevice(
    struct mmVKPoolLoader*                         p,
    struct mmVKDevice*                             pDevice);

void
mmVKPoolLoader_SetLayoutLoader(
    struct mmVKPoolLoader*                         p,
    struct mmVKLayoutLoader*                       pLayoutLoader);

VkResult
mmVKPoolLoader_PreparePoolInfo(
    struct mmVKPoolLoader*                         p,
    struct mmVKLayoutInfo*                         pInfo,
    struct mmVKPoolInfo*                           pPoolInfo);

void
mmVKPoolLoader_DiscardPoolInfo(
    struct mmVKPoolLoader*                         p,
    struct mmVKPoolInfo*                           pPoolInfo);

void
mmVKPoolLoader_UnloadByName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name);

void
mmVKPoolLoader_UnloadComplete(
    struct mmVKPoolLoader*                         p);

struct mmVKPoolInfo*
mmVKPoolLoader_LoadFromInfo(
    struct mmVKPoolLoader*                         p,
    const char*                                    name,
    struct mmVKLayoutInfo*                         pInfo);

struct mmVKPoolInfo*
mmVKPoolLoader_LoadFromLayoutName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name,
    const char*                                    pLayoutName);

struct mmVKPoolInfo*
mmVKPoolLoader_GetFromName(
    struct mmVKPoolLoader*                         p,
    const char*                                    name);

#include "core/mmSuffix.h"

#endif//__mmVKPoolLoader_h__
