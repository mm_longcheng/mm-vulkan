/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKDrawable_h__
#define __mmVKDrawable_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKBuffer.h"
#include "vk/mmVKDescriptorSet.h"

#include "core/mmPrefix.h"

struct mmVKPipelineInfo;
struct mmVKPoolInfo;

struct mmVKAssets;
struct mmVKUploader;

struct mmVKDrawableCreateInfo
{
    VkIndexType hIdxType;
    
    uint32_t hIdxCount;
    uint32_t hVtxCount;
    
    size_t hIdxBufferSize;
    size_t hVtxBufferSize;
    size_t hCstBufferSize;
    
    const uint8_t* pIdxBuffer;
    const uint8_t* pVtxBuffer;
    const uint8_t* pCstBuffer;
    
    const char* pPipelineName;
    const char* pPoolName;
};

struct mmVKDrawable
{
    struct mmString hPipelineName;
    struct mmString hPoolName;
    
    struct mmVKBuffer vIdxBuffer;
    struct mmVKBuffer vVtxBuffer;
    
    struct mmByteBuffer vConstantBytes;
    
    struct mmVKDescriptorSet vDescriptorSet;

    struct mmVKPipelineInfo* pPipelineInfo;
    struct mmVKPoolInfo* pPoolInfo;
    
    uint32_t hIdxCount;
    uint32_t hVtxCount;

    VkIndexType hIdxType;
};

void
mmVKDrawable_Init(
    struct mmVKDrawable*                           p);

void
mmVKDrawable_Destroy(
    struct mmVKDrawable*                           p);

VkResult
mmVKDrawable_PrepareBuffer(
    struct mmVKDrawable*                           p,
    struct mmVKUploader*                           pUploader,
    struct mmVKDrawableCreateInfo*                 info);

void
mmVKDrawable_DiscardBuffer(
    struct mmVKDrawable*                           p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKDrawable_PreparePipeline(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets);

void
mmVKDrawable_DiscardPipeline(
    struct mmVKDrawable*                           p);

VkResult
mmVKDrawable_PreparePool(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets);

void
mmVKDrawable_DiscardPool(
    struct mmVKDrawable*                           p);

VkResult
mmVKDrawable_DescriptorSetProduce(
    struct mmVKDrawable*                           p);

void
mmVKDrawable_DescriptorSetRecycle(
    struct mmVKDrawable*                           p);

VkResult
mmVKDrawable_Prepare(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets,
    struct mmVKDrawableCreateInfo*                 info);

void
mmVKDrawable_Discard(
    struct mmVKDrawable*                           p,
    struct mmVKAssets*                             pAssets);

void
mmVKDrawable_OnRecord(
    struct mmVKDrawable*                           p,
    VkCommandBuffer                                cmdBuffer);

#include "core/mmSuffix.h"

#endif//__mmVKDrawable_h__
