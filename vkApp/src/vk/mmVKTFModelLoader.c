/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKTFModelLoader.h"

#include "vk/mmVKPipelineLoader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKTFModelInfo_Init(
    struct mmVKTFModelInfo*                        p)
{
    mmString_Init(&p->name);
    mmVKTFModel_Init(&p->vModel);
    p->reference = 0;
}

void
mmVKTFModelInfo_Destroy(
    struct mmVKTFModelInfo*                        p)
{
    p->reference = 0;
    mmVKTFModel_Destroy(&p->vModel);
    mmString_Destroy(&p->name);
}

int
mmVKTFModelInfo_Invalid(
    const struct mmVKTFModelInfo*                  p)
{
    return
        (NULL == p) ||
        (VK_SUCCESS != p->vModel.code);
}

void
mmVKTFModelInfo_Increase(
    struct mmVKTFModelInfo*                        p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKTFModelInfo_Decrease(
    struct mmVKTFModelInfo*                        p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKTFModelInfo_Prepare(
    struct mmVKTFModelInfo*                        p,
    struct mmVKAssets*                             pAssets,
    const struct mmVKTFAssetsName*                 pAssetsName,
    const char*                                    path)
{
    return mmVKTFModel_Prepare(&p->vModel, pAssets, pAssetsName, path);
}

void
mmVKTFModelInfo_Discard(
    struct mmVKTFModelInfo*                        p,
    struct mmVKAssets*                             pAssets)
{
    mmVKTFModel_Discard(&p->vModel, pAssets);
}

void
mmVKTFModelLoader_Init(
    struct mmVKTFModelLoader*                      p)
{
    p->pAssets = NULL;
    p->pAssetsName = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    mmRbtsetVpt_Init(&p->vPipelineInfos);
}

void
mmVKTFModelLoader_Destroy(
    struct mmVKTFModelLoader*                      p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmRbtsetVpt_Destroy(&p->vPipelineInfos);
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pAssetsName = NULL;
    p->pAssets = NULL;
}

void
mmVKTFModelLoader_SetAssets(
    struct mmVKTFModelLoader*                      p,
    struct mmVKAssets*                             pAssets)
{
    p->pAssets = pAssets;
}

void
mmVKTFModelLoader_SetAssetsName(
    struct mmVKTFModelLoader*                      p,
    const struct mmVKTFAssetsName*                 pAssetsName)
{
    p->pAssetsName = pAssetsName;
}

VkResult
mmVKTFModelLoader_PrepareModelInfo(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path,
    struct mmVKTFModelInfo*                        pModelInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pModelInfo->reference && "reference not zero.");

        if (NULL == path)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " path is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKTFModelInfo_Prepare(pModelInfo, p->pAssets, p->pAssetsName, path);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTFModelInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        pModelInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKTFModelLoader_DiscardModelInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKTFModelInfo*                        pModelInfo)
{
    assert(0 == pModelInfo->reference && "reference not zero.");
    mmVKTFModelInfo_Discard(pModelInfo, p->pAssets);
}

void
mmVKTFModelLoader_UnloadByPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path)
{
    struct mmVKTFModelInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKTFModelInfo*)it->v;
        mmVKTFModelLoader_DiscardModelInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKTFModelInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKTFModelLoader_UnloadComplete(
    struct mmVKTFModelLoader*                      p)
{
    struct mmVKTFModelInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKTFModelInfo*)(it->v);
        mmVKTFModelLoader_DiscardModelInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKTFModelInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKTFModelInfo*
mmVKTFModelLoader_LoadFromPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path)
{
    struct mmVKTFModelInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKTFModelInfo*)mmMalloc(sizeof(struct mmVKTFModelInfo));
        mmVKTFModelInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKTFModelLoader_PrepareModelInfo(p, path, s);
    }
    else
    {
        s = (struct mmVKTFModelInfo*)(it->v);
    }
    return s;
}

struct mmVKTFModelInfo*
mmVKTFModelLoader_GetFromPath(
    struct mmVKTFModelLoader*                      p,
    const char*                                    path)
{
    struct mmVKTFModelInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, path);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKTFModelInfo*)(it->v);
    }
    return s;
}

void
mmVKTFModelLoader_AddPipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineInfo*                       pPipelineInfo)
{
    mmRbtsetVpt_Add(&p->vPipelineInfos, (void*)(pPipelineInfo));
    mmVKPipelineInfo_Increase(pPipelineInfo);
}

void
mmVKTFModelLoader_RmvPipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineInfo*                       pPipelineInfo)
{
    mmVKPipelineInfo_Decrease(pPipelineInfo);
    mmRbtsetVpt_Rmv(&p->vPipelineInfos, (void*)(pPipelineInfo));
}

void
mmVKTFModelLoader_UnloadCompletePipelineInfo(
    struct mmVKTFModelLoader*                      p,
    struct mmVKPipelineLoader*                     pPipelineLoader)
{
    struct mmVKPipelineInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    n = mmRb_First(&p->vPipelineInfos.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKPipelineInfo*)it->k;
        mmRbtsetVpt_Erase(&p->vPipelineInfos, it);
        mmVKPipelineInfo_Decrease(s);
        mmVKPipelineLoader_UnloadByName(pPipelineLoader, mmString_CStr(&s->name));
    }
}
