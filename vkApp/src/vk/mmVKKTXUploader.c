/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKKTXUploader.h"

#include "core/mmAlloc.h"

#include <stdlib.h>
#include <memory.h>

#include "ktxvulkan.h"
#include "ktxint.h"
#include "unused.h"
#include "texture1.h"
#include "texture2.h"
#include "vk_format.h"

// Macro to check and display Vulkan return results.
// Use when the only possible errors are caused by invalid usage by this loader.
#if defined(_DEBUG)
#define VK_CHECK_RESULT(f)                                                  \
{                                                                           \
    VkResult res = (f);                                                     \
    if (res != VK_SUCCESS)                                                  \
    {                                                                       \
        /* XXX Find an errorString function. */                             \
        fprintf(stderr, "Fatal error in ktxLoadvImage*: "                \
                "VkResult is \"%d\" in %s at line %d\n",                    \
                res, __FILE__, __LINE__);                                   \
        assert(res == VK_SUCCESS);                                          \
    }                                                                       \
}
#else
#define VK_CHECK_RESULT(f) ((void)f)
#endif

#define ARRAY_LEN(a) (sizeof(a) / sizeof(a[0]))

#define DEFAULT_FENCE_TIMEOUT 100000000000
#define VK_FLAGS_NONE 0

//======================================================================
//  ReadImages callbacks
//======================================================================

typedef struct user_cbdata_optimal {
    VkBufferImageCopy* region; // Specify destination region in final image.
    VkDeviceSize offset;       // Offset of current level in staging buffer
    ktx_uint32_t numFaces;
    ktx_uint32_t numLayers;
    // The following are used only by optimalTilingPadCallback
    ktx_uint8_t* dest;         // Pointer to mapped staging buffer.
    ktx_uint32_t elementSize;
    ktx_uint32_t numDimensions;
#if defined(_DEBUG)
    VkBufferImageCopy* regionsArrayEnd;
#endif
} user_cbdata_optimal;

/**
 * @internal
 * @~English
 * @brief Callback for optimally tiled textures with no source row padding.
 *
 * Images must be preloaded into the staging buffer. Each iteration, i.e.
 * the value of @p faceLodSize must be for a complete mip level, regardless of
 * texture type. This should be used only with @c ktx_Texture_IterateLevels.
 *
 * Sets up a region to copy the data from the staging buffer to the final
 * image.
 *
 * @note @p pixels is not used.
 *
 * @copydetails PFNKTXITERCB
 */
static ktx_error_code_e
optimalTilingCallback(int miplevel, int face,
                      int width, int height, int depth,
                      ktx_uint64_t faceLodSize,
                      void* pixels, void* userdata)
{
    user_cbdata_optimal* ud = (user_cbdata_optimal*)userdata;
    UNUSED(pixels);

    // Set up copy to destination region in final image
#if defined(_DEBUG)
    assert(ud->region < ud->regionsArrayEnd);
#endif
    ud->region->bufferOffset = ud->offset;
    ud->offset += faceLodSize;
    // These 2 are expressed in texels.
    ud->region->bufferRowLength = 0;
    ud->region->bufferImageHeight = 0;
    ud->region->imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    ud->region->imageSubresource.mipLevel = miplevel;
    ud->region->imageSubresource.baseArrayLayer = face;
    ud->region->imageSubresource.layerCount = ud->numLayers * ud->numFaces;
    ud->region->imageOffset.x = 0;
    ud->region->imageOffset.y = 0;
    ud->region->imageOffset.z = 0;
    ud->region->imageExtent.width = width;
    ud->region->imageExtent.height = height;
    ud->region->imageExtent.depth = depth;

    ud->region += 1;

    return KTX_SUCCESS;
}

uint32_t lcm4(uint32_t a);

/**
 * @internal
 * @~English
 * @brief Callback for optimally tiled textures with possible source row
 *        padding.
 *
 * Copies data to the staging buffer removing row padding, if necessary.
 * Increments the offset for destination of the next copy increasing it to an
 * appropriate common multiple of the element size and 4 to comply with Vulkan
 * valid usage. Finally sets up a region to copy the face/lod from the staging
 * buffer to the final image.
 *
 * This longer method is needed because row padding is different between
 * KTX (pad to 4) and Vulkan (none). Also region->bufferOffset, i.e. the start
 * of each image, has to be a multiple of 4 and also a multiple of the
 * element size.
 *
 * This should be used with @c ktx_Texture_IterateFaceLevels or
 * @c ktx_Texture_IterateLoadLevelFaces. Face-level iteration has been
 * selected to minimize the buffering needed between reading the file and
 * copying the data into the staging buffer. Obviously when
 * @c ktx_Texture_IterateFaceLevels is being used, this is a moot point.
*
 * @copydetails PFNKTXITERCB
 */
static ktx_error_code_e
optimalTilingPadCallback(int miplevel, int face,
                         int width, int height, int depth,
                         ktx_uint64_t faceLodSize,
                         void* pixels, void* userdata)
{
    user_cbdata_optimal* ud = (user_cbdata_optimal*)userdata;
    ktx_uint32_t rowPitch = width * ud->elementSize;

    // Set bufferOffset in destination region in final image
#if defined(_DEBUG)
    assert(ud->region < ud->regionsArrayEnd);
#endif
    ud->region->bufferOffset = ud->offset;

    // Copy data into staging buffer
    if (_KTX_PAD_UNPACK_ALIGN_LEN(rowPitch) == 0) {
        // No padding. Can copy in bulk.
        mmMemcpy(ud->dest + ud->offset, pixels, faceLodSize);
        ud->offset += faceLodSize;
    } else {
        // Must remove padding. Copy a row at a time.
        ktx_uint32_t image, imageIterations;
        ktx_int32_t row;
        ktx_uint32_t paddedRowPitch;

        if (ud->numDimensions == 3)
            imageIterations = depth;
        else if (ud->numLayers > 1)
            imageIterations = ud->numLayers * ud->numFaces;
        else
            imageIterations = 1;
        rowPitch = paddedRowPitch = width * ud->elementSize;
        paddedRowPitch = _KTX_PAD_UNPACK_ALIGN(paddedRowPitch);
        for (image = 0; image < imageIterations; image++) {
            for (row = 0; row < height; row++) {
                mmMemcpy(ud->dest + ud->offset, pixels, rowPitch);
                ud->offset += rowPitch;
                pixels = (ktx_uint8_t*)pixels + paddedRowPitch;
            }
        }
    }

    // Round to needed multiples for next region, if necessary.
    if (ud->offset % ud->elementSize != 0 || ud->offset % 4 != 0) {
        // Only elementSizes of 1,2 and 3 will bring us here.
        assert(ud->elementSize < 4 && ud->elementSize > 0);
        ktx_uint32_t lcm = lcm4(ud->elementSize);
        ud->offset = _KTX_PADN(lcm, ud->offset);
    }
    // These 2 are expressed in texels; not suitable for dealing with padding.
    ud->region->bufferRowLength = 0;
    ud->region->bufferImageHeight = 0;
    ud->region->imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    ud->region->imageSubresource.mipLevel = miplevel;
    ud->region->imageSubresource.baseArrayLayer = face;
    ud->region->imageSubresource.layerCount = ud->numLayers * ud->numFaces;
    ud->region->imageOffset.x = 0;
    ud->region->imageOffset.y = 0;
    ud->region->imageOffset.z = 0;
    ud->region->imageExtent.width = width;
    ud->region->imageExtent.height = height;
    ud->region->imageExtent.depth = depth;

    ud->region += 1;

    return KTX_SUCCESS;
}

typedef struct user_cbdata_linear {
    VkImage destImage;
    VkDevice device;
    uint8_t* dest;   // Pointer to mapped Image memory
    ktxTexture* texture;
} user_cbdata_linear;

/**
 * @internal
 * @~English
 * @brief Callback for linear tiled textures with no source row padding.
 *
 * Copy the image data into the mapped Vulkan image.
 */
static ktx_error_code_e
linearTilingCallback(int miplevel, int face,
                      int width, int height, int depth,
                      ktx_uint64_t faceLodSize,
                      void* pixels, void* userdata)
{
    user_cbdata_linear* ud = (user_cbdata_linear*)userdata;
    VkSubresourceLayout subResLayout;
    VkImageSubresource subRes = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .mipLevel = miplevel,
      .arrayLayer = face
    };
    UNUSED(width);
    UNUSED(height);
    UNUSED(depth);

    // Get sub resources layout. Includes row pitch, size,
    // offsets, etc.
    vkGetImageSubresourceLayout(ud->device, ud->destImage, &subRes,
                                &subResLayout);
    // Copies all images of the miplevel (for array & 3d) or a single face.
    mmMemcpy(ud->dest + subResLayout.offset, pixels, faceLodSize);
    return KTX_SUCCESS;
}

/**
 * @internal
 * @~English
 * @brief Callback for linear tiled textures with possible source row
 *        padding.
 *
 * Need to use this long method as row padding is different
 * between KTX (pad to 4) and Vulkan (none).
 *
 * In theory this should work for the no-padding case too but it is much
 * clearer and a bit faster to use the simple callback above. It also avoids
 * potential Vulkan implementation bugs.
 *
 * I have seen weird subResLayout results with a BC2_UNORM texture in the only
 * real Vulkan implementation I have available (Mesa). The reported row & image
 * strides appears to be for an R8G8B8A8_UNORM of the same texel size.
 */
static ktx_error_code_e
linearTilingPadCallback(int miplevel, int face,
                      int width, int height, int depth,
                      ktx_uint64_t faceLodSize,
                      void* pixels, void* userdata)
{
    user_cbdata_linear* ud = (user_cbdata_linear*)userdata;
    VkSubresourceLayout subResLayout;
    VkImageSubresource subRes = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .mipLevel = miplevel,
      .arrayLayer = face
    };
    VkDeviceSize offset;
    ktx_size_t   imageSize = 0;
    VkDeviceSize imagePitch = 0;
    ktx_uint32_t srcRowPitch;
    ktx_uint32_t rowIterations;
    ktx_uint32_t imageIterations;
    ktx_uint32_t row, image;
    ktx_uint8_t* pSrc;
    ktx_size_t   copySize;
    UNUSED(width);

    // Get sub resources layout. Includes row pitch, size,
    // offsets, etc.
    vkGetImageSubresourceLayout(ud->device, ud->destImage, &subRes,
                                &subResLayout);

    srcRowPitch = ktxTexture_GetRowPitch(ud->texture, miplevel);

    if (subResLayout.rowPitch != srcRowPitch)
        rowIterations = height;
    else
        rowIterations = 1;

    imageIterations = 1;
    // Arrays, including cube map arrays, or 3D textures
    // Note from the Vulkan spec:
    //  *  arrayPitch is undefined for images that were not
    //     created as arrays.
    //  *  depthPitch is defined only for 3D images.
    if (ud->texture->numLayers > 1 || ud->texture->numDimensions == 3) {
        imageSize = ktxTexture_GetImageSize(ud->texture, miplevel);
        if (ud->texture->numLayers > 1) {
            imagePitch = subResLayout.arrayPitch;
            if (imagePitch != imageSize)
                imageIterations
                        = ud->texture->numLayers * ud->texture->numFaces;
        } else {
            imagePitch = subResLayout.depthPitch;
            if (imagePitch != imageSize)
                imageIterations = depth;
        }
        assert(imageSize <= imagePitch);
    }

    if (rowIterations > 1) {
        // Copy the minimum of srcRowPitch, the GL_UNPACK_ALIGNMENT padded size,
        // and subResLayout.rowPitch.
        if (subResLayout.rowPitch < srcRowPitch)
            copySize = subResLayout.rowPitch;
        else
            copySize = srcRowPitch;
    } else if (imageIterations > 1)
        copySize = faceLodSize / imageIterations;
    else
        copySize = faceLodSize;

    offset = subResLayout.offset;
    // Copy image data to destImage via its mapped memory.
    for (image = 0; image < imageIterations; image++) {
        pSrc = (ktx_uint8_t*)pixels + imageSize * image;
        for (row = 0; row < rowIterations; row++) {
            mmMemcpy(ud->dest + offset, pSrc, copySize);
            offset += subResLayout.rowPitch;
            pSrc += srcRowPitch;
          }
        offset += imagePitch;
    }
    return KTX_SUCCESS;
}

void
mmVKKTXUploader_GenerateMipmaps(
    struct mmVKUploader*                           vdi,
    struct mmVKImage*                              pImage,
    VkFilter                                       filter,
    VkImageLayout                                  initialLayout)
{
    uint32_t i = 0;

    // GenerateMipmaps vkCmdBlitImage need VK_QUEUE_GRAPHICS_BIT.
    VkCommandBuffer cmdBuffer = vdi->graphicsCmdBuffer;

    VkImageSubresourceRange subresourceRange;
    mmMemset(&subresourceRange, 0, sizeof(subresourceRange));
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = 1;
    subresourceRange.baseArrayLayer = 0;
    subresourceRange.layerCount = pImage->layerCount;

    // Transition base level to SRC_OPTIMAL for blitting.
    mmVKSetImageLayout(
        cmdBuffer,
        pImage->image,
        initialLayout,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        &subresourceRange);

    // Generate the mip chain
    // ----------------------
    // Blit level n from level n-1.
    for (i = 1; i < pImage->levelCount; i++)
    {
        VkImageBlit imageBlit;
        mmMemset(&imageBlit, 0, sizeof(imageBlit));

        // Source
        imageBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.srcSubresource.layerCount = pImage->layerCount;
        imageBlit.srcSubresource.mipLevel = i-1;
        imageBlit.srcOffsets[1].x = MAX(1, pImage->width >> (i - 1));
        imageBlit.srcOffsets[1].y = MAX(1, pImage->height >> (i - 1));
        imageBlit.srcOffsets[1].z = MAX(1, pImage->depth >> (i - 1));;

        // Destination
        imageBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.dstSubresource.layerCount = 1;
        imageBlit.dstSubresource.mipLevel = i;
        imageBlit.dstOffsets[1].x = MAX(1, pImage->width >> i);
        imageBlit.dstOffsets[1].y = MAX(1, pImage->height >> i);
        imageBlit.dstOffsets[1].z = MAX(1, pImage->depth >> i);

        VkImageSubresourceRange mipSubRange;
        mmMemset(&mipSubRange, 0, sizeof(mipSubRange));

        mipSubRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        mipSubRange.baseMipLevel = i;
        mipSubRange.levelCount = 1;
        mipSubRange.layerCount = pImage->layerCount;

        // Transiton current mip level to transfer dest
        mmVKSetImageLayout(
            cmdBuffer,
            pImage->image,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &mipSubRange);

        // Blit from previous level
        vkCmdBlitImage(
            cmdBuffer,
            pImage->image,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            pImage->image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1,
            &imageBlit,
            filter);

        // Transiton current mip level to transfer source for read in
        // next iteration.
        mmVKSetImageLayout(
            cmdBuffer,
            pImage->image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            &mipSubRange);
    }

    // After the loop, all mip layers are in TRANSFER_SRC layout.
    // Transition all to final layout.
    subresourceRange.levelCount = pImage->levelCount;
    mmVKSetImageLayout(
        cmdBuffer,
        pImage->image,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        pImage->imageLayout,
        &subresourceRange);
}

ktx_error_code_e
mmVKKTXUploader_VkUploadEx(
    ktxTexture*                                    This,
    struct mmVKUploader*                           vdi,
    struct mmVKImage*                              pImage,
    VkImageTiling                                  tiling,
    VkImageUsageFlags                              usageFlags,
    VkImageLayout                                  finalLayout)
{
    ktx_error_code_e          kResult;
    VkFilter                 blitFilter = VK_FILTER_LINEAR;
    VkFormat                 vkFormat;
    VkImageType              imageType;
    VkImageViewType          viewType;
    VkImageCreateFlags       createFlags = 0;
    VkImageFormatProperties  imageFormatProperties;
    VkResult                 vResult;
    VkQueue                  queue = VK_NULL_HANDLE;
    VkCommandBuffer          cmdBuffer = VK_NULL_HANDLE;

    VkCommandBufferBeginInfo cmdBufBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL
    };
    VkImageCreateInfo        imageCreateInfo = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .pNext = NULL
    };

    ktx_uint32_t             numImageLayers, numImageLevels;
    ktx_uint32_t elementSize = ktxTexture_GetElementSize(This);
    ktx_bool_t               canUseFasterPath;

    if (!vdi || !This || !pImage) {
        return KTX_INVALID_VALUE;
    }

    if (!This->pData && !ktxTexture_isActiveStream(This)) {
        /* Nothing to upload. */
        return KTX_INVALID_OPERATION;
    }

    /* _ktxCheckHeader should have caught this. */
    assert(This->numFaces == 6 ? This->numDimensions == 2 : VK_TRUE);

    numImageLayers = This->numLayers;
    if (This->isCubemap) {
        numImageLayers *= 6;
        createFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }

    assert(This->numDimensions >= 1 && This->numDimensions <= 3);
    switch (This->numDimensions) {
      case 1:
        imageType = VK_IMAGE_TYPE_1D;
        viewType = This->isArray ?
                        VK_IMAGE_VIEW_TYPE_1D_ARRAY : VK_IMAGE_VIEW_TYPE_1D;
        break;
      case 2:
      default: // To keep compilers happy.
        imageType = VK_IMAGE_TYPE_2D;
        if (This->isCubemap)
            viewType = This->isArray ?
                        VK_IMAGE_VIEW_TYPE_CUBE_ARRAY : VK_IMAGE_VIEW_TYPE_CUBE;
        else
            viewType = This->isArray ?
                        VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;
        break;
      case 3:
        imageType = VK_IMAGE_TYPE_3D;
        /* 3D array textures not supported in Vulkan. Attempts to create or
         * load them should have been trapped long before this.
         */
        assert(!This->isArray);
        viewType = VK_IMAGE_VIEW_TYPE_3D;
        break;
    }

    vkFormat = ktxTexture_GetVkFormat(This);
    if (vkFormat == VK_FORMAT_UNDEFINED) {
        return KTX_INVALID_OPERATION;
    }

    /* Get device properties for the requested image format */
    if (tiling == VK_IMAGE_TILING_OPTIMAL) {
        // Ensure we can copy from staging buffer to image.
        usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }
    if (This->generateMipmaps) {
        // Ensure we can blit between levels.
        usageFlags |= (VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    }
    vResult = vkGetPhysicalDeviceImageFormatProperties(vdi->physicalDevice,
                                                      vkFormat,
                                                      imageType,
                                                      tiling,
                                                      usageFlags,
                                                      createFlags,
                                                      &imageFormatProperties);
    if (vResult == VK_ERROR_FORMAT_NOT_SUPPORTED) {
        return KTX_INVALID_OPERATION;
    }
    if (This->numLayers > imageFormatProperties.maxArrayLayers) {
        return KTX_INVALID_OPERATION;
    }

    if (This->generateMipmaps) {
        uint32_t max_dim;
        VkFormatProperties    formatProperties;
        VkFormatFeatureFlags  formatFeatureFlags;
        VkFormatFeatureFlags  neededFeatures
            = VK_FORMAT_FEATURE_BLIT_DST_BIT | VK_FORMAT_FEATURE_BLIT_SRC_BIT;
        vkGetPhysicalDeviceFormatProperties(vdi->physicalDevice,
                                            vkFormat,
                                            &formatProperties);
        assert(vResult == VK_SUCCESS);
        if (tiling == VK_IMAGE_TILING_OPTIMAL)
            formatFeatureFlags = formatProperties.optimalTilingFeatures;
        else
            formatFeatureFlags = formatProperties.linearTilingFeatures;

        if ((formatFeatureFlags & neededFeatures) != neededFeatures)
            return KTX_INVALID_OPERATION;

        if (formatFeatureFlags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)
            blitFilter = VK_FILTER_LINEAR;
        else
            blitFilter = VK_FILTER_NEAREST; // XXX INVALID_OP?

        max_dim = MAX(MAX(This->baseWidth, This->baseHeight), This->baseDepth);
        numImageLevels = (uint32_t)floor(log2(max_dim)) + 1;

        // GenerateMipmaps vkCmdBlitImage need VK_QUEUE_GRAPHICS_BIT.
        queue = vdi->graphicsQueue;
        cmdBuffer = vdi->graphicsCmdBuffer;
    } else {
        numImageLevels = This->numLevels;

        // general only need VK_QUEUE_TRANSFER_BIT
        queue = vdi->transferQueue;
        cmdBuffer = vdi->transferCmdBuffer;
    }

    if (numImageLevels > imageFormatProperties.maxMipLevels) {
        return KTX_INVALID_OPERATION;
    }

    if (This->classId == ktxTexture2_c) {
        canUseFasterPath = KTX_TRUE;
    } else {
        ktx_uint32_t actualRowPitch = ktxTexture_GetRowPitch(This, 0);
        ktx_uint32_t tightRowPitch = elementSize * This->baseWidth;
        // If the texture's images do not have any row padding, we can use a
        // faster path. Only uncompressed textures might have padding.
        //
        // The first test in the if will match compressed textures, because
        // they all have a block size that is a multiple of 4, as well as
        // a class of uncompressed textures that will never need padding.
        //
        // The second test matches textures whose level 0 has no padding. Any
        // texture whose block size is not a multiple of 4 will need padding
        // at some miplevel even if level 0 does not. So, if more than 1 level
        // exists, we must use the slower path.
        //
        // Note all elementSizes > 4 Will be a multiple of 4, so only
        // elementSizes of 1, 2 & 3 are a concern here.
        if (elementSize % 4 == 0  /* There'll be no padding at any level. */
               /* There is no padding at level 0 and no other levels. */
            || (This->numLevels == 1 && actualRowPitch == tightRowPitch))
            canUseFasterPath = KTX_TRUE;
        else
            canUseFasterPath = KTX_FALSE;
    }

    pImage->width = This->baseWidth;
    pImage->height = This->baseHeight;
    pImage->depth = This->baseDepth;
    pImage->imageLayout = finalLayout;
    pImage->format = vkFormat;
    pImage->levelCount = numImageLevels;
    pImage->layerCount = numImageLayers;
    pImage->viewType = viewType;

    VK_CHECK_RESULT(vkBeginCommandBuffer(cmdBuffer, &cmdBufBeginInfo));

    if (tiling == VK_IMAGE_TILING_OPTIMAL)
    {
        // Create a host-visible staging buffer that contains the raw image data
        VkBuffer stagingBuffer;
        VkBufferImageCopy* copyRegions;
        VkDeviceSize textureSize;
        VkBufferCreateInfo bufferCreateInfo = {
          .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
          .pNext = NULL
        };
        VkImageSubresourceRange subresourceRange;
        VkFence copyFence;
        VkFenceCreateInfo fenceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .pNext = NULL,
            .flags = VK_FLAGS_NONE
        };
        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext = NULL
        };
        ktx_uint8_t* pMappedStagingBuffer;
        ktx_uint32_t numCopyRegions;
        user_cbdata_optimal cbData;
        
        VmaAllocationCreateInfo allocInfoStage = { 0 };
        VmaAllocationCreateInfo allocInfoImage = { 0 };
        
        VmaAllocation allocationStage;
        VmaAllocationInfo allocationInfoStage;

        textureSize = ktxTexture_GetDataSizeUncompressed(This);
        bufferCreateInfo.size = textureSize;
        if (canUseFasterPath) {
            /*
             * Because all array layers and faces are the same size they can
             * be copied in a single operation so there'll be 1 copy per mip
             * level.
             */
            numCopyRegions = This->numLevels;
        } else {
            /*
             * Have to copy all images individually into the staging
             * buffer so we can place them at correct multiples of
             * elementSize and 4 and also need a copy region per image
             * in case they end up with padding between them.
             */
            numCopyRegions = This->isArray ? This->numLevels
                                  : This->numLevels * This->numFaces;
            /*
             * Add extra space to allow for possible padding described
             * above. A bit ad-hoc but it's only a small amount of
             * memory.
             */
            bufferCreateInfo.size += numCopyRegions * elementSize * 4;
        }
        copyRegions = (VkBufferImageCopy*)mmMalloc(sizeof(VkBufferImageCopy)
                                                   * numCopyRegions);
        if (copyRegions == NULL) {
            return KTX_OUT_OF_MEMORY;
        }

        // This buffer is used as a transfer source for the buffer copy
        bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        
        allocInfoStage.flags = 0;
        allocInfoStage.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        VK_CHECK_RESULT(vmaCreateBuffer(vdi->vmaAllocator,
                                        &bufferCreateInfo,
                                        &allocInfoStage,
                                        &stagingBuffer,
                                        &allocationStage,
                                        &allocationInfoStage));
        VK_CHECK_RESULT(vmaMapMemory(vdi->vmaAllocator, allocationStage, (void**)&pMappedStagingBuffer));

        cbData.offset = 0;
        cbData.region = copyRegions;
        cbData.numFaces = This->numFaces;
        cbData.numLayers = This->numLayers;
        cbData.dest = pMappedStagingBuffer;
        cbData.elementSize = elementSize;
        cbData.numDimensions = This->numDimensions;
#if defined(_DEBUG)
        cbData.regionsArrayEnd = copyRegions + numCopyRegions;
#endif
        if (canUseFasterPath) {
            // Bulk load the data to the staging buffer and iterate
            // over levels.

            if (This->pData) {
                // Image data has already been loaded. Copy to staging
                // buffer.
                assert(This->dataSize <= allocationInfoStage.size);
                mmMemcpy(pMappedStagingBuffer, This->pData, This->dataSize);
            } else {
                /* Load the image data directly into the staging buffer. */
                /* The strange cast quiets an Xcode warning when building
                 * for the Generic iOS Device where size_t is 32-bit even
                 * when building for arm64. */
                kResult = ktxTexture_LoadImageData(This,
                                      pMappedStagingBuffer,
                                      (ktx_size_t)allocationInfoStage.size);
                if (kResult != KTX_SUCCESS)
                    return kResult;
            }

            // Iterate over mip levels to set up the copy regions.
            kResult = ktxTexture_IterateLevels(This,
                                               optimalTilingCallback,
                                               &cbData);
            // XXX Check for possible errors.
        } else {
            // Iterate over face-levels with callback that copies the
            // face-levels to Vulkan-valid offsets in the staging buffer while
            // removing padding. Using face-levels minimizes pre-staging-buffer
            // buffering, in the event the data is not already loaded.
            if (This->pData) {
                kResult = ktxTexture_IterateLevelFaces(
                                            This,
                                            optimalTilingPadCallback,
                                            &cbData);
            } else {
                kResult = ktxTexture_IterateLoadLevelFaces(
                                            This,
                                            optimalTilingPadCallback,
                                            &cbData);
                // XXX Check for possible errors.
            }
        }

        vmaUnmapMemory(vdi->vmaAllocator, allocationStage);

        // Create optimal tiled target image
        imageCreateInfo.imageType = imageType;
        imageCreateInfo.flags = createFlags;
        imageCreateInfo.format = vkFormat;
        // numImageLevels ensures enough levels for generateMipmaps.
        imageCreateInfo.mipLevels = numImageLevels;
        imageCreateInfo.arrayLayers = numImageLayers;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCreateInfo.usage = usageFlags;
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        imageCreateInfo.extent.width = pImage->width;
        imageCreateInfo.extent.height = pImage->height;
        imageCreateInfo.extent.depth = pImage->depth;
        
        allocInfoImage.flags = 0;
        allocInfoImage.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        VK_CHECK_RESULT(vmaCreateImage(vdi->vmaAllocator,
                                       &imageCreateInfo,
                                       &allocInfoImage,
                                       &pImage->image,
                                       &pImage->allocation,
                                       NULL));
        
        // VK_CHECK_RESULT(vmaBindImageMemory(vdi->allocator, vImage->allocation, vImage->image));

        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = This->numLevels;
        subresourceRange.baseArrayLayer = 0;
        subresourceRange.layerCount = numImageLayers;

        // Image barrier to transition, possibly only the base level, image
        // layout to TRANSFER_DST_OPTIMAL so it can be used as the copy
        // destination.
        mmVKSetImageLayout(
            cmdBuffer,
            pImage->image,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &subresourceRange);

        // Copy mip levels from staging buffer
        vkCmdCopyBufferToImage(
            cmdBuffer, stagingBuffer,
            pImage->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            numCopyRegions, copyRegions
            );

        // free copyRegions.
        mmFree(copyRegions);
        
        if (This->generateMipmaps) {
            mmVKKTXUploader_GenerateMipmaps(vdi, pImage,
                            blitFilter, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        } else {
            // Transition image layout to finalLayout after all mip levels
            // have been copied.
            // In this case numImageLevels == This->numLevels
            //subresourceRange.levelCount = numImageLevels;
            mmVKSetImageLayout(
                cmdBuffer,
                pImage->image,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                finalLayout,
                &subresourceRange);
        }

        // Submit command buffer containing copy and image layout commands
        VK_CHECK_RESULT(vkEndCommandBuffer(cmdBuffer));

        // Create a fence to make sure that the copies have finished before
        // continuing
        VK_CHECK_RESULT(vkCreateFence(vdi->device, &fenceCreateInfo,
                                      vdi->pAllocator, &copyFence));

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdBuffer;

        VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, copyFence));

        VK_CHECK_RESULT(vkWaitForFences(vdi->device, 1, &copyFence,
                                        VK_TRUE, DEFAULT_FENCE_TIMEOUT));

        vkDestroyFence(vdi->device, copyFence, vdi->pAllocator);

        // Clean up staging resources
        vmaDestroyBuffer(vdi->vmaAllocator, stagingBuffer, allocationStage);
    }
    else
    {
        VkImage mappableImage;
        VkFence nullFence = { VK_NULL_HANDLE };
        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext = NULL
        };
        user_cbdata_linear cbData;
        PFNKTXITERCB callback;
        
        VmaAllocation allocationImage;
        VmaAllocationInfo allocationInfoImage;
        VmaAllocationCreateInfo allocInfoImage = { 0 };

        imageCreateInfo.imageType = imageType;
        imageCreateInfo.flags = createFlags;
        imageCreateInfo.format = vkFormat;
        imageCreateInfo.extent.width = pImage->width;
        imageCreateInfo.extent.height = pImage->height;
        imageCreateInfo.extent.depth = pImage->depth;
        // numImageLevels ensures enough levels for generateMipmaps.
        imageCreateInfo.mipLevels = numImageLevels;
        imageCreateInfo.arrayLayers = numImageLayers;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.tiling = VK_IMAGE_TILING_LINEAR;
        imageCreateInfo.usage = usageFlags;
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;

        // Load mip map level 0 to linear tiling image
        allocInfoImage.flags = 0;
        allocInfoImage.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        VK_CHECK_RESULT(vmaCreateImage(vdi->vmaAllocator,
                                       &imageCreateInfo,
                                       &allocInfoImage,
                                       &mappableImage,
                                       &allocationImage,
                                       &allocationInfoImage));
        
        VK_CHECK_RESULT(vmaBindImageMemory(vdi->vmaAllocator, allocationImage, mappableImage));
        VK_CHECK_RESULT(vmaMapMemory(vdi->vmaAllocator, allocationImage, (void**)&cbData.dest));

        cbData.destImage = mappableImage;
        cbData.device = vdi->device;
        cbData.texture = This;
        callback = canUseFasterPath ? linearTilingCallback : linearTilingPadCallback;
        
        // Iterate over images to copy texture data into mapped image memory.
        if (ktxTexture_isActiveStream(This)) {
            kResult = ktxTexture_IterateLoadLevelFaces(This,
                                                       callback,
                                                       &cbData);
        } else {
            kResult = ktxTexture_IterateLevelFaces(This,
                                                   callback,
                                                   &cbData);
        }
        // XXX Check for possible errors
        vmaUnmapMemory(vdi->vmaAllocator, allocationImage);

        // Linear tiled images can be directly used as textures.
        pImage->image = mappableImage;

        if (This->generateMipmaps) {
            mmVKKTXUploader_GenerateMipmaps(vdi, pImage,
                            blitFilter,
                            VK_IMAGE_LAYOUT_PREINITIALIZED);
        } else {
            VkImageSubresourceRange subresourceRange;
            subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            subresourceRange.baseMipLevel = 0;
            subresourceRange.levelCount = numImageLevels;
            subresourceRange.baseArrayLayer = 0;
            subresourceRange.layerCount = numImageLayers;

           // Transition image layout to finalLayout.
            mmVKSetImageLayout(
                cmdBuffer,
                pImage->image,
                VK_IMAGE_LAYOUT_PREINITIALIZED,
                finalLayout,
                &subresourceRange);
        }

        // Submit command buffer containing image layout commands
        VK_CHECK_RESULT(vkEndCommandBuffer(cmdBuffer));

        submitInfo.waitSemaphoreCount = 0;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdBuffer;

        VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, nullFence));
        VK_CHECK_RESULT(vkQueueWaitIdle(queue));
    }
    return KTX_SUCCESS;
}
