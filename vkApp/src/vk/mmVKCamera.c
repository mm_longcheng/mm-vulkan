/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKCamera.h"

#include "core/mmLogger.h"

#include "math/mmMath.h"
#include "math/mmMatrix4x4.h"
#include "math/mmMatrix4x4Projection.h"

void
mmVKCamera_Init(
    struct mmVKCamera*                             p)
{
    mmMat4x4MakeIdentity(p->projection);
    mmMat4x4MakeIdentity(p->view);
    p->n = 0.1f;
    p->f = 50.0f;
}

void
mmVKCamera_Destroy(
    struct mmVKCamera*                             p)
{
    p->f = 50.0f;
    p->n = 0.1f;
    mmMat4x4MakeIdentity(p->view);
    mmMat4x4MakeIdentity(p->projection);
}

void
mmVKCamera_OrthogonalVK(
    struct mmVKCamera* p,
    float l, float r,
    float b, float t,
    float n, float f)
{
    float eye[3] = { 0.0f, 0.0f, 0.0f };
    float origin[3] = { 0, 0, -1, };
    float up[3] = { 0.0f, 1.0f, 0.0 };
    mmMat4x4OrthogonalVK(p->projection, l, r, b, t, n, f);
    mmMat4x4LookAtLH(p->view, eye, origin, up);
    p->n = n;
    p->f = f;
}

void
mmVKCamera_PerspectiveVK(
    struct mmVKCamera* p,
    float fovy,
    float aspect,
    float n,
    float f)
{
    float eye[3] = { 0.0f, 0.0f, 0.0f };
    float origin[3] = { 0, 0, -1, };
    float up[3] = { 0.0f, 1.0f, 0.0 };
    mmMat4x4PerspectiveVK(p->projection, fovy, aspect, n, f);
    mmMat4x4LookAtRH(p->view, eye, origin, up);
    p->n = n;
    p->f = f;
}
