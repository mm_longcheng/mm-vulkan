/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKLayoutLoader.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKDescriptorLayout.h"
#include "vk/mmVKJSON.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByte.h"

#include "container/mmVectorValue.h"

#include "dish/mmFileContext.h"

void
mmVKLayoutInfo_Init(
    struct mmVKLayoutInfo*                         p)
{
    mmString_Init(&p->name);
    mmVKDescriptorSetLayoutBindings_Init(&p->dslb);
    p->dslc = 64;
    p->descriptorSetLayout = VK_NULL_HANDLE;
    p->reference = 0;
}

void
mmVKLayoutInfo_Destroy(
    struct mmVKLayoutInfo*                         p)
{
    p->reference = 0;
    p->descriptorSetLayout = VK_NULL_HANDLE;
    p->dslc = 64;
    mmVKDescriptorSetLayoutBindings_Destroy(&p->dslb);
    mmString_Destroy(&p->name);
}

int
mmVKLayoutInfo_Invalid(
    const struct mmVKLayoutInfo*                   p)
{
    return
        (NULL == p) ||
        (VK_NULL_HANDLE == p->descriptorSetLayout);
}

void
mmVKLayoutInfo_Increase(
    struct mmVKLayoutInfo*                         p)
{
    if (NULL != p)
    {
        p->reference++;
    }
}

void
mmVKLayoutInfo_Decrease(
    struct mmVKLayoutInfo*                         p)
{
    if (NULL != p)
    {
        assert(0 < p->reference && "reference is invalid.");
        p->reference--;
    }
}

VkResult
mmVKLayoutInfo_Prepare(
    struct mmVKLayoutInfo*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKLayoutCreateInfo*             info)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        const struct mmVectorValue* dslb = info->dslb;

        VkDescriptorSetLayoutCreateInfo hDescriptorSetLayoutInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        mmVKDescriptorSetLayoutBindings_CopyFrom(&p->dslb, info->dslb);
        p->dslc = info->dslc;

        hDescriptorSetLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        hDescriptorSetLayoutInfo.pNext = NULL;
        hDescriptorSetLayoutInfo.flags = 0;

        hDescriptorSetLayoutInfo.bindingCount = (uint32_t)dslb->size;
        hDescriptorSetLayoutInfo.pBindings = (const VkDescriptorSetLayoutBinding*)dslb->arrays;

        err = vkCreateDescriptorSetLayout(
            device,
            &hDescriptorSetLayoutInfo,
            pAllocator,
            &p->descriptorSetLayout);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateDescriptorSetLayout failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

void
mmVKLayoutInfo_Discard(
    struct mmVKLayoutInfo*                         p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (VK_NULL_HANDLE != p->descriptorSetLayout)
        {
            vkDestroyDescriptorSetLayout(
                device,
                p->descriptorSetLayout,
                pAllocator);
            p->descriptorSetLayout = VK_NULL_HANDLE;
        }
    } while (0);
}

void
mmVKLayoutLoader_Init(
    struct mmVKLayoutLoader*                       p)
{
    p->pFileContext = NULL;
    p->pDevice = NULL;
    mmRbtreeStringVpt_Init(&p->sets);
    mmParseJSON_Init(&p->parse);
}

void
mmVKLayoutLoader_Destroy(
    struct mmVKLayoutLoader*                       p)
{
    assert(0 == p->sets.size && "assets is not destroy complete.");

    mmParseJSON_Destroy(&p->parse);
    mmRbtreeStringVpt_Destroy(&p->sets);
    p->pDevice = NULL;
    p->pFileContext = NULL;
}

void
mmVKLayoutLoader_SetFileContext(
    struct mmVKLayoutLoader*                       p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

void
mmVKLayoutLoader_SetDevice(
    struct mmVKLayoutLoader*                       p,
    struct mmVKDevice*                             pDevice)
{
    p->pDevice = pDevice;
}

VkResult
mmVKLayoutLoader_PrepareLayoutInfo(
    struct mmVKLayoutLoader*                       p,
    const struct mmVKLayoutCreateInfo*             info,
    struct mmVKLayoutInfo*                         pLayoutInfo)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        assert(0 == pLayoutInfo->reference && "reference not zero.");

        if (0 == info->dslb->size || 0 == info->dslc)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " info is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }

        err = mmVKLayoutInfo_Prepare(pLayoutInfo, p->pDevice, info);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKLayoutInfo_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }

        pLayoutInfo->reference = 0;
    } while (0);

    return err;
}

void
mmVKLayoutLoader_DiscardLayoutInfo(
    struct mmVKLayoutLoader*                       p,
    struct mmVKLayoutInfo*                         pLayoutInfo)
{
    assert(0 == pLayoutInfo->reference && "reference not zero.");
    mmVKLayoutInfo_Discard(pLayoutInfo, p->pDevice);
}

void
mmVKLayoutLoader_UnloadByName(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name)
{
    struct mmVKLayoutInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKLayoutInfo*)it->v;
        mmVKLayoutLoader_DiscardLayoutInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKLayoutInfo_Destroy(s);
        mmFree(s);
    }
}

void
mmVKLayoutLoader_UnloadComplete(
    struct mmVKLayoutLoader*                       p)
{
    struct mmVKLayoutInfo* s = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sets.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        s = (struct mmVKLayoutInfo*)(it->v);
        mmVKLayoutLoader_DiscardLayoutInfo(p, s);
        mmRbtreeStringVpt_Erase(&p->sets, it);
        mmVKLayoutInfo_Destroy(s);
        mmFree(s);
    }
}

struct mmVKLayoutInfo*
mmVKLayoutLoader_LoadFromInfo(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name,
    const struct mmVKLayoutCreateInfo*             info)
{
    struct mmVKLayoutInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL == it)
    {
        s = (struct mmVKLayoutInfo*)mmMalloc(sizeof(struct mmVKLayoutInfo));
        mmVKLayoutInfo_Init(s);
        it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
        mmString_MakeWeak(&s->name, &it->k);

        mmVKLayoutLoader_PrepareLayoutInfo(p, info, s);
    }
    else
    {
        s = (struct mmVKLayoutInfo*)(it->v);
    }
    return s;
}

struct mmVKLayoutInfo*
mmVKLayoutLoader_LoadFromFile(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name,
    const char*                                    path)
{
    struct mmVKLayoutInfo* s = NULL;

    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmString hWeakKey;

        int i;

        const uint8_t* data = NULL;
        size_t size = 0;

        struct mmByteBuffer bytes;

        struct mmVKLayoutInfoArgs hArgs;
        struct mmVKLayoutCreateInfo hInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakKey, name);
        it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
        if (NULL != it)
        {
            s = (struct mmVKLayoutInfo*)(it->v);
            break;
        }

        if (NULL == p->pFileContext)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pFileContext is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmFileContext_IsFileExists(p->pFileContext, path))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmFileContext_IsFileExists failure. file: %s", __FUNCTION__, __LINE__, path);
            break;
        }

        mmFileContext_AcquireFileByteBuffer(p->pFileContext, path, &bytes);

        if (NULL == bytes.buffer || 0 == bytes.length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " bytes is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        data = bytes.buffer + bytes.offset;
        size = bytes.length;

        mmVKLayoutInfoArgs_Init(&hArgs);

        mmParseJSON_Reset(&p->parse);
        mmParseJSON_SetJsonBuffer(&p->parse, (const char*)data, (size_t)size);
        mmParseJSON_Analysis(&p->parse);
        i = mmParseJSON_DecodeObject(&p->parse, 0, &hArgs, &mmParseJSON_DecodeVKLayoutInfoArgs);
        if (i <= 0)
        {
            if (p->parse.code <= 0)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON AnalysisMetadata failure.", __FUNCTION__, __LINE__);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " JSON root is invalid.", __FUNCTION__, __LINE__);
            }

            // do not break here.
        }
        else
        {
            s = (struct mmVKLayoutInfo*)mmMalloc(sizeof(struct mmVKLayoutInfo));
            mmVKLayoutInfo_Init(s);
            it = mmRbtreeStringVpt_Set(&p->sets, &hWeakKey, (void*)s);
            mmString_MakeWeak(&s->name, &it->k);

            hInfo.dslb = &hArgs.dslb;
            hInfo.dslc = hArgs.dslc;
            mmVKLayoutLoader_PrepareLayoutInfo(p, &hInfo, s);
        }

        mmVKLayoutInfoArgs_Destroy(&hArgs);

        mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &bytes);
    } while (0);

    return s;
}

struct mmVKLayoutInfo*
mmVKLayoutLoader_GetFromName(
    struct mmVKLayoutLoader*                       p,
    const char*                                    name)
{
    struct mmVKLayoutInfo* s = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sets, &hWeakKey);
    if (NULL != it)
    {
        s = (struct mmVKLayoutInfo*)(it->v);
    }
    return s;
}

VkResult
mmVKLayoutLoader_GetLayoutsByStrings(
    struct mmVKLayoutLoader*                       p,
    const struct mmVectorValue*                    strings,
    struct mmVectorVpt*                            dsli)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i;
        const char* name;
        struct mmString* s;
        struct mmVKLayoutInfo* pLayoutInfo;

        assert(NULL != strings && "strings is a invalid.");
        assert(NULL != dsli && "dsli is a invalid.");

        if (0 == strings->size)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        else
        {
            err = VK_SUCCESS;
        }

        mmVectorVpt_Reset(dsli);
        mmVectorVpt_AllocatorMemory(dsli, strings->size);

        for (i = 0; i < strings->size; i++)
        {
            s = (struct mmString*)mmVectorValue_At(strings, i);
            name = mmString_CStr(s);
            pLayoutInfo = mmVKLayoutLoader_GetFromName(p, name);
            if (mmVKLayoutInfo_Invalid(pLayoutInfo))
            {
                err = VK_ERROR_INITIALIZATION_FAILED;
                break;
            }
            mmVectorVpt_SetIndex(dsli, i, pLayoutInfo);
        }

    } while (0);

    return err;
}

VkResult
mmVKLayoutLoader_GetLayoutsByNames(
    struct mmVKLayoutLoader*                       p,
    const struct mmVectorVpt*                      names,
    struct mmVectorVpt*                            dsli)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        size_t i;
        const char* name;
        struct mmVKLayoutInfo* pLayoutInfo;

        assert(NULL != names && "names is a invalid.");
        assert(NULL != dsli && "dsli is a invalid.");

        if (0 == names->size)
        {
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        else
        {
            err = VK_SUCCESS;
        }

        mmVectorVpt_Reset(dsli);
        mmVectorVpt_AllocatorMemory(dsli, names->size);

        for (i = 0; i < names->size; i++)
        {
            name = (const char*)mmVectorVpt_At(names, i);
            pLayoutInfo = mmVKLayoutLoader_GetFromName(p, name);
            if (mmVKLayoutInfo_Invalid(pLayoutInfo))
            {
                err = VK_ERROR_INITIALIZATION_FAILED;
                break;
            }
            mmVectorVpt_SetIndex(dsli, i, pLayoutInfo);
        }

    } while (0);

    return err;
}
