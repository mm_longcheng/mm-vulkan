/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKDevice_h__
#define __mmVKDevice_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKInstance.h"
#include "vk/mmVKPhysicalDevice.h"
#include "vk/mmVKSurface.h"

#include "core/mmPrefix.h"

struct mmVKDeviceCreateInfo
{
    struct mmVKInstance* pInstance;
    struct mmVKPhysicalDevice* pPhysicalDevice;
    VkSurfaceKHR surface;
};

struct mmVKDevice
{
    VkDevice device;
    
    VkQueue graphicsQueue;
    VkQueue computeQueue;
    VkQueue transferQueue;
    VkQueue presentQueue;
    
    uint32_t graphicsIndex;
    uint32_t computeIndex;
    uint32_t transferIndex;
    uint32_t presentIndex;
    
    PFN_vkCreateSwapchainKHR                        vkCreateSwapchainKHR;
    PFN_vkDestroySwapchainKHR                       vkDestroySwapchainKHR;
    PFN_vkGetSwapchainImagesKHR                     vkGetSwapchainImagesKHR;
    PFN_vkAcquireNextImageKHR                       vkAcquireNextImageKHR;
    PFN_vkQueuePresentKHR                           vkQueuePresentKHR;

    const VkAllocationCallbacks* pAllocator;
};

void
mmVKDevice_Reset(
    struct mmVKDevice*                             p);

void
mmVKDevice_SetAllocator(
    struct mmVKDevice*                             p,
    const VkAllocationCallbacks*                   pAllocator);

VkResult
mmVKDevice_EvaluationSuitable(
    struct mmVKDevice*                             p,
    struct mmVKInstance*                           pInstance,
    VkPhysicalDevice                               physicalDevice,
    VkSurfaceKHR                                   surface);

VkResult
mmVKDevice_Create(
    struct mmVKDevice*                             p,
    const struct mmVKDeviceCreateInfo*             info,
    const VkAllocationCallbacks*                   pAllocator);

void
mmVKDevice_Delete(
    struct mmVKDevice*                             p);

void
mmVKDevice_WaitIdle(
    struct mmVKDevice*                             p);

size_t
mmVKIndexTypeSize(
    VkIndexType                                    type);

#include "core/mmSuffix.h"

#endif//__mmVKDevice_h__
