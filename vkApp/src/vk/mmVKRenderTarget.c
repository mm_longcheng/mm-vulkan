/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKRenderTarget.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKRenderTarget_Reset(
    struct mmVKRenderTarget*                       p)
{
    mmMemset(p, 0, sizeof(struct mmVKRenderTarget));
}

VkResult
mmVKRenderTarget_Create(
    struct mmVKRenderTarget*                       p,
    const struct mmVKRenderTargetCreateInfo*       info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        uint32_t i = 0;
        struct mmVKFrameCreateInfo hFrameInfo;
        struct mmVKFrame* frame = NULL;
        VkFormat pickFormat = mmVKPickDepthFormat(pUploader->physicalDevice, info->depthFormat);
        
        struct mmLogger* gLogger = mmLogger_Instance();

        p->depthFormat = pickFormat;
        
        hFrameInfo.format = pickFormat;
        hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
        hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        hFrameInfo.extent[0] = info->windowSize[0];
        hFrameInfo.extent[1] = info->windowSize[1];
        hFrameInfo.extent[2] = 1;
        hFrameInfo.mipLevels = 1;
        hFrameInfo.arrayLayers = 1;
        hFrameInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        hFrameInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        hFrameInfo.vmaFlags = 0;
        hFrameInfo.vmaUsage = info->vmaUsage;

        err = mmVKFrame_Create(&p->depth, &hFrameInfo, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "mmVKFrameCreate failure.");
            break;
        }
        
        p->windowSize[0] = info->windowSize[0];
        p->windowSize[1] = info->windowSize[1];
        
        p->frameCount = info->frameCount;
        p->frames = (struct mmVKFrame*)mmMalloc(sizeof(struct mmVKFrame) * p->frameCount);
        
        hFrameInfo.format = info->surfaceFormat;
        hFrameInfo.mipLevels = 1;
        hFrameInfo.arrayLayers = 1;
        hFrameInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        hFrameInfo.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

        p->surfaceFormat = info->surfaceFormat;
        
        for (i = 0;i < p->frameCount;++i)
        {
            frame = &p->frames[i];
            err = mmVKFrame_Create(frame, &hFrameInfo, pUploader);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "mmVKFrameCreate failure.");
                break;
            }
        }
    } while(0);
    
    return err;
}

void
mmVKRenderTarget_Delete(
    struct mmVKRenderTarget*                       p,
    struct mmVKUploader*                           pUploader)
{
    uint32_t i = 0;
    struct mmVKFrame* frame = NULL;
    for (i = 0;i < p->frameCount;++i)
    {
        frame = &p->frames[i];
        mmVKFrame_Delete(frame, pUploader);
    }
    
    if (NULL != p->frames)
    {
        mmFree(p->frames);
        p->frames = NULL;
        p->frameCount = 0;
    }
    
    mmVKFrame_Delete(&p->depth, pUploader);
}
