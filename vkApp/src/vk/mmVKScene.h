/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKScene_h__
#define __mmVKScene_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUploader.h"
#include "vk/mmVKUniformType.h"
#include "vk/mmVKLight.h"
#include "vk/mmVKDescriptorSet.h"
#include "vk/mmVKTFModel.h"
#include "vk/mmVKShadow.h"
#include "vk/mmVKCamera.h"

#include "core/mmPrefix.h"

struct mmVKDescriptorPool;
struct mmVKDevice;
struct mmVKAssets;
struct mmVKAssetsDefault;

struct mmVKUBOSceneDepthVert
{
    mmUniformMat4 ProjectionView[4];
};

struct mmVKUBOSceneWholeVert
{
    mmUniformMat4 ProjectionView;
    mmUniformMat4 Projection;
    mmUniformMat4 View;
};

struct mmVKUBOSceneFragment
{
    mmUniformMat4 inverseView;
    mmUniformVec3 camera;
    mmUniformVec3 ambient;
    mmUniformFloat exposure;
    mmUniformInt lightCount;
};

extern const struct mmVectorValue mmVKSceneDepth_DSLB0;
extern const struct mmVectorValue mmVKSceneWhole_DSLB0;

extern const char* mmVKSceneWholePoolName;
extern const char* mmVKSceneDepthPoolName;

void
mmVKTFSceneLights_Init(
    struct mmVectorValue*                          p);

void
mmVKTFSceneLights_Destroy(
    struct mmVectorValue* p);

struct mmVKScene
{
    struct mmVKUBOSceneDepthVert uboSceneDepthVert;
    struct mmVKUBOSceneWholeVert uboSceneWholeVert;
    struct mmVKUBOSceneFragment  uboSceneFragment;
    struct mmVectorValue         uboSceneLights;

    struct mmVKBuffer bufferSceneDepthVert;
    struct mmVKBuffer bufferSceneWholeVert;
    struct mmVKBuffer bufferSceneFragment;
    struct mmVKBuffer bufferSceneLights;
    struct mmVKBuffer bufferSceneShadow;
    
    struct mmVKPipelineIndex hIndex;
    struct mmVKShadow hShadow;
    struct mmVKCamera hCamera;

    struct mmVKDescriptorSet vDescriptorSet[mmVKPipelineMax];

    struct mmVKPoolInfo* pPoolInfo[mmVKPipelineMax];
};

void
mmVKScene_Init(
    struct mmVKScene*                              p);

void
mmVKScene_Destroy(
    struct mmVKScene*                              p);

void
mmVKScene_MakeData(
    struct mmVKScene*                              p,
    uint32_t                                       hWindowSize[2]);

void
mmVKScene_SetPipelineIndex(
    struct mmVKScene*                              p,
    int                                            hPipeline,
    int                                            hIdxMat,
    int                                            hIdxMap);

VkResult
mmVKScene_PrepareUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

void
mmVKScene_DiscardUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBufferSceneDepth(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBufferSceneWhole(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBufferSceneFragment(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBufferSceneLights(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBufferShadow(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_UpdateUBOBuffer(
    struct mmVKScene*                              p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKScene_DescriptorSetProduce(
    struct mmVKScene*                              p);

void
mmVKScene_DescriptorSetRecycle(
    struct mmVKScene*                              p);

VkResult
mmVKScene_DescriptorSetUpdateCommon(
    struct mmVKScene*                              p,
    VkDevice                                       device);

VkResult
mmVKScene_DescriptorSetUpdateShadow(
    struct mmVKScene*                              p,
    VkDevice                                       device);

VkResult
mmVKScene_DescriptorSetUpdate(
    struct mmVKScene*                              p,
    VkDevice                                       device);

VkResult
mmVKScene_Prepare(
    struct mmVKScene*                              p,
    struct mmVKAssets*                             pAssets,
    const struct mmVectorValue*                    pLightInfos,
    VkFormat                                       hDepthFormat);

void
mmVKScene_Discard(
    struct mmVKScene*                              p,
    struct mmVKAssets*                             pAssets);

void
mmVKScene_UpdateUBODataLights(
    struct mmVKScene*                              p);

void
mmVKScene_UpdateUBODataTransforms(
    struct mmVKScene*                              p);

void
mmVKScene_UpdateTransforms(
    struct mmVKScene*                              p);

void
mmVKScene_UpdateLights(
    struct mmVKScene*                              p,
    const struct mmVectorValue*                    pLightInfos,
    struct mmVectorVpt                             lights[3]);

#include "core/mmSuffix.h"

#endif//__mmVKScene_h__
