/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKShadow_h__
#define __mmVKShadow_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"
#include "container/mmVectorVpt.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKUniformType.h"
#include "vk/mmVKFrame.h"
#include "vk/mmVKLight.h"

#include "vma/VmaUsage.h"

#include "core/mmPrefix.h"

struct mmVKAssets;
struct mmVKTFObject;
struct mmVKScene;

struct mmVKShadowCascadeFrustum
{
    VkFramebuffer framebuffer;
    VkImageView imageView;

    float ProjectionView[4][4];
    float Projection[4][4];
    float View[4][4];
    float SplitDepth;
};

struct mmVKShadowCascadeLight
{
    // cascade frustum.
    struct mmVKShadowCascadeFrustum hFrustum[4];

    // cascade split lambda. default is 0.95f.
    float hSplitLambda;

    // matrix index.
    int hIdxMat;

    // shadow map index.
    int hIdxMap;

    // shadow mapping cascade count.
    // default is 4. range[0, 4].
    int hCount;
};

struct mmVKShadowCascade
{
    // vector<struct mmVKShadowCascadeLight> lights.
    struct mmVectorValue hLights;

    // depth mapping.
    struct mmVKFrame hShadowMap;
    
    // framebuffer number.
    uint32_t hFrameNumber;

    // shadow map size, default is 2048x2048.
    uint32_t hShadowMapSize[2];

    // depth format.
    VkFormat hDepthFormat;

    // RenderPassInfo.
    struct mmVKPassInfo* pPassInfo;

    // SamplerInfo.
    struct mmVKSamplerInfo* pSamplerInfo;
};

void
mmVKShadowCascade_Init(
    struct mmVKShadowCascade*                      p);

void
mmVKShadowCascade_Destroy(
    struct mmVKShadowCascade*                      p);

VkResult
mmVKShadowCascade_Prepare(
    struct mmVKShadowCascade*                      p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights);

void
mmVKShadowCascade_Discard(
    struct mmVKShadowCascade*                      p,
    struct mmVKAssets*                             pAssets);

VkResult
mmVKShadowCascade_OnRecordCommand(
    struct mmVKShadowCascade*                      p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene);

void
mmVKShadowCascade_UpdateTransforms(
    struct mmVKShadowCascade*                      p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip);

void
mmVKShadowCascade_UpdateUBODataLights(
    struct mmVKShadowCascade*                      p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews);

struct mmVKShadowOmniFrustum
{
    VkFramebuffer framebuffer;
    VkImageView imageView;

    float ProjectionView[4][4];
    float Projection[4][4];
    float View[4][4];
};

struct mmVKShadowOmniLight
{
    // omni frustum.
    struct mmVKShadowOmniFrustum hFrustum[6];

    // matrix index.
    int hIdxMat;

    // shadow map index.
    int hIdxMap;
};

struct mmVKShadowOmni
{
    // vector<struct mmVKShadowOmniLight> lights.
    struct mmVectorValue hLights;

    // depth mapping.
    struct mmVKFrame hShadowMap;

    // framebuffer number.
    uint32_t hFrameNumber;
    
    // shadow map size, default is 2048x2048.
    uint32_t hShadowMapSize[2];

    // depth format.
    VkFormat hDepthFormat;

    // RenderPassInfo.
    struct mmVKPassInfo* pPassInfo;

    // SamplerInfo.
    struct mmVKSamplerInfo* pSamplerInfo;
};

void
mmVKShadowOmni_Init(
    struct mmVKShadowOmni*                         p);

void
mmVKShadowOmni_Destroy(
    struct mmVKShadowOmni*                         p);

VkResult
mmVKShadowOmni_Prepare(
    struct mmVKShadowOmni*                         p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights);

void
mmVKShadowOmni_Discard(
    struct mmVKShadowOmni*                         p,
    struct mmVKAssets*                             pAssets);

VkResult
mmVKShadowOmni_OnRecordCommand(
    struct mmVKShadowOmni*                         p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene);

void
mmVKShadowOmni_UpdateTransforms(
    struct mmVKShadowOmni*                         p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip);

void
mmVKShadowOmni_UpdateUBODataLights(
    struct mmVKShadowOmni*                         p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews);

struct mmVKShadowSpotFrustum
{
    VkFramebuffer framebuffer;
    VkImageView imageView;

    float ProjectionView[4][4];
    float Projection[4][4];
    float View[4][4];
};

struct mmVKShadowSpotLight
{
    // spot frustum.
    struct mmVKShadowSpotFrustum hFrustum;

    // matrix index.
    int hIdxMat;

    // shadow map index.
    int hIdxMap;
};

struct mmVKShadowSpot
{
    // vector<struct mmVKShadowSpotLight> lights.
    struct mmVectorValue hLights;

    // depth mapping.
    struct mmVKFrame hShadowMap;

    // framebuffer number.
    uint32_t hFrameNumber;
    
    // shadow map size, default is 2048x2048.
    uint32_t hShadowMapSize[2];

    // depth format.
    VkFormat hDepthFormat;

    // RenderPassInfo.
    struct mmVKPassInfo* pPassInfo;

    // SamplerInfo.
    struct mmVKSamplerInfo* pSamplerInfo;
};

void
mmVKShadowSpot_Init(
    struct mmVKShadowSpot*                         p);

void
mmVKShadowSpot_Destroy(
    struct mmVKShadowSpot*                         p);

VkResult
mmVKShadowSpot_Prepare(
    struct mmVKShadowSpot*                         p,
    struct mmVKAssets*                             pAssets,
    VkFormat                                       hDepthFormat,
    const struct mmVectorVpt*                      pLights);

void
mmVKShadowSpot_Discard(
    struct mmVKShadowSpot*                         p,
    struct mmVKAssets*                             pAssets);

VkResult
mmVKShadowSpot_OnRecordCommand(
    struct mmVKShadowSpot*                         p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene);

void
mmVKShadowSpot_UpdateTransforms(
    struct mmVKShadowSpot*                         p,
    const struct mmVKUBOLight*                     pUBOLight,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip);

void
mmVKShadowSpot_UpdateUBODataLights(
    struct mmVKShadowSpot*                         p,
    struct mmVKUBOLight*                           pUBOLight,
    int                                            idxMat,
    struct mmVectorValue*                          uboProjectionViews);

struct mmVKShadow
{
    struct mmVKShadowCascade hCascade;
    struct mmVKShadowOmni hOmni;
    struct mmVKShadowSpot hSpot;

    // vector<float> uboSplits.
    struct mmVectorValue uboSplits;
    
    // vector<mmMat4x4> uboProjectionViews.
    struct mmVectorValue uboProjectionViews;

    // buffer uboProjectionViews.
    struct mmVKBuffer bufferUBOSplits;
    
    // buffer uboProjectionViews.
    struct mmVKBuffer bufferUBOProjectionViews;

    // framebuffer number.
    uint32_t hFrameNumber;
};

void
mmVKShadow_Init(
    struct mmVKShadow*                             p);

void
mmVKShadow_Destroy(
    struct mmVKShadow*                             p);

VkResult
mmVKShadow_Prepare(
    struct mmVKShadow*                             p,
    struct mmVKAssets*                             pAssets,
    const struct mmVectorVpt                       lights[3],
    VkFormat                                       hDepthFormat);

void
mmVKShadow_Discard(
    struct mmVKShadow*                             p,
    struct mmVKAssets*                             pAssets);

void
mmVKShadow_UpdateUBODataTransforms(
    struct mmVKShadow*                             p);

VkResult
mmVKShadow_UploadUBOBuffer(
    struct mmVKShadow*                             p,
    struct mmVKUploader*                           pUploader);

VkResult
mmVKShadow_OnRecordCommand(
    struct mmVKShadow*                             p,
    VkCommandBuffer                                cmdBuffer,
    struct mmVKTFObject*                           pObject,
    struct mmVKScene*                              pScene);

void
mmVKShadow_UpdateTransforms(
    struct mmVKShadow*                             p,
    struct mmVectorValue*                          uboSceneLights,
    const float                                    hProjectionView[4][4],
    float                                          nearClip,
    float                                          farClip);

void
mmVKShadow_UpdateUBODataLights(
    struct mmVKShadow*                             p,
    struct mmVectorValue*                          uboSceneLights,
    struct mmVectorValue*                          uboProjectionViews);

#include "core/mmSuffix.h"

#endif//__mmVKShadow_h__
