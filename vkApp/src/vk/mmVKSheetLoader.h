/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKSheetLoader_h__
#define __mmVKSheetLoader_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"
#include "container/mmVectorVpt.h"

#include "parse/mmParseTSV.h"

#include "dish/mmFileContext.h"

#include "core/mmPrefix.h"

struct mmVKSheetImage;

struct mmVKSheetFrame
{
    int id;                       /* id                      */
    int frame[4];                 /* (x, y, w, h)            */
    int source[4];                /* (x, y, w, h)            */
    int offset[2];                /* (x, y)                  */
    int original[2];              /* (w, h)                  */
    int r;                        /* rotated clockwise       */
    int t;                        /* trimmed                 */
    float anchor[2];              /* (x, y)                  */
    int borders[4];               /* (l, b, r, t)            */
    const char* fn;               /* frame name, read only.  */
    struct mmVKSheetImage* image; /* sheet image, read only. */
    size_t reference;             /* reference count.        */
};

void 
mmVKSheetFrame_Init(
    struct mmVKSheetFrame*                         p);

void 
mmVKSheetFrame_Destroy(
    struct mmVKSheetFrame*                         p);

void 
mmVKSheetFrame_AnalysisRow(
    struct mmVKSheetFrame*                         p, 
    const char**                                   row);

void 
mmVKSheetFrame_SetSheetImage(
    struct mmVKSheetFrame*                         p, 
    struct mmVKSheetImage*                         image);

int
mmVKSheetFrame_Invalid(
    const struct mmVKSheetFrame*                   p);

void
mmVKSheetFrame_Increase(
    struct mmVKSheetFrame*                         p);

void
mmVKSheetFrame_Decrease(
    struct mmVKSheetFrame*                         p);

void
mmVKSheetGetNameByPath(
    const char*                                    path,
    struct mmString*                               name);

struct mmVKSheetImage
{
    // name for image set info, read only. 
    // identify name, sheetpath = "imagesets/MyImage.sheet" identify = "MyImage"
    // It is use for combine "SheetImage" and "SheetFrame".
    // string is strong reference value.
    struct mmString name;

    // sheet path., read only.
    // string is weak reference value.
    struct mmString sheetpath;

    // image prefix directory path.
    // path/imagesets
    struct mmString path;

    // image basename.
    // MyImage.png
    struct mmString image;

    // image size. [w, h]
    size_t imageSize[2];

    // sheet size. [c, r]
    size_t sheetSize[2];

    // image frames.
    struct mmRbtreeStringVpt frames;

    // id weak index.
    struct mmVectorVpt vec;

    // reference count. 
    size_t reference;
};

void 
mmVKSheetImage_Init(
    struct mmVKSheetImage*                         p);

void 
mmVKSheetImage_Destroy(
    struct mmVKSheetImage*                         p);

void 
mmVKSheetImage_SetSheetPath(
    struct mmVKSheetImage*                         p,
    const char*                                    pSheetPath);

void 
mmVKSheetImage_GetImagePath(
    struct mmVKSheetImage*                         p,
    struct mmString*                               pImagePath);

void
mmVKSheetImage_GenerateIdentifyName(
    struct mmVKSheetImage*                         p);

struct mmVKSheetFrame* 
mmVKSheetImage_AddFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn);

struct mmVKSheetFrame* 
mmVKSheetImage_GetFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn);

void 
mmVKSheetImage_RmvFrame(
    struct mmVKSheetImage*                         p,
    const char*                                    fn);

void 
mmVKSheetImage_FrameClear(
    struct mmVKSheetImage*                         p);

void 
mmVKSheetImage_AllocatorMemory(
    struct mmVKSheetImage*                         p,
    size_t                                         size);

void 
mmVKSheetImage_SetFrameById(
    struct mmVKSheetImage*                         p,
    int                                            id, 
    struct mmVKSheetFrame*                         e);

struct mmVKSheetFrame* 
mmVKSheetImage_GetFrameById(
    struct mmVKSheetImage*                         p,
    int                                            id);

void 
mmVKSheetImage_FrameIdReset(
    struct mmVKSheetImage*                         p);

int
mmVKSheetImage_Invalid(
    const struct mmVKSheetImage*                   p);

void
mmVKSheetImage_Increase(
    struct mmVKSheetImage*                         p);

void
mmVKSheetImage_Decrease(
    struct mmVKSheetImage*                         p);

struct mmVKSheetParse
{
    // tsv parse.
    struct mmParseTSV tsv;

    // split position.
    size_t split;

    // bytes buffer, weak reference.
    struct mmByteBuffer* bytes;

    // current sheet image, weak reference.
    struct mmVKSheetImage* image;
};

void 
mmVKSheetParse_Init(
    struct mmVKSheetParse*                         p);

void 
mmVKSheetParse_Destroy(
    struct mmVKSheetParse*                         p);

void 
mmVKSheetParse_Reset(
    struct mmVKSheetParse*                         p);

void 
mmVKSheetParse_SetByteBuffer(
    struct mmVKSheetParse*                         p,
    struct mmByteBuffer*                           bytes);

void 
mmVKSheetParse_SetSheetImage(
    struct mmVKSheetParse*                         p,
    struct mmVKSheetImage*                         image);

void 
mmVKSheetParse_AnalysisKeyValuePairs(
    struct mmVKSheetParse*                         p,
    mmUInt8_t*                                     w);

void 
mmVKSheetParse_AnalysisLine(
    struct mmVKSheetParse*                         p,
    mmUInt8_t*                                     w, 
    size_t                                         z, 
    size_t                                         x, 
    size_t                                         y, 
    size_t                                         i);

void 
mmVKSheetParse_AnalysisMetadata(
    struct mmVKSheetParse*                         p);

void
mmVKSheetParse_AnalysisTSV(
    struct mmVKSheetParse*                         p);

void 
mmVKSheetParse_Analysis(
    struct mmVKSheetParse*                         p);

struct mmVKSheetLoader
{
    struct mmFileContext* pFileContext;
    struct mmRbtreeStringVpt sets;
    struct mmVKSheetParse parse;
};

void 
mmVKSheetLoader_Init(
    struct mmVKSheetLoader*                        p);

void 
mmVKSheetLoader_Destroy(
    struct mmVKSheetLoader*                        p);

void 
mmVKSheetLoader_SetFileContext(
    struct mmVKSheetLoader*                        p,
    struct mmFileContext*                          pFileContext);

struct mmVKSheetImage*
mmVKSheetLoader_LoadFromFile(
    struct mmVKSheetLoader*                        p,
    const char*                                    path);

void 
mmVKSheetLoader_UnloadByFile(
    struct mmVKSheetLoader*                        p,
    const char*                                    path);

void 
mmVKSheetLoader_UnloadComplete(
    struct mmVKSheetLoader*                        p);

#include "core/mmSuffix.h"

#endif//__mmVKSheetLoader_h__
