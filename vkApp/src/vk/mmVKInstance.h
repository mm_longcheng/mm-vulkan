/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKInstance_h__
#define __mmVKInstance_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "vk/mmVKPlatform.h"

#include "core/mmPrefix.h"

struct mmVKInstanceCreateInfo
{
    const char* appName;
    VkBool32 validation;
    uint32_t vApiVersion;
    uint32_t vAppVersion;
};

struct mmVKInstance
{
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugger;
    int validation;
    
    PFN_vkGetDeviceProcAddr                         vkGetDeviceProcAddr;
    PFN_vkGetPhysicalDeviceSurfaceSupportKHR        vkGetPhysicalDeviceSurfaceSupportKHR;
    PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR   vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
    PFN_vkGetPhysicalDeviceSurfaceFormatsKHR        vkGetPhysicalDeviceSurfaceFormatsKHR;
    PFN_vkGetPhysicalDeviceSurfacePresentModesKHR   vkGetPhysicalDeviceSurfacePresentModesKHR;

    PFN_vkCreateDebugUtilsMessengerEXT              vkCreateDebugUtilsMessengerEXT;
    PFN_vkDestroyDebugUtilsMessengerEXT             vkDestroyDebugUtilsMessengerEXT;
    PFN_vkSubmitDebugUtilsMessageEXT                vkSubmitDebugUtilsMessageEXT;
    PFN_vkCmdBeginDebugUtilsLabelEXT                vkCmdBeginDebugUtilsLabelEXT;
    PFN_vkCmdEndDebugUtilsLabelEXT                  vkCmdEndDebugUtilsLabelEXT;
    PFN_vkCmdInsertDebugUtilsLabelEXT               vkCmdInsertDebugUtilsLabelEXT;
    PFN_vkSetDebugUtilsObjectNameEXT                vkSetDebugUtilsObjectNameEXT;
};

void
mmVKInstance_Reset(
    struct mmVKInstance*                           p);

VkResult
mmVKInstance_Create(
    struct mmVKInstance*                           p,
    const struct mmVKInstanceCreateInfo*           info,
    const VkAllocationCallbacks*                   pAllocator);

void
mmVKInstance_Delete(
    struct mmVKInstance*                           p,
    const VkAllocationCallbacks*                   pAllocator);

#include "core/mmSuffix.h"

#endif//__mmmmVKInstance_h__
