/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKManager.h"

#include "vk/mmVKImageUploader.h"
#include "vk/mmVKCommandBuffer.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmSurfaceMaster.h"

#ifdef _DEBUG
#define MM_VK_VALIDATION MM_TRUE
#else
#define MM_VK_VALIDATION MM_FALSE
#endif//_DEBUG

void
mmVKManager_Init(
    struct mmVKManager*                            p)
{
    mmVKRenderer_Init(&p->vRenderer);
    mmVKSurface_Init(&p->vSurface);
    mmVKSwapchain_Init(&p->vSwapchain);
    mmVKAssets_Init(&p->vAssets);
    mmVKPipelineTarget_Init(&p->vPipelineTarget);

    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;
}

void
mmVKManager_Destroy(
    struct mmVKManager*                            p)
{
    p->pSurfaceMaster = NULL;
    p->pContextMaster = NULL;
    
    mmVKPipelineTarget_Destroy(&p->vPipelineTarget);
    mmVKAssets_Destroy(&p->vAssets);
    mmVKSwapchain_Destroy(&p->vSwapchain);
    mmVKSurface_Destroy(&p->vSurface);
    mmVKRenderer_Destroy(&p->vRenderer);
}

void
mmVKManager_SetContextMaster(
    struct mmVKManager*                            p,
    struct mmContextMaster*                        pContextMaster)
{
    p->pContextMaster = pContextMaster;
}

void
mmVKManager_SetSurfaceMaster(
    struct mmVKManager* p,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    p->pSurfaceMaster = pSurfaceMaster;
}

void
mmVKManager_Prepare(
    struct mmVKManager*                            p)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    struct mmVKPassLoader* pPassLoader = &p->vAssets.vPassLoader;

    struct mmVKPassInfo* pPassInfo;

    struct mmVKUploader*   pUploader;
    struct mmVKDevice*       pDevice;
    struct mmVKPhysicalDevice* pPhysicalDevice;
    struct mmVKSwapchain* pSwapchain;
    VkSurfaceKHR surface;

    struct mmVKRendererCreateInfo hRendererInfo;
    struct mmVKPipelineTargetCreateInfo hPipelineTargetInfo;
    uint32_t windowSize[2];
    ktx_transcode_fmt_e transcodefmt;
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_4_BIT;
    double hDisplayDensity = pSurfaceMaster->hDisplayDensity;
    int validation = MM_VK_VALIDATION;

    mmVKImageUploader_Prepare();

    hRendererInfo.appName = "vkApp";
    hRendererInfo.validation = validation;
    hRendererInfo.vApiVersion = VK_MAKE_VERSION(1, 0, 0);
    hRendererInfo.vAppVersion = VK_MAKE_VERSION(1, 0, 0);
    hRendererInfo.index = -1;
    hRendererInfo.viewSurface = pSurfaceMaster->pViewSurface;
    mmVKRenderer_Create(&p->vRenderer, &hRendererInfo, &p->vSurface, NULL);

    pUploader = &p->vRenderer.vUploader;
    pDevice = &p->vRenderer.vDevice;
    pPhysicalDevice = &p->vRenderer.vPhysicalDevice;
    pSwapchain = &p->vSwapchain;
    surface = p->vSurface.surface;

    samples = mmVKPhysicalDevice_SuitableSamples(pPhysicalDevice, samples, hDisplayDensity);

    windowSize[0] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[0];
    windowSize[1] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[1];
    mmVKRenderer_PrepareSwapchain(&p->vRenderer, surface, windowSize, samples, pSwapchain);

    transcodefmt = pPhysicalDevice->transcodefmt;
    mmVKAssets_SetPackageAssets(&p->vAssets, pPackageAssets);
    mmVKAssets_SetFileContext(&p->vAssets, pFileContext);
    mmVKAssets_SetUploader(&p->vAssets, pUploader);
    mmVKAssets_SetDevice(&p->vAssets, pDevice);
    mmVKAssets_SetPipelineCacheName(&p->vAssets, "VkPipelineCache.binary");
    mmVKAssets_SetTranscodeFmt(&p->vAssets, transcodefmt);
    mmVKAssets_SetSamples(&p->vAssets, samples);
    mmVKAssets_Prepare(&p->vAssets);
    mmVKAssets_PrepareDefaultPass(&p->vAssets, pSwapchain);
    mmVKAssets_PrepareDefaultPipeline(&p->vAssets);

    pPassInfo = mmVKPassLoader_GetFromName(pPassLoader, mmVKRenderPassMasterName);

    hPipelineTargetInfo.pSwapchain = &p->vSwapchain;
    hPipelineTargetInfo.commandPool = p->vRenderer.graphicsCmdPool;
    hPipelineTargetInfo.pPassInfo = pPassInfo;
    mmVKPipelineTarget_Create(&p->vPipelineTarget, pDevice, &hPipelineTargetInfo);
}

void
mmVKManager_Discard(
    struct mmVKManager*                            p)
{
    struct mmVKSwapchain* pSwapchain = &p->vSwapchain;

    mmVKPipelineTarget_Delete(&p->vPipelineTarget);

    mmVKAssets_DiscardDefaultPipeline(&p->vAssets);
    mmVKAssets_DiscardDefaultPass(&p->vAssets);
    mmVKAssets_Discard(&p->vAssets);

    mmVKRenderer_DiscardSwapchain(&p->vRenderer, pSwapchain);

    mmVKRenderer_Delete(&p->vRenderer, &p->vSurface);

    mmVKImageUploader_Discard();
}

void
mmVKManager_OnSurfacePrepare(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    struct mmVKSwapchain* pSwapchain = &p->vSwapchain;
    struct mmVKPhysicalDevice* pPhysicalDevice = &p->vRenderer.vPhysicalDevice;

    double hDisplayDensity;
    uint32_t windowSize[2];
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_4_BIT;

    windowSize[0] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[0];
    windowSize[1] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[1];

    hDisplayDensity = pSurfaceMaster->hDisplayDensity;

    samples = mmVKPhysicalDevice_SuitableSamples(pPhysicalDevice, samples, hDisplayDensity);

    mmVKRenderer_PrepareSurface(&p->vRenderer, pSurfaceMaster->pViewSurface, &p->vSurface);

    mmVKRenderer_PrepareSwapchain(&p->vRenderer, p->vSurface.surface, windowSize, samples, pSwapchain);

    mmVKPipelineTarget_PrepareSurfaceFrame(&p->vPipelineTarget);
}

void
mmVKManager_OnSurfaceDiscard(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content)
{
    struct mmVKSwapchain* pSwapchain = &p->vSwapchain;

    mmVKPipelineTarget_DiscardSurfaceFrame(&p->vPipelineTarget);

    mmVKRenderer_DiscardSwapchain(&p->vRenderer, pSwapchain);

    mmVKRenderer_DiscardSurface(&p->vRenderer, &p->vSurface);
}

void
mmVKManager_OnSurfaceChanged(
    struct mmVKManager*                            p,
    struct mmEventMetrics*                         content)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    struct mmVKSwapchain* pSwapchain = &p->vSwapchain;

    uint32_t windowSize[2];

    windowSize[0] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[0];
    windowSize[1] = (uint32_t)pSurfaceMaster->hDisplayFrameSize[1];

    mmVKPipelineTarget_DiscardSurfaceFrame(&p->vPipelineTarget);

    // Rebuild is better than Discard Prepare.
    mmVKRenderer_RebuildSwapchain(&p->vRenderer, p->vSurface.surface, windowSize, pSwapchain);

    mmVKPipelineTarget_PrepareSurfaceFrame(&p->vPipelineTarget);
}

void
mmVKManager_DeviceWaitIdle(
    struct mmVKManager*                            p)
{
    mmVKRenderer_DeviceWaitIdle(&p->vRenderer);
}

void
mmVKManager_AcquireNextFrame(
    struct mmVKManager*                            p,
    struct mmVKCommandBuffer*                      pCommandBuffer,
    uint32_t                                       hWindowRect[4])
{
    struct mmVKSwapchain* pSwapchain = &p->vSwapchain;
    struct mmVKPipelineTarget* pPipelineTarget = &p->vPipelineTarget;
    uint32_t* pWindowSize = pSwapchain->windowSize;

    hWindowRect[0] = 0;
    hWindowRect[1] = 0;
    hWindowRect[2] = pWindowSize[0];
    hWindowRect[3] = pWindowSize[1];

    mmVKCommandBuffer_SetWindowSize(pCommandBuffer, pWindowSize);

    /* acquire next image */
    mmVKPipelineTarget_AcquireNextImage(pPipelineTarget);
    mmVKPipelineTarget_CurrentCommandBuffer(pPipelineTarget, &pCommandBuffer->cmdBuffer);
}

VkResult
mmVKManager_BeginCommandBuffer(
    struct mmVKManager*                            p)
{
    return mmVKPipelineTarget_BeginCommandBuffer(&p->vPipelineTarget);
}

VkResult
mmVKManager_BeginRenderPass(
    struct mmVKManager*                            p,
    uint32_t                                       hWindowRect[4],
    float                                          hClearColor[4])
{
    return mmVKPipelineTarget_BeginRenderPass(&p->vPipelineTarget, hWindowRect, hClearColor);
}

VkResult
mmVKManager_EndRenderPass(
    struct mmVKManager*                            p)
{
    return mmVKPipelineTarget_EndRenderPass(&p->vPipelineTarget);
}

VkResult
mmVKManager_EndCommandBuffer(
    struct mmVKManager*                            p)
{
    return mmVKPipelineTarget_EndCommandBuffer(&p->vPipelineTarget);
}

VkResult
mmVKManager_QueueSubmit(
    struct mmVKManager*                            p)
{
    return mmVKPipelineTarget_QueueSubmit(&p->vPipelineTarget);
}

VkResult
mmVKManager_QueuePresent(
    struct mmVKManager*                            p)
{
    return mmVKPipelineTarget_QueuePresent(&p->vPipelineTarget);
}
