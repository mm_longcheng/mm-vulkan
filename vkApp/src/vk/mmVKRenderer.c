/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKRenderer.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

void
mmVKRenderer_Init(
    struct mmVKRenderer*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKRenderer));
}

void
mmVKRenderer_Destroy(
    struct mmVKRenderer*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKRenderer));
}

void
mmVKRenderer_Reset(
    struct mmVKRenderer*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKRenderer));
}

void
mmVKRenderer_SetAllocator(
    struct mmVKRenderer*                           p,
    const VkAllocationCallbacks*                   pAllocator)
{
    p->pAllocator = pAllocator;
}

VkResult
mmVKRenderer_PrepareInstance(
    struct mmVKRenderer*                           p,
    struct mmVKInstanceCreateInfo*                 info)
{
    p->vApiVersion = info->vApiVersion;
    p->vAppVersion = info->vAppVersion;
    return mmVKInstance_Create(&p->vInstance, info, p->pAllocator);
}

VkResult
mmVKRenderer_PreparePhysicalDevice(
    struct mmVKRenderer*                           p,
    uint32_t                                       index)
{
    VkInstance instance = p->vInstance.instance;
    return mmVKPhysicalDevice_Create(&p->vPhysicalDevice, instance, index);
}

VkResult
mmVKRenderer_PrepareDevice(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface)
{
    struct mmVKDeviceCreateInfo vDeviceInfo;
    vDeviceInfo.pInstance = &p->vInstance;
    vDeviceInfo.pPhysicalDevice = &p->vPhysicalDevice;
    vDeviceInfo.surface = surface;
    return mmVKDevice_Create(&p->vDevice, &vDeviceInfo, p->pAllocator);
}

VkResult
mmVKRenderer_PrepareVmaAllocator(
    struct mmVKRenderer*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkInstance instance = VK_NULL_HANDLE;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkDevice device = VK_NULL_HANDLE;
        
        struct VmaAllocatorCreateInfo allocatorInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        instance = p->vInstance.instance;
        if (VK_NULL_HANDLE == instance)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " instance is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        physicalDevice = p->vPhysicalDevice.physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        device = p->vDevice.device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        mmMemset(&allocatorInfo, 0, sizeof(struct VmaAllocatorCreateInfo));
        allocatorInfo.vulkanApiVersion = p->vApiVersion;
        allocatorInfo.physicalDevice = p->vPhysicalDevice.physicalDevice;
        allocatorInfo.device = p->vDevice.device;
        allocatorInfo.instance = p->vInstance.instance;
        err = vmaCreateAllocator(&allocatorInfo, &p->vmaAllocator);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaCreateAllocator failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = VK_SUCCESS;
    } while(0);
    
    return err;
}

VkResult
mmVKRenderer_PrepareCommandPool(
    struct mmVKRenderer*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkCommandPoolCreateInfo hCmdPoolInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        device = p->vDevice.device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        hCmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        hCmdPoolInfo.pNext = NULL;
        hCmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        hCmdPoolInfo.queueFamilyIndex = 0;
        
        hCmdPoolInfo.queueFamilyIndex = p->vDevice.graphicsIndex;
        err = vkCreateCommandPool(device, &hCmdPoolInfo, p->pAllocator, &p->graphicsCmdPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateCommandPool failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        hCmdPoolInfo.queueFamilyIndex = p->vDevice.transferIndex;
        err = vkCreateCommandPool(device, &hCmdPoolInfo, p->pAllocator, &p->transferCmdPool);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateCommandPool failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = VK_SUCCESS;
    } while(0);
    
    return err;
}

VkResult
mmVKRenderer_PrepareUploader(
    struct mmVKRenderer*                           p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkDevice device = VK_NULL_HANDLE;
        VkQueue graphicsQueue = VK_NULL_HANDLE;
        VkQueue transferQueue = VK_NULL_HANDLE;
        
        struct mmVKUploaderCreateInfo hUploaderInfo;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (VK_NULL_HANDLE == p->transferCmdPool)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " transferCmdPool is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        if (NULL == p->vmaAllocator)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vmaAllocator is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        physicalDevice = p->vPhysicalDevice.physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        device = p->vDevice.device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        graphicsQueue = p->vDevice.graphicsQueue;
        if (VK_NULL_HANDLE == graphicsQueue)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " graphicsQueue is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        transferQueue = p->vDevice.transferQueue;
        if (VK_NULL_HANDLE == transferQueue)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " transferQueue is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        // Uploader vkCmdBlitImage need VK_QUEUE_GRAPHICS_BIT.
        hUploaderInfo.vmaAllocator = p->vmaAllocator;
        hUploaderInfo.physicalDevice = physicalDevice;
        hUploaderInfo.device = device;
        hUploaderInfo.graphicsQueue = graphicsQueue;
        hUploaderInfo.transferQueue = transferQueue;
        hUploaderInfo.graphicsCmdPool = p->graphicsCmdPool;
        hUploaderInfo.transferCmdPool = p->transferCmdPool;
        err = mmVKUploader_Create(&p->vUploader, &hUploaderInfo, p->pAllocator);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKUploader_Create failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        err = VK_SUCCESS;
    } while(0);

    return err;
}

void
mmVKRenderer_DiscardInstance(
    struct mmVKRenderer*                           p)
{
    mmVKInstance_Delete(&p->vInstance, p->pAllocator);
}

void
mmVKRenderer_DiscardPhysicalDevice(
    struct mmVKRenderer*                           p)
{
    mmVKPhysicalDevice_Delete(&p->vPhysicalDevice);
}

void
mmVKRenderer_DiscardDevice(
    struct mmVKRenderer*                           p)
{
    mmVKDevice_Delete(&p->vDevice);
}

void
mmVKRenderer_DiscardVmaAllocator(
    struct mmVKRenderer*                           p)
{
    if (VK_NULL_HANDLE != p->vmaAllocator)
    {
        vmaDestroyAllocator(p->vmaAllocator);
        p->vmaAllocator = VK_NULL_HANDLE;
    }
}

void
mmVKRenderer_DiscardCommandPool(
    struct mmVKRenderer*                           p)
{
    VkDevice device = p->vDevice.device;
    
    if (VK_NULL_HANDLE != device)
    {
        if (VK_NULL_HANDLE != p->graphicsCmdPool)
        {
            vkDestroyCommandPool(device, p->graphicsCmdPool, p->pAllocator);
            p->graphicsCmdPool = VK_NULL_HANDLE;
        }

        if (VK_NULL_HANDLE != p->transferCmdPool)
        {
            vkDestroyCommandPool(device, p->transferCmdPool, p->pAllocator);
            p->transferCmdPool = VK_NULL_HANDLE;
        }
    }
}

void
mmVKRenderer_DiscardUploader(
    struct mmVKRenderer*                           p)
{
    mmVKUploader_Delete(&p->vUploader);
}

VkResult
mmVKRenderer_Create(
    struct mmVKRenderer*                           p,
    const struct mmVKRendererCreateInfo*           info,
    struct mmVKSurface*                            pSurface,
    const VkAllocationCallbacks*                   pAllocator)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmVKInstanceCreateInfo hVKInstanceInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        hVKInstanceInfo.appName = info->appName;
        hVKInstanceInfo.validation = info->validation;
        hVKInstanceInfo.vApiVersion = info->vApiVersion;
        hVKInstanceInfo.vAppVersion = info->vApiVersion;

        mmVKRenderer_SetAllocator(p, pAllocator);

        err = mmVKRenderer_PrepareInstance(p, &hVKInstanceInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareInstance failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PreparePhysicalDevice(p, info->index);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PreparePhysicalDevice failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PrepareSurface(p, info->viewSurface, pSurface);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareSurface failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PrepareDevice(p, pSurface->surface);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareDevice failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PrepareVmaAllocator(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareVmaAllocator failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PrepareCommandPool(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareCommandPool failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKRenderer_PrepareUploader(p);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKRenderer_PrepareUploader failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while(0);

    return err;
}

void
mmVKRenderer_Delete(
    struct mmVKRenderer*                           p,
    struct mmVKSurface*                            pSurface)
{
    mmVKRenderer_DiscardUploader(p);
    mmVKRenderer_DiscardCommandPool(p);
    mmVKRenderer_DiscardVmaAllocator(p);
    mmVKRenderer_DiscardDevice(p);
    mmVKRenderer_DiscardSurface(p, pSurface);
    mmVKRenderer_DiscardPhysicalDevice(p);
    mmVKRenderer_DiscardInstance(p);
}

VkResult
mmVKRenderer_PrepareSurface(
    struct mmVKRenderer*                           p,
    void*                                          viewSurface,
    struct mmVKSurface*                            pSurface)
{
    VkInstance instance = p->vInstance.instance;
    return mmVKSurface_Create(pSurface, instance, viewSurface, p->pAllocator);
}

void
mmVKRenderer_DiscardSurface(
    struct mmVKRenderer*                           p,
    struct mmVKSurface*                            pSurface)
{
    VkInstance instance = p->vInstance.instance;
    mmVKSurface_Delete(pSurface, instance, p->pAllocator);
}

VkResult
mmVKRenderer_PrepareSwapchain(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface,
    uint32_t                                       windowSize[2],
    VkSampleCountFlagBits                          samples,
    struct mmVKSwapchain*                          pSwapchain)
{
    struct mmVKSwapchainCreateInfo hVKSwapchainInfo;
    hVKSwapchainInfo.pInstance = &p->vInstance;
    hVKSwapchainInfo.pDevice = &p->vDevice;
    hVKSwapchainInfo.physicalDevice = p->vPhysicalDevice.physicalDevice;
    hVKSwapchainInfo.surface = surface;
    hVKSwapchainInfo.depthFormat = VK_FORMAT_D32_SFLOAT;
    hVKSwapchainInfo.surfaceFormat = VK_FORMAT_R8G8B8A8_UNORM;
    hVKSwapchainInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    hVKSwapchainInfo.samples = samples;
    hVKSwapchainInfo.windowSize[0] = windowSize[0];
    hVKSwapchainInfo.windowSize[1] = windowSize[1];
    return mmVKSwapchain_Create(pSwapchain, &hVKSwapchainInfo, &p->vUploader);
}

void
mmVKRenderer_DiscardSwapchain(
    struct mmVKRenderer*                           p,
    struct mmVKSwapchain*                          pSwapchain)
{
    mmVKSwapchain_Delete(pSwapchain, &p->vDevice, &p->vUploader);
}

VkResult
mmVKRenderer_RebuildSwapchain(
    struct mmVKRenderer*                           p,
    VkSurfaceKHR                                   surface,
    uint32_t                                       windowSize[2],
    struct mmVKSwapchain*                          pSwapchain)
{
    struct mmVKSwapchainCreateInfo hVKSwapchainInfo;
    hVKSwapchainInfo.pInstance = &p->vInstance;
    hVKSwapchainInfo.pDevice = &p->vDevice;
    hVKSwapchainInfo.physicalDevice = p->vPhysicalDevice.physicalDevice;
    hVKSwapchainInfo.surface = surface;
    hVKSwapchainInfo.depthFormat = VK_FORMAT_D32_SFLOAT;
    hVKSwapchainInfo.surfaceFormat = VK_FORMAT_R8G8B8A8_UNORM;
    hVKSwapchainInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    hVKSwapchainInfo.samples = pSwapchain->samples;
    hVKSwapchainInfo.windowSize[0] = windowSize[0];
    hVKSwapchainInfo.windowSize[1] = windowSize[1];
    return mmVKSwapchain_Rebuild(pSwapchain, &hVKSwapchainInfo, &p->vUploader);
}

void
mmVKRenderer_DeviceWaitIdle(
    struct mmVKRenderer*                           p)
{
    mmVKDevice_WaitIdle(&p->vDevice);
}