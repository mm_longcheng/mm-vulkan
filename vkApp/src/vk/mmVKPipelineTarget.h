/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKPipelineTarget_h__
#define __mmVKPipelineTarget_h__

#include "core/mmCore.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDevice.h"
#include "vk/mmVKSwapchain.h"

#include "core/mmPrefix.h"

struct mmVKPassInfo;

struct mmVKPipelineTargetCreateInfo
{
    struct mmVKSwapchain*                          pSwapchain;
    struct mmVKPassInfo*                           pPassInfo;
    VkCommandPool                                  commandPool;
};

struct mmVKPipelineFrame
{
    VkFramebuffer framebuffer;
    VkCommandBuffer cmdBuffer;
};

struct mmVKPipelineTarget
{
    struct mmVKPipelineFrame* frames;
    uint32_t frameCount;

    VkFence fences[2];
    VkSemaphore acquiredSemaphores[2];
    VkSemaphore completeSemaphores[2];

    uint32_t currentBuffer;
    uint32_t frameIndex;

    // weak reference.
    struct mmVKSwapchain* pSwapchain;
    struct mmVKDevice*    pDevice;
    struct mmVKPassInfo*  pPassInfo;
    VkCommandPool         commandPool;
};

void
mmVKPipelineTarget_Init(
    struct mmVKPipelineTarget*                     p);

void
mmVKPipelineTarget_Destroy(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_PrepareFrames(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       frameCount);

void
mmVKPipelineTarget_DiscardFrames(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_PrepareFrameCommands(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    VkCommandPool                                  commandPool);

void
mmVKPipelineTarget_DiscardFrameCommands(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    VkCommandPool                                  commandPool);

VkResult
mmVKPipelineTarget_PrepareFramebuffers(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    struct mmVKSwapchain*                          pSwapchain,
    VkRenderPass                                   renderPass);

void
mmVKPipelineTarget_DiscardFramebuffers(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKPipelineTarget_PrepareSemaphores(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice);

void
mmVKPipelineTarget_DiscardSemaphores(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKPipelineTarget_Create(
    struct mmVKPipelineTarget*                     p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKPipelineTargetCreateInfo*     info);

void
mmVKPipelineTarget_Delete(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_PrepareSurfaceFrame(
    struct mmVKPipelineTarget*                     p);

void
mmVKPipelineTarget_DiscardSurfaceFrame(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_AcquireNextImage(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_QueueSubmit(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_QueuePresent(
    struct mmVKPipelineTarget*                     p);

void
mmVKPipelineTarget_WaitIdle(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_CurrentCommandBuffer(
    struct mmVKPipelineTarget*                     p,
    VkCommandBuffer*                               cmdBuffer);

VkResult
mmVKPipelineTarget_BeginCommandBuffer(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_EndCommandBuffer(
    struct mmVKPipelineTarget*                     p);

VkResult
mmVKPipelineTarget_BeginRenderPass(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       windowRect[4],
    float                                          clearColor[4]);

VkResult
mmVKPipelineTarget_EndRenderPass(
    struct mmVKPipelineTarget*                     p);

// hViewport [minx, miny, maxx, maxy]
VkResult
mmVKPipelineTarget_SetViewport(
    struct mmVKPipelineTarget*                     p,
    float                                          hViewport[4], 
    float                                          hMinDepth,
    float                                          hMaxDepth);

// hScissor  [minx, miny, maxx, maxy]
VkResult
mmVKPipelineTarget_SetScissor(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       hScissor[4]);

VkResult
mmVKPipelineTarget_BeginFrame(
    struct mmVKPipelineTarget*                     p,
    uint32_t                                       windowSize[2],
    float                                          clearColor[4]);

VkResult
mmVKPipelineTarget_EndFrame(
    struct mmVKPipelineTarget*                     p);

#include "core/mmSuffix.h"

#endif//__mmVKPipelineTarget_h__
