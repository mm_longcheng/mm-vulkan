/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKInstance.h"

#include "vk/mmVKExtension.h"
#include "vk/mmVKVersion.h"
#include "vk/mmVKMacro.h"
#include "vk/mmVKDebugger.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmLoggerManager.h"

static
void
mmVKDebuggerMakeCreateInfo(
    struct mmVKInstance*                           p,
    VkDebugUtilsMessengerCreateInfoEXT*            pDebugInfo)
{
    if (MM_TRUE == p->validation)
    {
        VkDebugUtilsMessageSeverityFlagsEXT hMessageSeverity = 0;
        VkDebugUtilsMessageTypeFlagsEXT hMessageType = 0;

        hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
        hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
        hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
        hMessageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

        hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
        hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
        hMessageType |= VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

        // VK_EXT_debug_utils style
        pDebugInfo->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        pDebugInfo->pNext = NULL;
        pDebugInfo->flags = 0;
        pDebugInfo->messageSeverity = hMessageSeverity;
        pDebugInfo->messageType = hMessageType;
        pDebugInfo->pfnUserCallback = mmVKDebuggerMessengerCallback;
        pDebugInfo->pUserData = p;
    }
}

static
void
mmVKDebuggerCreateDebugUtils(
    struct mmVKInstance*                           p,
    int                                            hIsSupportDebugUtils,
    VkDebugUtilsMessengerCreateInfoEXT*            pDebugInfo,
    const VkAllocationCallbacks*                   pAllocator)
{
    if (MM_TRUE == p->validation && hIsSupportDebugUtils)
    {
        VkResult err = VK_ERROR_UNKNOWN;
        struct mmLogger* gLogger = mmLogger_Instance();

        // Setup VK_EXT_debug_utils function pointers always (we use them for debug labels and names).
        mmVKGetInstanceProcAddr(p, p->instance, CreateDebugUtilsMessengerEXT);
        mmVKGetInstanceProcAddr(p, p->instance, DestroyDebugUtilsMessengerEXT);
        mmVKGetInstanceProcAddr(p, p->instance, SubmitDebugUtilsMessageEXT);
        mmVKGetInstanceProcAddr(p, p->instance, CmdBeginDebugUtilsLabelEXT);
        mmVKGetInstanceProcAddr(p, p->instance, CmdEndDebugUtilsLabelEXT);
        mmVKGetInstanceProcAddr(p, p->instance, CmdInsertDebugUtilsLabelEXT);
        mmVKGetInstanceProcAddr(p, p->instance, SetDebugUtilsObjectNameEXT);

        err = p->vkCreateDebugUtilsMessengerEXT(p->instance, pDebugInfo, pAllocator, &p->debugger);
        switch (err)
        {
        case VK_ERROR_OUT_OF_HOST_MEMORY:
            mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT failure.");
            mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT out of host memory.");
            break;
        case VK_SUCCESS:
            mmLogger_LogV(gLogger, "vkCreateDebugUtilsMessengerEXT success.");
            break;
        default:
            mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT failure.");
            mmLogger_LogE(gLogger, "vkCreateDebugUtilsMessengerEXT unknown failure.");
            break;
        }
    }
}

static
void
mmVKDebuggerDeleteDebugUtils(
    struct mmVKInstance*                           p,
    const VkAllocationCallbacks*                   pAllocator)
{
    if (MM_TRUE == p->validation && VK_NULL_HANDLE != p->instance && VK_NULL_HANDLE != p->debugger)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        p->vkDestroyDebugUtilsMessengerEXT(p->instance, p->debugger, pAllocator);
        p->debugger = VK_NULL_HANDLE;
        mmLogger_LogV(gLogger, "vkDestroyDebugUtilsMessengerEXT success.");
    }
}

void
mmVKInstance_Reset(
    struct mmVKInstance*                           p)
{
    mmMemset(p, 0, sizeof(struct mmVKInstance));
}

VkResult
mmVKInstance_Create(
    struct mmVKInstance*                           p,
    const struct mmVKInstanceCreateInfo*           info,
    const VkAllocationCallbacks*                   pAllocator)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    struct mmVKExtension hInstanceLayerArray;
    struct mmVKExtension hInstanceExtensionArray;

    mmVKExtension_Init(&hInstanceLayerArray);
    mmVKExtension_Init(&hInstanceExtensionArray);

    mmVKExtension_AllocArraySize(&hInstanceLayerArray, 1);
    mmVKExtension_AllocArraySize(&hInstanceExtensionArray, 4);

    do
    {
        int hIsSupportDebugUtils;
        VkApplicationInfo hApplicationInfo;
        VkInstanceCreateInfo hInstanceInfo;
        VkDebugUtilsMessengerCreateInfoEXT hDebugInfo;

        struct mmLogger* gLogger = mmLogger_Instance();
        const char* pApplicationName = info->appName;

        // Assignment.
        p->validation = info->validation;

        mmVKExtensionMakeInstanceLayer(&hInstanceLayerArray);
        mmVKExtensionMakeInstanceExtension(&hInstanceExtensionArray);

        mmVKDebuggerMakeExtension(&hInstanceLayerArray, &hInstanceExtensionArray, p->validation);

        mmVKExtension_EnabledInstanceLayer(&hInstanceLayerArray);
        mmVKExtension_EnabledInstanceExtension(&hInstanceExtensionArray);

        if (!mmVKExtension_GetIsSuitable(&hInstanceLayerArray))
        {
            mmLogger_LogE(gLogger, "InstanceLayer extension is not complete suitable.");
            err = VK_ERROR_EXTENSION_NOT_PRESENT;
            break;
        }
        if (!mmVKExtension_GetIsSuitable(&hInstanceExtensionArray))
        {
            mmLogger_LogE(gLogger, "InstanceExtension extension is not complete suitable.");
            err = VK_ERROR_EXTENSION_NOT_PRESENT;
            break;
        }

        // check the DebugUtils support.
        hIsSupportDebugUtils = mmVKExtension_GetIsSupportName(&hInstanceExtensionArray, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

        mmMemset(&hDebugInfo, 0, sizeof(VkDebugUtilsMessengerCreateInfoEXT));
        mmVKDebuggerMakeCreateInfo(p, &hDebugInfo);

        hApplicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        hApplicationInfo.pNext = NULL;
        hApplicationInfo.pApplicationName = pApplicationName;
        hApplicationInfo.applicationVersion = info->vAppVersion;
        hApplicationInfo.pEngineName = mmVKEngineName;
        hApplicationInfo.engineVersion = VK_MAKE_VERSION(mmVKMajor, mmVKMinor, mmVKPatch);
        hApplicationInfo.apiVersion = info->vApiVersion;

        hInstanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        hInstanceInfo.pNext = (MM_TRUE == p->validation) ? &hDebugInfo : NULL;
        hInstanceInfo.flags = 0;
        hInstanceInfo.pApplicationInfo = &hApplicationInfo;
        hInstanceInfo.enabledLayerCount = hInstanceLayerArray.hEnabledCount;
        hInstanceInfo.ppEnabledLayerNames = (const char* const*)hInstanceLayerArray.pEnabledArray;
        hInstanceInfo.enabledExtensionCount = hInstanceExtensionArray.hEnabledCount;
        hInstanceInfo.ppEnabledExtensionNames = (const char* const*)hInstanceExtensionArray.pEnabledArray;

        err = vkCreateInstance(&hInstanceInfo, pAllocator, &p->instance);
        if (err == VK_ERROR_INCOMPATIBLE_DRIVER)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance failure.");
            mmLogger_LogE(gLogger, "Cannot find a compatible Vulkan installable client driver (ICD).");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }
        if (err == VK_ERROR_EXTENSION_NOT_PRESENT)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance failure.");
            mmLogger_LogE(gLogger, "Cannot find a specified extension library.");
            mmLogger_LogE(gLogger, "Make sure your layers path is set appropriately.");
            break;
        }
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "vkCreateInstance failure.");
            mmLogger_LogE(gLogger, "Do you have a compatible Vulkan installable client driver (ICD) installed?");
            mmLogger_LogE(gLogger, "Please look at the Getting Started guide for additional information.");
            break;
        }

        mmLogger_LogV(gLogger, "vkCreateInstance success.");

        mmVKGetInstanceProcAddr(p, p->instance, GetDeviceProcAddr);
        // Setup PhysicalDeviceSurface function pointers
        mmVKGetInstanceProcAddr(p, p->instance, GetPhysicalDeviceSurfaceSupportKHR);
        mmVKGetInstanceProcAddr(p, p->instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
        mmVKGetInstanceProcAddr(p, p->instance, GetPhysicalDeviceSurfaceFormatsKHR);
        mmVKGetInstanceProcAddr(p, p->instance, GetPhysicalDeviceSurfacePresentModesKHR);
        
        mmVKDebuggerCreateDebugUtils(p, hIsSupportDebugUtils, &hDebugInfo, pAllocator);
        
        err = VK_SUCCESS;
    } while (0);

    mmVKExtension_DeallocArray(&hInstanceLayerArray);
    mmVKExtension_DeallocArray(&hInstanceExtensionArray);

    mmVKExtension_Destroy(&hInstanceLayerArray);
    mmVKExtension_Destroy(&hInstanceExtensionArray);
    
    return err;
}

void
mmVKInstance_Delete(
    struct mmVKInstance*                           p,
    const VkAllocationCallbacks*                   pAllocator)
{
    if (VK_NULL_HANDLE != p->instance)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmVKDebuggerDeleteDebugUtils(p, pAllocator);
        vkDestroyInstance(p->instance, pAllocator);
        p->instance = VK_NULL_HANDLE;
        mmLogger_LogV(gLogger, "vkDestroyInstance success.");
    }
}


