/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVKDescriptorPool_h__
#define __mmVKDescriptorPool_h__

#include "core/mmCore.h"

#include "container/mmRbtsetU64.h"
#include "container/mmRbtreeU64.h"
#include "container/mmRbtsetVpt.h"

#include "vk/mmVKPlatform.h"
#include "vk/mmVKDescriptorLayout.h"
#include "vk/mmVKDescriptorSet.h"

#include "core/mmPrefix.h"

struct mmVKDevice;
struct mmVKLayoutInfo;

struct mmVKDescriptorPool;

struct mmVKDescriptorChunk
{
    // set<VkDescriptorSet> VkDescriptorSet idle.
    struct mmRbtsetU64 idle;

    // pool handle.
    VkDescriptorPool descriptorPool;

    // number record Allocate number.
    mmUInt32_t number;

    // index produce.
    mmUInt32_t hIndexProduce;

    // index recycle.
    mmUInt32_t hIndexRecycle;

    // pool pointer.
    struct mmVKDescriptorPool* pool;
};

void
mmVKDescriptorChunk_Init(
    struct mmVKDescriptorChunk*                    p);

void
mmVKDescriptorChunk_Destroy(
    struct mmVKDescriptorChunk*                    p);

void
mmVKDescriptorChunk_SetPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorPool*                     pool);

void
mmVKDescriptorChunk_ResetIndex(
    struct mmVKDescriptorChunk*                    p);

int 
mmVKDescriptorChunk_ProduceComplete(
    const struct mmVKDescriptorChunk*              p);

int 
mmVKDescriptorChunk_RecycleComplete(
    const struct mmVKDescriptorChunk*              p);

VkResult
mmVKDescriptorChunk_PrepareDescriptorPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDevice*                             pDevice,
    struct mmVectorValue*                          pSizes,
    uint32_t                                       hMaxSets);

void
mmVKDescriptorChunk_DiscardDescriptorPool(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDevice*                             pDevice);

VkResult
mmVKDescriptorChunk_DescriptorSetProduce(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorSet*                      pSet);

void
mmVKDescriptorChunk_DescriptorSetRecycle(
    struct mmVKDescriptorChunk*                    p,
    struct mmVKDescriptorSet*                      pSet);

struct mmVKDescriptorPool
{
    // vector<VkDescriptorPoolSize> pool size descriptor.
    struct mmVectorValue sizes;

    // <VkDescriptorPool, struct mmVKDescriptorChunk> pool map.
    // strong ref.
    struct mmRbtreeU64Vpt rbtree;

    // weak ref.
    struct mmRbtsetVpt used;

    // weak ref.
    struct mmRbtsetVpt idle;

    // weak ref.
    struct mmVKDescriptorChunk* current;

    // default is 0.75.
    float hRateTrimBorder;

    // default is 0.25.
    float hRateTrimNumber;

    // descriptor set layout sets info. 
    // reference value.
    struct mmVKLayoutInfo* pLayoutInfo;

    // max sets.
    uint32_t maxSets;

    // weak reference.
    struct mmVKDevice* pDevice;
};

void
mmVKDescriptorPool_Init(
    struct mmVKDescriptorPool*                     p);

void
mmVKDescriptorPool_Destroy(
    struct mmVKDescriptorPool*                     p);

void
mmVKDescriptorPool_UpdatePoolSize(
    struct mmVKDescriptorPool*                     p,
    const struct mmVectorValue*                    dslb,
    uint32_t                                       dslc);

VkResult
mmVKDescriptorPool_Create(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDevice*                             pDevice,
    struct mmVKLayoutInfo*                         info);

void
mmVKDescriptorPool_Delete(
    struct mmVKDescriptorPool*                     p);

size_t
mmVKDescriptorPool_Capacity(
    const struct mmVKDescriptorPool*               p);

struct mmVKDescriptorChunk*
mmVKDescriptorPool_AddChunk(
    struct mmVKDescriptorPool*                     p);

struct mmVKDescriptorChunk*
mmVKDescriptorPool_GetChunk(
    const struct mmVKDescriptorPool*               p,
    mmUInt64_t                                     page);

void
mmVKDescriptorPool_RmvChunk(
    struct mmVKDescriptorPool*                     p,
    mmUInt64_t                                     page);

void
mmVKDescriptorPool_DestroyChunk(
    struct mmVKDescriptorPool*                     p);

VkResult
mmVKDescriptorPool_DescriptorSetProduce(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDescriptorSet*                      pSet);

void
mmVKDescriptorPool_DescriptorSetRecycle(
    struct mmVKDescriptorPool*                     p,
    struct mmVKDescriptorSet*                      pSet);

#include "core/mmSuffix.h"

#endif//__mmVKDescriptorPool_h__
