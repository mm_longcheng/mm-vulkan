/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKRenderPass.h"

#include "vk/mmVKDevice.h"
#include "vk/mmVKSwapchain.h"

#include "core/mmLogger.h"

const char* mmVKRenderPassMasterName = "mmVKRenderPassMaster";
const char* mmVKRenderPassDepthName = "mmVKRenderPassDepth";

void
mmVKRenderPass_Init(
    struct mmVKRenderPass*                         p)
{
    p->renderPass = VK_NULL_HANDLE;
}

void
mmVKRenderPass_Destroy(
    struct mmVKRenderPass*                         p)
{
    p->renderPass = VK_NULL_HANDLE;
}

VkResult
mmVKRenderPass_CreateDepth(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    VkFormat                                       depthFormat)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        /*
            Depth map renderpass
        */
        VkAttachmentDescription hAttachment;
        VkAttachmentReference depthReference;
        VkSubpassDescription subpass;

        VkSubpassDependency attachmentDependencies[2];

        VkRenderPassCreateInfo hRenderPassInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        hAttachment.flags = 0;
        hAttachment.format = depthFormat;
        hAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        hAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        hAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        hAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

        depthReference.attachment = 0;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        subpass.flags = 0;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = NULL;
        subpass.colorAttachmentCount = 0;
        subpass.pColorAttachments = NULL;
        subpass.pResolveAttachments = NULL;
        subpass.pDepthStencilAttachment = &depthReference;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = NULL;

        attachmentDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[0].dstSubpass = 0;
        attachmentDependencies[0].srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        attachmentDependencies[0].dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        attachmentDependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        attachmentDependencies[0].dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        attachmentDependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        attachmentDependencies[1].srcSubpass = 0;
        attachmentDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[1].srcStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        attachmentDependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        attachmentDependencies[1].srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        attachmentDependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        attachmentDependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        hRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        hRenderPassInfo.pNext = NULL;
        hRenderPassInfo.flags = 0;
        hRenderPassInfo.attachmentCount = 1;
        hRenderPassInfo.pAttachments = &hAttachment;
        hRenderPassInfo.subpassCount = 1;
        hRenderPassInfo.pSubpasses = &subpass;
        hRenderPassInfo.dependencyCount = 2;
        hRenderPassInfo.pDependencies = attachmentDependencies;

        err = vkCreateRenderPass(device, &hRenderPassInfo, pAllocator, &p->renderPass);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateRenderPass failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKRenderPass_CreateBasicSampled(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkAttachmentDescription hAttachment[2];
        VkAttachmentReference colorReference;
        VkAttachmentReference depthReference;
        VkSubpassDescription subpass;

        VkSubpassDependency attachmentDependencies[2];
        VkPipelineStageFlags srcStageMask = 0;
        VkPipelineStageFlags dstStageMask = 0;
        VkAccessFlags srcAccessMask = 0;
        VkAccessFlags dstAccessMask = 0;

        VkRenderPassCreateInfo hRenderPassInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        // The initial layout for the color and depth attachments will be LAYOUT_UNDEFINED
        // because at the start of the renderpass, we don't care about their contents.
        // At the start of the subpass, the color attachment's layout will be transitioned
        // to LAYOUT_COLOR_ATTACHMENT_OPTIMAL and the depth stencil attachment's layout
        // will be transitioned to LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL.  At the end of
        // the renderpass, the color attachment's layout will be transitioned to
        // LAYOUT_PRESENT_SRC_KHR to be ready to present.  This is all done as part of
        // the renderpass, no barriers are necessary.

        hAttachment[0].flags = 0;
        hAttachment[0].format = pSwapchain->surfaceFormat.format;
        hAttachment[0].samples = VK_SAMPLE_COUNT_1_BIT;
        hAttachment[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        hAttachment[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        hAttachment[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        hAttachment[1].flags = 0;
        hAttachment[1].format = pSwapchain->depthFormat;
        hAttachment[1].samples = VK_SAMPLE_COUNT_1_BIT;
        hAttachment[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        hAttachment[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        colorReference.attachment = 0;
        colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        depthReference.attachment = 1;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        subpass.flags = 0;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = NULL;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorReference;
        subpass.pResolveAttachments = NULL;
        subpass.pDepthStencilAttachment = &depthReference;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = NULL;

        srcStageMask = 0;
        srcStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        srcStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        dstStageMask = 0;
        dstStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dstStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        srcAccessMask = 0;
        srcAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        dstAccessMask = 0;
        dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
        dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        // Depth buffer is shared between swapchain images
        attachmentDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[0].dstSubpass = 0;
        attachmentDependencies[0].srcStageMask = srcStageMask;
        attachmentDependencies[0].dstStageMask = dstStageMask;
        attachmentDependencies[0].srcAccessMask = srcAccessMask;
        attachmentDependencies[0].dstAccessMask = dstAccessMask;
        attachmentDependencies[0].dependencyFlags = 0;

        srcStageMask = 0;
        srcStageMask |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dstStageMask = 0;
        dstStageMask |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        srcAccessMask = 0;
        dstAccessMask = 0;
        dstAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dstAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;

        // Image Layout Transition
        attachmentDependencies[1].srcSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[1].dstSubpass = 0;
        attachmentDependencies[1].srcStageMask = srcStageMask;
        attachmentDependencies[1].dstStageMask = dstStageMask;
        attachmentDependencies[1].srcAccessMask = srcAccessMask;
        attachmentDependencies[1].dstAccessMask = dstAccessMask;
        attachmentDependencies[1].dependencyFlags = 0;

        hRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        hRenderPassInfo.pNext = NULL;
        hRenderPassInfo.flags = 0;
        hRenderPassInfo.attachmentCount = 2;
        hRenderPassInfo.pAttachments = hAttachment;
        hRenderPassInfo.subpassCount = 1;
        hRenderPassInfo.pSubpasses = &subpass;
        hRenderPassInfo.dependencyCount = 2;
        hRenderPassInfo.pDependencies = attachmentDependencies;

        err = vkCreateRenderPass(device, &hRenderPassInfo, pAllocator, &p->renderPass);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateRenderPass failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKRenderPass_CreateMultiSampled(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        VkAttachmentDescription hAttachment[3];
        VkAttachmentReference colorReference;
        VkAttachmentReference depthReference;
        VkAttachmentReference resolveReference;
        VkSubpassDescription subpass;

        VkSubpassDependency attachmentDependencies[2];
        VkPipelineStageFlags srcStageMask = 0;
        VkPipelineStageFlags dstStageMask = 0;
        VkAccessFlags srcAccessMask = 0;
        VkAccessFlags dstAccessMask = 0;

        VkRenderPassCreateInfo hRenderPassInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        if (NULL == pSwapchain)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pSwapchain is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        // The initial layout for the color and depth attachments will be LAYOUT_UNDEFINED
        // because at the start of the renderpass, we don't care about their contents.
        // At the start of the subpass, the color attachment's layout will be transitioned
        // to LAYOUT_COLOR_ATTACHMENT_OPTIMAL and the depth stencil attachment's layout
        // will be transitioned to LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL.  At the end of
        // the renderpass, the color attachment's layout will be transitioned to
        // LAYOUT_PRESENT_SRC_KHR to be ready to present.  This is all done as part of
        // the renderpass, no barriers are necessary.

        // Multisampled attachment that we render to
        hAttachment[0].flags = 0;
        hAttachment[0].format = pSwapchain->surfaceFormat.format;
        hAttachment[0].samples = pSwapchain->samples;
        hAttachment[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        hAttachment[0].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        // This is the frame buffer attachment to where the multisampled image
        // will be resolved to and which will be presented to the swapchain
        hAttachment[1].flags = 0;
        hAttachment[1].format = pSwapchain->surfaceFormat.format;
        hAttachment[1].samples = VK_SAMPLE_COUNT_1_BIT;
        hAttachment[1].loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        hAttachment[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment[1].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Multisampled depth attachment we render to
        hAttachment[2].flags = 0;
        hAttachment[2].format = pSwapchain->depthFormat;
        hAttachment[2].samples = pSwapchain->samples;
        hAttachment[2].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        hAttachment[2].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[2].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        hAttachment[2].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        hAttachment[2].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        hAttachment[2].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        colorReference.attachment = 0;
        colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        depthReference.attachment = 2;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        // Resolve attachment reference for the color attachment
        resolveReference.attachment = 1;
        resolveReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        subpass.flags = 0;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = NULL;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorReference;
        subpass.pResolveAttachments = &resolveReference;
        subpass.pDepthStencilAttachment = &depthReference;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = NULL;

        srcStageMask = 0;
        srcStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        srcStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        dstStageMask = 0;
        dstStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dstStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        srcAccessMask = 0;
        srcAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        dstAccessMask = 0;
        dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
        dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        // Depth buffer is shared between swapchain images
        attachmentDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[0].dstSubpass = 0;
        attachmentDependencies[0].srcStageMask = srcStageMask;
        attachmentDependencies[0].dstStageMask = dstStageMask;
        attachmentDependencies[0].srcAccessMask = srcAccessMask;
        attachmentDependencies[0].dstAccessMask = dstAccessMask;
        attachmentDependencies[0].dependencyFlags = 0;

        srcStageMask = 0;
        srcStageMask |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dstStageMask = 0;
        dstStageMask |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        srcAccessMask = 0;
        dstAccessMask = 0;
        dstAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dstAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;

        // Image Layout Transition
        attachmentDependencies[1].srcSubpass = VK_SUBPASS_EXTERNAL;
        attachmentDependencies[1].dstSubpass = 0;
        attachmentDependencies[1].srcStageMask = srcStageMask;
        attachmentDependencies[1].dstStageMask = dstStageMask;
        attachmentDependencies[1].srcAccessMask = srcAccessMask;
        attachmentDependencies[1].dstAccessMask = dstAccessMask;
        attachmentDependencies[1].dependencyFlags = 0;

        hRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        hRenderPassInfo.pNext = NULL;
        hRenderPassInfo.flags = 0;
        hRenderPassInfo.attachmentCount = 3;
        hRenderPassInfo.pAttachments = hAttachment;
        hRenderPassInfo.subpassCount = 1;
        hRenderPassInfo.pSubpasses = &subpass;
        hRenderPassInfo.dependencyCount = 2;
        hRenderPassInfo.pDependencies = attachmentDependencies;

        err = vkCreateRenderPass(device, &hRenderPassInfo, pAllocator, &p->renderPass);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateRenderPass failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return err;
}

VkResult
mmVKRenderPass_Create(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice,
    const struct mmVKSwapchain*                    pSwapchain)
{
    if (pSwapchain && VK_SAMPLE_COUNT_1_BIT != pSwapchain->samples)
    {
        return mmVKRenderPass_CreateMultiSampled(p, pDevice, pSwapchain);
    }
    else
    {
        return mmVKRenderPass_CreateBasicSampled(p, pDevice, pSwapchain);
    }
}

void
mmVKRenderPass_Delete(
    struct mmVKRenderPass*                         p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        const VkAllocationCallbacks* pAllocator = NULL;

        if (NULL == pDevice)
        {
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (VK_NULL_HANDLE != p->renderPass)
        {
            vkDestroyRenderPass(device, p->renderPass, pAllocator);
            p->renderPass = VK_NULL_HANDLE;
        }
    } while (0);
}
