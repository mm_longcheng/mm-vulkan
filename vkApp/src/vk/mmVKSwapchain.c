/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKSwapchain.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static 
VkResult
mmVKSwapchainPhysicalDeviceSurfaceSupportUpdate(
    struct mmVKInstance*                           pInstance,
    VkPhysicalDevice                               physicalDevice,
    VkSurfaceKHR                                   surface)
{
    VkResult err = VK_ERROR_UNKNOWN;
    uint32_t i = 0;
    uint32_t hPropertyCount = 0;
    VkBool32 hSupported = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &hPropertyCount, NULL);
    for (i = 0; i < hPropertyCount; i++)
    {
        err = pInstance->vkGetPhysicalDeviceSurfaceSupportKHR(
            physicalDevice,
            i,
            surface,
            &hSupported);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfaceSupportKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }
    }

    return err;
}

VkSurfaceFormatKHR
mmVKPickSurfaceFormat(
    const VkSurfaceFormatKHR*                      surfaceFormats,
    uint32_t                                       count,
    VkFormat                                       format)
{
    uint32_t i = 0;

    struct mmLogger* gLogger = mmLogger_Instance();
    
    assert(count >= 1);
    
    for (i = 0; i < count; i++)
    {
        const VkFormat sFormat = surfaceFormats[i].format;

        if (sFormat == format)
        {
            return surfaceFormats[i];
        }
    }
    
    // Prefer non-SRGB formats...
    for (i = 0; i < count; i++)
    {
        const VkFormat sFormat = surfaceFormats[i].format;

        if (sFormat == VK_FORMAT_R8G8B8A8_UNORM ||
            sFormat == VK_FORMAT_B8G8R8A8_UNORM ||
            sFormat == VK_FORMAT_A2B10G10R10_UNORM_PACK32 ||
            sFormat == VK_FORMAT_A2R10G10B10_UNORM_PACK32 ||
            sFormat == VK_FORMAT_R16G16B16A16_SFLOAT)
        {
            return surfaceFormats[i];
        }
    }

    mmLogger_LogI(gLogger, "Can't find our preferred formats... ");
    mmLogger_LogI(gLogger, "Falling back to first exposed format.");
    mmLogger_LogI(gLogger, "Rendering may be incorrect.");

    return surfaceFormats[0];
}

void
mmVKSwapchain_Init(
    struct mmVKSwapchain*                          p)
{
    mmMemset(p, 0, sizeof(struct mmVKSwapchain));
}

void
mmVKSwapchain_Destroy(
    struct mmVKSwapchain*                          p)
{
    mmMemset(p, 0, sizeof(struct mmVKSwapchain));
}

void
mmVKSwapchain_Reset(
    struct mmVKSwapchain*                          p)
{
    mmMemset(p, 0, sizeof(struct mmVKSwapchain));
}

VkResult
mmVKSwapchain_EvaluationSuitable(
    struct mmVKSwapchain*                          p,
    struct mmVKInstance*                           pInstance,
    VkPhysicalDevice                               physicalDevice,
    VkSurfaceKHR                                   surface,
    VkFormat                                       surfaceFormat)
{
    VkResult err = VK_ERROR_UNKNOWN;
    VkSurfaceFormatKHR* pSurfaceFormats = NULL;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        // Get the list of VkFormat's that are supported:
        uint32_t hFormatCount;
        err = pInstance->vkGetPhysicalDeviceSurfaceFormatsKHR(
            physicalDevice,
            surface,
            &hFormatCount,
            NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfaceFormatsKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }
        
        pSurfaceFormats = (VkSurfaceFormatKHR*)mmMalloc(hFormatCount * sizeof(VkSurfaceFormatKHR));
        if (NULL == pSurfaceFormats)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }
        
        err = pInstance->vkGetPhysicalDeviceSurfaceFormatsKHR(
            physicalDevice,
            surface,
            &hFormatCount,
            pSurfaceFormats);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfaceFormatsKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        p->surfaceFormat = mmVKPickSurfaceFormat(pSurfaceFormats, hFormatCount, surfaceFormat);
    } while(0);
    
    mmFree(pSurfaceFormats);
    
    return err;
}

VkResult
mmVKSwapchain_PrepareSwapchain(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    VkPresentModeKHR* pPresentModes = NULL;

    do
    {
        struct mmVKInstance* pInstance = NULL;
        struct mmVKDevice* pDevice = NULL;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkSurfaceKHR surface = VK_NULL_HANDLE;
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = pUploader->pAllocator;

        VkSwapchainKHR pOldSwapchain = p->swapchain;

        // Check the surface capabilities and formats
        VkSurfaceCapabilitiesKHR hSurfaceCapabilities;

        uint32_t hPresentModeCount;
        VkExtent2D hSwapchainExtent;

        uint32_t hDesiredNumOfSwapchainImages = 3;
        uint32_t i = 0;

        VkSurfaceTransformFlagsKHR hPreTransform;
        VkSwapchainCreateInfoKHR hSwapchainInfo;

        // The FIFO present mode is guaranteed by the spec to be supported
        // and to have no tearing.  It's a great default present mode to use.
        VkPresentModeKHR hSwapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

        // Find a supported composite alpha mode - one of these is guaranteed to be set
        VkCompositeAlphaFlagBitsKHR hCompositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        static const VkCompositeAlphaFlagBitsKHR hCompositeAlphaFlags[4] =
        {
            VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
            VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
            VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
        };

        struct mmLogger* gLogger = mmLogger_Instance();

        pInstance = info->pInstance;
        if (NULL == pInstance)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pInstance is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pDevice = info->pDevice;
        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        physicalDevice = info->physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        surface = info->surface;
        if (VK_NULL_HANDLE == surface)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " surface is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        err = mmVKSwapchain_EvaluationSuitable(
            p,
            pInstance,
            physicalDevice,
            surface,
            info->surfaceFormat);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_EvaluationSuitable failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        err = pInstance->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
            physicalDevice,
            surface,
            &hSurfaceCapabilities);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfaceCapabilitiesKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        err = pInstance->vkGetPhysicalDeviceSurfacePresentModesKHR(
            physicalDevice,
            surface,
            &hPresentModeCount,
            NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfacePresentModesKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        pPresentModes = (VkPresentModeKHR *)mmMalloc(hPresentModeCount * sizeof(VkPresentModeKHR));
        if (NULL == pPresentModes)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }

        err = pInstance->vkGetPhysicalDeviceSurfacePresentModesKHR(
            physicalDevice,
            surface,
            &hPresentModeCount,
            pPresentModes);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetPhysicalDeviceSurfacePresentModesKHR failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        // Avoid triggering Validation Error when rebuilding the swapchain.
        //
        // vkCreateSwapchainKHR(): pCreateInfo->surface is not known at this time to be supported 
        // for presentation by this device. The vkGetPhysicalDeviceSurfaceSupportKHR() must be 
        // called beforehand, and it must return VK_TRUE support with this surface for at least 
        // one queue family of this device.The Vulkan spec states : surface must be a surface that 
        // is supported by the device as determined using vkGetPhysicalDeviceSurfaceSupportKHR
        // (https ://vulkan.lunarg.com/doc/view/1.2.170.0/windows/1.2-extensions/
        // vkspec.html#VUID-VkSwapchainCreateInfoKHR-surface-01270)
        err = mmVKSwapchainPhysicalDeviceSurfaceSupportUpdate(pInstance, physicalDevice, surface);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchainPhysicalDeviceSurfaceSupportUpdate failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        // width and height are either both 0xFFFFFFFF, or both not 0xFFFFFFFF.
        if (hSurfaceCapabilities.currentExtent.width == 0xFFFFFFFF)
        {
            // If the surface size is undefined, the size is set to the size
            // of the images requested, which must fit within the minimum and
            // maximum values.
            hSwapchainExtent.width  = info->windowSize[0];
            hSwapchainExtent.height = info->windowSize[1];

            if (hSwapchainExtent.width < hSurfaceCapabilities.minImageExtent.width)
            {
                hSwapchainExtent.width = hSurfaceCapabilities.minImageExtent.width;
            }
            else if (hSwapchainExtent.width > hSurfaceCapabilities.maxImageExtent.width)
            {
                hSwapchainExtent.width = hSurfaceCapabilities.maxImageExtent.width;
            }

            if (hSwapchainExtent.height < hSurfaceCapabilities.minImageExtent.height)
            {
                hSwapchainExtent.height = hSurfaceCapabilities.minImageExtent.height;
            }
            else if (hSwapchainExtent.height > hSurfaceCapabilities.maxImageExtent.height)
            {
                hSwapchainExtent.height = hSurfaceCapabilities.maxImageExtent.height;
            }

            p->windowSize[0] = hSwapchainExtent.width;
            p->windowSize[1] = hSwapchainExtent.height;
        }
        else
        {
            if (info->windowSize[0] == 0 || info->windowSize[1] == 0)
            {
                // If the surface size is defined, the swap chain size must match
                hSwapchainExtent = hSurfaceCapabilities.currentExtent;
                p->windowSize[0] = hSurfaceCapabilities.currentExtent.width;
                p->windowSize[1] = hSurfaceCapabilities.currentExtent.height;
            }
            else
            {
                // On mobile devices, the rotating screen of the device will have a rotating animation. 
                // At this time, the current screen width and height are not the size after the rotation. 
                // We directly use the real size passed in from outside.
                // 
                // surface possible at animation transform, we use the final windowSize.
                hSwapchainExtent = hSurfaceCapabilities.currentExtent;
                hSwapchainExtent.width  = info->windowSize[0];
                hSwapchainExtent.height = info->windowSize[1];
                p->windowSize[0] = info->windowSize[0];
                p->windowSize[1] = info->windowSize[1];
            }
        }

        if (p->windowSize[0] == 0 || p->windowSize[1] == 0)
        {
            // surface is minimize.
            err = VK_SUCCESS;
            break;
        }

        //  There are times when you may wish to use another present mode.  The
        //  following code shows how to select them, and the comments provide some
        //  reasons you may wish to use them.
        //
        // It should be noted that Vulkan 1.0 doesn't provide a method for
        // synchronizing rendering with the presentation engine's display.  There
        // is a method provided for throttling rendering with the display, but
        // there are some presentation engines for which this method will not work.
        // If an application doesn't throttle its rendering, and if it renders much
        // faster than the refresh rate of the display, this can waste power on
        // mobile devices.  That is because power is being spent rendering images
        // that may never be seen.

        // VK_PRESENT_MODE_IMMEDIATE_KHR is for applications that don't care about
        // tearing, or have some way of synchronizing their rendering with the
        // display.
        // VK_PRESENT_MODE_MAILBOX_KHR may be useful for applications that
        // generally render a new presentable image every refresh cycle, but are
        // occasionally early.  In this case, the application wants the new image
        // to be displayed instead of the previously-queued-for-presentation image
        // that has not yet been displayed.
        // VK_PRESENT_MODE_FIFO_RELAXED_KHR is for applications that generally
        // render a new presentable image every refresh cycle, but are occasionally
        // late.  In this case (perhaps because of stuttering/latency concerns),
        // the application wants the late image to be immediately displayed, even
        // though that may mean some tearing.

        if (info->presentMode != hSwapchainPresentMode)
        {
            for (i = 0; i < hPresentModeCount; ++i)
            {
                if (pPresentModes[i] == info->presentMode)
                {
                    hSwapchainPresentMode = info->presentMode;
                    break;
                }
            }
        }

        if (hSwapchainPresentMode != info->presentMode)
        {
            // ERR_EXIT("Present mode specified is not supported\n", "Present mode unsupported");
            mmLogger_LogE(gLogger, "Present mode unsupported.");
            mmLogger_LogE(gLogger, "Present mode specified is not supportede.");
            err = VK_ERROR_FEATURE_NOT_PRESENT;
            break;
        }

        p->presentMode = hSwapchainPresentMode;

        // Determine the number of VkImages to use in the swap chain.
        // Application desires to acquire 3 images at a time for triple
        // buffering
        if (hDesiredNumOfSwapchainImages < hSurfaceCapabilities.minImageCount)
        {
            hDesiredNumOfSwapchainImages = hSurfaceCapabilities.minImageCount;
        }
        // If maxImageCount is 0, we can ask for as many images as we want;
        // otherwise we're limited to maxImageCount
        if ((hSurfaceCapabilities.maxImageCount > 0) &&
            (hDesiredNumOfSwapchainImages > hSurfaceCapabilities.maxImageCount))
        {
            // Application must settle for fewer images than desired:
            hDesiredNumOfSwapchainImages = hSurfaceCapabilities.maxImageCount;
        }

        if (hSurfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
        {
            hPreTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        }
        else
        {
            hPreTransform = hSurfaceCapabilities.currentTransform;
        }

        for (i = 0; i < MM_ARRAY_SIZE(hCompositeAlphaFlags); i++)
        {
            if (hSurfaceCapabilities.supportedCompositeAlpha & hCompositeAlphaFlags[i])
            {
                hCompositeAlpha = hCompositeAlphaFlags[i];
                break;
            }
        }

        hSwapchainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        hSwapchainInfo.pNext = NULL;
        hSwapchainInfo.flags = 0;
        hSwapchainInfo.surface = surface;
        hSwapchainInfo.minImageCount = hDesiredNumOfSwapchainImages;
        hSwapchainInfo.imageFormat = p->surfaceFormat.format;
        hSwapchainInfo.imageColorSpace = p->surfaceFormat.colorSpace;
        hSwapchainInfo.imageExtent = hSwapchainExtent;
        hSwapchainInfo.imageArrayLayers = 1;
        hSwapchainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        hSwapchainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        hSwapchainInfo.queueFamilyIndexCount = 0;
        hSwapchainInfo.pQueueFamilyIndices = NULL;
        hSwapchainInfo.preTransform = hPreTransform;
        hSwapchainInfo.compositeAlpha = hCompositeAlpha;
        hSwapchainInfo.presentMode = hSwapchainPresentMode;
        hSwapchainInfo.clipped = VK_TRUE;
        hSwapchainInfo.oldSwapchain = pOldSwapchain;

        err = pDevice->vkCreateSwapchainKHR(
            device,
            &hSwapchainInfo,
            pAllocator,
            &p->swapchain);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateSwapchainKHR failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmLogger_LogV(gLogger, "vkCreateSwapchainKHR Success.");

        // If we just re-created an existing swapchain, we should destroy the old
        // swapchain at this point.
        // Note: destroying the swapchain also cleans up all its associated
        // presentable images once the platform is done with them.
        if (pOldSwapchain != VK_NULL_HANDLE)
        {
            pDevice->vkDestroySwapchainKHR(device, pOldSwapchain, pAllocator);
            pOldSwapchain = VK_NULL_HANDLE;
        }

        err = VK_SUCCESS;
    } while (0);

    mmFree(pPresentModes);

    return err;
}

void
mmVKSwapchain_DiscardSwapchain(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice,
    struct mmVKUploader*                           pUploader)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pUploader is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pUploader->pAllocator;

        if (VK_NULL_HANDLE != p->swapchain)
        {
            pDevice->vkDestroySwapchainKHR(device, p->swapchain, pAllocator);
            p->swapchain = VK_NULL_HANDLE;
        }

        mmLogger_LogV(gLogger, "vkDestroySwapchainKHR Success.");
    } while (0);
}

VkResult
mmVKSwapchain_RebuildSwapchain(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    return mmVKSwapchain_PrepareSwapchain(p, info, pUploader);
}

VkResult
mmVKSwapchain_PrepareFrames(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice)
{
    VkResult err = VK_ERROR_UNKNOWN;

    VkImage* pSwapchainImages = NULL;

    do
    {
        VkDevice device = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;

        uint32_t i = 0;

        VkImageViewCreateInfo hImageViewInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        pAllocator = pDevice->pAllocator;

        err = pDevice->vkGetSwapchainImagesKHR(device, p->swapchain, &p->frameCount, NULL);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetSwapchainImagesKHR failure.", __FUNCTION__, __LINE__);
            break;
        }

        pSwapchainImages = (VkImage*)mmMalloc(p->frameCount * sizeof(VkImage));
        if (NULL == pSwapchainImages)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }

        err = pDevice->vkGetSwapchainImagesKHR(device, p->swapchain, &p->frameCount, pSwapchainImages);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkGetSwapchainImagesKHR failure.", __FUNCTION__, __LINE__);
            break;
        }

        p->frames = (struct mmVKSwapchainFrame*)mmMalloc(sizeof(struct mmVKSwapchainFrame) * p->frameCount);
        if (NULL == p->frames)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " Not enough Memory.", __FUNCTION__, __LINE__);
            err = VK_ERROR_OUT_OF_HOST_MEMORY;
            break;
        }

        hImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        hImageViewInfo.pNext = NULL;
        hImageViewInfo.flags = 0;
        hImageViewInfo.image = VK_NULL_HANDLE;
        hImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        hImageViewInfo.format = p->surfaceFormat.format;
        hImageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        hImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        hImageViewInfo.subresourceRange.baseMipLevel = 0;
        hImageViewInfo.subresourceRange.levelCount = 1;
        hImageViewInfo.subresourceRange.baseArrayLayer = 0;
        hImageViewInfo.subresourceRange.layerCount = 1;

        for (i = 0; i < p->frameCount; i++)
        {
            hImageViewInfo.image = pSwapchainImages[i];
            p->frames[i].image = pSwapchainImages[i];

            err = vkCreateImageView(device, &hImageViewInfo, pAllocator, &p->frames[i].imageView);
            if (VK_SUCCESS != err)
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " vkCreateImageView failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    mmFree(pSwapchainImages);

    return err;
}

void
mmVKSwapchain_DiscardFrames(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice)
{
    do
    {
        VkDevice device = VK_NULL_HANDLE;

        uint32_t i = 0;

        const VkAllocationCallbacks* pAllocator = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDevice is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        device = pDevice->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            break;
        }

        pAllocator = pDevice->pAllocator;

        if (NULL != p->frames)
        {
            for (i = 0; i < p->frameCount; i++)
            {
                if (VK_NULL_HANDLE != p->frames[i].imageView)
                {
                    vkDestroyImageView(device, p->frames[i].imageView, pAllocator);
                    p->frames[i].imageView = VK_NULL_HANDLE;
                }
                p->frames[i].image = VK_NULL_HANDLE;
            }

            mmFree(p->frames);
            p->frames = NULL;
            p->frameCount = 0;
        }
    } while (0);
}

VkResult
mmVKSwapchain_PrepareColor(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

        struct mmVKFrameCreateInfo hFrameInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        p->samples = info->samples;

        if (VK_SAMPLE_COUNT_1_BIT == info->samples)
        {
            // samples count 1 not need additional color attachment.
            err = VK_SUCCESS;
            break;
        }

        physicalDevice = info->physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        hFrameInfo.format = p->surfaceFormat.format;
        hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
        hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        hFrameInfo.extent[0] = p->windowSize[0];
        hFrameInfo.extent[1] = p->windowSize[1];
        hFrameInfo.extent[2] = 1;
        hFrameInfo.mipLevels = 1;
        hFrameInfo.arrayLayers = 1;
        hFrameInfo.samples = info->samples;
        hFrameInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        hFrameInfo.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        hFrameInfo.vmaFlags = 0;
        hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

        err = mmVKFrame_Create(&p->color, &hFrameInfo, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "mmVKFrameCreate failure.");
            break;
        }
    } while (0);

    return err;
}

void
mmVKSwapchain_DiscardColor(
    struct mmVKSwapchain*                          p,
    struct mmVKUploader*                           pUploader)
{
    mmVKFrame_Delete(&p->color, pUploader);
}

VkResult
mmVKSwapchain_PrepareDepthStencil(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

        struct mmVKFrameCreateInfo hFrameInfo;

        struct mmLogger* gLogger = mmLogger_Instance();

        physicalDevice = info->physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        p->depthFormat = mmVKPickDepthFormat(physicalDevice, info->depthFormat);

        hFrameInfo.format = p->depthFormat;
        hFrameInfo.imageType = VK_IMAGE_TYPE_2D;
        hFrameInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        hFrameInfo.extent[0] = p->windowSize[0];
        hFrameInfo.extent[1] = p->windowSize[1];
        hFrameInfo.extent[2] = 1;
        hFrameInfo.mipLevels = 1;
        hFrameInfo.arrayLayers = 1;
        hFrameInfo.samples = info->samples;
        hFrameInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        hFrameInfo.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        hFrameInfo.vmaFlags = 0;
        hFrameInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;

        err = mmVKFrame_Create(&p->depth, &hFrameInfo, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "mmVKFrameCreate failure.");
            break;
        }
    } while (0);

    return err;
}

void
mmVKSwapchain_DiscardDepthStencil(
    struct mmVKSwapchain*                          p,
    struct mmVKUploader*                           pUploader)
{
    mmVKFrame_Delete(&p->depth, pUploader);
}

VkResult
mmVKSwapchain_Create(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        err = mmVKSwapchain_PrepareSwapchain(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareSwapchain failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareFrames(p, info->pDevice);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareFrames failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareColor(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareColor failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareDepthStencil(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareDepthStencil failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}

void
mmVKSwapchain_Delete(
    struct mmVKSwapchain*                          p,
    struct mmVKDevice*                             pDevice,
    struct mmVKUploader*                           pUploader)
{
    mmVKSwapchain_DiscardDepthStencil(p, pUploader);
    mmVKSwapchain_DiscardColor(p, pUploader);
    mmVKSwapchain_DiscardFrames(p, pDevice);
    mmVKSwapchain_DiscardSwapchain(p, pDevice, pUploader);
}

VkResult
mmVKSwapchain_Rebuild(
    struct mmVKSwapchain*                          p,
    const struct mmVKSwapchainCreateInfo*          info,
    struct mmVKUploader*                           pUploader)
{
    VkResult err = VK_ERROR_UNKNOWN;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmVKSwapchain_DiscardDepthStencil(p, pUploader);
        mmVKSwapchain_DiscardColor(p, pUploader);
        mmVKSwapchain_DiscardFrames(p, info->pDevice);

        err = mmVKSwapchain_RebuildSwapchain(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareSwapchain failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareFrames(p, info->pDevice);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareFrames failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareColor(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareColor failure.", __FUNCTION__, __LINE__);
            break;
        }

        err = mmVKSwapchain_PrepareDepthStencil(p, info, pUploader);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKSwapchain_PrepareDepthStencil failure.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return err;
}
