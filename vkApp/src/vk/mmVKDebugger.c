/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVKDebugger.h"

#include "vk/mmVKExtension.h"

#include "core/mmLogger.h"
#include "core/mmLoggerManager.h"

VKAPI_ATTR
VkBool32
VKAPI_CALL
mmVKDebuggerMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT         messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT                messageType,
    const VkDebugUtilsMessengerCallbackDataEXT*    pCallbackData,
    void*                                          pUserData)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt32_t lvl = MM_LOG_INFO;
    switch (messageSeverity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        lvl = MM_LOG_VERBOSE;
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
        lvl = MM_LOG_INFO;
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        lvl = MM_LOG_WARNING;
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        lvl = MM_LOG_ERROR;
        break;
    default:
        lvl = MM_LOG_INFO;
        break;
    }
    mmLoggerManager_LoggerSection(gLogger, "vk", lvl, pCallbackData->pMessage);
    // Don't bail out, but keep going.
    return VK_FALSE;
}

void
mmVKDebuggerMakeExtension(
    struct mmVKExtension*                          il,
    struct mmVKExtension*                          ie,
    int                                            hValidationLayer)
{
    if (MM_TRUE == hValidationLayer)
    {
        mmVKExtension_SetRequireFeature(il, il->hRequireCount, 0, "VK_LAYER_KHRONOS_validation");
        mmVKExtension_SetRequireFeature(ie, ie->hRequireCount, 0, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
}

