/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmGlslangValidator_h__
#define __mmGlslangValidator_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmFileContext;

/**
 This function will Assign IO interface.

 @param fileContext The io interface.
 */
void mmGlslangValidatorSetFileContext(struct mmFileContext* fileContext);

/**
 This function will reset the Compiler.

 The io interface will not be reset.
 */
void mmGlslangValidatorReset(void);

/**
 This function will add "#define xxx".

 @param define The define Macro.
 */
void mmGlslangValidatorAddDefine(const char* define);

/**
 This function will check if \a t1 is equal to \a t2.

 @param I The include path such as "-I.".
 @param i The input   path such as "ui.vert".
 @param o The output  path such as "ui.vert.spv".
 @return zero if success.
 */
int mmGlslangValidatorCompile(const char* I, const char* i, const char* o);

#include "core/mmSuffix.h"

#endif//__mmGlslangValidator_h__
