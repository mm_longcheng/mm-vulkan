/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"

#include "vk/mmVKCommandBuffer.h"

#include "view/mmUIViewTypes.h"

#include "algorithm/mmMurmurHash.h"

#ifdef _DEBUG
#define MM_VK_VALIDATION MM_TRUE
#else
#define MM_VK_VALIDATION MM_FALSE
#endif//_DEBUG

void 
mmDumpMurmurHash2(
    const char*                                    s)
{
    mmUInt32_t h;
    size_t len = strlen(s);
    h = (mmUInt32_t)mmMurmurHash2((const void*)s, (int)len, 0);
    printf("0x%08X %s\n", h, s);
}

void
mmActivityMaster_LoggerFrameStats(
    struct mmActivityMaster*                       p,
    struct mmEventUpdate*                          content)
{
    struct mmFrameTimer* Timer = &p->pSurfaceMaster->hFrameScheduler.frame_timer;
    struct mmFrameStats* Stats = &Timer->status;
    if (0 == Stats->index)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "app dynamic: %f average: %f cost: %f wait_usec: %f",
            Timer->frequency_dynamic,
            Stats->average,
            Timer->frame_cost_usec,
            Timer->wait_usec);
    }
}

// virtual function for super interface implement.
void
mmActivityMaster_SetNativePointer(
    struct mmIActivity*                            pSuper,
    void*                                          pNativePointer)
{
    
}

void
mmActivityMaster_SetContext(
    struct mmIActivity*                            pSuper,
    struct mmContextMaster*                        pContextMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    p->pContextMaster = pContextMaster;
}

void
mmActivityMaster_SetSurface(
    struct mmIActivity*                            pSuper,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    p->pSurfaceMaster = pSurfaceMaster;
}

void
mmActivityMaster_OnDragging(
    struct mmIActivity*                            pSuper,
    struct mmEventDragging*                        content)
{
    
}

void
mmActivityMaster_OnKeypadPressed(
    struct mmIActivity*                            pSuper,
    struct mmEventKeypad*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnKeypadPressed(&p->vUIManager, content);
}

void
mmActivityMaster_OnKeypadRelease(
    struct mmIActivity*                            pSuper,
    struct mmEventKeypad*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnKeypadRelease(&p->vUIManager, content);
}

void
mmActivityMaster_OnKeypadStatus(
    struct mmIActivity*                            pSuper,
    struct mmEventKeypadStatus*                    content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnKeypadStatus(&p->vUIManager, content);
}

void
mmActivityMaster_OnCursorBegan(
    struct mmIActivity*                            pSuper,
    struct mmEventCursor*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnCursorBegan(&p->vUIManager, content);
}

void
mmActivityMaster_OnCursorMoved(
    struct mmIActivity*                            pSuper,
    struct mmEventCursor*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnCursorMoved(&p->vUIManager, content);
}

void
mmActivityMaster_OnCursorEnded(
    struct mmIActivity*                            pSuper,
    struct mmEventCursor*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnCursorEnded(&p->vUIManager, content);
}

void
mmActivityMaster_OnCursorBreak(
    struct mmIActivity*                            pSuper,
    struct mmEventCursor*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnCursorBreak(&p->vUIManager, content);
}

void
mmActivityMaster_OnCursorWheel(
    struct mmIActivity*                            pSuper,
    struct mmEventCursor*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnCursorWheel(&p->vUIManager, content);
}

void
mmActivityMaster_OnTouchsBegan(
    struct mmIActivity*                            pSuper,
    struct mmEventTouchs*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnTouchsBegan(&p->vUIManager, content);
}

void
mmActivityMaster_OnTouchsMoved(
    struct mmIActivity*                            pSuper,
    struct mmEventTouchs*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnTouchsMoved(&p->vUIManager, content);
}

void
mmActivityMaster_OnTouchsEnded(
    struct mmIActivity*                            pSuper,
    struct mmEventTouchs*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnTouchsEnded(&p->vUIManager, content);
}

void
mmActivityMaster_OnTouchsBreak(
    struct mmIActivity*                            pSuper,
    struct mmEventTouchs*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnTouchsBreak(&p->vUIManager, content);
}

void
mmActivityMaster_OnSurfacePrepare(
    struct mmIActivity*                            pSuper,
    struct mmEventMetrics*                         content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);

    mmVKManager_DeviceWaitIdle(&p->vVKManager);

    mmVKManager_OnSurfacePrepare(&p->vVKManager, content);
    mmUIManager_OnSurfacePrepare(&p->vUIManager, content);
}

void
mmActivityMaster_OnSurfaceDiscard(
    struct mmIActivity*                            pSuper,
    struct mmEventMetrics*                         content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);

    mmVKManager_DeviceWaitIdle(&p->vVKManager);

    mmUIManager_OnSurfaceDiscard(&p->vUIManager, content);
    mmVKManager_OnSurfaceDiscard(&p->vVKManager, content);
}

void
mmActivityMaster_OnSurfaceChanged(
    struct mmIActivity*                            pSuper,
    struct mmEventMetrics*                         content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);

    mmVKManager_DeviceWaitIdle(&p->vVKManager);

    mmVKManager_OnSurfaceChanged(&p->vVKManager, content);
    mmUIManager_OnSurfaceChanged(&p->vUIManager, content);
}

void
mmActivityMaster_OnGetText(
    struct mmIActivity*                            pSuper,
    struct mmEventEditText*                        content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnGetText(&p->vUIManager, content);
}

void
mmActivityMaster_OnSetText(
    struct mmIActivity*                            pSuper,
    struct mmEventEditText*                        content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnSetText(&p->vUIManager, content);
}

void
mmActivityMaster_OnGetTextEditRect(
    struct mmIActivity*                            pSuper,
    double                                         rect[4])
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnGetTextEditRect(&p->vUIManager, rect);
}

void
mmActivityMaster_OnInsertTextUtf16(
    struct mmIActivity*                            pSuper,
    struct mmEventEditString*                      content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnInsertTextUtf16(&p->vUIManager, content);
}

void
mmActivityMaster_OnReplaceTextUtf16(
    struct mmIActivity*                            pSuper,
    struct mmEventEditString*                      content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnReplaceTextUtf16(&p->vUIManager, content);
}

void
mmActivityMaster_OnInjectCopy(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnInjectCopy(&p->vUIManager);
}

void
mmActivityMaster_OnInjectCut(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnInjectCut(&p->vUIManager);
}

void
mmActivityMaster_OnInjectPaste(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_OnInjectPaste(&p->vUIManager);
}

void
mmActivityMaster_OnDisplayOneFrame(
    struct mmIActivity*                            pSuper,
    struct mmEventUpdate*                          content)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);

    struct mmVKCommandBuffer hCommandBuffer = { 0 };
    float hClearColor[4] = { 0.2f, 0.2f, 0.2f, 0.2f, };
    uint32_t hWindowRect[4];

    // debugger FrameStats.
    // mmActivityMaster_LoggerFrameStats(p, content);

    mmALManager_Update(&p->vALManager, content->interval);
    mmUIManager_Update(&p->vUIManager, content);

    /* acquire next image */
    mmVKManager_AcquireNextFrame(&p->vVKManager, &hCommandBuffer, hWindowRect);

    /* begin */
    mmVKManager_BeginCommandBuffer(&p->vVKManager);
    
    mmUIManager_RecordBeforePass(&p->vUIManager, &hCommandBuffer);
    
    mmVKManager_BeginRenderPass(&p->vVKManager, hWindowRect, hClearColor);

    mmVKCommandBuffer_SetViewport(&hCommandBuffer, 0.0f, 1.0f);
    mmVKCommandBuffer_SetScissor(&hCommandBuffer);

    /* record CommandBuffer begin */
    mmUIManager_RecordBeforeView(&p->vUIManager, &hCommandBuffer);
    mmUIManager_RecordBasicsView(&p->vUIManager, &hCommandBuffer);
    mmUIManager_RecordBehindView(&p->vUIManager, &hCommandBuffer);
    /* record CommandBuffer end */

    /* end */
    mmVKManager_EndRenderPass(&p->vVKManager);
    
    mmUIManager_RecordBehindPass(&p->vUIManager, &hCommandBuffer);
    
    mmVKManager_EndCommandBuffer(&p->vVKManager);

    /* submit present */
    mmVKManager_QueueSubmit(&p->vVKManager);
    mmVKManager_QueuePresent(&p->vVKManager);
}

void
mmActivityMaster_OnStart(
    struct mmIActivity*                            pSuper)
{
    
}

void
mmActivityMaster_OnInterrupt(
    struct mmIActivity*                            pSuper)
{
    
}

void
mmActivityMaster_OnShutdown(
    struct mmIActivity*                            pSuper)
{
    
}

void
mmActivityMaster_OnJoin(
    struct mmIActivity*                            pSuper)
{
    
}

void
mmActivityMaster_OnFinishLaunching(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);

    mmALManager_SetFileContext(&p->vALManager, &p->pContextMaster->hFileContext);
    mmALManager_Prepare(&p->vALManager);

    mmVKManager_SetContextMaster(&p->vVKManager, p->pContextMaster);
    mmVKManager_SetSurfaceMaster(&p->vVKManager, p->pSurfaceMaster);
    mmVKManager_Prepare(&p->vVKManager);

    mmUIManager_AddModule(&p->vUIManager, "mmALManager", &p->vALManager);
    mmUIManager_AddModule(&p->vUIManager, "mmVKManager", &p->vVKManager);
    mmUIManager_AddModule(&p->vUIManager, "mmUIManager", &p->vUIManager);
    mmUIManager_AddSchemePath(&p->vUIManager, "scheme/mmUISchemeMaster.scheme.json");
    mmUIManager_AddSchemePath(&p->vUIManager, "scheme/mmUISchemeDark.scheme.json");
    mmUIManager_SetContextMaster(&p->vUIManager, p->pContextMaster);
    mmUIManager_SetSurfaceMaster(&p->vUIManager, p->pSurfaceMaster);
    mmUIManager_SetSwapchain(&p->vUIManager, &p->vVKManager.vSwapchain);
    mmUIManager_SetAssets(&p->vUIManager, &p->vVKManager.vAssets);
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewMaster");
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewWorld");
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewCube");
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewModel");
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewLayer");
    mmUIManager_SetViewMaster(&p->vUIManager, "mmUIViewCalendar");
    mmUIManager_SetScheme(&p->vUIManager, "mmUISchemeMaster");
    mmUIManager_Prepare(&p->vUIManager);
}

void
mmActivityMaster_OnBeforeTerminate(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    mmVKManager_DeviceWaitIdle(&p->vVKManager);

    mmUIManager_Discard(&p->vUIManager);
    mmUIManager_ClearSchemePath(&p->vUIManager);
    mmUIManager_ClearModule(&p->vUIManager);

    mmVKManager_Discard(&p->vVKManager);

    mmALManager_Discard(&p->vALManager);
}

void
mmActivityMaster_OnEnterBackground(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmALManager_EnterBackground(&p->vALManager);
    mmUIManager_EnterBackground(&p->vUIManager);
}

void
mmActivityMaster_OnEnterForeground(
    struct mmIActivity*                            pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mmUIManager_EnterForeground(&p->vUIManager);
    mmALManager_EnterForeground(&p->vALManager);
}

const struct mmIActivity mmIActivityMaster =
{
    &mmActivityMaster_SetNativePointer,
    &mmActivityMaster_SetContext,
    &mmActivityMaster_SetSurface,
    
    &mmActivityMaster_OnDragging,

    &mmActivityMaster_OnKeypadPressed,
    &mmActivityMaster_OnKeypadRelease,
    &mmActivityMaster_OnKeypadStatus,

    &mmActivityMaster_OnCursorBegan,
    &mmActivityMaster_OnCursorMoved,
    &mmActivityMaster_OnCursorEnded,
    &mmActivityMaster_OnCursorBreak,
    &mmActivityMaster_OnCursorWheel,

    &mmActivityMaster_OnTouchsBegan,
    &mmActivityMaster_OnTouchsMoved,
    &mmActivityMaster_OnTouchsEnded,
    &mmActivityMaster_OnTouchsBreak,

    &mmActivityMaster_OnSurfacePrepare,
    &mmActivityMaster_OnSurfaceDiscard,
    &mmActivityMaster_OnSurfaceChanged,

    &mmActivityMaster_OnGetText,
    &mmActivityMaster_OnSetText,
    &mmActivityMaster_OnGetTextEditRect,
    &mmActivityMaster_OnInsertTextUtf16,
    &mmActivityMaster_OnReplaceTextUtf16,

    &mmActivityMaster_OnInjectCopy,
    &mmActivityMaster_OnInjectCut,
    &mmActivityMaster_OnInjectPaste,

    &mmActivityMaster_OnDisplayOneFrame,

    &mmActivityMaster_OnStart,
    &mmActivityMaster_OnInterrupt,
    &mmActivityMaster_OnShutdown,
    &mmActivityMaster_OnJoin,

    &mmActivityMaster_OnFinishLaunching,
    &mmActivityMaster_OnBeforeTerminate,
    
    &mmActivityMaster_OnEnterBackground,
    &mmActivityMaster_OnEnterForeground,
};

void
mmActivityMaster_Init(
    struct mmActivityMaster*                       p)
{
    mmIActivity_Init(&p->hSuper);
    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;
    
    mmALManager_Init(&p->vALManager);
    mmVKManager_Init(&p->vVKManager);
    mmUIManager_Init(&p->vUIManager);

    // add view types.
    mmUIViewFactoryAddTypes(&p->vUIManager.vViewFactory);

    p->hSuper = mmIActivityMaster;
}

void
mmActivityMaster_Destroy(
    struct mmActivityMaster*                       p)
{
    p->hSuper = mmIActivityMaster;

    // rmv view types.
    mmUIViewFactoryRmvTypes(&p->vUIManager.vViewFactory);

    mmUIManager_Destroy(&p->vUIManager);
    mmVKManager_Destroy(&p->vVKManager);
    mmALManager_Destroy(&p->vALManager);

    p->pSurfaceMaster = NULL;
    p->pContextMaster = NULL;
    mmIActivity_Destroy(&p->hSuper);
}
