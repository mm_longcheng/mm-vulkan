/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityComponentManager_h__
#define __mmEntityComponentManager_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeString.h"

#include "entityx/mmEntityComponent.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntityComponentManager
{
    struct mmRbtreeU64Vpt pools;
    struct mmRbtreeStringVpt names;
    size_t familys;
};

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_Init(
    struct mmEntityComponentManager*               p);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_Destroy(
    struct mmEntityComponentManager*               p);

MM_EXPORT_ENTITY 
struct mmEntityComponentType* 
mmEntityComponentManager_Add(
    struct mmEntityComponentManager*               p, 
    const char*                                    n, 
    const struct mmEntityComponentMetadata*        m);

MM_EXPORT_ENTITY 
struct mmEntityComponentType* 
mmEntityComponentManager_GetById(
    const struct mmEntityComponentManager*         p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
struct mmEntityComponentType* 
mmEntityComponentManager_Get(
    const struct mmEntityComponentManager*         p, 
    const char*                                    n);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_RmvById(
    struct mmEntityComponentManager*               p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_Rmv(
    struct mmEntityComponentManager*               p, 
    const char*                                    n);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_Clear(
    struct mmEntityComponentManager*               p);

MM_EXPORT_ENTITY 
void* 
mmEntityComponentManager_ProduceById(
    struct mmEntityComponentManager*               p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_RecycleById(
    struct mmEntityComponentManager*               p, 
    size_t                                         id, 
    void*                                          v);

MM_EXPORT_ENTITY 
void* 
mmEntityComponentManager_Produce(
    struct mmEntityComponentManager*               p, 
    const char*                                    n, 
    size_t*                                        id);

MM_EXPORT_ENTITY 
void 
mmEntityComponentManager_Recycle(
    struct mmEntityComponentManager*               p, 
    const char*                                    n, 
    void*                                          v);

#include "core/mmSuffix.h"

#endif//__mmEntityComponentManager_h__
