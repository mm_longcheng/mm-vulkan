/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityManager_h__
#define __mmEntityManager_h__

#include "core/mmCore.h"
#include "core/mmPoolElement.h"

#include "container/mmVectorVpt.h"

#include "dish/mmIdGenerater.h"

#include "entityx/mmEntity.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntityManager
{
    struct mmUInt32IdGenerater generater;
    struct mmPoolElement pool;
    struct mmVectorVpt entitys;
};

MM_EXPORT_ENTITY 
void 
mmEntityManager_Init(
    struct mmEntityManager*                        p);

MM_EXPORT_ENTITY 
void 
mmEntityManager_Destroy(
    struct mmEntityManager*                        p);

MM_EXPORT_ENTITY 
void 
mmEntityManager_SetChunkSize(
    struct mmEntityManager*                        p, 
    size_t                                         chunk_size);

MM_EXPORT_ENTITY 
struct mmEntity* 
mmEntityManager_Produce(
    struct mmEntityManager*                        p);

MM_EXPORT_ENTITY 
void 
mmEntityManager_Recycle(
    struct mmEntityManager*                        p, 
    struct mmEntity*                               entity);

MM_EXPORT_ENTITY 
struct mmEntity* 
mmEntityManager_Get(
    const struct mmEntityManager*                  p, 
    mmEntityIdType                                 e);

MM_EXPORT_ENTITY 
void 
mmEntityManager_Clear(
    struct mmEntityManager*                        p);

#include "core/mmSuffix.h"

#endif//__mmEntityManager_h__
