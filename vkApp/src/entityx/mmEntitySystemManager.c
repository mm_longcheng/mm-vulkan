/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntitySystemManager.h"

#include "core/mmAlloc.h"

MM_EXPORT_ENTITY
void
mmEntitySystemManager_Init(
    struct mmEntitySystemManager*                  p)
{
    mmRbtreeU64Vpt_Init(&p->systems);
    mmRbtreeStringVpt_Init(&p->names);
    p->familys = 0;
}

MM_EXPORT_ENTITY
void
mmEntitySystemManager_Destroy(
    struct mmEntitySystemManager*                  p)
{
    mmRbtreeStringVpt_Destroy(&p->names);
    mmRbtreeU64Vpt_Destroy(&p->systems);
    p->familys = 0;
}

MM_EXPORT_ENTITY
struct mmEntitySystemType*
mmEntitySystemManager_Add(
    struct mmEntitySystemManager*                  p,
    const char*                                    n,
    const struct mmEntitySystemMetadata*           m)
{
    struct mmEntitySystemType* e = NULL;

    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    it = mmRbtreeStringVpt_GetIterator(&p->names, &hWeakKey);
    if (NULL == it)
    {
        e = (struct mmEntitySystemType*)mmMalloc(sizeof(struct mmEntitySystemType));
        mmEntitySystemType_Init(e);
        mmEntitySystemType_Assign(e, n, p->familys++);
        mmEntitySystemType_Prepare(e, m);
        mmRbtreeU64Vpt_Set(&p->systems, (mmUInt64_t)e->family, e);
        mmRbtreeStringVpt_Set(&p->names, &hWeakKey, e);
    }
    return e;
}

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntitySystemManager_GetById(
    const struct mmEntitySystemManager*            p,
    size_t                                         id)
{
    return (struct mmEntitySystemType*)mmRbtreeU64Vpt_Get(&p->systems, (mmUInt64_t)id);
}

MM_EXPORT_ENTITY
struct mmEntitySystemType*
mmEntitySystemManager_Get(
    const struct mmEntitySystemManager*            p,
    const char*                                    n)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    return (struct mmEntitySystemType*)mmRbtreeStringVpt_Get(&p->names, &hWeakKey);
}

MM_EXPORT_ENTITY
void
mmEntitySystemManager_RmvById(
    struct mmEntitySystemManager*                  p,
    size_t                                         id)
{
    struct mmEntitySystemType* e = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    it = mmRbtreeU64Vpt_GetIterator(&p->systems, (mmUInt64_t)id);
    if (NULL != it)
    {
        e = (struct mmEntitySystemType*)it->v;
        mmRbtreeStringVpt_Rmv(&p->names, &e->name);
        mmRbtreeU64Vpt_Erase(&p->systems, it);
        mmEntitySystemType_Discard(e);
        mmEntitySystemType_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_ENTITY
void
mmEntitySystemManager_Rmv(
    struct mmEntitySystemManager*                  p,
    const char*                                    n)
{
    struct mmEntitySystemType* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    it = mmRbtreeStringVpt_GetIterator(&p->names, &hWeakKey);
    if (NULL != it)
    {
        e = (struct mmEntitySystemType*)it->v;
        mmRbtreeU64Vpt_Rmv(&p->systems, e->family);
        mmRbtreeStringVpt_Erase(&p->names, it);
        mmEntitySystemType_Discard(e);
        mmEntitySystemType_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_ENTITY
void
mmEntitySystemManager_Clear(
    struct mmEntitySystemManager*                  p)
{
    struct mmEntitySystemType* e = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    struct mmRbNode* n = NULL;
    n = mmRb_First(&p->systems.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmEntitySystemType*)it->v;
        mmRbtreeStringVpt_Rmv(&p->names, &e->name);
        mmRbtreeU64Vpt_Erase(&p->systems, it);
        mmEntitySystemType_Discard(e);
        mmEntitySystemType_Destroy(e);
        mmFree(e);
    }
}

