/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityWorld_h__
#define __mmEntityWorld_h__

#include "core/mmCore.h"

#include "entityx/mmEntityManager.h"
#include "entityx/mmEntitySystemManager.h"
#include "entityx/mmEntityComponentManager.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntityWorld
{
    struct mmEntityManager entityManager;
    struct mmEntityComponentManager componentManager;
    struct mmEntitySystemManager systemManager;
    struct mmEntityMaskTuple maskTuple;
    struct mmEntity* worldEntity;
    void* context;
};

MM_EXPORT_ENTITY 
void 
mmEntityWorld_Init(
    struct mmEntityWorld*                          p);

MM_EXPORT_ENTITY 
void 
mmEntityWorld_Destroy(
    struct mmEntityWorld*                          p);

MM_EXPORT_ENTITY 
void 
mmEntityWorld_SetEntityChunkSize(
    struct mmEntityWorld*                          p,
    size_t                                         chunk_size);

MM_EXPORT_ENTITY 
void 
mmEntityWorld_SetContext(
    struct mmEntityWorld*                          p,
    void*                                          context);

// clear entity component.
// note: this api will rmv the component data, and rmv the mask tuple component.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_ClearEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity);

// clear all world entity.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_ClearEntity(
    struct mmEntityWorld*                          p);


// add system to world.
MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_AddSystem(
    struct mmEntityWorld*                          p,
    const char*                                    n,
    const struct mmEntitySystemMetadata*           m);

// get system to world.
MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_GetSystem(
    const struct mmEntityWorld*                    p,
    const char*                                    n);

// get system to world.
MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_GetSystemById(
    const struct mmEntityWorld*                    p, 
    size_t                                         id);

// rmv system to world.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvSystem(
    struct mmEntityWorld*                          p,
    const char*                                    n);

// rmv system to world.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvSystemById(
    struct mmEntityWorld*                          p,
    size_t                                         id);


// component registration name -> data_type pool.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AddComponent(
    struct mmEntityWorld*                          p,
    const char*                                    n,
    const struct mmEntityComponentMetadata*        m);

// component cancellation name -> data_type pool.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvComponent(
    struct mmEntityWorld*                          p,
    const char*                                    n);


// produce entity. 
MM_EXPORT_ENTITY 
struct mmEntity* 
mmEntityWorld_Produce(
    struct mmEntityWorld*                          p);

// recycle entity. 
MM_EXPORT_ENTITY 
void 
mmEntityWorld_Recycle(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity);

// get entity by entity id. 
MM_EXPORT_ENTITY 
struct mmEntity* 
mmEntityWorld_GetEntity(
    const struct mmEntityWorld*                    p,
    mmEntityIdType                                 e);


// add component mask to system.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AddSystemComponentMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s, 
    const char*                                    n);

// rmv component mask to system.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvSystemComponentMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s, 
    const char*                                    n);

// add component mask to system.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AddSystemMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s);

// rmv component mask to system.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvSystemMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s);

// add entity component by entity and data type name.
// note: this api only add the component data, not add the mask tuple component.
MM_EXPORT_ENTITY 
void* 
mmEntityWorld_AddEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity, 
    const char*                                    n);

// add entity component by entity and data type id.
// note: this api only add the component data, not add the mask tuple component.
MM_EXPORT_ENTITY 
void* 
mmEntityWorld_AddEntityComponentById(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    size_t                                         id);

// rmv entity component by entity and data type name.
// note: this api will rmv the component data, and rmv the mask tuple component.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    const char*                                    n);

// rmv entity component by entity and data type id.
// note: this api will rmv the component data, and rmv the mask tuple component.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_RmvEntityComponentById(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    size_t                                         id);

// get entity component by data type name.
MM_EXPORT_ENTITY 
void* 
mmEntityWorld_GetEntityComponent(
    const struct mmEntityWorld*                    p, 
    struct mmEntity*                               entity,
    const char*                                    n);

// get entity component by data type id.
MM_EXPORT_ENTITY 
void* 
mmEntityWorld_GetEntityComponentById(
    const struct mmEntityWorld*                    p, 
    struct mmEntity*                               entity,
    size_t                                         id);

// get entity component by data type name array.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_GetEntityComponents(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    const char**                                   n,
    void**                                         c,
    size_t                                         z);

// get entity component by system.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_GetEntityComponentsBySystem(
    const struct mmEntityWorld*                    p, 
    struct mmEntity*                               entity,
    struct mmEntitySystemType*                     s, 
    void**                                         c);

// get entity component by system name.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_GetEntityComponentsBySystemName(
    const struct mmEntityWorld*                    p, 
    struct mmEntity*                               entity,
    const char*                                    n,
    void**                                         c);

// attach all data type id by the entity mask to mask tuple component.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AttachEntityMaskTuple(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity);

// detach all data type id by the entity mask to mask tuple component.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_DetachEntityMaskTuple(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity);

// attach entity system event. specialization mmEntitySystemType.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AttachEntitySystemEvent(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s);

// detach entity system event. specialization mmEntitySystemType.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_DetachEntitySystemEvent(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s);

// attach entity component event. specialization mmEntityComponentType.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_AttachEntityComponentEvent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    struct mmEntityComponentType*                  c, 
    void*                                          d);

// detach entity component event. specialization mmEntityComponentType.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_DetachEntityComponentEvent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity, 
    struct mmEntityComponentType*                  c, 
    void*                                          d);

// update system by event.
MM_EXPORT_ENTITY 
void 
mmEntityWorld_UpdateSystem(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s, 
    void*                                          evt,
    void*                                          func);

#include "core/mmSuffix.h"

#endif//__mmEntityWorld_h__
