/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntity_h__
#define __mmEntity_h__

#include "core/mmCore.h"

#include "container/mmBitset.h"

#include "entityx/mmEntityTuple.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

typedef mmUInt32_t mmEntityIdType;

MM_EXPORT_ENTITY extern const mmEntityIdType MM_ENTITY_INVALID_ID;

struct mmEntity
{
    mmEntityIdType id;
    struct mmBitset mask;
    struct mmEntityTuple tuple;
};

MM_EXPORT_ENTITY 
void 
mmEntity_Init(
    struct mmEntity*                               p);

MM_EXPORT_ENTITY 
void 
mmEntity_Destroy(
    struct mmEntity*                               p);

MM_EXPORT_ENTITY 
void 
mmEntity_SetId(
    struct mmEntity*                               p, 
    const mmEntityIdType                           id);

MM_EXPORT_ENTITY 
mmEntityIdType 
mmEntity_GetId(
    const struct mmEntity*                         p);

MM_EXPORT_ENTITY 
void 
mmEntity_AttachComponent(
    struct mmEntity*                               p, 
    size_t                                         id, 
    void*                                          d);

MM_EXPORT_ENTITY 
void* 
mmEntity_DetachComponent(
    struct mmEntity*                               p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
void* 
mmEntity_GetComponent(
    const struct mmEntity*                         p, 
    size_t                                         id);

#include "core/mmSuffix.h"

#endif//__mmEntity_h__
