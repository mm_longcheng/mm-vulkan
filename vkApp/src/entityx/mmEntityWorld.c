/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityWorld.h"

#include "core/mmAlloc.h"

MM_EXPORT_ENTITY
void
mmEntityWorld_Init(
    struct mmEntityWorld*                          p)
{
    mmEntityManager_Init(&p->entityManager);
    mmEntityComponentManager_Init(&p->componentManager);
    mmEntitySystemManager_Init(&p->systemManager);
    mmEntityMaskTuple_Init(&p->maskTuple);
    p->context = NULL;
}

MM_EXPORT_ENTITY
void
mmEntityWorld_Destroy(
    struct mmEntityWorld*                          p)
{
    p->context = NULL;
    mmEntityMaskTuple_Destroy(&p->maskTuple);
    mmEntitySystemManager_Destroy(&p->systemManager);
    mmEntityComponentManager_Destroy(&p->componentManager);
    mmEntityManager_Destroy(&p->entityManager);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_SetEntityChunkSize(
    struct mmEntityWorld*                          p,
    size_t                                         chunk_size)
{
    mmEntityManager_SetChunkSize(&p->entityManager, chunk_size);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_SetContext(
    struct mmEntityWorld*                          p,
    void*                                          context)
{
    p->context = context;
}

// clear entity component.
// note: this api will rmv the component data, and rmv the mask tuple component.
MM_EXPORT_ENTITY
void
mmEntityWorld_ClearEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity)
{
    const struct mmBitset* mask = &entity->mask;
    
    size_t n = 0;
    n = mmBitset_FindFirst(mask);
    while (MM_BITSET_NPOS != n)
    {
        mmEntityWorld_RmvEntityComponentById(p, entity, n);
        n = mmBitset_FindNext(mask, n);
    }
}

// clear all world entity.
MM_EXPORT_ENTITY
void
mmEntityWorld_ClearEntity(
    struct mmEntityWorld*                          p)
{
    struct mmEntity* entity = NULL;
    struct mmVectorVpt* entitys = &p->entityManager.entitys;

    size_t i = 0;
    size_t size = entitys->size;
    for (i = 0; i < size; ++i)
    {
        entity = (struct mmEntity*)mmVectorVpt_GetIndexReference(entitys, i);
        if (NULL != entity)
        {
            mmEntityWorld_ClearEntityComponent(p, entity);
        }
    }
    
    mmEntityManager_Clear(&p->entityManager);
    
    // world entity at entity manager.
    p->worldEntity = NULL;
}

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_AddSystem(
    struct mmEntityWorld*                          p,
    const char*                                    n,
    const struct mmEntitySystemMetadata*           m)
{
    struct mmEntitySystemType* s = mmEntitySystemManager_Add(&p->systemManager, n, m);
    mmEntityWorld_AttachEntitySystemEvent(p, s);
    return s;
}

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_GetSystem(
    const struct mmEntityWorld*                    p,
    const char*                                    n)
{
    return mmEntitySystemManager_Get(&p->systemManager, n);
}

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntityWorld_GetSystemById(
    const struct mmEntityWorld*                    p, 
    size_t                                         id)
{
    return mmEntitySystemManager_GetById(&p->systemManager, id);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_RmvSystem(
    struct mmEntityWorld*                          p,
    const char*                                    n)
{
    struct mmEntitySystemType* s = mmEntitySystemManager_Get(&p->systemManager, n);
    if (NULL != s)
    {
        mmEntityWorld_DetachEntitySystemEvent(p, s);
        mmEntitySystemManager_Rmv(&p->systemManager, n);
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_RmvSystemById(
    struct mmEntityWorld*                          p,
    size_t                                         id)
{
    struct mmEntitySystemType* s = mmEntitySystemManager_GetById(&p->systemManager, id);
    if (NULL != s)
    {
        mmEntityWorld_DetachEntitySystemEvent(p, s);
        mmEntitySystemManager_RmvById(&p->systemManager, id);
    }
}


// component registration name -> data_type pool.
MM_EXPORT_ENTITY
void
mmEntityWorld_AddComponent(
    struct mmEntityWorld*                          p,
    const char*                                    n,
    const struct mmEntityComponentMetadata*        m)
{
    mmEntityComponentManager_Add(&p->componentManager, n, m);
}

// component cancellation name -> data_type pool.
MM_EXPORT_ENTITY
void
mmEntityWorld_RmvComponent(
    struct mmEntityWorld*                          p,
    const char*                                    n)
{
    mmEntityComponentManager_Rmv(&p->componentManager, n);
}


// produce entity. 
MM_EXPORT_ENTITY
struct mmEntity*
mmEntityWorld_Produce(
    struct mmEntityWorld*                          p)
{
    return mmEntityManager_Produce(&p->entityManager);
}

// recycle entity. 
MM_EXPORT_ENTITY
void
mmEntityWorld_Recycle(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity)
{
    mmEntityWorld_ClearEntityComponent(p, entity);
    mmEntityManager_Recycle(&p->entityManager, entity);
}

// get entity by entity id. 
MM_EXPORT_ENTITY
struct mmEntity*
mmEntityWorld_GetEntity(
    const struct mmEntityWorld*                    p,
    mmEntityIdType                                 e)
{
    return mmEntityManager_Get(&p->entityManager, e);
}


// add component mask to system.
MM_EXPORT_ENTITY
void
mmEntityWorld_AddSystemComponentMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s,
    const char*                                    n)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_Get(&p->componentManager, n);
    if (NULL != c)
    {
        size_t id = c->family;
        mmBitset_UpdateSize(&s->mask, id);
        mmBitset_Set(&s->mask, id, 1);
    }
}

// rmv component mask to system.
MM_EXPORT_ENTITY
void
mmEntityWorld_RmvSystemComponentMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s,
    const char*                                    n)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_Get(&p->componentManager, n);
    if (NULL != c)
    {
        size_t id = c->family;
        mmBitset_UpdateSize(&s->mask, id);
        mmBitset_Set(&s->mask, id, 0);
    }
}

// add component mask to system.
MM_EXPORT_ENTITY
void
mmEntityWorld_AddSystemMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s)
{
    struct mmEntitySystemMetadata* metadata = &s->metadata;

    size_t i = 0;
    for (i = 0; i < metadata->TypesLength; ++i)
    {
        mmEntityWorld_AddSystemComponentMask(p, s, metadata->TypesArrays[i]);
    }
}

// rmv component mask to system.
MM_EXPORT_ENTITY
void
mmEntityWorld_RmvSystemMask(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s)
{
    struct mmEntitySystemMetadata* metadata = &s->metadata;

    size_t i = 0;
    for (i = 0; i < metadata->TypesLength; ++i)
    {
        mmEntityWorld_RmvSystemComponentMask(p, s, metadata->TypesArrays[i]);
    }
}

MM_EXPORT_ENTITY
void*
mmEntityWorld_AddEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    const char*                                    n)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_Get(&p->componentManager, n);
    if (NULL != c)
    {
        size_t id = 0;
        void* d = mmEntityComponentManager_Produce(&p->componentManager, n, &id);
        mmEntity_AttachComponent(entity, id, d);
        mmEntityWorld_AttachEntityComponentEvent(p, entity, c, d);
        return d;
    }
    else
    {
        return NULL;
    }
}

MM_EXPORT_ENTITY
void*
mmEntityWorld_AddEntityComponentById(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    size_t                                         id)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_GetById(&p->componentManager, id);
    if (NULL != c)
    {
        void* d = mmEntityComponentManager_ProduceById(&p->componentManager, id);
        mmEntity_AttachComponent(entity, id, d);
        mmEntityWorld_AttachEntityComponentEvent(p, entity, c, d);
        return d;
    }
    else
    {
        return NULL;
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_RmvEntityComponent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    const char*                                    n)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_Get(&p->componentManager, n);
    if (NULL != c)
    {
        size_t id = c->family;
        void* d = mmEntity_DetachComponent(entity, id);
        mmEntityWorld_DetachEntityComponentEvent(p, entity, c, d);
        mmEntityComponentManager_Recycle(&p->componentManager, n, d);
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_RmvEntityComponentById(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    size_t                                         id)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_GetById(&p->componentManager, id);
    if (NULL != c)
    {
        void* d = mmEntity_DetachComponent(entity, id);
        mmEntityWorld_DetachEntityComponentEvent(p, entity, c, d);
        mmEntityComponentManager_RecycleById(&p->componentManager, id, d);
    }
}

MM_EXPORT_ENTITY
void*
mmEntityWorld_GetEntityComponent(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    const char*                                    n)
{
    struct mmEntityComponentType* c = mmEntityComponentManager_Get(&p->componentManager, n);
    if (NULL != c)
    {
        size_t id = c->family;
        return mmEntity_GetComponent(entity, id);
    }
    else
    {
        return NULL;
    }
}

MM_EXPORT_ENTITY
void*
mmEntityWorld_GetEntityComponentById(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    size_t                                         id)
{
    return mmEntity_GetComponent(entity, id);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_GetEntityComponents(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    const char**                                   n,
    void**                                         c,
    size_t                                         z)
{
    size_t i = 0;
    for (i = 0; i < z; ++i)
    {
        c[i] = mmEntityWorld_GetEntityComponent(p, entity, n[i]);
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_GetEntityComponentsBySystem(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    struct mmEntitySystemType*                     s,
    void**                                         c)
{
    struct mmEntitySystemMetadata* metadata = &s->metadata;
    mmEntityWorld_GetEntityComponents(p, entity, metadata->TypesArrays, c, metadata->TypesLength);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_GetEntityComponentsBySystemName(
    const struct mmEntityWorld*                    p,
    struct mmEntity*                               entity,
    const char*                                    n,
    void**                                         c)
{
    struct mmEntitySystemType* s = mmEntitySystemManager_Get(&p->systemManager, n);
    if (NULL != s)
    {
        mmEntityWorld_GetEntityComponentsBySystem(p, entity, s, c);
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_AttachEntityMaskTuple(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity)
{
    mmEntityMaskTuple_AttachEntityMaskTuple(&p->maskTuple, entity);
}

MM_EXPORT_ENTITY
void
mmEntityWorld_DetachEntityMaskTuple(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity)
{
    mmEntityMaskTuple_DetachEntityMaskTuple(&p->maskTuple, entity);
}


MM_EXPORT_ENTITY
void
mmEntityWorld_AttachEntitySystemEvent(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s)
{
    struct mmRbtreeU32Vpt* tuples = NULL;

    if (NULL != s->metadata.Attach)
    {
        typedef void(*AttachFuncType)(struct mmEntityWorld* w, struct mmEntitySystemType* s, void* d);
        AttachFuncType Attach = (AttachFuncType)(s->metadata.Attach);
        (*(Attach))(p, s, s->element);
    }

    mmEntityWorld_AddSystemMask(p, s);
    tuples = mmEntityMaskTuple_AttachComponentMaskTuple(&p->maskTuple, &s->mask);

    s->tuples = tuples;
}

MM_EXPORT_ENTITY
void
mmEntityWorld_DetachEntitySystemEvent(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s)
{
    s->tuples = NULL;

    mmEntityMaskTuple_DetachComponentMaskTuple(&p->maskTuple, &s->mask);
    mmEntityWorld_RmvSystemMask(p, s);

    if (NULL != s->metadata.Detach)
    {
        typedef void(*AttachFuncType)(struct mmEntityWorld* w, struct mmEntitySystemType* s, void* d);
        AttachFuncType Detach = (AttachFuncType)(s->metadata.Detach);
        (*(Detach))(p, s, s->element);
    }
}

// attach entity component event. specialization mmEntityComponentType.
MM_EXPORT_ENTITY
void
mmEntityWorld_AttachEntityComponentEvent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    struct mmEntityComponentType*                  c,
    void*                                          d)
{
    if (NULL != c->metadata.Attach)
    {
        typedef void(*AttachFuncType)(struct mmEntityWorld* w, struct mmEntity* e, struct mmEntityComponentType* c, void* d);
        AttachFuncType Attach = (AttachFuncType)(c->metadata.Attach);
        (*(Attach))(p, entity, c, d);
    }
}

// detach entity component event. specialization mmEntityComponentType.
MM_EXPORT_ENTITY
void
mmEntityWorld_DetachEntityComponentEvent(
    struct mmEntityWorld*                          p,
    struct mmEntity*                               entity,
    struct mmEntityComponentType*                  c,
    void*                                          d)
{
    if (NULL != c->metadata.Detach)
    {
        typedef void(*DetachFuncType)(struct mmEntityWorld* w, struct mmEntity* e, struct mmEntityComponentType* c, void* d);
        DetachFuncType Detach = (DetachFuncType)(c->metadata.Detach);
        (*(Detach))(p, entity, c, d);
    }
}

MM_EXPORT_ENTITY
void
mmEntityWorld_UpdateSystem(
    struct mmEntityWorld*                          p,
    struct mmEntitySystemType*                     s,
    void*                                          evt,
    void*                                          func)
{
    typedef void(*UpdateFuncType)(
        struct mmEntityWorld*      w,
        struct mmEntitySystemType* s,
        struct mmEntity*           entity,
        void**                     c, 
        void*                      evt);
    
    const struct mmRbtreeU32Vpt* tuples = s->tuples;
    UpdateFuncType Update = (UpdateFuncType)func;
    void** c = mmMalloc(sizeof(void*) * s->metadata.TypesLength);
    struct mmEntity* entity = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmRbNode* n = NULL;
    n = mmRb_First(&tuples->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        entity = (struct mmEntity*)it->v;

        mmEntityWorld_GetEntityComponentsBySystem(p, entity, s, c);

        (*(Update))(p, s, entity, c, evt);
    }
    mmFree(c);
}
