/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntitySystem_h__
#define __mmEntitySystem_h__

#include "core/mmCore.h"

#include "container/mmBitset.h"

#include "entityx/mmEntityMaskTuple.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntitySystemMetadata
{
    // component type array.
    const char** TypesArrays;

    // component type array length.
    size_t TypesLength;

    // element size. sizeof(Type).
    size_t ElementSize;

    // typedef void(*ProduceFuncType)(void* e);
    void* Produce;

    // typedef void(*RecycleFuncType)(void* e);
    void* Recycle;

    // typedef void(*AttachFuncType)(struct mmEntityWorld* w, struct mmEntitySystemType* s, void* d);
    void* Attach;

    // typedef void(*DetachFuncType)(struct mmEntityWorld* w, struct mmEntitySystemType* s, void* d);
    void* Detach;
};

MM_EXPORT_ENTITY 
void 
mmEntitySystemMetadata_Reset(
    struct mmEntitySystemMetadata*                 p);

struct mmEntitySystemType
{
    size_t family;
    struct mmString name;
    void* element;

    struct mmEntitySystemMetadata metadata;

    struct mmBitset mask;
    const struct mmRbtreeU32Vpt* tuples;
};

MM_EXPORT_ENTITY 
void 
mmEntitySystemType_Init(
    struct mmEntitySystemType*                     p);

MM_EXPORT_ENTITY 
void 
mmEntitySystemType_Destroy(
    struct mmEntitySystemType*                     p);

MM_EXPORT_ENTITY 
void 
mmEntitySystemType_Assign(
    struct mmEntitySystemType*                     p, 
    const char*                                    name, 
    size_t                                         family);

MM_EXPORT_ENTITY 
void 
mmEntitySystemType_Prepare(
    struct mmEntitySystemType*                     p, 
    const struct mmEntitySystemMetadata*           m);

MM_EXPORT_ENTITY 
void 
mmEntitySystemType_Discard(
    struct mmEntitySystemType*                     p);

#include "core/mmSuffix.h"

#endif//__mmEntitySystem_h__
