/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntitySystemManager_h__
#define __mmEntitySystemManager_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeString.h"

#include "entityx/mmEntitySystem.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntitySystemManager
{
    struct mmRbtreeU64Vpt systems;
    struct mmRbtreeStringVpt names;
    size_t familys;
};

MM_EXPORT_ENTITY 
void 
mmEntitySystemManager_Init(
    struct mmEntitySystemManager*                  p);

MM_EXPORT_ENTITY 
void 
mmEntitySystemManager_Destroy(
    struct mmEntitySystemManager*                  p);

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntitySystemManager_Add(
    struct mmEntitySystemManager*                  p,
    const char*                                    n, 
    const struct mmEntitySystemMetadata*           m);

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntitySystemManager_GetById(
    const struct mmEntitySystemManager*            p,
    size_t                                         id);

MM_EXPORT_ENTITY 
struct mmEntitySystemType* 
mmEntitySystemManager_Get(
    const struct mmEntitySystemManager*            p,
    const char*                                    n);

MM_EXPORT_ENTITY 
void 
mmEntitySystemManager_RmvById(
    struct mmEntitySystemManager*                  p,
    size_t                                         id);

MM_EXPORT_ENTITY 
void 
mmEntitySystemManager_Rmv(
    struct mmEntitySystemManager*                  p,
    const char*                                    n);

MM_EXPORT_ENTITY 
void 
mmEntitySystemManager_Clear(
    struct mmEntitySystemManager*                  p);

#include "core/mmSuffix.h"

#endif//__mmEntitySystemManager_h__
