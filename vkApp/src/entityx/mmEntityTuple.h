/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityTuple_h__
#define __mmEntityTuple_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU64.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntityTuple
{
    struct mmRbtreeU64Vpt components;
};

MM_EXPORT_ENTITY 
void 
mmEntityTuple_Init(
    struct mmEntityTuple*                          p);

MM_EXPORT_ENTITY 
void 
mmEntityTuple_Destroy(
    struct mmEntityTuple*                          p);

MM_EXPORT_ENTITY 
void 
mmEntityTuple_Set(
    struct mmEntityTuple*                          p, 
    size_t                                         id, 
    void*                                          d);

MM_EXPORT_ENTITY 
void* 
mmEntityTuple_Rmv(
    struct mmEntityTuple*                          p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
void* 
mmEntityTuple_Get(
    const struct mmEntityTuple*                    p, 
    size_t                                         id);

MM_EXPORT_ENTITY 
void 
mmEntityTuple_Clear(
    struct mmEntityTuple*                          p);

#include "core/mmSuffix.h"

#endif//__mmEntityTuple_h__
