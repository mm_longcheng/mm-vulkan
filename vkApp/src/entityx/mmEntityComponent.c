/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityComponent.h"

#include "core/mmAlloc.h"

MM_EXPORT_ENTITY
void
mmEntityComponentMetadata_Reset(
    struct mmEntityComponentMetadata*              p)
{
    mmMemset(p, 0, sizeof(struct mmEntityComponentMetadata));
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Init(
    struct mmEntityComponentType*                  p)
{
    p->family = 0;
    mmString_Init(&p->name);
    mmPoolElement_Init(&p->pool);

    mmEntityComponentMetadata_Reset(&p->metadata);
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Destroy(
    struct mmEntityComponentType*                  p)
{
    mmEntityComponentMetadata_Reset(&p->metadata);

    mmPoolElement_Destroy(&p->pool);
    mmString_Destroy(&p->name);
    p->family = 0;
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Reset(
    struct mmEntityComponentType*                  p)
{
    p->family = 0;
    mmString_Reset(&p->name);
    mmPoolElement_ClearChunk(&p->pool);
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Assign(
    struct mmEntityComponentType*                  p,
    const char*                                    name,
    size_t                                         family)
{
    mmString_Assigns(&p->name, name);
    p->family = family;
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Prepare(
    struct mmEntityComponentType*                  p,
    const struct mmEntityComponentMetadata*        m)
{
    struct mmPoolElementAllocator hAllocator;

    const struct mmEntityComponentMetadata* metadata = m;

    hAllocator.Produce = metadata->Produce;
    hAllocator.Recycle = metadata->Recycle;
    hAllocator.obj = p;

    mmPoolElement_SetElementSize(&p->pool, metadata->ElementSize);
    mmPoolElement_SetChunkSize(&p->pool, metadata->ChunkSize);
    mmPoolElement_SetAllocator(&p->pool, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool);

    p->metadata = *metadata;
}

MM_EXPORT_ENTITY
void
mmEntityComponentType_Discard(
    struct mmEntityComponentType*                  p)
{
    mmPoolElement_ClearChunk(&p->pool);
    mmEntityComponentMetadata_Reset(&p->metadata);
}
