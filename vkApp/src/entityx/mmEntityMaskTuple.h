/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityMaskTuple_h__
#define __mmEntityMaskTuple_h__

#include "core/mmCore.h"

#include "container/mmBitset.h"
#include "container/mmRbtreeU32.h"
#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeBitset.h"
#include "container/mmRbtsetBitset.h"

#include "dish/mmIdGenerater.h"

#include "entityx/mmEntity.h"
#include "entityx/mmEntityTuple.h"

#include "entityx/mmEntityExport.h"

#include "core/mmPrefix.h"

struct mmEntityMaskTuple
{
    // mask -> entity tuple.
    // entity mask tuple
    struct mmRbtreeBitsetVpt entitys;

    // id -> mask set.
    // component mask tuple
    struct mmRbtreeU64Vpt components;
};

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_Init(
    struct mmEntityMaskTuple*                      p);

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_Destroy(
    struct mmEntityMaskTuple*                      p);

// add component to component_mask_tuple immediately.
MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_AddComponentMaskTupleComponent(
    struct mmEntityMaskTuple*                      p, 
    const struct mmBitset*                         mask, 
    size_t                                         n);

// rmv component to component_mask_tuple immediately.
MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_RmvComponentMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask, 
    size_t                                         n);

// attach mask.
MM_EXPORT_ENTITY 
struct mmRbtreeU32Vpt* 
mmEntityMaskTuple_AttachComponentMaskTuple(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask);

// detach mask.
MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_DetachComponentMaskTuple(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask);

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_AddEntityMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity, 
    size_t                                         i);

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_RmvEntityMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity, 
    size_t                                         i);

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_AttachEntityMaskTuple(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity);

MM_EXPORT_ENTITY 
void 
mmEntityMaskTuple_DetachEntityMaskTuple(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity);

#include "core/mmSuffix.h"

#endif//__mmEntityMaskTuple_h__
