/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityManager.h"

MM_EXPORT_ENTITY
void
mmEntityManager_Init(
    struct mmEntityManager*                        p)
{
    struct mmPoolElementAllocator hAllocator;

    mmUInt32IdGenerater_Init(&p->generater);
    mmPoolElement_Init(&p->pool);
    mmVectorVpt_Init(&p->entitys);

    hAllocator.Produce = &mmEntity_Init;
    hAllocator.Recycle = &mmEntity_Destroy;
    hAllocator.obj = p;

    mmPoolElement_SetElementSize(&p->pool, sizeof(struct mmEntity));
    mmPoolElement_SetChunkSize(&p->pool, MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT);
    mmPoolElement_SetAllocator(&p->pool, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool);
}

MM_EXPORT_ENTITY
void
mmEntityManager_Destroy(
    struct mmEntityManager*                        p)
{
    mmVectorVpt_Destroy(&p->entitys);
    mmPoolElement_Destroy(&p->pool);
    mmUInt32IdGenerater_Destroy(&p->generater);
}

MM_EXPORT_ENTITY
void
mmEntityManager_SetChunkSize(
    struct mmEntityManager*                        p,
    size_t                                         chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool);
}

MM_EXPORT_ENTITY
struct mmEntity*
mmEntityManager_Produce(
    struct mmEntityManager*                        p)
{
    struct mmEntity* entity = NULL;
    mmEntityIdType e = mmUInt32IdGenerater_Produce(&p->generater);
    if (e < p->entitys.size)
    {
        entity = (struct mmEntity*)mmPoolElement_Produce(&p->pool);
        mmVectorVpt_SetIndex(&p->entitys, e, entity);
        mmEntity_SetId(entity, e);
    }
    else
    {
        size_t capacity = 0;
        entity = (struct mmEntity*)mmPoolElement_Produce(&p->pool);
        capacity = mmPoolElement_Capacity(&p->pool);
        mmVectorVpt_Resize(&p->entitys, capacity);
        mmVectorVpt_SetIndex(&p->entitys, e, entity);
        mmEntity_SetId(entity, e);
    }
    return entity;
}

MM_EXPORT_ENTITY
void
mmEntityManager_Recycle(
    struct mmEntityManager*                        p,
    struct mmEntity*                               entity)
{
    mmUInt32_t id_counter = 0;
    size_t capacity = 0;
    size_t size = 0;
    size_t shrink_size = 0;

    mmEntityIdType e = mmEntity_GetId(entity);
    mmUInt32IdGenerater_Recycle(&p->generater, e);
    mmPoolElement_Recycle(&p->pool, entity);

    id_counter = p->generater.id_counter;
    capacity = mmPoolElement_Capacity(&p->pool);
    size = p->entitys.size;
    shrink_size = capacity > id_counter ? capacity : id_counter;
    if (shrink_size < size)
    {
        mmVectorVpt_Resize(&p->entitys, shrink_size);
    }
    else
    {
        mmVectorVpt_SetIndex(&p->entitys, e, NULL);
    }
}

MM_EXPORT_ENTITY
struct mmEntity*
mmEntityManager_Get(
    const struct mmEntityManager*                  p,
    mmEntityIdType                                 e)
{
    assert(e < p->entitys.size && "e < p->entitys.size is invalid.");
    return mmVectorVpt_At(&p->entitys, e);
}

MM_EXPORT_ENTITY
void
mmEntityManager_Clear(
    struct mmEntityManager*                        p)
{
    struct mmEntity* entity = NULL;
    size_t i = 0;
    size_t size = p->entitys.size;
    for (i = 0; i < size; ++i)
    {
        entity = (struct mmEntity*)mmVectorVpt_GetIndexReference(&p->entitys, i);
        if (NULL != entity)
        {
            mmEntityIdType e = mmEntity_GetId(entity);
            mmUInt32IdGenerater_Recycle(&p->generater, e);
            mmPoolElement_Recycle(&p->pool, entity);
        }
    }

    mmVectorVpt_Reset(&p->entitys);
}

