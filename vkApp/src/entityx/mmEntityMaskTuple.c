/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityMaskTuple.h"

#include "core/mmAlloc.h"

static 
void* 
mmRbtreeBitsetVpt_RbtreeU32VptProduce(
    struct mmRbtreeBitsetVpt*                      p, 
    const struct mmBitset*                         k)
{
    struct mmRbtreeU32Vpt* e = (struct mmRbtreeU32Vpt*)mmMalloc(sizeof(struct mmRbtreeU32Vpt));
    mmRbtreeU32Vpt_Init(e);
    return e;
}

static 
void* 
mmRbtreeBitsetVpt_RbtreeU32VptRecycle(
    struct mmRbtreeBitsetVpt*                      p, 
    const struct mmBitset*                         k, 
    void*                                          v)
{
    struct mmRbtreeU32Vpt* e = (struct mmRbtreeU32Vpt*)(v);
    mmRbtreeU32Vpt_Destroy(e);
    mmFree(e);
    return NULL;
}

static 
void* 
mmRbtreeU64Vpt_RbtsetBitsetProduce(
    struct mmRbtreeU64Vpt*                         p, 
    mmUInt64_t                                     k)
{
    struct mmRbtsetBitset* e = (struct mmRbtsetBitset*)mmMalloc(sizeof(struct mmRbtsetBitset));
    mmRbtsetBitset_Init(e);
    return e;
}

static 
void* 
mmRbtreeU64Vpt_RbtsetBitsetRecycle(
    struct mmRbtreeU64Vpt*                         p, 
    mmUInt64_t                                     k, 
    void*                                          v)
{
    struct mmRbtsetBitset* e = (struct mmRbtsetBitset*)(v);
    mmRbtsetBitset_Destroy(e);
    mmFree(e);
    return NULL;
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_Init(
    struct mmEntityMaskTuple*                      p)
{
    struct mmRbtreeBitsetVptAllocator hAllocator0;
    struct mmRbtreeU64VptAllocator hAllocator1;

    mmRbtreeBitsetVpt_Init(&p->entitys);
    mmRbtreeU64Vpt_Init(&p->components);

    hAllocator0.Produce = &mmRbtreeBitsetVpt_RbtreeU32VptProduce;
    hAllocator0.Recycle = &mmRbtreeBitsetVpt_RbtreeU32VptRecycle;
    mmRbtreeBitsetVpt_SetAllocator(&p->entitys, &hAllocator0);

    hAllocator1.Produce = &mmRbtreeU64Vpt_RbtsetBitsetProduce;
    hAllocator1.Recycle = &mmRbtreeU64Vpt_RbtsetBitsetRecycle;
    mmRbtreeU64Vpt_SetAllocator(&p->components, &hAllocator1);
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_Destroy(
    struct mmEntityMaskTuple*                      p)
{
    mmRbtreeU64Vpt_Destroy(&p->components);
    mmRbtreeBitsetVpt_Destroy(&p->entitys);
}

// add component to component_mask_tuple immediately.
MM_EXPORT_ENTITY
void
mmEntityMaskTuple_AddComponentMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask,
    size_t                                         n)
{
    struct mmRbtsetBitset* masks = mmRbtreeU64Vpt_GetInstance(&p->components, n);
    mmRbtsetBitset_Add(masks, mask);
}

// rmv component to component_mask_tuple immediately.
MM_EXPORT_ENTITY
void
mmEntityMaskTuple_RmvComponentMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask,
    size_t                                         n)
{
    struct mmRbtsetBitset* masks = mmRbtreeU64Vpt_GetInstance(&p->components, n);
    mmRbtsetBitset_Rmv(masks, mask);
}

// attach mask.
MM_EXPORT_ENTITY 
struct mmRbtreeU32Vpt* 
mmEntityMaskTuple_AttachComponentMaskTuple(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask)
{
    struct mmRbtreeU32Vpt* e = mmRbtreeBitsetVpt_GetInstance(&p->entitys, mask);

    size_t n = 0;
    n = mmBitset_FindFirst(mask);
    while (MM_BITSET_NPOS != n)
    {
        mmEntityMaskTuple_AddComponentMaskTupleComponent(p, mask, n);
        n = mmBitset_FindNext(mask, n);
    }

    return e;
}

// detach mask.
MM_EXPORT_ENTITY
void
mmEntityMaskTuple_DetachComponentMaskTuple(
    struct mmEntityMaskTuple*                      p,
    const struct mmBitset*                         mask)
{
    mmRbtreeBitsetVpt_Rmv(&p->entitys, mask);

    size_t n = 0;
    n = mmBitset_FindFirst(mask);
    while (MM_BITSET_NPOS != n)
    {
        mmEntityMaskTuple_RmvComponentMaskTupleComponent(p, mask, n);
        n = mmBitset_FindNext(mask, n);
    }
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_AddEntityMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity,
    size_t                                         i)
{
    const struct mmBitset* mask = &entity->mask;

    struct mmRbtsetBitset* masks = mmRbtreeU64Vpt_GetInstance(&p->components, i);

    struct mmBitset* e = NULL;
    struct mmRbtsetBitsetIterator* it = NULL;
    struct mmRbNode* n = NULL;
    n = mmRb_First(&masks->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetBitsetIterator, n);
        n = mmRb_Next(n);
        e = &it->k;

        if (mmBitset_IsSubsetOf(e, mask))
        {
            struct mmRbtreeU32Vpt* tuples = (struct mmRbtreeU32Vpt*)mmRbtreeBitsetVpt_GetInstance(&p->entitys, e);
            mmRbtreeU32Vpt_Set(tuples, entity->id, entity);
        }
    }
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_RmvEntityMaskTupleComponent(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity,
    size_t                                         i)
{
    const struct mmBitset* mask = &entity->mask;

    struct mmRbtsetBitset* masks = mmRbtreeU64Vpt_GetInstance(&p->components, i);

    struct mmBitset* e = NULL;
    struct mmRbtsetBitsetIterator* it = NULL;
    struct mmRbNode* n = NULL;
    n = mmRb_First(&masks->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetBitsetIterator, n);
        n = mmRb_Next(n);
        e = &it->k;

        if (mmBitset_IsSubsetOf(e, mask))
        {
            struct mmRbtreeU32Vpt* tuples = (struct mmRbtreeU32Vpt*)mmRbtreeBitsetVpt_GetInstance(&p->entitys, e);
            mmRbtreeU32Vpt_Rmv(tuples, entity->id);
        }
    }
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_AttachEntityMaskTuple(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity)
{
    const struct mmBitset* mask = &entity->mask;

    size_t n = 0;
    n = mmBitset_FindFirst(mask);
    while (MM_BITSET_NPOS != n)
    {
        mmEntityMaskTuple_AddEntityMaskTupleComponent(p, entity, n);
        n = mmBitset_FindNext(mask, n);
    }
}

MM_EXPORT_ENTITY
void
mmEntityMaskTuple_DetachEntityMaskTuple(
    struct mmEntityMaskTuple*                      p,
    struct mmEntity*                               entity)
{
    const struct mmBitset* mask = &entity->mask;

    size_t n = 0;
    n = mmBitset_FindFirst(mask);
    while (MM_BITSET_NPOS != n)
    {
        mmEntityMaskTuple_RmvEntityMaskTupleComponent(p, entity, n);
        n = mmBitset_FindNext(mask, n);
    }
}

