/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntitySystem.h"

#include "core/mmAlloc.h"

MM_EXPORT_ENTITY
void
mmEntitySystemMetadata_Reset(
    struct mmEntitySystemMetadata*                 p)
{
    mmMemset(p, 0, sizeof(struct mmEntitySystemMetadata));
}

MM_EXPORT_ENTITY
void
mmEntitySystemType_Init(
    struct mmEntitySystemType*                     p)
{
    p->family = 0;
    mmString_Init(&p->name);
    p->element = NULL;

    mmEntitySystemMetadata_Reset(&p->metadata);

    mmBitset_Init(&p->mask);
    p->tuples = NULL;
}

MM_EXPORT_ENTITY
void
mmEntitySystemType_Destroy(
    struct mmEntitySystemType*                     p)
{
    p->tuples = NULL;
    mmBitset_Destroy(&p->mask);

    mmEntitySystemMetadata_Reset(&p->metadata);

    p->element = NULL;
    mmString_Destroy(&p->name);
    p->family = 0;
}

MM_EXPORT_ENTITY
void
mmEntitySystemType_Assign(
    struct mmEntitySystemType*                     p,
    const char*                                    name,
    size_t                                         family)
{
    mmString_Assigns(&p->name, name);
    p->family = family;
}

MM_EXPORT_ENTITY
void
mmEntitySystemType_Prepare(
    struct mmEntitySystemType*                     p,
    const struct mmEntitySystemMetadata*           m)
{
    typedef void(*ProduceFuncType)(void* e);
    const struct mmEntitySystemMetadata* metadata = m;
    ProduceFuncType Produce = (ProduceFuncType)metadata->Produce;
    p->element = (void*)mmMalloc(metadata->ElementSize);
    (*(Produce))(p->element);
    p->metadata = *metadata;
}

MM_EXPORT_ENTITY
void
mmEntitySystemType_Discard(
    struct mmEntitySystemType*                     p)
{
    typedef void(*RecycleFuncType)(void* e);
    const struct mmEntitySystemMetadata* metadata = &p->metadata;
    RecycleFuncType Recycle = (RecycleFuncType)metadata->Recycle;
    (*(Recycle))(p->element);
    mmFree(p->element);
    p->element = NULL;
    mmEntitySystemMetadata_Reset(&p->metadata);
}
