/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntity.h"

const mmEntityIdType MM_ENTITY_INVALID_ID = (mmEntityIdType)(-1);

MM_EXPORT_ENTITY
void
mmEntity_Init(
    struct mmEntity*                               p)
{
    p->id = MM_ENTITY_INVALID_ID;
    mmBitset_Init(&p->mask);
    mmEntityTuple_Init(&p->tuple);
}

MM_EXPORT_ENTITY
void
mmEntity_Destroy(
    struct mmEntity*                               p)
{
    mmEntityTuple_Destroy(&p->tuple);
    mmBitset_Destroy(&p->mask);
    p->id = MM_ENTITY_INVALID_ID;
}

MM_EXPORT_ENTITY
void
mmEntity_SetId(
    struct mmEntity*                               p,
    const mmEntityIdType                           id)
{
    p->id = id;
}

MM_EXPORT_ENTITY
mmEntityIdType
mmEntity_GetId(
    const struct mmEntity*                         p)
{
    return p->id;
}

MM_EXPORT_ENTITY
void
mmEntity_AttachComponent(
    struct mmEntity*                               p,
    size_t                                         id,
    void*                                          d)
{
    mmBitset_UpdateSize(&p->mask, id);
    mmBitset_Set(&p->mask, id, 1);
    mmEntityTuple_Set(&p->tuple, id, d);
}

MM_EXPORT_ENTITY
void*
mmEntity_DetachComponent(
    struct mmEntity*                               p,
    size_t                                         id)
{
    mmBitset_UpdateSize(&p->mask, id);
    mmBitset_Set(&p->mask, id, 0);
    return mmEntityTuple_Rmv(&p->tuple, id);
}

MM_EXPORT_ENTITY
void*
mmEntity_GetComponent(
    const struct mmEntity*                         p,
    size_t                                         id)
{
    return mmEntityTuple_Get(&p->tuple, id);
}

