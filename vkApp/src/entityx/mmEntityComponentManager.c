/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityComponentManager.h"

#include "core/mmAlloc.h"

MM_EXPORT_ENTITY
void
mmEntityComponentManager_Init(
    struct mmEntityComponentManager*               p)
{
    mmRbtreeU64Vpt_Init(&p->pools);
    mmRbtreeStringVpt_Init(&p->names);
    p->familys = 0;
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_Destroy(
    struct mmEntityComponentManager*               p)
{
    mmRbtreeStringVpt_Destroy(&p->names);
    mmRbtreeU64Vpt_Destroy(&p->pools);
    p->familys = 0;
}

MM_EXPORT_ENTITY
struct mmEntityComponentType*
mmEntityComponentManager_Add(
    struct mmEntityComponentManager*               p,
    const char*                                    n,
    const struct mmEntityComponentMetadata*        m)
{
    struct mmEntityComponentType* e = NULL;

    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    it = mmRbtreeStringVpt_GetIterator(&p->names, &hWeakKey);
    if (NULL == it)
    {
        e = (struct mmEntityComponentType*)mmMalloc(sizeof(struct mmEntityComponentType));
        mmEntityComponentType_Init(e);
        mmEntityComponentType_Assign(e, n, p->familys++);
        mmEntityComponentType_Prepare(e, m);
        mmRbtreeU64Vpt_Set(&p->pools, (mmUInt64_t)e->family, e);
        mmRbtreeStringVpt_Set(&p->names, &hWeakKey, e);
    }
    return e;
}

MM_EXPORT_ENTITY
struct mmEntityComponentType*
mmEntityComponentManager_GetById(
    const struct mmEntityComponentManager*         p,
    size_t                                         id)
{
    return (struct mmEntityComponentType*)mmRbtreeU64Vpt_Get(&p->pools, (mmUInt64_t)id);
}

MM_EXPORT_ENTITY
struct mmEntityComponentType*
mmEntityComponentManager_Get(
    const struct mmEntityComponentManager*         p,
    const char*                                    n)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    return (struct mmEntityComponentType*)mmRbtreeStringVpt_Get(&p->names, &hWeakKey);
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_RmvById(
    struct mmEntityComponentManager*               p,
    size_t                                         id)
{
    struct mmEntityComponentType* e = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    it = mmRbtreeU64Vpt_GetIterator(&p->pools, (mmUInt64_t)id);
    if (NULL != it)
    {
        e = (struct mmEntityComponentType*)it->v;
        mmRbtreeStringVpt_Rmv(&p->names, &e->name);
        mmRbtreeU64Vpt_Erase(&p->pools, it);
        mmEntityComponentType_Discard(e);
        mmEntityComponentType_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_Rmv(
    struct mmEntityComponentManager*               p,
    const char*                                    n)
{
    struct mmEntityComponentType* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, n);
    it = mmRbtreeStringVpt_GetIterator(&p->names, &hWeakKey);
    if (NULL != it)
    {
        e = (struct mmEntityComponentType*)it->v;
        mmRbtreeU64Vpt_Rmv(&p->pools, e->family);
        mmRbtreeStringVpt_Erase(&p->names, it);
        mmEntityComponentType_Discard(e);
        mmEntityComponentType_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_Clear(
    struct mmEntityComponentManager*               p)
{
    struct mmEntityComponentType* e = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    struct mmRbNode* n = NULL;
    n = mmRb_First(&p->pools.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmEntityComponentType*)it->v;
        mmRbtreeStringVpt_Rmv(&p->names, &e->name);
        mmRbtreeU64Vpt_Erase(&p->pools, it);
        mmEntityComponentType_Discard(e);
        mmEntityComponentType_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_ENTITY
void*
mmEntityComponentManager_ProduceById(
    struct mmEntityComponentManager*               p,
    size_t                                         id)
{
    void* v = NULL;
    struct mmEntityComponentType* e = mmEntityComponentManager_GetById(p, id);
    assert(NULL != e && "can not find valid pool type.");
    v = mmPoolElement_Produce(&e->pool);
    assert(NULL != v && "memory not enough, can not alloc data.");
    return v;
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_RecycleById(
    struct mmEntityComponentManager*               p,
    size_t                                         id,
    void*                                          v)
{
    struct mmEntityComponentType* e = mmEntityComponentManager_GetById(p, id);
    assert(NULL != e && "can not find valid pool type.");
    mmPoolElement_Recycle(&e->pool, v);
}

MM_EXPORT_ENTITY
void*
mmEntityComponentManager_Produce(
    struct mmEntityComponentManager*               p,
    const char*                                    n,
    size_t*                                        id)
{
    void* v = NULL;
    struct mmEntityComponentType* e = mmEntityComponentManager_Get(p, n);
    assert(NULL != e && "can not find valid pool type.");
    v = mmPoolElement_Produce(&e->pool);
    assert(NULL != v && "memory not enough, can not alloc data.");
    *id = e->family;
    return v;
}

MM_EXPORT_ENTITY
void
mmEntityComponentManager_Recycle(
    struct mmEntityComponentManager*               p,
    const char*                                    n,
    void*                                          v)
{
    struct mmEntityComponentType* e = mmEntityComponentManager_Get(p, n);
    assert(NULL != e && "can not find valid pool type.");
    mmPoolElement_Recycle(&e->pool, v);
}
