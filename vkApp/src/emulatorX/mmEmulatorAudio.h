/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorAudio_h__
#define __mmEmulatorAudio_h__

#include "core/mmCore.h"

#include "container/mmRbtsetU32.h"

#include "emu/mmEmuEmulator.h"

#include "al/mmALPrereqs.h"

#include "emulatorX/mmEmulatorExport.h"

#include "core/mmPrefix.h"

#define MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT 0.25

#define MM_EMULATOR_AUDIO_ACCUMULATION_DEFAULT 100

struct mmEmulatorAudio
{
    // strong ref.
    struct mmRbtsetU32 pool;
    // weak ref.
    struct mmRbtsetU32 used;
    // weak ref.
    struct mmRbtsetU32 idle;

    // trim factor border.
    float hRateTrimBorder;
    // trim factor number.
    float hRateTrimNumber;

    // accumulation limit default 100.
    int accumulation;

    // mute or not.
    mmBool_t mute;

    // background status.
    mmBool_t background;

    // audio Volume. default is 1.0f.
    float hVolume;

    // SampleRate. default 22050.
    int nRate;

    // OpenAL format
    ALenum nFormat;

    // OpenAL Source
    ALuint source;
};

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Init(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Destroy(
    struct mmEmulatorAudio*                        p);


MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetRate(
    struct mmEmulatorAudio*                        p,
    int                                            nRate);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetFormat(
    struct mmEmulatorAudio*                        p,
    ALenum                                         nFormat);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetMute(
    struct mmEmulatorAudio*                        p,
    mmBool_t                                       mute);

MM_EXPORT_EMULATOR
int
mmEmulatorAudio_Prepare(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Discard(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_EnterBackground(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_EnterForeground(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetVolume(
    struct mmEmulatorAudio*                        p,
    float                                          hVolume);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Play(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Stop(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Pause(
    struct mmEmulatorAudio*                        p);

MM_EXPORT_EMULATOR
int
mmEmulatorAudio_ProcessBuffer(
    struct mmEmulatorAudio*                        p,
    mmByte_t*                                      buffer,
    size_t                                         length);

#include "core/mmSuffix.h"

#endif//__mmEmulatorAudio_h__
