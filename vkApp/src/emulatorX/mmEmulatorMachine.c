/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorMachine.h"

#include "core/mmFilePath.h"
#include "core/mmBinarySerach.h"

#include "nwsi/mmContextMaster.h"

#include "emu/mmEmuNes.h"

MM_EXPORT_EMULATOR
void
mmEmulatorMachineEventArgs_Init(
    struct mmEmulatorMachineEventArgs*             p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->window = NULL;
    p->mid = 0;
    p->p0 = 0;
    p->p1 = 0;
    mmString_Init(&p->ps);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachineEventArgs_Destroy(
    struct mmEmulatorMachineEventArgs*             p)
{
    mmString_Destroy(&p->ps);
    p->p1 = 0;
    p->p0 = 0;
    p->mid = 0;
    p->window = NULL;
    mmEventArgs_Reset(&p->hSuper);
}

MM_EXPORT_EMULATOR
void
mmEventEmulatorArgs_Init(
    struct mmEventEmulatorArgs*                    p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->mid = 0;
    p->p0 = 0;
    p->p1 = 0;
    mmString_Init(&p->ps);
}

MM_EXPORT_EMULATOR
void
mmEventEmulatorArgs_Destroy(
    struct mmEventEmulatorArgs*                    p)
{
    mmString_Destroy(&p->ps);
    p->p1 = 0;
    p->p0 = 0;
    p->mid = 0;
    mmEventArgs_Reset(&p->hSuper);
}

MM_EXPORT_EMULATOR const char* Event_MM_EMU_UNKNOWN = "Event_MM_EMU_UNKNOWN";

MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_EXIT = "Event_MM_EMU_RS_EXIT";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_NONE = "Event_MM_EMU_RS_NONE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_INITIAL = "Event_MM_EMU_RS_INITIAL";

MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_PLAY = "Event_MM_EMU_RS_PLAY";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_STOP = "Event_MM_EMU_RS_STOP";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_PAUSE = "Event_MM_EMU_RS_PAUSE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_RESUME = "Event_MM_EMU_RS_RESUME";

MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_VIDEOMODE = "Event_MM_EMU_RS_VIDEOMODE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_LOADROM = "Event_MM_EMU_RS_LOADROM";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_SCREENCLEAR = "Event_MM_EMU_RS_SCREENCLEAR";
// For Logger
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_SETMESSAGESTRING = "Event_MM_EMU_RS_SETMESSAGESTRING";

MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_FULLSCREEN_GDI = "Event_MM_EMU_RS_FULLSCREEN_GDI";
//
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_EMUPAUSE = "Event_MM_EMU_RS_EMUPAUSE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_ONEFRAME = "Event_MM_EMU_RS_ONEFRAME";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_THROTTLE = "Event_MM_EMU_RS_THROTTLE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_FRAMESKIP_AUTO = "Event_MM_EMU_RS_FRAMESKIP_AUTO";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_FRAMESKIP_UP = "Event_MM_EMU_RS_FRAMESKIP_UP";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_FRAMESKIP_DOWN = "Event_MM_EMU_RS_FRAMESKIP_DOWN";
//
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_HWRESET = "Event_MM_EMU_RS_HWRESET";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_SWRESET = "Event_MM_EMU_RS_SWRESET";
//
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_NETPLAY_START = "Event_MM_EMU_RS_NETPLAY_START";
//
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_STATE_LOAD = "Event_MM_EMU_RS_STATE_LOAD";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_STATE_SAVE = "Event_MM_EMU_RS_STATE_SAVE";
//
// For Disk system
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_DISK_COMMAND = "Event_MM_EMU_RS_DISK_COMMAND";
// For ExController
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_EXCONTROLLER = "Event_MM_EMU_RS_EXCONTROLLER";
// For Sound
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_SOUND_MUTE = "Event_MM_EMU_RS_SOUND_MUTE";
//
// For Snapshot
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_SNAPSHOT = "Event_MM_EMU_RS_SNAPSHOT";
// For Movie
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_MOVIE_PLAY = "Event_MM_EMU_RS_MOVIE_PLAY";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_MOVIE_REC = "Event_MM_EMU_RS_MOVIE_REC";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_MOVIE_RECAPPEND = "Event_MM_EMU_RS_MOVIE_RECAPPEND";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_MOVIE_STOP = "Event_MM_EMU_RS_MOVIE_STOP";
//
// For Wave recording
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_WAVEREC_START = "Event_MM_EMU_RS_WAVEREC_START";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_WAVEREC_STOP = "Event_MM_EMU_RS_WAVEREC_STOP";
// For Tape recording
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_TAPE_PLAY = "Event_MM_EMU_RS_TAPE_PLAY";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_TAPE_REC = "Event_MM_EMU_RS_TAPE_REC";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_TAPE_STOP = "Event_MM_EMU_RS_TAPE_STOP";
//
// For Barcode
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_BARCODE = "Event_MM_EMU_RS_BARCODE";
//
// For TurboFile
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_TURBOFILE = "Event_MM_EMU_RS_TURBOFILE";
//
// For Debugger
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_DEBUG_RUN = "Event_MM_EMU_RS_DEBUG_RUN";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_DEBUG_BRAKE = "Event_MM_EMU_RS_DEBUG_BRAKE";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_DEBUG_STEP = "Event_MM_EMU_RS_DEBUG_STEP";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_RS_DEBUG_COMMAND = "Event_MM_EMU_RS_DEBUG_COMMAND";

MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_NONE = "Event_MM_EMU_NT_NONE";
//
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_PLAY = "Event_MM_EMU_NT_PLAY";
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_STOP = "Event_MM_EMU_NT_STOP";
// For Screen
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_SCREENMESSAGE = "Event_MM_EMU_NT_SCREENMESSAGE";
// For Movie
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_MOVIE_FINISH = "Event_MM_EMU_NT_MOVIE_FINISH";
// For VideoMode
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_VIDEOMODE = "Event_MM_EMU_NT_VIDEOMODE";
// For ExController
MM_EXPORT_EMULATOR const char* Event_MM_EMU_NT_EXCONTROLLER = "Event_MM_EMU_NT_EXCONTROLLER";

MM_EXPORT_EMULATOR const struct mmEmulatorEventMidName MM_EMULATOR_EVENT_NAME[] =
{
    { MM_EMU_RS_EXIT            , "Event_MM_EMU_RS_EXIT"             , },
    { MM_EMU_RS_NONE            , "Event_MM_EMU_RS_NONE"             , },
    { MM_EMU_RS_INITIAL         , "Event_MM_EMU_RS_INITIAL"          , },

    { MM_EMU_RS_PLAY            , "Event_MM_EMU_RS_PLAY"             , },
    { MM_EMU_RS_STOP            , "Event_MM_EMU_RS_STOP"             , },
    { MM_EMU_RS_PAUSE           , "Event_MM_EMU_RS_PAUSE"            , },
    { MM_EMU_RS_RESUME          , "Event_MM_EMU_RS_RESUME"           , },

    { MM_EMU_RS_VIDEOMODE       , "Event_MM_EMU_RS_VIDEOMODE"        , },
    { MM_EMU_RS_LOADROM         , "Event_MM_EMU_RS_LOADROM"          , },
    { MM_EMU_RS_SCREENCLEAR     , "Event_MM_EMU_RS_SCREENCLEAR"      , },
    // For Logger
    { MM_EMU_RS_SETMESSAGESTRING, "Event_MM_EMU_RS_SETMESSAGESTRING" , },

    { MM_EMU_RS_FULLSCREEN_GDI  , "Event_MM_EMU_RS_FULLSCREEN_GDI"   , },
    //
    { MM_EMU_RS_EMUPAUSE        , "Event_MM_EMU_RS_EMUPAUSE"         , },
    { MM_EMU_RS_ONEFRAME        , "Event_MM_EMU_RS_ONEFRAME"         , },
    { MM_EMU_RS_THROTTLE        , "Event_MM_EMU_RS_THROTTLE"         , },
    { MM_EMU_RS_FRAMESKIP_AUTO  , "Event_MM_EMU_RS_FRAMESKIP_AUTO"   , },
    { MM_EMU_RS_FRAMESKIP_UP    , "Event_MM_EMU_RS_FRAMESKIP_UP"     , },
    { MM_EMU_RS_FRAMESKIP_DOWN  , "Event_MM_EMU_RS_FRAMESKIP_DOWN"   , },
    //
    { MM_EMU_RS_HWRESET         , "Event_MM_EMU_RS_HWRESET"          , },
    { MM_EMU_RS_SWRESET         , "Event_MM_EMU_RS_SWRESET"          , },
    //
    { MM_EMU_RS_NETPLAY_START   , "Event_MM_EMU_RS_NETPLAY_START"    , },
    //
    { MM_EMU_RS_STATE_LOAD      , "Event_MM_EMU_RS_STATE_LOAD"       , },
    { MM_EMU_RS_STATE_SAVE      , "Event_MM_EMU_RS_STATE_SAVE"       , },
    //
    // For Disk system
    { MM_EMU_RS_DISK_COMMAND    , "Event_MM_EMU_RS_DISK_COMMAND"     , },
    // For ExController
    { MM_EMU_RS_EXCONTROLLER    , "Event_MM_EMU_RS_EXCONTROLLER"     , },
    // For Sound
    { MM_EMU_RS_SOUND_MUTE      , "Event_MM_EMU_RS_SOUND_MUTE"       , },
    //
    // For Snapshot
    { MM_EMU_RS_SNAPSHOT        , "Event_MM_EMU_RS_SNAPSHOT"         , },
    // For Movie
    { MM_EMU_RS_MOVIE_PLAY      , "Event_MM_EMU_RS_MOVIE_PLAY"       , },
    { MM_EMU_RS_MOVIE_REC       , "Event_MM_EMU_RS_MOVIE_REC"        , },
    { MM_EMU_RS_MOVIE_RECAPPEND , "Event_MM_EMU_RS_MOVIE_RECAPPEND"  , },
    { MM_EMU_RS_MOVIE_STOP      , "Event_MM_EMU_RS_MOVIE_STOP"       , },
    //
    // For Wave recording
    { MM_EMU_RS_WAVEREC_START   , "Event_MM_EMU_RS_WAVEREC_START"    , },
    { MM_EMU_RS_WAVEREC_STOP    , "Event_MM_EMU_RS_WAVEREC_STOP"     , },
    // For Tape recording
    { MM_EMU_RS_TAPE_PLAY       , "Event_MM_EMU_RS_TAPE_PLAY"        , },
    { MM_EMU_RS_TAPE_REC        , "Event_MM_EMU_RS_TAPE_REC"         , },
    { MM_EMU_RS_TAPE_STOP       , "Event_MM_EMU_RS_TAPE_STOP"        , },
    //
    // For Barcode
    { MM_EMU_RS_BARCODE         , "Event_MM_EMU_RS_BARCODE"          , },
    //
    // For TurboFile
    { MM_EMU_RS_TURBOFILE       , "Event_MM_EMU_RS_TURBOFILE"        , },
    //
    // For Debugger
    { MM_EMU_RS_DEBUG_RUN       , "Event_MM_EMU_RS_DEBUG_RUN"        , },
    { MM_EMU_RS_DEBUG_BRAKE     , "Event_MM_EMU_RS_DEBUG_BRAKE"      , },
    { MM_EMU_RS_DEBUG_STEP      , "Event_MM_EMU_RS_DEBUG_STEP"       , },
    { MM_EMU_RS_DEBUG_COMMAND   , "Event_MM_EMU_RS_DEBUG_COMMAND"    , },

    { MM_EMU_NT_NONE            , "Event_MM_EMU_NT_NONE"             , },

    { MM_EMU_NT_PLAY            , "Event_MM_EMU_NT_PLAY"             , },
    { MM_EMU_NT_STOP            , "Event_MM_EMU_NT_STOP"             , },
    // For Screen
    { MM_EMU_NT_SCREENMESSAGE   , "Event_MM_EMU_NT_SCREENMESSAGE"    , },
    // For Movie
    { MM_EMU_NT_MOVIE_FINISH    , "Event_MM_EMU_NT_MOVIE_FINISH"     , },
    // For VideoMode
    { MM_EMU_NT_VIDEOMODE       , "Event_MM_EMU_NT_VIDEOMODE"        , },
    // For ExController
    { MM_EMU_NT_EXCONTROLLER    , "Event_MM_EMU_NT_EXCONTROLLER"     , },
};

static const size_t MM_EMULATOR_EVENT_NAME_SIZE = MM_ARRAY_SIZE(MM_EMULATOR_EVENT_NAME);

static
int
mmEmulatorMachineEventNameCompare(
    const void*                                   item,
    const void*                                   key)
{
    return (int)(((const struct mmEmulatorEventMidName*)(item))->mid - (*(mmUInt32_t*)key));
}

static
void
mmEmulatorMachine_EmuEmulatorHandleDefault(
    void*                                          obj,
    void*                                          u,
    mmUInt32_t                                     mid,
    struct mmEmuEmulatorEvent*                     pEvent)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(emulator->callback_q.obj);
    size_t index = 0;

    struct mmEventEmulatorArgs hArgs;
    mmEventEmulatorArgs_Init(&hArgs);
    hArgs.mid = mid;
    hArgs.p0 = pEvent->p0;
    hArgs.p1 = pEvent->p1;
    mmString_Assign(&hArgs.ps, &pEvent->ps);

    index = mmBinarySerachArrayFindEqual(
        &mid, 
        MM_EMULATOR_EVENT_NAME, 
        sizeof(struct mmEmulatorEventMidName), 
        MM_EMULATOR_EVENT_NAME_SIZE, 
        &mmEmulatorMachineEventNameCompare);

    if ((size_t)(-1) != index)
    {
        const struct mmEmulatorEventMidName* item = &MM_EMULATOR_EVENT_NAME[index];
        mmEventSet_FireEvent(&p->hEventSet, item->name, &hArgs);
    }
    else
    {
        mmEventSet_FireEvent(&p->hEventSet, Event_MM_EMU_UNKNOWN, &hArgs);
    }
    mmEventEmulatorArgs_Destroy(&hArgs);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Init(
    struct mmEmulatorMachine*                      p)
{
    struct mmEmuEmulatorCallback hEmulatorCallback;

    mmEmuEmulatorProcessor_Init(&p->hSuper);
    mmEventSet_Init(&p->hEventSet);

    p->pPackageAssets = NULL;
    p->pFileContext = NULL;

    mmString_Init(&p->hName);
    mmString_Init(&p->hDataPath);
    mmString_Init(&p->hWritablePath);

    mmEmuEmulator_Init(&p->hEmulator);
    mmEmulatorAudio_Init(&p->hAudio);
    mmEmulatorFrame_Init(&p->hFrame);

    mmEmulatorAssets_Init(&p->hEmulatorAssets);

    mmString_Assigns(&p->hName, "mm");
    mmString_Assigns(&p->hDataPath, "emulator");
    mmString_Assigns(&p->hWritablePath, "emulator");

    mmEmuEmulator_SetProcessor(&p->hEmulator, &p->hSuper);

    hEmulatorCallback.Handle = &mmEmulatorMachine_EmuEmulatorHandleDefault;
    hEmulatorCallback.obj = p;
    mmEmuEmulator_SetCallbackDefault(&p->hEmulator, &hEmulatorCallback);

    mmEmulatorFrame_SetPaletteRGBA(&p->hFrame, p->hEmulator.palette);

    mmEmulatorAudio_SetRate(&p->hAudio, p->hEmulator.emu->config.audio.nRate);
    mmEmulatorAudio_SetFormat(&p->hAudio, AL_FORMAT_MONO8);

    p->hSuper.ThreadEnter = &mmEmulatorMachine_ThreadEnter;
    p->hSuper.ThreadLeave = &mmEmulatorMachine_ThreadLeave;
    p->hSuper.ProcessAudio = &mmEmulatorMachine_ProcessAudio;
    p->hSuper.ProcessFrame = &mmEmulatorMachine_ProcessFrame;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Destroy(
    struct mmEmulatorMachine*                      p)
{
    mmEmulatorAssets_Destroy(&p->hEmulatorAssets);

    mmEmulatorFrame_Destroy(&p->hFrame);
    mmEmulatorAudio_Destroy(&p->hAudio);
    mmEmuEmulator_Destroy(&p->hEmulator);

    mmString_Destroy(&p->hWritablePath);
    mmString_Destroy(&p->hDataPath);
    mmString_Destroy(&p->hName);

    p->pFileContext = NULL;
    p->pPackageAssets = NULL;

    mmEventSet_Destroy(&p->hEventSet);
    mmEmuEmulatorProcessor_Destroy(&p->hSuper);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetUploader(
    struct mmEmulatorMachine*                      p,
    struct mmVKUploader*                           pUploader)
{
    mmEmulatorFrame_SetUploader(&p->hFrame, pUploader);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetPackageAssets(
    struct mmEmulatorMachine*                      p,
    struct mmPackageAssets*                        pPackageAssets)
{
    p->pPackageAssets = pPackageAssets;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetName(
    struct mmEmulatorMachine*                      p,
    const char*                                    pName)
{
    mmString_Assigns(&p->hName, pName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetRomPath(
    struct mmEmulatorMachine*                      p,
    const char*                                    szRomPath)
{
    mmEmuEmulator_SetRomPath(&p->hEmulator, szRomPath);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetDataPath(
    struct mmEmulatorMachine*                      p,
    const char*                                    pPath)
{
    mmString_Assigns(&p->hDataPath, pPath);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetWritablePath(
    struct mmEmulatorMachine*                      p,
    const char*                                    pPath)
{
    mmString_Assigns(&p->hWritablePath, pPath);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetFileContext(
    struct mmEmulatorMachine*                      p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetPaletteTableByte(
    struct mmEmulatorMachine*                      p,
    const mmUInt32_t                               hPal[64])
{
    mmEmuEmulator_SetPaletteTableByte(&p->hEmulator, hPal);
    mmEmulatorFrame_SetPaletteRGBA(&p->hFrame, p->hEmulator.palette);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetTaskBackground(
    struct mmEmulatorMachine*                      p,
    mmUInt8_t                                      hBackground)
{
    mmEmuEmulator_SetTaskBackground(&p->hEmulator, hBackground);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetAssets(
    struct mmEmulatorMachine*                      p,
    struct mmEmulatorAssets*                       pAssets)
{
    // cache current value.
    mmEmulatorAssets_CopyFrom(&p->hEmulatorAssets, pAssets);

    mmMkdirIfNotExist(mmString_CStr(&pAssets->hWritableDirectory));

    mmEmulatorMachine_SetFileContext(p, pAssets->pFileContext);
    mmEmulatorMachine_SetRomPath(p, mmString_CStr(&pAssets->hAssetsName));
    mmEmulatorMachine_SetWritablePath(p, mmString_CStr(&pAssets->hEmulatorDirectory));

    mmEmulatorMachine_UpdateAssetsPath(p);

    mmEmulatorMachine_SetResourceRoot(
        p,
        pAssets->hAssetsType,
        mmString_CStr(&pAssets->hRootPath),
        "");
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetMute(
    struct mmEmulatorMachine*                      p,
    mmBool_t                                       hMute)
{
    mmEmulatorAudio_SetMute(&p->hAudio, hMute);
}

MM_EXPORT_EMULATOR
mmBool_t
mmEmulatorMachine_GetMute(
    const struct mmEmulatorMachine*                p)
{
    return p->hAudio.mute;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetExControllerType(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        hType)
{
    mmEmuEmulator_ExController(&p->hEmulator, hType);
}

MM_EXPORT_EMULATOR
mmInt_t
mmEmulatorMachine_GetExControllerType(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_GetExControllerType(&p->hEmulator);
}

MM_EXPORT_EMULATOR
mmInt_t
mmEmulatorMachine_GetVideoMode(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_GetVideoMode(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetRapidSpeed(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        no,
    mmWord_t                                       speed[2])
{
    mmEmuEmulator_SetRapidSpeed(&p->hEmulator, no, speed);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_GetRapidSpeed(
    const struct mmEmulatorMachine*                p,
    mmInt_t                                        no,
    mmWord_t                                       speed[2])
{
    mmEmuEmulator_GetRapidSpeed(&p->hEmulator, no, speed);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetResourceRoot(
    struct mmEmulatorMachine*                      p,
    int                                            hTypeAssets,
    const char*                                    pRootPath,
    const char*                                    pRootBase)
{
    mmEmuEmulator_SetResourceRoot(&p->hEmulator, hTypeAssets, pRootPath, pRootBase);
}

MM_EXPORT_EMULATOR
mmUInt32_t
mmEmulatorMachine_GetRomLoadCode(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_GetRomLoadCode(&p->hEmulator);
}

MM_EXPORT_EMULATOR
struct mmFileContext*
mmEmulatorMachine_GetAssetsFileContext(
    const struct mmEmulatorMachine*                p)
{
    return &p->hEmulator.emu->assets.hFileContext;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_UpdateAssetsPath(
    struct mmEmulatorMachine*                      p)
{
    struct mmFileContext* pFileContext = p->pFileContext;
    struct mmPackageAssets* pPackageAssets = p->pPackageAssets;
    struct mmString* pExtFilesDir= &pPackageAssets->hExternalFilesDirectory;

    struct mmString hWritablePath;
    struct mmString hFolderBase;
    struct mmString hSourceBase;

    mmString_Init(&hWritablePath);
    mmString_Init(&hFolderBase);
    mmString_Init(&hSourceBase);

    mmString_Assign(&hWritablePath, pExtFilesDir);
    mmString_Appends(&hWritablePath, mmString_Empty(pExtFilesDir) ? "" : "/");
    mmString_Append(&hWritablePath, &p->hWritablePath);

    // use little base.
    mmString_Assign(&hFolderBase, &pFileContext->folder_base);
    mmString_Appends(&hFolderBase, mmString_Empty(&pFileContext->folder_base) ? "" : "/");
    mmString_Append(&hFolderBase, &p->hDataPath);

    mmString_Assign(&hSourceBase, &pFileContext->source_base);
    mmString_Appends(&hSourceBase, mmString_Empty(&pFileContext->source_base) ? "" : "/");
    mmString_Append(&hSourceBase, &p->hDataPath);
    // mkdir emu writable path.
    mmMkdirIfNotExist(mmString_CStr(&hWritablePath));
    // use the main pFileContext.
    mmEmuEmulator_SetResourceRoot(
        &p->hEmulator,
        MM_EMU_ASSETS_FOLDER,
        mmString_CStr(&pFileContext->folder_path),
        mmString_CStr(&hFolderBase));
    
    mmEmuEmulator_SetResourceRoot(
        &p->hEmulator,
        MM_EMU_ASSETS_SOURCE,
        mmString_CStr(&pFileContext->source_path),
        mmString_CStr(&hSourceBase));

    mmEmuEmulator_SetWritablePath(
        &p->hEmulator,
        mmString_CStr(&hWritablePath));

    mmString_Destroy(&hSourceBase);
    mmString_Destroy(&hFolderBase);
    mmString_Destroy(&hWritablePath);
}

MM_EXPORT_EMULATOR
int
mmEmulatorMachine_Prepare(
    struct mmEmulatorMachine*                      p)
{
    int err = MM_UNKNOWN;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmEmulatorMachine_UpdateAssetsPath(p);
        
        err = mmEmulatorAudio_Prepare(&p->hAudio);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmEmulatorAudio_Prepare failure.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        err = mmEmulatorFrame_Prepare(&p->hFrame);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmEmulatorFrame_Prepare failure.", __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        mmEmulatorAudio_Play(&p->hAudio);
        
    } while(0);
    
    return err;
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Discard(
    struct mmEmulatorMachine*                      p)
{
    mmEmulatorAudio_Stop(&p->hAudio);
    
    mmEmulatorFrame_Discard(&p->hFrame);
    mmEmulatorAudio_Discard(&p->hAudio);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetVideoMode(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        nMode)
{
    mmEmuEmulator_SetVideoMode(&p->hEmulator, nMode);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_LoadRom(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_LoadRom(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_ScreenClear(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_ScreenClear(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetVideoModeImmediately(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        nMode)
{
    mmEmuEmulator_SetVideoModeImmediately(&p->hEmulator, nMode);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_LoadRomImmediately(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_LoadRomImmediately(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_ScreenClearImmediately(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_ScreenClearImmediately(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Play(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Play(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Stop(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Stop(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Pause(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Pause(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Resume(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Resume(&p->hEmulator);
}

MM_EXPORT_EMULATOR
mmInt_t
mmEmulatorMachine_GetState(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_GetState(&p->hEmulator);
}

// event keyboard.
MM_EXPORT_EMULATOR
void
mmEmulatorMachine_KeyboardPressed(
    struct mmEmulatorMachine*                      p,
    int                                            nId)
{
    mmEmuEmulator_KeyboardPressed(&p->hEmulator, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_KeyboardRelease(
    struct mmEmulatorMachine*                      p,
    int                                            nId)
{
    mmEmuEmulator_KeyboardRelease(&p->hEmulator, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MouseBegan(
    struct mmEmulatorMachine*                      p,
    mmLong_t                                       x,
    mmLong_t                                       y,
    int                                            hButtonMask)
{
    mmEmuEmulator_MouseBegan(&p->hEmulator, x, y, hButtonMask);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MouseMoved(
    struct mmEmulatorMachine*                      p,
    mmLong_t                                       x,
    mmLong_t                                       y,
    int                                            hButtonMask)
{
    mmEmuEmulator_MouseMoved(&p->hEmulator, x, y, hButtonMask);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MouseEnded(
    struct mmEmulatorMachine*                      p,
    mmLong_t                                       x,
    mmLong_t                                       y,
    int                                            hButtonMask)
{
    mmEmuEmulator_MouseEnded(&p->hEmulator, x, y, hButtonMask);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_EnterBackground(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_EnterBackground(&p->hEmulator);
    mmEmulatorFrame_EnterBackground(&p->hFrame);
    mmEmulatorAudio_EnterBackground(&p->hAudio);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_EnterForeground(
    struct mmEmulatorMachine*                      p)
{
    mmEmulatorAudio_EnterForeground(&p->hAudio);
    mmEmulatorFrame_EnterForeground(&p->hFrame);
    mmEmuEmulator_EnterForeground(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_JoypadBitSetData(
    struct mmEmulatorMachine*                      p,
    int                                            n,
    mmWord_t                                       hData)
{
    mmEmuEmulator_JoypadBitSetData(&p->hEmulator, n, hData);
}

MM_EXPORT_EMULATOR
mmWord_t
mmEmulatorMachine_JoypadBitGetData(
    struct mmEmulatorMachine*                      p,
    int                                            n)
{
    return mmEmuEmulator_JoypadBitGetData(&p->hEmulator, n);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_JoypadBitPressed(
    struct mmEmulatorMachine*                      p,
    int                                            n,
    int                                            nId)
{
    mmEmuEmulator_JoypadBitPressed(&p->hEmulator, n, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_JoypadBitRelease(
    struct mmEmulatorMachine*                      p,
    int                                            n,
    int                                            nId)
{
    mmEmuEmulator_JoypadBitRelease(&p->hEmulator, n, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_NsfBitSetData(
    struct mmEmulatorMachine*                      p,
    mmByte_t                                       hData)
{
    mmEmuEmulator_NsfBitSetData(&p->hEmulator, hData);
}

MM_EXPORT_EMULATOR
mmByte_t
mmEmulatorMachine_NsfBitGetData(
    struct mmEmulatorMachine*                      p)
{
    return mmEmuEmulator_NsfBitGetData(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_NsfBitPressed(
    struct mmEmulatorMachine*                      p,
    int                                            nId)
{
    mmEmuEmulator_NsfBitPressed(&p->hEmulator, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_NsfBitRelease(
    struct mmEmulatorMachine*                      p,
    int                                            nId)
{
    mmEmuEmulator_NsfBitRelease(&p->hEmulator, nId);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_HardwareReset(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_HardwareReset(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SoftwareReset(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_SoftwareReset(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_StateLoad(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_StateLoad(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_StateSave(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_StateSave(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_DiskCommand(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        cmd)
{
    mmEmuEmulator_DiskCommand(&p->hEmulator, cmd);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Snapshot(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Snapshot(&p->hEmulator);
}

MM_EXPORT_EMULATOR
mmBool_t
mmEmulatorMachine_IsMoviePlay(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_IsMoviePlay(&p->hEmulator);
}

MM_EXPORT_EMULATOR
mmBool_t
mmEmulatorMachine_IsMovieRec(
    const struct mmEmulatorMachine*                p)
{
    return mmEmuEmulator_IsMovieRec(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MoviePlay(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_MoviePlay(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MovieRec(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_MovieRec(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MovieRecAppend(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_MovieRecAppend(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_MovieStop(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_MovieStop(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_WaveRecStart(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_WaveRecStart(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_WaveRecStop(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_WaveRecStop(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_TapePlay(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_TapePlay(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_TapeRec(
    struct mmEmulatorMachine*                      p,
    const char*                                    szFName)
{
    mmEmuEmulator_TapeRec(&p->hEmulator, szFName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_TapeStop(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_TapeStop(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetBarcodeData(
    struct mmEmulatorMachine*                      p,
    mmByte_t*                                      code,
    mmInt_t                                        len)
{
    mmEmuEmulator_SetBarcodeData(&p->hEmulator, code, len);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_SetTurboFileBank(
    struct mmEmulatorMachine*                      p,
    mmInt_t                                        bank)
{
    mmEmuEmulator_SetTurboFileBank(&p->hEmulator, bank);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_UpdateFrameBitmap(
    struct mmEmulatorMachine*                      p)
{
    mmEmulatorFrame_UpdateFrameBitmap(&p->hFrame);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_UpdateRenderer(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_UpdateRenderer(&p->hEmulator);
    mmEmulatorMachine_UpdateFrameBitmap(p);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Start(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Start(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Interrupt(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Interrupt(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Shutdown(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Shutdown(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_Join(
    struct mmEmulatorMachine*                      p)
{
    mmEmuEmulator_Join(&p->hEmulator);
}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_ThreadEnter(
    struct mmEmuEmulatorProcessor*                 pSuper)
{

}

MM_EXPORT_EMULATOR
void
mmEmulatorMachine_ThreadLeave(
    struct mmEmuEmulatorProcessor*                 pSuper)
{

}

MM_EXPORT_EMULATOR
int
mmEmulatorMachine_ProcessAudio(
    struct mmEmuEmulatorProcessor*                 pSuper,
    mmByte_t*                                      buffer,
    size_t                                         length)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(pSuper);

    return mmEmulatorAudio_ProcessBuffer(&p->hAudio, buffer, length);
}

MM_EXPORT_EMULATOR
int
mmEmulatorMachine_ProcessFrame(
    struct mmEmuEmulatorProcessor*                 pSuper,
    mmByte_t*                                      buffer,
    size_t                                         length)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(pSuper);

    return mmEmulatorFrame_ProcessBuffer(&p->hFrame, buffer, length);
}

