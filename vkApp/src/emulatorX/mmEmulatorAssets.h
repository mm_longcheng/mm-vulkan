/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorAssets_h__
#define __mmEmulatorAssets_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "emulatorX/mmEmulatorExport.h"

#include "core/mmPrefix.h"

struct mmFileContext;

struct mmEmulatorAssets
{
    struct mmString hFullName;
    struct mmString hPathName;
    struct mmString hBaseName;
    struct mmString hSuffixName;

    struct mmString hRootPath;

    struct mmString hAssetsName;
    struct mmString hSourceName;

    struct mmString hWritableDirectory;
    struct mmString hWritablePath;
    struct mmString hWritableBase;

    struct mmString hEmulatorDirectory;

    mmUInt32_t hAssetsType;

    struct mmFileContext* pFileContext;
};

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Init(
    struct mmEmulatorAssets*                       p);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Destroy(
    struct mmEmulatorAssets*                       p);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Reset(
    struct mmEmulatorAssets*                       p);

// copy q -> p
MM_EXPORT_EMULATOR
void
mmEmulatorAssets_CopyFrom(
    struct mmEmulatorAssets*                       p,
    const struct mmEmulatorAssets*                 q);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetPath(
    struct mmEmulatorAssets*                       p,
    const char*                                    path,
    const char*                                    base);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetAssetsType(
    struct mmEmulatorAssets*                       p,
    mmUInt32_t                                     hAssetsType);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetRootPath(
    struct mmEmulatorAssets*                       p,
    const char*                                    pRootPath);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetAssetsName(
    struct mmEmulatorAssets*                       p,
    const char*                                    pAssetsName);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetSourceName(
    struct mmEmulatorAssets*                       p,
    const char*                                    pSourceName);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetWritable(
    struct mmEmulatorAssets*                       p,
    const char*                                    pWritablePath,
    const char*                                    pWritableBase);

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetFileContext(
    struct mmEmulatorAssets*                       p,
    struct mmFileContext*                          pFileContext);

#include "core/mmSuffix.h"

#endif//__mmEmulatorAssets_h__
