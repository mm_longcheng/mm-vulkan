/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorFrame.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmBit.h"
#include "core/mmSpinlock.h"
#include "core/mmInteger.h"

#include "emu/mmEmuScreen.h"

#include "vk/mmVKImageUploader.h"
#include "vk/mmVKKTXUploader.h"
#include "vk/mmVKAssets.h"

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_Init(
    struct mmEmulatorFrame*                        p)
{
    mmMemset(p->vTexel, 0, sizeof(p->vTexel));
    p->sampler = VK_NULL_HANDLE;

    p->hScreenBuffSize = sizeof(mmByte_t) * MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H;
    p->hScreenRealSize = p->hScreenBuffSize;
    mmRoundup32(p->hScreenRealSize);
    p->pScreenBufferPtr = (mmByte_t*)mmMalloc(p->hScreenRealSize * 2);
    p->pScreenPtr[0] = p->pScreenBufferPtr;
    p->pScreenPtr[1] = p->pScreenBufferPtr + p->hScreenRealSize;

    p->pPaletteRGB = NULL;

    p->pUploader = NULL;

    mmSpinlock_Init(&p->hLockerScreenPtr0, NULL);
    mmSpinlock_Init(&p->hLockerScreenPtr1, NULL);
    // 0x3F is black color index.
    mmMemset(p->pScreenBufferPtr, 0x3F, p->hScreenRealSize * 2);
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_Destroy(
    struct mmEmulatorFrame*                        p)
{
    p->pUploader = NULL;

    p->pPaletteRGB = NULL;

    mmSpinlock_Destroy(&p->hLockerScreenPtr0);
    mmSpinlock_Destroy(&p->hLockerScreenPtr1);
    // 0x3F is black color index.
    mmMemset(p->pScreenBufferPtr, 0x3F, p->hScreenRealSize * 2);

    mmFree(p->pScreenBufferPtr);

    p->sampler = VK_NULL_HANDLE;
    mmMemset(p->vTexel, 0, sizeof(p->vTexel));
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_SetUploader(
    struct mmEmulatorFrame* p,
    struct mmVKUploader*                           pUploader)
{
    p->pUploader = pUploader;
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_SetPaletteRGBA(
    struct mmEmulatorFrame*                        p,
    void*                                          data)
{
    p->pPaletteRGB = data;
}

MM_EXPORT_EMULATOR
int
mmEmulatorFrame_Prepare(
    struct mmEmulatorFrame*                        p)
{
    VkResult err = VK_ERROR_UNKNOWN;
    
    do
    {
        VkDevice device = VK_NULL_HANDLE;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

        const VkAllocationCallbacks* pAllocator = NULL;
        
        struct mmVKUploader* pUploader = p->pUploader;

        struct mmVKTexelCreateInfo hTexelInfo;
        VkSamplerCreateInfo hSamplerInfo;
        
        int hFormatSupport0, hFormatSupport1;

        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == pUploader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " uploader is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " device is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        physicalDevice = pUploader->physicalDevice;
        if (VK_NULL_HANDLE == physicalDevice)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " physicalDevice is invalid.", __FUNCTION__, __LINE__);
            err = VK_ERROR_INITIALIZATION_FAILED;
            break;
        }
        
        pAllocator = pUploader->pAllocator;
        
        hFormatSupport0 =
        mmVKImageGetIsFormatSupported(physicalDevice, VK_FORMAT_R8_UINT);
        if (!hFormatSupport0)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " Format not Supported: VK_FORMAT_R8_UINT.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }
        
        hFormatSupport1 =
        mmVKImageGetIsFormatSupported(physicalDevice, VK_FORMAT_B8G8R8A8_UINT);
        if (!hFormatSupport0)
        {
            mmLogger_LogI(gLogger, "%s %d"
                " Format not Supported: VK_FORMAT_B8G8R8A8_UINT.", __FUNCTION__, __LINE__);
            err = VK_ERROR_FORMAT_NOT_SUPPORTED;
            break;
        }
        
        // Screen R8       256 x 240
        hTexelInfo.format = VK_FORMAT_R8_UNORM;
        hTexelInfo.imageType = VK_IMAGE_TYPE_2D;
        hTexelInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        hTexelInfo.extent[0] = MM_EMU_SCREEN_RENDER_W;
        hTexelInfo.extent[1] = MM_EMU_SCREEN_RENDER_H;
        hTexelInfo.extent[2] = 1;
        hTexelInfo.mipLevels = 1;
        hTexelInfo.arrayLayers = 1;
        hTexelInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        hTexelInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        hTexelInfo.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        hTexelInfo.vmaFlags = 0;
        hTexelInfo.vmaUsage = VMA_MEMORY_USAGE_GPU_ONLY;
        
        err = mmVKTexel_Prepare(
            &p->vTexel[0],
            pUploader,
            (const uint8_t*)p->pScreenPtr[0],
            (size_t)(MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H),
            &hTexelInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTexel_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        // Palette R8G8B8A8 16 x 16 = 256 x 1 x 1
        hTexelInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
        hTexelInfo.imageType = VK_IMAGE_TYPE_1D;
        hTexelInfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
        hTexelInfo.extent[0] = 256;
        hTexelInfo.extent[1] = 1;
        hTexelInfo.extent[2] = 1;
        
        err = mmVKTexel_Prepare(
            &p->vTexel[1],
            pUploader,
            (const uint8_t*)p->pPaletteRGB,
            (size_t)(16 * 16 * 4),
            &hTexelInfo);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmVKTexel_Prepare failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        hSamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        hSamplerInfo.pNext = NULL;
        hSamplerInfo.flags = 0;
        hSamplerInfo.magFilter = VK_FILTER_NEAREST;
        hSamplerInfo.minFilter = VK_FILTER_NEAREST;
        hSamplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        hSamplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        hSamplerInfo.mipLodBias = 0.0f;
        hSamplerInfo.anisotropyEnable = VK_FALSE;
        hSamplerInfo.maxAnisotropy = 1;
        hSamplerInfo.compareEnable = VK_FALSE;
        hSamplerInfo.compareOp = VK_COMPARE_OP_NEVER;
        hSamplerInfo.minLod = 0.0f;
        hSamplerInfo.maxLod = 0.0f;
        hSamplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
        hSamplerInfo.unnormalizedCoordinates = VK_FALSE;
        /* create sampler */
        err = vkCreateSampler(device, &hSamplerInfo, pAllocator, &p->sampler);
        if (VK_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " vkCreateSampler failure.", __FUNCTION__, __LINE__);
            break;
        }
        
        // update palette.
        mmEmulatorFrame_UpdatePalette(p);
    } while(0);
    
    return (int)err;
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_Discard(
    struct mmEmulatorFrame*                        p)
{
    do
    {
        VkDevice device = NULL;
        const VkAllocationCallbacks* pAllocator = NULL;
        
        struct mmVKUploader* pUploader = p->pUploader;

        if (NULL == pUploader)
        {
            break;
        }

        device = pUploader->device;
        if (VK_NULL_HANDLE == device)
        {
            break;
        }

        pAllocator = pUploader->pAllocator;
        
        if (VK_NULL_HANDLE != p->sampler)
        {
            vkDestroySampler(device, p->sampler, pAllocator);
            p->sampler = VK_NULL_HANDLE;
        }
        
        mmVKTexel_Discard(&p->vTexel[1], pUploader);
        mmVKTexel_Discard(&p->vTexel[0], pUploader);
    } while(0);
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_EnterBackground(
    struct mmEmulatorFrame*                        p)
{

}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_EnterForeground(
    struct mmEmulatorFrame*                        p)
{

}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_UpdatePalette(
    struct mmEmulatorFrame*                        p)
{
    mmVKTexel_UpdateBuffer(
        &p->vTexel[1],
        p->pUploader,
        (const uint8_t*)p->pPaletteRGB,
        (size_t)(16 * 16 * 4));
}

MM_EXPORT_EMULATOR
void
mmEmulatorFrame_UpdateFrameBitmap(
    struct mmEmulatorFrame*                        p)
{
    if (mmSpinlock_Trylock(&p->hLockerScreenPtr1))
    {
        mmVKTexel_UpdateBuffer(
            &p->vTexel[0],
            p->pUploader,
            (const uint8_t*)p->pScreenPtr[1],
            (size_t)(MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H));
        
        mmSpinlock_Unlock(&p->hLockerScreenPtr1);
    }
    else
    {
        mmSpinlock_Lock(&p->hLockerScreenPtr0);
        
        mmVKTexel_UpdateBuffer(
            &p->vTexel[0],
            p->pUploader,
            (const uint8_t*)p->pScreenPtr[0],
            (size_t)(MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H));

        mmSpinlock_Unlock(&p->hLockerScreenPtr0);
    }
}

MM_EXPORT_EMULATOR
int
mmEmulatorFrame_ProcessBuffer(
    struct mmEmulatorFrame*                        p,
    mmByte_t*                                      buffer,
    size_t                                         length)
{
    if (mmSpinlock_Trylock(&p->hLockerScreenPtr0))
    {
        mmMemcpy(p->pScreenPtr[0], buffer, length);

        mmSpinlock_Lock(&p->hLockerScreenPtr1);
        mmInteger_UIntptrSwap((uintptr_t*)&p->pScreenPtr[0], (uintptr_t*)&p->pScreenPtr[1]);
        mmSpinlock_Unlock(&p->hLockerScreenPtr1);

        mmSpinlock_Unlock(&p->hLockerScreenPtr0);
        
        return 0;
    }
    else
    {
        mmSpinlock_Lock(&p->hLockerScreenPtr1);
        mmMemcpy(p->pScreenPtr[1], buffer, length);
        mmSpinlock_Unlock(&p->hLockerScreenPtr1);
        
        return 1;
    }
}

