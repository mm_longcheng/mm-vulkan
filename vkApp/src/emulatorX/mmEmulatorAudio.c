/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorAudio.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "math/mmMath.h"

#include "al/mmALError.h"

static
ALuint
mmEmulatorAudio_BufferAdd(
    struct mmEmulatorAudio*                        p);

static
void
mmEmulatorAudio_BufferRmv(
    struct mmEmulatorAudio*                        p,
    ALuint                                         e);

static
void
mmEmulatorAudio_BufferClear(
    struct mmEmulatorAudio*                        p);

static
ALuint
mmEmulatorAudio_BufferProduce(
    struct mmEmulatorAudio*                        p);

static
void
mmEmulatorAudio_BufferRecycle(
    struct mmEmulatorAudio*                        p,
    ALuint                                         v);

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Init(
    struct mmEmulatorAudio*                        p)
{
    mmRbtsetU32_Init(&p->pool);
    mmRbtsetU32_Init(&p->used);
    mmRbtsetU32_Init(&p->idle);
    p->hRateTrimBorder = MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT;
    p->hRateTrimNumber = MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT;

    p->accumulation = MM_EMULATOR_AUDIO_ACCUMULATION_DEFAULT;

    p->mute = MM_FALSE;
    p->background = MM_FALSE;

    p->hVolume = 1.0f;

    p->nRate = 22050;
    p->nFormat = AL_FORMAT_MONO8;
    p->source = AL_NONE;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Destroy(
    struct mmEmulatorAudio*                        p)
{
    mmEmulatorAudio_BufferClear(p);

    p->source = AL_NONE;
    p->nFormat = AL_FORMAT_MONO8;
    p->nRate = 22050;
    
    p->hVolume = 1.0f;
    
    p->background = MM_FALSE;
    p->mute = MM_FALSE;
    
    p->accumulation = MM_EMULATOR_AUDIO_ACCUMULATION_DEFAULT;
    
    p->hRateTrimNumber = MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT;
    p->hRateTrimBorder = MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT;

    mmRbtsetU32_Destroy(&p->idle);
    mmRbtsetU32_Destroy(&p->used);
    mmRbtsetU32_Destroy(&p->pool);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetRate(
    struct mmEmulatorAudio*                        p,
    int                                            nRate)
{
    p->nRate = nRate;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetFormat(
    struct mmEmulatorAudio*                        p,
    ALenum                                         nFormat)
{
    p->nFormat = nFormat;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetMute(
    struct mmEmulatorAudio*                        p,
    mmBool_t                                       mute)
{
    p->mute = mute;
}

MM_EXPORT_EMULATOR
int
mmEmulatorAudio_Prepare(
    struct mmEmulatorAudio*                        p)
{
    int err = MM_UNKNOWN;
    
    ALenum ALErr;
    
    struct mmLogger* gLogger = mmLogger_Instance();

    alGenSources(1, &p->source);
    ALErr = alGetError();
    mmALCheck(MM_LOG_ERROR, ALErr);
    if (AL_NO_ERROR != ALErr || AL_NONE == p->source)
    {
        mmLogger_LogE(gLogger, "%s %d audio source create failure.",
            __FUNCTION__, __LINE__);
        
        err = MM_FAILURE;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d audio source create success.",
            __FUNCTION__, __LINE__);
        
        alSourcei(p->source, AL_LOOPING, AL_FALSE);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        alSourcef(p->source, AL_GAIN, p->hVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        err = MM_SUCCESS;
    }
    
    return err;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Discard(
    struct mmEmulatorAudio*                        p)
{
    mmEmulatorAudio_BufferClear(p);

    if (AL_NONE != p->source)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        alDeleteSources(1, &p->source);
        mmALCheck(MM_LOG_ERROR, alGetError());
        p->source = AL_NONE;
        mmLogger_LogI(gLogger, "%s %d audio source delete success.",
            __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_EnterBackground(
    struct mmEmulatorAudio*                        p)
{
    p->background = MM_TRUE;
    
    if(AL_NONE != p->source)
    {
        alSourcef(p->source, AL_GAIN, 0.0f);
        mmALCheck(MM_LOG_ERROR, alGetError());
        
        mmEmulatorAudio_Pause(p);
    }
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_EnterForeground(
    struct mmEmulatorAudio*                        p)
{
    if(AL_NONE != p->source)
    {
        mmEmulatorAudio_Play(p);
        
        alSourcef(p->source, AL_GAIN, p->hVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
    
    p->background = MM_FALSE;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_SetVolume(
    struct mmEmulatorAudio*                        p,
    float                                          hVolume)
{
    p->hVolume = mmMathMax(hVolume, 0.0f);

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_GAIN, p->hVolume);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Play(
    struct mmEmulatorAudio*                        p)
{
    if (AL_NONE != p->source)
    {
        alSourcePlay(p->source);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Stop(
    struct mmEmulatorAudio*                        p)
{
    if (AL_NONE != p->source)
    {
        alSourceStop(p->source);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR
void
mmEmulatorAudio_Pause(
    struct mmEmulatorAudio*                        p)
{
    if (AL_NONE != p->source)
    {
        alSourcePause(p->source);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR
int
mmEmulatorAudio_ProcessBuffer(
    struct mmEmulatorAudio*                        p,
    mmByte_t*                                      buffer,
    size_t                                         length)
{
    if (MM_FALSE == p->mute &&
        AL_NONE != p->source &&
        MM_FALSE == p->background)
    {
        int processed;
        int queued;
        int remaining;

        ALuint hBuffer;
        ALint hState;

        alGetSourcei(p->source, AL_BUFFERS_PROCESSED, &processed);
        mmALCheck(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_BUFFERS_QUEUED, &queued);
        mmALCheck(MM_LOG_ERROR, alGetError());

        remaining = queued - processed;
        if (remaining > p->accumulation)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogW(gLogger, "%s %d AL accumulation queued: %d processed: %d.",
                __FUNCTION__, __LINE__, queued, processed);
        }

// #define DEBUG_AUDIO
#if defined(DEBUG_AUDIO)
        static int aidx = 0;
        if ((++aidx) >= 60)
        {
            int s = 0;
            int b = 0;

            alGetSourcei(p->source, AL_SAMPLE_OFFSET, &s);
            mmALCheck(MM_LOG_ERROR, alGetError());

            alGetSourcei(p->source, AL_BYTE_OFFSET, &b);
            mmALCheck(MM_LOG_ERROR, alGetError());

            printf("AL queue length: %d remaining: %d queued: %d processed: %d s: %d b: %d\n",
                (int)length, remaining, queued, processed, s, b);

            aidx = 0;
        }
#endif//DEBUG_AUDIO

        while (processed--)
        {
            alSourceUnqueueBuffers(p->source, 1, &hBuffer);
            mmALCheck(MM_LOG_ERROR, alGetError());

            mmEmulatorAudio_BufferRecycle(p, hBuffer);
        }

        hBuffer = mmEmulatorAudio_BufferProduce(p);

        alBufferData(
            hBuffer,
            (ALenum)p->nFormat,
            (const ALvoid*)buffer,
            (ALsizei)length,
            (ALsizei)p->nRate);
        mmALCheck(MM_LOG_ERROR, alGetError());

        alSourceQueueBuffers(p->source, 1, &hBuffer);
        mmALCheck(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_SOURCE_STATE, &hState);
        mmALCheck(MM_LOG_ERROR, alGetError());

        // AL_INITIAL is init finish, but not play.
        // AL_STOPPED is playing but QueueBuffers empty.
        if (AL_STOPPED == hState || AL_INITIAL == hState)
        {
            mmEmulatorAudio_Play(p);
        }

        return remaining;
    }
    else
    {
        int processed;
        int queued;
        int remaining;

        alGetSourcei(p->source, AL_BUFFERS_PROCESSED, &processed);
        mmALCheck(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_BUFFERS_QUEUED, &queued);
        mmALCheck(MM_LOG_ERROR, alGetError());

        remaining = queued - processed;
        return remaining;
    }
}

static
ALuint
mmEmulatorAudio_BufferAdd(
    struct mmEmulatorAudio*                        p)
{
    ALuint e = 0;

    alGenBuffers(1, &e);
    mmALCheck(MM_LOG_ERROR, alGetError());

    // add to pool set.
    mmRbtsetU32_Add(&p->pool, (mmUInt32_t)e);
    return e;
}

static
void
mmEmulatorAudio_BufferRmv(
    struct mmEmulatorAudio*                        p,
    ALuint                                         e)
{
    // rmv to pool set.
    mmRbtsetU32_Rmv(&p->pool, (mmUInt32_t)e);

    alDeleteBuffers(1, &e);
    mmALCheck(MM_LOG_ERROR, alGetError());
}

static
void
mmEmulatorAudio_BufferClear(
    struct mmEmulatorAudio*                        p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    ALuint e = 0;

    n = mmRb_First(&p->used.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        e = (ALuint)(it->k);
        n = mmRb_Next(n);
        mmRbtsetU32_Erase(&p->used, it);

        alSourceUnqueueBuffers(p->source, 1, &e);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }

    n = mmRb_First(&p->pool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        e = (ALuint)(it->k);
        n = mmRb_Next(n);
        mmRbtsetU32_Erase(&p->pool, it);

        alDeleteBuffers(1, &e);
        mmALCheck(MM_LOG_ERROR, alGetError());
    }

    mmRbtsetU32_Clear(&p->idle);
}

static
ALuint
mmEmulatorAudio_BufferProduce(
    struct mmEmulatorAudio*                        p)
{
    ALuint v = 0;

    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    // min.
    n = mmRb_Last(&p->idle.rbt);
    if (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        v = (ALuint)it->k;
        // rmv to idle set.
        mmRbtsetU32_Rmv(&p->idle, v);
        // add to used set.
        mmRbtsetU32_Add(&p->used, (mmUInt32_t)v);
    }
    else
    {
        // add new.
        v = mmEmulatorAudio_BufferAdd(p);
        // add to used set.
        mmRbtsetU32_Add(&p->used, (mmUInt32_t)v);
    }
    return v;
}

static
void
mmEmulatorAudio_BufferRecycle(
    struct mmEmulatorAudio*                        p,
    ALuint                                         v)
{
    float idle_sz = 0;
    float memb_sz = 0;
    int trim_number = 0;

    // rmv to used set.
    mmRbtsetU32_Rmv(&p->used, (mmUInt32_t)v);
    // add to idle set.
    mmRbtsetU32_Add(&p->idle, (mmUInt32_t)v);
    //
    idle_sz = (float)p->idle.size;
    memb_sz = (float)p->pool.size;
    trim_number = (int)(memb_sz * p->hRateTrimNumber);

    if (idle_sz / memb_sz > p->hRateTrimBorder)
    {
        int i = 0;
        //
        struct mmRbNode* n = NULL;
        struct mmRbtsetU32Iterator* it = NULL;
        // max
        n = mmRb_First(&p->idle.rbt);
        while (NULL != n && i < trim_number)
        {
            it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
            v = (ALuint)it->k;
            n = mmRb_Next(n);
            mmRbtsetU32_Erase(&p->idle, it);
            mmEmulatorAudio_BufferRmv(p, v);
            i++;
        }
    }
}

