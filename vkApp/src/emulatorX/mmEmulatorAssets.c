/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorAssets.h"

#include "core/mmFilePath.h"

#include "emu/mmEmuEmulator.h"

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Init(
    struct mmEmulatorAssets*                       p)
{
    mmString_Init(&p->hFullName);
    mmString_Init(&p->hPathName);
    mmString_Init(&p->hBaseName);
    mmString_Init(&p->hSuffixName);

    mmString_Init(&p->hRootPath);

    mmString_Init(&p->hAssetsName);
    mmString_Init(&p->hSourceName);

    mmString_Init(&p->hWritableDirectory);
    mmString_Init(&p->hWritablePath);
    mmString_Init(&p->hWritableBase);

    mmString_Init(&p->hEmulatorDirectory);

    p->hAssetsType = MM_EMU_ASSETS_FOLDER;

    p->pFileContext = NULL;

    mmString_Assigns(&p->hPathName, "emulator");
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Destroy(
    struct mmEmulatorAssets*                       p)
{
    p->pFileContext = NULL;
    
    p->hAssetsType = MM_EMU_ASSETS_FOLDER;
    
    mmString_Destroy(&p->hEmulatorDirectory);
    
    mmString_Destroy(&p->hWritableBase);
    mmString_Destroy(&p->hWritablePath);
    mmString_Destroy(&p->hWritableDirectory);
    
    mmString_Destroy(&p->hSourceName);
    mmString_Destroy(&p->hAssetsName);
    
    mmString_Destroy(&p->hRootPath);
    
    mmString_Destroy(&p->hSuffixName);
    mmString_Destroy(&p->hBaseName);
    mmString_Destroy(&p->hPathName);
    mmString_Destroy(&p->hFullName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_Reset(
    struct mmEmulatorAssets*                       p)
{
    mmString_Clear(&p->hFullName);
    mmString_Clear(&p->hPathName);
    mmString_Clear(&p->hBaseName);
    mmString_Clear(&p->hSuffixName);

    mmString_Clear(&p->hRootPath);

    mmString_Clear(&p->hAssetsName);
    mmString_Clear(&p->hSourceName);

    mmString_Clear(&p->hWritableDirectory);
    mmString_Clear(&p->hWritablePath);
    mmString_Clear(&p->hWritableBase);

    mmString_Clear(&p->hEmulatorDirectory);

    p->hAssetsType = MM_EMU_ASSETS_FOLDER;

    p->pFileContext = NULL;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_CopyFrom(
    struct mmEmulatorAssets*                       p,
    const struct mmEmulatorAssets*                 q)
{
    mmString_Assign(&p->hFullName, &q->hFullName);
    mmString_Assign(&p->hPathName, &q->hPathName);
    mmString_Assign(&p->hBaseName, &q->hBaseName);
    mmString_Assign(&p->hSuffixName, &q->hSuffixName);

    mmString_Assign(&p->hRootPath, &q->hRootPath);

    mmString_Assign(&p->hAssetsName, &q->hAssetsName);
    mmString_Assign(&p->hSourceName, &q->hSourceName);

    mmString_Assign(&p->hWritableDirectory, &q->hWritableDirectory);
    mmString_Assign(&p->hWritablePath, &q->hWritablePath);
    mmString_Assign(&p->hWritableBase, &q->hWritableBase);

    mmString_Assign(&p->hEmulatorDirectory, &q->hEmulatorDirectory);

    p->hAssetsType = q->hAssetsType;

    p->pFileContext = q->pFileContext;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetPath(
    struct mmEmulatorAssets*                       p,
    const char*                                    path,
    const char*                                    base)
{
    struct mmString bname;

    mmString_Init(&bname);

    mmString_Assigns(&p->hPathName, path);
    mmString_Assigns(&p->hBaseName, base);
    mmString_Assigns(&p->hFullName, path);
    mmString_Appends(&p->hFullName, "/");
    mmString_Appends(&p->hFullName, base);

    mmPathSplitSuffixName(&p->hBaseName, &bname, &p->hSuffixName);

    mmString_Destroy(&bname);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetAssetsType(
    struct mmEmulatorAssets*                       p,
    mmUInt32_t                                     hAssetsType)
{
    p->hAssetsType = hAssetsType;
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetRootPath(
    struct mmEmulatorAssets*                       p,
    const char*                                    pRootPath)
{
    mmString_Assigns(&p->hRootPath, pRootPath);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetAssetsName(
    struct mmEmulatorAssets*                       p,
    const char*                                    pAssetsName)
{
    mmString_Assigns(&p->hAssetsName, pAssetsName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetSourceName(
    struct mmEmulatorAssets*                       p,
    const char*                                    pSourceName)
{
    mmString_Assigns(&p->hSourceName, pSourceName);
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetWritable(
    struct mmEmulatorAssets*                       p,
    const char*                                    pWritablePath,
    const char*                                    pWritableBase)
{
    mmString_Assigns(&p->hWritablePath, pWritablePath);
    mmString_Assigns(&p->hWritableBase, pWritableBase);

    mmString_Assign(&p->hWritableDirectory, &p->hWritablePath);
    mmString_Appends(&p->hWritableDirectory, mmString_Empty(&p->hWritablePath) ? "" : "/");
    mmString_Append(&p->hWritableDirectory, &p->hWritableBase);

    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&p->hWritableDirectory, mmString_CStr(&p->hWritableDirectory));

    mmString_Assign(&p->hEmulatorDirectory, &p->hWritableBase);
    mmString_Appends(&p->hEmulatorDirectory, "/");
    mmString_Append(&p->hEmulatorDirectory, &p->hSourceName);

    // remove the suffix if source_name is empty.
    mmDirectoryNoneSuffix(&p->hEmulatorDirectory, mmString_CStr(&p->hEmulatorDirectory));
}

MM_EXPORT_EMULATOR
void
mmEmulatorAssets_SetFileContext(
    struct mmEmulatorAssets*                       p,
    struct mmFileContext*                          pFileContext)
{
    p->pFileContext = pFileContext;
}
