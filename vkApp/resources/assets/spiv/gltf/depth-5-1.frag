#version 450

#extension GL_GOOGLE_include_directive : enable

// Interface protocol macro definition.
// #define HAS_VaryingPosition
// #define HAS_VaryingNormal
// #define HAS_VaryingTBN
// #define HAS_VaryingColor
#define HAS_VaryingTexcoord

// Material texture macro definition.
#define HAS_TextureBaseColor
// #define HAS_TextureMetallicRoughness
// #define HAS_TextureNormal
// #define HAS_TextureOcclusion
// #define HAS_TextureEmissive

// Vertex input Protocol.
layout (location = 0) in vec2 v_Texcoord[5];

// Material sampler Protocol.
layout (set = 1, binding = 1) uniform sampler2D baseColorTexture;

#include "fs-MainShadow.glsl"
