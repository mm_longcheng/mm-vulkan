// Metallic Roughness PBR Materials
//
// BRDF Lighting Equation
//
// f(l,v,h) = Diff(l,n) + F(l,h)G(l,v,h)D(h) / 4(n * l)(n * v)
//
//     l is the light direction
//     v is the view direction
//     h is the half vector
//     n is the normal
//
// The metallic-roughness material model is defined by the following properties:
// 
//     baseColor - The base color of the material
//     metallic  - The metalness of the material
//     roughness - The roughness of the material
//
//
// BRDF Diffuse
//
// Lambertian with energy conservation
//
//     Diff(l,n) = (1 - F(v*h))Cdiff/PI
// 
// cdiff is the diffuse reflected color. To conserve energy, the Fresnel term from specular
// component is subtracted from diffuse component.
//
//     const dielectricSpecular = rgb(0.04, 0.04, 0.04)
//     const black = rgb(0, 0, 0)
//     cdiff = lerp(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic)
//
//
// BRDF Specular
// 
// BRDF Specular : F
// F is the Fresnel function used to simulate the way light interacts with a surface at different
// viewing angles. 
//
// Schlick Fernel model
//
//     F(l,h) = F0 + (1 - F0) * (1 - v * h)^5 
//
//     const dielectricSpecular = rgb(0.04, 0.04, 0.04)
//     F0 = lerp(dieletricSpecular, baseColor.rgb, metallic)
//
// BRDF Specular : G
// G is the geometric occlusion derived from a normal distribution function like Smith’s function
//
//     G(l,v,h) = G1(n,l)G1(n,v)
//     G1(n,v) = 2(n * v) / ((n * v) + sqrt(a^2 + (1 - a^2)*(n * v)^2))
//     a = roughness^2
// 
// BRDF Specular : D
// D is the normal distribution function like GGX that defines the statistical distribution of microfacets.
//
//     D(h) = a^2 / (PI * ((n * h)^2(a - 1) + 1)^2)
//     a = roughness^2
//
// We use the following notation:
// - V is the normalized vector from the shading location to the eye
// - L is the normalized vector from the shading location to the light
// - N is the surface normal in the same space as the above values
// - H is the half vector, where H = normalize(L + V)

// Fresnel Schlick approximation translates
vec3 F_Schlick(
    const in vec3  f0, 
    const in float f90, 
    const in float dotVH) 
{
    // Rf(l,h) = F0 + (1 - F0) * (1 - (l dot h))^5
    // F0 = Ff(h,h) = Cspec
    float fresnel = pow(1.0 - dotVH, 5.0);
    return f0 + (f90 - f0) * fresnel;
}

// Fresnel Schlick approximation translates
vec3 F_SchlickEpic( 
    const in vec3  f0, 
    const in float f90, 
    const in float dotVH) 
{
    // Original approximation by Christophe Schlick '94
    // float fresnel = pow( 1.0 - dotVH, 5.0 );
    //
    // Optimized variant (presented by Epic at SIGGRAPH '13)
    // https://cdn2.unrealengine.com/Resources/files/2013SiggraphPresentationsNotes-26915738.pdf
    float fresnel = exp2((-5.55473 * dotVH - 6.98316) * dotVH);

    return f0 + (f90 - f0) * fresnel;
}

// Geometric Shadowing function
// Moving Frostbite to Physically Based Rendering 3.0 - page 12, listing 2
// https://seblagarde.files.wordpress.com/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf
float V_GGX_SmithCorrelated( 
    const in float alpha, 
    const in float dotNL, 
    const in float dotNV) 
{
    float a2 = pow2( alpha );

    float gv = dotNL * sqrt(a2 + (1.0 - a2) * pow2(dotNV));
    float gl = dotNV * sqrt(a2 + (1.0 - a2) * pow2(dotNL));

    return 0.5 / max(gv + gl, MM_EPSILON);
}
// Geometric Shadowing function
float G_GGX( 
    const in float alpha, 
    const in float dotNL, 
    const in float dotNV) 
{
    float a2 = pow2( alpha );
    float GGXV = 2.0 * dotNV / (dotNV + sqrt(a2 + (1.0 - a2) * (dotNV * dotNV)));
    float GGXL = 2.0 * dotNL / (dotNL + sqrt(a2 + (1.0 - a2) * (dotNL * dotNL)));
    return GGXV * GGXL;
}

// Microfacet Models for Refraction through Rough Surfaces - equation (33)
// http://graphicrants.blogspot.com/2013/08/specular-brdf-reference.html
// alpha is "roughness squared" in Disney’s reparameterization
float D_GGX( 
    const in float alpha, 
    const in float dotNH ) 
{
    float a2 = pow2(alpha);

    // avoid alpha = 0 with dotNH = 1
    float denom = pow2(dotNH) * (a2 - 1.0) + 1.0;

    return MM_1_DIV_PI * a2 / pow2(denom);
}

vec3 BRDF_Diffuse(
    const in vec3  diffuseColor)
{
    return MM_1_DIV_PI * diffuseColor;
}

vec3 BRDF_Specular(
    const in float dotNH, 
    const in float dotNL, 
    const in float dotNV, 
    const in float alpha)
{
    if (dotNL > 0.0 && dotNV > 0.0)
    {
        // Geometric Shadowing function
        float G = G_GGX(alpha, dotNL, dotNV);

        // Normal Distribution function
        float D = D_GGX(alpha, dotNH);

        // Visibility function
        float V = G / (4.0 * dotNL * dotNV);
        
        return vec3(V * D);
    }
    else
    {
        return vec3(0.0);
    }
}

vec3 BRDF_Material(
    const in vec3  L, 
    const in vec3  V, 
    const in vec3  N,
    const in vec3  diffuseColor,
    const in vec3  f0,
    const in float f90,
    const in float roughness)
{
    vec3 H = normalize (V + L);
    float dotNV = clamp(dot(N, V), 0.0, 1.0);
    float dotNL = clamp(dot(N, L), 0.0, 1.0);
    float dotNH = clamp(dot(N, H), 0.0, 1.0);
    float dotVH = clamp(dot(V, H), 0.0, 1.0);
    float alpha = roughness * roughness;
    vec3        F = F_Schlick(f0, f90, dotVH);
    vec3  diffuse = BRDF_Diffuse(diffuseColor);
    vec3 specular = BRDF_Specular(dotNH, dotNL, dotNV, alpha);
    vec3 material = (1 - F) * diffuse + F * specular;
    return dotNL * material;
}
