#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsDepth.glsl"
#include "vs-UBOSceneDepth.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec2  a_Texcoord[2];

layout (location = 0) out vec2 v_Texcoord[2];

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    mat4 ProjectionViewMat = GetLightProjectionView();
    mat4 hModelMat = pConstants.Model;
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Texcoord[0] = a_Texcoord[0];
    v_Texcoord[1] = a_Texcoord[1];
    gl_Position = ProjectionViewMat * w_Position;
}
