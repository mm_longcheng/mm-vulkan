layout (set = 0, binding = 3) uniform sampler2DArray   shadowMapCascade;
layout (set = 0, binding = 4) uniform samplerCubeArray shadowMapOmni;
layout (set = 0, binding = 5) uniform sampler2DArray   shadowMapSpot;

layout (set = 0, binding = 6, std430) readonly buffer UBOShadowCascadeSplits 
{
    float splits[];
} uCascadeSplits;

layout (set = 0, binding = 7, std430) readonly buffer UBOShadowProjectionViews 
{
    mat4 matrices[];
} uShadowProjectionViews;

const mat4 biasMat = mat4
( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 
);

const vec3 gCascadesColor[4] = 
{
	{ 1.00, 0.25, 0.25 },
	{ 0.25, 1.00, 0.25 },
	{ 0.25, 0.25, 1.00 },
	{ 1.00, 1.00, 0.25 },
};

const vec3 gCubeFaceColor[6] = 
{
	{ 1.00, 0.25, 0.25 },
	{ 0.25, 1.00, 0.25 },
	{ 0.25, 0.25, 1.00 },
	{ 1.00, 1.00, 0.25 },
	{ 1.00, 0.25, 1.00 },
	{ 0.25, 1.00, 1.00 },
};

float 
ShadowSampler(
	const in sampler2DArray shadowMap,
	const in vec4 shadowCoord, 
	const in vec2 offset, 
	const in int index, 
	const in float bias,
	const in float ambient)
{
	vec3 coord3 = vec3(shadowCoord.st + offset, float(index));
	float distance = texture(shadowMap, coord3).r;
	if (shadowCoord.w > 0.0 && distance < shadowCoord.z - bias) 
	{
		return ambient;
	}
	else
	{
		return 1.0;
	}
}

float 
ShadowFilterPCF1(
	const in sampler2DArray shadowMap,
	const in vec4 shadowCoord, 
	const in int index, 
	const in float bias,
	const in float ambient)
{
	if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0) 
	{
		return ShadowSampler(shadowMap, shadowCoord, vec2(0.0, 0.0), index, bias, ambient);
	}
	else
	{
		return 1.0;
	}
}

float 
ShadowFilterPCF5(
	const in sampler2DArray shadowMap,
	const in vec4 shadowCoord, 
	const in int index, 
	const in float bias,
	const in float ambient)
{
	if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0) 
	{
		const float scale = 0.75;
		ivec2 texDim = textureSize(shadowMap, 0).xy;
		float dx = scale * 1.0 / float(texDim.x);
		float dy = scale * 1.0 / float(texDim.y);
		
		// 0.2972979                                                  0.2379280
		//
		//   1     0.0453542 0.0566406 0.0453542                      0.1905180
		// 1 2 1   0.0566406 0.0707355 0.0566406            0.1905180 0.2379280 0.1905180
		//   1     0.0453542 0.0566406 0.0453542                      0.1905180
		
		float shadowFactor = 0.0;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(-dx, 0.0), index, bias, ambient) * 0.1905180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(+dx, 0.0), index, bias, ambient) * 0.1905180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, 0.0), index, bias, ambient) * 0.2379280;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, -dy), index, bias, ambient) * 0.1905180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, +dy), index, bias, ambient) * 0.1905180;
		return shadowFactor;
	}
	else
	{
		return 1.0;
	}
}

float 
ShadowFilterPCF9(
	const in sampler2DArray shadowMap,
	const in vec4 shadowCoord, 
	const in int index, 
	const in float bias,
	const in float ambient)
{
	if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0) 
	{
		const float scale = 0.75;
		ivec2 texDim = textureSize(shadowMap, 0).xy;
		float dx = scale * 1.0 / float(texDim.x);
		float dy = scale * 1.0 / float(texDim.y);
		
		// 0.4787147                                                  0.1477610
		//
		// 1 2 1    0.0453542 0.0566406 0.0453542           0.0947416 0.1183180 0.0947416
		// 2 3 2    0.0566406 0.0707355 0.0566406           0.1183180 0.1477616 0.1183180
		// 1 2 1    0.0453542 0.0566406 0.0453542           0.0947416 0.1183180 0.0947416
		
		float shadowFactor = 0.0;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(-dx, -dy), index, bias, ambient) * 0.0947416;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, -dy), index, bias, ambient) * 0.1183180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(+dx, -dy), index, bias, ambient) * 0.0947416;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(-dx, 0.0), index, bias, ambient) * 0.1183180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, 0.0), index, bias, ambient) * 0.1477616;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(+dx, 0.0), index, bias, ambient) * 0.1183180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(-dx, +dy), index, bias, ambient) * 0.0947416;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(0.0, +dy), index, bias, ambient) * 0.1183180;
	    shadowFactor += ShadowSampler(shadowMap, shadowCoord, vec2(+dx, +dy), index, bias, ambient) * 0.0947416;
		return shadowFactor;
	}
	else
	{
		return 1.0;
	}
}

float 
ShadowCubeSampler(
	const in samplerCubeArray shadowMap,
	const in vec4 shadowCoord, 
	const in vec4 coord4, 
	const in float bias,
	const in float ambient)
{
	float distance = texture(shadowMapOmni, coord4).r;
	if (shadowCoord.w > 0.0 && distance < shadowCoord.z - bias) 
	{
		return ambient;
	}
	else
	{
		return 1.0;
	}
}

float 
ShadowCubeFilterPCF1(
	const in samplerCubeArray shadowMap,
	const in vec4 shadowCoord, 
	const in vec4 coord4, 
	const in float bias,
	const in float ambient)
{
	if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0) 
	{
		return ShadowCubeSampler(shadowMap, shadowCoord, coord4, bias, ambient);
	}
	else
	{
		return 1.0;
	}
}

int 
GetCascadeShadowIndex(
	UBOLight light)
{
	int i;
	// Get cascade index for the current fragment's view position
	int cascadeIndex = 0;
	for(i = 0; i < light.count - 1; ++i) 
	{
		if(v_ViewPosition.z < uCascadeSplits.splits[light.idxMap + i]) 
		{	
			cascadeIndex = i + 1;
		}
	}
	return cascadeIndex;
}

int 
GetCoordCubeFaceId(
	const in vec3 coord)
{
	float absX = abs(coord.x);
	float absY = abs(coord.y);
	float absZ = abs(coord.z);
	
	float mag = max(max(absX, absY), absZ);
	if (mag == absX)
	{
		return (coord.x > 0.0) ? 0 : 1;
	}
	else if (mag == absY)
	{
		return (coord.y > 0.0) ? 2 : 3;
	}
	else if (mag == absZ)
	{
		return (coord.z > 0.0) ? 4 : 5;
	}
	
	return 0;
}

vec3 
GetLightShadowDirectional(
	UBOLight light,
	const in float dotNL)
{
	if (-1 == light.shadow)
	{
		return vec3(1.0);
	}
	else
	{
		int cascadeIndex = GetCascadeShadowIndex(light);

		int index = light.idxMap + cascadeIndex;
		float bias = max(light.biasMax * (1.0 - dotNL), light.biasMin);
		float ambient = light.ambient;
		
		mat4 ProjectionViewMat = uShadowProjectionViews.matrices[light.idxMat + index];
		vec4 shadowCoord = (biasMat * ProjectionViewMat) * vec4(v_Position, 1.0);

		float shadow = 1.0;

		// Projection Coord.
		shadowCoord = shadowCoord / shadowCoord.w;

		switch(light.mode)
		{
		case 1:
			shadow = ShadowFilterPCF1(shadowMapCascade, shadowCoord, index, bias, ambient);
			break;
		case 5:
			shadow = ShadowFilterPCF5(shadowMapCascade, shadowCoord, index, bias, ambient);
			break;
		case 9:
			shadow = ShadowFilterPCF9(shadowMapCascade, shadowCoord, index, bias, ambient);
			break;
		default:
			shadow = 1.0;
			break;
		}
		
		if (1 == light.debug)
		{			
			// cascadeIndex [0, 3]
			return gCascadesColor[cascadeIndex] * shadow;
		}
		else
		{
			return vec3(shadow);
		}
	}
}

vec3 
GetLightShadowPoint(
	UBOLight light,
	const in float dotNL)
{
	if (-1 == light.shadow)
	{
		return vec3(1.0);
	}
	else
	{
		int index = light.idxMap;
		float bias = max(light.biasMax * (1.0 - dotNL), light.biasMin);
		float ambient = light.ambient;
		
		vec3 lightVec = v_Position - light.position;

		int hCubeFaceId = GetCoordCubeFaceId(lightVec);
		mat4 ProjectionViewMat = uShadowProjectionViews.matrices[light.idxMat + index + hCubeFaceId];
		vec4 shadowCoord = (biasMat * ProjectionViewMat) * vec4(v_Position, 1.0);

		float shadow = 1.0;		
		
		// Projection Coord.
		shadowCoord = shadowCoord / shadowCoord.w;
		
		vec4 coord4 = vec4(lightVec, float(index / 6));
		shadow = ShadowCubeFilterPCF1(shadowMapOmni, shadowCoord, coord4, bias, ambient);
		
		if (1 == light.debug)
		{			
			return gCubeFaceColor[hCubeFaceId] * shadow;
		}
		else
		{
			return vec3(shadow);
		}
	}
}

vec3 
GetLightShadowSpot(
	UBOLight light,
	const in float dotNL)
{
	if (-1 == light.shadow)
	{
		return vec3(1.0);
	}
	else
	{
		int index = light.idxMap;
		float bias = max(light.biasMax * (1.0 - dotNL), light.biasMin);
		float ambient = light.ambient;

		mat4 ProjectionViewMat = uShadowProjectionViews.matrices[light.idxMat + index];
		vec4 shadowCoord = (biasMat * ProjectionViewMat) * vec4(v_Position, 1.0);

		float shadow = 1.0;

		// Projection Coord.
		shadowCoord = shadowCoord / shadowCoord.w;

		switch(light.mode)
		{
		case 1:
			shadow = ShadowFilterPCF1(shadowMapSpot, shadowCoord, index, bias, ambient);
			break;
		case 5:
			shadow = ShadowFilterPCF5(shadowMapSpot, shadowCoord, index, bias, ambient);
			break;
		case 9:
			shadow = ShadowFilterPCF9(shadowMapSpot, shadowCoord, index, bias, ambient);
			break;
		default:
			shadow = 1.0;
			break;
		}
		
		return vec3(shadow);
	}
}

vec3 
GetLightShadow(
	UBOLight light, 
	const in float dotNL)
{
	vec3 hShadow;
	
	switch(light.type)
	{
	case mmGLTFLightTypeDirectional:
		hShadow = GetLightShadowDirectional(light, dotNL);
		break;
	case mmGLTFLightTypePoint:
		hShadow = GetLightShadowPoint      (light, dotNL);
		break;
	case mmGLTFLightTypeSpot:
		hShadow = GetLightShadowSpot       (light, dotNL);
		break;
	default:
		hShadow = vec3(1.0);
		break;
	}
	
	return hShadow;
}
