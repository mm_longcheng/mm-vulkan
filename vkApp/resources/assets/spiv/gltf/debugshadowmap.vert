#version 450

layout (location = 0) out vec2 outUV;
layout (location = 1) out uint outCascadeIndex;

out gl_PerVertex {
	vec4 gl_Position;   
};

void main() 
{
	// clock wise
	// outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
	// counter clock wise
	outUV = vec2((gl_VertexIndex) & 2, (gl_VertexIndex << 1) & 2);
	outCascadeIndex = 0;
	gl_Position = vec4(outUV * 2.0f - 1.0f, 0.0f, 1.0f);
}
