// KHR_lights_punctual extension.
// see https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_lights_punctual
struct UBOLight
{
    vec3 direction;
    vec3 position;
    vec3 color;
    float range;
    float intensity;
    float innerConeCos;
    float outerConeCos;
    int type;
    int shadow;

    int count;
    int mode;
    float ambient;
    float biasMax;
    float biasMin;

    int debug;
    int idxMat;
    int idxMap;
};

const int mmGLTFLightTypeInvalid      = 0; //  Invalid
const int mmGLTFLightTypeDirectional  = 1; // "directional"
const int mmGLTFLightTypePoint        = 2; // "point"
const int mmGLTFLightTypeSpot         = 3; // "spot"

// https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_lights_punctual/README.md#range-property
float 
GetRangeAttenuation(
    const in float range, 
    const in float distance)
{
    if (range <= 0.0)
    {
        // negative range means unlimited
        return 1.0 / pow(distance, 2.0);
    }
    else
    {
        return max(min(1.0 - pow(distance / range, 4.0), 1.0), 0.0) / pow(distance, 2.0);
    }
}

// https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_lights_punctual/README.md#inner-and-outer-cone-angles
float 
GetSpotAttenuation(
    const in vec3  pointToLight, 
    const in vec3  spotDirection, 
    const in float outerConeCos, 
    const in float innerConeCos)
{
    float actualCos = dot(normalize(spotDirection), normalize(-pointToLight));
    if (actualCos > outerConeCos)
    {
        if (actualCos < innerConeCos)
        {
            return smoothstep(outerConeCos, innerConeCos, actualCos);
        }
        else
        {
            return 1.0;
        }
    }
    else
    {
        return 0.0;
    }
}

vec3 
GetLighIntensity(
    const in UBOLight light, 
    const in vec3 pointToLight)
{
    float rangeAttenuation = 1.0;
    float spotAttenuation = 1.0;

    if (light.type != mmGLTFLightTypeDirectional)
    {
        rangeAttenuation = GetRangeAttenuation(
            light.range, 
            length(pointToLight));
    }
    
    if (light.type == mmGLTFLightTypeSpot)
    {
        spotAttenuation = GetSpotAttenuation(
            pointToLight, 
            light.direction, 
            light.outerConeCos, 
            light.innerConeCos);
    }

    return rangeAttenuation * spotAttenuation * light.intensity * light.color;
}

vec3 
GetPointToLight(
    const in UBOLight light, 
    const in vec3 position)
{
    if (light.type != mmGLTFLightTypeDirectional)
    {
        return light.position - position;
    }
    else
    {
        return -light.direction;
    }
}
