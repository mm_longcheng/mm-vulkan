#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsDepth.glsl"
#include "vs-UBOSceneDepth.glsl"
#include "vs-UBOSkin.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec2  a_Texcoord[7];
layout (location = 8) in uvec4 a_Joints_0;
layout (location = 9) in vec4  a_Weights_0;

layout (location = 0) out vec2 v_Texcoord[7];

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 hSkinMat = mat4(0.0);
    hSkinMat += a_Weights_0.x * uSkin.Joints[a_Joints_0.x];
    hSkinMat += a_Weights_0.y * uSkin.Joints[a_Joints_0.y];
    hSkinMat += a_Weights_0.z * uSkin.Joints[a_Joints_0.z];
    hSkinMat += a_Weights_0.w * uSkin.Joints[a_Joints_0.w];

    mat4 ProjectionViewMat = GetLightProjectionView();
    mat4 hModelMat = pConstants.Model * hSkinMat;
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Texcoord = a_Texcoord;

    gl_Position = ProjectionViewMat * w_Position;
}
