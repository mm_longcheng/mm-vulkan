#include "fs-Constant.glsl"
#include "fs-PerturbNormal.glsl"
#include "fs-SRGB.glsl"
#include "fs-Lights.glsl"
// #include "fs-UBOScene.glsl"
#include "fs-UBOMaterial.glsl"
#include "fs-BRDF.glsl"
// #include "fs-ToneMap.glsl"
#include "fs-AlphaMode.glsl"

void main()
{
#ifndef HAS_TextureBaseColor
	// empty function.
#else
    vec4 baseColor = GetBaseColor();
    
    baseColor = AlphaModeColor(baseColor);
	
    if (baseColor.a < 0.5) 
    {
        discard;
    }
#endif//HAS_TextureBaseColor
}
