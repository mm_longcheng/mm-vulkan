#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsWhole.glsl"
#include "vs-UBOSceneWhole.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec3  a_Normal;
layout (location = 2) in vec4  a_Tangent;
layout (location = 3) in vec2  a_Texcoord[2];

layout (location = 0) out vec3 v_Position;
layout (location = 1) out vec2 v_Texcoord[2];
layout (location = 3) out mat3 v_TBN;

layout (location = 6) out vec3 v_ViewPosition;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 hProjectionViewMat = uScene.ProjectionView;
    mat4 hModelMat = pConstants.Model;
    mat3 hNormalMat = mat3(pConstants.Normal);
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Position = vec3(w_Position);
    v_Texcoord[0] = a_Texcoord[0];
    v_Texcoord[1] = a_Texcoord[1];

    // TBN matrix.
    vec3 n = normalize(hNormalMat * a_Normal);
    vec3 t = normalize(hNormalMat * vec3(a_Tangent.xyz));
    vec3 b = normalize(cross(n, t) * a_Tangent.w);
    v_TBN = mat3(t, b, n);

	v_ViewPosition = vec3(uScene.View * w_Position);
    gl_Position = hProjectionViewMat * w_Position;
}
