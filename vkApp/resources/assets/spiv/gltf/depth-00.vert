#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsDepth.glsl"
#include "vs-UBOSceneDepth.glsl"

layout (location = 0) in vec3  a_Position;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    mat4 ProjectionViewMat = GetLightProjectionView();
    mat4 hModelMat = pConstants.Model;
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    gl_Position = ProjectionViewMat * w_Position;
}
