layout (set = 0, binding = 1, std140) uniform UBOSceneFragment
{
	mat4 inverseView;
    vec3 camera;
    vec3 ambient;
    float exposure;
    int lightCount;
} uScene;

layout (set = 0, binding = 2, std430) readonly buffer UBOLights 
{
    UBOLight lights[];
} uLights;
