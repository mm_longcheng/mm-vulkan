// UBOScene whole
layout (set = 0, binding = 0, std140) uniform UBOSceneWhole
{
    mat4 ProjectionView;
    mat4 Projection;
    mat4 View;
} uScene;
