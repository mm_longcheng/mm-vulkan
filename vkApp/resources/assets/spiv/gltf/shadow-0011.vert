#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-UBOScene.glsl"
#include "vs-PushConstants.glsl"

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord[1];

layout (location = 0) out vec2 v_texcoord[1];

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 modelMat = constants.model;
    vec3 v_position = vec3(modelMat * vec4(a_position, 1.0));
    v_texcoord[0] = a_texcoord[0];
    gl_Position = uboScene.projection * uboScene.view * vec4(v_position, 1.0);
}
