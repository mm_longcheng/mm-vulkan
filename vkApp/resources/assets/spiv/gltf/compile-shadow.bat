@echo off

set Compiler=%VULKAN_SDK%/Bin/glslangValidator

:: call :build-spv shadow-0001.vert
:: call :build-spv shadow-0011.vert

:: call :build-spv shadow-001-00000.frag
:: call :build-spv shadow-011-00000.frag

call :build-spv debugshadowmap.vert
call :build-spv debugshadowmap.frag

:: call :build-spv 3d-0001.vert
:: call :build-spv 3d-0011.vert
:: call :build-spv 3d-001-00000.frag
:: call :build-spv 3d-011-00000.frag

call :build-spv depth-0-0.frag
call :build-spv depth-1-0.frag
call :build-spv depth-1-1.frag
call :build-spv depth-2-0.frag
call :build-spv depth-2-1.frag
call :build-spv depth-3-0.frag
call :build-spv depth-4-1.frag
call :build-spv depth-5-1.frag
call :build-spv depth-7-1.frag

call :build-spv depth-00.vert
call :build-spv depth-01.vert
call :build-spv depth-02.vert
call :build-spv depth-03.vert
call :build-spv depth-04.vert
call :build-spv depth-05.vert
call :build-spv depth-10.vert
call :build-spv depth-11.vert
call :build-spv depth-12.vert
call :build-spv depth-17.vert

GOTO :EOF

:build-spv
SETLOCAL
:: REM.
%Compiler% -V %~1 -o %~1.spv
ENDLOCAL
GOTO :EOF