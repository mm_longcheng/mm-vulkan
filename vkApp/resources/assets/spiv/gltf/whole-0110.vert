#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsWhole.glsl"
#include "vs-UBOSceneWhole.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec2  a_Texcoord[1];
layout (location = 2) in vec3  a_Color_0;

layout (location = 0) out vec3 v_Position;
layout (location = 1) out vec2 v_Texcoord[1];
layout (location = 2) out vec4 v_Color_0;

layout (location = 3) out vec3 v_ViewPosition;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 hProjectionViewMat = uScene.ProjectionView;
    mat4 hModelMat = pConstants.Model;
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Position = vec3(w_Position);
    v_Texcoord[0] = a_Texcoord[0];
    v_Color_0 = vec4(a_Color_0, 1.0);
	v_ViewPosition = vec3(uScene.View * w_Position);
    gl_Position = hProjectionViewMat * w_Position;
}
