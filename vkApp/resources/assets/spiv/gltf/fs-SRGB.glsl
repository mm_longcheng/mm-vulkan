// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LinearToSRGB(vec3 color)
{
    return pow(color, vec3(MM_1_DIV_GAMMA));
}

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 SRGBToLinear(vec3 srgbIn)
{
    return vec3(pow(srgbIn.xyz, vec3(MM_GAMMA)));
}

vec4 SRGBToLinear(vec4 srgbIn)
{
    return vec4(SRGBToLinear(srgbIn.xyz), srgbIn.w);
}
