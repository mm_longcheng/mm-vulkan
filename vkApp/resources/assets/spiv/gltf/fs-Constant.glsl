#define MM_E                  2.71828182845904523536028747135266250   /* e           */
#define MM_LOG2E              1.44269504088896340735992468100189214   /* log2(e)     */
#define MM_LOG10E             0.434294481903251827651128918916605082  /* log10(e)    */
#define MM_LN2                0.693147180559945309417232121458176568  /* loge(2)     */
#define MM_LN10               2.30258509299404568401799145468436421   /* loge(10)    */
#define MM_PI                 3.14159265358979323846264338327950288   /* pi          */
#define MM_PI_DIV_2           1.57079632679489661923132169163975144   /* pi/2        */
#define MM_PI_DIV_4           0.785398163397448309615660845819875721  /* pi/4        */
#define MM_1_DIV_PI           0.318309886183790671537767526745028724  /* 1/pi        */
#define MM_2_DIV_PI           0.636619772367581343075535053490057448  /* 2/pi        */
#define MM_2_DIV_SQRTPI       1.12837916709551257389615890312154517   /* 2/sqrt(pi)  */
#define MM_SQRT2              1.41421356237309504880168872420969808   /* sqrt(2)     */
#define MM_1_DIV_SQRT2        0.707106781186547524400844362104849039  /* 1/sqrt(2)   */

#define MM_EPSILON   1e-6

#define saturate( a ) clamp( a, 0.0, 1.0 )

#define pow2( a ) (a * a)

const float MM_GAMMA = 2.2;
const float MM_1_DIV_GAMMA = 1.0 / MM_GAMMA;

// enum mmGLTFAlphaMode_t
const int mmGLTFAlphaModeOpaque  = 0;// "OPAQUE"
const int mmGLTFAlphaModeMask    = 1;// "MASK"
const int mmGLTFAlphaModeBlend   = 2;// "BLEND"
