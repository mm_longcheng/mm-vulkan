// ACES tone map (faster approximation)
// see: https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 ToneMapACES_Narkowicz(const in vec3 color)
{
    const float A = 2.51;
    const float B = 0.03;
    const float C = 2.43;
    const float D = 0.59;
    const float E = 0.14;
    return clamp((color * (A * color + B)) / (color * (C * color + D) + E), 0.0, 1.0);
}

vec3 ToneMap(vec3 color)
{
    color *= uScene.exposure;

    color = ToneMapACES_Narkowicz(color);

    return LinearToSRGB(color);
}
