#version 450

#extension GL_GOOGLE_include_directive : enable

// Interface protocol macro definition.
#define HAS_VaryingPosition
// #define HAS_VaryingNormal
#define HAS_VaryingTBN
// #define HAS_VaryingColor
// #define HAS_VaryingTexcoord

// Material texture macro definition.
// #define HAS_TextureBaseColor
// #define HAS_TextureMetallicRoughness
// #define HAS_TextureNormal
// #define HAS_TextureOcclusion
// #define HAS_TextureEmissive

// Vertex input Protocol.
layout (location = 0) in vec3 v_Position;
layout (location = 1) in mat3 v_TBN;

layout (location = 4) in vec3 v_ViewPosition;

// Material sampler Protocol.

#include "fs-Main.glsl"
