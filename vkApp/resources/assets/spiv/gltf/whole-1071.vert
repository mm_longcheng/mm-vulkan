#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsWhole.glsl"
#include "vs-UBOSceneWhole.glsl"
#include "vs-UBOSkin.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec3  a_Normal;
layout (location = 2) in vec2  a_Texcoord[7];
layout (location = 9) in uvec4 a_Joints_0;
layout (location = 10) in vec4  a_Weights_0;

layout (location = 0) out vec3 v_Position;
layout (location = 1) out vec3 v_Normal;
layout (location = 2) out vec2 v_Texcoord[7];

layout (location = 9) out vec3 v_ViewPosition;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 hSkinMat = mat4(0.0);
    hSkinMat += a_Weights_0.x * uSkin.Joints[a_Joints_0.x];
    hSkinMat += a_Weights_0.y * uSkin.Joints[a_Joints_0.y];
    hSkinMat += a_Weights_0.z * uSkin.Joints[a_Joints_0.z];
    hSkinMat += a_Weights_0.w * uSkin.Joints[a_Joints_0.w];

    mat4 hProjectionViewMat = uScene.ProjectionView;
    mat4 hModelMat = pConstants.Model * hSkinMat;
    mat3 hNormalMat = transpose(inverse(mat3(hModelMat)));
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Position = vec3(w_Position);
    v_Normal = normalize(hNormalMat * a_Normal);
    v_Texcoord = a_Texcoord;
	v_ViewPosition = vec3(uScene.View * w_Position);
    gl_Position = hProjectionViewMat * w_Position;
}
