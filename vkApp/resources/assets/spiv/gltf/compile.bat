@echo off

set Compiler=%VULKAN_SDK%/Bin/glslangValidator

call :build-spv whole-0000.vert
call :build-spv whole-0001.vert
call :build-spv whole-0002.vert
call :build-spv whole-0010.vert
call :build-spv whole-0011.vert
call :build-spv whole-0012.vert
call :build-spv whole-0021.vert
call :build-spv whole-0022.vert
call :build-spv whole-0031.vert
call :build-spv whole-0042.vert
call :build-spv whole-0052.vert
call :build-spv whole-0100.vert
call :build-spv whole-0110.vert
call :build-spv whole-0201.vert
call :build-spv whole-0202.vert
call :build-spv whole-0211.vert
call :build-spv whole-0212.vert
call :build-spv whole-1001.vert
call :build-spv whole-1010.vert
call :build-spv whole-1011.vert
call :build-spv whole-1012.vert
call :build-spv whole-1022.vert
call :build-spv whole-1071.vert

call :build-spv whole-000-00000.frag
call :build-spv whole-001-00000.frag
call :build-spv whole-002-00000.frag
call :build-spv whole-010-00001.frag
call :build-spv whole-010-10001.frag
call :build-spv whole-011-00000.frag
call :build-spv whole-011-00001.frag
call :build-spv whole-011-00010.frag
call :build-spv whole-011-00100.frag
call :build-spv whole-011-01000.frag
call :build-spv whole-011-10000.frag
call :build-spv whole-011-01001.frag
call :build-spv whole-011-01100.frag
call :build-spv whole-011-01101.frag
call :build-spv whole-011-01111.frag
call :build-spv whole-011-11111.frag
call :build-spv whole-012-00000.frag
call :build-spv whole-012-00001.frag
call :build-spv whole-012-00011.frag
call :build-spv whole-012-00101.frag
call :build-spv whole-012-00111.frag
call :build-spv whole-012-01000.frag
call :build-spv whole-012-01100.frag
call :build-spv whole-012-01111.frag
call :build-spv whole-012-10001.frag
call :build-spv whole-012-10111.frag
call :build-spv whole-012-11111.frag
call :build-spv whole-012-11100.frag
call :build-spv whole-021-00001.frag
call :build-spv whole-021-01000.frag
call :build-spv whole-021-01001.frag
call :build-spv whole-021-01011.frag
call :build-spv whole-021-01101.frag
call :build-spv whole-022-01001.frag
call :build-spv whole-022-00111.frag
call :build-spv whole-022-01111.frag
call :build-spv whole-031-00000.frag
call :build-spv whole-042-01111.frag
call :build-spv whole-052-11111.frag
call :build-spv whole-071-00101.frag
call :build-spv whole-110-00000.frag
call :build-spv whole-110-00001.frag
call :build-spv whole-201-00000.frag
call :build-spv whole-202-00000.frag
call :build-spv whole-211-00000.frag
call :build-spv whole-212-00001.frag

GOTO :EOF

:build-spv
SETLOCAL
:: REM.
%Compiler% -V %~1 -o %~1.spv
ENDLOCAL
GOTO :EOF