#version 450

#extension GL_GOOGLE_include_directive : enable

// Interface protocol macro definition.
#define HAS_VaryingPosition
#define HAS_VaryingNormal
// #define HAS_VaryingTBN
// #define HAS_VaryingColor
// #define HAS_VaryingTexcoord

// Material texture macro definition.
// #define HAS_TextureBaseColor
// #define HAS_TextureMetallicRoughness
// #define HAS_TextureNormal
// #define HAS_TextureOcclusion
// #define HAS_TextureEmissive

// Vertex input Protocol.
layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec3 v_Normal;

layout (location = 2) in vec3 v_ViewPosition;

// Material sampler Protocol.

// #include "fs-Main.glsl"

#include "fs-Constant.glsl"
#include "fs-PerturbNormal.glsl"
#include "fs-SRGB.glsl"
#include "fs-Lights.glsl"
#include "fs-UBOScene.glsl"
#include "fs-UBOMaterial.glsl"
#include "fs-UBOShadow.glsl"
#include "fs-BRDF.glsl"
#include "fs-ToneMap.glsl"
#include "fs-AlphaMode.glsl"

layout (location = 0) out vec4 outColor;

void main()
{
	uint i;
	
    // view vector
    // world space.
    vec3 V = normalize(uScene.camera - v_Position);
    
    // normal vector
    vec3 N = GetNormal();

    vec4 baseColor = GetBaseColor();
    
    baseColor = AlphaModeColor(baseColor);
    
	// Get cascade index for the current fragment's view position
	uint cascadeIndex = GetCascadeIndex();
	
/*
#ifndef HAS_TextureMetallicRoughness
    float metallic = uMaterial.metallicFactor;
    float roughness = uMaterial.roughnessFactor;
#else
    vec2 value = GetMetallicRoughness();
    float metallic = value.x;
    float roughness = value.y;
#endif//HAS_TextureMetallicRoughness    
    
    vec3 color = vec3(0.0);
    vec3 ambient = uboScene.ambient;
    vec3 ambientColor = baseColor.rgb * ambient;
    
    const vec3 black = vec3(0.0);
    vec3 diffuseColor = mix(baseColor.rgb, black, metallic);
    const float f90 = 1.0;
    vec3 f0 = mix(vec3(0.04), baseColor.rgb, metallic);
    
    int i;
    for (i = 0;i < uScene.lightCount;++i)
    {
        UBOLight light = uboLights.lights[i];
        // vec3 L = GetPointToLight(light, v_position);
        // vec3 intensity = GetLighIntensity(light, L);
		vec3 L = -light.direction;
        vec3 intensity = light.intensity * light.color;
        vec3 material  = BRDF_Material(L, V, N, diffuseColor, f0, f90, roughness);
        color += material * intensity;
    }
    
    color += ambientColor;
*/

#ifdef HAS_TextureOcclusion
    color = GetOcclusionColor(color);
#endif//HAS_TextureOcclusion

#ifdef HAS_TextureEmissive
    color += GetEmissiveColor();
#endif//HAS_TextureEmissive

/*
	float ambient = 0.1;
    UBOLight light = uboLights.lights[0];
	
	vec3 L = normalize(-light.direction);
	// vec3 R = normalize(-reflect(L, N));
	vec3 color = max(dot(N, L), ambient) * baseColor.rgb;
	
	// Depth compare for shadowing
	vec4 shadowCoord = (biasMat * ubo.cascadeViewProjMat[cascadeIndex]) * vec4(v_position, 1.0);	
	float shadow = textureProj(shadowCoord / shadowCoord.w, vec2(0.0), cascadeIndex, N, L);
	
	color *= shadow;
*/

/*
	vec3 color;
	vec3 lightColor = vec3(0.4);
    UBOLight light = uboLights.lights[0];
	vec3 L = normalize(-light.direction);
    // Ambient
    vec3 ambient = 0.2 * lightColor;
    // Diffuse
    float diff = max(dot(L, N), 0.0);
    vec3 diffuse = diff * lightColor;
    // Specular
    float spec = 0.0;
    vec3 H = normalize(L + V);  
    spec = pow(max(dot(N, H), 0.0), 64.0);
    vec3 specular = spec * lightColor;  
	
	// float bias = 0.005;
	// float bias = max(0.05 * (1.0 - dot(N, L)), 0.005);
	// float bias = 0.000;
	// float bias = max(0.001 * (1.0 - dot(N, L)), 0.0);
	float bias = 0.0;

	// Depth compare for shadowing
	// vec4 shadowCoord = (biasMat * ubo.cascadeViewProjMat[cascadeIndex]) * vec4(v_position, 1.0);	
	vec4 shadowCoord = (ubo.cascadeViewProjMat[cascadeIndex]) * vec4(v_position, 1.0);	
	// float shadow = textureProj(shadowCoord / shadowCoord.w, vec2(0.0), cascadeIndex, bias);
	float shadow = filterPCF(shadowCoord / shadowCoord.w, cascadeIndex, bias);

	// color = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;
	// diffuse = vec3(0.0);
	color = (ambient + shadow * (diffuse + specular)) * baseColor.rgb;

	// color *= shadow;
*/	
	
	vec3 color;	
	
	vec3 lightColor = vec3(0.4);
	vec3 ambient = 0.0 * lightColor;

#ifndef HAS_TextureMetallicRoughness
    float metallic = uMaterial.metallicFactor;
    float roughness = uMaterial.roughnessFactor;
#else
    vec2 value = GetMetallicRoughness();
    float metallic = value.x;
    float roughness = value.y;
#endif//HAS_TextureMetallicRoughness    

	const vec3 black = vec3(0.0);
    vec3 diffuseColor = mix(baseColor.rgb, black, metallic);
    const float f90 = 1.0;
    vec3 f0 = mix(vec3(0.04), baseColor.rgb, metallic);

	UBOLight light = uLights.lights[0];
	vec3 L = normalize(-light.direction);

    vec3 H = normalize (V + L);
    float dotNV = clamp(dot(N, V), 0.0, 1.0);
    float dotNL = clamp(dot(N, L), 0.0, 1.0);
    float dotNH = clamp(dot(N, H), 0.0, 1.0);
    float dotVH = clamp(dot(V, H), 0.0, 1.0);
    float alpha = roughness * roughness;
    vec3        F = F_Schlick(f0, f90, dotVH);
    vec3  diffuse = BRDF_Diffuse(diffuseColor);
    vec3 specular = BRDF_Specular(dotNH, dotNL, dotNV, alpha);
    vec3 material = (1 - F) * diffuse + F * specular;
	
	// cascadeIndex = 1;
	// Depth shadow bias.
	float bias = max(uShadow.ShadowBiasMax * (1.0 - dotNL), uShadow.ShadowBiasMin);
	// float bias = 0.0;
	// Depth compare for shadowing
	vec4 shadowCoord = (biasMat * uShadow.CascadeProjectionView[cascadeIndex]) * vec4(v_Position, 1.0);	
	float shadow = ShadowFilterPCF(shadowCoord / shadowCoord.w, cascadeIndex, bias);
	
	color = (ambient * baseColor.rgb + shadow * (dotNL * material));
	
	if (1 == uShadow.ColorCascades)
	{
		const vec3 gCascadesColor[4] = 
		{
			{ 1.00f, 0.25f, 0.25f },
			{ 0.25f, 1.00f, 0.25f },
			{ 0.25f, 0.25f, 1.00f },
			{ 1.00f, 1.00f, 0.25f },
		};
		
		// cascadeIndex [0, 3]
		color *= gCascadesColor[cascadeIndex];
	}

    // Tone mapping
    outColor = vec4(ToneMap(color), baseColor.a);
}
