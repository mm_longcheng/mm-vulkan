#version 450

// layout (set = 0, binding = 0) uniform sampler2DArray shadowMap;
layout (set = 0, binding = 0) uniform samplerCubeArray shadowMap;
// layout (set = 0, binding = 0) uniform samplerCube shadowMap;
// layout (set = 0, binding = 0) uniform sampler2D colorMap;

layout (location = 0) in vec2 inUV;
layout (location = 1) flat in uint inCascadeIndex;

layout (location = 0) out vec4 outFragColor;

vec3 GetCubeMapSamplePosition(const in vec2 inUV)
{
	vec3 samplePos = vec3(0.0f);
	
	// Crude statement to visualize different cube map faces based on UV coordinates
	int x = int(floor(inUV.x / 0.25f));
	int y = int(floor(inUV.y / (1.0 / 3.0))); 
	if (y == 1) 
	{
		vec2 uv = vec2(inUV.x * 4.0f, (inUV.y - 1.0 / 3.0) * 3.0);
		uv = 2.0 * vec2(uv.x - float(x) * 1.0, uv.y) - 1.0;
		switch (x) 
		{
		case 0:	// NEGATIVE_X
			samplePos = vec3(-1.0f, uv.y, +uv.x);
			break;
		case 1: // POSITIVE_Z				
			samplePos = vec3(+uv.x, uv.y, +1.0f);
			break;
		case 2: // POSITIVE_X
			samplePos = vec3(+1.0f, uv.y, -uv.x);
			break;				
		case 3: // NEGATIVE_Z
			samplePos = vec3(-uv.x, uv.y, -1.0f);
			break;
		}
	} 
	else 
	{
		if (x == 1) 
		{ 
			vec2 uv = vec2((inUV.x - 0.25) * 4.0, (inUV.y - float(y) / 3.0) * 3.0);
			uv = 2.0 * uv - 1.0;
			switch (y) 
			{
			case 0: // NEGATIVE_Y
				samplePos = vec3(uv.x, -1.0f, +uv.y);
				break;
			case 2: // POSITIVE_Y
				samplePos = vec3(uv.x, +1.0f, -uv.y);
				break;
			}
		}
	}
	
	return samplePos;
}

void main() 
{
/*
	float depth = texture(shadowMap, vec3(inUV, float(inCascadeIndex))).r;
	float a = (1.0 == depth) ? 0.0 : 1.0;
	outFragColor = vec4(vec3(depth), a);
	// outFragColor = vec4(vec3(depth), 1.0);
*/

/*
	float depth = 0.0;
	depth += texture(shadowMap, vec3(inUV, float(0))).r;
	depth += texture(shadowMap, vec3(inUV, float(1))).r;
	depth += texture(shadowMap, vec3(inUV, float(2))).r;
	depth += texture(shadowMap, vec3(inUV, float(3))).r;
	depth /= 4.0;
	float a = (1.0 == depth) ? 0.0 : 1.0;
	outFragColor = vec4(vec3(depth), a);
*/

/*
*/
	//float depth = 0.0;
	vec3 samplePos = GetCubeMapSamplePosition(inUV);
	if ((samplePos.x != 0.0f) && (samplePos.y != 0.0f)) 
	{
		// float dist = length(texture(shadowMap, vec4(samplePos, 0.0)).xyz) * 0.005;
		float dist = texture(shadowMap, vec4(samplePos, 0.0)).r;
		// float dist = length(texture(shadowMap, samplePos).xyz) * 0.005;
		outFragColor = vec4(vec3(dist), 1.0);
		// outFragColor = texture(shadowMap, vec4(samplePos, 0.0));
		// outFragColor = texture(shadowMap, samplePos);
	}
	else
	{
		outFragColor = vec4(0.0, 0.0, 0.0, 0.0);
	}

	// float a = (1.0 == depth) ? 0.0 : 1.0;
	// outFragColor = vec4(vec3(depth), a);

/*
	float depth = 0.0;
	depth += texture(shadowMap, vec3(inUV, float(0))).r;
	float a = (1.0 == depth) ? 0.0 : 1.0;
	outFragColor = vec4(vec3(depth), a);
*/

/*
    outFragColor = texture(colorMap, inUV);
*/

	// outFragColor = vec4(1.0, 0.0, 0.0, 1.0);
}