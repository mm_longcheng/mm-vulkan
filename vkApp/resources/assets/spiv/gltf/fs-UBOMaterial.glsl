struct UBOTextureInfo
{
    int index;
    int texCoord;
};

layout (set = 1, binding = 0, std140) uniform UBOMaterial 
{
    UBOTextureInfo baseColorTexture;
    UBOTextureInfo metallicRoughnessTexture;
    UBOTextureInfo normalTexture;
    UBOTextureInfo occlusionTexture;
    UBOTextureInfo emissiveTexture;
    vec4 baseColorFactor;
    vec3 emissiveFactor;
    float metallicFactor;
    float roughnessFactor;
    float scale;
    float strength;
    float alphaCutoff;
    int alphaMode;
    int doubleSided;
} uMaterial;

// BaseColor
#ifndef HAS_VaryingColor

#ifndef HAS_TextureBaseColor

vec4 GetBaseColor()
{
    return uMaterial.baseColorFactor;
}

#else

#ifndef HAS_VaryingTexcoord
vec4 GetBaseColor()
{
    return uMaterial.baseColorFactor;
}
#else
vec4 GetBaseColor()
{
    vec4 color;
    UBOTextureInfo t = uMaterial.baseColorTexture;
    if (-1 == t.index)
    {
        color = uMaterial.baseColorFactor;
    }
    else
    {
        color = texture(baseColorTexture, v_Texcoord[t.texCoord]);
        color = uMaterial.baseColorFactor * SRGBToLinear(color);
    }
    return color;
}
#endif//HAS_VaryingTexcoord

#endif//HAS_TextureBaseColor

#else

#ifndef HAS_TextureBaseColor

vec4 GetBaseColor()
{
    return uMaterial.baseColorFactor * v_Color_0;
}

#else

#ifndef HAS_VaryingTexcoord
vec4 GetBaseColor()
{
    return uMaterial.baseColorFactor * v_Color_0;
}
#else
vec4 GetBaseColor()
{
    vec4 color;
    UBOTextureInfo t = uMaterial.baseColorTexture;
    if (-1 == t.index)
    {
        color = uMaterial.baseColorFactor;
    }
    else
    {
        color = texture(baseColorTexture, v_Texcoord[t.texCoord]);
        color = uMaterial.baseColorFactor * SRGBToLinear(color);
    }
    return color * v_Color_0;
}
#endif//HAS_VaryingTexcoord

#endif//HAS_TextureBaseColor

#endif//HAS_VaryingColor

// Normal
#ifndef HAS_TextureNormal

#ifndef HAS_VaryingNormal

#ifndef HAS_VaryingTBN

#ifndef HAS_VaryingPosition
vec3 GetNormal()
{
    return vec3(1.0);
}
#else
// 0 NotNORMAL     NotTANGENT  NotTextureNormal
vec3 GetNormal()
{
    // Use flat normal
    return normalize(cross(dFdx(v_Position), dFdy(v_Position)));
}
#endif//HAS_VaryingPosition

#else

// x NotNORMAL     HasTANGENT  NotTextureNormal
vec3 GetNormal()
{
    // Use TBN normal
    return normalize(v_TBN[2]);
}

#endif//HAS_VaryingTBN

#else

#ifndef HAS_VaryingTBN
// 1 HasNORMAL     NotTANGENT  NotTextureNormal
vec3 GetNormal()
{
    // Use varying normal
    return normalize(v_Normal);
}
#else
// 2 HasNORMAL     HasTANGENT  NotTextureNormal
vec3 GetNormal()
{
    // Use TBN normal
    return normalize(v_TBN[2]);
}
#endif//HAS_VaryingTBN

#endif//HAS_VaryingNormal

#else

#ifndef HAS_VaryingNormal

#ifndef HAS_VaryingTBN

#ifndef HAS_VaryingPosition
vec3 GetNormal()
{
    return vec3(1.0f);
}
#else
// 0 NotNORMAL     NotTANGENT  HasTextureNormal
vec3 GetNormal()
{
    vec3 normal;
    UBOTextureInfo t = uMaterial.normalTexture;
    if (-1 == t.index)
    {
        // Use flat normal
        normal = normalize(cross(dFdx(v_Position), dFdy(v_Position)));
    }
    else
    {
        vec2 vUv = v_Texcoord[t.texCoord];
        normal = texture(normalTexture, vUv).rgb;
        normal = normalize(normal * 2.0f - 1.0f);
    }
    return normal;
}
#endif//HAS_VaryingPosition

#else
// x NotNORMAL     HasTANGENT  HasTextureNormal
vec3 GetNormal()
{
    vec3 normal;
    UBOTextureInfo t = uMaterial.normalTexture;
    if (-1 == t.index)
    {
        // Use TBN normal
        normal = normalize(v_TBN[2]);
    }
    else
    {
        vec2 vUv = v_Texcoord[t.texCoord];
        normal = texture(normalTexture, vUv).rgb;
        normal = normalize(normal * 2.0f - 1.0f);
        normal = normalize(v_TBN * normal);
    }
    return normal;
}

#endif//HAS_VaryingTBN

#else

#ifndef HAS_VaryingTBN
// 1 HasNORMAL     NotTANGENT  HasTextureNormal
vec3 GetNormal()
{
    vec3 normal;
    UBOTextureInfo t = uMaterial.normalTexture;
    if (-1 == t.index)
    {
        // Use varying normal
        normal = normalize(v_Normal);
    }
    else
    {
        // face direction.
        float faceDirection = gl_FrontFacing ? 1.0 : - 1.0;

        vec2 vUv = v_Texcoord[t.texCoord];
        normal = texture(normalTexture, vUv).rgb;
        normal = normalize(normal * 2.0f - 1.0f);
        normal = PerturbNormal2Arb(-uScene.camera, v_Normal, normal, faceDirection, vUv);
    }
    return normal;
}
#else
// 2 HasNORMAL     HasTANGENT  HasTextureNormal
vec3 GetNormal()
{
    vec3 normal;
    UBOTextureInfo t = uMaterial.normalTexture;
    if (-1 == t.index)
    {
        // Use varying normal
        normal = normalize(v_Normal);
    }
    else
    {
        normal = texture(normalTexture, v_Texcoord[t.texCoord]).rgb;
        normal = normalize(normal * 2.0f - 1.0f);
        normal = normalize(v_TBN * normal);
    }
    return normal;
}
#endif//HAS_VaryingTBN

#endif//HAS_VaryingNormal

#endif//HAS_TextureNormal

// Metallic Roughness
#ifndef HAS_TextureMetallicRoughness
vec2 GetMetallicRoughness()
{
    vec2 value;
    value.x = uMaterial.metallicFactor;
    value.y = uMaterial.roughnessFactor;
    return value;
}
#else
vec2 GetMetallicRoughness()
{
    vec2 value;
    UBOTextureInfo t = uMaterial.metallicRoughnessTexture;
    if (-1 == t.index)
    {
        value.x = uMaterial.metallicFactor;
        value.y = uMaterial.roughnessFactor;
    }
    else
    {
        value = texture(metallicRoughnessTexture, v_Texcoord[t.texCoord]).bg;
    }
    return value;
}
#endif//HAS_TextureMetallicRoughness

// Occlusion
#ifndef HAS_TextureOcclusion
vec3 GetOcclusionColor(vec3 color)
{
    return color;
}
#else
vec3 GetOcclusionColor(vec3 color)
{
    float occlusion;
    UBOTextureInfo t = uMaterial.occlusionTexture;
    if (-1 == t.index)
    {
        occlusion = 1.0;
    }
    else
    {
        occlusion = texture(occlusionTexture, v_Texcoord[t.texCoord]).r;
        color = mix(color, color * occlusion, uMaterial.strength);
    }
    return color;
}
#endif//HAS_TextureOcclusion

// Emissive
#ifndef HAS_TextureEmissive
vec3 GetEmissiveColor()
{
    return vec3(0.0);
}
#else
vec3 GetEmissiveColor()
{
    vec3 color;
    UBOTextureInfo t = uMaterial.emissiveTexture;
    if (-1 == t.index)
    {
        color = vec3(0.0);
    }
    else
    {
        color = texture(emissiveTexture, v_Texcoord[t.texCoord]).rgb;
        color = uMaterial.emissiveFactor * SRGBToLinear(color);
    }
    return color;
}
#endif//HAS_TextureEmissive
