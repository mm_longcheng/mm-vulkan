// UBOScene depth
layout (set = 0, binding = 0, std430) readonly buffer UBOSceneDepth
{
    mat4 ProjectionView[];
} uSceneDepth;

mat4 
GetLightProjectionView()
{
	// Adreno: QUALCOMM EV031.22.00.01
	//
	// At switch case return mat4 will crash.
	// We must predefinition mat4 m;
	
	int idx = pConstants.idxMat + pConstants.idxMap;
	return uSceneDepth.ProjectionView[idx];
}
