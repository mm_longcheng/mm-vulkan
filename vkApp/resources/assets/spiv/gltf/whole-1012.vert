#version 450

#extension GL_GOOGLE_include_directive : enable

#include "vs-PushConstantsWhole.glsl"
#include "vs-UBOSceneWhole.glsl"
#include "vs-UBOSkin.glsl"

layout (location = 0) in vec3  a_Position;
layout (location = 1) in vec3  a_Normal;
layout (location = 2) in vec4  a_Tangent;
layout (location = 3) in vec2  a_Texcoord[1];
layout (location = 4) in uvec4 a_Joints_0;
layout (location = 5) in vec4  a_Weights_0;

layout (location = 0) out vec3 v_Position;
layout (location = 1) out vec2 v_Texcoord[1];
layout (location = 2) out mat3 v_TBN;

layout (location = 5) out vec3 v_ViewPosition;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    // world space.
    mat4 hSkinMat = mat4(0.0);
    hSkinMat += a_Weights_0.x * uSkin.Joints[a_Joints_0.x];
    hSkinMat += a_Weights_0.y * uSkin.Joints[a_Joints_0.y];
    hSkinMat += a_Weights_0.z * uSkin.Joints[a_Joints_0.z];
    hSkinMat += a_Weights_0.w * uSkin.Joints[a_Joints_0.w];

    mat4 hProjectionViewMat = uScene.ProjectionView;
    mat4 hModelMat = pConstants.Model * hSkinMat;
    mat3 hNormalMat = transpose(inverse(mat3(hModelMat)));
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Position = vec3(w_Position);
    v_Texcoord[0] = a_Texcoord[0];
    
    // TBN matrix.
    vec3 n = normalize(hNormalMat * a_Normal);
    vec3 t = normalize(hNormalMat * vec3(a_Tangent.xyz));
    vec3 b = normalize(cross(n, t) * a_Tangent.w);
    v_TBN = mat3(t, b, n);
    
	v_ViewPosition = vec3(uScene.View * w_Position);
    gl_Position = hProjectionViewMat * w_Position;
}
