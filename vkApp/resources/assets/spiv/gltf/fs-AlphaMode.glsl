
vec4 AlphaModeColor(vec4 baseColor)
{
    switch(uMaterial.alphaMode)
    {
    case mmGLTFAlphaModeOpaque:
    {
        baseColor.a = 1.0;
    }
    break;
    case mmGLTFAlphaModeMask:
    {
        // Late discard to avoid samplig artifacts. 
        // See https://github.com/KhronosGroup/glTF-Sample-Viewer/issues/267
        if (baseColor.a < uMaterial.alphaCutoff)
        {
            discard;
        }
        baseColor.a = 1.0;
    }
    break;
    case mmGLTFAlphaModeBlend:
    default:
    break;
    }

    return baseColor;
}