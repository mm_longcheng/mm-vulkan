#include "fs-Constant.glsl"
#include "fs-PerturbNormal.glsl"
#include "fs-SRGB.glsl"
#include "fs-Lights.glsl"
#include "fs-UBOScene.glsl"
#include "fs-UBOMaterial.glsl"
#include "fs-UBOShadow.glsl"
#include "fs-CubeMap.glsl"
#include "fs-BRDF.glsl"
#include "fs-ToneMap.glsl"
#include "fs-AlphaMode.glsl"

layout (location = 0) out vec4 outColor;

void main()
{
    // view vector
    // world space.
    vec3 V = normalize(uScene.camera - v_Position);
    
    // normal vector
    vec3 N = GetNormal();

    vec4 baseColor = GetBaseColor();
    
    baseColor = AlphaModeColor(baseColor);
    
#ifndef HAS_TextureMetallicRoughness
    float metallic = uMaterial.metallicFactor;
    float roughness = uMaterial.roughnessFactor;
#else
    vec2 value = GetMetallicRoughness();
    float metallic = value.x;
    float roughness = value.y;
#endif//HAS_TextureMetallicRoughness    
        
    vec3 color = vec3(0.0);
    vec3 ambient = uScene.ambient;
    vec3 ambientColor = baseColor.rgb * ambient;
    
    const vec3 black = vec3(0.0);
    vec3 diffuseColor = mix(baseColor.rgb, black, metallic);
    const float f90 = 1.0;
    vec3 f0 = mix(vec3(0.04), baseColor.rgb, metallic);
	
    int i;
    for (i = 0;i < uScene.lightCount;++i)
    {
        UBOLight light = uLights.lights[i];
        vec3 pointToLight = GetPointToLight(light, v_Position);
        vec3 L = normalize(pointToLight);

		// material color.
		vec3 H = normalize (V + L);
		float dotNV = clamp(dot(N, V), 0.0, 1.0);
		float dotNL = clamp(dot(N, L), 0.0, 1.0);
		float dotNH = clamp(dot(N, H), 0.0, 1.0);
		float dotVH = clamp(dot(V, H), 0.0, 1.0);
		float alpha = roughness * roughness;
		vec3        F = F_Schlick(f0, f90, dotVH);
		vec3  diffuse = BRDF_Diffuse(diffuseColor);
		vec3 specular = BRDF_Specular(dotNH, dotNL, dotNV, alpha);
		vec3 material = (1.0 - F) * diffuse + F * specular;

		// shadow.
		vec3 shadow = GetLightShadow(light, dotNL);

		// light intensity.
		vec3 intensity = GetLighIntensity(light, pointToLight);
		// vec3 intensity = vec3(1.0);

		color += (dotNL) * (shadow * material * intensity);
    }
    
	/*
	vec3 cI = normalize(v_Position);
	vec3 cR = reflect(cI, N);

	// cR = vec3(ubo.invModel * vec4(cR, 0.0));
	// Convert cubemap coordinates into Vulkan coordinate space
	cR.xy *= -1.0;

	color = texture(shadowMapOmni, vec4(cR, 0.0), 0.0).rrr;
	*/
	
    color += ambientColor;

#ifdef HAS_TextureOcclusion
    color = GetOcclusionColor(color);
#endif//HAS_TextureOcclusion

#ifdef HAS_TextureEmissive
    color += GetEmissiveColor();
#endif//HAS_TextureEmissive
		
    // Tone mapping
    outColor = vec4(ToneMap(color), baseColor.a);
}
