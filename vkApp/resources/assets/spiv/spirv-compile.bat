@echo off

set Compiler=%VULKAN_SDK%/Bin/glslangValidator

call :build-spv spirv-1.frag
call :build-spv spirv-1.vert

call :build-spv spirv-ui.frag
call :build-spv spirv-ui.vert

call :build-spv spirv-2d.frag
call :build-spv spirv-2d-gray.frag
call :build-spv spirv-2d.vert

call :build-spv spirv-2dnes.frag
call :build-spv spirv-2dnes.vert

call :build-spv spirv-3d.frag
call :build-spv spirv-3d.vert

call :build-spv spirv-3dx.frag
call :build-spv spirv-3dx.vert

call :build-spv spirv-3dy.frag
call :build-spv spirv-3dy.vert

GOTO :EOF

:build-spv
SETLOCAL
:: REM.
%Compiler% -V %~1 -o %~1.spv
ENDLOCAL
GOTO :EOF