#version 450

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec3 a_Normal;
layout (location = 2) in vec2 a_Texcoord;

layout (set = 0, binding = 0, std140) uniform UBOScene
{
    mat4 ProjectionView;
} uScene;

layout (push_constant, std140) uniform PushConstants
{
    mat4 Normal;
    mat4 Model;
} pConstants;

layout (location = 0) out vec3 v_Position;
layout (location = 1) out vec2 v_Texcoord;
layout (location = 2) out vec3 v_Normal;

layout (location = 3) out vec3 v_LocalPosition;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    mat4 hModelMat = pConstants.Model;
    mat3 hNormalMat = mat3(pConstants.Normal);
    vec4 w_Position = hModelMat * vec4(a_Position, 1.0);
    v_Position = vec3(w_Position);
    v_Texcoord = a_Texcoord;
    v_Normal = normalize(hNormalMat * a_Normal);
    v_LocalPosition = a_Position;
    gl_Position = uScene.ProjectionView * w_Position;
}
