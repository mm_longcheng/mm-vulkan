#version 450

//layout (set = 0, binding = 1) uniform sampler2D texture_0;
layout (set = 0, binding = 1) uniform sampler3D shader_noise_tex;

layout (set = 0, binding = 2, std140) uniform UBOSceneFragment
{
    mat4 inverseView;
    vec3 camera;
    vec3 ambient;
    float exposure;
    int lightCount;
    float time;
    float scale;
} uScene;

layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec2 v_Texcoord;
layout (location = 2) in vec3 v_Normal;

layout (location = 3) in vec3 v_LocalPosition;

layout (location = 0) out vec4 outColor;

/*
 * GLSL Shader functions for fast fake Perlin 3D noise
 *
 * The required shader_noise_tex texture can be generated using the
 * ShaderNoiseTexture class.  It is a toroidal tiling 3D texture with each texel
 * containing two 16-bit noise source channels. The shader permutes the source
 * texture values by combining the channels such that the noise repeats at a
 * much larger interval than the input texture.
 */

//uniform sampler3D shader_noise_tex;
const float twopi = 3.1415926 * 2.0;

/* Simple perlin noise work-alike */
float
pnoise(vec3 position)
{
    vec4 hi = 2.0 * texture(shader_noise_tex, position.xyz) - 1.0;
    vec4 lo = 2.0 * texture(shader_noise_tex, position.xyz / 9.0) - 1.0;
    return hi.r * cos(twopi * lo.r) + hi.g * sin(twopi * lo.r);
}

/* Multi-octave fractal brownian motion perlin noise */
float
fbmnoise(vec3 position, int octaves)
{
    float m = 1.0;
    vec3 p = position;
    vec4 hi = vec4(0.0);
    /* XXX Loops may not work correctly on all video cards */
    for (int x = 0; x < octaves; x++)
    {
        hi += (2.0 * texture(shader_noise_tex, p.xyz) - 1.0) * m;
        p *= 2.0;
        m *= 0.5;
    }
    vec4 lo = 2.0 * texture(shader_noise_tex, position.xyz / 9.0) - 1.0;
    return hi.r * cos(twopi * lo.r) + hi.g * sin(twopi * lo.r);
}

/* Multi-octave turbulent noise */
float
fbmturbulence(vec3 position, int octaves)
{
    float m = 1.0;
    vec3 p = position;
    vec4 hi = vec4(0.0);
    /* XXX Loops may not work correctly on all video cards */
    for (int x = 0; x < octaves; x++)
    {
        hi += abs(2.0 * texture(shader_noise_tex, p.xyz) - 1.0) * m;
        p *= 2.0;
        m *= 0.5;
    }
    vec4 lo = texture(shader_noise_tex, position.xyz / 9.0);
    return 2.0 * mix(hi.r, hi.g, cos(twopi * lo.r) * 0.5 + 0.5) - 1.0;
}

void main()
{
    // vec3 N = normalize(v_Normal);
    // outColor = texture(texture_0, v_Texcoord);
    float time = uScene.time;
    vec3 position = v_LocalPosition * uScene.scale;

    float t = fbmnoise(position * 2.0 + time * 0.001, 2);
    vec3 turb = vec3(sin(t * 0.5), cos(t), 0) * 0.04;
    float h = fbmturbulence(turb + position * 0.5 + time * 0.002, 6) * 1.35;
    h = h * h;

    // outColor = vec4(h, h, h, 1.0);

    vec4 color = vec4(1.0);

    vec3 lightvec = uScene.camera - v_Position;
    vec4 light_ambient = vec4(0.001, 0.001, 0.001, 1.0);
    vec4 light_diffuse = vec4(2.0, 2.0, 2.0, 1.0);
    vec4 light_specular = vec4(0.001, 0.001, 0.001, 1.0);
    vec3 light_position = vec3(0.0, 3.0, 4.0);

    vec3 pointToLight = light_position - v_Position;

    vec3 L = normalize(pointToLight);
    vec3 V = normalize(lightvec);
    vec3 N = normalize(v_Normal);
    vec3 H = normalize (V + L);
    float intensity = max(0.0, dot(N, normalize(lightvec)));
    float glare = max(0.0, dot(N, H));
    vec4 ambient = light_ambient;
    vec4 diffuse = light_diffuse * intensity;
    vec4 specular = light_specular * pow(glare, 64.0);
    outColor = (ambient + diffuse + specular) * color * h;
    
    
//    vec3 lightvec = uScene.camera - v_Position;
//    vec4 light_ambient = vec4(0.1, 0.1, 0.1, 1.0);
//    vec4 light_diffuse = vec4(0.9, 0.9, 0.9, 1.0);
//    vec4 light_specular = vec4(1.5, 1.5, 1.5, 1.0);
//    vec3 light_position = vec3(-250, 250, 500);
//    
//    vec3 pointToLight = light_position - v_Position;
//    vec3 position = v_LocalPosition;//  * uScene.scale;
//    vec3 normal = v_Normal;
//    
//    vec3 L = normalize(pointToLight);
//    vec3 V = normalize(lightvec);
//    // vec3 N = normalize(v_Normal);
//    vec3 H = normalize (V + L);
//    
//    const vec4 color = vec4(0.3, 0.7, 0.3, 1.0);
//    const float height = 0.045;
//    const int detail = 1;
//    
//    const float offset = 0.05; // offset for computing surface derivative
//
//    /* Calculate the normal resulting from the bump surface */
//    float h = fbmnoise(position, detail);
//    float f0 = h * height;
//    float fx = fbmnoise(position + vec3(offset, 0.0, 0.0), detail) * height;
//    float fy = fbmnoise(position + vec3(0.0, offset, 0.0), detail) * height;
//    float fz = fbmnoise(position + vec3(0.0, 0.0, offset), detail) * height;
//    vec3 df = vec3((fx - f0) / offset, (fy - f0) / offset, (fz - f0) / offset);
//    vec3 N = normalize(normal - df);
//
//    /* Calculate the lighting */
//    float intensity = max(0.0, dot(N, normalize(lightvec)));
//    float glare = max(0.0, dot(N, H));
//    h = (h + 1.0) * 0.5;
//    vec4 ambient = light_ambient * h;
//    vec4 diffuse = light_diffuse * intensity;
//    vec4 specular = light_specular * pow(glare, 64.0);
//    outColor = (diffuse + specular) * color + ambient;
}
