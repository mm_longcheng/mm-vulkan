#version 400
 
// Vulkan中想使用着色器进行开发，需要开启这两个扩展
// 启动GL_ARB_separate_shader_objects
#extension GL_ARB_separate_shader_objects : enable
// 启动GL_ARB_shading_language_420pack
#extension GL_ARB_shading_language_420pack : enable
 
// 声明一致块myBufferVals，包含接收总变换矩阵的成员mvp
// 一致块
layout (std140,set = 0, binding = 0) uniform bufferVals 
{
	//总变换矩阵
    mat4 mvp;
} myBufferVals;

// 传入的物体坐标系顶点位置 
layout (location = 0) in vec3 pos;
// 传入的顶点颜色
layout (location = 1) in vec3 color;
// 传到片元着色器的顶点颜色
layout (location = 0) out vec3 vcolor;
 
// 定义了输出接口块
out gl_PerVertex 
{
	// 顶点最终位置
	vec4 gl_Position;
};
 
// 顶点着色器的主方法
void main() 
{
	// 计算最终顶点位置：最终变换矩阵*物体坐标系下的顶点坐标
    gl_Position = myBufferVals.mvp * vec4(pos,1.0);
	// 传递顶点颜色给片元着色器
    vcolor=color;
}
