#version 450

#extension GL_GOOGLE_include_directive : enable
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#include "constant.glsl"
#include "param-vert.glsl"
#include "animation.glsl"

out gl_PerVertex 
{
	vec4 gl_Position;   
};

vec4 getPosition()
{
    vec4 pos = vec4(a_position, 1.0);

#ifdef USE_MORPHING
    pos += getTargetPosition();
#endif

#ifdef USE_SKINNING
    pos = getSkinningMatrix() * pos;
#endif

    return pos;
}

#ifdef HAS_NORMAL_VEC3
vec3 getNormal()
{
    vec3 normal = a_normal;

#ifdef USE_MORPHING
    normal += getTargetNormal();
#endif

#ifdef USE_SKINNING
    normal = mat3(getSkinningNormalMatrix()) * normal;
#endif

    return normalize(normal);
}
#endif


#ifdef HAS_TANGENT_VEC4
vec3 getTangent()
{
    vec3 tangent = a_tangent.xyz;

#ifdef USE_MORPHING
    tangent += getTargetTangent();
#endif

#ifdef USE_SKINNING
    tangent = mat3(getSkinningMatrix()) * tangent;
#endif

    return normalize(tangent);
}
#endif


void main()
{
    vec4 pos = u_ModelMatrix * getPosition();
    v_position = vec3(pos.xyz) / pos.w;

    if (1 == u_Primitive.has_NORMAL_VEC3)
    {
        if (1 == u_Primitive.has_TANGENT_VEC4)
        {
            vec3 tangent = getTangent();
            vec3 normalW = normalize(vec3(u_NormalMatrix * vec4(getNormal(), 0.0)));
            vec3 tangentW = normalize(vec3(u_ModelMatrix * vec4(tangent, 0.0)));
            vec3 bitangentW = cross(normalW, tangentW) * a_tangent.w;
            v_TBN = mat3(tangentW, bitangentW, normalW);

            v_normal = vec3(0.0);
        }
        else
        {
            v_normal = normalize(vec3(u_NormalMatrix * vec4(getNormal(), 0.0)));
            v_TBN = mat3(0.0);
        }
    }

    v_texcoord_0 = vec2(0.0, 0.0);
    v_texcoord_0 = vec2(0.0, 0.0);

#ifdef HAS_TEXCOORD_0_VEC2
    if (1 == u_Primitive.has_TEXCOORD_0_VEC2)
    {
        v_texcoord_0 = a_texcoord_0;
    }
#endif

#ifdef HAS_TEXCOORD_1_VEC2
    if (1 == u_Primitive.has_TEXCOORD_1_VEC2)
    {
        v_texcoord_1 = a_texcoord_1;
    }
#endif

#if defined(HAS_COLOR_0_VEC3) || defined(HAS_COLOR_0_VEC4)
    if (1 == u_Primitive.has_COLOR_0_VEC3 || 1 == u_Primitive.has_COLOR_0_VEC4)
    {
        v_color_0 = a_color_0;
    }
#endif

    gl_Position = u_ViewProjectionMatrix * pos;
}
