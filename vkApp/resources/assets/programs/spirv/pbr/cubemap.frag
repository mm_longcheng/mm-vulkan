#version 450

#extension GL_GOOGLE_include_directive : enable
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

precision highp float;


layout (set = 0, binding = 1) uniform UBOScene
{
	float u_EnvIntensity;
	float u_EnvBlurNormalized;
	int u_MipCount;

    // tonemap
    float u_Exposure;
};

#include "tonemapping.glsl"

layout (set = 0, binding = 2) uniform samplerCube u_GGXEnvSampler;

layout (location = 0) out vec4 FragColor;

layout (location = 0) in vec3 v_TexCoords;


void main()
{
    vec4 color = textureLod(u_GGXEnvSampler, v_TexCoords, u_EnvBlurNormalized * float(u_MipCount - 1)) * u_EnvIntensity;

#ifdef LINEAR_OUTPUT
    FragColor = color.rgba;
#else
    FragColor = vec4(toneMap(color.rgb), color.a);
#endif
}
