@echo off

set Compiler=%VULKAN_SDK%/Bin/glslangValidator

call :build-spv pbr.frag
call :build-spv pbr.vert

call :build-spv fullscreen.vert
call :build-spv ibl_filtering.frag

call :build-spv cubemap.vert
call :build-spv cubemap.frag

call :build-spv panorama_to_cubemap.frag

GOTO :EOF

:build-spv
SETLOCAL
:: REM.
%Compiler% -I./ -V %~1 -o %~1.spv
ENDLOCAL
GOTO :EOF