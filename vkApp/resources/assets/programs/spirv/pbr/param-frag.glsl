
layout (location = 0) in vec3 v_position;

layout (location = 1) in vec2 v_texcoord_0;
layout (location = 2) in vec2 v_texcoord_1;

#ifdef HAS_COLOR_0_VEC3
layout (location = 3) in vec3 v_color_0;
#endif
#ifdef HAS_COLOR_0_VEC4
layout (location = 3) in vec4 v_color_0;
#endif

#ifdef HAS_NORMAL_VEC3
layout (location = 4) in vec3 v_normal;
#endif

#ifdef HAS_TANGENT_VEC4
layout (location = 5) in mat3 v_TBN;
#endif

layout (set = 0, binding = 1) uniform UBOScene
{
    mat4 u_ViewProjectionMatrix;

    mat4 u_ViewMatrix;
    mat4 u_ProjectionMatrix;

    vec3 u_Camera;

    // IBL
    int u_MipCount;
    mat3 u_EnvRotation;

    // tonemap
    float u_Exposure;
};

layout (set = 1, binding = 2) uniform UBONode 
{
    mat4 u_ModelMatrix;
    mat4 u_NormalMatrix;
};

layout (set = 2, binding = 1) uniform UBOPrimitive
{
    int has_POSITION_VEC3;
    int has_NORMAL_VEC3;
    int has_TANGENT_VEC4;
    int has_TEXCOORD_0_VEC2;
    int has_TEXCOORD_1_VEC2;
    int has_COLOR_0_VEC3;
    int has_COLOR_0_VEC4;
    int has_JOINTS_0_VEC4;
    int has_JOINTS_1_VEC4;
    int has_WEIGHTS_0_VEC4;
    int has_WEIGHTS_1_VEC4;
} u_Primitive;

