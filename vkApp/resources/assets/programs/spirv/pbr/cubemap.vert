#version 450

#extension GL_GOOGLE_include_directive : enable
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (set = 0, binding = 0) uniform UBOScene 
{
	mat4 u_ViewProjectionMatrix;
	mat3 u_EnvRotation;
};


layout (location = 0) in vec3 a_position;

layout (location = 0) out vec3 v_TexCoords;

out gl_PerVertex 
{
	vec4 gl_Position;   
};

void main()
{
    v_TexCoords = u_EnvRotation * a_position;
    mat4 mat = u_ViewProjectionMatrix;
    mat[3] = vec4(0.0, 0.0, 0.0, 0.1);
    vec4 pos = mat * vec4(a_position, 1.0);
    gl_Position = pos.xyww;
}
