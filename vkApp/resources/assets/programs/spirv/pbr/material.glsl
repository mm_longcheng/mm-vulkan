
const int mmGLTFAlphaModeOpaque = 0; // "OPAQUE"
const int mmGLTFAlphaModeMask   = 1; // "MASK"
const int mmGLTFAlphaModeBlend  = 2; // "BLEND"

struct mmGLTFTextureInfo
{
    mat3 transform;
    int has_transform;

    int index;
    int texCoord;
};

struct mmGLTFMaterialsPbrMetallicRoughness
{
    mmGLTFTextureInfo baseColorTexture;
    mmGLTFTextureInfo metallicRoughnessTexture;
    vec4 baseColorFactor;
    float metallicFactor;
    float roughnessFactor;
};

struct mmGLTFMaterialsClearcoat
{
    mmGLTFTextureInfo clearcoatTexture;
    mmGLTFTextureInfo clearcoatRoughnessTexture;
    mmGLTFTextureInfo clearcoatNormalTexture;
    float clearcoatNormalScale;
    float clearcoatFactor;
    float clearcoatRoughnessFactor;
};

struct mmGLTFMaterialsIor
{
    float ior;
};

struct mmGLTFMaterialsPbrSpecularGlossiness
{
    mmGLTFTextureInfo diffuseTexture;
    mmGLTFTextureInfo specularGlossinessTexture;
    vec4 diffuseFactor;
    vec3 specularFactor;
    float glossinessFactor;
};

struct mmGLTFMaterialsSheen
{
    mmGLTFTextureInfo sheenColorTexture;
    mmGLTFTextureInfo sheenRoughnessTexture;
    vec3 sheenColorFactor;
    float sheenRoughnessFactor;
};

struct mmGLTFMaterialsSpecular
{
    mmGLTFTextureInfo specularTexture;
    mmGLTFTextureInfo specularColorTexture;
    vec3 specularColorFactor;
    float specularFactor;
};

struct mmGLTFMaterialsTransmission
{
    mmGLTFTextureInfo transmissionTexture;
    float transmissionFactor;

    ivec2 transmissionFramebufferSize;
};

struct mmGLTFMaterialsUnlit
{
    int enable;
};

struct mmGLTFMaterialsVolume
{
    mmGLTFTextureInfo thicknessTexture;
    float thicknessFactor;
    vec3 attenuationColor;
    float attenuationDistance;
};

layout (set = 3, binding = 0) uniform UBOMaterial
{
    mmGLTFMaterialsPbrMetallicRoughness pbrMetallicRoughness;
    mmGLTFTextureInfo normalTexture;
    mmGLTFTextureInfo occlusionTexture;
    mmGLTFTextureInfo emissiveTexture;
    float normalScale;
    float occlusionStrength;
    vec3 emissiveFactor;
    int alphaMode;
    float alphaCutoff;
    int doubleSided;

    int has_clearcoat;
    int has_ior;
    int has_pbrSpecularGlossiness;
    int has_sheen;
    int has_specular;
    int has_transmission;
    int has_unlit;
    int has_volume;
    mmGLTFMaterialsClearcoat ext_clearcoat;
    mmGLTFMaterialsIor ext_ior;
    mmGLTFMaterialsPbrSpecularGlossiness ext_pbrSpecularGlossiness;
    mmGLTFMaterialsSheen ext_sheen;
    mmGLTFMaterialsSpecular ext_specular;
    mmGLTFMaterialsTransmission ext_transmission;
    mmGLTFMaterialsUnlit ext_unlit;
    mmGLTFMaterialsVolume ext_volume;
} u_Material;

// IBL
layout (set = 4, binding = 0) uniform sampler2D u_GGXLUT;
layout (set = 4, binding = 1) uniform sampler2D u_CharlieLUT;
layout (set = 4, binding = 2) uniform sampler2D u_SheenELUT;
layout (set = 4, binding = 3) uniform samplerCube u_LambertianEnvSampler;
layout (set = 4, binding = 4) uniform samplerCube u_GGXEnvSampler;
layout (set = 4, binding = 5) uniform samplerCube u_CharlieEnvSampler;

// General Material
layout (set = 5, binding = 0) uniform sampler2D u_NormalSampler;
layout (set = 5, binding = 1) uniform sampler2D u_EmissiveSampler;
layout (set = 5, binding = 2) uniform sampler2D u_OcclusionSampler;


vec2 mmGLTFTextureInfoGetUV(mmGLTFTextureInfo t)
{
    vec3 uv = vec3(t.texCoord < 1 ? v_texcoord_0 : v_texcoord_1, 1.0);
#ifdef KHR_texture_transform
    if (1 == t.has_transform)
    {
        uv = t.transform * uv;
    }
#endif
    return uv.xy;
}


vec2 getNormalUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.normalTexture);
}

vec2 getOcclusionUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.occlusionTexture);
}

vec2 getEmissiveUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.emissiveTexture);
}

// Metallic Roughness Material


#ifdef MATERIAL_METALLICROUGHNESS

layout (set = 5, binding = 3) uniform sampler2D u_BaseColorSampler;
layout (set = 5, binding = 4) uniform sampler2D u_MetallicRoughnessSampler;

vec2 getBaseColorUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.pbrMetallicRoughness.baseColorTexture);
}

vec2 getMetallicRoughnessUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.pbrMetallicRoughness.metallicRoughnessTexture);
}

#endif


// Specular Glossiness Material


#ifdef MATERIAL_SPECULARGLOSSINESS

layout (set = 5, binding = 5) uniform sampler2D u_DiffuseSampler;
layout (set = 5, binding = 6) uniform sampler2D u_SpecularGlossinessSampler;

vec2 getSpecularGlossinessUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_pbrSpecularGlossiness.specularGlossinessTexture);
}

vec2 getDiffuseUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_pbrSpecularGlossiness.diffuseTexture);
}

#endif


// Clearcoat Material


#ifdef MATERIAL_CLEARCOAT

layout (set = 5, binding = 7) uniform sampler2D u_ClearcoatSampler;
layout (set = 5, binding = 8) uniform sampler2D u_ClearcoatRoughnessSampler;
layout (set = 5, binding = 9) uniform sampler2D u_ClearcoatNormalSampler;

vec2 getClearcoatUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_clearcoat.clearcoatTexture);
}

vec2 getClearcoatRoughnessUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_clearcoat.clearcoatRoughnessTexture);
}

vec2 getClearcoatNormalUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_clearcoat.clearcoatNormalTexture);
}

#endif

// Sheen Material


#ifdef MATERIAL_SHEEN

layout (set = 5, binding = 10) uniform sampler2D u_SheenColorSampler;
layout (set = 5, binding = 11) uniform sampler2D u_SheenRoughnessSampler;

vec2 getSheenColorUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_sheen.sheenColorTexture);
}

vec2 getSheenRoughnessUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_sheen.sheenRoughnessTexture);
}

#endif

// Specular Material


#ifdef MATERIAL_SPECULAR

layout (set = 5, binding = 12) uniform sampler2D u_SpecularSampler;
layout (set = 5, binding = 13) uniform sampler2D u_SpecularColorSampler;

vec2 getSpecularUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_specular.specularTexture);
}

vec2 getSpecularColorUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_specular.specularColorTexture);
}

#endif

// Transmission Material


#ifdef MATERIAL_TRANSMISSION

layout (set = 5, binding = 14) uniform sampler2D u_TransmissionSampler;
layout (set = 5, binding = 15) uniform sampler2D u_TransmissionFramebufferSampler;

vec2 getTransmissionUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_transmission.transmissionTexture);
}

#endif

// Volume Material


#ifdef MATERIAL_VOLUME

layout (set = 5, binding = 16) uniform sampler2D u_ThicknessSampler;

vec2 getThicknessUV()
{
    return mmGLTFTextureInfoGetUV(u_Material.ext_volume.thicknessTexture);
}

#endif

struct MaterialInfo
{
    float ior;
    // roughness value, as authored by the model creator (input to shader)
    float perceptualRoughness;      
    // full reflectance color (n incidence angle)
    vec3 f0;                        

    // roughness mapped to a more linear change in the roughness (proposed by [2])
    float alphaRoughness;           
    vec3 c_diff;

    // reflectance color at grazing angle
    vec3 f90;                       
    float metallic;

    vec3 baseColor;

    float sheenRoughnessFactor;
    vec3 sheenColorFactor;

    vec3 clearcoatF0;
    vec3 clearcoatF90;
    float clearcoatFactor;
    vec3 clearcoatNormal;
    float clearcoatRoughness;

    // KHR_materials_specular 
    // product of specularFactor and specularTexture.a
    float specularWeight; 

    float transmissionFactor;

    float thickness;
    vec3 attenuationColor;
    float attenuationDistance;
};


// Get normal, tangent and bitangent vectors.
NormalInfo getNormalInfo(vec3 v)
{
    vec2 UV = getNormalUV();
    vec3 uv_dx = dFdx(vec3(UV, 0.0));
    vec3 uv_dy = dFdy(vec3(UV, 0.0));

    vec3 t0 = uv_dy.t * dFdx(v_position) - uv_dx.t * dFdy(v_position);
    float t1 = uv_dx.s * uv_dy.t - uv_dy.s * uv_dx.t;
    vec3 t_ = t0 / t1;

    vec3 n, t, b, ng;

    // Compute geometrical TBN:
    if (1 == u_Primitive.has_TANGENT_VEC4)
    {
        // Trivial TBN computation, present as vertex attribute.
        // Normalize eigenvectors as matrix is linearly interpolated.
        t = normalize(v_TBN[0]);
        b = normalize(v_TBN[1]);
        ng = normalize(v_TBN[2]);
    }
    else
    {
        // Normals are either present as vertex attributes or approximated.
        if (1 == u_Primitive.has_NORMAL_VEC3)
        {
            ng = normalize(v_normal);
        }
        else
        {
            ng = normalize(cross(dFdx(v_position), dFdy(v_position)));
        }

        t = normalize(t_ - ng * dot(ng, t_));
        b = cross(ng, t);
    }

    // For a back-facing surface, the tangential basis vectors are negated.
    if (gl_FrontFacing == false)
    {
        t *= -1.0;
        b *= -1.0;
        ng *= -1.0;
    }

    // Compute pertubed normals:
#ifdef HAS_NORMAL_MAP
    if (-1 != u_Material.normalTexture.index)
    {
        n = texture(u_NormalSampler, UV).rgb * 2.0 - vec3(1.0);
        n *= vec3(u_Material.normalScale, u_Material.normalScale, 1.0);
        n = mat3(t, b, ng) * normalize(n);
    }
    else
    {
        n = ng;
    }
#else
    n = ng;
#endif

    NormalInfo info;
    info.ng = ng;
    info.t = t;
    info.b = b;
    info.n = n;
    return info;
}


vec3 getClearcoatNormal(NormalInfo normalInfo)
{
#ifdef HAS_CLEARCOAT_NORMAL_MAP
    if (-1 != u_Material.ext_clearcoat.clearcoatNormalTexture.index)
    {
        vec3 n = texture(u_ClearcoatNormalSampler, getClearcoatNormalUV()).rgb * 2.0 - vec3(1.0);
        float clearcoatNormalScale = u_Material.ext_clearcoat.clearcoatNormalScale;
        n *= vec3(clearcoatNormalScale, clearcoatNormalScale, 1.0);
        n = mat3(normalInfo.t, normalInfo.b, normalInfo.ng) * normalize(n);
        return n;
    }
#else
    return normalInfo.ng;
#endif
}


vec4 getBaseColor()
{
    vec4 baseColor = vec4(1);

#if defined(MATERIAL_SPECULARGLOSSINESS)
    baseColor = u_Material.ext_pbrSpecularGlossiness.diffuseFactor;
#elif defined(MATERIAL_METALLICROUGHNESS)
    baseColor = u_Material.pbrMetallicRoughness.baseColorFactor;
#endif

#if defined(MATERIAL_SPECULARGLOSSINESS) && defined(HAS_DIFFUSE_MAP)
    if (-1 != u_Material.ext_pbrSpecularGlossiness.diffuseTexture.index)
    {
        baseColor *= texture(u_DiffuseSampler, getDiffuseUV());
    }
#elif defined(MATERIAL_METALLICROUGHNESS) && defined(HAS_BASE_COLOR_MAP)
    if (-1 != u_Material.pbrMetallicRoughness.baseColorTexture.index)
    {
        baseColor *= texture(u_BaseColorSampler, getBaseColorUV());
    }
#endif

    return baseColor * getVertexColor();
}


#ifdef MATERIAL_SPECULARGLOSSINESS
MaterialInfo getSpecularGlossinessInfo(MaterialInfo info)
{
    info.f0 = u_Material.ext_pbrSpecularGlossiness.specularFactor;
    info.perceptualRoughness = u_Material.ext_pbrSpecularGlossiness.glossinessFactor;

#ifdef HAS_SPECULAR_GLOSSINESS_MAP
    if (-1 != u_Material.ext_pbrSpecularGlossiness.specularGlossinessTexture.index)
    {
        vec4 sgSample = texture(u_SpecularGlossinessSampler, getSpecularGlossinessUV());
        // glossiness to roughness
        info.perceptualRoughness *= sgSample.a ; 
        info.f0 *= sgSample.rgb; // specular
    }
#endif // ! HAS_SPECULAR_GLOSSINESS_MAP

    info.perceptualRoughness = 1.0 - info.perceptualRoughness; // 1 - glossiness
    info.c_diff = info.baseColor.rgb * (1.0 - max(max(info.f0.r, info.f0.g), info.f0.b));
    return info;
}
#endif


#ifdef MATERIAL_METALLICROUGHNESS
MaterialInfo getMetallicRoughnessInfo(MaterialInfo info)
{
    info.metallic = u_Material.pbrMetallicRoughness.metallicFactor;
    info.perceptualRoughness = u_Material.pbrMetallicRoughness.roughnessFactor;

#ifdef HAS_METALLIC_ROUGHNESS_MAP
    if (-1 != u_Material.pbrMetallicRoughness.metallicRoughnessTexture.index)
    {
        // Roughness is stored in the 'g' channel, metallic is stored in the 'b' channel.
        // This layout intentionally reserves the 'r' channel for (optional) occlusion map data
        vec4 mrSample = texture(u_MetallicRoughnessSampler, getMetallicRoughnessUV());
        info.perceptualRoughness *= mrSample.g;
        info.metallic *= mrSample.b;
    }
#endif

    // Achromatic f0 based on IOR.
    info.c_diff = mix(info.baseColor.rgb * (vec3(1.0) - info.f0),  vec3(0), info.metallic);
    info.f0 = mix(info.f0, info.baseColor.rgb, info.metallic);
    return info;
}
#endif


#ifdef MATERIAL_SHEEN
MaterialInfo getSheenInfo(MaterialInfo info)
{
    info.sheenColorFactor = u_Material.ext_sheen.sheenColorFactor;
    info.sheenRoughnessFactor = u_Material.ext_sheen.sheenRoughnessFactor;

#ifdef HAS_SHEEN_COLOR_MAP
    if (-1 != u_Material.ext_sheen.sheenColorTexture.index)
    {
        vec4 sheenColorSample = texture(u_SheenColorSampler, getSheenColorUV());
        info.sheenColorFactor *= sheenColorSample.rgb;
    }
#endif

#ifdef HAS_SHEEN_ROUGHNESS_MAP
    if (-1 != u_Material.ext_sheen.sheenRoughnessTexture.index)
    {
        vec4 sheenRoughnessSample = texture(u_SheenRoughnessSampler, getSheenRoughnessUV());
        info.sheenRoughnessFactor *= sheenRoughnessSample.a;
    }
#endif
    return info;
}
#endif


#ifdef MATERIAL_SPECULAR
MaterialInfo getSpecularInfo(MaterialInfo info)
{   
    vec4 specularTexture = vec4(1.0);
#ifdef HAS_SPECULAR_MAP
    if (-1 != u_Material.ext_specular.specularTexture.index)
    {
        specularTexture.a = texture(u_SpecularSampler, getSpecularUV()).a;
    }
#endif
#ifdef HAS_SPECULAR_COLOR_MAP
    if (-1 != u_Material.ext_specular.specularColorTexture.index)
    {
        specularTexture.rgb = texture(u_SpecularColorSampler, getSpecularColorUV()).rgb;
    }
#endif

    vec3 dielectricSpecularF0 = min(info.f0 * u_Material.ext_specular.specularColorFactor * specularTexture.rgb, vec3(1.0));
    info.f0 = mix(dielectricSpecularF0, info.baseColor.rgb, info.metallic);
    info.specularWeight = u_Material.ext_specular.specularFactor * specularTexture.a;
    info.c_diff = mix(info.baseColor.rgb * (1.0 - max3(dielectricSpecularF0)),  vec3(0), info.metallic);
    return info;
}
#endif


#ifdef MATERIAL_TRANSMISSION
MaterialInfo getTransmissionInfo(MaterialInfo info)
{
    info.transmissionFactor = u_Material.ext_transmission.transmissionFactor;

#ifdef HAS_TRANSMISSION_MAP
    if (-1 != u_Material.ext_transmission.transmissionTexture.index)
    {
        vec4 transmissionSample = texture(u_TransmissionSampler, getTransmissionUV());
        info.transmissionFactor *= transmissionSample.r;
    }
#endif
    return info;
}
#endif


#ifdef MATERIAL_VOLUME
MaterialInfo getVolumeInfo(MaterialInfo info)
{
    info.thickness = u_Material.ext_volume.thicknessFactor;
    info.attenuationColor = u_Material.ext_volume.attenuationColor;
    info.attenuationDistance = u_Material.ext_volume.attenuationDistance;

#ifdef HAS_THICKNESS_MAP
    if (-1 != u_Material.ext_volume.thicknessTexture.index)
    {
        vec4 thicknessSample = texture(u_ThicknessSampler, getThicknessUV());
        info.thickness *= thicknessSample.g;
    }
#endif
    return info;
}
#endif


#ifdef MATERIAL_CLEARCOAT
MaterialInfo getClearCoatInfo(MaterialInfo info, NormalInfo normalInfo)
{
    info.clearcoatFactor = u_Material.ext_clearcoat.clearcoatFactor;
    info.clearcoatRoughness = u_Material.ext_clearcoat.clearcoatRoughnessFactor;
    info.clearcoatF0 = vec3(info.f0);
    info.clearcoatF90 = vec3(1.0);

#ifdef HAS_CLEARCOAT_MAP
    if (-1 != u_Material.ext_clearcoat.clearcoatTexture.index)
    {
        vec4 clearcoatSample = texture(u_ClearcoatSampler, getClearcoatUV());
        info.clearcoatFactor *= clearcoatSample.r;
    }
#endif

#ifdef HAS_CLEARCOAT_ROUGHNESS_MAP
    if (-1 != u_Material.ext_clearcoat.clearcoatRoughnessTexture.index)
    {
        vec4 clearcoatSampleRoughness = texture(u_ClearcoatRoughnessSampler, getClearcoatRoughnessUV());
        info.clearcoatRoughness *= clearcoatSampleRoughness.g;
    }
#endif

    info.clearcoatNormal = getClearcoatNormal(normalInfo);
    info.clearcoatRoughness = clamp(info.clearcoatRoughness, 0.0, 1.0);
    return info;
}
#endif


#ifdef MATERIAL_IOR
MaterialInfo getIorInfo(MaterialInfo info)
{
    float ior = u_Material.ext_ior.ior;
    info.f0 = vec3(pow(( ior - 1.0) /  (ior + 1.0), 2.0));
    info.ior = ior;
    return info;
}
#endif


float albedoSheenScalingLUT(float NdotV, float sheenRoughnessFactor)
{
    return texture(u_SheenELUT, vec2(NdotV, sheenRoughnessFactor)).r;
}
