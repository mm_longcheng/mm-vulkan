// constant value.

#define TARGET_COUNT 8
#define JOINT_COUNT 64
#define LIGHT_COUNT 4

#define HAS_NORMAL_VEC3 1
#define HAS_TANGENT_VEC4 1

#define HAS_TEXCOORD_0_VEC2 1
#define HAS_TEXCOORD_1_VEC2 1

// #define HAS_COLOR_0_VEC3 0
#define HAS_COLOR_0_VEC4 1

#define HAS_JOINTS_0_VEC4 1
#define HAS_JOINTS_1_VEC4 1
#define HAS_WEIGHTS_0_VEC4 1
#define HAS_WEIGHTS_1_VEC4 1

#define USE_MORPHING 1
#define USE_SKINNING 1
#define USE_PUNCTUAL 1

#define KHR_texture_transform

#define KHR_materials_clearcoat 1
#define KHR_materials_ior 1
#define KHR_materials_pbrSpecularGlossiness 1
#define KHR_materials_sheen 1
#define KHR_materials_specular 1
#define KHR_materials_transmission 1
#define KHR_materials_unlit 1
#define KHR_materials_variants 1
#define KHR_materials_volume 1

#define MATERIAL_METALLICROUGHNESS 1
#define MATERIAL_CLEARCOAT 1
#define MATERIAL_IOR 1
#define MATERIAL_SPECULARGLOSSINESS 1
#define MATERIAL_SHEEN 1
#define MATERIAL_SPECULAR 1
#define MATERIAL_TRANSMISSION 1
#define MATERIAL_UNLIT 1
#define MATERIAL_VOLUME 1

#define HAS_NORMAL_MAP 1
#define HAS_OCCLUSION_MAP 1
#define HAS_EMISSIVE_MAP 1

#define HAS_BASE_COLOR_MAP 1
#define HAS_METALLIC_ROUGHNESS_MAP 1

#define HAS_DIFFUSE_MAP 1
#define HAS_SPECULAR_GLOSSINESS_MAP 1

#define HAS_CLEARCOAT_MAP 1
#define HAS_CLEARCOAT_ROUGHNESS_MAP 1
#define HAS_CLEARCOAT_NORMAL_MAP 1

#define HAS_SHEEN_COLOR_MAP 1
#define HAS_SHEEN_ROUGHNESS_MAP 1

#define HAS_SPECULAR_MAP 1
#define HAS_SPECULAR_COLOR_MAP 1

#define HAS_TRANSMISSION_MAP 1

#define HAS_THICKNESS_MAP 1

// #define LINEAR_OUTPUT 0
// #define USE_IBL 0