

#ifdef USE_SKINNING


mat4 GetTextureMatrix( const in highp sampler2D tex, int textureSize, const in float i ) 
{
    float j = i * 4.0;
    float x = mod( j, float( textureSize ) );
    float y = floor( j / float( textureSize ) );

    float dx = 1.0 / float( textureSize );
    float dy = 1.0 / float( textureSize );

    y = dy * ( y + 0.5 );

    vec4 v1 = texture( tex, vec2( dx * ( x + 0.5 ), y ) );
    vec4 v2 = texture( tex, vec2( dx * ( x + 1.5 ), y ) );
    vec4 v3 = texture( tex, vec2( dx * ( x + 2.5 ), y ) );
    vec4 v4 = texture( tex, vec2( dx * ( x + 3.5 ), y ) );

    return mat4( v1, v2, v3, v4 );
}

mat4 getSkinningMatrix()
{
    mat4 skin = mat4(0);

#if defined(HAS_WEIGHTS_0_VEC4) && defined(HAS_JOINTS_0_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_0_VEC4 && 1 == u_Primitive.has_JOINTS_0_VEC4)
    {
        skin +=
            a_weights_0.x * u_jointMatrix[int(a_joints_0.x)] +
            a_weights_0.y * u_jointMatrix[int(a_joints_0.y)] +
            a_weights_0.z * u_jointMatrix[int(a_joints_0.z)] +
            a_weights_0.w * u_jointMatrix[int(a_joints_0.w)];
    }
#endif

#if defined(HAS_WEIGHTS_1_VEC4) && defined(HAS_JOINTS_1_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_1_VEC4 && 1 == u_Primitive.has_JOINTS_1_VEC4)
    {
        skin +=
            a_weights_1.x * u_jointMatrix[int(a_joints_1.x)] +
            a_weights_1.y * u_jointMatrix[int(a_joints_1.y)] +
            a_weights_1.z * u_jointMatrix[int(a_joints_1.z)] +
            a_weights_1.w * u_jointMatrix[int(a_joints_1.w)];
    }
#endif

    return skin;
}


mat4 getSkinningNormalMatrix()
{
    mat4 skin = mat4(0);

#if defined(HAS_WEIGHTS_0_VEC4) && defined(HAS_JOINTS_0_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_0_VEC4 && 1 == u_Primitive.has_JOINTS_0_VEC4)
    {
        skin +=
            a_weights_0.x * u_jointNormalMatrix[int(a_joints_0.x)] +
            a_weights_0.y * u_jointNormalMatrix[int(a_joints_0.y)] +
            a_weights_0.z * u_jointNormalMatrix[int(a_joints_0.z)] +
            a_weights_0.w * u_jointNormalMatrix[int(a_joints_0.w)];
    }
#endif

#if defined(HAS_WEIGHTS_1_VEC4) && defined(HAS_JOINTS_1_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_1_VEC4 && 1 == u_Primitive.has_JOINTS_1_VEC4)
    {
        skin +=
            a_weights_1.x * u_jointNormalMatrix[int(a_joints_1.x)] +
            a_weights_1.y * u_jointNormalMatrix[int(a_joints_1.y)] +
            a_weights_1.z * u_jointNormalMatrix[int(a_joints_1.z)] +
            a_weights_1.w * u_jointNormalMatrix[int(a_joints_1.w)];
    }
#endif

    return skin;
}

#endif // !USE_SKINNING


#ifdef USE_MORPHING

vec4 getTargetPosition()
{
    vec4 pos = vec4(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            pos.xyz += u_morphWeights[i] * a_target_position[i];
        }
    }

    return pos;
}

vec3 getTargetNormal()
{
    vec3 normal = vec3(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            normal += u_morphWeights[i] * a_target_normal[i];
        }
    }

    return normal;
}


vec3 getTargetTangent()
{
    vec3 tangent = vec3(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            tangent += u_morphWeights[i] * a_target_tangent[i];
        }
    }

    return tangent;
}

#endif // !USE_MORPHING
