const float M_PI = 3.141592653589793;


vec4 getVertexColor()
{
   vec4 color = vec4(1.0);

#ifdef HAS_COLOR_0_VEC3
    if (1 == u_Primitive.has_COLOR_0_VEC3)
    {
        color.rgb = v_color_0.rgb;
    }
#endif

#ifdef HAS_COLOR_0_VEC4
    if (1 == u_Primitive.has_COLOR_0_VEC4)
    {
        color = v_color_0;
    }
#endif

   return color;
}


struct NormalInfo 
{
    vec3 ng;   // Geometric normal
    vec3 n;    // Pertubed normal
    vec3 t;    // Pertubed tangent
    vec3 b;    // Pertubed bitangent
};


float clampedDot(vec3 x, vec3 y)
{
    return clamp(dot(x, y), 0.0, 1.0);
}


float max3(vec3 v)
{
    return max(max(v.x, v.y), v.z);
}


float applyIorToRoughness(float roughness, float ior)
{
    // Scale roughness with IOR so that an IOR of 1.0 results in no microfacet refraction and
    // an IOR of 1.5 results in the default amount of microfacet refraction.
    return roughness * clamp(ior * 2.0 - 2.0, 0.0, 1.0);
}
