#version 450

#extension GL_GOOGLE_include_directive : enable
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#include "constant.glsl"
#include "param-frag.glsl"
#include "tonemapping.glsl"
#include "functions.glsl"
#include "brdf.glsl"
#include "punctual.glsl"
#include "material.glsl"
#include "ibl.glsl"

layout (location = 0) out vec4 g_finalColor;

void main()
{
    vec4 baseColor = getBaseColor();

	if (u_Material.alphaMode == mmGLTFAlphaModeOpaque)
	{
	    baseColor.a = 1.0;
	}

#ifdef MATERIAL_UNLIT
	if (1 == u_Material.has_unlit)
	{
	    g_finalColor = (vec4(linearTosRGB(baseColor.rgb), baseColor.a));
	    return;
	}
#endif

    vec3 v = normalize(u_Camera - v_position);
    NormalInfo normalInfo = getNormalInfo(v);
    vec3 n = normalInfo.n;
    vec3 t = normalInfo.t;
    vec3 b = normalInfo.b;

    float NdotV = clampedDot(n, v);
    float TdotV = clampedDot(t, v);
    float BdotV = clampedDot(b, v);

    MaterialInfo materialInfo;
    materialInfo.baseColor = baseColor.rgb;
    
    // The default index of refraction of 1.5 yields a dielectric normal incidence reflectance of 0.04.
    materialInfo.ior = 1.5;
    materialInfo.f0 = vec3(0.04);
    materialInfo.specularWeight = 1.0;
    
#ifdef MATERIAL_IOR
	if (1 == u_Material.has_ior)
	{
	    materialInfo = getIorInfo(materialInfo);
	}
#endif

#ifdef MATERIAL_SPECULARGLOSSINESS
	if (1 == u_Material.has_pbrSpecularGlossiness)
	{
    	materialInfo = getSpecularGlossinessInfo(materialInfo);
	}
#endif

#ifdef MATERIAL_METALLICROUGHNESS
	// default
    materialInfo = getMetallicRoughnessInfo(materialInfo);
#endif

#ifdef MATERIAL_SHEEN
	if (1 == u_Material.has_sheen)
	{
    	materialInfo = getSheenInfo(materialInfo);
	}
#endif

#ifdef MATERIAL_CLEARCOAT
	if (1 == u_Material.has_clearcoat)
	{
    	materialInfo = getClearCoatInfo(materialInfo, normalInfo);
	}
#endif

#ifdef MATERIAL_SPECULAR
	if (1 == u_Material.has_specular)
	{
    	materialInfo = getSpecularInfo(materialInfo);
	}
#endif

#ifdef MATERIAL_TRANSMISSION
	if (1 == u_Material.has_transmission)
	{
    	materialInfo = getTransmissionInfo(materialInfo);
	}
#endif

#ifdef MATERIAL_VOLUME
	if (1 == u_Material.has_volume)
	{
    	materialInfo = getVolumeInfo(materialInfo);
	}
#endif

    materialInfo.perceptualRoughness = clamp(materialInfo.perceptualRoughness, 0.0, 1.0);
    materialInfo.metallic = clamp(materialInfo.metallic, 0.0, 1.0);

    // Roughness is authored as perceptual roughness; as is convention,
    // convert to material roughness by squaring the perceptual roughness.
    materialInfo.alphaRoughness = materialInfo.perceptualRoughness * materialInfo.perceptualRoughness;

    // Compute reflectance.
    float reflectance = max(max(materialInfo.f0.r, materialInfo.f0.g), materialInfo.f0.b);

    // Anything less than 2% is physically impossible and is instead considered to be shadowing. Compare to "Real-Time-Rendering" 4th editon on page 325.
    materialInfo.f90 = vec3(1.0);

    // LIGHTING
    vec3 f_specular = vec3(0.0);
    vec3 f_diffuse = vec3(0.0);
    vec3 f_emissive = vec3(0.0);
    vec3 f_clearcoat = vec3(0.0);
    vec3 f_sheen = vec3(0.0);
    vec3 f_transmission = vec3(0.0);

    float albedoSheenScaling = 1.0;

    // Calculate lighting contribution from image based lighting source (IBL)
#ifdef USE_IBL
    f_specular += getIBLRadianceGGX(n, v, materialInfo.perceptualRoughness, materialInfo.f0, materialInfo.specularWeight);
    f_diffuse += getIBLRadianceLambertian(n, v, materialInfo.perceptualRoughness, 
    	materialInfo.c_diff, materialInfo.f0, materialInfo.specularWeight);

#ifdef MATERIAL_CLEARCOAT
	if (1 == u_Material.has_clearcoat)
	{
    	f_clearcoat += getIBLRadianceGGX(materialInfo.clearcoatNormal, v, materialInfo.clearcoatRoughness, materialInfo.clearcoatF0, 1.0);
	}
#endif

#ifdef MATERIAL_SHEEN
	if (1 == u_Material.has_sheen)
	{
    	f_sheen += getIBLRadianceCharlie(n, v, materialInfo.sheenRoughnessFactor, materialInfo.sheenColorFactor);
	}
#endif
#endif

#if (defined(MATERIAL_TRANSMISSION) || defined(MATERIAL_VOLUME)) && (defined(USE_PUNCTUAL) || defined(USE_IBL))
	if ((1 == u_Material.has_transmission || 1 == u_Material.has_volume))
	{
	    f_transmission += materialInfo.transmissionFactor * getIBLVolumeRefraction(
	        n, v,
	        materialInfo.perceptualRoughness,
	        materialInfo.baseColor, materialInfo.f0, materialInfo.f90,
	        v_position, u_ModelMatrix, u_ViewMatrix, u_ProjectionMatrix,
	        materialInfo.ior, materialInfo.thickness, materialInfo.attenuationColor, materialInfo.attenuationDistance);
	}
#endif

    float ao = 1.0;
    // Apply optional PBR terms for additional (optional) shading
#ifdef HAS_OCCLUSION_MAP
	if (-1 != u_Material.occlusionTexture.index)
	{
	    ao = texture(u_OcclusionSampler,  getOcclusionUV()).r;
	    float occlusionStrength = u_Material.occlusionStrength;
	    f_diffuse = mix(f_diffuse, f_diffuse * ao, occlusionStrength);
	    // apply ambient occlusion to all lighting that is not punctual
	    f_specular = mix(f_specular, f_specular * ao, occlusionStrength);
	    f_sheen = mix(f_sheen, f_sheen * ao, occlusionStrength);
	    f_clearcoat = mix(f_clearcoat, f_clearcoat * ao, occlusionStrength);
	}
#endif

#ifdef USE_PUNCTUAL
    for (int i = 0; i < u_lightCount; ++i)
    {
        Light light = u_Lights[i];

        vec3 pointToLight;
        if (light.type != mmGLTFLightTypeDirectional)
        {
            pointToLight = light.position - v_position;
        }
        else
        {
            pointToLight = -light.direction;
        }

        // BSTF
        vec3 l = normalize(pointToLight);   // Direction from surface point to light
        vec3 h = normalize(l + v);          // Direction of the vector between l and v, called halfway vector
        float NdotL = clampedDot(n, l);
        float NdotV = clampedDot(n, v);
        float NdotH = clampedDot(n, h);
        float LdotH = clampedDot(l, h);
        float VdotH = clampedDot(v, h);
        if (NdotL > 0.0 || NdotV > 0.0)
        {
            // Calculation of analytical light
            // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#acknowledgments AppendixB
            vec3 intensity = getLighIntensity(light, pointToLight);

			vec3 lambertian = BRDF_lambertian(materialInfo.f0, materialInfo.f90, 
				materialInfo.c_diff, materialInfo.specularWeight, VdotH);

			vec3 specularGGX = BRDF_specularGGX(materialInfo.f0, materialInfo.f90, 
				materialInfo.alphaRoughness, materialInfo.specularWeight, VdotH, NdotL, NdotV, NdotH);

            f_diffuse += intensity * NdotL *  lambertian;
            f_specular += intensity * NdotL * specularGGX;

#ifdef MATERIAL_SHEEN
			if (1 == u_Material.has_sheen)
			{
	            f_sheen += intensity * getPunctualRadianceSheen(materialInfo.sheenColorFactor, 
	            	materialInfo.sheenRoughnessFactor, NdotL, NdotV, NdotH);

	            float tNV = albedoSheenScalingLUT(NdotV, materialInfo.sheenRoughnessFactor);
	            float tNL = albedoSheenScalingLUT(NdotL, materialInfo.sheenRoughnessFactor);
	            albedoSheenScaling = min(1.0 - max3(materialInfo.sheenColorFactor) * tNV, 1.0 - max3(materialInfo.sheenColorFactor) * tNL);
			}
#endif

#ifdef MATERIAL_CLEARCOAT
			if (1 == u_Material.has_clearcoat)
			{
	            f_clearcoat += intensity * getPunctualRadianceClearCoat(
	            	materialInfo.clearcoatNormal, v, l, h, VdotH,
	                materialInfo.clearcoatF0, materialInfo.clearcoatF90, materialInfo.clearcoatRoughness);
			}
#endif
        }

        // BDTF
#ifdef MATERIAL_TRANSMISSION
		if (1 == u_Material.has_transmission)
		{
	        // If the light ray travels through the geometry, use the point it exits the geometry again.
	        // That will change the angle to the light source, if the material refracts the light ray.
	        vec3 transmissionRay = getVolumeTransmissionRay(n, v, materialInfo.thickness, materialInfo.ior, u_ModelMatrix);
	        pointToLight -= transmissionRay;
	        l = normalize(pointToLight);

	        vec3 intensity = getLighIntensity(light, pointToLight);
	        vec3 tPRT = getPunctualRadianceTransmission(n, v, l, materialInfo.alphaRoughness, 
	        	materialInfo.f0, materialInfo.f90, materialInfo.baseColor, materialInfo.ior);
	        vec3 transmittedLight = intensity * tPRT;

#ifdef MATERIAL_VOLUME
			if (1 == u_Material.has_volume)
			{
	        	transmittedLight = applyVolumeAttenuation(
	        		transmittedLight, 
	        		length(transmissionRay), 
	        		materialInfo.attenuationColor, 
	        		materialInfo.attenuationDistance);
			}
#endif
        	f_transmission += materialInfo.transmissionFactor * transmittedLight;
		}
#endif
    }
#endif

    f_emissive = u_Material.emissiveFactor;
#ifdef HAS_EMISSIVE_MAP
	if (-1 != u_Material.emissiveTexture.index)
	{
    	f_emissive *= texture(u_EmissiveSampler, getEmissiveUV()).rgb;
	}
#endif

    vec3 color = vec3(0);

    // Layer blending

    float clearcoatFactor = 0.0;
    vec3 clearcoatFresnel = vec3(0);

#ifdef MATERIAL_CLEARCOAT
	if (1 == u_Material.has_clearcoat)
	{
	    clearcoatFactor = materialInfo.clearcoatFactor;
	    clearcoatFresnel = F_Schlick(materialInfo.clearcoatF0, materialInfo.clearcoatF90, clampedDot(materialInfo.clearcoatNormal, v));
	    f_clearcoat = f_clearcoat * clearcoatFactor;
	}
#endif

#ifdef MATERIAL_TRANSMISSION
	vec3 diffuse = f_diffuse;
	if (1 == u_Material.has_transmission)
	{
    	diffuse = mix(f_diffuse, f_transmission, materialInfo.transmissionFactor);
	}
#else
    vec3 diffuse = f_diffuse;
#endif

    color = f_emissive + diffuse + f_specular;
    color = f_sheen + color * albedoSheenScaling;
    color = color * (1.0 - clearcoatFactor * clearcoatFresnel) + f_clearcoat;

#if DEBUG == DEBUG_NONE

	if (u_Material.alphaMode == mmGLTFAlphaModeMask)
	{
	    // Late discard to avoid samplig artifacts. See https://github.com/KhronosGroup/glTF-Sample-Viewer/issues/267
	    if (baseColor.a < u_Material.alphaCutoff)
	    {
	        discard;
	    }
	    baseColor.a = 1.0;
	}

#ifdef LINEAR_OUTPUT
    g_finalColor = vec4(color.rgb, baseColor.a);
#else
    g_finalColor = vec4(toneMap(color), baseColor.a);
#endif

#else
    g_finalColor.a = 1.0;
#endif

#if DEBUG == DEBUG_METALLIC
    g_finalColor.rgb = vec3(materialInfo.metallic);
#endif

#if DEBUG == DEBUG_ROUGHNESS
    g_finalColor.rgb = vec3(materialInfo.perceptualRoughness);
#endif

#if DEBUG == DEBUG_NORMAL
#ifdef HAS_NORMAL_MAP
	if (-1 != u_Material.normalTexture.index)
	{
    	g_finalColor.rgb = texture(u_NormalSampler, getNormalUV()).rgb;
	}
	else
	{
    	g_finalColor.rgb = vec3(0.5, 0.5, 1.0);
	}
#else
    g_finalColor.rgb = vec3(0.5, 0.5, 1.0);
#endif
#endif

#if DEBUG == DEBUG_NORMAL_GEOMETRY
    g_finalColor.rgb = (normalInfo.ng + 1.0) / 2.0;
#endif

#if DEBUG == DEBUG_NORMAL_WORLD
    g_finalColor.rgb = (n + 1.0) / 2.0;
#endif

#if DEBUG == DEBUG_TANGENT
    g_finalColor.rgb = t * 0.5 + vec3(0.5);
#endif

#if DEBUG == DEBUG_BITANGENT
    g_finalColor.rgb = b * 0.5 + vec3(0.5);
#endif

#if DEBUG == DEBUG_BASE_COLOR_SRGB
    g_finalColor.rgb = linearTosRGB(materialInfo.baseColor);
#endif

#if DEBUG == DEBUG_BASE_COLOR_LINEAR
    g_finalColor.rgb = materialInfo.baseColor;
#endif

#if DEBUG == DEBUG_OCCLUSION
    g_finalColor.rgb = vec3(ao);
#endif

#if DEBUG == DEBUG_F0
    g_finalColor.rgb = materialInfo.f0;
#endif

#if DEBUG == DEBUG_EMISSIVE_SRGB
    g_finalColor.rgb = linearTosRGB(f_emissive);
#endif

#if DEBUG == DEBUG_EMISSIVE_LINEAR
    g_finalColor.rgb = f_emissive;
#endif

#if DEBUG == DEBUG_SPECULAR_SRGB
    g_finalColor.rgb = linearTosRGB(f_specular);
#endif

#if DEBUG == DEBUG_DIFFUSE_SRGB
    g_finalColor.rgb = linearTosRGB(f_diffuse);
#endif

#if DEBUG == DEBUG_CLEARCOAT_SRGB
    g_finalColor.rgb = linearTosRGB(f_clearcoat);
#endif

#if DEBUG == DEBUG_SHEEN_SRGB
    g_finalColor.rgb = linearTosRGB(f_sheen);
#endif

#if DEBUG == DEBUG_TRANSMISSION_SRGB
    g_finalColor.rgb = linearTosRGB(f_transmission);
#endif

#if DEBUG == DEBUG_ALPHA
    g_finalColor.rgb = vec3(baseColor.a);
#endif
}