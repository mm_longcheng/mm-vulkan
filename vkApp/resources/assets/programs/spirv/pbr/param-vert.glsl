
layout (location = 0) in vec3 a_position;
layout (location = 0) out vec3 v_position;

layout (location = 1) out vec2 v_texcoord_0;
layout (location = 2) out vec2 v_texcoord_1;

#ifdef HAS_TEXCOORD_0_VEC2
layout (location = 1) in vec2 a_texcoord_0;
#endif

#ifdef HAS_TEXCOORD_1_VEC2
layout (location = 2) in vec2 a_texcoord_1;
#endif

#ifdef HAS_COLOR_0_VEC3
layout (location = 3) in vec3 a_color_0;
layout (location = 3) out vec3 v_color_0;
#endif

#ifdef HAS_COLOR_0_VEC4
layout (location = 3) in vec4 a_color_0;
layout (location = 3) out vec4 v_color_0;
#endif


#ifdef HAS_NORMAL_VEC3
layout (location = 4) in vec3 a_normal;
#endif

#ifdef HAS_TANGENT_VEC4
layout (location = 5) in vec4 a_tangent;
#endif

#ifdef HAS_NORMAL_VEC3
layout (location = 4) out vec3 v_normal;
#endif

#ifdef HAS_TANGENT_VEC4
layout (location = 5) out mat3 v_TBN;
#endif


#ifdef HAS_JOINTS_0_VEC4
layout (location = 6) in vec4 a_joints_0;
#endif

#ifdef HAS_JOINTS_1_VEC4
layout (location = 7) in vec4 a_joints_1;
#endif

#ifdef HAS_WEIGHTS_0_VEC4
layout (location = 8) in vec4 a_weights_0;
#endif

#ifdef HAS_WEIGHTS_1_VEC4
layout (location = 9) in vec4 a_weights_1;
#endif

layout (location = 10) in vec3 a_target_position[TARGET_COUNT];
layout (location = 18) in vec3 a_target_normal[TARGET_COUNT];
layout (location = 26) in vec3 a_target_tangent[TARGET_COUNT];

layout (set = 0, binding = 0) uniform UBOScene
{
    mat4 u_ViewProjectionMatrix;

    mat4 u_ViewMatrix;
    mat4 u_ProjectionMatrix;

    vec3 u_Camera;

    // IBL
    int u_MipCount;
    mat3 u_EnvRotation;

    // tonemap
    float u_Exposure;
};

layout (set = 1, binding = 0) uniform UBONode 
{
    mat4 u_ModelMatrix;
    mat4 u_NormalMatrix;
};

layout (set = 1, binding = 1) uniform UBOAnimation 
{
#ifdef USE_SKINNING
    mat4 u_jointMatrix[JOINT_COUNT];
    mat4 u_jointNormalMatrix[JOINT_COUNT];
#endif

#ifdef USE_MORPHING
    float u_morphWeights[TARGET_COUNT];
#endif

    int u_jointCount;
    int u_morphCount;
};

layout (set = 2, binding = 0) uniform UBOPrimitive
{
    int has_POSITION_VEC3;
    int has_NORMAL_VEC3;
    int has_TANGENT_VEC4;
    int has_TEXCOORD_0_VEC2;
    int has_TEXCOORD_1_VEC2;
    int has_COLOR_0_VEC3;
    int has_COLOR_0_VEC4;
    int has_JOINTS_0_VEC4;
    int has_JOINTS_1_VEC4;
    int has_WEIGHTS_0_VEC4;
    int has_WEIGHTS_1_VEC4;
} u_Primitive;

