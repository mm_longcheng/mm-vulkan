#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in uvec4 a_joints_0;
layout (location = 3) in vec4 a_weights_0;

layout (set = 0, binding = 0, std140) uniform UBOScene
{
	mat4 projection;
	mat4 view;
} uboScene;

layout (set = 2, binding = 0, std430) readonly buffer UBOSkin 
{
	mat4 joints[];
} uboSkin;

layout (push_constant, std140) uniform PushConstants
{
	mat4 normal;
    mat4 model;
} constants;

layout (location = 0) out vec3 v_position;
layout (location = 1) out vec3 v_normal;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
	
	mat4 skinMat = mat4( 0.0 );
	skinMat += a_weights_0.x * uboSkin.joints[a_joints_0.x];
	skinMat += a_weights_0.y * uboSkin.joints[a_joints_0.y];
	skinMat += a_weights_0.z * uboSkin.joints[a_joints_0.z];
	skinMat += a_weights_0.w * uboSkin.joints[a_joints_0.w];

	mat4 mv = uboScene.view * constants.model;
	mat4 smv = mv * skinMat;
	v_position = vec3(smv * vec4(a_position, 1.0));
	mat3 normalMat = transpose(inverse(mat3(smv)));
	v_normal = normalize(normalMat * a_normal);
	gl_Position = uboScene.projection * vec4(v_position, 1.0);
}
