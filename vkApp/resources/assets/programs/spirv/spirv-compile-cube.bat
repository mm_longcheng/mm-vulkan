@echo off

set Compiler=%VULKAN_SDK%/Bin/glslangValidator

call :build-spv spirv-cube.frag
call :build-spv spirv-cube.vert

call :build-spv spirv-sphere.frag
call :build-spv spirv-sphere.vert

GOTO :EOF

:build-spv
SETLOCAL
:: REM.
%Compiler% -V %~1 -o %~1.spv
ENDLOCAL
GOTO :EOF