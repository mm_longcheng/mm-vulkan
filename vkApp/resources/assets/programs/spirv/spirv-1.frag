#version 400
 
// Vulkan中想使用着色器进行开发，需要开启这两个扩展
// 启动GL_ARB_separate_shader_objects
#extension GL_ARB_separate_shader_objects : enable
// 启动GL_ARB_shading_language_420pack
#extension GL_ARB_shading_language_420pack : enable
 
// 定义两个颜色数据值
// 顶点着色器传入的顶点颜色数据
layout (location = 0) in vec3 vcolor;
// 输出到渲染管线的片元颜色值
layout (location = 0) out vec4 outColor;
 
// 片元着色器的主方法
void main() 
{
	// 将顶点着色器传递过来的颜色值输出，最后的1.0代表A通道
   outColor=vec4(vcolor.rgb,1.0);
}
