#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec2 inUV;
layout (location = 2) in vec4 inColor;

layout(set = 0, binding = 0, std140) uniform UBO 
{
    mat4 MVP;
} ubo;

layout (push_constant) uniform PushConstants 
{
	mat4 MVP;
} constants;

layout (location = 0) out vec2 outUV;
layout (location = 1) out vec4 outColor;

out gl_PerVertex 
{
	vec4 gl_Position;   
};

void main() 
{
	outUV = inUV;
	outColor = inColor;
	gl_Position = ubo.MVP * vec4(inPos, 1.0);
}