#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec2 a_texcoord;
layout (location = 2) in vec4 a_color;

layout (set = 0, binding = 0, std140) uniform UBO
{
    mat4 viewProjection;
} ubo;

layout (push_constant, std140) uniform PushConstants
{
    mat4 model;
    vec4 color;
} constants;

layout (location = 0) out vec2 v_texcoord;
layout (location = 1) out vec4 v_color;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    v_texcoord = a_texcoord;
    v_color = a_color * constants.color;
    gl_Position = ubo.viewProjection * constants.model * vec4(a_position, 1.0);
}
