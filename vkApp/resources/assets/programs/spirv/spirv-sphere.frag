#version 450

layout (set = 0, binding = 1) uniform sampler2D texture_0;

layout (set = 0, binding = 2, std140) uniform UBOSceneFragment
{
    mat4 inverseView;
    vec3 camera;
    vec3 ambient;
    float exposure;
    int lightCount;
    float time;
    float scale;
} uScene;

layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec2 v_Texcoord;
layout (location = 2) in vec3 v_Normal;

layout (location = 0) out vec4 outColor;

void main()
{
//    // vec3 N = normalize(v_Normal);
    outColor = texture(texture_0, v_Texcoord);
    
//    vec4 baseColor = texture(texture_0, v_Texcoord);
//
//    vec4 color = vec4(1.0);
//
//    vec3 lightvec = uScene.camera - v_Position;
//    vec4 light_ambient = vec4(0.001, 0.001, 0.001, 1.0);
//    vec4 light_diffuse = vec4(2.0, 2.0, 2.0, 1.0);
//    vec4 light_specular = vec4(0.001, 0.001, 0.001, 1.0);
//    vec3 light_position = vec3(0.0, 3.0, 4.0);
//
//    vec3 pointToLight = light_position - v_Position;
//
//    vec3 L = normalize(pointToLight);
//    vec3 V = normalize(lightvec);
//    vec3 N = normalize(v_Normal);
//    vec3 H = normalize (V + L);
//    float intensity = max(0.0, dot(N, normalize(lightvec)));
//    float glare = max(0.0, dot(N, H));
//    vec4 ambient = light_ambient;
//    vec4 diffuse = light_diffuse * intensity * vec4(baseColor.xyz, 1.0);
//    vec4 specular = light_specular * pow(glare, 64.0);
//    vec4 oColor = (ambient + diffuse + specular) * color;
//
//    outColor = vec4(oColor.xyz, baseColor.a);
    
//    vec4 baseColor = texture(texture_0, v_Texcoord);
//
//    baseColor = vec4(1.0);
//
//    vec4 color = vec4(1.0);
//
//    vec3 lightvec = uScene.camera - v_Position;
//    vec4 light_ambient = vec4(0.001, 0.001, 0.001, 1.0);
//    vec4 light_diffuse = vec4(0.9, 0.9, 0.9, 1.0);
//    vec4 light_specular = vec4(1.001, 1.001, 1.001, 1.0);
//    vec3 light_position = vec3(0.0, 3.0, -4.0);
//
//    vec3 pointToLight = light_position - v_Position;
//
//    vec3 L = normalize(pointToLight);
//    vec3 V = normalize(lightvec);
//    vec3 N = normalize(v_Normal);
//    vec3 H = normalize (V + L);
//    float intensity = max(0.0, dot(N, normalize(lightvec)));
//    float glare = max(0.0, dot(N, H));
//    vec4 ambient = light_ambient;
//    vec4 diffuse = light_diffuse * intensity * vec4(baseColor.xyz, 1.0);
//    vec4 specular = light_specular * pow(glare, 64.0);
//    vec4 oColor = (ambient + diffuse + specular) * color;
//
//    outColor = vec4(oColor.xyz, baseColor.a);
}
