#!/bin/bash

Compiler=${VULKAN_SDK}/macOS/bin/glslangValidator

build-spv()
{
	${Compiler} -V $1 -o $1.spv
}

build-spv 'spirv-1.frag'
build-spv 'spirv-1.vert'

build-spv 'spirv-ui.frag'
build-spv 'spirv-ui.vert'

build-spv 'spirv-2d.frag'
build-spv 'spirv-2d-gray.frag'
build-spv 'spirv-2d.vert'

build-spv 'spirv-2dnes.frag'
build-spv 'spirv-2dnes.frag'
