#version 450

layout (location = 0) in vec3 v_position;
layout (location = 1) in vec2 v_texcoord_0;
layout (location = 2) in vec3 v_color_0;

layout (set = 0, binding = 1, std140) uniform UBOScene
{
	vec3 camera;
	vec3 light;
	float exposure;
} uboScene;

layout (set = 1, binding = 0, std140) uniform UBOMaterial 
{
    vec4 baseColorFactor;
    float metallicFactor;
    float roughnessFactor;
} uboMaterial;

layout (set = 1, binding = 1) uniform sampler2D baseColorTexture;

#define PI 3.1415926535897932384626433832795
const float GAMMA = 2.2;
const float INV_GAMMA = 1.0 / GAMMA;

// ACES tone map (faster approximation)
// see: https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 toneMapACES_Narkowicz(vec3 color)
{
    const float A = 2.51;
    const float B = 0.03;
    const float C = 2.43;
    const float D = 0.59;
    const float E = 0.14;
    return clamp((color * (A * color + B)) / (color * (C * color + D) + E), 0.0, 1.0);
}

// Normal Distribution function
float D_GGX(float dotNH, float roughness)
{
    //    a = roughness^2
	// Dggx = a^2 / PI * ((a^2 - 1) (n dot h) ^ 2 + 1)^2
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
	return (alpha2)/(PI * denom * denom); 
}

// Geometric Shadowing function
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
	// G(l,v) = G1(l) * G1(v)
	//  G1(x) = 1 / (n dot x) * (1 - K) + K
	//      K = roughness^2 / 2                  direct
	//      K = (roughness + 1)^2 / 8            IBL
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;
	float GL = dotNL / (dotNL * (1.0 - k) + k);
	float GV = dotNV / (dotNV * (1.0 - k) + k);
	return GL * GV;
}

// Fresnel Schlick approximation translates
vec3 F_Schlick(float cosTheta, vec3 F0)
{
	// Rf(l,h) = F0 + (1 - F0) * (1 - (l dot h))^5
	// F0 = Ff(h,h) = Cspec
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

// Specular BRDF composition
vec3 BRDF(vec3 L, vec3 V, vec3 N, vec3 F0, float roughness)
{
	// Precalculate vectors and dot products	
	vec3 H = normalize (V + L);
	float dotNV = clamp(dot(N, V), 0.0, 1.0);
	float dotNL = clamp(dot(N, L), 0.0, 1.0);
	float dotLH = clamp(dot(L, H), 0.0, 1.0);
	float dotNH = clamp(dot(N, H), 0.0, 1.0);
	float dotVH = clamp(dot(V, H), 0.0, 1.0);

	// Light color fixed
	vec3 lightColor = vec3(1.0);

	vec3 color = vec3(0.0);
	
	if (dotNL > 0.0)
	{
		float rroughness = max(0.05, roughness);
		// D = Normal distribution (Distribution of the microfacets)
		float D = D_GGX(dotNH, roughness); 
		// G = Geometric shadowing term (Microfacets shadowing)
		float G = G_SchlicksmithGGX(dotNL, dotNV, rroughness);
		// F = Fresnel factor (Reflectance depending on angle of incidence)
		vec3 F = F_Schlick(dotVH, F0);

		vec3 spec = D * F * G / (4.0 * dotNL * dotNV);

		color = spec * dotNL * lightColor;
	}

	return color;
}

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 linearTosRGB(vec3 color)
{
    return pow(color, vec3(INV_GAMMA));
}

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 sRGBToLinear(vec3 srgbIn)
{
    return vec3(pow(srgbIn.xyz, vec3(GAMMA)));
}

vec4 sRGBToLinear(vec4 srgbIn)
{
    return vec4(sRGBToLinear(srgbIn.xyz), srgbIn.w);
}

vec3 toneMap(vec3 color)
{
    color *= uboScene.exposure;

	color = toneMapACES_Narkowicz(color);

    return linearTosRGB(color);
}

layout (location = 0) out vec4 outColor;

void main()
{
	vec3 N = normalize(cross(dFdx(v_position), dFdy(v_position)));
	vec3 V = normalize(uboScene.camera - v_position);
	// vec3 V = normalize(v_position - uboScene.camera);
	// N = -N;
	V = -V;

	vec4 baseColor = uboMaterial.baseColorFactor * texture(baseColorTexture, v_texcoord_0);
	
	// baseColor = vec4(v_color_0 * baseColor.rgb, baseColor.a);
	
	float metallic = uboMaterial.metallicFactor;
	float roughness = uboMaterial.roughnessFactor;

	vec4 linearBaseColor = sRGBToLinear(baseColor);
	
	vec3 F0 = mix(vec3(0.04), linearBaseColor.rgb, metallic);
	
	vec3 L = normalize(uboScene.light - v_position);
	vec3 Lo = BRDF(L, V, N, F0, roughness);

	vec3 ambient = vec3(0.0);
	vec3 color = ambient + Lo;
	
	// color = F0;
	// color = linearBaseColor.rgb;
	// color = linearBaseColor.rgb + color;
	
	color = baseColor.rgb;
	color = vec3(0.0, 1.0, 0.0);
	
	// Tone mapping
	outColor = vec4(toneMap(color), baseColor.a);
	
	// outColor = vec4(0.0, 1.0, 0.0, 1.0);
}
