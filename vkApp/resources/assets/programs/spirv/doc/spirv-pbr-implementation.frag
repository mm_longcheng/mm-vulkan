#version 450

const float pi = 3.141592653589793;

function specular_brdf(α) {
  return V * D
}

function diffuse_brdf(color) {
  return (1/pi) * color
}

function conductor_fresnel(f0, bsdf) {
  return bsdf * (f0 + (1 - f0) * (1 - abs(VdotH))^5)
}

function fresnel_mix(ior, base, layer) {
  f0 = ((1-ior)/(1+ior))^2
  fr = f0 + (1 - f0)*(1 - abs(VdotH))^5
  return mix(base, layer, fr)
}

{
	metal_brdf = specular_brdf(roughness^2) * (baseColor.rgb + (1 - baseColor.rgb) * (1 - abs(VdotH))^5)
	dielectric_brdf = mix(diffuse_brdf(baseColor.rgb), specular_brdf(roughness^2), 0.04 + (1 - 0.04) * (1 - abs(VdotH))^5)
	material = mix(dielectric_brdf, metal_brdf, metallic)
}


{
	const dielectricSpecular = 0.04
	const black = 0

	c_diff = lerp(baseColor.rgb * (1 - dielectricSpecular), black, metallic)
	f0 = lerp(0.04, baseColor.rgb, metallic)
	α = roughness^2

	F = f0 + (1 - f0) * (1 - abs(VdotH))^5

	f_diffuse = (1 - F) * (1 / π) * c_diff
	f_specular = F * D(α) * G(α) / (4 * abs(VdotN) * abs(LdotN))

	material = f_diffuse + f_specular
}

mat4 GetTextureMatrix( const in highp sampler2D tex, int textureSize, const in float i ) 
{
    float j = i * 4.0;
    float x = mod( j, float( textureSize ) );
    float y = floor( j / float( textureSize ) );

    float dx = 1.0 / float( textureSize );
    float dy = 1.0 / float( textureSize );

    y = dy * ( y + 0.5 );

    vec4 v1 = texture( tex, vec2( dx * ( x + 0.5 ), y ) );
    vec4 v2 = texture( tex, vec2( dx * ( x + 1.5 ), y ) );
    vec4 v3 = texture( tex, vec2( dx * ( x + 2.5 ), y ) );
    vec4 v4 = texture( tex, vec2( dx * ( x + 3.5 ), y ) );

    return mat4( v1, v2, v3, v4 );
}

uniform highp sampler2D boneNormalTexture;
uniform highp sampler2D boneTexture;
uniform int boneTextureSize;

mat4 getBoneMatrix( const in float i ) 
{
	float j = i * 4.0;
	float x = mod( j, float( boneTextureSize ) );
	float y = floor( j / float( boneTextureSize ) );

	float dx = 1.0 / float( boneTextureSize );
	float dy = 1.0 / float( boneTextureSize );

	y = dy * ( y + 0.5 );

	vec4 v1 = texture2D( boneTexture, vec2( dx * ( x + 0.5 ), y ) );
	vec4 v2 = texture2D( boneTexture, vec2( dx * ( x + 1.5 ), y ) );
	vec4 v3 = texture2D( boneTexture, vec2( dx * ( x + 2.5 ), y ) );
	vec4 v4 = texture2D( boneTexture, vec2( dx * ( x + 3.5 ), y ) );

	mat4 bone = mat4( v1, v2, v3, v4 );

	return bone;
}

mat4 getBoneNormalMatrix( const in float i ) 
{
	float j = i * 4.0;
	float x = mod( j, float( boneTextureSize ) );
	float y = floor( j / float( boneTextureSize ) );

	float dx = 1.0 / float( boneTextureSize );
	float dy = 1.0 / float( boneTextureSize );

	y = dy * ( y + 0.5 );

	vec4 v1 = texture2D( boneNormalTexture, vec2( dx * ( x + 0.5 ), y ) );
	vec4 v2 = texture2D( boneNormalTexture, vec2( dx * ( x + 1.5 ), y ) );
	vec4 v3 = texture2D( boneNormalTexture, vec2( dx * ( x + 2.5 ), y ) );
	vec4 v4 = texture2D( boneNormalTexture, vec2( dx * ( x + 3.5 ), y ) );

	mat4 bone = mat4( v1, v2, v3, v4 );

	return bone;
}
