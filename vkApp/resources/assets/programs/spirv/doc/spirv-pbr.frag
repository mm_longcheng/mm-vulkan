#version 450

layout (location = 0) in vec3 i_POSITION;
layout (location = 1) in vec3 i_NORMAL;
layout (location = 2) in vec4 i_TANGENT;
layout (location = 3) in vec2 i_TEXCOORD_0;
layout (location = 4) in vec2 i_TEXCOORD_1;
layout (location = 5) in vec4 i_COLOR_0;

layout (set = 0, binding = 1) uniform sampler2D texture0;

layout (set = 1, binding = 0) uniform sampler2D baseColorTexture;
layout (set = 1, binding = 1) uniform sampler2D metallicRoughnessTexture;

layout (set = 1, binding = 2) uniform sampler2D normalTexture;
layout (set = 1, binding = 3) uniform sampler2D occlusionTexture;
layout (set = 1, binding = 4) uniform sampler2D emissiveTexture;

struct MaterialsPbrMetallicRoughness
{
	int baseColorTextureSet;
	int metallicRoughnessTextureSet;
	vec4 baseColorFactor;
	float metallicFactor;	
	float roughnessFactor;
};

layout (push_constant) uniform Material 
{
	MaterialsPbrMetallicRoughness pbrMetallicRoughness;
	int normalTextureSet;
	int occlusionTextureSet;
	int emissiveTextureSet;
	vec3 emissiveFactor;
	int alphaMode;
	float alphaCutoff;
} material;

layout (location = 0) out vec4 outColor;

void main() 
{
	outColor = i_COLOR_0 * texture(texture0, i_TEXCOORD_0);
}
