#version 450

#define MAX_NUM_JOINTS 128

layout (location = 0) in vec3 i_POSITION;
layout (location = 1) in vec3 i_NORMAL;
layout (location = 2) in vec4 i_TANGENT;
layout (location = 3) in vec2 i_TEXCOORD_0;
layout (location = 4) in vec2 i_TEXCOORD_1;
layout (location = 5) in vec4 i_COLOR_0;
layout (location = 6) in vec4 i_JOINTS_0;
layout (location = 7) in vec4 i_JOINTS_1;
layout (location = 8) in vec4 i_WEIGHTS_0;
layout (location = 9) in vec4 i_WEIGHTS_1;

layout (location = 0) out vec3 o_POSITION;
layout (location = 1) out vec3 o_NORMAL;
layout (location = 2) out vec4 o_TANGENT;
layout (location = 3) out vec2 o_TEXCOORD_0;
layout (location = 4) out vec2 o_TEXCOORD_1;
layout (location = 5) out vec4 o_COLOR_0;

layout(set = 0, binding = 0, std140) uniform UBO 
{
	mat4 projection;
	mat4 view;
	mat4 model;
	bool has_JOINTS_0;
	bool has_JOINTS_1;
	bool has_WEIGHTS_0;
	bool has_WEIGHTS_1;
} ubo;

layout (set = 2, binding = 0) uniform UBONode
{
	mat4 matrix;
	mat4 jointMatrix[MAX_NUM_JOINTS];
	int jointCount;
} node;

out gl_PerVertex 
{
	vec4 gl_Position;   
};

mat4 getSkinningMatrix()
{
	mat4 skinMat = mat4(0);

	if (ubo.has_JOINTS_0 && ubo.has_WEIGHTS_0)
	{
		skinMat += 
			i_WEIGHTS_0.x * node.jointMatrix[int(i_JOINTS_0.x)] +
			i_WEIGHTS_0.y * node.jointMatrix[int(i_JOINTS_0.y)] +
			i_WEIGHTS_0.z * node.jointMatrix[int(i_JOINTS_0.z)] +
			i_WEIGHTS_0.w * node.jointMatrix[int(i_JOINTS_0.w)];
	}

	if (ubo.has_JOINTS_1 && ubo.has_WEIGHTS_1)
	{
		skinMat +=
			i_WEIGHTS_1.x * node.jointMatrix[int(i_JOINTS_1.x)] +
			i_WEIGHTS_1.y * node.jointMatrix[int(i_JOINTS_1.y)] +
			i_WEIGHTS_1.z * node.jointMatrix[int(i_JOINTS_1.z)] +
			i_WEIGHTS_1.w * node.jointMatrix[int(i_JOINTS_1.w)];
	}

	return skinMat;
}

void main() 
{
	o_TANGENT = i_TANGENT;
	o_TEXCOORD_0 = i_TEXCOORD_0;
	o_TEXCOORD_1 = i_TEXCOORD_1;
	o_COLOR_0 = i_COLOR_0;

	vec4 localPosition;
	mat4 skinWorldMat;

	if (node.jointCount > 0) 
	{
		mat4 skinMat = getSkinningMatrix();
		skinWorldMat = ubo.model * node.matrix * skinMat;
	} 
	else
	{
		skinWorldMat = ubo.model * node.matrix;
	}

	localPosition = skinWorldMat * vec4(i_POSITION, 1.0);
	o_NORMAL = normalize(transpose(inverse(mat3(skinWorldMat))) * i_NORMAL);

	o_POSITION = localPosition.xyz / localPosition.w;

	gl_Position =  ubo.projection * ubo.view * vec4(o_POSITION, 1.0);
}