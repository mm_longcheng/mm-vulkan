// [Beckmann 1963, "The scattering of electromagnetic waves from rough surfaces"]
float D_Beckmann(float NoH, float a )
{
    float NoH2 = NoH * NoH;
    return exp( (NoH2 - 1) / (a * a * NoH2) ) / ( PI * a * a * NoH2 * NoH2 );
}

// [Blinn 1977, "Models of light reflection for computer synthesized pictures"]
float D_Blinn( float NoH, float a2)
{
        float n = 2 / (a2*a2) - 2;
        return (n+2) / (2*PI) * pow( NoH, n );      // 1 mad, 1 exp, 1 mul, 1 log
}

// GGX / Trowbridge-Reitz
// [Walter et al. 2007, "Microfacet models for refraction through rough surfaces"]
float D_GGXT(float NoH , float a)
{
    float d = (NoH * NoH) * (a * a -1) + 1; 
    return (a*a) / ( PI*d*d );         
}

// Generalized-Trowbridge-Reitz distribution
float D_GTR1(float alpha, float dotNH)
{
    float a2 = alpha * alpha;
    float cos2th = dotNH * dotNH;
    float den = (1.0 + (a2 - 1.0) * cos2th);

    return (a2 - 1.0) / (PI * log(a2) * den);
}

float D_GTR2(float alpha, float dotNH)
{
    float a2 = alpha * alpha;
    float cos2th = dotNH * dotNH;
    float den = (1.0 + (a2 - 1.0) * cos2th);

    return a2 / (PI * den * den);
}

// Anisotropic Beckmann
float D_Beckmann_aniso( float ax, float ay, float NoH, float3 H, float3 X, float3 Y )
{
    float XoH = dot( X, H );
    float YoH = dot( Y, H );
    float d = - (XoH*XoH / (ax*ax) + YoH*YoH / (ay*ay)) / NoH*NoH;
    return exp(d) / ( PI * ax*ay * NoH * NoH * NoH * NoH );
}

// Anisotropic GGX
// [Burley 2012, "Physically-Based Shading at Disney"]
float D_GGXaniso( float ax, float ay, float NoH, float3 H, float3 X, float3 Y )
{
    float XoH = dot( X, H );
    float YoH = dot( Y, H );
    float d = XoH*XoH / (ax*ax) + YoH*YoH / (ay*ay) + NoH*NoH;
    return 1 / ( PI * ax*ay * d*d );
}

vec3 F_Schlick(float cosTheta, float metallic)
{
	vec3 F0 = mix(vec3(0.04), materialcolor(), metallic); // * material.specular
	vec3 F = F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0); 
	return F;    
}

vec3 Fresnel_UE4(float cosTheta, float metallic)
{
	vec3 F0 = mix(vec3(0.04), materialcolor(), metallic); // * material.specular
	vec3 F = F0 + (1.0 - F0) *  pow(2, ((-5.55473) * cosTheta - 6.98316) * cosTheta); 

	return F;    
}

// Appoximation of joint Smith term for GGX
// [Heitz 2014, "Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs"]
float Vis_SmithJointApprox( float NoL , float NoV,float a)
{
    float a2 = a*a;
    float Vis_SmithV = NoL * ( NoV * ( 1 - a2 ) + a2 );
    float Vis_SmithL = NoV * ( NoL * ( 1 - a2 ) + a2 );
    return 0.5 /( Vis_SmithV + Vis_SmithL );
}

float V_SmithJointGGX(float NdotL, float NdotV, float roughness)
{
    float a2 = roughness*roughness;

    float lambdaV = NdotL * (NdotV * (1 - roughness) + roughness);
    float lambdaL = NdotV * sqrt((-NdotL * a2 + NdotL) * NdotL + a2);

    return 0.5 / (lambdaV + lambdaL);
}

float V_SmithGGXCorrelated(float NoL, float NoV,  float a)
{
    float a2 = a * a;
    float GGXL = NoV * sqrt((-NoL * a2 + NoL) * NoL + a2);
    float GGXV = NoL * sqrt((-NoV * a2 + NoV) * NoV + a2);
    return 0.5 / (GGXV + GGXL);
}

float G_RespawnEntertainment(float NoL, float NoV,  float a)
{
    float lerp = mix(2*abs(NoL)*abs(NoV),abs(NoL)+abs(NoV),a);
    return 0.5 / lerp;
}



// final version.

// Normal Distribution function 
float D_GGX(float dotNH, float roughness)
{
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
	return (alpha2)/(PI * denom*denom); 
}

// Fresnel function 
vec3 F_Schlick(float cosTheta, float metallic)
{
	vec3 F0 = mix(vec3(0.04), materialcolor(), metallic); // * material.specular
	vec3 F = F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0); 
	return F;    
}

// Geometric Shadowing function 
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;
	float GL = dotNL / (dotNL * (1.0 - k) + k);
	float GV = dotNV / (dotNV * (1.0 - k) + k);
	return GL * GV;
}


