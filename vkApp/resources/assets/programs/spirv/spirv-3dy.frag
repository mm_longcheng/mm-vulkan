#version 450

layout (location = 0) in vec3 v_position;
layout (location = 1) in vec3 v_normal;

struct UBOTextureInfo
{
    int index;
    int texCoord;
};

layout (set = 0, binding = 1, std140) uniform UBOScene
{
	vec3 camera;
	vec3 light;
	vec3 lightColor;
	float exposure;
} uboScene;

layout (set = 1, binding = 0, std140) uniform UBOMaterial 
{
    UBOTextureInfo baseColorTexture;
    UBOTextureInfo metallicRoughnessTexture;
    UBOTextureInfo normalTexture;
    UBOTextureInfo occlusionTexture;
    UBOTextureInfo emissiveTexture;
    vec4 baseColorFactor;
    vec3 emissiveFactor;
    float metallicFactor;
    float roughnessFactor;
    float scale;
    float strength;
    float alphaCutoff;
    int alphaMode;
    int doubleSided;
} uboMaterial;

layout (set = 1, binding = 1) uniform sampler2D baseColorTexture;

#define PI 3.1415926535897932384626433832795
const float GAMMA = 2.2;
const float INV_GAMMA = 1.0 / GAMMA;

// ACES tone map (faster approximation)
// see: https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 toneMapACES_Narkowicz(vec3 color)
{
    const float A = 2.51;
    const float B = 0.03;
    const float C = 2.43;
    const float D = 0.59;
    const float E = 0.14;
    return clamp((color * (A * color + B)) / (color * (C * color + D) + E), 0.0, 1.0);
}

// Normal Distribution function
float D_GGX(float dotNH, float roughness)
{
    //    a = roughness^2
	// Dggx = a^2 / PI * ((a^2 - 1) (n dot h) ^ 2 + 1)^2
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
	return (alpha2)/(PI * denom * denom); 
}

// Geometric Shadowing function
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
	// G(l,v) = G1(l) * G1(v)
	//  G1(x) = 1 / (n dot x) * (1 - K) + K
	//      K = roughness^2 / 2                  direct
	//      K = (roughness + 1)^2 / 8            IBL
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;
	float GL = dotNL / (dotNL * (1.0 - k) + k);
	float GV = dotNV / (dotNV * (1.0 - k) + k);
	return GL * GV;
}

// Fresnel Schlick approximation translates
vec3 F_Schlick(float cosTheta, vec3 F0)
{
	// Rf(l,h) = F0 + (1 - F0) * (1 - (l dot h))^5
	// F0 = Ff(h,h) = Cspec
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

// Specular BRDF composition
vec3 BRDF(vec3 L, vec3 V, vec3 N, vec3 F0, float roughness)
{
	// Precalculate vectors and dot products	
	vec3 H = normalize (V + L);
	float dotNV = clamp(dot(N, V), 0.0, 1.0);
	float dotNL = clamp(dot(N, L), 0.0, 1.0);
	float dotNH = clamp(dot(N, H), 0.0, 1.0);
	float dotVH = clamp(dot(V, H), 0.0, 1.0);
	
	vec3 lightColor = uboScene.lightColor;
	
	// if (NdotL > 0.0 || NdotV > 0.0)
	// if (dotNL > 0.0)
	if (dotNL > 0.0 && dotNV > 0.0)
	{
		float rroughness = max(0.05, roughness);
		// D = Normal distribution (Distribution of the microfacets)
		float D = D_GGX(dotNH, roughness); 
		// G = Geometric shadowing term (Microfacets shadowing)
		float G = G_SchlicksmithGGX(dotNL, dotNV, rroughness);
		// F = Fresnel factor (Reflectance depending on angle of incidence)
		vec3 F = F_Schlick(dotVH, F0);

		vec3 spec = D * F * G / (4.0 * dotNL * dotNV);

		return spec * dotNL * lightColor;
	}
	else
	{
		return vec3(0.0);
	}
}

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 linearTosRGB(vec3 color)
{
    return pow(color, vec3(INV_GAMMA));
}

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 sRGBToLinear(vec3 srgbIn)
{
    return vec3(pow(srgbIn.xyz, vec3(GAMMA)));
}

vec4 sRGBToLinear(vec4 srgbIn)
{
    return vec4(sRGBToLinear(srgbIn.xyz), srgbIn.w);
}

vec3 toneMap(vec3 color)
{
    color *= uboScene.exposure;

	color = toneMapACES_Narkowicz(color);

    return linearTosRGB(color);
}

vec4 GetBaseColor()
{
	// vec4 color = uboMaterial.baseColorFactor;
	// return sRGBToLinear(color);
	return uboMaterial.baseColorFactor;
}

layout (location = 0) out vec4 outColor;

void main()
{
	// We use the following notation:
	// - V is the normalized vector from the shading location to the eye
	// - L is the normalized vector from the shading location to the light
	// - N is the surface normal in the same space as the above values
	// - H is the half vector, where H = normalize(L + V)

	vec3 N = normalize(v_normal);
	
	// world space.
	// vec3 V = normalize(uboScene.camera - v_position);
	// view space.
	vec3 V = normalize(- v_position);

	const vec3 dielectricSpecular = vec3(0.04, 0.04, 0.04);
	const vec3 black = vec3(0, 0, 0);

	vec4 baseColor = GetBaseColor();
	float metallic = uboMaterial.metallicFactor;
	float roughness = uboMaterial.roughnessFactor;
	
	vec3 diffuse = mix(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic);
	
	vec3 F0 = mix(dielectricSpecular, baseColor.rgb, metallic);
	
	vec3 L = normalize(uboScene.light - v_position);
	vec3 specular = BRDF(L, V, N, F0, roughness);
	
	vec3 ambient = vec3(0.0);
	vec3 color = ambient + diffuse + specular;

	// Tone mapping
	outColor = vec4(toneMap(color), baseColor.a);
}

// Metallic Roughness PBR Materials
//
// BRDF Lighting Equation
//
// f(l,v,h) = Diff(l,n) + F(l,h)G(l,v,h)D(h) / 4(n * l)(n * v)
//
//     l is the light direction
//     v is the view direction
//     h is the half vector
//     n is the normal
//
// The metallic-roughness material model is defined by the following properties:
// 
//     baseColor - The base color of the material
//     metallic  - The metalness of the material
//     roughness - The roughness of the material
//
//
// BRDF Diffuse
//
// Lambertian with energy conservation
//
//     Diff(l,n) = (1 - F(v*h))Cdiff/PI
// 
// cdiff is the diffuse reflected color. To conserve energy, the Fresnel term from specular
// component is subtracted from diffuse component.
//
//     const dielectricSpecular = rgb(0.04, 0.04, 0.04)
//     const black = rgb(0, 0, 0)
//     cdiff = lerp(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic)
//
//
// BRDF Specular
// 
// BRDF Specular : F
// F is the Fresnel function used to simulate the way light interacts with a surface at different
// viewing angles. 
//
// Schlick Fernel model
//
//     F(l,h) = F0 + (1 - F0) * (1 - v * h)^5 
//
//     const dielectricSpecular = rgb(0.04, 0.04, 0.04)
//     F0 = lerp(dieletricSpecular, baseColor.rgb, metallic)
//
// BRDF Specular : G
// G is the geometric occlusion derived from a normal distribution function like Smith’s function
//
//     G(l,v,h) = G1(n,l)G1(n,v)
//     G1(n,v) = 2(n * v) / ((n * v) + sqrt(a^2 + (1 - a^2)*(n * v)^2))
//     a = roughness^2
// 
// BRDF Specular : D
// D is the normal distribution function like GGX that defines the statistical distribution of microfacets.
//
//     D(h) = a^2 / (PI * ((n * h)^2(a - 1) + 1)^2)
//     a = roughness^2


