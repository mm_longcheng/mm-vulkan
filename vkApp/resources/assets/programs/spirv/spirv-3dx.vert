#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec2 a_texcoord_0;
layout (location = 2) in vec3 a_color_0;

layout (set = 0, binding = 0, std140) uniform UBOScene
{
	mat4 normal;
	mat4 projection;
	mat4 view;
	mat4 model;
} uboScene;

layout (location = 0) out vec3 v_position;
layout (location = 1) out vec2 v_texcoord_0;
layout (location = 2) out vec3 v_color_0;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
	v_position = vec3(uboScene.view * uboScene.model * vec4(a_position, 1.0));	
    v_texcoord_0 = a_texcoord_0;
    v_color_0 = a_color_0;
	gl_Position = uboScene.projection * vec4(v_position, 1.0);
}
