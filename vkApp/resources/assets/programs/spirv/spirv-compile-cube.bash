#!/bin/bash

Compiler=${VULKAN_SDK}/macOS/bin/glslangValidator

build-spv()
{
	${Compiler} -V $1 -o $1.spv
}

build-spv 'spirv-cube.frag'
build-spv 'spirv-cube.vert'

build-spv 'spirv-sphere.frag'
build-spv 'spirv-sphere.vert'
