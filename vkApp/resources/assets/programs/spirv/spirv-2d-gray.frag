#version 450

layout (set = 0, binding = 1) uniform sampler2D texture_0;

layout (location = 0) in vec2 v_texcoord;
layout (location = 1) in vec4 v_color;

layout (location = 0) out vec4 outColor;

void main()
{
	vec4 color = v_color * texture(texture_0, v_texcoord);
	outColor.rgb = vec3(0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b);
	outColor.a = color.a;
}
