#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord_0;

layout (set = 0, binding = 0, std140) uniform UBOScene
{
	mat4 projection;
	mat4 view;
} uboScene;

layout (push_constant, std140) uniform PushConstants
{
	mat4 normal;
    mat4 model;
} constants;

layout (location = 0) out vec3 v_position;
layout (location = 1) out vec3 v_normal;
layout (location = 2) out vec2 v_texcoord_0;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{	
	// world space.
	mat4 m = constants.model;
	mat3 normalMat = mat3(constants.normal);//  transpose(inverse(mat3(m)));
	v_position = vec3(m * vec4(a_position, 1.0));
	v_texcoord_0 = a_texcoord_0;
	v_normal = normalize(normalMat * a_normal);
	gl_Position = uboScene.projection * uboScene.view * vec4(v_position, 1.0);
}
