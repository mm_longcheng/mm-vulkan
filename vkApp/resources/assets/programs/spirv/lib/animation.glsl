

#ifdef USE_SKINNING

uniform highp sampler2D boneTexture;
uniform int boneTextureSize;

mat4 GetTextureMatrix( const in highp sampler2D tex, int textureSize, const in float i ) 
{
    float j = i * 4.0;
    float x = mod( j, float( textureSize ) );
    float y = floor( j / float( textureSize ) );

    float dx = 1.0 / float( textureSize );
    float dy = 1.0 / float( textureSize );

    y = dy * ( y + 0.5 );

    vec4 v1 = texture( tex, vec2( dx * ( x + 0.5 ), y ) );
    vec4 v2 = texture( tex, vec2( dx * ( x + 1.5 ), y ) );
    vec4 v3 = texture( tex, vec2( dx * ( x + 2.5 ), y ) );
    vec4 v4 = texture( tex, vec2( dx * ( x + 3.5 ), y ) );

    return mat4( v1, v2, v3, v4 );
}

void SkinningMatrix()
{
    int textureSize = boneTextureSize;
    mat4 boneMatX = GetTextureMatrix( boneTexture, textureSize, skinIndex.x );
    mat4 boneMatY = GetTextureMatrix( boneTexture, textureSize, skinIndex.y );
    mat4 boneMatZ = GetTextureMatrix( boneTexture, textureSize, skinIndex.z );
    mat4 boneMatW = GetTextureMatrix( boneTexture, textureSize, skinIndex.w );

    vec4 skinVertex = bindMatrix * vec4( transformed, 1.0 );
    vec4 skinned = vec4( 0.0 );
    skinned += boneMatX * skinVertex * skinWeight.x;
    skinned += boneMatY * skinVertex * skinWeight.y;
    skinned += boneMatZ * skinVertex * skinWeight.z;
    skinned += boneMatW * skinVertex * skinWeight.w;
    transformed = ( bindMatrixInverse * skinned ).xyz;

    mat4 skinMatrix = mat4( 0.0 );
    skinMatrix += skinWeight.x * boneMatX;
    skinMatrix += skinWeight.y * boneMatY;
    skinMatrix += skinWeight.z * boneMatZ;
    skinMatrix += skinWeight.w * boneMatW;
    skinMatrix = bindMatrixInverse * skinMatrix * bindMatrix;
    objectNormal = vec4( skinMatrix * vec4( objectNormal, 0.0 ) ).xyz;
    #ifdef USE_TANGENT
        objectTangent = vec4( skinMatrix * vec4( objectTangent, 0.0 ) ).xyz;
    #endif

}

mat4 getSkinningMatrix()
{
    mat4 skin = mat4(0);

#if defined(HAS_WEIGHTS_0_VEC4) && defined(HAS_JOINTS_0_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_0_VEC4 && 1 == u_Primitive.has_JOINTS_0_VEC4)
    {
        skin +=
            a_weights_0.x * u_jointMatrix[int(a_joints_0.x)] +
            a_weights_0.y * u_jointMatrix[int(a_joints_0.y)] +
            a_weights_0.z * u_jointMatrix[int(a_joints_0.z)] +
            a_weights_0.w * u_jointMatrix[int(a_joints_0.w)];
    }
#endif

#if defined(HAS_WEIGHTS_1_VEC4) && defined(HAS_JOINTS_1_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_1_VEC4 && 1 == u_Primitive.has_JOINTS_1_VEC4)
    {
        skin +=
            a_weights_1.x * u_jointMatrix[int(a_joints_1.x)] +
            a_weights_1.y * u_jointMatrix[int(a_joints_1.y)] +
            a_weights_1.z * u_jointMatrix[int(a_joints_1.z)] +
            a_weights_1.w * u_jointMatrix[int(a_joints_1.w)];
    }
#endif

    return skin;
}


mat4 getSkinningNormalMatrix()
{
    mat4 skin = mat4(0);

#if defined(HAS_WEIGHTS_0_VEC4) && defined(HAS_JOINTS_0_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_0_VEC4 && 1 == u_Primitive.has_JOINTS_0_VEC4)
    {
        skin +=
            a_weights_0.x * u_jointNormalMatrix[int(a_joints_0.x)] +
            a_weights_0.y * u_jointNormalMatrix[int(a_joints_0.y)] +
            a_weights_0.z * u_jointNormalMatrix[int(a_joints_0.z)] +
            a_weights_0.w * u_jointNormalMatrix[int(a_joints_0.w)];
    }
#endif

#if defined(HAS_WEIGHTS_1_VEC4) && defined(HAS_JOINTS_1_VEC4)
    if (1 == u_Primitive.has_WEIGHTS_1_VEC4 && 1 == u_Primitive.has_JOINTS_1_VEC4)
    {
        skin +=
            a_weights_1.x * u_jointNormalMatrix[int(a_joints_1.x)] +
            a_weights_1.y * u_jointNormalMatrix[int(a_joints_1.y)] +
            a_weights_1.z * u_jointNormalMatrix[int(a_joints_1.z)] +
            a_weights_1.w * u_jointNormalMatrix[int(a_joints_1.w)];
    }
#endif

    return skin;
}

#endif // !USE_SKINNING


#ifdef USE_MORPHING

vec4 getTargetPosition()
{
    vec4 pos = vec4(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            pos.xyz += u_morphWeights[i] * a_target_position[i];
        }
    }

    return pos;
}

vec3 getTargetNormal()
{
    vec3 normal = vec3(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            normal += u_morphWeights[i] * a_target_normal[i];
        }
    }

    return normal;
}


vec3 getTargetTangent()
{
    vec3 tangent = vec3(0);

    if (0 < u_morphCount)
    {
        int i = 0;
        for (i = 0;i < u_morphCount;++i)
        {
            tangent += u_morphWeights[i] * a_target_tangent[i];
        }
    }

    return tangent;
}

#endif // !USE_MORPHING
