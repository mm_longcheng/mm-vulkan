@echo off

:: order +X, -X, +Y, -Y, +Z, -Z
::
:: right left top bottom front back
::
:: rt lf up dn fr bk

set index=0

set arr[%index%]=cloudy_noon_rt.jpg & set /a index+=1
set arr[%index%]=cloudy_noon_lf.jpg & set /a index+=1
set arr[%index%]=cloudy_noon_up.jpg & set /a index+=1
set arr[%index%]=cloudy_noon_dn.jpg & set /a index+=1
set arr[%index%]=cloudy_noon_fr.jpg & set /a index+=1
set arr[%index%]=cloudy_noon_bk.jpg & set /a index+=1

set images=%arr[0]% %arr[1]% %arr[2]% %arr[3]% %arr[4]% %arr[5]%

basisu -ktx2 -linear -tex_type cubemap -uastc -output_file cloudy_noon.ktx2 %images%

GOTO :EOF
