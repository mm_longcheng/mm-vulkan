@echo off

:: --t2          ktx2
:: --uastc       uastc or --bcmp
:: --zcmp 3      Zstandard level
:: --assign_oetf linear or srgb
toktx --t2 --uastc 2 --zcmp 3 --assign_oetf linear Image_2.ktx2 Image_2.png

