@echo off

:: order +X, -X, +Y, -Y, +Z, -Z
::
:: right left top bottom front back
::
:: rt lf up dn fr bk

set index=0

set arr[%index%]=right.jpg  & set /a index+=1
set arr[%index%]=left.jpg   & set /a index+=1
set arr[%index%]=top.jpg    & set /a index+=1
set arr[%index%]=bottom.jpg & set /a index+=1
set arr[%index%]=front.jpg  & set /a index+=1
set arr[%index%]=back.jpg   & set /a index+=1

set images=%arr[0]% %arr[1]% %arr[2]% %arr[3]% %arr[4]% %arr[5]%

:: basisu -ktx2 -linear -tex_type cubemap -uastc -output_file island-uastc.ktx2 %images%
basisu -ktx2 -linear -tex_type cubemap        -output_file island-etc1s.ktx2 %images%

GOTO :EOF
