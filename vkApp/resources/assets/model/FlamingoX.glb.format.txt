{
	"asset": {
		"generator": "Khronos glTF Blender I/O v1.5.17",
		"version": "2.0"
	},
	"scene": 0,
	"scenes": [{
		"name": "Scene",
		"nodes": [0, 1, 2]
	}],
	"nodes": [{
		"mesh": 0,
		"name": "Mesh_0"
	}, {
		"name": "Light",
		"rotation": [0.16907575726509094, 0.7558803558349609, -0.27217137813568115, 0.570947527885437],
		"translation": [4.076245307922363, 5.903861999511719, -1.0054539442062378]
	}, {
		"name": "Camera",
		"rotation": [0.483536034822464, 0.33687159419059753, -0.20870360732078552, 0.7804827094078064],
		"translation": [7.358891487121582, 4.958309173583984, 6.925790786743164]
	}],
	"animations": [{
		"channels": [{
			"sampler": 0,
			"target": {
				"node": 0,
				"path": "weights"
			}
		}],
		"name": "flamingo_flyA_",
		"samplers": [{
			"input": 18,
			"interpolation": "LINEAR",
			"output": 19
		}]
	}],
	"materials": [{
		"name": "Material_0",
		"pbrMetallicRoughness": {
			"metallicFactor": 0
		}
	}],
	"meshes": [{
		"extras": {
			"targetNames": ["flamingo_flyA_000", "flamingo_flyA_001", "flamingo_flyA_002", "flamingo_flyA_003", "flamingo_flyA_004", "flamingo_flyA_005", "flamingo_flyA_006", "flamingo_flyA_007", "flamingo_flyA_008", "flamingo_flyA_009", "flamingo_flyA_010", "flamingo_flyA_011", "flamingo_flyA_012", "flamingo_flyA_013"]
		},
		"name": "Mesh_0",
		"primitives": [{
			"attributes": {
				"POSITION": 0,
				"TEXCOORD_0": 1,
				"COLOR_0": 2
			},
			"indices": 3,
			"material": 0,
			"targets": [{
				"POSITION": 4
			}, {
				"POSITION": 5
			}, {
				"POSITION": 6
			}, {
				"POSITION": 7
			}, {
				"POSITION": 8
			}, {
				"POSITION": 9
			}, {
				"POSITION": 10
			}, {
				"POSITION": 11
			}, {
				"POSITION": 12
			}, {
				"POSITION": 13
			}, {
				"POSITION": 14
			}, {
				"POSITION": 15
			}, {
				"POSITION": 16
			}, {
				"POSITION": 17
			}]
		}],
		"weights": [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	}],
	"accessors": [{
		"bufferView": 0,
		"componentType": 5126,
		"count": 337,
		"max": [26.700000762939453, 10.699999809265137, 100.4000015258789],
		"min": [-25.5, -78.4000015258789, -76.19999694824219],
		"type": "VEC3"
	}, {
		"bufferView": 1,
		"componentType": 5126,
		"count": 337,
		"type": "VEC2"
	}, {
		"bufferView": 2,
		"componentType": 5123,
		"count": 337,
		"normalized": true,
		"type": "VEC4"
	}, {
		"bufferView": 3,
		"componentType": 5123,
		"count": 1626,
		"type": "SCALAR"
	}, {
		"bufferView": 4,
		"componentType": 5126,
		"count": 337,
		"max": [0, 0, 0],
		"min": [0, 0, 0],
		"type": "VEC3"
	}, {
		"bufferView": 5,
		"componentType": 5126,
		"count": 337,
		"max": [2.3999996185302734, 3.799999237060547, 0.7000007629394531],
		"min": [-3.6000003814697266, 0.5999984741210938, -3.200000762939453],
		"type": "VEC3"
	}, {
		"bufferView": 6,
		"componentType": 5126,
		"count": 337,
		"max": [11.699999809265137, 11.200000762939453, 2.8000011444091797],
		"min": [-13.700000762939453, 0.7999999523162842, -3.5],
		"type": "VEC3"
	}, {
		"bufferView": 7,
		"componentType": 5126,
		"count": 337,
		"max": [36.29999923706055, 22.399999618530273, 3.9000000953674316],
		"min": [-36.69999694824219, -0.40000003576278687, -4],
		"type": "VEC3"
	}, {
		"bufferView": 8,
		"componentType": 5126,
		"count": 337,
		"max": [72.5, 41.5, 4],
		"min": [-68.5999984741211, -2.4000000953674316, -9.800000190734863],
		"type": "VEC3"
	}, {
		"bufferView": 9,
		"componentType": 5126,
		"count": 337,
		"max": [88.79999542236328, 94.10000610351562, 5.300000190734863],
		"min": [-89.69999694824219, -4.699999809265137, -12.399999618530273],
		"type": "VEC3"
	}, {
		"bufferView": 10,
		"componentType": 5126,
		"count": 337,
		"max": [64.79999542236328, 143.10000610351562, 6.199999809265137],
		"min": [-75, -6.800000190734863, -9.899999618530273],
		"type": "VEC3"
	}, {
		"bufferView": 11,
		"componentType": 5126,
		"count": 337,
		"max": [31.200000762939453, 160.39999389648438, 6.5],
		"min": [-39.70000076293945, -8.300000190734863, -6.599999904632568],
		"type": "VEC3"
	}, {
		"bufferView": 12,
		"componentType": 5126,
		"count": 337,
		"max": [19.30000114440918, 163.5, 5.800000190734863],
		"min": [-19.5, -9.100000381469727, -9.300000190734863],
		"type": "VEC3"
	}, {
		"bufferView": 13,
		"componentType": 5126,
		"count": 337,
		"max": [32.400001525878906, 159.20001220703125, 4.599999904632568],
		"min": [-27, -8.899999618530273, -10.199999809265137],
		"type": "VEC3"
	}, {
		"bufferView": 14,
		"componentType": 5126,
		"count": 337,
		"max": [66.69999694824219, 137.10000610351562, 3.1999998092651367],
		"min": [-60.70000076293945, -7.800000190734863, -8.999999046325684],
		"type": "VEC3"
	}, {
		"bufferView": 15,
		"componentType": 5126,
		"count": 337,
		"max": [81.0999984741211, 83.5999984741211, 8.900001525878906],
		"min": [-82.5999984741211, -6.099999904632568, -5.199999809265137],
		"type": "VEC3"
	}, {
		"bufferView": 16,
		"componentType": 5126,
		"count": 337,
		"max": [53.70000076293945, 26.300003051757812, 11.400001525878906],
		"min": [-61.69999694824219, -4.100000381469727, -1.8000001907348633],
		"type": "VEC3"
	}, {
		"bufferView": 17,
		"componentType": 5126,
		"count": 337,
		"max": [15.90000057220459, 2.6000022888183594, 5.80000114440918],
		"min": [-20.200000762939453, -1.899999976158142, -0.5],
		"type": "VEC3"
	}, {
		"bufferView": 18,
		"componentType": 5126,
		"count": 34,
		"max": [1.375],
		"min": [0],
		"type": "SCALAR"
	}, {
		"bufferView": 19,
		"componentType": 5126,
		"count": 476,
		"type": "SCALAR"
	}],
	"bufferViews": [{
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 0
	}, {
		"buffer": 0,
		"byteLength": 2696,
		"byteOffset": 4044
	}, {
		"buffer": 0,
		"byteLength": 2696,
		"byteOffset": 6740
	}, {
		"buffer": 0,
		"byteLength": 3252,
		"byteOffset": 9436
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 12688
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 16732
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 20776
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 24820
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 28864
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 32908
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 36952
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 40996
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 45040
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 49084
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 53128
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 57172
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 61216
	}, {
		"buffer": 0,
		"byteLength": 4044,
		"byteOffset": 65260
	}, {
		"buffer": 0,
		"byteLength": 136,
		"byteOffset": 69304
	}, {
		"buffer": 0,
		"byteLength": 1904,
		"byteOffset": 69440
	}],
	"buffers": [{
		"byteLength": 71344
	}]
}