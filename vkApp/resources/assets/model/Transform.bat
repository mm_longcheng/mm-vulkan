@echo off

SET TOKTX_OPTIONS=--t2 --uastc 2 --zcmp 3 --assign_oetf linear

gltf-Transform uastc BoxTextured_Binary.glb BoxTextured_Binary_uastc.glb
gltf-Transform draco BoxTextured_Binary_uastc.glb BoxTextured_Binary_uastc_draco.glb

gltf-Transform uastc BoxTextured_Binary.glb BoxTextured_Binary_Embedded.glb
gltf-Transform draco BoxTextured_Binary_Embedded.glb BoxTextured_Binary_Embedded_draco.glb

gltf-Transform uastc BoxTextured_Embedded.gltf BoxTextured_Embedded_uastc.gltf

gltf-Transform uastc BoxTextured.gltf BoxTextured_uastc.gltf