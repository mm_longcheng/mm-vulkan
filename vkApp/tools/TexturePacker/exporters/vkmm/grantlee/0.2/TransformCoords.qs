var ExportData = function(result)
{
    var output = "";
    var settings = result.settings;
    var texture = result.texture;

    var count = result.allSprites.length;

    output += ":path=" + settings.textureSubPath + "\n";
    output += ":image=" + texture.fullName + "\n";
    output += ":imageSize=" + texture.size.width + "x" + texture.size.height + "\n";
    output += ":sheetSize=" + "22" + "x" + count + "\n";
    output += "\n";

    // --+-----------+-----------+-----------+-+-+-----+-----------+---
    // id fx fy fw fh sx sy sw sh ox oy ow oh r t ax ay bl bb br bt fn

    var arr = Array(22);

    arr[ 0] = 2;
    arr[ 1] = 2;
    arr[ 2] = 2;
    arr[ 3] = 2;
    arr[ 4] = 2;
    arr[ 5] = 2;
    arr[ 6] = 2;
    arr[ 7] = 2;
    arr[ 8] = 2;
    arr[ 9] = 2;
    arr[10] = 2;
    arr[11] = 2;
    arr[12] = 2;
    arr[13] = 1;
    arr[14] = 1;
    arr[15] = 2;
    arr[16] = 2;
    arr[17] = 2;
    arr[18] = 2;
    arr[19] = 2;
    arr[20] = 2;
    arr[21] = 2;

    arr[ 0] = MaxStringLangth(count, arr[ 0]);

    for (var j = 0; j < count; j++)
    {
        var sprite = result.allSprites[j];

		var bl = sprite.scale9Borders.x;
		var bb = sprite.untrimmedSize.height - sprite.scale9Borders.y - sprite.scale9Borders.height;
		var br = sprite.untrimmedSize.width  - sprite.scale9Borders.x - sprite.scale9Borders.width ;
		var bt = sprite.scale9Borders.y;

        arr[ 1] = MaxStringLangth(sprite.frameRect.x, arr[ 1]);
        arr[ 2] = MaxStringLangth(sprite.frameRect.y, arr[ 2]);
        arr[ 3] = MaxStringLangth(sprite.frameRect.width , arr[ 3]);
        arr[ 4] = MaxStringLangth(sprite.frameRect.height, arr[ 4]);

        arr[ 5] = MaxStringLangth(sprite.sourceRect.x, arr[ 5]);
        arr[ 6] = MaxStringLangth(sprite.sourceRect.y, arr[ 6]);
        arr[ 7] = MaxStringLangth(sprite.sourceRect.width , arr[ 7]);
        arr[ 8] = MaxStringLangth(sprite.sourceRect.height, arr[ 8]);

        arr[ 9] = MaxStringLangth(sprite.cornerOffset.x, arr[ 9]);
        arr[10] = MaxStringLangth(sprite.cornerOffset.y, arr[10]);
        arr[11] = MaxStringLangth(sprite.untrimmedSize.width , arr[11]);
        arr[12] = MaxStringLangth(sprite.untrimmedSize.height, arr[12]);

        arr[15] = MaxStringLangth(sprite.pivotPoint.x, arr[15]);
        arr[16] = MaxStringLangth(sprite.pivotPoint.y, arr[16]);

		arr[17] = MaxStringLangth(bl, arr[17]);
		arr[18] = MaxStringLangth(bb, arr[18]);
		arr[19] = MaxStringLangth(br, arr[19]);
		arr[20] = MaxStringLangth(bt, arr[20]);
    }

    output += PaddingString("id", " ", arr[ 0]);
    output += "\t";

    output += PaddingString("fx", " ", arr[ 1]);
    output += "\t";
    output += PaddingString("fy", " ", arr[ 2]);
    output += "\t";
    output += PaddingString("fw", " ", arr[ 3]);
    output += "\t";
    output += PaddingString("fh", " ", arr[ 4]);
    output += "\t";

    output += PaddingString("sx", " ", arr[ 5]);
    output += "\t";
    output += PaddingString("sy", " ", arr[ 6]);
    output += "\t";
    output += PaddingString("sw", " ", arr[ 7]);
    output += "\t";
    output += PaddingString("sh", " ", arr[ 8]);
    output += "\t";

    output += PaddingString("ox", " ", arr[ 9]);
    output += "\t";
    output += PaddingString("oy", " ", arr[10]);
    output += "\t";
    output += PaddingString("ow", " ", arr[11]);
    output += "\t";
    output += PaddingString("oh", " ", arr[12]);
    output += "\t";

    output += PaddingString("r", " ", arr[13]);
    output += "\t";
    output += PaddingString("t", " ", arr[14]);
    output += "\t";

    output += PaddingString("ax", " ", arr[15]);
    output += "\t";
    output += PaddingString("ay", " ", arr[16]);
    output += "\t";

    output += PaddingString("bl", " ", arr[17]);
    output += "\t";
    output += PaddingString("bb", " ", arr[18]);
    output += "\t";
    output += PaddingString("br", " ", arr[19]);
    output += "\t";
    output += PaddingString("bt", " ", arr[20]);
    output += "\t";

    output += "fn";
    output += "\n";

    for (var j = 0; j < count; j++)
    {
        var sprite = result.allSprites[j];

		var bl = sprite.scale9Borders.x;
		var bb = sprite.untrimmedSize.height - sprite.scale9Borders.y - sprite.scale9Borders.height;
		var br = sprite.untrimmedSize.width  - sprite.scale9Borders.x - sprite.scale9Borders.width ;
		var bt = sprite.scale9Borders.y;

        var fn = settings.trimSpriteNames ? sprite.trimmedName : sprite.fullName;

        output += PaddingString(j, " ", arr[ 0]);
        output += "\t";

        output += PaddingString(sprite.frameRect.x, " ", arr[ 1]);
        output += "\t";
        output += PaddingString(sprite.frameRect.y, " ", arr[ 2]);
        output += "\t";
        output += PaddingString(sprite.frameRect.width , " ", arr[ 3]);
        output += "\t";
        output += PaddingString(sprite.frameRect.height, " ", arr[ 4]);
        output += "\t";

        output += PaddingString(sprite.sourceRect.x, " ", arr[ 5]);
        output += "\t";
        output += PaddingString(sprite.sourceRect.y, " ", arr[ 6]);
        output += "\t";
        output += PaddingString(sprite.sourceRect.width , " ", arr[ 7]);
        output += "\t";
        output += PaddingString(sprite.sourceRect.height, " ", arr[ 8]);
        output += "\t";

        output += PaddingString(sprite.cornerOffset.x, " ", arr[ 9]);
        output += "\t";
        output += PaddingString(sprite.cornerOffset.y, " ", arr[10]);
        output += "\t";
        output += PaddingString(sprite.untrimmedSize.width , " ", arr[11]);
        output += "\t";
        output += PaddingString(sprite.untrimmedSize.height, " ", arr[12]);
        output += "\t";

        output += PaddingBoolToString(sprite.rotated, " ", arr[13]);
        output += "\t";
        output += PaddingBoolToString(sprite.trimmed, " ", arr[14]);
        output += "\t";

        output += PaddingString(sprite.pivotPoint.x, " ", arr[15]);
        output += "\t";
        output += PaddingString(sprite.pivotPoint.y, " ", arr[16]);
        output += "\t";

		output += PaddingString(bl, " ", arr[17]);
		output += "\t";
		output += PaddingString(bb, " ", arr[18]);
		output += "\t";
		output += PaddingString(br, " ", arr[19]);
		output += "\t";
		output += PaddingString(bt, " ", arr[20]);
		output += "\t";

        output += EscapeSpecialChars(fn);
        output += "\n";
    }
    return output.trim();
}

var MaxStringLangth = function(num, max)
{
    var ns = "" + num;
    var len = ns.length;
    return len < max ? max : len;
};


var PaddingString = function(num, s, length)
{
    var ns = "" + num;
    var len = ns.length;
    var diff = length - len;
    if(diff > 0) 
    {
        return Array(diff + 1).join(s) + num;
    }
    else
    {
        return num;
    }
};

var PaddingBoolToString = function(b, s, length)
{
    var bs = b ? "1" : "0";
    return PaddingString(bs , s, length);
};


var RectToString = function(rect)
{
    var x = PaddingString(rect.x, " ", 5);
    var y = PaddingString(rect.y, " ", 5);
    var w = PaddingString(rect.width , " ", 5);
    var h = PaddingString(rect.height, " ", 5);
    return "(" + x + ", " + y + ", " + w + ", " + h + ")";
};

var PointToString = function(point)
{
    var x = PaddingString(point.x, " ", 5);
    var y = PaddingString(point.y, " ", 5);
    return "(" + x + ", " + y + ")";
};

var SizeToString = function(size)
{
    var w = PaddingString(size.width , " ", 5);
    var h = PaddingString(size.height, " ", 5);
    return "(" + w + ", " + h + ")";
};

var BoolToString = function(b)
{
    var bs = b ? "1" : "0";
    return PaddingString(bs , " ", 7);
};

var ExportData1 = function(result)
{
    var output = "";
    var texture = result.texture;

    // if there is at least one sprite with scale9 enabled, the borders will be imported by unity
    var scale9SpritesFound = false;
    for (var j = 0; j < result.allSprites.length; j++)
    {
        scale9SpritesFound = scale9SpritesFound || result.allSprites[j].scale9Enabled;
    }

    output += ":format=40300\n";
    output += ":texture=" + texture.fullName + "\n";
    output += ":size=" + texture.size.width + "x" + texture.size.height + "\n";
    output += texture.normalMapFileName ? ":normalmap=" + texture.normalMapFileName + "\n" : "";
    output += ":pivotpoints=" + (result.settings.writePivotPoints ? "enabled" : "disabled") + "\n";
    output += ":borders=" + (scale9SpritesFound ? "enabled" : "disabled") + "\n";
    output += ":alphahandling=" + result.settings.alphaHandling + "\n";
    output += "\n";

    for (var j = 0; j < result.allSprites.length; j++)
    {
        var sprite = result.allSprites[j];
        output += EscapeSpecialChars(sprite.trimmedName) + ";";

        // frame rect in sheet
        output += sprite.frameRect.x + ";";
        output += MirroredFrameRectY(sprite, result.texture.size.height) + ";";
        output += sprite.frameRect.width + ";";
        output += sprite.frameRect.height + "; ";

        // pivot point
        output += TrimmedPivotX(sprite) + ";";
        output += TrimmedMirroredPivotY(sprite) + "; ";

        // borders
        if (sprite.scale9Enabled)
        {
            output += sprite.scale9Borders.x + ";";
            output += sprite.frameRect.width - sprite.scale9Borders.x - sprite.scale9Borders.width + ";";
            output += sprite.scale9Borders.y + ";";
            output += sprite.frameRect.height - sprite.scale9Borders.y - sprite.scale9Borders.height + ";";
        }
        else
        {
            output += "0;0;0;0;"
        }

        if (sprite.vertices.length > 0)
        {
            output += PrintVertices(sprite) + ";"
        }

        output = output.slice(0, -1) + "\n"; // replace last ";" by newline
    }
    return output.trim();
}
ExportData.filterName = "ExportData";
Library.addFilter("ExportData");


var EscapeSpecialChars = function(name)
{
    return name.replace(/%/g, "%25")
               .replace(/#/g, "%23")
               .replace(/:/g, "%3A")
               .replace(/;/g, "%3B")
               .replace(/\\/g, "-")
               .replace(/\//g, "-")
};


var MirroredFrameRectY = function(sprite, textureHeight)
{
    return "" + (textureHeight - sprite.frameRect.y - sprite.frameRect.height);
};


var TrimmedPivotX = function(sprite)
{
    var ppX = sprite.pivotPointNorm.x;

    if(sprite.trimmed && sprite.sourceRect.width > 0) // polygon trim -> normalize pp based on trimmed size
    {
        ppX = (sprite.pivotPoint.x - sprite.sourceRect.x) / sprite.sourceRect.width;
    }
    return "" + ppX;
};


var TrimmedMirroredPivotY = function(sprite)
{
    var ppY = sprite.pivotPointNorm.y;

    if(sprite.trimmed && sprite.sourceRect.height > 0) // polygon trim -> normalize pp based on trimmed size
    {
        ppY = sprite.untrimmedSize.height - sprite.pivotPoint.y; // move origin to top-left
        ppY = ppY - sprite.sourceRect.y;                         // subtract corner offset, as unity does not support trimming
        ppY = ppY / sprite.sourceRect.height;                    // normalized pivot point, based on trimmed size
        ppY = 1 - ppY;                                           // move origin back to bottom-left
    }
    return "" + ppY;
};


var PrintVertices = function(sprite)
{
    var height = sprite.frameRect.height
    var str = ''
    var vertices = sprite.vertices
    var sourceRect = sprite.sourceRect
    str += " " + vertices.length
    for (var i = 0; i < vertices.length; i++)
    {
        str += ";" + (vertices[i].x - sourceRect.x) +
               ";" + (height - vertices[i].y + sourceRect.y)
    }
    var triangleIndices = sprite.triangleIndices
    str += "; " + triangleIndices.length / 3;
    for (i = 0; i < triangleIndices.length; i++)
    {
        str += ";" + triangleIndices[i]
    }
    return str
};
