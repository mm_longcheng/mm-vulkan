/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "stdafx.h"

/* Visual Leak Detector */
// #define MM_USE_VLD
#if defined(_DEBUG) && defined(MM_USE_VLD)
#include "vld.h"
#endif

#include <tchar.h>
#include <stdio.h>

#include <exception>

#include "mmUIWindowApplication.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    struct mmUIWindowApplication hUIApplication;

    // Use HeapSetInformation to specify that the process should
    // terminate if the heap manager detects an error in any heap used
    // by the process.
    // The return value is ignored, because we want to continue running in the
    // unlikely event that HeapSetInformation fails.
    HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);

    try
    {
        mmUIWindowApplication_RegisterClass(hInstance);

        mmUIWindowApplication_Init(&hUIApplication);

        mmUIWindowApplication_SetInstance(&hUIApplication, hInstance);
        mmUIWindowApplication_SetCommandShow(&hUIApplication, nCmdShow);
        mmUIWindowApplication_SetManifestFile(&hUIApplication, "launch.config");

        mmUIWindowApplication_OnFinishLaunching(&hUIApplication);

        mmUIWindowApplication_OnStart(&hUIApplication);
        mmUIWindowApplication_OnJoin(&hUIApplication);

        mmUIWindowApplication_OnBeforeTerminate(&hUIApplication);

        mmUIWindowApplication_Destroy(&hUIApplication);

        mmUIWindowApplication_UnregisterClass(hInstance);
    }
    catch (std::exception& e)
    {
        const char* pMessage = e.what();
        mmPrintf("A untreated exception occur: %s\n", pMessage);
        MessageBoxA(NULL, pMessage, "An exception has occurred!", MB_ICONERROR | MB_TASKMODAL);
    }

    return hUIApplication.hTerminateCode;
}
