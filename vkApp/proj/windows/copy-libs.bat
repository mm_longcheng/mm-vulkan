:: copylibs.bat 32 x86 Debug   "_d"
:: copylibs.bat 32 x86 Release ""
:: copylibs.bat 64 x64 Debug   "_d"
:: copylibs.bat 64 x64 Release ""

@echo off

@set OPTION=/s /e /h /d /y

call :cp-libs %3 %4 %1 %2
call datafiles-mslink.bat %3

GOTO:EOF

:: cp-libs Debug _d 32 x86
:cp-libs
call :cp-lib  mm-libx %~1   pthread        libpthread_shared%~2.dll
call :cp-lib  mm-libx %~1   zlib           libz_shared%~2.dll
call :cp-lib  mm-libx %~1   zlib           libminizip_shared%~2.dll
call :cp-lib  mm-libx %~1   zstd           libzstd_shared%~2.dll
call :cp-lib  mm-libx %~1   dfdutils       libdfdutils_shared%~2.dll

call :cp-lib  mm-libx %~1   FreeImage      libFreeImage_shared%~2.dll
call :cp-lib  mm-libx %~1   jpeg           libjpeg_shared%~2.dll
call :cp-lib  mm-libx %~1   libwebp        libwebp_shared%~2.dll
call :cp-lib  mm-libx %~1   libpng         libpng_shared%~2.dll
call :cp-lib  mm-libx %~1   tiff           libtiff_shared%~2.dll
call :cp-lib  mm-libx %~1   openjpeg       libopenjp2_shared%~2.dll
call :cp-lib  mm-libx %~1   LibRaw         libRaw_shared%~2.dll
call :cp-lib  mm-libx %~1   Imath          libImath_shared%~2.dll
call :cp-lib  mm-libx %~1   openexr        libIex_shared%~2.dll
call :cp-lib  mm-libx %~1   openexr        libIlmThread_shared%~2.dll
call :cp-lib  mm-libx %~1   openexr        libOpenEXR_shared%~2.dll
call :cp-lib  mm-libx %~1   jxrlib         libjxr_shared%~2.dll
call :cp-lib  mm-libx %~1   openal-soft    libOpenAL_shared%~2.dll

call :cp-core mm-core %~1   mm             libmm_core_shared%~2.dll
call :cp-core mm-core %~1   dish           libmm_dish_shared%~2.dll
call :cp-core mm-core %~1   dish           libmm_unicode_shared%~2.dll
call :cp-core mm-core %~1   dish           libmm_sxwnl_shared%~2.dll
call :cp-core mm-core %~1   nwsi           libmm_nwsi_shared%~2.dll
call :cp-core mm-nes  %~1   emu            libmm_emu_shared%~2.dll

:: sdk lib.
call :cp-sdk  mm-libx %~1   vulkan %~4     vulkan-1.dll
:: Debug only
call :cp-sdk  mm-libx Debug vld    %~4     dbghelp.dll
call :cp-sdk  mm-libx Debug vld    %~4     vld_%~4.dll
call :cp-sdk  mm-libx Debug vld    %~4     Microsoft.DTfW.DHL.manifest

call :cp-file launch-binary.config %~1     launch.config

GOTO:EOF

:: cp-lib mm-lib Debug pthread libpthread_shared_d.dll
:cp-lib
@xcopy %MM_HOME%\\%~1\\build\\%~3\\proj_windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-core mm-core Debug mm libmm_core_shared_d.dll
:cp-core
@xcopy %MM_HOME%\\%~1\\%~3\\proj\\windows\\bin\\%~2\%~4       bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-sdk mm-libx Debug vulkan x64 vulkan-1.dll
:cp-sdk
@xcopy %MM_HOME%\\%~1\\sdk\\%~3\\bin\\windows\\%~4\%~5        bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-file %~1 Debug launch.config
:cp-file
@echo f | @xcopy %~1                                            bin\\%~2\%~3 /h /d /y  1>nul
GOTO:EOF

