#ifndef __targetver_h__
#define __targetver_h__

// Including SDKDDKVer.h will define the highest version of the Windows platform available.

// If you want to build an application for a previous Windows platform, include WinSDKVer.h and
// Set the _WIN32_WINNT macro to the supported platform, and then include SDKDDKVer.h.

#include <SDKDDKVer.h>

#endif//__targetver_h__

