:: call datafiles_mslink.bat Debug
:: call datafiles_mslink.bat Release

@echo off 

call :ln-target %1
GOTO:EOF

:: ln-target Debug
:ln-target
call :ln-s /j "bin/%~1/resources" "../../resources"
GOTO:EOF

:: ln-s mode target source
:ln-s
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF