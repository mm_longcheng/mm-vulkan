//
//  ViewController.h
//  vkApp macOS
//
//  Created by longcheng on 2021/6/28.
//  Copyright © 2021 mm. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// Our macOS view controller.
@interface ViewController : NSViewController

@end
