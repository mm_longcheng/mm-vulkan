//
//  ViewController.m
//  vkApp iOS
//
//  Created by longcheng on 2021/6/28.
//  Copyright © 2021 mm. All rights reserved.
//

#import "ViewController.h"
#import "mmUIViewSurfaceMaster.h"

#import "nwsi/mmObjcARC.h"
#import "nwsi/mmNativeApplication.h"

#import "application/mmActivityMaster.h"

@interface ViewController()
{
    struct mmNativeApplication hApplication;
    struct mmActivityMaster pActivityMaster;
}

@property (strong, nonatomic) mmUIViewSurfaceMaster* viewSurfaceMaster;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIApplication* pUIApplication = [UIApplication sharedApplication];
    struct mmSurfaceMaster* pSurfaceMaster = &self->hApplication.hSurfaceMaster;
    
    self.viewSurfaceMaster = (mmUIViewSurfaceMaster*)self.view;
    
    [self.viewSurfaceMaster Init];
    
    mmNativeApplication_Init(&self->hApplication);
    mmActivityMaster_Init(&self->pActivityMaster);
    
    mmNativeApplication_SetAppHandler(&self->hApplication, (__bridge void*)pUIApplication);
    mmNativeApplication_SetActivityMaster(&self->hApplication, &self->pActivityMaster.hSuper);
    
    mmNativeApplication_OnFinishLaunching(&self->hApplication);
    mmNativeApplication_OnStart(&self->hApplication);
    
    [self.viewSurfaceMaster SetSurfaceMaster: pSurfaceMaster];
    [self.viewSurfaceMaster OnFinishLaunching];
    
    __static_ViewController_AttachNotification(self);
}

- (void)dealloc
{
    // when Application Terminate, the dealloc may not trigger.
    // we Terminate at ApplicationWillTerminate event.
    mmObjc_Dealloc(super);
}

- (void)OnApplicationDidEnterBackground:(NSNotification *)notification
{
    mmNativeApplication_OnEnterBackground(&self->hApplication);
}
- (void)OnApplicationWillEnterForeground:(NSNotification *)notification
{
    mmNativeApplication_OnEnterForeground(&self->hApplication);
}
- (void)OnApplicationWillTerminate:(NSNotification *)notification
{
    __static_ViewController_DetachNotification(self);
    
    [self.viewSurfaceMaster OnBeforeTerminate];
    
    mmNativeApplication_OnShutdown(&self->hApplication);
    mmNativeApplication_OnJoin(&self->hApplication);
    mmNativeApplication_OnBeforeTerminate(&self->hApplication);
    
    mmActivityMaster_Destroy(&self->pActivityMaster);
    mmNativeApplication_Destroy(&self->hApplication);
    
    [self.viewSurfaceMaster Destroy];
}

static void __static_ViewController_AttachNotification(ViewController* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify addObserver:p selector:@selector(OnApplicationDidEnterBackground:)
                    name:UIApplicationDidEnterBackgroundNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillEnterForeground:)
                    name:UIApplicationWillEnterForegroundNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillTerminate:)
                    name:UIApplicationWillTerminateNotification
                  object:nil];
}
static void __static_ViewController_DetachNotification(ViewController* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify removeObserver:p name:UIApplicationDidEnterBackgroundNotification object:nil];
    [pNotify removeObserver:p name:UIApplicationWillEnterForegroundNotification object:nil];
    [pNotify removeObserver:p name:UIApplicationWillTerminateNotification object:nil];
}

@end
