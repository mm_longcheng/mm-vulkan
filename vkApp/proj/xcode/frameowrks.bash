#!/bin/bash

path_libx=proj_xcode/build/Debug-iphoneos
path_core=proj/xcode/build/Debug-iphoneos

mk-sdks()
{
	rm Frameworks/${3}.xcframework
	ln -s ${MM_HOME}/${1}/sdk/${2}/lib/xcode/${3}.xcframework Frameworks/${3}.xcframework
}

mk-libx()
{
	rm Frameworks/${3}.framework
	ln -s ${MM_HOME}/${1}/build/${2}/${path_libx}/${3}.framework Frameworks/${3}.framework
}

mk-core()
{
	rm Frameworks/${3}.framework
	ln -s ${MM_HOME}/${1}/${2}/${path_core}/${3}.framework Frameworks/${3}.framework
}

mk-sdks mm-libx vulkan          MoltenVK

mk-libx mm-libx zlib            libz_shared
mk-libx mm-libx zlib            libz_static
mk-libx mm-libx zlib            libminizip_shared
mk-libx mm-libx zlib            libminizip_static
mk-libx mm-libx zstd            libzstd_shared
mk-libx mm-libx zstd            libzstd_static
mk-libx mm-libx cJSON           libcJSON_shared
mk-libx mm-libx cJSON           libcJSON_static
mk-libx mm-libx basis_universal libbasisu_shared
mk-libx mm-libx basis_universal libbasisu_static
mk-libx mm-libx draco           libdraco_shared
mk-libx mm-libx draco           libdraco_static
mk-libx mm-libx dfdutils        libdfdutils_shared
mk-libx mm-libx dfdutils        libdfdutils_static
mk-libx mm-libx glslang         libSPIRV_shared
mk-libx mm-libx glslang         libSPIRV_static
mk-libx mm-libx glslang         libglslang_shared
mk-libx mm-libx glslang         libglslang_static
mk-libx mm-libx KTX-Software    libktx_shared
mk-libx mm-libx KTX-Software    libktx_static
mk-libx mm-libx Imath           libImath_shared
mk-libx mm-libx Imath           libImath_static
mk-libx mm-libx openexr         libIex_shared
mk-libx mm-libx openexr         libIex_static
mk-libx mm-libx openexr         libIlmThread_shared
mk-libx mm-libx openexr         libIlmThread_static
mk-libx mm-libx openexr         libOpenEXR_shared
mk-libx mm-libx openexr         libOpenEXR_static
mk-libx mm-libx jpeg            libjpeg_shared
mk-libx mm-libx jpeg            libjpeg_static
mk-libx mm-libx jxrlib          libjxr_shared
mk-libx mm-libx jxrlib          libjxr_static
mk-libx mm-libx openjpeg        libopenjp2_shared
mk-libx mm-libx openjpeg        libopenjp2_static
mk-libx mm-libx libpng          libpng_shared
mk-libx mm-libx libpng          libpng_static
mk-libx mm-libx LibRaw          libRaw_shared
mk-libx mm-libx LibRaw          libRaw_static
mk-libx mm-libx libwebp         libwebp_shared
mk-libx mm-libx libwebp         libwebp_static
mk-libx mm-libx tiff            libtiff_shared
mk-libx mm-libx tiff            libtiff_static
mk-libx mm-libx FreeImage       libFreeImage_shared
mk-libx mm-libx FreeImage       libFreeImage_static
mk-libx mm-libx openal-soft     libOpenAL_shared
mk-libx mm-libx openal-soft     libOpenAL_static
mk-libx mm-libx libogg          libogg_shared
mk-libx mm-libx libogg          libogg_static
mk-libx mm-libx libvorbis       libvorbis_shared
mk-libx mm-libx libvorbis       libvorbis_static

mk-core mm-core mm              libmm_core_shared
mk-core mm-core mm              libmm_core_static
mk-core mm-core mm              libmm_net_shared
mk-core mm-core mm              libmm_net_static
mk-core mm-core dish            libmm_dish_shared
mk-core mm-core dish            libmm_dish_static
mk-core mm-core dish            libmm_unicode_shared
mk-core mm-core dish            libmm_unicode_static
mk-core mm-core dish            libmm_sxwnl_shared
mk-core mm-core dish            libmm_sxwnl_static
mk-core mm-core nwsi            libmm_nwsi_shared
mk-core mm-core nwsi            libmm_nwsi_static
mk-core mm-nes  emu             libmm_nes_shared
mk-core mm-nes  emu             libmm_nes_static
