package org.mm.corps.vkApp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import org.mm.core.mmLogger;
import org.mm.nwsi.mmActivityMaster;
import org.mm.nwsi.mmUIApplication;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getSimpleName();

    private mmUIApplication pUIApplication = null;
    private mmActivityMaster pActivityMaster = null;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.i(TAG, TAG + " onCreate event begin.");

        LoadNativeLibrary();

        this.pUIApplication = new mmUIApplication(this);
        this.pActivityMaster = new mmActivityMaster();

        this.pUIApplication.Init();
        this.pActivityMaster.Init();

        this.pUIApplication.SetActivity(this);
        this.pUIApplication.SetActivityMaster(this.pActivityMaster);
        this.pUIApplication.SetModuleName("mm.nwsi");
        this.pUIApplication.SetRenderSystemName("OpenGL ES 2.x Rendering Subsystem");
        this.pUIApplication.SetLoggerFileName("nwsi");
        this.pUIApplication.SetLoggerLevel(mmLogger.MM_LOG_INFO);
        this.pUIApplication.SetUIFullScreenLayout(true);
        this.pUIApplication.OnFinishLaunching();
        this.pUIApplication.OnStart();

        this.setContentView(this.pUIApplication);

        Log.i(TAG, TAG + " onCreate event end.");
        Log.i(TAG, TAG + " init succeed.");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i(TAG, TAG + " onStart event.");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i(TAG, TAG + " onResume event.");
        this.pUIApplication.OnEnterForeground();
    }

    @Override
    protected void onPause()
    {
        this.pUIApplication.OnEnterBackground();
        Log.i(TAG, TAG + " onPause event.");
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i(TAG, TAG + " onStop event.");
    }

    @Override
    protected void onDestroy()
    {
        Log.i(TAG, TAG + " onDestroy event.");

        this.pUIApplication.OnShutdown();
        this.pUIApplication.OnJoin();
        this.pUIApplication.OnBeforeTerminate();

        this.pActivityMaster.Destroy();
        this.pUIApplication.Destroy();

        this.pUIApplication = null;
        this.pActivityMaster = null;

        super.onDestroy();
        Log.i(TAG, TAG + " destroy succeed.");
    }

    static public void LoadNativeLibrary()
    {
        // os library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("OpenSLES");
        mmUIApplication.LoadLibrary("vulkan");
        // ----------------------------------------------------------

        // stl library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("c++_shared");
        // ----------------------------------------------------------

        // Tools library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("z_shared");
        mmUIApplication.LoadLibrary("minizip_shared");

        mmUIApplication.LoadLibrary("mm_core_shared");
        mmUIApplication.LoadLibrary("mm_net_shared");
        mmUIApplication.LoadLibrary("mm_dish_shared");
        mmUIApplication.LoadLibrary("mm_nwsi_shared");
        mmUIApplication.LoadLibrary("mm_emu_shared");
        // ----------------------------------------------------------

        // Expansion library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("OpenAL_shared");
        // ----------------------------------------------------------
        // Main library.
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("mm_vkApp_shared");
        // ----------------------------------------------------------
    }
}