LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
# MM_HOME ?= ../../../../..
################################################################################
include $(LOCAL_PATH)/lib-nwsi-prebuild.mk
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libOpenAL_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openal-soft/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libOpenAL_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openal-soft/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libcJSON_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/cJSON/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcJSON_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libcJSON_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/cJSON/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcJSON_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libogg_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libogg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libogg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libogg_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libogg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libogg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libvorbis_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libvorbis/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libvorbis_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libvorbis_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libvorbis/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libvorbis_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_sxwnl_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_sxwnl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_sxwnl_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_sxwnl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libmm_emu_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-nes/emu/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_emu_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libmm_emu_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-nes/emu/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_emu_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
