################################################################################
# MM_HOME ?= ../../../../..
################################################################################
LOCAL_LDLIBS 	+= -lvulkan
##########################################################################
LOCAL_SHARED_LIBRARIES += c++_shared

LOCAL_SHARED_LIBRARIES += libz_shared
LOCAL_SHARED_LIBRARIES += libminizip_shared

LOCAL_SHARED_LIBRARIES += libmm_core_shared
LOCAL_SHARED_LIBRARIES += libmm_net_shared
LOCAL_SHARED_LIBRARIES += libmm_dish_shared
LOCAL_SHARED_LIBRARIES += libmm_nwsi_shared
##########################################################################
LOCAL_STATIC_LIBRARIES += libmm_unicode_static

LOCAL_STATIC_LIBRARIES += libFreeImage_static
LOCAL_STATIC_LIBRARIES += libOpenEXR_static
LOCAL_STATIC_LIBRARIES += libIlmThread_static
LOCAL_STATIC_LIBRARIES += libIex_static
LOCAL_STATIC_LIBRARIES += libImath_static
LOCAL_STATIC_LIBRARIES += libtiff_static
LOCAL_STATIC_LIBRARIES += libwebp_static
LOCAL_STATIC_LIBRARIES += libopenjp2_static
LOCAL_STATIC_LIBRARIES += libRaw_static
LOCAL_STATIC_LIBRARIES += libjxr_static
LOCAL_STATIC_LIBRARIES += libjpeg_static
LOCAL_STATIC_LIBRARIES += libpng_static

LOCAL_STATIC_LIBRARIES += libktx_static
LOCAL_STATIC_LIBRARIES += libdfdutils_static
LOCAL_STATIC_LIBRARIES += libbasisu_static
LOCAL_STATIC_LIBRARIES += libzstd_static

LOCAL_STATIC_LIBRARIES += libdraco_static

LOCAL_STATIC_LIBRARIES += libglslang_static
LOCAL_STATIC_LIBRARIES += libSPIRV_static

LOCAL_STATIC_LIBRARIES += cpufeatures
LOCAL_STATIC_LIBRARIES += android_native_app_glue
##########################################################################
LOCAL_C_INCLUDES += $(ANDROID_NDK)/sources/cpufeatures 

LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/mm/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/mm/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/dish/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/dish/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/nwsi/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/nwsi/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/nwsi/src/os/$(MM_PLATFORM)/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/lua/src

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/FreeImage/Source

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/zlib
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/zlib/contrib

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/KTX-Software/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/KTX-Software/lib
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/KTX-Software/utils

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/draco/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/draco/include/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/glslang
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/glslang/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
##########################################################################
