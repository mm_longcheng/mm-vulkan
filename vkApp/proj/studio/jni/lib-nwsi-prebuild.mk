LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
# MM_HOME ?= ../../../../..
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libminizip_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libminizip_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_net_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_net_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_net_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_net_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmm_unicode_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_unicode_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libmm_unicode_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_unicode_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmm_nwsi_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_nwsi_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libmm_nwsi_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_nwsi_static.a
include $(PREBUILT_STATIC_LIBRARY)

################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzstd_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libzstd_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zstd/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzstd_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libdfdutils_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/dfdutils/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libdfdutils_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libdfdutils_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/dfdutils/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libdfdutils_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libktx_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/KTX-Software/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libktx_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libktx_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/KTX-Software/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libktx_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libbasisu_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/basis_universal/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libbasisu_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libbasisu_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/basis_universal/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libbasisu_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libjpeg_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libjpeg_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/jpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjpeg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libjxr_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/jxrlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjxr_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libjxr_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/jxrlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libjxr_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libpng_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libpng/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libpng_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libpng_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libpng/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libpng_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libopenjp2_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openjpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libopenjp2_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libopenjp2_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openjpeg/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libopenjp2_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libRaw_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/LibRaw/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRaw_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libRaw_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/LibRaw/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRaw_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libtiff_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/tiff/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtiff_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libtiff_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/tiff/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtiff_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libwebp_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libwebp_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/libwebp/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libwebp_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libImath_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/Imath/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libImath_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libImath_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/Imath/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libImath_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libIex_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIex_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libIex_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIex_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libIlmThread_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIlmThread_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libIlmThread_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libIlmThread_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libOpenEXR_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenEXR_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libOpenEXR_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openexr/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenEXR_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libFreeImage_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/FreeImage/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libFreeImage_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libFreeImage_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/FreeImage/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libFreeImage_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libdraco_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/draco/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libdraco_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libdraco_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/draco/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libdraco_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libglslang_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/glslang/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libglslang_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libglslang_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/glslang/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libglslang_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSPIRV_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/glslang/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libSPIRV_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := libSPIRV_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/glslang/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libSPIRV_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################