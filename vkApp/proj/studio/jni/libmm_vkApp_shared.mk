LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_vkApp_shared
LOCAL_MODULE_FILENAME := libmm_vkApp_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DMM_STATIC_NWSI

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libOpenAL_shared
LOCAL_SHARED_LIBRARIES += libmm_emu_shared
########################################################################
LOCAL_STATIC_LIBRARIES += libcJSON_static
LOCAL_STATIC_LIBRARIES += libvorbis_static
LOCAL_STATIC_LIBRARIES += libogg_static

LOCAL_STATIC_LIBRARIES += libmm_sxwnl_static
########################################################################
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/protobuf/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/openssl/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/openssl/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/zlib/contrib
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/sqlite/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/sqlite/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/openal-soft/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/cJSON
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/libogg/include/$(MM_PLATFORM)/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/libogg/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/build/libvorbis/include/$(MM_PLATFORM)/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-libx/src/libvorbis/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm-core/data/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm-nes/emu/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/application
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
include $(LOCAL_PATH)/lib-nwsi-flags.mk
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/application
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/al
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/vk
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ui
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/parse
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/glsl
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/vma
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/mp3
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/nwsi
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/entityx
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/test
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/emulatorX
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/view
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/cube
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/data
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/vkcube%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
