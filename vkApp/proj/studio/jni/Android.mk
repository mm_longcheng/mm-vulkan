LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libmm_vkApp_shared.mk

$(call import-module,android/cpufeatures) 
$(call import-module,android/native_app_glue)