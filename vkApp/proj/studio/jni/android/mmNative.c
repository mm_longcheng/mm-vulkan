#include <jni.h>

#include "dish/mmJavaVMEnv.h"

#include "dish/mmDishNative.h"
#include "nwsi/mmNwsiNative.h"

#include "mmActivityMasterAndroid.h"

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    mmDishNative_AttachVirtualMachine(vm);
    mmNwsiNative_AttachVirtualMachine(vm);

    mmActivityMaster_AttachVirtualMachine(env);
    return JNI_VERSION_1_4;
}
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    mmActivityMaster_DetachVirtualMachine(env);

    mmNwsiNative_DetachVirtualMachine(vm);
    mmDishNative_DetachVirtualMachine(vm);
}
